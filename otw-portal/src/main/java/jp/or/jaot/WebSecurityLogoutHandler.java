package jp.or.jaot;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import jp.or.jaot.core.util.JwtUtil;
import jp.or.jaot.model.dto.MemberLoginHistoryDto;
import jp.or.jaot.model.entity.MemberLoginHistoryEntity;
import jp.or.jaot.service.MemberLoginHistoryService;

/**
 * ログアウトハンドラ
 */
@Configuration
public class WebSecurityLogoutHandler implements LogoutHandler {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(WebSecurityLogoutHandler.class);

	/** 会員ポータルログイン履歴サービス */
	@Autowired
	private MemberLoginHistoryService memberLoginHistoryService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {

		// JWT取得
		String token = request.getHeader(JwtUtil.HEADER_STRING);
		if (!JwtUtil.isValidToken(token)) {
			logger.error("logout(Invalid) : token={}", token);
			return;
		}

		// ログアウト処理
		try {

			// 認証情報取得
			String principal = JwtUtil.getPrincipal(token);

			// ログイン履歴追加
			enterMemberLoginHistory(Integer.parseInt(principal));

			// ログ出力
			logger.info("logout() : username={}", principal);
		} catch (Exception e) {
			logger.error("logout()", e);
		}
	}

	/**
	 * ログアウト履歴追加
	 * @param memberNo 会員番号
	 */
	private void enterMemberLoginHistory(Integer memberNo) {

		// DTO生成
		MemberLoginHistoryDto enterDto = MemberLoginHistoryDto.createSingle();
		MemberLoginHistoryEntity enterEntity = enterDto.getFirstEntity();
		enterEntity.setMemberNo(memberNo); // 会員番号
		enterEntity.setLogoutDatetime(new Timestamp(System.currentTimeMillis())); // ログアウト日時(システム現在日時)

		// 登録
		memberLoginHistoryService.enter(enterDto);
	}
}
