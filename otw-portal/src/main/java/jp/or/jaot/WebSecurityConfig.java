package jp.or.jaot;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import jp.or.jaot.core.util.JwtUtil;
import jp.or.jaot.filter.JWTAuthenticationFilter;
import jp.or.jaot.filter.JWTAuthorizationFilter;

/**
 * Springセキュリティ設定クラス
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);

	/** HTTPアクセス除外パターン */
	private static String[] DEF_IGNORING_PATTERNS = { "/favicon.ico", "/webjars/**", "/*.js" };

	/** 許可リクエストパターン */
	private static String[] DEF_PERMIT_ALL_PATTERNS = { "/profile/enter_member", "/profile/send_password_reissue_mail" , "/profile/send_password_resetting_mail"}; // TODO:開発用(/profile/enter_member)

	/** アクセス許可サーバ */
	@Value("${jwt.allowed.origin}")
	private String allowedOrigin;

	/** ユーザ名(デフォルト) */
	@Value("${jwt.default.user}")
	private String defaultUser;

	/** パスワード(デフォルト) */
	@Value("${jwt.default.password}")
	private String defaultPassword;

	/** インメモリ認証フラグ */
	@Value("${jwt.authentication.memory}")
	private Boolean inMemory;

	/** パスワードエンコーダ */
	@Autowired
	private PasswordEncoder passwordEncoder;

	/** 認証プロバイダ */
	@Autowired
	private WebSecurityAuthenticationProvider authenticationProvider;

	/** ログアウトハンドラ */
	@Autowired
	private WebSecurityLogoutHandler logoutHandler;

	/**
	 * JWT認証フィルタ(Bean定義)
	 */
	@Bean
	public JWTAuthenticationFilter jwtAuthenticationFilter() throws Exception {
		return new JWTAuthenticationFilter(authenticationManager());
	}

	/**
	 * JWT認可フィルタ(Bean定義)
	 */
	@Bean
	public JWTAuthorizationFilter jwtAuthorizationFilter() throws Exception {
		return new JWTAuthorizationFilter(authenticationManager());
	}

	/**
	 * ログアウト成功ハンドラ(Bean定義)
	 */
	@Bean
	public LogoutSuccessHandler logoutSuccessHandler() {
		logger.info("logoutSuccessHandler()");
		return new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK);
	}

	/**
	 * オリジン間リソース共有設定ソース(Bean定義)
	 */
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		logger.info("corsConfigurationSource() : allowedOrigin={}", allowedOrigin);
		CorsConfiguration configuration = new CorsConfiguration();
		List<String> allAllowedMethods = Collections.singletonList("*");
		configuration.setAllowedOrigins(Arrays.asList(allowedOrigin));
		configuration.setAllowedMethods(allAllowedMethods);
		configuration.setAllowedHeaders(allAllowedMethods);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

	/**
	 * グローバル設定
	 * @param auth 認証マネージャビルダ
	 * @throws Exception 例外
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		logger.info("configureGlobal() : inMemory={}, EXPIRATION_TIME={}, SECRET_KEY={}", inMemory,
				JwtUtil.EXPIRATION_TIME, JwtUtil.SECRET_KEY);
		// 認証
		if (inMemory) {
			String encodePass = passwordEncoder.encode(defaultPassword);
			auth.inMemoryAuthentication().withUser(defaultUser).password(encodePass).roles("USER");
			logger.info("configureGlobal() : User={}, Password={}, Encode={}", defaultUser, defaultPassword,
					encodePass);
		} else {
			auth.authenticationProvider(authenticationProvider);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		logger.info("configure(web) : DEF_IGNORING_PATTERNS={}", StringUtils.join(DEF_IGNORING_PATTERNS, ','));
		web.ignoring().antMatchers(DEF_IGNORING_PATTERNS); // Httpアクセス除外パターン設定
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		logger.info("configure(http) : DEF_PERMIT_ALL_PATTERNS={}", StringUtils.join(DEF_PERMIT_ALL_PATTERNS, ','));
		http
				.cors()// オリジン間リソース共有
				.and().authorizeRequests()
				.antMatchers("/").permitAll() // 暫定的にすべてのリクエストを許可する場合は"/*"を追加
				.antMatchers(DEF_PERMIT_ALL_PATTERNS).permitAll() // 許可リクエストパターン設定
				.anyRequest().authenticated()
				.and().logout().addLogoutHandler(logoutHandler).logoutSuccessHandler(logoutSuccessHandler())
				.and().csrf().disable() // CSRF不使用
				.addFilter(jwtAuthenticationFilter()) // JWT認証フィルタ
				.addFilter(jwtAuthorizationFilter()) // JWT認可フィルタ
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); // ステートレス
	}
}