package jp.or.jaot.aspect;

import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * ロギング用インタセプター
 */
@Aspect
@Component
@Order(100)
public class LoggingInterceptor {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(LoggingInterceptor.class);

	/**
	 * ロギング用INVOKE
	 * @param invocation メソッド情報
	 * @return Object
	 * @throws Throwable
	 */
	@Around("execution(public * jp.or.jaot.controller..*.*(..))")
	public Object invoke(ProceedingJoinPoint joinPoint) throws Throwable {

		String methodName = joinPoint.getSignature().getName();
		String className = joinPoint.getTarget().getClass().toString();
		Object[] args = joinPoint.getArgs();

		HttpServletRequest request = null;
		for (Object arg : args) {
			if (arg instanceof HttpServletRequest) {
				request = (HttpServletRequest) arg;
			}
		}

		HttpSession session = null;
		if (request != null) {
			session = request.getSession(false);
		}

		Object ret;

		// ログインユーザID取得
		String userId = null;
		if (session != null) {
			userId = (String) session.getAttribute("uid");
		}
		if (userId == null) {
			userId = "no session";
		}

		// ログに、接続情報を記録(開始情報)
		logger.info("USER:【" + userId + "】 StartClassName:【" + className + "】 StartMethod【" + methodName + "】");

		// 入力値の内容をログに出力する
		try {
			StringBuilder message = new StringBuilder();
			// Form取得
			for (Object arg : args) {
				if ((arg == null) || (arg.getClass() == null) || (arg.getClass().getName() == null)) {
					continue;
				}

				// 引数について出力
				if (message.length() > 0) {
					message.append("\n");
				}
				message.append(methodName + " : " + arg.getClass().getSimpleName() + " : ");
				if (arg.getClass().getName().endsWith("Form")) {
					message.append("Form contents...\n");
					message.append(generateFormLog(arg));
				} else {
					message.append(arg.toString());
				}
			}

			// ログ出力
			logger.info("----- [" + methodName + "] log start-----");
			logger.info(message.toString());
			logger.info("----- [" + methodName + "] log end-----");
		} catch (ClassCastException e) {
			// エラーは握りつぶす
			logger.error("[" + methodName + "] audit log failed. message = " + e.getMessage());
		}

		// メソッドを実行し、結果を保持
		ret = joinPoint.proceed();
		// ログに、接続情報を記録
		logger.info("USER:【" + userId + "】 EndClassName:【" + className + "】EndMethod【" + methodName + "】");

		return ret;
	}

	/**
	 * オブジェクトの内容について、メッセージを生成する
	 * @param formObject 入力情報
	 * @return メッセージ文字列
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws IntrospectionException
	 */
	private String generateFormLog(Object formObject)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		Field[] fields = formObject.getClass().getDeclaredFields();

		// 文言を作る
		StringBuilder sb = new StringBuilder();
		for (Field field : fields) {
			if (field == null) {
				continue;
			}

			// フィールド名を取得
			String fieldName = field.getName();
			if (fieldName == null) {
				fieldName = "";
			}

			// フィールドの値を取得
			field.setAccessible(true);
			Object value = field.get(formObject);
			StringBuilder valueString = new StringBuilder();
			if (value == null) {
				valueString.append("null");
			} else if (value instanceof Iterable) {
				@SuppressWarnings("rawtypes")
				Iterable ite = (Iterable) value;
				for (Object item : ite) {
					if (valueString.length() > 0) {
						valueString.append(", ");
					}
					valueString.append(item.toString());
				}
			} else {
				valueString.append(value.toString());
			}

			// メッセージ文字列に追加
			fieldName = fieldName + repeatString(" ", 20 - fieldName.length());
			if (sb.length() > 0) {
				sb.append("\n");
			}
			sb.append("  " + fieldName + " : " + valueString);
		}

		return sb.toString();
	}

	/**
	 * 文字列を指定回数分繰り返した文字列を返す(結果整形用)
	 */
	private String repeatString(String str, int cnt) {
		String allString = "";
		for (int i = 0; i < cnt; i++) {
			allString = allString + str;
		}
		return allString;
	}

}
