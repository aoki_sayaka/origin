package jp.or.jaot.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * メソッドロギング用インタセプター
 */
@Aspect
@Component
public class MethodLoggingInterceptor {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(MethodLoggingInterceptor.class);

	/**
	 * メソッド前処理
	 * @param joinPoint 挿入場所
	 */
	@Before("execution(* jp.or.jaot.controller..*.*(..)) || execution(* jp.or.jaot.service..*.*(..)) || execution(* jp.or.jaot.domain..*.*(..)) || execution(* jp.or.jaot.datasource..*.*(..))")
	public void doBefore(JoinPoint joinPoint) {
		logger.trace("@Before : {}", joinPoint.toString());
	}

	/**
	 * メソッド後処理(正常終了)
	 * @param joinPoint 挿入場所
	 */
	@AfterReturning("execution(* jp.or.jaot.controller..*.*(..)) || execution(* jp.or.jaot.service..*.*(..)) || execution(* jp.or.jaot.domain..*.*(..)) || execution(* jp.or.jaot.datasource..*.*(..))")
	public void doAfterReturning(JoinPoint joinPoint) {
		logger.trace("@After  : {}", joinPoint.toString());
	}

	/**
	 * メソッド後処理(例外発生)
	 * @param joinPoint 挿入場所
	 */
	@AfterThrowing("execution(* jp.or.jaot.controller..*.*(..)) || execution(* jp.or.jaot.service..*.*(..)) || execution(* jp.or.jaot.domain..*.*(..)) || execution(* jp.or.jaot.datasource..*.*(..))")
	public void doAfterThrowing(JoinPoint joinPoint) {
		logger.trace("#After  : {}", joinPoint.toString());
	}
}
