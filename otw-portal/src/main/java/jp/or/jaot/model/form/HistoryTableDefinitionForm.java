package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.HistoryTableDefinitionEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 履歴テーブル定義フォーム
 * @see HistoryTableDefinitionEntity
 */
@Getter
@Setter
public class HistoryTableDefinitionForm extends BaseForm {

	/** ID */
	private Long id;

	/** テーブルコード */
	private Integer tableCd;

	/** テーブル名 */
	private String tableNm;

	/** テーブル名(物理) */
	private String tablePhysicalNm;

	/** 履歴フィールド定義情報 */
	private List<HistoryFieldDefinitionForm> historyFieldDefinitions = new ArrayList<HistoryFieldDefinitionForm>();
}
