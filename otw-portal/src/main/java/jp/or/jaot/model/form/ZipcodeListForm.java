package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 郵便番号リストフォーム
 */
@Getter
@Setter
public class ZipcodeListForm extends BaseListForm {

	/** 郵便番号 */
	List<ZipcodeForm> zipcodes = new ArrayList<ZipcodeForm>();
}
