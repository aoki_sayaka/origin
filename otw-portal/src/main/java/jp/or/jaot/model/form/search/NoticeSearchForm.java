package jp.or.jaot.model.form.search;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ検索フォーム
 */
@Getter
@Setter
public class NoticeSearchForm implements Serializable {

	/** 掲載日範囲(検索対象とする日数) */
	private Integer publishRangeDay;
}
