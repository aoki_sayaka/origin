package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.SocialContributionActual2FieldEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績の領域2(後輩育成・社会貢献)フォーム
 * @see SocialContributionActual2FieldEntity
 */
@Getter
@Setter
public class SocialContributionActual2FieldForm extends BaseForm {

	/** ID */
	private Long id;

	/** 活動実績ID */
	private Long resultId;

	/** 領域コード */
	private String fieldCd;

	/** 削除フラグ */
	private Boolean deleted;
}
