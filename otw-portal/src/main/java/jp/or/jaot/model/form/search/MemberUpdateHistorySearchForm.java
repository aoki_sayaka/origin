package jp.or.jaot.model.form.search;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

/**
 * 会員更新履歴検索フォーム
 */
@Getter
@Setter
public class MemberUpdateHistorySearchForm implements Serializable {

	/** 会員番号 */
	private Integer memberNo;

	/** 更新区分コード */
	private Integer updateCategoryCd;

	/** テーブルコード */
	private Integer tableCd;

	/** 項目コード */
	private List<Integer> fieldCds = new ArrayList<Integer>();

	/** 更新日(From) */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Timestamp updateFromDate;

	/** 更新日(To) */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Timestamp updateToDate;
}
