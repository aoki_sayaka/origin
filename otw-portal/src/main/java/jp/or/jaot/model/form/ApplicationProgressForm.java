package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.ApplicationProgressEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 申請進捗フォーム
 * @see ApplicationProgressEntity
 */
@Getter
@Setter
public class ApplicationProgressForm extends BaseForm {

	/** ID */
	private Long id;

	/** 申請番号 */
	private Long applicationNo;

	/** 申請枝番 */
	private Integer branchNo;

	/** 連番 */
	private Integer seqNo;

	/** 登録者区分 */
	private String registerCategoryCd;

	/** 会員番号 */
	private Integer memberNo;

	/** 申請種別 */
	private String applicationTypeCd;

	/** 申請種類 */
	private String applicationKindCd;

	/** 申請区分 */
	private String applicationCategoryCd;

	/** 受付日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date applyDate;

	/** 送付日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date sendDate;

	/** 受領日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date receiptDate;

	/** 承認日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date approvalDate;

	/** 状況 */
	private String progressCd;

	/** 状態 */
	private String statusCd;

	/** 送付区分 */
	private String sendCategoryCd;

	/** 送付回数 */
	private Integer sendCount;
}
