package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.MemberFacilityCategoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員勤務先業務分類フォーム
 * @see MemberFacilityCategoryEntity
 */
@Getter
@Setter
public class MemberFacilityCategoryForm extends BaseFullForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 分類区分 */
	private Integer categoryDivision;

	/** 主従区分 */
	private Integer primaryDivision;

	/** 親分類コード */
	private String parentCode;

	/** 分類コード */
	private String categoryCode;
}