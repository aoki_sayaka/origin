package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.PaotOfficerHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 県士会役員履歴フォーム
 * @see PaotOfficerHistoryEntity
 */
@Getter
@Setter
public class PaotOfficerHistoryForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 県士会番号 */
	private Integer paotCd;

	/** 役職コード */
	private Integer positionCd;

	/** 部署コード */
	private Integer departmentCd;

	/** 任用開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startAppointDate;

	/** 任用終了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endAppointDate;

	/** 備考 */
	private String remarks;

	// TODO
}
