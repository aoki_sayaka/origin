package jp.or.jaot.model.form.search;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 汎用コード検索フォーム
 */
@Getter
@Setter
public class GeneralCodeSearchForm implements Serializable {

	/** コードID */
	private String id;

	/** コード値 */
	private String code;
}
