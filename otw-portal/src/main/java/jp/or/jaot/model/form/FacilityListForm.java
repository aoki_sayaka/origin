package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校リストフォーム
 */
@Getter
@Setter
public class FacilityListForm extends BaseListForm {

	/** 施設養成校 */
	List<FacilityForm> facilities = new ArrayList<FacilityForm>();
}
