package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MemberRoleEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員ロールフォーム
 * @see MemberRoleEntity
 */
@Getter
@Setter
public class MemberRoleForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 権限コード識別ID */
	private String roleCodeId;

	/** 権限コード値 */
	private String roleCode;
}
