package jp.or.jaot.model.utils;

public class GenericResponse {
	public boolean success;
	public String message;

	public GenericResponse(boolean success, String message) {
		this.success = success;
		this.message = message;
	}

	public GenericResponse(boolean success) {
		this.success =success;
		this.message = null;
	}
}