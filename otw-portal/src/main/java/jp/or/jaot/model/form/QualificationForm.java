package jp.or.jaot.model.form;

import jp.or.jaot.model.entity.QualificationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 関連資格フォーム
 * @see QualificationEntity
 */
@Getter
@Setter
public class QualificationForm {

	/** ID */
	private Long id;

	/** コード */
	private String code;

	/** 区分コード */
	private String categoryCd;

	/** 名称 */
	private String name;

	/** 旧コード */
	private String oldCode;
}
