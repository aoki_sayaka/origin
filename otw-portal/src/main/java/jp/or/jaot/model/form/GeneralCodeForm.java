package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.entity.GeneralCodeEntity;
import jp.or.jaot.model.form.search.GeneralCodeSearchForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 汎用コードフォーム
 * @see GeneralCodeEntity
 */
@Getter
@Setter
public class GeneralCodeForm extends GeneralCodeSearchForm {

	/** 内容 */
	private String content;

	/** 省略内容 */
	private String simpleContent;

	/** 親コード値 */
	private String parentCode;

	/** 表示順序 */
	private Integer sortOrder;

	/** 汎用コード(子要素) */
	private List<GeneralCodeForm> children = new ArrayList<GeneralCodeForm>();
}
