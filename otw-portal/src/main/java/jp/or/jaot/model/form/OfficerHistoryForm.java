package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.OfficerHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 役員履歴フォーム
 * @see OfficerHistoryEntity
 */
@Getter
@Setter
public class OfficerHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 役職コード */
	private String positionCd;

	/** 役職分類 */
	private Integer positionType;

	/** 都道府県コード */
	private String prefectureCd;

	/** 任用開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startAppointDate;

	/** 任用終了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endAppointDate;

	/** 備考 */
	private String remarks;
}
