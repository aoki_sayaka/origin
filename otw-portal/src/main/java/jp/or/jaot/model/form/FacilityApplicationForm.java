package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.FacilityApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請フォーム
 * @see FacilityApplicationEntity
 */
@Getter
@Setter
public class FacilityApplicationForm extends BaseForm {

	/** ID */
	private Long id;

	/** 施設区分 */
	private String facilityCategory;

	/** 申請者区分コード */
	private String memberCategoryCd;

	/** 会員番号 */
	private Integer memberNo;

	/** 仮会員番号 */
	private Integer temporaryMemberNo;

	/** 申請ステータス */
	private Integer status;

	/** 開設者種別コード */
	private String openerTypeCd;

	/** 開設者名 */
	private String openerNm;

	/** 開設者名(カナ) */
	private String openerNmKana;

	/** 責任者メールアドレス */
	private String responsibleEmailAddress;

	/** 施設名 */
	private String facilityNm;

	/** 施設名２ */
	private String facilityNm2;

	/** 施設名(カナ) */
	private String facilityNmKana;

	/** 施設名(カナ)２ */
	private String facilityNmKana2;

	/** 郵便番号 */
	private String zipcode;

	/** 施設都道府県コード */
	private String facilityPrefectureCd;

	/** 住所１ */
	private String address1;

	/** 住所２ */
	private String address2;

	/** 住所３ */
	private String address3;

	/** 代表電話番号 */
	private String primaryPhoneNumber;

	/** FAX番号 */
	private String faxNumber;

	/** 団体・法人名 */
	private String corporateNm;

	/** 団体・法人名(カナ) */
	private String corporateNmKana;

	/** 学部・学科・専攻 */
	private String department;

	/** 養成校種別 */
	private String trainingSchoolCategoryCd;

	/** 施設登録申請分類 */
	List<FacilityApplicationCategoryForm> facilityApplicationCategories = new ArrayList<FacilityApplicationCategoryForm>();
}
