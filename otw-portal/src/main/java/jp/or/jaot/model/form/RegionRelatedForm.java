package jp.or.jaot.model.form;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 領域関連分類フォーム
 */
@Getter
@Setter
public class RegionRelatedForm implements Serializable {

	/** コード識別ID */
	private String codeId;

	/** コード値 */
	private String code;

	/** 内容 */
	private String content;

	/** 省略内容 */
	private String simpleContent;

	/** 親コード値 */
	private String parentCode;

	/** 表示順序 */
	private Integer sortOrder;
}
