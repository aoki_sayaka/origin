package jp.or.jaot.model.form.update;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.NoticeForm;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせフォーム(更新)
 */
@Getter
@Setter
public class NoticeUpdateForm extends NoticeForm {

	/** 初期化対象プロパティ */
	private List<String> initProperties = new ArrayList<String>();

	/** お知らせ(都道府県) */
	private List<NoticeConditionPrefUpdateForm> updateNoticeConditionPrefs = new ArrayList<NoticeConditionPrefUpdateForm>();

	/** お知らせ(宛先) */
	private List<NoticeDestinationUpdateForm> updateNoticeDestinations = new ArrayList<NoticeDestinationUpdateForm>();
}
