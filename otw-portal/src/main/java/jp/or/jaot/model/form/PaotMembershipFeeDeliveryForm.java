package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.PaotMembershipFeeDeliveryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 県士会会費納入フォーム
 * @see PaotMembershipFeeDeliveryEntity
 */
@Getter
@Setter
public class PaotMembershipFeeDeliveryForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 県士会番号 */
	private Integer paotCd;

	/** 年度 */
	private Integer fisicalYear;

	/** 会費区分コード */
	private String categoryCd;

	/** 請求額 */
	private Integer billingAmount;

	/** 入金額 */
	private Integer paymentAmount;

	/** 請求残額 */
	private Integer billingBalance;

	/** 入金日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date paymentDate;

	/** 備考 */
	private String remarks;
}
