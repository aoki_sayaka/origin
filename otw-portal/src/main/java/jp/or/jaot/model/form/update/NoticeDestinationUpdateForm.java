package jp.or.jaot.model.form.update;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.NoticeDestinationForm;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(宛先)フォーム(更新)
 */
@Getter
@Setter
public class NoticeDestinationUpdateForm extends NoticeDestinationForm {

	/** 初期化対象プロパティ */
	private List<String> initProperties = new ArrayList<String>();
}
