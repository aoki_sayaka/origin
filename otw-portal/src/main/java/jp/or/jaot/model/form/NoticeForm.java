package jp.or.jaot.model.form;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.NoticeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせフォーム
 * @see NoticeEntity
 */
@Getter
@Setter
public class NoticeForm extends BaseForm {

	/** ID */
	private Long id;

	/** 対象 */
	private Integer target;

	/** カテゴリ */
	private Integer category;

	/** 重要 */
	private Boolean priority;

	/** 掲載開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp publishFrom;

	/** 掲載終了日 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp publishTo;

	/** システムメッセージ */
	private Boolean systemMessage;

	/** 件名 */
	private String subject;

	/** 内容 */
	private String content;

	/** 宛先 */
	private Integer destination;

	/** 宛先区分 */
	private Integer destinationType;

	/** 宛先ファイル名 */
	private String destinationFileNm;

	/** 宛先範囲施設 */
	private Boolean destinationRangeFacility;

	/** 宛先範囲養成校 */
	private Boolean destinationRangeTrainingSchool;

	/** お知らせ(都道府県) */
	private List<NoticeConditionPrefForm> noticeConditionPrefs = new ArrayList<NoticeConditionPrefForm>();

	/** お知らせ(宛先) */
	private List<NoticeDestinationForm> noticeDestinations = new ArrayList<NoticeDestinationForm>();
}
