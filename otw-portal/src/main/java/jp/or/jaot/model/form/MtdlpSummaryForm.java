package jp.or.jaot.model.form;

import jp.or.jaot.model.entity.MtdlpSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP履歴フォーム
 * @see MtdlpSummaryEntity
 */
@Getter
@Setter
public class MtdlpSummaryForm extends MtdlpSummaryUpdateForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 削除フラグ */
	private Boolean deleted;

	/** バージョン */
	private Integer versionNo;

}