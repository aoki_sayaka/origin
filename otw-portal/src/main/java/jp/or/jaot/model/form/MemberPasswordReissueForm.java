package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import lombok.Getter;
import lombok.Setter;

/**
 * パスワード再発行フォーム
 * @see MemberPasswordReissueEntity
 */
@Getter
@Setter
public class MemberPasswordReissueForm extends BaseForm {
	
	/** 会員コード */
	private String memberCd;
	
	/** セキュリティコード */
	private String securityCd;
	
	/** 生年月日 */
	@JsonFormat(pattern = "yyyyMMdd")
	private Date birthDay;
	
	/** メールアドレス */
	private String emailAddress;
}