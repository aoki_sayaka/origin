package jp.or.jaot.model.form;

import javax.persistence.Id;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.LifelongEducationStatusEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 生涯教育ステータスフォーム
 * @see LifelongEducationStatusEntity
 */
@Getter
@Setter
public class LifeLongEducationStatusesForm extends BaseForm {

	/** ID */
	@Id
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 現在のポイント */
	private Integer point;

	/** 使用済みポイント */
	private Integer usedPoint;
}
