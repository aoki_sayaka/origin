package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.FacilityEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校フォーム
 * @see FacilityEntity
 */
@Getter
@Setter
public class FacilityForm extends BaseForm {

	/** ID */
	private Long id;

	/** 住所１ */
	private String address1;

	/** 住所２ */
	private String address2;

	/** 住所３ */
	private String address3;

	/** 団体・法人名 */
	private String corporateNm;

	/** 団体・法人名(カナ) */
	private String corporateNmKana;

	/** 免許取得３年以上 */
	private Integer countLicense3year;

	/** 免許取得５年以上 */
	private Integer countLicense5year;

	/** 会員在籍者 */
	private Integer countMember;

	/** OT在籍者数 */
	private Integer countOt;

	/** 課程年限 */
	private String courseYears;

	/** 昼間・夜間フラグ */
	private String dayNightFlag;

	/** 学部・学科・専攻 */
	private String department;

	/** 博士課程有無 */
	private String doctorCourseFlag;

	/** 英文表記 */
	private String englishNotation;

	/** 在学中フラグ */
	private Boolean enrollment;

	/** 開設者種別 */
	private String openerTypeCd;

	/** 開設者名 */
	private String openerNm;

	/** 開設者名(カナ) */
	private String openerNmKana;

	/** 施設基本番号 */
	private String facilityBaseNo;

	/** 施設区分 */
	private String facilityCategory;

	/** 施設番号 */
	private String facilityNo;

	/** 施設連番 */
	private Integer facilitySeqNo;

	/** FAX番号 */
	private String faxNumber;

	/** 修士課程有無 */
	private String masterCourseFlag;

	/** MTDLP推進協力校フラグ */
	private Boolean mtdlpCooperatorFlag;

	/** 施設養成校名(漢字) */
	private String name;

	/** 施設養成校名(カナ) */
	private String nameKana;

	/** 施設養成校名２(カナ) */
	private String nameKana2;

	/** 施設養成校名２(漢字) */
	private String name2;

	/** 出身養成校番号 */
	private Integer originSchoolNo;

	/** 都道府県コード */
	private String prefectureCd;

	/** 備考 */
	private String remarks;

	/** 責任者メールアドレス */
	private String responsibleEmailAddress;

	/** 施設情報責任者名 */
	private String responsibleMemberNm;

	/** 施設情報責任者名(カナ) */
	private String responsibleMemberNmKana;

	/** 施設情報責任者会員番号 */
	private Integer responsibleMemberNo;

	/** 代表電話番号 */
	private String telNo;

	/** 養成校種別 */
	private String trainingSchoolCategoryCd;

	/** WFOT認定状況 */
	private Integer wfotCertifiedStatus;

	/** 郵便番号 */
	private String zipcode;

	/** 施設分類 */
	List<FacilityCategoryForm> facilityCategories = new ArrayList<FacilityCategoryForm>();
}
