package jp.or.jaot.model.form.search;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 領域関連分類検索フォーム
 */
@Getter
@Setter
public class RegionRelatedSearchForm implements Serializable {

	/** コード識別ID */
	private String codeId;

	/** コード値(大分類) */
	private String largeCode;

	/** コード値(中分類) */
	private String middleCode;

	/** コード値(小分類) */
	private String smallCode;
}
