package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.AuthorPresenterActualFieldEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績の領域(発表・著作等)フォーム
 * @see AuthorPresenterActualFieldEntity
 */
@Getter
@Setter
public class AuthorPresenterActualFieldForm extends BaseForm {

	/** ID */
	private Long id;

	/** 活動実績ID */
	private Long resultId;

	/** 領域コード */
	private String fieldCd;

	/** 削除フラグ */
	private Boolean deleted;
}
