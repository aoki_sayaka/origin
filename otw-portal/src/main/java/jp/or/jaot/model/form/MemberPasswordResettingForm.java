package jp.or.jaot.model.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import jp.or.jaot.core.model.BaseForm;
import lombok.Getter;
import lombok.Setter;

/**
 * パスワード再発行フォーム
 * @see MemberPasswordReissueEntity
 */
@Getter
@Setter
public class MemberPasswordResettingForm extends BaseForm {
	
	/**
	 * 新しいパスワード
	 */
	@NotNull
	@Size(min = 8, max = 12)
	private String password;
	/**
	 * 新しいパスワード（再入力）
	 */
	@NotNull
	@Size(min = 8, max = 12)
	private String rePassword;
	/**
	 * 仮パスワード
	 */
	@NotNull
	@Size(min = 32, max = 32)
	private String tempPassword;
}