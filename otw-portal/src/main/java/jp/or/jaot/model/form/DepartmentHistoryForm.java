package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.DepartmentHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 部署履歴フォーム
 * @see DepartmentHistoryEntity
 */
@Getter
@Setter
public class DepartmentHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 部署コード */
	private String departmentCd;

	/** 職務コード */
	private String jobCd;

	/** 任用開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startAppointDate;

	/** 任用終了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endAppointDate;

	/** 備考 */
	private String remarks;

	/** 公開日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date releaseDate;
}
