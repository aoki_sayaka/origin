package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修履歴フォーム
 * @see QualifiedOtAttendingSummaryEntity
 */
@Getter
@Setter
public class QualifiedOtAttendingSummaryForm extends QualifiedOtAttendingSummaryUpdateForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 認定年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationDate;

	/** 認定期間満了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationPeriodDate;

	/** 認定番号 */
	private Integer qualificationNumber;

	/** 更新回数 */
	private Integer renewalCount;

}
