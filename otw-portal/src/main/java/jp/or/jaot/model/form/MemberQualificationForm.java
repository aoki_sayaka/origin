package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MemberQualificationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員関連資格情報フォーム
 * @see MemberQualificationEntity
 */
@Getter
@Setter
public class MemberQualificationForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 関連資格コード */
	private String qualificationCd;

	/** 区分コード */
	private String qualificationCategoryCd;

	/** 削除フラグ */
	private Boolean deleted;
}
