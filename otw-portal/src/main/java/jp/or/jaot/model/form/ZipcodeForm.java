package jp.or.jaot.model.form;

import java.io.Serializable;

import jp.or.jaot.model.entity.ZipcodeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 郵便番号フォーム
 * @see ZipcodeEntity
 */
@Getter
@Setter
public class ZipcodeForm implements Serializable {

	/** 郵便番号 */
	private String zipcode;

	/** 住所 */
	private String address;

	/** 都道府県番号 */
	private String prefectureCd;

	/** 都道府県名 */
	private String prefectureName;
}
