package jp.or.jaot.model.form;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.MemberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員フォーム
 * @see MemberEntity
 */
@Getter
@Setter
public class MemberForm extends MemberProfileUpdateForm {

	/** 所属都道府県コード */
	private String affiliatedPrefectureCd;

	/** 年齢 */
	private Integer age;

	/** 県士会所属フラグ */
	private Boolean belongPrefAssociation;

	/** 児童福祉法関連施設 */
	private String childWelfareFacility;

	/** 児童福祉法指定サービス分類コード */
	private String childWelfareServiceCategoryCd;

	/** 生涯教育単位数合計 */
	private Integer educationCredits;

	/** 入会年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date entryDate;

	/** 認可分類コード */
	private String facilityAuthCategoryCd;

	/** 施設情報開示拒否フラグ */
	private Boolean facilityInfoHide;

	/** 外国免許フラグ */
	private Boolean foreignLicense;

	/** 学位取得 */
	private Boolean hasDegree;

	/** 名誉会員 */
	private Boolean honoraryMembership;

	/** 名誉会員取得年 */
	private Integer honoraryMembershipAcquiredYear;

	/** 学術誌送付媒体区分 */
	private String journalSendCategoryCd;

	/** 会員名簿フラグ */
	private Boolean memberList;

	/** 会員証発行フラグ */
	private Boolean membershipCertificateIssuanced;

	/** 会費合計 */
	private Integer membershipFeeBalance;

	/** 生活行為向上リハビリテーション実施加算 */
	private Boolean mtdlpRehabilitationAddition;

	/** 協会メール受取可否 */
	private Integer otAssociationMailReceiveCd;

	/** 前勤務先の都道府県コード */
	private String preWorkingPrefectureCd;

	/** 県士会支部番号 */
	private Integer prefAssociationBranchCd;

	/** 県士会向け生涯教育開示拒否フラグ */
	private Boolean prefAssociationEducationInfoHide;

	/** 県士会会費合計 */
	private Integer prefAssociationFeeBalance;

	/** 県士会メール受取可否 */
	private Integer prefAssociationMailReceiveCd;

	/** 県士会向け会員情報開示拒否フラグ */
	private Boolean prefAssociationMemberInfoHide;

	/** 県士会番号 */
	private Integer prefAssociationNo;

	/** 県士会非会員フラグ */
	private Boolean prefAssociationNonmember;

	/** 県士会発送区分番号 */
	private Integer prefAssociationShippingCategoryNo;

	/** 県士会更新年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date prefAssociationUpdateDate;

	/** 県士会使用欄 */
	private Integer prefAssociationValue;

	/** 旧会員番号 */
	private Integer previousMemberNo;

	/** 印刷フラグ */
	private Boolean printEnabled;

	/** 備考 */
	private String remarks;

	/** ステータス番号 */
	private Integer statusNo;

	/** 対象者の年齢 */
	private Integer targetAge;

	/** 障害者自立支援法サービス分類コード */
	private String targetIndependenceSupportCategoryCd;

	/** 対象者のOT日数 */
	private Integer targetOtDayCount;

	/** 対象者属性の主目的 */
	private String targetPrimaryPurpose;

	/** 卒業養成校番号 */
	private Integer trainingSchoolNo;

	/** 異動確認フラグ */
	private Boolean transferConfirmation;

	/** 会員ポータル更新年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date updateFromPortalDate;

	/** WEB会員ID */
	private String webMemberId;

	/** WEB会員氏名 */
	private String webMemberName;

	/** WEB会員パスワード */
	private String webMemberPassword;

	/** 匿名希望会員フラグ */
	private Boolean webPublished;

	/** 退会年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date withdrawalDate;

	/** 勤務先電話番号 */
	private String workPhoneNumber;

	/** 勤務先都道府県コード */
	private String workingPrefectureCd;

	/** 前回ログイン日時 */
	private Timestamp lastLoginDatetime;

	/** 前回ログアウト日時 */
	private Timestamp lastLogoutDatetime;

	/** 会員コード */
	private String memberCd;

	/** セキュリティコード */
	private String securityCd;

	/** 認定開示フラグ */
	private Boolean disclosed;
}