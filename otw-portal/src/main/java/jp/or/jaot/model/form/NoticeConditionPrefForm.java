package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.NoticeConditionPrefEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(都道府県)フォーム
 * @see NoticeConditionPrefEntity
 */
@Getter
@Setter
public class NoticeConditionPrefForm extends BaseForm {

	/** ID */
	private Long id;

	/** お知らせID */
	private Long noticeId;

	/** 都道府県コード */
	private String prefCode;
}
