package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修履歴検索結果フォーム()
 * @see QualifiedOtAttendingSummaryEntity
 */
@Getter
@Setter
public class QualifiedOtAttendingSummaryUpdateForm extends BaseFullForm {

	private Long id;

	/** 会員関連資格情報 */
	private List<QualifiedOtCompletionHistoryForm> qualifiedOtCompletionHistories = new ArrayList<QualifiedOtCompletionHistoryForm>();

	/** 会員自治体参画情報 */
	private List<QualifiedOtTrainingHistoryForm> qualifiedOtTrainingHistories = new ArrayList<QualifiedOtTrainingHistoryForm>();

	/** 会員自治体参画情報 */
	private List<CaseReportForm> caseReports = new ArrayList<CaseReportForm>();

	/** 会員自治体参画情報 */
	private List<QualificationOtAppHistoryForm> qualificationOtAppHistories = new ArrayList<QualificationOtAppHistoryForm>();

	/** 期間延長申請情報 */
	private List<PeriodExtensionApplicationForm> periodExtensionApplication = new ArrayList<PeriodExtensionApplicationForm>();

	/** 認定作業療法士申請履歴 */
	private List<QualificationOtAppHistoryForm> qualificationOtAppHistory = new ArrayList<QualificationOtAppHistoryForm>();
}
