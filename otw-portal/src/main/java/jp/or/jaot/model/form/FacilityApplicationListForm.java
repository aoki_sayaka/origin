package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請リストフォーム
 */
@Getter
@Setter
public class FacilityApplicationListForm extends BaseListForm {

	/** 施設登録申請 */
	List<FacilityApplicationForm> facilityApplications = new ArrayList<FacilityApplicationForm>();
}
