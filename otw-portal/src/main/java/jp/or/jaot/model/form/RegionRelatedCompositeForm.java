package jp.or.jaot.model.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 領域関連分類フォーム(複合)
 */
@Getter
@Setter
public class RegionRelatedCompositeForm implements Serializable {

	/** 領域関連分類フォーム(大分類) */
	private List<RegionRelatedForm> larges = new ArrayList<RegionRelatedForm>();

	/** 領域関連分類フォーム(中分類) */
	private List<RegionRelatedForm> middles = new ArrayList<RegionRelatedForm>();

	/** 領域関連分類フォーム(小分類) */
	private List<RegionRelatedForm> smalls = new ArrayList<RegionRelatedForm>();
}
