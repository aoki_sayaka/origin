package jp.or.jaot.model.form.update;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import jp.or.jaot.core.model.BaseForm;
import lombok.Getter;
import lombok.Setter;

/**
 * メールアドレス承認フォーム
 */
@Getter
@Setter
public class MemberEmailConfirmForm extends BaseForm {
    /**
     * キー値
     */
    @NotNull
    @Size(min = 1, max = 32)
    private String key;
}
