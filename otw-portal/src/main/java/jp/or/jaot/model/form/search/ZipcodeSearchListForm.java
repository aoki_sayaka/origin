package jp.or.jaot.model.form.search;

import jp.or.jaot.model.form.ZipcodeForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 郵便番号検索リストフォーム
 */
@Getter
@Setter
public class ZipcodeSearchListForm extends ZipcodeForm {

	/** 検索結果の開始位置(初期位置:0) */
	private Integer startPosition = 0;

	/** 検索結果の取得件数(初期件数:0(指定なし)) */
	private Integer maxResult = 0;
}
