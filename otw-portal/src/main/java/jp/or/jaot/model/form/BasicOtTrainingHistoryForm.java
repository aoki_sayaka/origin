package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修受講履歴フォーム
 * @see BasicOtTrainingHistoryEntity
 */
@Getter
@Setter
public class BasicOtTrainingHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

//	/** 研修会コード */
//	private String trainingCd;

//	/** 年度 */
//	private String fiscalYear;

//	/** 主催者コード */
//	private Integer promoterCd;

	/** カテゴリコード */
	private String categoryCd;

	/** 専門分野コード */
	private String professionalFieldCd;

	/** グループコード */
	private String groupCd;

	/** クラスコード */
	private String classCd;

//	/** 枝番 */
//	private String branchCd;

//	/** 回数 */
//	private Integer seq;
//	//	private String seq;

//	/** 研修名 */
//	private String trainingName;

	/** 研修受講年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendDate;

	/** 免除理由コード */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date exemptionReasonCd;


	/** 削除フラグ */
	private Boolean deleted;

	/** バージョン */
	private Integer versionNo;
}
