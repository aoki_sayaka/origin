package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.HistoryFieldDefinitionEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 履歴フィールド定義フォーム
 * @see HistoryFieldDefinitionEntity
 */
@Getter
@Setter
public class HistoryFieldDefinitionForm extends BaseForm {

	/** ID */
	private Long id;

	/** 履歴テーブル定義ID */
	private Long tableDefinitionId;

	/** 項目コード */
	private Integer fieldCd;

	/** 項目名 */
	private String fieldNm;

	/** 項目名(物理) */
	private String fieldPhysicalNm;
}
