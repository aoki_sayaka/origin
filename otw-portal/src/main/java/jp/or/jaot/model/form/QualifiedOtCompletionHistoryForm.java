package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修修了認定更新履歴フォーム
 * @see QualifiedOtCompletionHistoryEntity
 */
@Getter
@Setter
public class QualifiedOtCompletionHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 回数 */
	private Integer seq;

	/** 研修修了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date completionDate;
}
