package jp.or.jaot.model.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import jp.or.jaot.core.model.BaseForm;
import lombok.Getter;
import lombok.Setter;

/**
 * パスワード変更フォーム
 */
@Getter
@Setter
public class ChangePasswordForm extends BaseForm {

	/**
	 * 現在のパスワード
	 */
	@NotNull
	@Size(min = 8, max = 12)
	private String currentPassword;
	/**
	 * 新しいパスワード
	 */
	@NotNull
	@Size(min = 8, max = 12)
	private String password;
	/**
	 * 新しいパスワード（再入力）
	 */
	@NotNull
	@Size(min = 8, max = 12)
	private String passwordConfirm;
}
