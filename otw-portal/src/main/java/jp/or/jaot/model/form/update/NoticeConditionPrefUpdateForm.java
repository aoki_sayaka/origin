package jp.or.jaot.model.form.update;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.NoticeConditionPrefForm;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(都道府県)フォーム(更新)
 */
@Getter
@Setter
public class NoticeConditionPrefUpdateForm extends NoticeConditionPrefForm {

	/** 初期化対象プロパティ */
	private List<String> initProperties = new ArrayList<String>();
}
