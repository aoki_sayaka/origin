package jp.or.jaot.model.form.search;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請検索フォーム
 */
@Getter
@Setter
public class FacilityApplicationSearchForm implements Serializable {

	/** 会員番号 */
	private Integer memberNo;

	/** 仮会員番号 */
	private Integer temporaryMemberNo;

	/** 申請ステータス */
	private Integer status;

	// TODO
}
