package jp.or.jaot.model.form.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 汎用コード検索フォーム(複数)
 */
@Getter
@Setter
public class GeneralCodeSearchMultipleForm implements Serializable {

	/** 汎用コード検索フォーム */
	private List<GeneralCodeSearchForm> generalCodes = new ArrayList<GeneralCodeSearchForm>();
}
