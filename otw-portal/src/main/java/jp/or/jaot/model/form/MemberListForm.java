package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員リストフォーム
 */
@Getter
@Setter
public class MemberListForm extends BaseListForm {

	/** 会員 */
	List<MemberForm> members = new ArrayList<MemberForm>();
}
