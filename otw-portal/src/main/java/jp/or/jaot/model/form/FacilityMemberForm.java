package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.FacilityMemberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校勤務者フォーム
 * @see FacilityMemberEntity
 */
@Getter
@Setter
public class FacilityMemberForm extends BaseForm {

	/** ID */
	private Long id;

	/** 施設養成校ID */
	private Long facilityId;

	/** 会員番号 */
	private Integer memberNo;
}
