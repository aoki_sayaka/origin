package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請分類フォーム
 * @see FacilityApplicationCategoryEntity
 */
@Getter
@Setter
public class FacilityApplicationCategoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 施設登録申請ID */
	private Long applicantId;

	/** 分類種別 */
	private Integer category;

	/** 分類区分 */
	private Integer categoryDivision;

	/** 親分類コード */
	private String parentCode;

	/** 分類コード */
	private String categoryCode;

	/** その他 */
	private String other;

	/** 削除フラグ */
	private Boolean deleted;
}
