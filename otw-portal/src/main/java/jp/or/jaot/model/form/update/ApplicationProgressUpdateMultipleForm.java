package jp.or.jaot.model.form.update;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.ApplicationProgressForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 申請進捗フォーム(複数更新)
 */
@Getter
@Setter
public class ApplicationProgressUpdateMultipleForm {

	/** 状況(一括設定用) */
	private String progressCd;

	/** 状態(一括設定用) */
	private String statusCd;

	/** 申請進捗 */
	List<ApplicationProgressForm> applicationProgresses = new ArrayList<ApplicationProgressForm>();
}
