package jp.or.jaot.model.form.update;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import jp.or.jaot.core.model.BaseForm;
import lombok.Getter;
import lombok.Setter;

/**
 * メールアドレス変更フォーム
 */
@Getter
@Setter
public class MemberEmailUpdateForm extends BaseForm {
	/**
	 * 新しいメールアドレス
	 */
	@NotNull
	@Size(min = 8, max = 40)
	private String email;
	/**
	 * 新しいメールアドレス(再入力)
	 */
	@NotNull
	@Size(min = 8, max = 40)
	private String emailConfirm;
}
