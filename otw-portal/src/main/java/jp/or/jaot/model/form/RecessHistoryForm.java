package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.entity.RecessHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 休会履歴フォーム
 * @see RecessHistoryEntity
 */
@Getter
@Setter
public class RecessHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 回数 */
	private Integer times;

	/** 休会手続き状況 */
	private String processStatus;

	/** 休会理由 */
	private String recessReason;

	/** 不受理理由 */
	private String rejectReason;

	/** 休会開始年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startRecessDate;

	/** 休会終了年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endRecessDate;

	/** 取り下げ手続き状況 */
	private String withdrawalProcessStatus;

	/** 取り下げ確定日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date withdrawalDate;

	/** 取り下げ理由 */
	private String withdrawalReason;

	/** 復会年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date comebackDate;

	/** 印刷年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date printDate;

	/** 勤務先データ削除フラグ */
	private Boolean workPlaceDataDeletion;

	/** 証明書受領フラグ */
	private Boolean certificateReceived;

	/** 備考 */
	private String remarks;

	/** 休会状況(表示専用) */
	public String getRecessStatusText() {
		Date nowDate = new Date(); // 現在日時
		if (startRecessDate != null && endRecessDate != null
				&& DateUtil.getFromDate(startRecessDate).getTime() <= nowDate.getTime()
				&& nowDate.getTime() < DateUtil.getToDate(endRecessDate).getTime()) {
			return String.format("%d年度 休暇中 %d回目", DateUtil.getFiscalYear(nowDate), times);
		}
		return "活動中";
	}
}
