package jp.or.jaot.model.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 会員利用可能状態取得フォーム
 */
@Getter
@Setter
public class MemberIsAvailableForm {

	/** パス/キー値 */
	private String pathOrKey;
}
