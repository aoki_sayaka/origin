package jp.or.jaot.model.form.search;

import jp.or.jaot.model.form.ApplicationProgressForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 申請進捗検索フォーム
 */
@Getter
@Setter
public class ApplicationProgressSearchForm extends ApplicationProgressForm {

	/** 受付日範囲(検索対象とする日数) */
	private Integer applyDateRangeDay;

	// TODO
}
