package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.QualificationOtAttendingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴フォーム
 * @see QualificationOtAttendingHistoryEntity
 */
@Getter
@Setter
public class QualificationOtAttendingHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendDate;

	private String category;

	private String professionalField;

	private String trainingType;

	private String trainingName;

	private Integer seq;

	private String remissionCauseCd;

	private String remissionQualificationCd;

	private String acceptanceCd;

	private String promoterCd;

	private String othersSigCd;

	private String content;

	private String pointTypeCd;

	private Integer days;

	private Integer point;

	private String facilityNo;

	private boolean usePoint;

	private String collationMemberName;

	private String collationRate;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startDate;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endDate;

	private boolean checkMarked;

	private String remarks;

	/** 会員関連資格情報 */
	private List<QualifiedOtCompletionHistoryForm> qualifiedOtCompletionHistories = new ArrayList<QualifiedOtCompletionHistoryForm>();

	/** 会員自治体参画情報 */
	private List<QualifiedOtTrainingHistoryForm> qualifiedOtTrainingHistories = new ArrayList<QualifiedOtTrainingHistoryForm>();

	/** 会員自治体参画情報 */
	private List<CasesReportForm> casesReports = new ArrayList<CasesReportForm>();

	/** 会員自治体参画情報 */
	private List<QualificationOtAppHistoryForm> qualificationOtAppHistories = new ArrayList<QualificationOtAppHistoryForm>();
}
