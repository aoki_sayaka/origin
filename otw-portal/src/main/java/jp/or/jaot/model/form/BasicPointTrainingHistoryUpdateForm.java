package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.BasicPointTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎ポイント研修履歴検索結果フォーム
 * @see BasicPointTrainingHistoryEntity
 */
@Getter
@Setter
public class BasicPointTrainingHistoryUpdateForm extends BaseFullForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 認定年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationDate;

	/** 認定期間満了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationPeriodDate;

	/** 更新回数 */
	private Integer renewalCount;

	/** 基礎ポイント研修履歴 */
	private List<BasicPointTrainingHistoryForm> basicPointTrainingHistories = new ArrayList<BasicPointTrainingHistoryForm>();
}
