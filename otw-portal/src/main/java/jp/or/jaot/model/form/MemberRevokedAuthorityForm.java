package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MemberRevokedAuthorityEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員権限フォーム
 * @see MemberRevokedAuthorityEntity
 */
@Getter
@Setter
public class MemberRevokedAuthorityForm extends BaseForm {

	/** ID */
	private Long id;

	/** コード識別ID */
	private String codeId;

	/** コード値 */
	private String code;

	/** パス/キー値 */
	private String pathOrKey;
}
