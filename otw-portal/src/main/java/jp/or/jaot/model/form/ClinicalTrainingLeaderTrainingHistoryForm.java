package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者研修受講履歴フォーム
 * @see ClinicalTrainingLeaderTrainingHistoryEntity
 */
@Getter
@Setter
public class ClinicalTrainingLeaderTrainingHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 研修会コード */
	private String trainingCd;

	/** 年度 */
	private String fiscalYear;

	/** 主催者コード */
	private String promoterCd;

	/** カテゴリーコード */
	private String categoryCd;

	/** 専門分野コード */
	private String professionalFieldCd;

	/** グループコード */
	private String groupCd;

	/** クラスコード */
	private String classCd;

	/** 枝番 */
	private String seqCd;

	/** 回数 */
	private Integer times;

	/** 研修名 */
	private String trainingName;

	/** 研修受講年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendDate;

	/** 作成日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date createDateTime;

	/** 作成者名 */
	private String createUser;

	/** 更新日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date updateDateTime;

	/** 更新者名 */
	private String updateUser;

	/** 削除フラグ */
	private Boolean deleted;

	/** バージョン */
	private Integer versionNo;

}
