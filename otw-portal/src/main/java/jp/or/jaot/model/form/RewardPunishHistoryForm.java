package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.RewardPunishHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 賞罰履歴フォーム
 * @see RewardPunishHistoryEntity
 */
@Getter
@Setter
public class RewardPunishHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 賞罰コード */
	private String rewardPunishCd;

	/** 賞罰分類 */
	private Integer rewardPunishType;

	/** 年度 */
	private Integer fiscalYear;

	/** 備考 */
	private String remarks;
}
