package jp.or.jaot.model.form.search;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 履歴テーブル定義検索フォーム
 */
@Getter
@Setter
public class HistoryTableDefinitionSearchForm implements Serializable {

	/** 会員名簿(true=含む, false=含まない) */
	private Boolean isMember = true;

	/** 受講履歴(true=含む, false=含まない) */
	private Boolean isAttend = true;
}
