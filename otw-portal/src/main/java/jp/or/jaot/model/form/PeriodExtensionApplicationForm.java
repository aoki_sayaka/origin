package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.PeriodExtensionApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 期間延長申請フォーム
 * @see PeriodExtensionApplicationEntity
 */
@Getter
@Setter
public class PeriodExtensionApplicationForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 申請資格 */
	private String qualificationCd;

	/** 期間延長理由 */
	private String applicationReasonCd;

	/** その他の理由 */
	private String applicationReasonOther;

	/** 状態結果 */
	private String applicationResultCd;

	/** 申請状態 */
	private String applicationStatusCd;

	/** 事務局確認日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date secretariatConfirmDate;

	/** 休止期間(自) */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date restPeriodFrom;

	/** 休止期間(至) */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date restPeriodTo;

	/** 証明書ファイル名 */
	private String certificateFileNm;

	/** 証明書ファイル */
	private byte[] certificateFile;
}
