package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士認定更新履歴フォーム
 * @see ProfessionalOtQualifyHistoryEntity
 */
@Getter
@Setter
public class ProfessionalOtQualifyHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 専門分野 */
	private String professionalField;

	/** 回数 */
	private Integer seq;

	/** 認定年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationDate;

	/** 認定番号 */
	private Integer qualificationNumber;

	/** 認定期間満了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationPeriodDate;
}
