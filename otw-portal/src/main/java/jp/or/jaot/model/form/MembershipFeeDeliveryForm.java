package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MembershipFeeDeliveryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会費納入フォーム
 * @see MembershipFeeDeliveryEntity
 */
@Getter
@Setter
public class MembershipFeeDeliveryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 年度 */
	private Integer fiscalYear;

	/** 会費区分コード */
	private String categoryCd;

	/** 請求額 */
	private Integer billingAmount;

	/** 入金額 */
	private Integer paymentAmount;

	/** 取込日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date importDate;

	/** 請求残額 */
	private Integer billingBalance;

	/** 備考 */
	private String remarks;

	/** 入金種別 */
	private String paymentType;

	/** データ種別 */
	private String dataType;

	/** 申請日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date applicationDate;

	/** 申請内容 */
	private String applicationContent;

	/** 申請状況 */
	private String applicationStatus;
}
