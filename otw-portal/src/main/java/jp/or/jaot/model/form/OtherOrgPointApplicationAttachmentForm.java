package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント添付ファイルフォーム
 * @see OtherOrgPointApplicationAttachmentEntity
 */
@Getter
@Setter

public class OtherOrgPointApplicationAttachmentForm extends BaseForm {
	
	/** ID */
	private Long id;

	/** 申請 */
	private Integer applicationId;

	/** 順番 */
	private Integer fileOrder;

	/** 受講証明書ファイル名 */
	private String fileNm;

	/** 受講証明書ファイル */
	private Integer file;
	
	/** 受講証明書不適切理由コード */
	private Integer fileRejectCd;
	
	/** 作成日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date createDateTime;

	/** 作成者名 */
	private String createUser;

	/** 更新日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date updateDateTime;

	/** 更新者名 */
	private String updateUser;

	/** 削除フラグ */
	private Boolean deleted;

	/** バージョン */
	private Integer versionNo;

	
}
