package jp.or.jaot.model.form;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.NoticeDestinationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(宛先)フォーム
 * @see NoticeDestinationEntity
 */
@Getter
@Setter
public class NoticeDestinationForm extends BaseForm {

	/** ID */
	private Long id;

	/** お知らせID */
	private Long noticeId;

	/** 会員番号 */
	private Integer memberNo;

	/** 既読フラグ */
	private Boolean readFlg;

	/** 開封日時 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp readTime;
}
