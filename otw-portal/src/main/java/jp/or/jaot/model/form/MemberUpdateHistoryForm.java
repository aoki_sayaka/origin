package jp.or.jaot.model.form;

import java.sql.Timestamp;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MemberUpdateHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員更新履歴フォーム
 * @see MemberUpdateHistoryEntity
 */
@Getter
@Setter
public class MemberUpdateHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 更新連番 */
	private Integer updateNo;

	/** 更新日時 */
	private Timestamp memberUpdateDatetime;

	/** 更新ユーザID */
	private String updateUserId;

	/** 会員番号 */
	private Integer memberNo;

	/** 県士会番号 */
	private Integer paotCd;

	/** 更新区分コード */
	private Integer updateCategoryCd;

	/** 更新日 */
	private Timestamp updateDate;

	/** テーブルコード */
	private Integer tableCd;

	/** テーブル名 */
	private String tableNm;

	/** 項目コード */
	private Integer fieldCd;

	/** 項目名 */
	private String fieldNm;

	/** 変更前 */
	private String contentBefore;

	/** 変更後 */
	private String contentAfter;
}
