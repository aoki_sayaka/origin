package jp.or.jaot.model.form;

import java.sql.Timestamp;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MemberPasswordHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員パスワード履歴フォーム
 * @see MemberPasswordHistoryEntity
 */
@Getter
@Setter
public class MemberPasswordHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** パスワード */
	private String password;

	/** 仮パスワード */
	private String tempPassword;
	
	/** 仮パスワードフラグ */
	private Boolean tempPasswordFlag;
	
	/** 有効期限 */
	private Timestamp tempPasswordExpirationDatetime;
}
