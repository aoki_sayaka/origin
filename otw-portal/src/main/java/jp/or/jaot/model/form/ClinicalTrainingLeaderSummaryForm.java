package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;
import jp.or.jaot.model.form.ClinicalTrainingLeaderSummaryUpdateForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者履歴フォーム
 * @see ClinicalTrainingLeaderSummaryEntity
 */
@Getter
@Setter
public class ClinicalTrainingLeaderSummaryForm extends ClinicalTrainingLeaderSummaryUpdateForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 臨床実習指導者履歴 */
	private Integer trainingQualificationNumber;

	/** 臨床実習指導者研修修了認定日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date trainingQualificationDate;

	/** 臨床実習指導者講習会受講認定日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date sessionAttendQualificationDate;

	/** 臨床実習指導者講習会受講認定番号 */
	private Integer sessionAttendQualificationNumber;

	/** 厚生労働省認定日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date ministryQualificationDate;

	/** 厚生労働省認定番号 */
	private Integer ministryQualificationNumber;

	/** 作成日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date createDateTime;

	/** 作成者名 */
	private String createUser;

	/** 更新日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date updateDateTime;

	/** 更新者名 */
	private String updateUser;

	/** 削除フラグ */
	private Boolean deleted;

	/** バージョン */
	private Integer versionNo;

}
