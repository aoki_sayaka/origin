package jp.or.jaot.model.form;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ件数フォーム(自己)
 */
@Getter
@Setter
public class NoticeMyselfCountForm implements Serializable {

	/** 重要なお知らせ件数(全体) */
	private Long importantCount;

	/** 重要なお知らせ件数(未読) */
	private Long importantUnreadCount;

	/** 重要なお知らせ件数(既読) */
	private Long importantAlreadyReadCount;

	/** 協会からのお知らせ件数(全体) */
	private Long associationCount;

	/** 協会からのお知らせ件数(未読) */
	private Long associationUnreadCount;

	/** 協会からのお知らせ件数(既読) */
	private Long associationAlreadyReadCount;
}
