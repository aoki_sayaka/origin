package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.FacilityCategoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設分類フォーム
 * @see FacilityCategoryEntity
 */
@Getter
@Setter
public class FacilityCategoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 施設養成校ID */
	private Long facilityId;

	/** 分類種別 */
	private Integer category;

	/** 分類区分 */
	private Integer categoryDivision;

	/** 親分類コード */
	private String parentCode;

	/** 分類コード */
	private String categoryCode;

	/** その他 */
	private String other;

	/** 削除フラグ */
	private Boolean deleted;
}
