package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 申請進捗リストフォーム
 */
@Getter
@Setter
public class ApplicationProgressListForm extends BaseListForm {

	/** 申請進捗 */
	List<ApplicationProgressForm> applicationProgresses = new ArrayList<ApplicationProgressForm>();
}
