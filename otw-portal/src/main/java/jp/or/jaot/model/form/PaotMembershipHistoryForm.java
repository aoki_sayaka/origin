package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.PaotMembershipHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 県士会在籍履歴フォーム
 * @see PaotMembershipHistoryEntity
 */
@Getter
@Setter
public class PaotMembershipHistoryForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 県士会番号 */
	private Integer paotCd;

	/** 在籍開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startEnrollDate;

	/** 在籍終了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endEnrollDate;

	/** 備考 */
	private String remarks;
}
