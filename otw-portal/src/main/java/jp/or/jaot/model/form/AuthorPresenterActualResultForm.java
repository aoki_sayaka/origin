package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.AuthorPresenterActualResultEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(発表・著作等)フォーム
 * @see AuthorPresenterActualResultEntity
 */
@Getter
@Setter
public class AuthorPresenterActualResultForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 登録者区分 */
	private String registerCategoryCd;

	/** 発表著作内容コード */
	private String activityContentCd;

	/** 活動詳細 */
	private String activityDetail;

	/** 著者/発表者区分コード */
	private String authorPresenterCategoryCd;

	/** 発行発表年月日 */
	private Integer publicationYm;

	/** 巻 */
	private String volume;

	/** 号 */
	private String issue;

	/** 掲載開始ページ */
	private Integer startPage;

	/** 掲載終了ページ */
	private Integer endPage;

	/** 発行者出版社 */
	private String publisher;

	/** 雑誌学術誌名 */
	private String journalName;

	/** 大会名 */
	private String conventionName;

	/** 開催回 */
	private Integer heldTimes;

	/** 開催地 */
	private String venue;

	/** 団体名 */
	private String groupName;

	/** 他学会発表抄録掲載開始ページ */
	private Integer otherStartPage;

	/** 他学会発表抄録掲載終了ページ */
	private Integer otherEndPage;

	/** ISBN/ISSN */
	private String isbnIssn;

	/** 発表区分コード */
	private String publishCategoryCd;

	/** 活動実績の領域(発表・著作等) */
	List<AuthorPresenterActualFieldForm> authorPresenterActualFields = new ArrayList<AuthorPresenterActualFieldForm>();
}
