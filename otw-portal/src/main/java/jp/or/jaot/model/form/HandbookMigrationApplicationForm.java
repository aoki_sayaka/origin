package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.HandbookMigrationApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 手帳移行申請フォーム
 * @see HandbookMigrationApplicationEntity
 */
@Getter
@Setter
public class HandbookMigrationApplicationForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 移行種別 */
	private String migrationTypeCd;

	/** 状態・結果 */
	private String statusCd;

	/** 作業療法士生涯教育概論 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date introductionDate;

	/** 作業療法士生涯教育概論 チェック */
	private Boolean introductionCheck;

	/**作業療法士における協業・後輩育成  受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date collaborationDate;

	/** 作業療法士における協業・後輩育成  チェック */
	private Boolean collaborationCheck;

	/**職業倫理 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date professionalEthicsDate;

	/** 職業倫理 チェック */
	private Boolean professionalEthicsCheck;

	/** 保険・医療・福祉と地域支援 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date healthMedicalCareDate;

	/**保険・医療・福祉と地域支援 チェック */
	private Boolean healthMedicalCareCheck;

	/**実践のための作業療法研究 受講日  */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date studyForPracticeDate;

	/**実践のための作業療法研究 チェック */
	private Boolean studyForPracticeCheck;

	/**作業療法の可能性 受講日  */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date potentialDate;

	/**作業療法の可能性 チェック */
	private Boolean potentialCheck;

	/**日本と世界の作業療法の動向 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date trendDate;

	/**日本と世界の作業療法の動向 チェック */
	private Boolean trendCheck;

	/**事例報告と事例研究 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date caseReportAndStudyDate;

	/**事例報告と事例研究 チェック */
	private Boolean caseReportAndStudyCheck;

	/**事例検討 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date caseConsiderDate;

	/**事例検討 チェック */
	private Boolean caseConsiderCheck;

	/**事例報告 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date caseReportDate;

	/**事例報告 チェック */
	private Boolean caseReportCheck;

	/**MTDLP基礎研修 受講日  */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date mtdlpBasicDate;

	/**MTDLP基礎研修 チェック  */
	private Boolean mtdlpBasicCheck;

	/**身体障碍領域 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date disabilityFieldDate;

	/**身体障碍領域 チェック */
	private Boolean disabilityFieldCheck;

	/**精神障害領域 受講日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date mentalDisorderFieldDate;

	/**精神障害領域 チェック */
	private Boolean mentalDisorderFieldCheck;

	/** 発達障害領域 受講日*/
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date developmentalDisorderFieldDate;

	/** 発達障害領域 チェック*/
	private Boolean developmentalDisorderFieldCheck;

	/** 老年期領域 受講日*/
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date oldAgeFieldDate;

	/** 老年期領域 チェック*/
	private Boolean oldAgeFieldCheck;

	/** 基礎研修受講記録（最終頁）ポイント*/
	private Integer basicTrainingPoint;

	/** 添付 受講記録3頁目ファイル名*/
	private String attachmentP3FileNm;

	/** 添付 受講記録3頁目ファイル */
	private byte[] attachmentP3File;

	/** 添付 受講記録4頁目ファイル名 */
	private String attachmentP4FileNm;

	/** 添付 受講記録4頁目ファイル */
	private byte[] attachmentP4File;

	/** 添付 受講記録5頁目ファイル名 */
	private String attachmentP5FileNm;

	/** 添付 受講記録5頁目ファイル */
	private byte[] attachmentP5File;

	/** 添付 基礎研修受講記録最終頁ファイル名 */
	private String attachmentLastPageFileNm;

	/** 添付 基礎研修受講記録最終頁ファイル */
	private byte[] attachmentLastPageFile;

	/** 再入会フラグ */
	private String reEnrollmentFlag;

	/** サンプルフラグ */
	private String samplingFlag;

	/** 削除フラグ */
	private Boolean deleted;

	/** 受講履歴 */
	private List<BasicOtTrainingHistoryForm> basicOtTrainingHistories = new ArrayList<BasicOtTrainingHistoryForm>();

	private Boolean b;
}
