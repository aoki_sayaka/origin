package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎ポイント研修履歴フォーム
 * @see BasicOtQualifyHistoryEntity
 */
@Getter
@Setter
public class BasicPointTrainingHistoryForm extends BasicPointTrainingHistoryUpdateForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 研修受講年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendDate;

	/** 内容 */
	private String content;

	/** ポイント種別 */
	private String pointType;

	/** 日数 */
	private Integer days;

	/** ポイント */
	private Integer Point;

	/** ポイント使用フラグ */
	private boolean usePoint;

	/** 主催者コード */
	private String promoterCd;

	/** 他団体・SIGコード */
	private String othersSigCd;

//	/**  施設番号*/
//	private String facilityNo;

//	/** 照会会員氏名 */
//	private String collationMemberName;

//	/** 照合率 */
//	private Integer collationRate;

//	/** 開始日 */
//	@JsonFormat(pattern = "yyyy-MM-dd")
//	private Date startDate;

//	/** 終了日 */
//	@JsonFormat(pattern = "yyyy-MM-dd")
//	private Date endDate;

//	/** 備考 */
//	private String remarks;

	/** 削除フラグ */
	private Boolean deleted;

	/** 生涯教育ステータス */
	private LifeLongEducationStatusesForm lifeLongEducationStatus = new LifeLongEducationStatusesForm();
}
