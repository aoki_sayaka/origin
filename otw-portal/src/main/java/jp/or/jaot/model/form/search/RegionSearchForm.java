package jp.or.jaot.model.form.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 領域検索フォーム
 */
@Getter
@Setter
public class RegionSearchForm implements Serializable {

	/** 分類種別(1=医療関連, 2=介護関連, 3=障害総合支援関連, 4=その他関連) */
	private Integer category;

	/** 該当(true=該当あり, false=該当なし) */
	private Boolean isApplicable;

	/** 分類 */
	private List<ClassificationSearchForm> classifications = new ArrayList<ClassificationSearchForm>();
}
