package jp.or.jaot.model.form.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 分類検索フォーム
 */
@Getter
@Setter
public class ClassificationSearchForm implements Serializable {

	/** 分類条件(1=OR(含む),2=NOT(含まない), 3=AND) */
	private Integer condition = 1; // 含む

	/** 分類区分(1=大分類, 2=中分類, 3=小分類) */
	private Integer division = 1; // 大分類

	/** 分類コード */
	private List<String> codes = new ArrayList<String>();
}
