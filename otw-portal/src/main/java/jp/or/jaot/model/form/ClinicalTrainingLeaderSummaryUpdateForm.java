package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;
import jp.or.jaot.model.form.ClinicalTrainingLeaderQualifyHistoryForm;
import jp.or.jaot.model.form.ClinicalTrainingLeaderTrainingHistoryForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者履歴検索結果フォーム
 * @see ClinicalTrainingLeaderSummaryEntity
 */
@Getter
@Setter
public class ClinicalTrainingLeaderSummaryUpdateForm extends BaseFullForm {
	private Long id;

	/** 会員関連資格情報 */
	private List<ClinicalTrainingLeaderQualifyHistoryForm> clinicalTrainingLeaderQualifyHistories = new ArrayList<ClinicalTrainingLeaderQualifyHistoryForm>();

	/** 会員自治体参画情報 */
	private List<ClinicalTrainingLeaderTrainingHistoryForm> clinicalTrainingLeaderTrainingHistories = new ArrayList<ClinicalTrainingLeaderTrainingHistoryForm>();
}
