package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.SocialContributionActualResultEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(後輩育成・社会貢献)フォーム
 * @see SocialContributionActualResultEntity
 */
@Getter
@Setter
public class SocialContributionActualResultForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 登録者区分 */
	private String registerCategoryCd;

	/** 審査等内容コード */
	private String activityContentCd;

	/** 開始年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startDate;

	/** 終了年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endDate;

	/** 種別 */
	private String type;

	/** 団体名 */
	private String groupName;

	/** 他団体詳細 */
	private String groupDetail;

	/** 研修会名 */
	private String trainingName;

	/** 開催回 */
	private Integer heldTimes;

	/** 開催地 */
	private String venue;

	/** 活動実績の領域(後輩育成・社会貢献) */
	List<SocialContributionActualFieldForm> socialContributionActualFields = new ArrayList<SocialContributionActualFieldForm>();

	/** 活動実績の領域2(後輩育成・社会貢献) */
	List<SocialContributionActual2FieldForm> socialContributionActual2Fields = new ArrayList<SocialContributionActual2FieldForm>();
}
