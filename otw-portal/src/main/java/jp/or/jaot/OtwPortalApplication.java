package jp.or.jaot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OtwPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(OtwPortalApplication.class, args);
	}

}
