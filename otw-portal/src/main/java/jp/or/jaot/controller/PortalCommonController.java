package jp.or.jaot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.GeneralCodeDto;
import jp.or.jaot.model.dto.search.GeneralCodeSearchDto;
import jp.or.jaot.model.dto.search.QualificationSearchDto;
import jp.or.jaot.model.dto.search.RegionRelatedLargeClassificationSearchDto;
import jp.or.jaot.model.dto.search.RegionRelatedMiddleClassificationSearchDto;
import jp.or.jaot.model.dto.search.RegionRelatedSmallClassificationSearchDto;
import jp.or.jaot.model.dto.search.ZipcodeSearchDto;
import jp.or.jaot.model.entity.GeneralCodeEntity;
import jp.or.jaot.model.entity.GeneralCodeId;
import jp.or.jaot.model.entity.HistoryFieldDefinitionEntity;
import jp.or.jaot.model.entity.RegionRelatedLargeClassificationEntity;
import jp.or.jaot.model.entity.RegionRelatedMiddleClassificationEntity;
import jp.or.jaot.model.entity.RegionRelatedSmallClassificationEntity;
import jp.or.jaot.model.form.GeneralCodeForm;
import jp.or.jaot.model.form.HistoryFieldDefinitionForm;
import jp.or.jaot.model.form.HistoryTableDefinitionForm;
import jp.or.jaot.model.form.QualificationForm;
import jp.or.jaot.model.form.RegionRelatedCompositeForm;
import jp.or.jaot.model.form.RegionRelatedForm;
import jp.or.jaot.model.form.ZipcodeForm;
import jp.or.jaot.model.form.ZipcodeListForm;
import jp.or.jaot.model.form.search.GeneralCodeSearchForm;
import jp.or.jaot.model.form.search.GeneralCodeSearchMultipleForm;
import jp.or.jaot.model.form.search.HistoryTableDefinitionSearchForm;
import jp.or.jaot.model.form.search.RegionRelatedSearchForm;
import jp.or.jaot.model.form.search.ZipcodeSearchListForm;
import jp.or.jaot.service.GeneralCodeService;
import jp.or.jaot.service.HistoryFieldDefinitionService;
import jp.or.jaot.service.HistoryTableDefinitionService;
import jp.or.jaot.service.QualificationService;
import jp.or.jaot.service.RegionRelatedLargeClassificationService;
import jp.or.jaot.service.RegionRelatedMiddleClassificationService;
import jp.or.jaot.service.RegionRelatedSmallClassificationService;
import jp.or.jaot.service.ZipcodeService;
import lombok.AllArgsConstructor;

/**
 * 共通コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "common", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PortalCommonController extends BaseController {

	/** 汎用コードサービス */
	private final GeneralCodeService generalCodeSrv;

	/** 関連資格サービス */
	private final QualificationService qualificationSrv;

	/** 履歴テーブル定義サービス */
	private final HistoryTableDefinitionService historyTableDefinitionSrv;

	/** 履歴フィールド定義サービス */
	private final HistoryFieldDefinitionService historyFieldDefinitionSrv;

	/** 郵便番号サービス */
	private final ZipcodeService zipcodeSrv;

	/** 領域関連大分類サービス */
	private final RegionRelatedLargeClassificationService regionRelatedLargeClassificationSrv;

	/** 領域関連中分類サービス */
	private final RegionRelatedMiddleClassificationService regionRelatedMiddleClassificationSrv;

	/** 領域関連小分類サービス */
	private final RegionRelatedSmallClassificationService regionRelatedSmallClassificationSrv;

	/**
	 * 条件検索(汎用コード)<br>
	 * [実装メモ]<br>
	 * 汎用コードエンティティのアノテーションで子要素リストを持たせた場合、複数回のDBアクセスが発生してしまうため、
	 * 対象となるエンティティを一括取得後に子要素のフォームリストを追加する。<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "search_general_code")
	public ResponseEntity<List<GeneralCodeForm>> searchGeneralCode(
			@RequestBody @Validated GeneralCodeSearchForm generalCodeForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		String id = generalCodeForm.getId();
		String code = generalCodeForm.getCode();
		GeneralCodeId[] generalCodeIds = { new GeneralCodeId(id, null) }; // コードID
		GeneralCodeSearchDto searchDto = new GeneralCodeSearchDto();
		searchDto.setGeneralCodeIds(generalCodeIds); // 汎用コードID配列

		// 検索結果取得(条件)
		GeneralCodeDto dto = generalCodeSrv.getSearchResultByCondition(searchDto);
		List<GeneralCodeForm> forms = new ArrayList<GeneralCodeForm>();
		for (GeneralCodeEntity entity : dto.getEntities()) {

			// コードID指定あり
			if (StringUtils.isNotEmpty(id) && !id.equals(entity.getGeneralCodeId().getId())) {
				continue;
			}

			// コード値指定あり
			if (StringUtils.isNotEmpty(code) && !code.equals(entity.getGeneralCodeId().getCode())) {
				continue;
			}

			// フォーム追加
			forms.add(createGeneralCodeForm(dto.getEntities(), entity));
		}

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(汎用コード(複数指定))<br>
	 * [実装メモ]<br>
	 * 汎用コードエンティティのアノテーションで子要素リストを持たせた場合、複数回のDBアクセスが発生してしまうため、
	 * 対象となるエンティティを一括取得後に子要素のフォームリストを追加する。<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "search_multiple_general_code")
	public ResponseEntity<List<GeneralCodeForm>> searchMultipleGeneralCode(
			@RequestBody @Validated GeneralCodeSearchMultipleForm generalCodeMultipleForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		GeneralCodeId[] generalCodeIds = generalCodeMultipleForm.getGeneralCodes().stream()
				.map(E -> new GeneralCodeId(E.getId(), null)).toArray(GeneralCodeId[]::new);
		GeneralCodeSearchDto searchDto = new GeneralCodeSearchDto();
		searchDto.setGeneralCodeIds(generalCodeIds); // 汎用コードID配列

		// 検索結果取得(条件)
		GeneralCodeDto dto = generalCodeSrv.getSearchResultByCondition(searchDto);
		List<GeneralCodeForm> forms = new ArrayList<GeneralCodeForm>();
		for (GeneralCodeEntity entity : dto.getEntities()) {
			for (GeneralCodeSearchForm form : generalCodeMultipleForm.getGeneralCodes()) {

				// コードID指定あり
				String id = form.getId();
				if (StringUtils.isNotEmpty(id) && !id.equals(entity.getGeneralCodeId().getId())) {
					continue;
				}

				// コード値指定あり
				String code = form.getCode();
				if (StringUtils.isNotEmpty(code) && !code.equals(entity.getGeneralCodeId().getCode())) {
					continue;
				}

				// フォーム追加
				forms.add(createGeneralCodeForm(dto.getEntities(), entity));
			}
		}

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(関連資格)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_qualification")
	public ResponseEntity<List<QualificationForm>> searchQualification(
			@RequestBody @Validated QualificationForm qualificationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		QualificationSearchDto searchDto = new QualificationSearchDto();
		searchDto.setCode(qualificationForm.getCode()); // コード値
		searchDto.setCategoryCd(qualificationForm.getCategoryCd()); // 区分コード値

		// 検索結果フォーム取得
		List<QualificationForm> forms = qualificationSrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, QualificationForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(履歴テーブル定義)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_history_table_definition")
	public ResponseEntity<List<HistoryTableDefinitionForm>> searchHistoryTableDefinition(
			@RequestBody @Validated HistoryTableDefinitionSearchForm historyTableDefinitionForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 履歴フィールド定義取得
		List<HistoryFieldDefinitionEntity> fieldEntities = historyFieldDefinitionSrv.getCacheResult();
		logger.info("履歴フィールド定義数 : {}", fieldEntities.size());

		// 検索結果フォーム取得
		List<HistoryTableDefinitionForm> forms = historyTableDefinitionSrv
				.getCacheResult(historyTableDefinitionForm.getIsMember(), historyTableDefinitionForm.getIsAttend())
				.stream().map(E -> BeanUtil.createCopyProperties(E, HistoryTableDefinitionForm.class, false))
				.collect(Collectors.toList());
		logger.info("履歴テーブル定義数 : {}", forms.size());

		// 履歴フィールド設定
		for (HistoryTableDefinitionForm form : forms) {
			form.setHistoryFieldDefinitions(fieldEntities.stream()
					.filter(E -> E.getTableDefinitionId().equals(form.getId()))
					.map(F -> BeanUtil.createCopyProperties(F, HistoryFieldDefinitionForm.class, false))
					.collect(Collectors.toList()));
		}

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(郵便番号リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_zipcode")
	public ResponseEntity<ZipcodeListForm> searchListZipcode(@RequestBody @Validated ZipcodeSearchListForm zipcodeForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ZipcodeSearchDto searchDto = new ZipcodeSearchDto();
		BeanUtil.copyProperties(zipcodeForm, searchDto, false);

		// 検索結果フォーム取得
		ZipcodeListForm form = new ZipcodeListForm();
		form.setZipcodes(zipcodeSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
				.map(E -> BeanUtil.createCopyProperties(E, ZipcodeForm.class, false)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 条件検索(領域関連分類)<br>
	 * [実装メモ]<br>
	 * 大中小の分類を親子関係を考慮した汎用コードフォームの形式ですべてまとめて取得する。<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "search_region_related_classification")
	public ResponseEntity<List<GeneralCodeForm>> searchRegionRelatedClassification(
			@RequestBody @Validated RegionRelatedSearchForm regionRelatedForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(条件)
		List<GeneralCodeForm> largeForms = new ArrayList<GeneralCodeForm>();
		for (RegionRelatedLargeClassificationEntity entity : regionRelatedLargeClassificationSrv
				.getSearchResultByCondition(createRegionRelatedLargeSearchDto(regionRelatedForm)).getEntities()) {
			GeneralCodeForm form = BeanUtil.createCopyProperties(entity, GeneralCodeForm.class, false);
			form.setId(entity.getCodeId()); // コード識別ID
			largeForms.add(form);
		}
		List<GeneralCodeForm> middleForms = new ArrayList<GeneralCodeForm>();
		for (RegionRelatedMiddleClassificationEntity entity : regionRelatedMiddleClassificationSrv
				.getSearchResultByCondition(createRegionRelatedMiddleSearchDto(regionRelatedForm)).getEntities()) {
			GeneralCodeForm form = BeanUtil.createCopyProperties(entity, GeneralCodeForm.class, false);
			form.setId(entity.getCodeId()); // コード識別ID
			middleForms.add(form);
		}
		List<GeneralCodeForm> smallForms = new ArrayList<GeneralCodeForm>();
		for (RegionRelatedSmallClassificationEntity entity : regionRelatedSmallClassificationSrv
				.getSearchResultByCondition(createRegionRelatedSmallSearchDto(regionRelatedForm)).getEntities()) {
			GeneralCodeForm form = BeanUtil.createCopyProperties(entity, GeneralCodeForm.class, false);
			form.setId(entity.getCodeId()); // コード識別ID
			smallForms.add(form);
		}

		// 子要素設定
		middleForms.stream()
				.forEach(V -> V.setChildren(smallForms.stream().filter(F -> V.getCode().equals(F.getParentCode()))
						.collect(Collectors.toList()))); // 中分類
		largeForms.stream()
				.forEach(V -> V.setChildren(middleForms.stream().filter(F -> V.getCode().equals(F.getParentCode()))
						.collect(Collectors.toList()))); // 大分類

		return ResponseEntity.ok(largeForms);
	}

	/**
	 * 条件検索(領域関連分類(複合))<br>
	 * [実装メモ]<br>
	 * 大中小の分類を親子関係を考慮しない領域関連分類フォームの形式ですべてまとめて取得する。<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "search_composite_region_related_classification")
	public ResponseEntity<RegionRelatedCompositeForm> searchCompositeRegionRelatedClassification(
			@RequestBody @Validated RegionRelatedSearchForm regionRelatedForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// フォーム設定
		RegionRelatedCompositeForm form = new RegionRelatedCompositeForm();
		form.setLarges(regionRelatedLargeClassificationSrv
				.getSearchResultByCondition(createRegionRelatedLargeSearchDto(regionRelatedForm)).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, RegionRelatedForm.class, false))
				.collect(Collectors.toList()));
		form.setMiddles(regionRelatedMiddleClassificationSrv
				.getSearchResultByCondition(createRegionRelatedMiddleSearchDto(regionRelatedForm)).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, RegionRelatedForm.class, false))
				.collect(Collectors.toList()));
		form.setSmalls(regionRelatedSmallClassificationSrv
				.getSearchResultByCondition(createRegionRelatedSmallSearchDto(regionRelatedForm)).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, RegionRelatedForm.class, false))
				.collect(Collectors.toList()));

		return ResponseEntity.ok(form);
	}

	/**
	 * 領域関連大分類検索DTO生成
	 * @param searcForm 領域関連分類検索フォーム
	 * @return 検索DTO
	 */
	private RegionRelatedLargeClassificationSearchDto createRegionRelatedLargeSearchDto(
			RegionRelatedSearchForm searcForm) {
		RegionRelatedLargeClassificationSearchDto searchDto = new RegionRelatedLargeClassificationSearchDto();
		searchDto.setCodeId(searcForm.getCodeId()); // コード識別ID
		searchDto.setCode(searcForm.getLargeCode()); // コード値(大分類)
		return searchDto;
	}

	/**
	 * 領域関連中分類検索DTO生成
	 * @param searchForm 領域関連分類検索フォーム
	 * @return 検索DTO
	 */
	private RegionRelatedMiddleClassificationSearchDto createRegionRelatedMiddleSearchDto(
			RegionRelatedSearchForm searchForm) {
		RegionRelatedMiddleClassificationSearchDto searchDto = new RegionRelatedMiddleClassificationSearchDto();
		searchDto.setCodeId(searchForm.getCodeId()); // コード識別ID
		searchDto.setCode(searchForm.getMiddleCode()); // コード値(中分類)
		return searchDto;
	}

	/**
	 * 領域関連小分類検索DTO生成
	 * @param searchForm 領域関連分類検索フォーム
	 * @return 検索DTO
	 */
	private RegionRelatedSmallClassificationSearchDto createRegionRelatedSmallSearchDto(
			RegionRelatedSearchForm searchForm) {
		RegionRelatedSmallClassificationSearchDto searchDto = new RegionRelatedSmallClassificationSearchDto();
		searchDto.setCodeId(searchForm.getCodeId()); // コード識別ID
		searchDto.setCode(searchForm.getSmallCode()); // コード値(小分類)
		return searchDto;
	}

	/**
	 * 汎用コードフォーム生成
	 * @param entities 汎用コードエンティティリスト
	 * @param entity 汎用コードエンティティ
	 * @return 汎用コードフォーム
	 */
	private GeneralCodeForm createGeneralCodeForm(List<GeneralCodeEntity> entities, GeneralCodeEntity entity) {

		// 属性
		GeneralCodeForm form = new GeneralCodeForm();
		BeanUtil.copyProperties(entity, form, false);
		form.setId(entity.getGeneralCodeId().getId()); // コードID
		form.setCode(entity.getGeneralCodeId().getCode()); // コード値

		// 子要素
		for (GeneralCodeEntity childEntity : entities) {
			if (entity.getGeneralCodeId().getCode().equals(childEntity.getParentCode())) {
				form.getChildren().add(createGeneralCodeForm(entities, childEntity));
			}
		}

		return form;
	}

	// TODO
}
