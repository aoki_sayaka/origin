package jp.or.jaot.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.MailContentDto;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.model.form.LoginForm;
import jp.or.jaot.service.OtcService;
import lombok.AllArgsConstructor;

/**
 * コントローラ
 * @version 疎通確認用
 */
@Controller
@RequestMapping("")
@AllArgsConstructor
public class OtwPortalController extends BaseController {

	/** サービス */
	private final OtcService otcSrv;

	/**
	 * 検索(正常)
	 * @return 実行結果
	 */
	@PostMapping(path = "search", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<LoginForm>> search(@RequestBody LoginForm requestForm, Principal principal) {
		logger.debug("Principal : {}, LoginForm : {}", principal, requestForm);

		// TODO : 各業務ロジックを実装
		otcSrv.getSearchResult();

		// TODO : JSONリストを返却
		List<LoginForm> forms = new ArrayList<LoginForm>();
		forms.add(new LoginForm("yamada", "yamada_pass"));
		forms.add(new LoginForm("tanaka", "tanaka_pass"));
		forms.add(new LoginForm("sasaki", "sasaki_pass"));

		return ResponseEntity.ok(forms);
	}

	/**
	 * 検索(業務例外)
	 * @exception SearchBusinessException
	 */
	@PostMapping(path = "search_error", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> searchError(@RequestBody LoginForm requestForm, Principal principal) {
		ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
		String message = String.format("検索条件に該当する利用者が存在しません");
		throw new SearchBusinessException(errorCause, ERROR_PLACE.LOGIC_MEMBER, "", message);
	}

	/**
	 * 検索(システム例外)
	 * @exception HibernateSystemException
	 */
	@PostMapping(path = "search_exception", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> searcException(@RequestBody LoginForm requestForm, Principal principal) {
		ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_NULL_POINTER;
		String message = String.format("更新時(楽観ロック)の排他制御に失敗しました");
		throw new HibernateSystemException(errorCause, message, new NullPointerException());
	}

	/**
	 * メール送信
	 */
	@PostMapping(path = "send_mail", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> sendMail(MailContentDto mailDto) {
		otcSrv.sendMail();
		return ResponseEntity.ok("sendMail ok");
	}
}
