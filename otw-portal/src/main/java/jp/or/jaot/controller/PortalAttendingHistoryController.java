package jp.or.jaot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.ListUtils;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.search.BasicOtAttendingSummarySearchDto;
import jp.or.jaot.model.dto.search.BasicPointTrainingHistorySearchDto;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderSummarySearchDto;
import jp.or.jaot.model.dto.search.MtdlpSummarySearchDto;
import jp.or.jaot.model.dto.search.ProfessionalOtAttendingSummarySearchDto;
import jp.or.jaot.model.dto.search.QualifiedOtAttendingSummarySearchDto;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.BasicPointTrainingHistoryEntity;
import jp.or.jaot.model.entity.CaseReportEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderQualifyHistoryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MtdlpCaseReportEntity;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;
import jp.or.jaot.model.entity.PeriodExtensionApplicationEntity;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.QualificationOtAppHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.composite.BasicOtAttendingSummaryCompositeEntity;
import jp.or.jaot.model.entity.composite.BasicPointTrainingHistoryCompositeEntity;
import jp.or.jaot.model.entity.composite.ClinicalTrainingLeaderSummaryCompositeEntity;
import jp.or.jaot.model.entity.composite.MtdlpCompositeEntity;
import jp.or.jaot.model.entity.composite.ProfessionalOtAttendingSummaryCompositeEntity;
import jp.or.jaot.model.entity.composite.QualifiedOtAttendingSummaryCompositeEntity;
import jp.or.jaot.model.form.BasicOtAttendingSummaryForm;
import jp.or.jaot.model.form.BasicOtQualifyHistoryForm;
import jp.or.jaot.model.form.BasicOtTrainingHistoryForm;
import jp.or.jaot.model.form.BasicPointTrainingHistoryForm;
import jp.or.jaot.model.form.CaseReportForm;
import jp.or.jaot.model.form.ClinicalTrainingLeaderQualifyHistoryForm;
import jp.or.jaot.model.form.ClinicalTrainingLeaderSummaryForm;
import jp.or.jaot.model.form.ClinicalTrainingLeaderTrainingHistoryForm;
import jp.or.jaot.model.form.LifeLongEducationStatusesForm;
import jp.or.jaot.model.form.MtdlpCaseReportForm;
import jp.or.jaot.model.form.MtdlpQualifyHistoryForm;
import jp.or.jaot.model.form.MtdlpSummaryForm;
import jp.or.jaot.model.form.MtdlpTrainingHistoryForm;
import jp.or.jaot.model.form.PeriodExtensionApplicationForm;
import jp.or.jaot.model.form.ProfessionalOtAttendingSummaryForm;
import jp.or.jaot.model.form.ProfessionalOtQualifyHistoryForm;
import jp.or.jaot.model.form.ProfessionalOtTestHistoryForm;
import jp.or.jaot.model.form.ProfessionalOtTrainingHistoryForm;
import jp.or.jaot.model.form.QualifiedOtAttendingSummaryForm;
import jp.or.jaot.model.form.QualifiedOtCompletionHistoryForm;
import jp.or.jaot.model.form.QualifiedOtTrainingHistoryForm;
import jp.or.jaot.service.BasicOtAttendingSummaryService;
import jp.or.jaot.service.BasicPointTrainingHistoryService;
import jp.or.jaot.service.ClinicalTrainingLeaderSummaryService;
import jp.or.jaot.service.MtdlpSummaryService;
import jp.or.jaot.service.ProfessionalOtAttendingSummaryService;
import jp.or.jaot.service.QualifiedOtAttendingSummaryService;
import lombok.AllArgsConstructor;

/**
 * ポータル受講履歴コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "attending_history", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PortalAttendingHistoryController extends BaseController {

	/** 基礎研修受講履歴サービス */
	private final BasicOtAttendingSummaryService basicOtAttendingSummarySrv;

	/** 基礎ポイント研修履歴サービス */
	private final BasicPointTrainingHistoryService basicPointTrainingHistorySrv;

	/** 認定作業療法士研修履歴サービス */
	private final QualifiedOtAttendingSummaryService qualifiedOtAttendingSummarySrv;

	/** 専門作業療法士受講履歴サービス */
	private final ProfessionalOtAttendingSummaryService professionalOtAttendingSummarySrv;

	/** 臨床実習指導者履歴サービス */
	private final ClinicalTrainingLeaderSummaryService clinicalTrainingLeaderSummarySrv;

	/** MTDLP履歴サービス */
	private final MtdlpSummaryService mtdlpSummarySrv;

	/**
	 * ログイン者情報(基礎研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_myself_basic_ot_attending_summary")
	public ResponseEntity<List<BasicOtAttendingSummaryForm>> searchMyselfBasicOtAttendingSummary(
			@RequestBody @Validated BasicOtAttendingSummaryForm basicOtAttendingSummaryForm,
			Errors errors, @AuthenticationPrincipal MemberEntity memberEntity) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		BasicOtAttendingSummarySearchDto searchDto = new BasicOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(basicOtAttendingSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<BasicOtAttendingSummaryForm> forms = basicOtAttendingSummarySrv.getSearchResultByCondition(searchDto)
				.getEntities().stream()
				.map(E -> createBasicOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(基礎研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_basic_ot_attending_summary")
	public ResponseEntity<List<BasicOtAttendingSummaryForm>> searchBasicOtAttendingSummary(
			@RequestBody @Validated BasicOtAttendingSummaryForm basicOtAttendingSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		BasicOtAttendingSummarySearchDto searchDto = new BasicOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(basicOtAttendingSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		//		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<BasicOtAttendingSummaryForm> forms = basicOtAttendingSummarySrv.getSearchResultByCondition(searchDto)
				.getEntities().stream()
				.map(E -> createBasicOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 基礎研修履歴フォーム生成
	 * @param compositeEntity 基礎研修履歴エンティティ(複合)
	 * @return 基礎研修履歴フォーム
	 */
	private BasicOtAttendingSummaryForm createBasicOtAttendingSummaryForm(
			BasicOtAttendingSummaryCompositeEntity compositeEntity) {

		// 基礎研修履歴フォーム
		BasicOtAttendingSummaryForm basicOtAttendingSummaryForm = BeanUtil.createCopyProperties(
				compositeEntity.getBasicOtAttendingSummaryEntity(), BasicOtAttendingSummaryForm.class,
				false);

		// 基礎研修修了認定更新履歴
		List<BasicOtQualifyHistoryEntity> basicOtQualifyHistoryEntities = compositeEntity
				.getBasicOtQualifyHistoryEntities();
		if (!ListUtils.isEmpty(basicOtQualifyHistoryEntities)) {
			basicOtAttendingSummaryForm.setBasicOtQualifyHistories(basicOtQualifyHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, BasicOtQualifyHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// 基礎研修受講履歴
		List<BasicOtTrainingHistoryEntity> basicOtTrainingHistoryEntities = compositeEntity
				.getBasicOtTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(basicOtTrainingHistoryEntities)) {
			basicOtAttendingSummaryForm.setBasicOtTrainingHistories(basicOtTrainingHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, BasicOtTrainingHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// 期間延長申請
		List<PeriodExtensionApplicationEntity> periodExtensionApplicationEntities = compositeEntity
				.getPeriodExtensionApplicationEntities();
		if (!CollectionUtils.isEmpty(periodExtensionApplicationEntities)) {
			basicOtAttendingSummaryForm.setPeriodExtensionApplication(periodExtensionApplicationEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, PeriodExtensionApplicationForm.class, false))
					.collect(Collectors.toList()));
		}

		return basicOtAttendingSummaryForm;
	}

	/**
	 * 条件検索(基礎ポイント研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_basic_point_training_history")
	public ResponseEntity<List<BasicPointTrainingHistoryForm>> searchBasicPointTrainingHistory(
			@RequestBody @Validated BasicPointTrainingHistoryForm basicPointTrainingHistoryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		BasicPointTrainingHistorySearchDto searchDto = new BasicPointTrainingHistorySearchDto();
		BeanUtil.copyProperties(basicPointTrainingHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		//		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<BasicPointTrainingHistoryForm> forms = basicPointTrainingHistorySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> createBasicPointTrainingHistoryForm(E))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * ログイン者情報(基礎ポイント研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_myself_basic_point_training_history")
	public ResponseEntity<List<BasicPointTrainingHistoryForm>> searchMyselfBasicPointTrainingHistory(
			@RequestBody @Validated BasicPointTrainingHistoryForm basicPointTrainingHistoryForm,
			Errors errors, @AuthenticationPrincipal MemberEntity memberEntity) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		BasicPointTrainingHistorySearchDto searchDto = new BasicPointTrainingHistorySearchDto();
		BeanUtil.copyProperties(basicPointTrainingHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<BasicPointTrainingHistoryForm> forms = basicPointTrainingHistorySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> createBasicPointTrainingHistoryForm(E))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 基礎ポイント研修履歴フォーム生成
	 * @param compositeEntity 基礎研修履歴エンティティ(複合)
	 * @return 基礎研修履歴フォーム
	 */
	private BasicPointTrainingHistoryForm createBasicPointTrainingHistoryForm(
			BasicPointTrainingHistoryCompositeEntity compositeEntity) {
		// 基礎ポイント研修履歴フォーム
		BasicPointTrainingHistoryForm basicPointTrainingHistoryForm = BeanUtil.createCopyProperties(
				compositeEntity.getBasicPointTrainingHistoryEntities(), BasicPointTrainingHistoryForm.class,
				false);

		// 基礎ポイント研修履歴
		List<BasicPointTrainingHistoryEntity> basicPointTrainingHistoryEntities = compositeEntity
				.getBasicPointTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(basicPointTrainingHistoryEntities)) {
			basicPointTrainingHistoryForm.setBasicPointTrainingHistories(basicPointTrainingHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, BasicPointTrainingHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		//生涯教育ステータス
		basicPointTrainingHistoryForm.setLifeLongEducationStatus(BeanUtil.createCopyProperties(
				compositeEntity.getLifelongEducationStatusesEntity(), LifeLongEducationStatusesForm.class, false));

		return basicPointTrainingHistoryForm;
	}

	/**
	 * ログイン者情報(認定作業療法士研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_myself_qualified_ot_attending_summary")
	public ResponseEntity<List<QualifiedOtAttendingSummaryForm>> searchMyselfQualifiedOtAttendingSummary(
			@RequestBody @Validated QualifiedOtAttendingSummaryForm qualifiedOtAttendingSummaryForm,
			Errors errors, @AuthenticationPrincipal MemberEntity memberEntity) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		QualifiedOtAttendingSummarySearchDto searchDto = new QualifiedOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(qualifiedOtAttendingSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<QualifiedOtAttendingSummaryForm> forms = qualifiedOtAttendingSummarySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createQualifiedOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(認定作業療法士研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_qualified_ot_attending_summary")
	public ResponseEntity<List<QualifiedOtAttendingSummaryForm>> searchQualifiedOtAttendingSummary(
			@RequestBody @Validated QualifiedOtAttendingSummaryForm qualifiedOtAttendingSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		QualifiedOtAttendingSummarySearchDto searchDto = new QualifiedOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(qualifiedOtAttendingSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		//		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<QualifiedOtAttendingSummaryForm> forms = qualifiedOtAttendingSummarySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createQualifiedOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 認定作業療法士研修履歴フォーム生成
	 * @param compositeEntity 基礎研修履歴エンティティ(複合)
	 * @return 基礎研修履歴フォーム
	 */
	private QualifiedOtAttendingSummaryForm createQualifiedOtAttendingSummaryForm(
			QualifiedOtAttendingSummaryCompositeEntity compositeEntity) {

		// 認定作業療法士研修履歴フォーム
		QualifiedOtAttendingSummaryForm qualifiedOtAttendingSummaryForm = BeanUtil.createCopyProperties(
				compositeEntity.getQualifiedOtAttendingSummaryEntity(), QualifiedOtAttendingSummaryForm.class,
				false);

		// 認定作業療法士研修修了認定更新履歴
		List<QualifiedOtCompletionHistoryEntity> qualifiedOtCompletionHistoryEntities = compositeEntity
				.getQualifiedOtCompletionHistoryEntities();
		if (!ListUtils.isEmpty(qualifiedOtCompletionHistoryEntities)) {
			qualifiedOtAttendingSummaryForm
					.setQualifiedOtCompletionHistories(qualifiedOtCompletionHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, QualifiedOtCompletionHistoryForm.class, false))
							.collect(Collectors.toList()));
		}

		// 認定作業療法士研修受講履歴
		List<QualifiedOtTrainingHistoryEntity> qualifiedOtTrainingHistoryEntities = compositeEntity
				.getQualifiedOtTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(qualifiedOtTrainingHistoryEntities)) {
			qualifiedOtAttendingSummaryForm.setQualifiedOtTrainingHistories(qualifiedOtTrainingHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, QualifiedOtTrainingHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// 事例報告
		List<CaseReportEntity> caseReportEntities = compositeEntity
				.getCaseReportEntities();
		if (!CollectionUtils.isEmpty(caseReportEntities)) {
			qualifiedOtAttendingSummaryForm.setCaseReports(caseReportEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, CaseReportForm.class, false))
					.collect(Collectors.toList()));
		}

		// 期間延長申請
		List<PeriodExtensionApplicationEntity> periodExtensionApplicationEntities = compositeEntity
				.getPeriodExtensionApplicationEntities();
		if (!CollectionUtils.isEmpty(periodExtensionApplicationEntities)) {
			qualifiedOtAttendingSummaryForm.setPeriodExtensionApplication(periodExtensionApplicationEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, PeriodExtensionApplicationForm.class, false))
					.collect(Collectors.toList()));
		}

		// 認定作業療法士申請履歴
		List<QualificationOtAppHistoryEntity> qualificationOtAppHistoryEntities = compositeEntity
				.getQualificationOtAppHistoryEntities();
		if (!CollectionUtils.isEmpty(qualificationOtAppHistoryEntities)) {
			qualifiedOtAttendingSummaryForm.setPeriodExtensionApplication(qualificationOtAppHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, PeriodExtensionApplicationForm.class, false))
					.collect(Collectors.toList()));
		}

		return qualifiedOtAttendingSummaryForm;
	}

	/**
	 * ログイン者情報(専門作業療法士受講履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_myself_professional_ot_attending_summary")
	public ResponseEntity<List<ProfessionalOtAttendingSummaryForm>> searchMyselfProfessionalOtAttendingSummary(
			@RequestBody @Validated ProfessionalOtAttendingSummaryForm professionalOtAttendingSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ProfessionalOtAttendingSummarySearchDto searchDto = new ProfessionalOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(professionalOtAttendingSummaryForm, searchDto, false); // 属性値
		/*		searchDto.setStartPosition(0); // 開始位置
				searchDto.setMaxResult(0); // 取得件数(全件)
				searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号
		*/
		/*		List<SearchOrder> order = new ArrayList<SearchOrder>();
				//		order.add(new SearchOrder(ORDER_ASC.DESC, "seq"));
				order.add(new SearchOrder(ORDER_ASC.ASC, "professionalField"));
				searchDto.setOrders(order); //順序
		*/
		// 検索結果フォーム取得
		List<ProfessionalOtAttendingSummaryForm> forms = professionalOtAttendingSummarySrv
				.getSearchMyselfResultByCondition(searchDto).getEntities().stream()
				.map(E -> createProfessionalOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(専門作業療法士受講履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_professional_ot_attending_summary")
	public ResponseEntity<List<ProfessionalOtAttendingSummaryForm>> searchProfessionalOtAttendingSummary(
			@RequestBody @Validated ProfessionalOtAttendingSummaryForm professionalOtAttendingSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ProfessionalOtAttendingSummarySearchDto searchDto = new ProfessionalOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(professionalOtAttendingSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		//		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<ProfessionalOtAttendingSummaryForm> forms = professionalOtAttendingSummarySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createProfessionalOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 専門作業療法士受講履歴フォーム生成
	 * @param compositeEntity 専門作業療法士受講履歴エンティティ(複合)
	 * @return 専門作業療法士受講履歴フォーム
	 */
	private ProfessionalOtAttendingSummaryForm createProfessionalOtAttendingSummaryForm(
			ProfessionalOtAttendingSummaryCompositeEntity compositeEntity) {

		// 専門作業療法士受講履歴フォーム
		ProfessionalOtAttendingSummaryForm professionalOtAttendingSummaryForm = BeanUtil.createCopyProperties(
				compositeEntity.getProfessionalOtAttendingSummaryEntity(), ProfessionalOtAttendingSummaryForm.class,
				false);

		// 専門作業療法士認定更新履歴
		List<ProfessionalOtQualifyHistoryEntity> professionalOtQualifyHistoryEntities = compositeEntity
				.getProfessionalOtQualifyHistoryEntities();
		if (!ListUtils.isEmpty(professionalOtQualifyHistoryEntities)) {
			professionalOtAttendingSummaryForm
					.setProfessionalOtQualifyHistories(professionalOtQualifyHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, ProfessionalOtQualifyHistoryForm.class, false))
							.collect(Collectors.toList()));
		}

		// 専門作業療法士試験結果履歴
		List<ProfessionalOtTestHistoryEntity> professionalOtTestHistoryEntities = compositeEntity
				.getProfessionalOtTestHistoryEntities();
		if (!CollectionUtils.isEmpty(professionalOtTestHistoryEntities)) {
			professionalOtAttendingSummaryForm.setProfessionalOtTestHistories(professionalOtTestHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, ProfessionalOtTestHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// 専門作業療法士研修受講履歴
		List<ProfessionalOtTrainingHistoryEntity> professionalOtTrainingHistoryEntities = compositeEntity
				.getProfessionalOtTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(professionalOtTrainingHistoryEntities)) {
			professionalOtAttendingSummaryForm
					.setProfessionalOtTrainingHistories(professionalOtTrainingHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, ProfessionalOtTrainingHistoryForm.class, false))
							.collect(Collectors.toList()));
		}

		// 専門作業療法士 認定番号、認定年月日、認定満了日用
		/*		if (!ListUtils.isEmpty(professionalOtQualifyHistoryEntities)) {
					List<ProfessionalOtQualifyHistoryForm> professionalOtQualifyHistoriesMaxSeqList = new ArrayList<ProfessionalOtQualifyHistoryForm>();
					//キーブレイク処理　professional_fieldごとに最大のseqを取得
					String beforProfessionalField = "";
					for (ProfessionalOtQualifyHistoryEntity professionalOtQualifyHistoryEntity : professionalOtQualifyHistoryEntities) {
						String afterProfessionalField = professionalOtQualifyHistoryEntity.getProfessionalField();
						if (beforProfessionalField.equals(afterProfessionalField)) {
							continue;
						}
						beforProfessionalField = afterProfessionalField;
						professionalOtQualifyHistoriesMaxSeqList.add(BeanUtil.createCopyProperties(
								professionalOtQualifyHistoryEntity, ProfessionalOtQualifyHistoryForm.class, false));
					}
					professionalOtAttendingSummaryForm
							.setProfessionalOtQualifyHistoriesMaxSeq(professionalOtQualifyHistoriesMaxSeqList);
				}
		*/
		// 専門作業療法士 最大SEQ 認定番号、認定年月日、認定満了日用キーブレイク処理
		if (!ListUtils.isEmpty(professionalOtQualifyHistoryEntities)) {
			List<ProfessionalOtQualifyHistoryForm> professionalOtQualifyHistoriesMaxSeqList = new ArrayList<ProfessionalOtQualifyHistoryForm>();

			ProfessionalOtQualifyHistoryEntity beforeProfessionalOtQualifyHistoryEntity = new ProfessionalOtQualifyHistoryEntity();
			ProfessionalOtQualifyHistoryEntity keepProfessionalOtQualifyHistoryEntity = new ProfessionalOtQualifyHistoryEntity();
			int count = 0;
			for (ProfessionalOtQualifyHistoryEntity professionalOtQualifyHistoryEntity : professionalOtQualifyHistoryEntities) {

				if (count == 0) {
					beforeProfessionalOtQualifyHistoryEntity = professionalOtQualifyHistoryEntity;
				}

				count = count + 1;

				keepProfessionalOtQualifyHistoryEntity = professionalOtQualifyHistoryEntity;

				if (professionalOtQualifyHistoryEntities.size() > count) {
					if (!beforeProfessionalOtQualifyHistoryEntity.getProfessionalField()
							.equals(professionalOtQualifyHistoryEntity.getProfessionalField())) {
						professionalOtQualifyHistoriesMaxSeqList.add(BeanUtil.createCopyProperties(
								beforeProfessionalOtQualifyHistoryEntity, ProfessionalOtQualifyHistoryForm.class,
								false));
					}
				} else if (professionalOtQualifyHistoryEntities.size() == count) { //ラストレコード
					if (!beforeProfessionalOtQualifyHistoryEntity.getProfessionalField()
							.equals(professionalOtQualifyHistoryEntity.getProfessionalField())) {
						professionalOtQualifyHistoriesMaxSeqList.add(BeanUtil.createCopyProperties(
								beforeProfessionalOtQualifyHistoryEntity, ProfessionalOtQualifyHistoryForm.class,
								false));
						professionalOtQualifyHistoriesMaxSeqList.add(BeanUtil.createCopyProperties(
								professionalOtQualifyHistoryEntity, ProfessionalOtQualifyHistoryForm.class, false));
					} else {
						professionalOtQualifyHistoriesMaxSeqList.add(BeanUtil.createCopyProperties(
								professionalOtQualifyHistoryEntity, ProfessionalOtQualifyHistoryForm.class, false));
					}

				}
				beforeProfessionalOtQualifyHistoryEntity = keepProfessionalOtQualifyHistoryEntity;
			}
			professionalOtAttendingSummaryForm
					.setProfessionalOtQualifyHistoriesMaxSeq(professionalOtQualifyHistoriesMaxSeqList);
		}

		return professionalOtAttendingSummaryForm;
	}

	/**
	 * ログイン者情報(臨床実習指導者履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_myself_clinical_training_leader_summary")
	public ResponseEntity<List<ClinicalTrainingLeaderSummaryForm>> searchMyselfClinicalTrainingLeaderSummary(
			@RequestBody @Validated ClinicalTrainingLeaderSummaryForm clinicalTrainingLeaderSummaryForm,
			Errors errors, @AuthenticationPrincipal MemberEntity memberEntity) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ClinicalTrainingLeaderSummarySearchDto searchDto = new ClinicalTrainingLeaderSummarySearchDto();
		BeanUtil.copyProperties(clinicalTrainingLeaderSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<ClinicalTrainingLeaderSummaryForm> forms = clinicalTrainingLeaderSummarySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createClinicalTrainingLeaderSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(臨床実習指導者履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_clinical_training_leader_summary")
	public ResponseEntity<List<ClinicalTrainingLeaderSummaryForm>> searchClinicalTrainingLeaderSummary(
			@RequestBody @Validated ClinicalTrainingLeaderSummaryForm clinicalTrainingLeaderSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ClinicalTrainingLeaderSummarySearchDto searchDto = new ClinicalTrainingLeaderSummarySearchDto();
		BeanUtil.copyProperties(clinicalTrainingLeaderSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		//		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<ClinicalTrainingLeaderSummaryForm> forms = clinicalTrainingLeaderSummarySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createClinicalTrainingLeaderSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 臨床作業療法士受講履歴フォーム生成
	 * @param compositeEntity 専門作業療法士受講履歴エンティティ(複合)
	 * @return 臨床作業療法士受講履歴フォーム
	 */
	private ClinicalTrainingLeaderSummaryForm createClinicalTrainingLeaderSummaryForm(
			ClinicalTrainingLeaderSummaryCompositeEntity compositeEntity) {

		// 臨床作業療法士受講履歴フォーム
		ClinicalTrainingLeaderSummaryForm clinicalTrainingLeaderSummaryForm = BeanUtil.createCopyProperties(
				compositeEntity.getClinicalTrainingLeaderSummaryEntity(), ClinicalTrainingLeaderSummaryForm.class,
				false);

		// 臨床作業療法士認定更新履歴
		List<ClinicalTrainingLeaderQualifyHistoryEntity> clinicalTrainingLeaderQualifyHistoryEntities = compositeEntity
				.getClinicalTrainingLeaderQualifyHistoryEntities();
		if (!ListUtils.isEmpty(clinicalTrainingLeaderQualifyHistoryEntities)) {
			clinicalTrainingLeaderSummaryForm
					.setClinicalTrainingLeaderQualifyHistories(clinicalTrainingLeaderQualifyHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, ClinicalTrainingLeaderQualifyHistoryForm.class,
									false))
							.collect(Collectors.toList()));
		}

		// 臨床作業療法士試験結果履歴
		List<ClinicalTrainingLeaderTrainingHistoryEntity> clinicalTrainingLeaderTrainingHistoryEntities = compositeEntity
				.getClinicalTrainingLeaderTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(clinicalTrainingLeaderTrainingHistoryEntities)) {
			clinicalTrainingLeaderSummaryForm
					.setClinicalTrainingLeaderTrainingHistories(clinicalTrainingLeaderTrainingHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, ClinicalTrainingLeaderTrainingHistoryForm.class,
									false))
							.collect(Collectors.toList()));
		}

		return clinicalTrainingLeaderSummaryForm;
	}

	/**
	 * ログイン者情報(MTDLP履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_myself_mtdlp_summary")
	public ResponseEntity<List<MtdlpSummaryForm>> searchMyselfMtdlpSummary(
			@RequestBody @Validated MtdlpSummaryForm mtdlpSummaryForm,
			Errors errors, @AuthenticationPrincipal MemberEntity memberEntity) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MtdlpSummarySearchDto searchDto = new MtdlpSummarySearchDto();
		BeanUtil.copyProperties(mtdlpSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<MtdlpSummaryForm> forms = mtdlpSummarySrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createMtdlpSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(MTDLP履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_mtdlp_summary")
	public ResponseEntity<List<MtdlpSummaryForm>> searchMtdlpSummary(
			@RequestBody @Validated MtdlpSummaryForm mtdlpSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MtdlpSummarySearchDto searchDto = new MtdlpSummarySearchDto();
		BeanUtil.copyProperties(mtdlpSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)
		//		searchDto.setMemberNo(Integer.parseInt(memberEntity.getUsername())); // 会員番号

		// 検索結果フォーム取得
		List<MtdlpSummaryForm> forms = mtdlpSummarySrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createMtdlpSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * MTDLP履歴フォーム生成
	 * @param compositeEntity MTDLP履歴エンティティ(複合)
	 * @return MTDLP履歴フォーム
	 */
	private MtdlpSummaryForm createMtdlpSummaryForm(MtdlpCompositeEntity compositeEntity) {

		// MTDLP履歴フォーム
		MtdlpSummaryForm mtdlpSummaryForm = BeanUtil.createCopyProperties(compositeEntity.getMtdlpSummaryEntity(),
				MtdlpSummaryForm.class,
				false);

		// MTDLP認定履歴
		List<MtdlpQualifyHistoryEntity> mtdlpQualifyHistoryEntities = compositeEntity.getMtdlpQualifyHistoryEntities();
		if (!ListUtils.isEmpty(mtdlpQualifyHistoryEntities)) {
			mtdlpSummaryForm.setMtdlpQualifyHistories(mtdlpQualifyHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MtdlpQualifyHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// MTDLP研修受講履歴
		List<MtdlpTrainingHistoryEntity> mtdlpTrainingHistoryEntities = compositeEntity
				.getMtdlpTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(mtdlpTrainingHistoryEntities)) {
			mtdlpSummaryForm.setMtdlpTrainingHistories(mtdlpTrainingHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MtdlpTrainingHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// MTDLP事例
		//		List<MtdlpCaseEntity> mtdlpCaseEntities = compositeEntity.getMtdlpCaseEntities();
		//		if (!CollectionUtils.isEmpty(mtdlpCaseEntities)) {
		//			mtdlpSummaryForm.setMtdlpCases(mtdlpCaseEntities.stream()
		//					.map(E -> BeanUtil.createCopyProperties(E, MtdlpCaseForm.class, false))
		//					.collect(Collectors.toList()));
		//		}

		// MTDLP事例報告
		List<MtdlpCaseReportEntity> mtdlpCaseReportEntities = compositeEntity.getMtdlpCaseReportEntities();
		if (!CollectionUtils.isEmpty(mtdlpCaseReportEntities)) {
			mtdlpSummaryForm.setMtdlpCaseReports(mtdlpCaseReportEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MtdlpCaseReportForm.class, false))
					.collect(Collectors.toList()));
		}

		return mtdlpSummaryForm;
	}
}
