package jp.or.jaot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.model.BaseSearchDto.CATEGORY_DIVISION;
import jp.or.jaot.core.model.BaseSearchDto.CLASS_CONDITION;
import jp.or.jaot.core.model.BaseSearchDto.RANGE_CONDITION;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.FacilityApplicationCategoryDto;
import jp.or.jaot.model.dto.FacilityApplicationDto;
import jp.or.jaot.model.dto.FacilityCategoryDto;
import jp.or.jaot.model.dto.FacilityDto;
import jp.or.jaot.model.dto.FacilityMemberDto;
import jp.or.jaot.model.dto.common.ClassificationSearchDto;
import jp.or.jaot.model.dto.common.RegionSearchDto;
import jp.or.jaot.model.dto.search.FacilityApplicationCategorySearchDto;
import jp.or.jaot.model.dto.search.FacilityApplicationSearchDto;
import jp.or.jaot.model.dto.search.FacilityCategorySearchDto;
import jp.or.jaot.model.dto.search.FacilityMemberSearchDto;
import jp.or.jaot.model.dto.search.FacilityPortalSearchDto;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;
import jp.or.jaot.model.entity.FacilityApplicationEntity;
import jp.or.jaot.model.entity.FacilityCategoryEntity;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.composite.FacilityApplicationCompositeEntity;
import jp.or.jaot.model.entity.composite.FacilityCompositeEntity;
import jp.or.jaot.model.form.FacilityApplicationCategoryForm;
import jp.or.jaot.model.form.FacilityApplicationForm;
import jp.or.jaot.model.form.FacilityApplicationListForm;
import jp.or.jaot.model.form.FacilityCategoryForm;
import jp.or.jaot.model.form.FacilityForm;
import jp.or.jaot.model.form.FacilityListForm;
import jp.or.jaot.model.form.FacilityMemberForm;
import jp.or.jaot.model.form.search.ClassificationSearchForm;
import jp.or.jaot.model.form.search.FacilityApplicationSearchForm;
import jp.or.jaot.model.form.search.FacilityApplicationSearchListForm;
import jp.or.jaot.model.form.search.FacilityPortalSearchForm;
import jp.or.jaot.model.form.search.FacilityPortalSearchListForm;
import jp.or.jaot.model.form.search.RegionSearchForm;
import jp.or.jaot.service.FacilityApplicationCategoryService;
import jp.or.jaot.service.FacilityApplicationService;
import jp.or.jaot.service.FacilityCategoryService;
import jp.or.jaot.service.FacilityMemberService;
import jp.or.jaot.service.FacilityService;
import lombok.AllArgsConstructor;

/**
 * 施設養成校コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "facility", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PortalFacilityController extends BaseController {

	/** 施設登録申請サービス */
	private final FacilityApplicationService facilityApplicationSrv;

	/** 施設登録申請分類サービス */
	private final FacilityApplicationCategoryService facilityApplicationCategorySrv;

	/** 施設養成校サービス */
	private final FacilityService facilitySrv;

	/** 施設分類サービス */
	private final FacilityCategoryService facilityCategorySrv;

	/** 施設養成校勤務者サービス */
	private final FacilityMemberService facilityMemberSrv;

	/**
	 * 条件検索(施設登録申請)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_facility_application")
	public ResponseEntity<List<FacilityApplicationForm>> searchFacilityApplication(
			@RequestBody @Validated FacilityApplicationSearchForm facilityApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		FacilityApplicationSearchDto searchDto = createFacilityApplicationSearchDto(facilityApplicationForm);

		// 検索結果フォーム取得
		List<FacilityApplicationForm> forms = facilityApplicationSrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createFacilityApplicationForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(施設養成校リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_facility_application")
	public ResponseEntity<FacilityApplicationListForm> searchListFacilityApplication(
			@RequestBody @Validated FacilityApplicationSearchListForm facilityApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		FacilityApplicationSearchDto searchDto = createFacilityApplicationSearchDto(facilityApplicationForm);

		// 検索結果フォーム取得
		FacilityApplicationListForm form = new FacilityApplicationListForm();
		form.setFacilityApplications(facilityApplicationSrv.getSearchResultListByCondition(searchDto).getEntities()
				.stream().map(E -> createFacilityApplicationForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録(施設登録申請:正会員)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_regular_facility_application")
	public ResponseEntity<String> enterRegularFacilityApplication(
			@RequestBody @Validated FacilityApplicationForm facilityApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		facilityApplicationSrv.enterRegular(createEnterFacilityApplicationDto(facilityApplicationForm));

		return ResponseEntity.ok("enterRegularFacilityApplication ok");
	}

	/**
	 * 登録(施設登録申請:仮会員)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_temporary_facility_application")
	public ResponseEntity<String> enterTemporaryFacilityApplication(
			@RequestBody @Validated FacilityApplicationForm facilityApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		facilityApplicationSrv.enterTemporary(createEnterFacilityApplicationDto(facilityApplicationForm));

		return ResponseEntity.ok("enterTemporaryFacilityApplication ok");
	}

	/**
	 * 更新(施設登録申請)
	 * @return 実行結果
	 */
	@PostMapping(path = "update_facility_application")
	public ResponseEntity<String> updateFacilityApplication(
			@RequestBody @Validated FacilityApplicationForm facilityApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		FacilityApplicationCompositeEntity compositeEntity = facilityApplicationSrv
				.getSearchResult(facilityApplicationForm.getId()).getFirstEntity();

		// 更新対象DTO生成
		FacilityApplicationDto updateDto = FacilityApplicationDto.createSingle();
		FacilityApplicationCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 施設登録申請
		FacilityApplicationEntity updateEntity = updateCompositeEntity.getFacilityApplicationEntity();
		BeanUtil.copyProperties(compositeEntity.getFacilityApplicationEntity(), updateEntity, false); // 現在値
		BeanUtil.copyProperties(facilityApplicationForm, updateEntity, true); // 更新値(差分)

		// 施設登録申請分類
		updateCompositeEntity.setFacilityApplicationCategoryEntities(getUpdateEntities(
				facilityApplicationForm.getFacilityApplicationCategories(),
				compositeEntity.getFacilityApplicationCategoryEntities(),
				FacilityApplicationCategoryEntity.class));

		// 実行
		facilityApplicationSrv.update(updateDto);

		return ResponseEntity.ok("updateFacilityApplication ok");
	}

	/**
	 * 削除(施設登録申請)<br>
	 * 関連する要素も同時に削除します。
	 * @return 実行結果
	 */
	@PostMapping(path = "delete_facility_application")
	public ResponseEntity<String> deleteFacilityApplication(
			@RequestBody @Validated FacilityApplicationSearchForm facilityApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		FacilityApplicationSearchDto searchDto = createFacilityApplicationSearchDto(facilityApplicationForm);

		// 実行
		facilityApplicationSrv.delete(searchDto);

		return ResponseEntity.ok("deleteFacilityApplication ok");
	}

	/**
	 * 条件検索(施設登録申請分類)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_facility_application_category")
	public ResponseEntity<List<FacilityApplicationCategoryForm>> searchFacilityApplicationCategory(
			@RequestBody @Validated FacilityApplicationCategoryForm facilityApplicationCategoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		FacilityApplicationCategorySearchDto searchDto = new FacilityApplicationCategorySearchDto();
		BeanUtil.copyProperties(facilityApplicationCategoryForm, searchDto, false);
		searchDto.setApplicantIds(createArray(facilityApplicationCategoryForm.getApplicantId())); // 施設登録申請ID

		// 検索結果フォーム取得
		List<FacilityApplicationCategoryForm> forms = facilityApplicationCategorySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> BeanUtil.createCopyProperties(E, FacilityApplicationCategoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(施設登録申請分類)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_facility_application_category")
	public ResponseEntity<String> enterFacilityApplicationCategory(
			@RequestBody @Validated FacilityApplicationCategoryForm facilityApplicationCategoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		facilityApplicationCategorySrv
				.enter(copyDto(facilityApplicationCategoryForm, FacilityApplicationCategoryDto.createSingle()));

		return ResponseEntity.ok("enterFacilityApplicationCategory ok");
	}

	/**
	 * 条件検索(施設養成校)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_facility")
	public ResponseEntity<List<FacilityForm>> searchFacility(
			@RequestBody @Validated FacilityPortalSearchForm facilityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		FacilityPortalSearchDto searchDto = createFacilityPortalSearchDto(facilityForm);

		// 検索結果フォーム取得
		List<FacilityForm> forms = facilitySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createFacilityForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(施設養成校リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_facility")
	public ResponseEntity<FacilityListForm> searchListFacility(
			@RequestBody @Validated FacilityPortalSearchListForm facilityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		FacilityPortalSearchDto searchDto = createFacilityPortalSearchDto(facilityForm);

		// 検索結果フォーム取得
		FacilityListForm form = new FacilityListForm();
		form.setFacilities(facilitySrv.getSearchResultListByCondition(searchDto).getEntities().stream()
				.map(E -> createFacilityForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録(施設養成校)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_facility")
	public ResponseEntity<String> enterFacility(@RequestBody @Validated FacilityForm facilityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		FacilityDto enterDto = FacilityDto.createSingle();
		FacilityCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 施設養成校
		FacilityEntity enterEntity = compositeEntity.getFacilityEntity();
		BeanUtil.copyProperties(facilityForm, enterEntity, false);

		// 施設分類
		compositeEntity.setFacilityCategoryEntities(
				getEnterEntities(facilityForm.getFacilityCategories(), FacilityCategoryEntity.class));

		// 実行
		facilitySrv.enter(enterDto);

		return ResponseEntity.ok("enterFacility ok");
	}

	/**
	 * 更新(施設養成校)
	 * @return 実行結果
	 */
	@PostMapping(path = "update_facility")
	public ResponseEntity<String> updateFacility(@RequestBody @Validated FacilityForm facilityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		FacilityCompositeEntity compositeEntity = facilitySrv.getSearchResult(facilityForm.getId()).getFirstEntity();

		// 更新対象DTO生成
		FacilityDto updateDto = FacilityDto.createSingle();
		FacilityCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 施設養成校
		FacilityEntity updateEntity = updateCompositeEntity.getFacilityEntity();
		BeanUtil.copyProperties(compositeEntity.getFacilityEntity(), updateEntity, false); // 現在値
		BeanUtil.copyProperties(facilityForm, updateEntity, true); // 更新値(差分)

		// 施設分類
		updateCompositeEntity.setFacilityCategoryEntities(getUpdateEntities(facilityForm.getFacilityCategories(),
				compositeEntity.getFacilityCategoryEntities(), FacilityCategoryEntity.class));

		// 実行
		facilitySrv.update(updateDto);

		return ResponseEntity.ok("updateFacility ok");
	}

	/**
	 * 条件検索(施設分類)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_facility_category")
	public ResponseEntity<List<FacilityCategoryForm>> searchFacilityCategory(
			@RequestBody @Validated FacilityCategoryForm facilityCategoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		FacilityCategorySearchDto searchDto = new FacilityCategorySearchDto();
		BeanUtil.copyProperties(facilityCategoryForm, searchDto, false);
		searchDto.setFacilityIds(createArray(facilityCategoryForm.getFacilityId())); // 施設養成校ID

		// 検索結果フォーム取得
		List<FacilityCategoryForm> forms = facilityCategorySrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, FacilityCategoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(施設分類)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_facility_category")
	public ResponseEntity<String> enterFacilityCategory(
			@RequestBody @Validated FacilityCategoryForm facilityCategoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		facilityCategorySrv.enter(copyDto(facilityCategoryForm, FacilityCategoryDto.createSingle()));

		return ResponseEntity.ok("enterFacilityCategory ok");
	}

	/**
	 * 条件検索(施設養成校勤務者)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_facility_member")
	public ResponseEntity<List<FacilityMemberForm>> searchFacilityMember(
			@RequestBody @Validated FacilityMemberForm facilityMemberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		FacilityMemberSearchDto searchDto = new FacilityMemberSearchDto();
		BeanUtil.copyProperties(facilityMemberForm, searchDto, false);
		searchDto.setFacilityIds(createArray(facilityMemberForm.getFacilityId())); // 施設養成校ID

		// 検索結果フォーム取得
		List<FacilityMemberForm> forms = facilityMemberSrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, FacilityMemberForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(施設養成校勤務者)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_facility_member")
	public ResponseEntity<String> enterFacilityMember(
			@RequestBody @Validated FacilityMemberForm facilityMemberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		facilityMemberSrv.enter(copyDto(facilityMemberForm, FacilityMemberDto.createSingle()));

		return ResponseEntity.ok("enterFacilityMember ok");
	}

	/**
	 * 施設登録申請検索DTO生成
	 * @param facilityForm  施設登録申請検索フォーム
	 * @return 施設登録申請検索DTO
	 */
	private FacilityApplicationSearchDto createFacilityApplicationSearchDto(
			FacilityApplicationSearchForm facilityForm) {

		// 検索条件DTO生成
		FacilityApplicationSearchDto searchDto = new FacilityApplicationSearchDto();
		BeanUtil.copyProperties(facilityForm, searchDto, false);

		// 属性設定(個別)
		searchDto.setMemberNos(createArray(facilityForm.getMemberNo())); // 会員番号
		searchDto.setTemporaryMemberNos(createArray(facilityForm.getTemporaryMemberNo())); // 仮会員番号
		searchDto.setStatuses(createArray(facilityForm.getStatus())); // 申請ステータス

		// TODO

		return searchDto;
	}

	/**
	 * 施設登録申請DTO生成
	 * @param facilityForm 施設登録申請フォーム
	 * @return 施設登録申請DTO生成
	 */
	private FacilityApplicationDto createEnterFacilityApplicationDto(FacilityApplicationForm facilityForm) {

		// 追加対象DTO生成
		FacilityApplicationDto enterDto = FacilityApplicationDto.createSingle();
		FacilityApplicationCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 施設登録申請
		FacilityApplicationEntity enterEntity = compositeEntity.getFacilityApplicationEntity();
		BeanUtil.copyProperties(facilityForm, enterEntity, false);

		// 施設登録申請分類
		compositeEntity.setFacilityApplicationCategoryEntities(
				getEnterEntities(facilityForm.getFacilityApplicationCategories(),
						FacilityApplicationCategoryEntity.class));

		return enterDto;
	}

	/**
	 * 施設養成校検索DTO生成(会員ポータル用)
	 * @param facilityForm  施設養成校検索フォーム
	 * @return 施設養成校検索DTO
	 */
	private FacilityPortalSearchDto createFacilityPortalSearchDto(FacilityPortalSearchForm facilityForm) {

		FacilityPortalSearchDto searchDto = new FacilityPortalSearchDto();
		BeanUtil.copyProperties(facilityForm, searchDto, false);

		// 複数要素
		searchDto.setPrefectureCds(
				facilityForm.getPrefectureCds().stream().toArray(V -> new String[V])); // 都道府県コード
		searchDto.setFacilityCategories(
				facilityForm.getFacilityCategories().stream().toArray(V -> new String[V])); // 施設区分
		searchDto.setTrainingSchoolCategoryCds(
				facilityForm.getTrainingSchoolCategoryCds().stream().toArray(V -> new String[V])); // 養成校種別
		searchDto.setOpenerTypeCds(
				facilityForm.getOpenerTypeCds().stream().toArray(V -> new String[V])); // 開設者種別
		searchDto.setClinicalTrainingFacilityCertifiedStatuses(
				facilityForm.getClinicalTrainingFacilityCertifiedStatuses().stream().toArray(V -> new String[V])); // 臨床実習指導施設認定状況
		searchDto.setWfotCertifiedStatuses(
				facilityForm.getWfotCertifiedStatuses().stream().toArray(V -> new Integer[V])); // WFOT認定状況
		searchDto.setProfessionalOtAttendingFields(
				facilityForm.getProfessionalOtAttendingFields().stream().toArray(V -> new String[V])); // 専門作業療法士在籍分野

		// 領域
		searchDto.setRegions(facilityForm.getRegions().stream().map(V -> createRegionSearchDto(V))
				.toArray(RegionSearchDto[]::new));

		// 範囲条件
		searchDto.setCountMemberRC(RANGE_CONDITION.valueOf(facilityForm.getCountMemberRC())); // 会員在籍者
		searchDto.setCountLicense3yearRC(RANGE_CONDITION.valueOf(facilityForm.getCountLicense3yearRC())); // 免許取得３年以上
		searchDto.setCountLicense5yearRC(RANGE_CONDITION.valueOf(facilityForm.getCountLicense5yearRC())); // 免許取得５年以上

		return searchDto;
	}

	/**
	 * 領域検索DTO生成
	 * @param regionForm 領域検索フォーム
	 * @return 領域検索DTO
	 */
	private RegionSearchDto createRegionSearchDto(RegionSearchForm regionForm) {

		RegionSearchDto regionDto = new RegionSearchDto();

		// 分類種別
		regionDto.setCategory(regionForm.getCategory());

		// 該当
		regionDto.setIsApplicable(regionForm.getIsApplicable());

		// 分類検索DTO
		List<ClassificationSearchDto> dtos = new ArrayList<ClassificationSearchDto>();
		for (ClassificationSearchForm form : regionForm.getClassifications()) {
			ClassificationSearchDto dto = new ClassificationSearchDto();
			dto.setCondition(CLASS_CONDITION.valueOf(form.getCondition())); // 条件
			dto.setDivision(CATEGORY_DIVISION.valueOf(form.getDivision())); // 分類区分 
			dto.setCodes(form.getCodes().stream().toArray(V -> new String[V])); // 分類コード
			dtos.add(dto);
		}
		regionDto.setClassifications(dtos.stream().toArray(V -> new ClassificationSearchDto[V]));

		return regionDto;
	}

	/**
	 * 施設登録申請フォーム生成
	 * @param compositeEntity 施設登録申請エンティティ(複合)
	 * @return 施設登録申請フォーム
	 */
	private FacilityApplicationForm createFacilityApplicationForm(FacilityApplicationCompositeEntity compositeEntity) {

		// 施設登録申請
		FacilityApplicationForm createForm = BeanUtil.createCopyProperties(
				compositeEntity.getFacilityApplicationEntity(), FacilityApplicationForm.class, false);

		// 施設登録申請分類
		List<FacilityApplicationCategoryEntity> categoryEntities = compositeEntity
				.getFacilityApplicationCategoryEntities();
		if (!CollectionUtils.isEmpty(categoryEntities)) {
			createForm.setFacilityApplicationCategories(categoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, FacilityApplicationCategoryForm.class, false))
					.collect(Collectors.toList()));
		}

		return createForm;
	}

	/**
	 * 施設養成校フォーム生成
	 * @param compositeEntity 施設養成校エンティティ(複合)
	 * @return 施設養成校フォーム
	 */
	private FacilityForm createFacilityForm(FacilityCompositeEntity compositeEntity) {

		// 施設養成校
		FacilityForm createForm = BeanUtil.createCopyProperties(compositeEntity.getFacilityEntity(),
				FacilityForm.class, false);

		// 施設分類
		List<FacilityCategoryEntity> categoryEntities = compositeEntity.getFacilityCategoryEntities();
		if (!CollectionUtils.isEmpty(categoryEntities)) {
			createForm.setFacilityCategories(categoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, FacilityCategoryForm.class, false))
					.collect(Collectors.toList()));
		}

		return createForm;
	}

	// TODO
}
