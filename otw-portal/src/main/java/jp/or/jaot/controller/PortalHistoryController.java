package jp.or.jaot.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.DepartmentHistoryDto;
import jp.or.jaot.model.dto.MemberPasswordHistoryDto;
import jp.or.jaot.model.dto.OfficerHistoryDto;
import jp.or.jaot.model.dto.RecessHistoryDto;
import jp.or.jaot.model.dto.RewardPunishHistoryDto;
import jp.or.jaot.model.dto.search.DepartmentHistorySearchDto;
import jp.or.jaot.model.dto.search.MemberPasswordHistorySearchDto;
import jp.or.jaot.model.dto.search.MemberUpdateHistorySearchDto;
import jp.or.jaot.model.dto.search.OfficerHistorySearchDto;
import jp.or.jaot.model.dto.search.PaotMembershipHistorySearchDto;
import jp.or.jaot.model.dto.search.PaotOfficerHistorySearchDto;
import jp.or.jaot.model.dto.search.RecessHistorySearchDto;
import jp.or.jaot.model.dto.search.RewardPunishHistorySearchDto;
import jp.or.jaot.model.form.DepartmentHistoryForm;
import jp.or.jaot.model.form.MemberPasswordHistoryForm;
import jp.or.jaot.model.form.MemberUpdateHistoryForm;
import jp.or.jaot.model.form.OfficerHistoryForm;
import jp.or.jaot.model.form.PaotMembershipHistoryForm;
import jp.or.jaot.model.form.PaotOfficerHistoryForm;
import jp.or.jaot.model.form.RecessHistoryForm;
import jp.or.jaot.model.form.RewardPunishHistoryForm;
import jp.or.jaot.model.form.search.MemberUpdateHistorySearchForm;
import jp.or.jaot.service.DepartmentHistoryService;
import jp.or.jaot.service.MemberPasswordHistoryService;
import jp.or.jaot.service.MemberUpdateHistoryService;
import jp.or.jaot.service.OfficerHistoryService;
import jp.or.jaot.service.PaotMembershipHistoryService;
import jp.or.jaot.service.PaotOfficerHistoryService;
import jp.or.jaot.service.RecessHistoryService;
import jp.or.jaot.service.RewardPunishHistoryService;
import lombok.AllArgsConstructor;

/**
 * 履歴コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "history", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PortalHistoryController extends BaseController {

	/** 役員履歴サービス */
	private final OfficerHistoryService officerHistorySrv;

	/** 賞罰履歴サービス */
	private final RewardPunishHistoryService rewardPunishHistorySrv;

	/** 部署履歴サービス */
	private final DepartmentHistoryService departmentHistorySrv;

	/** 県士会在籍履歴サービス */
	private final PaotMembershipHistoryService paotMembershipHistorySrv;

	/** 県士会役員履歴サービス */
	private final PaotOfficerHistoryService paotOfficerHistorySrv;

	/** 休会履歴サービス */
	private final RecessHistoryService recessHistorySrv;

	/** 会員パスワード履歴サービス */
	private final MemberPasswordHistoryService memberPasswordHistorySrv;

	/** 会員更新履歴サービス */
	private final MemberUpdateHistoryService memberUpdateHistorySrv;

	/**
	 * 条件検索(役員履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_officer_history")
	public ResponseEntity<List<OfficerHistoryForm>> searchOfficerHistory(
			@RequestBody @Validated OfficerHistoryForm officerHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		OfficerHistorySearchDto searchDto = new OfficerHistorySearchDto();
		BeanUtil.copyProperties(officerHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<OfficerHistoryForm> forms = officerHistorySrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, OfficerHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(役員履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_officer_history")
	public ResponseEntity<String> enterOfficerHistory(
			@RequestBody @Validated OfficerHistoryForm officerHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		officerHistorySrv.enter(copyDto(officerHistoryForm, OfficerHistoryDto.createSingle()));

		return ResponseEntity.ok("enterOfficerHistory ok");
	}

	/**
	 * 条件検索(賞罰履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_reward_punish_history")
	public ResponseEntity<List<RewardPunishHistoryForm>> searchRewardPunishHistory(
			@RequestBody @Validated RewardPunishHistoryForm rewardPunishHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		RewardPunishHistorySearchDto searchDto = new RewardPunishHistorySearchDto();
		BeanUtil.copyProperties(rewardPunishHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<RewardPunishHistoryForm> forms = rewardPunishHistorySrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, RewardPunishHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(賞罰履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_reward_punish_history")
	public ResponseEntity<String> enterRewardPunishHistory(
			@RequestBody @Validated RewardPunishHistoryForm rewardPunishHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		rewardPunishHistorySrv.enter(copyDto(rewardPunishHistoryForm, RewardPunishHistoryDto.createSingle()));

		return ResponseEntity.ok("enterRewardPunishHistory ok");
	}

	/**
	 * 条件検索(部署履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_department_history")
	public ResponseEntity<List<DepartmentHistoryForm>> searchDepartmentHistory(
			@RequestBody @Validated DepartmentHistoryForm departmentHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		DepartmentHistorySearchDto searchDto = new DepartmentHistorySearchDto();
		BeanUtil.copyProperties(departmentHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<DepartmentHistoryForm> forms = departmentHistorySrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, DepartmentHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(活動履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_department_history")
	public ResponseEntity<String> enterDepartmentHistory(
			@RequestBody @Validated DepartmentHistoryForm departmentHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		departmentHistorySrv.enter(copyDto(departmentHistoryForm, DepartmentHistoryDto.createSingle()));

		return ResponseEntity.ok("enterDepartmentHistory ok");
	}

	/**
	 * 条件検索(県士会在籍履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_paot_membership_history")
	public ResponseEntity<List<PaotMembershipHistoryForm>> searchPaotMembershipHistory(
			@RequestBody @Validated PaotMembershipHistoryForm paotMembershipHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		PaotMembershipHistorySearchDto searchDto = new PaotMembershipHistorySearchDto();
		BeanUtil.copyProperties(paotMembershipHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<PaotMembershipHistoryForm> forms = paotMembershipHistorySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, PaotMembershipHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(県士会役員履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_paot_officer_history")
	public ResponseEntity<List<PaotOfficerHistoryForm>> searchPaotOfficerHistory(
			@RequestBody @Validated PaotOfficerHistoryForm paotOfficerHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		PaotOfficerHistorySearchDto searchDto = new PaotOfficerHistorySearchDto();
		BeanUtil.copyProperties(paotOfficerHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<PaotOfficerHistoryForm> forms = paotOfficerHistorySrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, PaotOfficerHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(休会履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_recess_history")
	public ResponseEntity<List<RecessHistoryForm>> searchRecessHistory(
			@RequestBody @Validated RecessHistoryForm recessHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		RecessHistorySearchDto searchDto = new RecessHistorySearchDto();
		BeanUtil.copyProperties(recessHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<RecessHistoryForm> forms = recessHistorySrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, RecessHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(休会履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_recess_history")
	public ResponseEntity<String> enterRecessHistory(
			@RequestBody @Validated RecessHistoryForm recessHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		recessHistorySrv.enter(copyDto(recessHistoryForm, RecessHistoryDto.createSingle()));

		return ResponseEntity.ok("enterRecessHistory ok");
	}

	/**
	 * 条件検索(会員パスワード履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_member_password_history")
	public ResponseEntity<List<MemberPasswordHistoryForm>> searchMemberPasswordHistory(
			@RequestBody @Validated MemberPasswordHistoryForm memberPasswordHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberPasswordHistorySearchDto searchDto = new MemberPasswordHistorySearchDto();
		BeanUtil.copyProperties(memberPasswordHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberPasswordHistoryForm> forms = memberPasswordHistorySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, MemberPasswordHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(会員パスワード履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_member_password_history")
	public ResponseEntity<String> enterMemberPasswordHistory(
			@RequestBody @Validated MemberPasswordHistoryForm memberPasswordHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberPasswordHistorySrv.enter(copyDto(memberPasswordHistoryForm, MemberPasswordHistoryDto.createSingle()));

		return ResponseEntity.ok("enterMemberPasswordHistory ok");
	}

	/**
	 * 条件検索(会員更新履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_member_update_history")
	public ResponseEntity<List<MemberUpdateHistoryForm>> searchMemberUpdateHistory(
			@RequestBody @Validated MemberUpdateHistorySearchForm memberUpdateHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberUpdateHistorySearchDto searchDto = new MemberUpdateHistorySearchDto();
		BeanUtil.copyProperties(memberUpdateHistoryForm, searchDto, false); // 属性値
		searchDto.setFieldCds(memberUpdateHistoryForm.getFieldCds().stream().toArray(V -> new Integer[V])); // 項目コード配列
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberUpdateHistoryForm> forms = memberUpdateHistorySrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, MemberUpdateHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	// TODO
}
