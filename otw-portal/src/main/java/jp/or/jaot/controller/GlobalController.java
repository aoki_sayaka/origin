package jp.or.jaot.controller;

import javax.annotation.PostConstruct;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.support.WebContentGenerator;

import jp.or.jaot.core.util.SpringContext;

/**
 * グローバルコントローラ
 */
@Controller
public class GlobalController extends WebContentGenerator {
	/**
	 * Controllerの初期化処理。システム全体で１回行われれば良い処理を記述している。
	 */
	@PostConstruct
	private void init() {
		ApplicationContext ctx = getApplicationContext();
		SpringContext springContext = ctx.getBean(SpringContext.class);
		springContext.setApplicationContext(ctx);
	}
}
