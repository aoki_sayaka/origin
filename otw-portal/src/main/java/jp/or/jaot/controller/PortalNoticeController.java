package jp.or.jaot.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.dto.NoticeConditionPrefDto;
import jp.or.jaot.model.dto.NoticeDestinationDto;
import jp.or.jaot.model.dto.NoticeDto;
import jp.or.jaot.model.dto.search.NoticeConditionPrefSearchDto;
import jp.or.jaot.model.dto.search.NoticeDestinationSearchDto;
import jp.or.jaot.model.dto.search.NoticeSearchDto;
import jp.or.jaot.model.entity.NoticeConditionPrefEntity;
import jp.or.jaot.model.entity.NoticeDestinationEntity;
import jp.or.jaot.model.entity.NoticeEntity;
import jp.or.jaot.model.entity.composite.NoticeCompositeEntity;
import jp.or.jaot.model.form.NoticeConditionPrefForm;
import jp.or.jaot.model.form.NoticeDestinationForm;
import jp.or.jaot.model.form.NoticeForm;
import jp.or.jaot.model.form.NoticeMyselfCountForm;
import jp.or.jaot.model.form.NoticeMyselfForm;
import jp.or.jaot.model.form.search.NoticeSearchForm;
import jp.or.jaot.model.form.update.NoticeConditionPrefUpdateForm;
import jp.or.jaot.model.form.update.NoticeDestinationUpdateForm;
import jp.or.jaot.model.form.update.NoticeUpdateForm;
import jp.or.jaot.service.NoticeConditionPrefService;
import jp.or.jaot.service.NoticeDestinationService;
import jp.or.jaot.service.NoticeService;
import lombok.AllArgsConstructor;

/**
 * お知らせコントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "notice", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PortalNoticeController extends BaseController {

	/** お知らせ(都道府県)サービス */
	private final NoticeConditionPrefService noticeConditionPrefSrv;

	/** お知らせ(宛先)サービス */
	private final NoticeDestinationService noticeDestinationSrv;

	/** お知らせサービス */
	private final NoticeService noticeSrv;

	/**
	 * 条件検索(お知らせ(都道府県))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_notice_condition_pref")
	public ResponseEntity<List<NoticeConditionPrefForm>> searchNoticeConditionPref(
			@RequestBody @Validated NoticeConditionPrefForm noticeConditionPrefForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		NoticeConditionPrefSearchDto searchDto = new NoticeConditionPrefSearchDto();
		BeanUtil.copyProperties(noticeConditionPrefForm, searchDto, false); // 属性値
		if (noticeConditionPrefForm.getNoticeId() != null) {
			searchDto.setNoticeIds(new Long[] { noticeConditionPrefForm.getNoticeId() }); // お知らせID
		}
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<NoticeConditionPrefForm> forms = noticeConditionPrefSrv.getSearchResultByCondition(searchDto)
				.getEntities().stream()
				.map(E -> BeanUtil.createCopyProperties(E, NoticeConditionPrefForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(お知らせ(都道府県))
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_notice_condition_pref")
	public ResponseEntity<String> enterNoticeConditionPref(
			@RequestBody @Validated NoticeConditionPrefForm noticeConditionPrefForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		noticeConditionPrefSrv.enter(copyDto(noticeConditionPrefForm, NoticeConditionPrefDto.createSingle()));

		return ResponseEntity.ok("enterNoticeConditionPref ok");
	}

	/**
	 * 更新(お知らせ(都道府県))<br>
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * NULLを許容する要素を更新対象とする場合は、明示的に初期化対象プロパティを指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_notice_condition_pref")
	public ResponseEntity<String> updateNoticeConditionPref(
			@RequestBody @Validated NoticeConditionPrefUpdateForm noticeConditionPrefUpdateForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		noticeConditionPrefSrv.update(copyDto(noticeConditionPrefUpdateForm,
				noticeConditionPrefUpdateForm.getInitProperties(),
				noticeConditionPrefSrv.getSearchResult(noticeConditionPrefUpdateForm.getId()).getFirstEntity(),
				NoticeConditionPrefDto.createSingle()));

		return ResponseEntity.ok("updateNoticeConditionPref ok");
	}

	/**
	 * 条件検索(お知らせ(宛先))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_notice_destination")
	public ResponseEntity<List<NoticeDestinationForm>> searchNoticeDestination(
			@RequestBody @Validated NoticeDestinationForm noticeDestinationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		NoticeDestinationSearchDto searchDto = new NoticeDestinationSearchDto();
		BeanUtil.copyProperties(noticeDestinationForm, searchDto, false); // 属性値
		if (noticeDestinationForm.getNoticeId() != null) {
			searchDto.setNoticeIds(new Long[] { noticeDestinationForm.getNoticeId() }); // お知らせID
		}
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<NoticeDestinationForm> forms = noticeDestinationSrv.getSearchResultByCondition(searchDto)
				.getEntities().stream()
				.map(E -> BeanUtil.createCopyProperties(E, NoticeDestinationForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(お知らせ(宛先))
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_notice_destination")
	public ResponseEntity<String> enterNoticeDestination(
			@RequestBody @Validated NoticeDestinationForm noticeDestinationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		noticeDestinationSrv.enter(copyDto(noticeDestinationForm, NoticeDestinationDto.createSingle()));

		return ResponseEntity.ok("enterNoticeDestination ok");
	}

	/**
	 * 登録(お知らせ(宛先@自己))
	 * [補足事項]<br>
	 * 会員番号、開封日時(既読のみ)を補完後に処理を行う。
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_myself_notice_destination")
	public ResponseEntity<String> enterMyselfNoticeDestination(
			@RequestBody @Validated NoticeDestinationForm noticeDestinationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		noticeDestinationSrv.enterMyself(copyDto(noticeDestinationForm, NoticeDestinationDto.createSingle()));

		return ResponseEntity.ok("enterMyselfNoticeDestination ok");
	}

	/**
	 * 更新(お知らせ(宛先))<br>
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * NULLを許容する要素を更新対象とする場合は、明示的に初期化対象プロパティを指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_notice_destination")
	public ResponseEntity<String> updateNoticeDestination(
			@RequestBody @Validated NoticeDestinationUpdateForm noticeDestinationUpdateForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		noticeDestinationSrv.update(copyDto(noticeDestinationUpdateForm,
				noticeDestinationUpdateForm.getInitProperties(),
				noticeDestinationSrv.getSearchResult(noticeDestinationUpdateForm.getId()).getFirstEntity(),
				NoticeDestinationDto.createSingle()));

		return ResponseEntity.ok("updateNoticeDestination ok");
	}

	/**
	 * 更新(お知らせ(宛先@自己))<br>
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * NULLを許容する要素を更新対象とする場合は、明示的に初期化対象プロパティを指定する。
	 * 会員番号、開封日時(既読のみ)を補完後に処理を行う。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_myself_notice_destination")
	public ResponseEntity<String> updateMyselfNoticeDestination(
			@RequestBody @Validated NoticeDestinationUpdateForm noticeDestinationUpdateForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		noticeDestinationSrv.updateMyself(copyDto(noticeDestinationUpdateForm,
				noticeDestinationUpdateForm.getInitProperties(),
				noticeDestinationSrv.getSearchResult(noticeDestinationUpdateForm.getId()).getFirstEntity(),
				NoticeDestinationDto.createSingle()));

		return ResponseEntity.ok("updateMyselfNoticeDestination ok");
	}

	/**
	 * 条件検索(お知らせ)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_notice")
	public ResponseEntity<List<NoticeForm>> searchNotice(@RequestBody @Validated NoticeForm noticeForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		NoticeSearchDto searchDto = new NoticeSearchDto();
		BeanUtil.copyProperties(noticeForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<NoticeForm> forms = noticeSrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createNoticeForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(お知らせ(自己))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_myself_notice")
	public ResponseEntity<List<NoticeMyselfForm>> searchMyselfNotice(
			@RequestBody @Validated NoticeSearchForm noticeForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// システム現在日時
		Timestamp publish = new Timestamp(System.currentTimeMillis());

		// 検索結果フォーム取得
		List<NoticeMyselfForm> forms = noticeSrv
				.getSearchResultMyselfByCondition(createNoticeSearchDto(noticeForm, publish)).getEntities()
				.stream().map(E -> createNoticeMyselfForm(E, publish)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 件数取得(お知らせ(自己))
	 * @return 実行結果
	 */
	@PostMapping(path = "count_myself_notice")
	public ResponseEntity<NoticeMyselfCountForm> countMyselfNotice(
			@RequestBody @Validated NoticeSearchForm noticeForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// システム現在日時
		Timestamp publish = new Timestamp(System.currentTimeMillis());

		// 検索結果フォーム取得
		List<NoticeMyselfForm> forms = noticeSrv
				.getSearchResultMyselfByCondition(createNoticeSearchDto(noticeForm, publish)).getEntities()
				.stream().map(E -> createNoticeMyselfForm(E, publish)).collect(Collectors.toList());

		// お知らせ件数フォーム(自己)設定
		NoticeMyselfCountForm form = new NoticeMyselfCountForm();
		if (CollectionUtils.isNotEmpty(forms)) {

			// 重要なお知らせ件数
			form.setImportantCount(forms.stream().filter(E -> (E.getPriority() == true)).count());
			form.setImportantUnreadCount(forms.stream().filter(E -> (E.getPriority() == true)
					&& ((E.getNoticeDestination() == null) || (E.getNoticeDestination().getReadFlg() == false)))
					.count()); // 未読
			form.setImportantAlreadyReadCount(form.getImportantCount() - form.getImportantUnreadCount()); // 既読

			// 協会からのお知らせ件数
			form.setAssociationCount(forms.stream().filter(E -> (E.getPriority() == false)).count());
			form.setAssociationUnreadCount(forms.stream().filter(E -> (E.getPriority() == false)
					&& ((E.getNoticeDestination() == null) || (E.getNoticeDestination().getReadFlg() == false)))
					.count()); // 未読
			form.setAssociationAlreadyReadCount(form.getAssociationCount() - form.getAssociationUnreadCount()); // 既読
		} else {
			// 初期値
			form.setImportantCount(0L);
			form.setImportantUnreadCount(0L);
			form.setImportantAlreadyReadCount(0L);
			form.setAssociationCount(0L);
			form.setAssociationUnreadCount(0L);
			form.setAssociationAlreadyReadCount(0L);
		}

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録(お知らせ)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_notice")
	public ResponseEntity<String> enterNotice(@RequestBody @Validated NoticeForm noticeForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		NoticeDto enterDto = NoticeDto.createSingle();
		NoticeCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// お知らせ
		NoticeEntity enterNoticeEntity = compositeEntity.getNoticeEntity();
		BeanUtil.copyProperties(noticeForm, enterNoticeEntity, false);

		// お知らせ(都道府県)
		compositeEntity.setNoticeConditionPrefEntities(
				getEnterEntities(noticeForm.getNoticeConditionPrefs(), NoticeConditionPrefEntity.class));

		// お知らせ(宛先)
		compositeEntity.setNoticeDestinationEntities(
				getEnterEntities(noticeForm.getNoticeDestinations(), NoticeDestinationEntity.class));

		// 実行
		noticeSrv.enter(enterDto);

		return ResponseEntity.ok("enterNotice ok");
	}

	/**
	 * 更新(お知らせ)
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * NULLを許容する要素を更新対象とする場合は、明示的に初期化対象プロパティを指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_notice")
	public ResponseEntity<String> updateNotice(@RequestBody @Validated NoticeUpdateForm noticeForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		NoticeCompositeEntity compositeEntity = noticeSrv.getSearchResult(noticeForm.getId()).getFirstEntity();

		// 更新対象DTO生成
		NoticeDto updateDto = NoticeDto.createSingle();
		NoticeCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// お知らせ
		NoticeEntity updateNoticeEntity = updateCompositeEntity.getNoticeEntity();
		BeanUtil.copyProperties(compositeEntity.getNoticeEntity(), updateNoticeEntity, false); // 現在値
		BeanUtil.copyProperties(noticeForm, updateNoticeEntity, true,
				noticeForm.getInitProperties().toArray(new String[0])); // 更新値(差分)

		// お知らせ(都道府県)
		updateCompositeEntity.setNoticeConditionPrefEntities(
				getUpdateEntities(noticeForm.getUpdateNoticeConditionPrefs(),
						compositeEntity.getNoticeConditionPrefEntities(), NoticeConditionPrefEntity.class));

		// お知らせ(宛先)
		updateCompositeEntity.setNoticeDestinationEntities(
				getUpdateEntities(noticeForm.getUpdateNoticeDestinations(),
						compositeEntity.getNoticeDestinationEntities(), NoticeDestinationEntity.class));

		// 実行
		noticeSrv.update(updateDto);

		return ResponseEntity.ok("updateNotice ok");
	}

	/**
	 * お知らせフォーム生成
	 * @param compositeEntity お知らせエンティティ(複合)
	 * @return お知らせフォーム
	 */
	private NoticeForm createNoticeForm(NoticeCompositeEntity compositeEntity) {

		// お知らせ
		NoticeForm noticeForm = BeanUtil.createCopyProperties(compositeEntity.getNoticeEntity(), NoticeForm.class,
				false);

		// お知らせ(都道府県)
		List<NoticeConditionPrefEntity> prefEntities = compositeEntity.getNoticeConditionPrefEntities();
		if (!CollectionUtils.isEmpty(prefEntities)) {
			noticeForm.setNoticeConditionPrefs(prefEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, NoticeConditionPrefForm.class, false))
					.collect(Collectors.toList()));
		}

		// お知らせ(宛先)
		List<NoticeDestinationEntity> destinationEntities = compositeEntity.getNoticeDestinationEntities();
		if (!CollectionUtils.isEmpty(destinationEntities)) {
			noticeForm.setNoticeDestinations(destinationEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, NoticeDestinationForm.class, false))
					.collect(Collectors.toList()));
		}

		return noticeForm;
	}

	/**
	 * お知らせフォーム生成(自己)
	 * @param compositeEntity お知らせエンティティ(複合)
	 * @param publish 掲載日
	 * @return お知らせフォーム(自己)
	 */
	private NoticeMyselfForm createNoticeMyselfForm(NoticeCompositeEntity compositeEntity, Timestamp publish) {

		// お知らせ(自己)
		NoticeMyselfForm noticeMyselfForm = BeanUtil.createCopyProperties(compositeEntity.getNoticeEntity(),
				NoticeMyselfForm.class, false);
		noticeMyselfForm.setPublish(publish); // 掲載日

		// お知らせ(宛先)
		List<NoticeDestinationEntity> destinationEntities = compositeEntity.getNoticeDestinationEntities();
		NoticeDestinationEntity destinationEntity = null;
		if (!CollectionUtils.isEmpty(destinationEntities)
				&& ((destinationEntity = destinationEntities.stream().findFirst().orElseGet(null)) != null)) {
			noticeMyselfForm.setNoticeDestination(BeanUtil.createCopyProperties(destinationEntity,
					NoticeDestinationForm.class, false));
		}

		return noticeMyselfForm;
	}

	/**
	 * お知らせ検索DTO生成 
	 * @param noticeForm お知らせ検索フォーム
	 * @param publish システム現在日時
	 * @return お知らせ検索DTO
	 */
	private NoticeSearchDto createNoticeSearchDto(NoticeSearchForm noticeForm, Timestamp publish) {

		// 検索条件DTO生成
		NoticeSearchDto searchDto = new NoticeSearchDto();
		BeanUtil.copyProperties(noticeForm, searchDto, false); // 属性値

		// 掲載日
		if (noticeForm.getPublishRangeDay() != null) {

			// 掲載日範囲(From)
			searchDto.setPublishRangeFrom(
					new Timestamp(DateUtil.getFromDate(DateUtil.getNowPastDate(-noticeForm.getPublishRangeDay()))
							.getTime()));

			// 掲載日範囲(To)
			searchDto.setPublishRangeTo(new Timestamp(DateUtil.getToDate(new Date(publish.getTime())).getTime()));
		}
		logger.debug("掲載日 : {}, 範囲(日数) : {}, 掲載日範囲(From) : {}, 掲載日範囲(To) : {}",
				publish, noticeForm.getPublishRangeDay(), searchDto.getPublishRangeFrom(),
				searchDto.getPublishRangeTo());

		return searchDto;
	}

	// TODO
}
