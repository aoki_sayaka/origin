package jp.or.jaot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.HandbookMigrationApplicationDto;
import jp.or.jaot.model.dto.OtherOrgPointApplicationDto;
import jp.or.jaot.model.dto.search.BasicOtQualifyHistorySearchDto;
import jp.or.jaot.model.dto.search.HandbookMigrationApplicationSearchDto;
import jp.or.jaot.model.dto.search.OtherOrgPointApplicationSearchDto;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;
import jp.or.jaot.model.entity.composite.OtherOrgPointApplicationCompositeEntity;
import jp.or.jaot.model.form.BasicOtTrainingHistoryForm;
import jp.or.jaot.model.form.HandbookMigrationApplicationForm;
import jp.or.jaot.model.form.OtherOrgPointApplicationAttachmentForm;
import jp.or.jaot.model.form.OtherOrgPointApplicationForm;
import jp.or.jaot.model.form.OtherOrgPointApplicationUpdateForm;
import jp.or.jaot.service.BasicOtQualifyHistoryService;
import jp.or.jaot.service.HandbookMigrationApplicationService;
import jp.or.jaot.service.OtherOrgPointApplicationService;
import lombok.AllArgsConstructor;

/**
 * 各種申請コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "portal_various_application", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PortalVariousApplicationController extends BaseController {

	/** 手帳移行申請サービス */
	private final HandbookMigrationApplicationService handbookMigrationApplicationSrv;
	
	/** 他団体・SIGポイント申請サービス */
	private final OtherOrgPointApplicationService otherOrgPointApplicationSrv;

	/** 基礎研修修了認定更新履歴サービス */
	private final BasicOtQualifyHistoryService basicOtQualifyHistorySrv;
	
	public Boolean basicOtQualifyYorN(BasicOtQualifyHistorySearchDto searchDto) {

		searchDto.setMemberNo(1188); //動作確認のためにベタ書き

		Boolean b = basicOtQualifyHistorySrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.anyMatch(E -> E.getMemberNo().equals(searchDto.getMemberNo()));

		return b;
	}
	
	/** 
	 * 条件検索(手帳移行申請)
	 * @return 実行結果
	 */
	@PostMapping(path = "get_handbook_migration_application")
	public ResponseEntity<HandbookMigrationApplicationForm> getHandbookMigrationApplication(
			@RequestBody @Validated HandbookMigrationApplicationForm handbookMigrationApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		HandbookMigrationApplicationSearchDto searchDto = new HandbookMigrationApplicationSearchDto();
		BeanUtil.copyProperties(handbookMigrationApplicationForm, searchDto, false); // 属性値



		// 検索結果フォーム取得
		BeanUtil.copyProperties(
				handbookMigrationApplicationSrv.getSearchResultByMemberNo(searchDto.getMemberNo()).getFirstEntity(),
				handbookMigrationApplicationForm, false);

		// 検索結果受講履歴フォーム取得
		List<BasicOtTrainingHistoryForm> basicOtTrainingHistoryForms = handbookMigrationApplicationSrv
				.getSearchResultByCondition(searchDto.getMemberNo()).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, BasicOtTrainingHistoryForm.class, false))
				.collect(Collectors.toList());

		//受講履歴が登録されていれば画面に初期表示させる
		if (!CollectionUtils.isEmpty(basicOtTrainingHistoryForms)) {
			handbookMigrationApplicationForm.setBasicOtTrainingHistories(basicOtTrainingHistoryForms);
		}

		BasicOtQualifyHistorySearchDto searchDto2 = new BasicOtQualifyHistorySearchDto();

		searchDto2.setMemberNo(searchDto.getMemberNo());

		Boolean b = basicOtQualifyHistorySrv.getSearchResultByCondition(searchDto2).getEntities().stream()
				.anyMatch(E -> E.getMemberNo().equals(searchDto.getMemberNo()));
		
		handbookMigrationApplicationForm.setB(b);

		return ResponseEntity.ok(handbookMigrationApplicationForm);
	}
	
//	/**
//	 * 条件検索(手帳移行申請)
//	 * @return 実行結果
//	 */
//	@PostMapping(path = "search_handbook_migration_application")
//	public ResponseEntity<List<HandbookMigrationApplicationForm>> searchHandbookMigrationApplication(
//			@RequestBody @Validated HandbookMigrationApplicationForm handbookMigrationApplicationForm, Errors errors) {
//
//		// バリデーション実行
//		doValidation(errors);
//
//		// 検索条件DTO生成
//		HandbookMigrationApplicationSearchDto searchDto = new HandbookMigrationApplicationSearchDto();
//		BeanUtil.copyProperties(handbookMigrationApplicationForm, searchDto, false); // 属性値
//		searchDto.setStartPosition(0); // 開始位置
//		searchDto.setMaxResult(0); // 取得件数(全件)
//
//		// 検索結果フォーム取得
//		List<HandbookMigrationApplicationForm> forms = handbookMigrationApplicationSrv
//				.getSearchResultByCondition(searchDto).getEntities()
//				.stream().map(E -> BeanUtil.createCopyProperties(E, HandbookMigrationApplicationForm.class, false))
//				.collect(Collectors.toList());
//
//		// 検索結果受講履歴フォーム取得
//		List<BasicOtTrainingHistoryForm> basicOtTrainingHistoryForms = handbookMigrationApplicationSrv
//				.getSearchResultByCondition(searchDto.getMemberNo()).getEntities()
//				.stream().map(E -> BeanUtil.createCopyProperties(E, BasicOtTrainingHistoryForm.class, false))
//				.collect(Collectors.toList());
//
//		//受講履歴が登録されていれば画面に初期表示させる
//		if (basicOtTrainingHistoryForms.size() != 0 && forms.size() != 0) {
//			forms.get(0).setBasicOtTrainingHistories(basicOtTrainingHistoryForms);
//		} else {
//			handbookMigrationApplicationForm.setBasicOtTrainingHistories(basicOtTrainingHistoryForms);
//			forms.add(0, handbookMigrationApplicationForm);
//		}
//		
//		if (!CollectionUtils.isEmpty(basicOtTrainingHistoryForms)) {
//			handbookMigrationApplicationForm.setBasicOtTrainingHistories(basicOtTrainingHistoryForms);
//		}
//
//		//↓
//		BasicOtQualifyHistorySearchDto searchDto2 = new BasicOtQualifyHistorySearchDto();
//
//		searchDto2.setMemberNo(searchDto.getMemberNo()); //動作確認のためにベタ書き
//
//		Boolean b = basicOtQualifyHistorySrv.getSearchResultByCondition(searchDto2).getEntities().stream()
//				.anyMatch(E -> E.getMemberNo().equals(searchDto.getMemberNo()));
//		
//		handbookMigrationApplicationForm.setB(b);
//		
//		forms.add(0, handbookMigrationApplicationForm);
//
//		return ResponseEntity.ok(forms);
//	}

	/**
	 * 登録(手帳移行申請)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_handbook_migration_application")
	public ResponseEntity<String> enterHandbookMigrationApplication(
			@RequestBody @Validated HandbookMigrationApplicationForm handbookMigrationApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		handbookMigrationApplicationSrv
				.enter(copyDto(handbookMigrationApplicationForm, HandbookMigrationApplicationDto.createSingle()));

		return ResponseEntity.ok("enterHandbookMigrationApplication ok");
	}
	
	/**
	 * 条件検索(他団体・SIGポイント申請)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_other_org_point_application")
	public ResponseEntity<List<OtherOrgPointApplicationForm>> searchOtherOrgPointApplication(
			@RequestBody @Validated OtherOrgPointApplicationForm otherOrgPointApplicationForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		OtherOrgPointApplicationSearchDto searchDto = new OtherOrgPointApplicationSearchDto();
		BeanUtil.copyProperties(otherOrgPointApplicationForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<OtherOrgPointApplicationForm> forms = otherOrgPointApplicationSrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createOtherOrgPointApplicationForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}



	/**
	 * 更新<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "update_other_org_point_application")
	public ResponseEntity<String> updateOtherOrgPointApplication(
			@RequestBody @Validated OtherOrgPointApplicationUpdateForm OtherOrgPointApplicationUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		OtherOrgPointApplicationCompositeEntity compositeEntity = otherOrgPointApplicationSrv
				.getSearchResult(OtherOrgPointApplicationUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		OtherOrgPointApplicationDto updateDto = OtherOrgPointApplicationDto.createSingle();
		OtherOrgPointApplicationCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils
				.isTrue(OtherOrgPointApplicationUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// 他団体・SIGポイント申請
		OtherOrgPointApplicationEntity updateOtherOrgPointApplicationEntity = updateCompositeEntity
				.getOtherOrgPointApplicationEntity();
		BeanUtil.copyProperties(compositeEntity.getOtherOrgPointApplicationEntity(),
				updateOtherOrgPointApplicationEntity, false); // 現在値
		if (isIncrementalUpdate) {
			BeanUtil.copyProperties(OtherOrgPointApplicationUpdateForm, updateOtherOrgPointApplicationEntity,
					true); // 更新値(差分)
		} else {
			BeanUtil.copyProperties(OtherOrgPointApplicationUpdateForm, updateOtherOrgPointApplicationEntity,
					false); // 更新値(上書)
		}
		updateCompositeEntity.setOtherOrgPointApplicationEntity(updateOtherOrgPointApplicationEntity);

		// 他団体・SIGポイント添付ファイル
		List<OtherOrgPointApplicationAttachmentEntity> updateOtherOrgPointApplicationAttachmentEntities = new ArrayList<OtherOrgPointApplicationAttachmentEntity>();
		List<OtherOrgPointApplicationAttachmentEntity> qualificationEntities = compositeEntity
				.getOtherOrgPointApplicationAttachmentEntities();
		for (OtherOrgPointApplicationAttachmentForm form : OtherOrgPointApplicationUpdateForm
				.getOtherOrgPointApplicationAttachments()) {

			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(qualificationEntities)) {
				for (OtherOrgPointApplicationAttachmentEntity entity : qualificationEntities) {
					if (entity.getId().equals(form.getId())) {
						OtherOrgPointApplicationAttachmentEntity updateEntity = new OtherOrgPointApplicationAttachmentEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateOtherOrgPointApplicationAttachmentEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				OtherOrgPointApplicationAttachmentEntity insertEntity = new OtherOrgPointApplicationAttachmentEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateOtherOrgPointApplicationAttachmentEntities.add(insertEntity);
			}
		}

		// 他団体・SIGポイント添付ファイル(処理対象)
		updateCompositeEntity
				.setOtherOrgPointApplicationAttachmentEntities(updateOtherOrgPointApplicationAttachmentEntities);

		// 実行
		otherOrgPointApplicationSrv.update(updateDto);

		return ResponseEntity.ok("updateOtherOrgPointApplications ok");
	}

	/**
	 * 他団体・SIGポイント申請フォーム生成
	 * @param compositeEntity 他団体・SIGポイント申請エンティティ(複合)
	 * @return 他団体・SIGポイント申請フォーム
	 */
	private OtherOrgPointApplicationForm createOtherOrgPointApplicationForm(
			OtherOrgPointApplicationCompositeEntity compositeEntity) {

		// 他団体・SIGポイント申請フォーム
		OtherOrgPointApplicationForm otherOrgPointApplicationForm = BeanUtil.createCopyProperties(
				compositeEntity.getOtherOrgPointApplicationEntity(), OtherOrgPointApplicationForm.class,
				false);

		// 他団体・SIGポイント添付ファイル
		List<OtherOrgPointApplicationAttachmentEntity> otherOrgPointApplicationAttachmentEntities = compositeEntity.getOtherOrgPointApplicationAttachmentEntities();
		if (!CollectionUtils.isEmpty(otherOrgPointApplicationAttachmentEntities)) {
			otherOrgPointApplicationForm.setOtherOrgPointApplicationAttachments(otherOrgPointApplicationAttachmentEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, OtherOrgPointApplicationAttachmentForm.class, false))
					.collect(Collectors.toList()));
		}


		return otherOrgPointApplicationForm;

	}

}
