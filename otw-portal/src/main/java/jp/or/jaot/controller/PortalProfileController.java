package jp.or.jaot.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.exception.system.MailSystemException;
import jp.or.jaot.core.model.BaseEntity.ORDER_ASC;
import jp.or.jaot.core.model.BaseSearchDto.SearchOrder;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.core.util.CipherUtil;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.dto.ApplicationProgressDto;
import jp.or.jaot.model.dto.MemberDto;
import jp.or.jaot.model.dto.MemberEmailHistoryDto;
import jp.or.jaot.model.dto.MemberLocalActivityDto;
import jp.or.jaot.model.dto.MemberPasswordHistoryDto;
import jp.or.jaot.model.dto.MemberQualificationDto;
import jp.or.jaot.model.dto.MemberRevokedAuthorityDto;
import jp.or.jaot.model.dto.MemberRoleDto;
import jp.or.jaot.model.dto.MembershipFeeDeliveryDto;
import jp.or.jaot.model.dto.search.ApplicationProgressSearchDto;
import jp.or.jaot.model.dto.search.MemberEmailHistorySearchDto;
import jp.or.jaot.model.dto.search.MemberLocalActivitySearchDto;
import jp.or.jaot.model.dto.search.MemberPasswordHistorySearchDto;
import jp.or.jaot.model.dto.search.MemberQualificationSearchDto;
import jp.or.jaot.model.dto.search.MemberRevokedAuthoritySearchDto;
import jp.or.jaot.model.dto.search.MemberRoleSearchDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.dto.search.MembershipFeeDeliverySearchDto;
import jp.or.jaot.model.dto.search.PaotMembershipFeeDeliverySearchDto;
import jp.or.jaot.model.entity.ApplicationProgressEntity;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;
import jp.or.jaot.model.entity.FacilityApplicationEntity;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberFacilityCategoryEntity;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;
import jp.or.jaot.model.entity.MemberLoginHistoryEntity;
import jp.or.jaot.model.entity.MemberPasswordHistoryEntity;
import jp.or.jaot.model.entity.MemberQualificationEntity;
import jp.or.jaot.model.entity.RecessHistoryEntity;
import jp.or.jaot.model.entity.composite.FacilityApplicationCompositeEntity;
import jp.or.jaot.model.entity.composite.FacilityCompositeEntity;
import jp.or.jaot.model.entity.composite.MemberCompositeEntity;
import jp.or.jaot.model.form.ApplicationProgressForm;
import jp.or.jaot.model.form.ApplicationProgressListForm;
import jp.or.jaot.model.form.ChangePasswordForm;
import jp.or.jaot.model.form.FacilityApplicationCategoryForm;
import jp.or.jaot.model.form.FacilityApplicationForm;
import jp.or.jaot.model.form.FacilityCategoryForm;
import jp.or.jaot.model.form.FacilityForm;
import jp.or.jaot.model.form.MemberFacilityCategoryForm;
import jp.or.jaot.model.form.MemberForm;
import jp.or.jaot.model.form.MemberIsAvailableForm;
import jp.or.jaot.model.form.MemberListForm;
import jp.or.jaot.model.form.MemberLocalActivityForm;
import jp.or.jaot.model.form.MemberPasswordReissueForm;
import jp.or.jaot.model.form.MemberPasswordResettingForm;
import jp.or.jaot.model.form.MemberProfileUpdateForm;
import jp.or.jaot.model.form.MemberQualificationForm;
import jp.or.jaot.model.form.MemberRevokedAuthorityForm;
import jp.or.jaot.model.form.MemberRoleForm;
import jp.or.jaot.model.form.MembershipFeeDeliveryForm;
import jp.or.jaot.model.form.PaotMembershipFeeDeliveryForm;
import jp.or.jaot.model.form.RecessHistoryForm;
import jp.or.jaot.model.form.search.ApplicationProgressSearchForm;
import jp.or.jaot.model.form.search.ApplicationProgressSearchListForm;
import jp.or.jaot.model.form.search.MemberSearchListForm;
import jp.or.jaot.model.form.update.ApplicationProgressUpdateMultipleForm;
import jp.or.jaot.model.form.update.MemberEmailConfirmForm;
import jp.or.jaot.model.form.update.MemberEmailUpdateForm;
import jp.or.jaot.model.utils.GenericResponse;
import jp.or.jaot.service.ApplicationProgressService;
import jp.or.jaot.service.MemberEmailHistoryService;
import jp.or.jaot.service.MemberLocalActivityService;
import jp.or.jaot.service.MemberPasswordHistoryService;
import jp.or.jaot.service.MemberQualificationService;
import jp.or.jaot.service.MemberRevokedAuthorityService;
import jp.or.jaot.service.MemberRoleService;
import jp.or.jaot.service.MemberService;
import jp.or.jaot.service.MembershipFeeDeliveryService;
import jp.or.jaot.service.PaotMembershipFeeDeliveryService;
import lombok.AllArgsConstructor;

/**
 * ポータル会員情報コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "profile", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PortalProfileController extends BaseController {

	/** 会員自治体参画情報数(初期登録) */
	private final int DEF_INIT_ENTER_LOCAL_ACTIVITY_NUM = 16;

	/** 会員サービス */
	private final MemberService memberSrv;

	/** 会員ロールサービス */
	private final MemberRoleService memberRoleSrv;

	/** 会員権限サービス */
	private final MemberRevokedAuthorityService memberRevokedAuthoritySrv;

	/** 会員関連資格情報サービス */
	private final MemberQualificationService memberQualificationSrv;

	/** 会員自治体参画情報サービス */
	private final MemberLocalActivityService memberLocalActivitySrv;

	/** 県士会会費納入サービス */
	private final PaotMembershipFeeDeliveryService paotMembershipFeeDeliverySrv;

	/** 会費納入サービス */
	private final MembershipFeeDeliveryService membershipFeeDeliverySrv;

	/** 申請進捗サービス */
	private final ApplicationProgressService applicationProgressSrv;

	/**
	 * 会員パスワード履歴サービス
	 */
	private final MemberPasswordHistoryService memberPasswordHistorySrv;

	/** メールアドレス変更履歴サービス */
	private final MemberEmailHistoryService memberEmailHistoryService;

	/**
	 * ログイン情報
	 * @return 実行結果
	 */
	@PostMapping(path = "login_member")
	public ResponseEntity<MemberForm> loginMember(@AuthenticationPrincipal MemberEntity memberEntity) {
		return ResponseEntity.ok(createMemberForm(memberSrv.getSearchResult(memberEntity.getId()).getFirstEntity()));
	}

	/**
	 * 条件検索
	 * @return 実行結果
	 */
	@PostMapping(path = "search_member")
	public ResponseEntity<List<MemberForm>> searchMember(@RequestBody @Validated MemberForm memberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberSearchDto searchDto = new MemberSearchDto();
		BeanUtil.copyProperties(memberForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberForm> forms = memberSrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createMemberForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(会員リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_member")
	public ResponseEntity<MemberListForm> searchListMember(@RequestBody @Validated MemberSearchListForm memberForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberSearchDto searchDto = new MemberSearchDto();
		BeanUtil.copyProperties(memberForm, searchDto, false);

		// 検索結果フォーム取得
		MemberListForm form = new MemberListForm();
		form.setMembers(memberSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
				.map(E -> createMemberForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 利用可能状態取得
	 * @return 利用可能=true, 利用不可=false
	 */
	@PostMapping(path = "is_available")
	public ResponseEntity<Boolean> isAvailable(@AuthenticationPrincipal MemberEntity memberEntity,
			@RequestBody @Validated MemberIsAvailableForm memberIsAvailableForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		return ResponseEntity
				.ok(memberSrv.isAvailablePathOrKey(memberEntity.getMemberNo(), memberIsAvailableForm.getPathOrKey()));
	}

	/**
	 * 登録
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_member")
	public ResponseEntity<String> enterMember(@RequestBody @Validated MemberForm memberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		MemberDto enterDto = MemberDto.createSingle();
		MemberCompositeEntity enterCompositeEntity = enterDto.getFirstEntity();

		// 会員
		MemberEntity enterMemberEntity = enterCompositeEntity.getMemberEntity();
		BeanUtil.copyProperties(memberForm, enterMemberEntity, false);
		enterMemberEntity.setWebMemberPassword(CipherUtil.encrypt(memberForm.getWebMemberPassword())); // パスワード暗号化

		// 会員関連資格情報
		List<MemberQualificationEntity> enterQualificationEntities = new ArrayList<MemberQualificationEntity>();
		List<MemberQualificationForm> qualificationForms = memberForm.getMemberQualifications();
		if (!CollectionUtils.isEmpty(qualificationForms)) {
			for (MemberQualificationForm form : qualificationForms) {
				MemberQualificationEntity entity = new MemberQualificationEntity();
				BeanUtil.copyProperties(form, entity, true); // 更新値
				entity.setMemberNo(memberForm.getMemberNo());// 会員番号
				enterQualificationEntities.add(entity);
			}
		}
		enterCompositeEntity.setMemberQualificationEntities(enterQualificationEntities);

		// 会員自治体参画情報(固定件数をデフォルト値で追加)
		List<MemberLocalActivityEntity> enterLocalActivityEntities = new ArrayList<MemberLocalActivityEntity>();
		for (int seq = 1; seq <= DEF_INIT_ENTER_LOCAL_ACTIVITY_NUM; seq++) {
			MemberLocalActivityEntity entity = new MemberLocalActivityEntity();
			entity.setMemberNo(memberForm.getMemberNo());// 会員番号
			entity.setActivitySeq(seq); // 活動番号
			entity.setParticipation(false);// 参画フラグ(不参加)
			entity.setMunicipalityName(null); // 自治体名
			enterLocalActivityEntities.add(entity);
		}
		enterCompositeEntity.setMemberLocalActivityEntities(enterLocalActivityEntities);

		// 施設登録申請
		FacilityApplicationForm facilityForm = null;
		if ((facilityForm = memberForm.getFacilityApplication()) != null) {

			// エンティティ生成(複合)
			FacilityApplicationCompositeEntity enterFacilityCompositeEntity = new FacilityApplicationCompositeEntity();

			// 施設登録申請
			enterFacilityCompositeEntity.setFacilityApplicationEntity(
					BeanUtil.createCopyProperties(facilityForm, FacilityApplicationEntity.class, false));

			// 施設登録申請分類
			enterFacilityCompositeEntity.setFacilityApplicationCategoryEntities(
					facilityForm.getFacilityApplicationCategories().stream()
							.map(E -> BeanUtil.createCopyProperties(E, FacilityApplicationCategoryEntity.class, false))
							.collect(Collectors.toList()));

			// 設定
			enterCompositeEntity.setFacilityApplicationEntity(enterFacilityCompositeEntity);
		}

		// 実行
		memberSrv.enter(enterDto);

		return ResponseEntity.ok("enterMember ok");
	}

	/**
	 * 更新<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "update_member")
	public ResponseEntity<String> updateMember(@RequestBody @Validated MemberProfileUpdateForm memberProfileUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		MemberCompositeEntity compositeEntity = memberSrv.getSearchResult(memberProfileUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		MemberDto updateDto = MemberDto.createSingle();
		MemberCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils.isTrue(memberProfileUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// 会員
		MemberEntity updateMemberEntity = updateCompositeEntity.getMemberEntity();
		BeanUtil.copyProperties(compositeEntity.getMemberEntity(), updateMemberEntity, false); // 現在値
		if (isIncrementalUpdate) {
			BeanUtil.copyProperties(memberProfileUpdateForm, updateMemberEntity, true); // 更新値(差分)
		} else {
			BeanUtil.copyProperties(memberProfileUpdateForm, updateMemberEntity, false); // 更新値(上書)
		}
		updateCompositeEntity.setMemberEntity(updateMemberEntity);

		// 会員関連資格情報
		List<MemberQualificationEntity> updateQualificationEntities = new ArrayList<MemberQualificationEntity>();
		List<MemberQualificationEntity> qualificationEntities = compositeEntity.getMemberQualificationEntities();
		for (MemberQualificationForm form : memberProfileUpdateForm.getMemberQualifications()) {

			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(qualificationEntities)) {
				for (MemberQualificationEntity entity : qualificationEntities) {
					if (entity.getId().equals(form.getId())) {
						MemberQualificationEntity updateEntity = new MemberQualificationEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateQualificationEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				MemberQualificationEntity insertEntity = new MemberQualificationEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateQualificationEntities.add(insertEntity);
			}
		}

		// 会員関連資格情報(処理対象)
		updateCompositeEntity.setMemberQualificationEntities(updateQualificationEntities);

		// 会員自治体参画情報
		List<MemberLocalActivityEntity> updateLocalActivityEntities = new ArrayList<MemberLocalActivityEntity>();
		List<MemberLocalActivityEntity> localActivityEntities = compositeEntity.getMemberLocalActivityEntities();
		for (MemberLocalActivityForm form : memberProfileUpdateForm.getMemberLocalActivities()) {
			for (MemberLocalActivityEntity entity : localActivityEntities) {
				if (entity.getId().equals(form.getId())) {
					MemberLocalActivityEntity updateEntity = new MemberLocalActivityEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateLocalActivityEntities.add(updateEntity);
					break;
				}
			}
		}

		// 会員自治体参画情報(処理対象)
		updateCompositeEntity.setMemberLocalActivityEntities(updateLocalActivityEntities);

		// 施設登録申請
		FacilityApplicationCompositeEntity updateFacilityEntity = new FacilityApplicationCompositeEntity();
		FacilityApplicationCompositeEntity facilityEntity = compositeEntity.getFacilityApplicationEntity();
		FacilityApplicationForm facilityForm = null;
		if ((facilityForm = memberProfileUpdateForm.getFacilityApplication()) != null) {
			if (facilityEntity != null) {

				// 施設登録申請(更新)
				FacilityApplicationEntity updateEntity = updateFacilityEntity.getFacilityApplicationEntity();
				BeanUtil.copyProperties(facilityEntity.getFacilityApplicationEntity(), updateEntity, false); // 現在値
				BeanUtil.copyProperties(facilityForm, updateEntity, true); // 更新値(差分)
				updateFacilityEntity.setFacilityApplicationEntity(updateEntity);

				// 施設登録申請分類(更新)
				updateFacilityEntity.setFacilityApplicationCategoryEntities(getUpdateEntities(
						facilityForm.getFacilityApplicationCategories(),
						facilityEntity.getFacilityApplicationCategoryEntities(),
						FacilityApplicationCategoryEntity.class));

			} else {

				// 施設登録申請(追加)
				updateFacilityEntity.setFacilityApplicationEntity(
						BeanUtil.createCopyProperties(facilityForm, FacilityApplicationEntity.class, false));

				// 施設登録申請分類(追加)
				updateFacilityEntity.setFacilityApplicationCategoryEntities(facilityForm
						.getFacilityApplicationCategories().stream()
						.map(E -> BeanUtil.createCopyProperties(E, FacilityApplicationCategoryEntity.class, false))
						.collect(Collectors.toList()));
			}

			// 施設登録申請(処理対象)
			updateCompositeEntity.setFacilityApplicationEntity(updateFacilityEntity);
		}

		// 会員勤務先業務分類登録
		List<MemberFacilityCategoryEntity> updateMemberFacilityCategoryEntities = new ArrayList<>();
		List<MemberFacilityCategoryEntity> memberFacilityCategoryEntities = compositeEntity
				.getMemberFacilityCategoryEntities();

		for (MemberFacilityCategoryForm form : memberProfileUpdateForm.getMemberFacilityCategories()) {
			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(memberFacilityCategoryEntities)) {
				for (MemberFacilityCategoryEntity entity : memberFacilityCategoryEntities) {
					if (entity.getId().equals(form.getId())) {
						MemberFacilityCategoryEntity updateEntity = new MemberFacilityCategoryEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateMemberFacilityCategoryEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				MemberFacilityCategoryEntity insertEntity = new MemberFacilityCategoryEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateMemberFacilityCategoryEntities.add(insertEntity);
			}
		}
		// 会員勤務先業務分類登録(処理対象)
		updateCompositeEntity.setMemberFacilityCategoryEntities(updateMemberFacilityCategoryEntities);

		// 実行
		memberSrv.update(updateDto);

		return ResponseEntity.ok("updateMember ok");
	}

	/**
	 * 利用可能(会員)
	 * @return 実行結果
	 */
	@PostMapping(path = "enable_member")
	public ResponseEntity<String> enableMember(@RequestBody @Validated MemberForm memberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberSrv.setEnable(memberForm.getId(), true);

		return ResponseEntity.ok("enableMember ok");
	}

	/**
	 * 利用不可(会員)
	 * @return 実行結果
	 */
	@PostMapping(path = "disable_member")
	public ResponseEntity<String> disableMember(@RequestBody @Validated MemberForm memberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberSrv.setEnable(memberForm.getId(), false);

		return ResponseEntity.ok("disableMember ok");
	}

	/**
	 * 条件検索(会員ロール)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_member_role")
	public ResponseEntity<List<MemberRoleForm>> searchMemberRole(@RequestBody @Validated MemberRoleForm memberRoleForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberRoleSearchDto searchDto = new MemberRoleSearchDto();
		BeanUtil.copyProperties(memberRoleForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberRoleForm> forms = memberRoleSrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, MemberRoleForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(会員ロール)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_member_role")
	public ResponseEntity<String> enterMemberRole(@RequestBody @Validated MemberRoleForm memberRoleForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberRoleSrv.enter(copyDto(memberRoleForm, MemberRoleDto.createSingle()));

		return ResponseEntity.ok("enterMemberRole ok");
	}

	/**
	 * 条件検索(会員権限)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_member_revoked_authority")
	public ResponseEntity<List<MemberRevokedAuthorityForm>> searchMemberRevokedAuthority(
			@RequestBody @Validated MemberRevokedAuthorityForm memberRevokedAuthorityForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberRevokedAuthoritySearchDto searchDto = new MemberRevokedAuthoritySearchDto();
		BeanUtil.copyProperties(memberRevokedAuthorityForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberRevokedAuthorityForm> forms = memberRevokedAuthoritySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, MemberRevokedAuthorityForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(会員権限)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_member_revoked_authority")
	public ResponseEntity<String> enterMemberRevokedAuthority(
			@RequestBody @Validated MemberRevokedAuthorityForm memberRevokedAuthorityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberRevokedAuthoritySrv.enter(copyDto(memberRevokedAuthorityForm, MemberRevokedAuthorityDto.createSingle()));

		return ResponseEntity.ok("enterMemberRevokedAuthority ok");
	}

	/**
	 * 条件検索(会員関連資格情報)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_member_qualification")
	public ResponseEntity<List<MemberQualificationForm>> searchMemberQualification(
			@RequestBody @Validated MemberQualificationForm memberQualificationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberQualificationSearchDto searchDto = new MemberQualificationSearchDto();
		BeanUtil.copyProperties(memberQualificationForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberQualificationForm> forms = memberQualificationSrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, MemberQualificationForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(会員関連資格情報)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_member_qualification")
	public ResponseEntity<String> enterMemberQualification(
			@RequestBody @Validated MemberQualificationForm memberQualificationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberQualificationSrv.enter(copyDto(memberQualificationForm, MemberQualificationDto.createSingle()));

		return ResponseEntity.ok("enterMemberQualification ok");
	}

	/**
	 * 更新(会員関連資格情報)<br>
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * 値がNULLの場合は更新対象とならないため、明示的に文字列を初期化したい場合は空文字を指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_member_qualification")
	public ResponseEntity<String> updateMemberQualification(
			@RequestBody @Validated MemberQualificationForm memberQualificationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberQualificationSrv.update(copyDto(memberQualificationForm,
				memberQualificationSrv.getSearchResult(memberQualificationForm.getId()).getFirstEntity(),
				MemberQualificationDto.createSingle()));

		return ResponseEntity.ok("updateMemberQualification ok");
	}

	/**
	 * 条件検索(会員自治体参画情報)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_member_local_activitiy")
	public ResponseEntity<List<MemberLocalActivityForm>> searchMemberLocalActivity(
			@RequestBody @Validated MemberLocalActivityForm memberLocalActivityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberLocalActivitySearchDto searchDto = new MemberLocalActivitySearchDto();
		BeanUtil.copyProperties(memberLocalActivityForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberLocalActivityForm> forms = memberLocalActivitySrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, MemberLocalActivityForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(会員自治体参画情報)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_member_local_activitiy")
	public ResponseEntity<String> enterMemberLocalActivity(
			@RequestBody @Validated MemberLocalActivityForm memberLocalActivityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberLocalActivitySrv.enter(copyDto(memberLocalActivityForm, MemberLocalActivityDto.createSingle()));

		return ResponseEntity.ok("enterMemberLocalActivity ok");
	}

	/**
	 * 更新(会員自治体参画情報)<br>
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * 値がNULLの場合は更新対象とならないため、明示的に文字列を初期化したい場合は空文字を指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_member_local_activitiy")
	public ResponseEntity<String> updateMemberLocalActivity(
			@RequestBody @Validated MemberLocalActivityForm memberLocalActivityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		memberLocalActivitySrv.update(copyDto(memberLocalActivityForm,
				memberLocalActivitySrv.getSearchResult(memberLocalActivityForm.getId()).getFirstEntity(),
				MemberLocalActivityDto.createSingle()));

		return ResponseEntity.ok("updateMemberLocalActivity ok");
	}

	/**
	 * 条件検索(県士会会費納入)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_paot_membership_fee_delivery")
	public ResponseEntity<List<PaotMembershipFeeDeliveryForm>> searchPaotMembershipFeeDelivery(
			@RequestBody @Validated PaotMembershipFeeDeliveryForm paotMembershipFeeDeliveryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		PaotMembershipFeeDeliverySearchDto searchDto = new PaotMembershipFeeDeliverySearchDto();
		BeanUtil.copyProperties(paotMembershipFeeDeliveryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<PaotMembershipFeeDeliveryForm> forms = paotMembershipFeeDeliverySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, PaotMembershipFeeDeliveryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(会費納入)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_membership_fee_delivery")
	public ResponseEntity<List<MembershipFeeDeliveryForm>> searchMembershipFeeDelivery(
			@RequestBody @Validated MembershipFeeDeliveryForm membershipFeeDeliveryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MembershipFeeDeliverySearchDto searchDto = new MembershipFeeDeliverySearchDto();
		BeanUtil.copyProperties(membershipFeeDeliveryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MembershipFeeDeliveryForm> forms = membershipFeeDeliverySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, MembershipFeeDeliveryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(会費納入)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_membership_fee_delivery")
	public ResponseEntity<String> enterMembershipFeeDelivery(
			@RequestBody @Validated MembershipFeeDeliveryForm membershipFeeDeliveryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		membershipFeeDeliverySrv.enter(copyDto(membershipFeeDeliveryForm, MembershipFeeDeliveryDto.createSingle()));

		return ResponseEntity.ok("enterMembershipFeeDelivery ok");
	}

	/**
	 * フロントから接続用のapi
	 * @return 実行結果
	 */
	@PostMapping(path = "send_password_reissue_mail")
	public ResponseEntity<Integer> sendPasswordReissueMail(
			@RequestBody @Validated MemberPasswordReissueForm memberPasswordReissueForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// フロントから値を取得し、格納
		String memberCd = memberPasswordReissueForm.getMemberCd();
		String securityCd = memberPasswordReissueForm.getSecurityCd();
		Date birthday = memberPasswordReissueForm.getBirthDay();
		String emailAddress = memberPasswordReissueForm.getEmailAddress();

		// 値が格納されているかチェック
		logger.debug(memberCd);
		logger.debug(securityCd);
		logger.debug(birthday.toString());
		logger.debug(emailAddress);

		// 検索条件DTO作成
		MemberSearchDto searchDto = new MemberSearchDto();
		BeanUtil.copyProperties(memberPasswordReissueForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 取得開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得 (返り値は、"会員番号" or null)
		Integer confirmResoponse = memberSrv.passwordReissue(searchDto);

		// 確認用
		logger.debug("これはコントローラーの出力です。" + confirmResoponse);

		return ResponseEntity.ok(confirmResoponse);
	}

	/**
	 * フロントから接続用のapi
	 * @return 実行結果
	 */
	@PostMapping(path = "send_password_resetting_mail")
	public ResponseEntity<String> sendPasswordResettingMail(
			@RequestBody @Validated MemberPasswordResettingForm memberPasswordResettingForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// バリデーション
		String password = memberPasswordResettingForm.getPassword();
		String passwordChange = memberPasswordResettingForm.getRePassword();
		if (!password.contentEquals(passwordChange)) {
			return ResponseEntity.ok("パスワードが一致しません");
		} else {
			// 一時パスワードから会員を特定
			MemberPasswordHistorySearchDto memberPasswordHistorySearchDto = new MemberPasswordHistorySearchDto();
			BeanUtil.copyProperties(memberPasswordResettingForm, memberPasswordHistorySearchDto, false);
			memberPasswordHistorySearchDto.setMaxResult(1);
			memberPasswordHistorySearchDto.setOrders(new ArrayList<SearchOrder>() {
				{
					add(new SearchOrder(ORDER_ASC.DESC, "tempPasswordExpirationDatetime"));
				}
			});
			MemberPasswordHistoryDto memberPasswordHistory = memberPasswordHistorySrv
					.getSearchResultByCondition(memberPasswordHistorySearchDto);
			MemberPasswordHistoryEntity memberPasswordHistoryEntity = memberPasswordHistory.getFirstEntity();
			MemberDto memberDto = memberSrv.getSearchResultByMemberNo(memberPasswordHistoryEntity.getMemberNo());
			// 有効期限内かどうか
			if (memberPasswordHistoryEntity.getTempPasswordExpirationDatetime().before(new Date())) {
				return ResponseEntity.ok("期限切れです。");
			}
			MemberCompositeEntity memberCompositeEntity = memberDto.getFirstEntity();
			if (memberCompositeEntity == null) {
				return ResponseEntity.ok("更新に失敗しました");
			} else {
				// パスワードを変更
				ResponseEntity<String> response = updateMemberPassword(memberCompositeEntity, password);
				// メール送信
				memberSrv.sendPasswordResettingMail(memberCompositeEntity);
				return response;
			}
		}
	}

	/**
	 * 条件検索(申請進捗)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_application_progress")
	public ResponseEntity<List<ApplicationProgressForm>> searchApplicationProgress(
			@RequestBody @Validated ApplicationProgressForm applicationProgressForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ApplicationProgressSearchDto searchDto = new ApplicationProgressSearchDto();
		BeanUtil.copyProperties(applicationProgressForm, searchDto, false); // 属性値

		// 検索結果フォーム取得
		List<ApplicationProgressForm> forms = applicationProgressSrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, ApplicationProgressForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(申請進捗(自己))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_myself_application_progress")
	public ResponseEntity<List<ApplicationProgressForm>> searchMyselfApplicationProgress(
			@RequestBody @Validated ApplicationProgressSearchForm applicationProgressForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ApplicationProgressSearchDto searchDto = new ApplicationProgressSearchDto();
		BeanUtil.copyProperties(applicationProgressForm, searchDto, false); // 属性値

		// システム現在日時
		Timestamp publish = new Timestamp(System.currentTimeMillis());

		// 受付日
		if (applicationProgressForm.getApplyDateRangeDay() != null) {

			// 受付日範囲(From)
			searchDto.setApplyDateRangeFrom(
					DateUtil.getFromDate(DateUtil.getNowPastDate(-applicationProgressForm.getApplyDateRangeDay())));

			// 受付日範囲(To)
			searchDto.setApplyDateRangeTo(DateUtil.getToDate(new Date(publish.getTime())));
		}
		logger.debug("現在日 : {}, 範囲(日数) : {}, 受付日範囲(From) : {}, 受付日範囲(To) : {}",
				publish, applicationProgressForm.getApplyDateRangeDay(), searchDto.getApplyDateRangeFrom(),
				searchDto.getApplyDateRangeTo());

		// 検索結果フォーム取得
		List<ApplicationProgressForm> forms = applicationProgressSrv.getSearchResultMyselfByCondition(searchDto)
				.getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, ApplicationProgressForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(申請進捗リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_application_progress")
	public ResponseEntity<ApplicationProgressListForm> searchListApplicationProgress(
			@RequestBody @Validated ApplicationProgressSearchListForm applicationProgressForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ApplicationProgressSearchDto searchDto = new ApplicationProgressSearchDto();
		BeanUtil.copyProperties(applicationProgressForm, searchDto, false); // 属性値

		// 検索結果フォーム取得
		ApplicationProgressListForm form = new ApplicationProgressListForm();
		form.setApplicationProgresses(applicationProgressSrv.getSearchResultListByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, ApplicationProgressForm.class, false))
				.collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録(申請進捗)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_application_progress")
	public ResponseEntity<String> enterApplicationProgress(
			@RequestBody @Validated ApplicationProgressForm applicationProgressForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		applicationProgressSrv.enter(copyDto(applicationProgressForm, ApplicationProgressDto.createSingle()));

		return ResponseEntity.ok("enterApplicationProgress ok");
	}

	/**
	 * 更新(申請進捗)
	 * @return 実行結果
	 */
	@PostMapping(path = "update_application_progress")
	public ResponseEntity<String> updateApplicationProgress(
			@RequestBody @Validated ApplicationProgressForm applicationProgressForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		applicationProgressSrv.update(copyDto(applicationProgressForm,
				applicationProgressSrv.getSearchResult(applicationProgressForm.getId()).getFirstEntity(),
				ApplicationProgressDto.createSingle()));

		return ResponseEntity.ok("updateApplicationProgress ok");
	}

	/**
	 * 複数更新(申請進捗)
	 * @return 実行結果
	 */
	@PostMapping(path = "update_multiple_application_progress")
	public ResponseEntity<String> updateMultipleApplicationProgress(
			@RequestBody @Validated ApplicationProgressUpdateMultipleForm applicationProgressForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 一括設定値上書き(空文字の場合は値を初期化)
		if (applicationProgressForm.getStatusCd() != null) {
			applicationProgressForm.getApplicationProgresses().stream()
					.forEach(E -> E.setStatusCd(applicationProgressForm.getStatusCd())); // 状況
		}
		if (applicationProgressForm.getProgressCd() != null) {
			applicationProgressForm.getApplicationProgresses().stream()
					.forEach(E -> E.setProgressCd(applicationProgressForm.getProgressCd())); // 状態
		}

		// 実行
		applicationProgressSrv.update(ApplicationProgressDto.create()
				.setEntities(getUpdateEntities(applicationProgressForm.getApplicationProgresses(),
						applicationProgressSrv.getSearchResult(applicationProgressForm.getApplicationProgresses()
								.stream().map(F -> F.getId()).toArray(Long[]::new)).getEntities(),
						ApplicationProgressEntity.class)));

		return ResponseEntity.ok("updateMultipleApplicationProgress ok");
	}

	/**
	 * 会員フォーム生成
	 * @param compositeEntity 会員エンティティ(複合)
	 * @return 会員フォーム
	 */
	private MemberForm createMemberForm(MemberCompositeEntity compositeEntity) {

		// 会員フォーム
		MemberForm memberForm = BeanUtil.createCopyProperties(compositeEntity.getMemberEntity(), MemberForm.class,
				false);

		// 前回ログイン日時
		MemberLoginHistoryEntity lastLoginEntity = null;
		if ((lastLoginEntity = compositeEntity.getLastLoginMemberLoginHistoryEntity()) != null) {
			memberForm.setLastLoginDatetime(lastLoginEntity.getLoginDatetime());
		}

		// 前回ログアウト日時
		MemberLoginHistoryEntity lastLogoutEntity = null;
		if ((lastLogoutEntity = compositeEntity.getLastLogoutMemberLoginHistoryEntity()) != null) {
			memberForm.setLastLogoutDatetime(lastLogoutEntity.getLogoutDatetime());
		}

		// 会員関連資格情報
		List<MemberQualificationEntity> qualificationEntities = compositeEntity.getMemberQualificationEntities();
		if (!CollectionUtils.isEmpty(qualificationEntities)) {
			memberForm.setMemberQualifications(qualificationEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MemberQualificationForm.class, false))
					.collect(Collectors.toList()));
		}

		// 会員自治体参画情報
		List<MemberLocalActivityEntity> localActivityEntities = compositeEntity.getMemberLocalActivityEntities();
		if (!CollectionUtils.isEmpty(localActivityEntities)) {
			memberForm.setMemberLocalActivities(localActivityEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MemberLocalActivityForm.class, false))
					.collect(Collectors.toList()));
		}

		// 施設登録申請
		FacilityApplicationCompositeEntity facilityApplicationEntity = compositeEntity.getFacilityApplicationEntity();
		if (facilityApplicationEntity != null) {

			// 施設登録申請フォーム生成
			FacilityApplicationForm facilityApplicationForm = BeanUtil.createCopyProperties(
					facilityApplicationEntity.getFacilityApplicationEntity(), FacilityApplicationForm.class, false);

			// 施設登録申請分類フォーム生成
			facilityApplicationForm.setFacilityApplicationCategories(facilityApplicationEntity
					.getFacilityApplicationCategoryEntities()
					.stream().map(E -> BeanUtil.createCopyProperties(E, FacilityApplicationCategoryForm.class, false))
					.collect(Collectors.toList()));

			// 設定
			memberForm.setFacilityApplication(facilityApplicationForm);
		}

		// 施設養成校
		FacilityCompositeEntity facilityEntity = compositeEntity.getFacilityEntity();
		if (facilityEntity != null) {

			// 施設養成校フォーム生成
			FacilityForm facilityForm = BeanUtil.createCopyProperties(
					facilityEntity.getFacilityEntity(), FacilityForm.class, false);

			// 施設登録分類フォーム生成
			facilityForm.setFacilityCategories(facilityEntity
					.getFacilityCategoryEntities()
					.stream().map(E -> BeanUtil.createCopyProperties(E, FacilityCategoryForm.class, false))
					.collect(Collectors.toList()));

			// 設定
			memberForm.setWorkingFacility(facilityForm);
		}

		// 休会履歴
		RecessHistoryEntity recessHistoryEntity = compositeEntity.getRecessHistoryEntity();
		if (recessHistoryEntity != null) {

			// 休会履歴フォーム生成
			RecessHistoryForm recessHistoryForm = BeanUtil.createCopyProperties(
					recessHistoryEntity, RecessHistoryForm.class, false);

			// 設定
			memberForm.setRecessHistory(recessHistoryForm);
		}

		// 会員勤務先業務分類
		List<MemberFacilityCategoryEntity> memberFacilityCategoryEntities = compositeEntity
				.getMemberFacilityCategoryEntities();
		if (!CollectionUtils.isEmpty(memberFacilityCategoryEntities)) {
			memberForm.setMemberFacilityCategories(memberFacilityCategoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MemberFacilityCategoryForm.class, false))
					.collect(Collectors.toList()));
		}

		return memberForm;
	}

	/**
	 * パスワード変更
	 * @param memberEntity
	 * @param changePasswordForm
	 * @param errors
	 * @return メッセージ
	 */
	@PostMapping(path = "change_password")
	public GenericResponse changePassword(
			@AuthenticationPrincipal MemberEntity memberEntity,
			@RequestBody @Validated ChangePasswordForm changePasswordForm, Errors errors) {
		// バリデーション実行
		doValidation(errors);

		Long memberId = memberEntity.getId();
		MemberCompositeEntity memberCompositeEntity = memberSrv.getSearchResult(memberId).getFirstEntity();

		String currentPassword = changePasswordForm.getCurrentPassword();
		String password = changePasswordForm.getPassword();

		// パスワード認証(BCrypt)
		String decrypted = CipherUtil.decrypt(memberEntity.getPassword());
		if (!currentPassword.equals(decrypted)) {
			String msg = "パスワードが一致しません";
			return new GenericResponse(false, msg);
		} // パスワード（再入力）のチェック
		else if (!password.equals(changePasswordForm.getPasswordConfirm())) {
			String msg = "パスワードが一致しません";
			return new GenericResponse(false, msg);
		} // パスワード変更
		else {
			updateMemberPassword(memberCompositeEntity, password);
			return new GenericResponse(true);
		}
	}

	/**
	 * パスワード変更共通処理
	 * @param memberCompositeEntity
	 * @param password
	 * @return
	 */
	private ResponseEntity<String> updateMemberPassword(
			MemberCompositeEntity memberCompositeEntity, String password) {
		MemberDto memberDto = MemberDto.createSingle();
		MemberCompositeEntity updateCompositeEntity = memberDto.getFirstEntity();
		BeanUtil.copyProperties(memberCompositeEntity, updateCompositeEntity, false);
		updateCompositeEntity.getMemberEntity().setWebMemberPassword(CipherUtil.encrypt(password));
		memberSrv.update(memberDto);
		return ResponseEntity.ok(null);
	}

	/**
	 * メールアドレス変更
	 * @param memberEntity
	 * @param memberEmailUpdateForm
	 * @param errors
	 * @return メッセージ
	 */
	@PostMapping(path = "update_email")
	public ResponseEntity<String> updateEmail(
			@AuthenticationPrincipal final MemberEntity memberEntity,
			@RequestBody @Validated final MemberEmailUpdateForm memberEmailUpdateForm, final Errors errors) {
		// バリデーション実行
		doValidation(errors);
		final Integer memberNo = memberEntity.getMemberNo();
		final String email = memberEmailUpdateForm.getEmail();
		final String emailConfirm = memberEmailUpdateForm.getEmailConfirm();

		// メールアドレスが入力されいるかチェック
		if (email == null || emailConfirm == null) {
			final String msg = "メールアドレスが入力されていません。";
			return ResponseEntity.ok(msg);
		} // メールアドレス（再入力）のチェック
		else if (!email.equals(emailConfirm)) {
			final String msg = "メールアドレスが一致しません。";
			return ResponseEntity.ok(msg);
		} // メール送信
		else {
			final String key = CipherUtil.encrypt(memberNo.toString());

			final MemberEmailHistoryDto memberEmailHistoryDto = MemberEmailHistoryDto.createSingle();

			final MemberEmailHistoryEntity memberEmailHistoryEntity = memberEmailHistoryDto.getFirstEntity();;
			memberEmailHistoryEntity.setEmailAddress(email);
			memberEmailHistoryEntity.setMemberNo(memberEntity.getMemberNo());
			memberEmailHistoryEntity.setTempKey(key);
			final Calendar calendar = Calendar.getInstance();
					calendar.setTime(new Date());
					calendar.add(Calendar.HOUR_OF_DAY, 24);
			memberEmailHistoryEntity.setTempExpirationDatetime(calendar.getTime());
			memberEmailHistoryEntity.setTempFlag(true);

			// 有効な仮メールアドレスは1件のみにしたいので過去のもののtmp_flagをfalseにする
            final MemberEmailHistorySearchDto searchDto = new MemberEmailHistorySearchDto();

            // 検索条件
            searchDto.setStartPosition(0);
            searchDto.setMaxResult(0); // 全件
            searchDto.setMemberNo(memberNo);
            searchDto.setTempFlag(true);

            var oldMemberEmailHistoryDto = memberEmailHistoryService.getSearchResultByCondition(searchDto);
            for (MemberEmailHistoryEntity e : oldMemberEmailHistoryDto.getEntities()) {
                e.setTempFlag(false);
            }

            final MemberCompositeEntity memberCompositeEntity = memberSrv.getSearchResult(memberEntity.getId()).getFirstEntity();

			try{
				memberSrv.sendEmailConfirmMail(memberCompositeEntity,memberEmailHistoryEntity);

				memberEmailHistoryService.enter(memberEmailHistoryDto); // 新しい仮メールアドレス登録
				memberEmailHistoryService.update(oldMemberEmailHistoryDto); // 古い仮メールアドレスを無効に

				return ResponseEntity.ok("メールが送信されたっぽい");
			} catch (MailSystemException e) {

				return ResponseEntity.ok(e.getMessage());
			}
		}
	}

	/**
	 * メールアドレス変更承認
	 * @param memberEntity
	 * @param memberEmailConfirmForm
	 * @param errors
	 * @return メッセージ
	 */
	@PostMapping(path = "confirm_email")
	public ResponseEntity<String> confirmEmail(
			@AuthenticationPrincipal MemberEntity memberEntity,
			@RequestBody @Validated MemberEmailConfirmForm memberEmailConfirmForm,
			Errors errors) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.HOUR_OF_DAY, -24);

		MemberEmailHistorySearchDto memberEmailHistorySearchDto = new MemberEmailHistorySearchDto();
		memberEmailHistorySearchDto.setMemberNo(memberEntity.getMemberNo());
		memberEmailHistorySearchDto.setTempFlag(true);
		memberEmailHistorySearchDto.setTempKey(memberEmailConfirmForm.getKey());
		memberEmailHistorySearchDto.setTempExpirationDatetime(calendar.getTime());

		var emailAddress = memberEmailHistoryService.getSearchResultByCondition(memberEmailHistorySearchDto).getFirstEntity().getEmailAddress();

		Long memberId = memberEntity.getId();
		MemberCompositeEntity memberCompositeEntity = memberSrv.getSearchResult(memberId).getFirstEntity();

		MemberDto memberDto = MemberDto.createSingle();
			MemberCompositeEntity updateCompositeEntity = memberDto.getFirstEntity();
			BeanUtil.copyProperties(memberCompositeEntity, updateCompositeEntity, false);
			updateCompositeEntity.getMemberEntity().setEmailAddress(emailAddress);
			memberSrv.update(memberDto);
		return ResponseEntity.ok(null);
	}
}

// TODO
