package jp.or.jaot.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.AuthorPresenterActualFieldDto;
import jp.or.jaot.model.dto.AuthorPresenterActualResultDto;
import jp.or.jaot.model.dto.SocialContributionActual2FieldDto;
import jp.or.jaot.model.dto.SocialContributionActualFieldDto;
import jp.or.jaot.model.dto.SocialContributionActualResultDto;
import jp.or.jaot.model.dto.search.AuthorPresenterActualFieldSearchDto;
import jp.or.jaot.model.dto.search.AuthorPresenterActualResultSearchDto;
import jp.or.jaot.model.dto.search.SocialContributionActual2FieldSearchDto;
import jp.or.jaot.model.dto.search.SocialContributionActualFieldSearchDto;
import jp.or.jaot.model.dto.search.SocialContributionActualResultSearchDto;
import jp.or.jaot.model.entity.AuthorPresenterActualFieldEntity;
import jp.or.jaot.model.entity.AuthorPresenterActualResultEntity;
import jp.or.jaot.model.entity.SocialContributionActual2FieldEntity;
import jp.or.jaot.model.entity.SocialContributionActualFieldEntity;
import jp.or.jaot.model.entity.SocialContributionActualResultEntity;
import jp.or.jaot.model.entity.composite.AuthorPresenterActualResultCompositeEntity;
import jp.or.jaot.model.entity.composite.SocialContributionActualResultCompositeEntity;
import jp.or.jaot.model.form.AuthorPresenterActualFieldForm;
import jp.or.jaot.model.form.AuthorPresenterActualResultForm;
import jp.or.jaot.model.form.SocialContributionActual2FieldForm;
import jp.or.jaot.model.form.SocialContributionActualFieldForm;
import jp.or.jaot.model.form.SocialContributionActualResultForm;
import jp.or.jaot.service.AuthorPresenterActualFieldService;
import jp.or.jaot.service.AuthorPresenterActualResultService;
import jp.or.jaot.service.SocialContributionActual2FieldService;
import jp.or.jaot.service.SocialContributionActualFieldService;
import jp.or.jaot.service.SocialContributionActualResultService;
import lombok.AllArgsConstructor;

/**
 * 実績コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "performance", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class PortalPerformanceController extends BaseController {

	/** 活動実績の領域(後輩育成・社会貢献)サービス */
	private final SocialContributionActualFieldService socialContributionActualFieldSrv;

	/** 活動実績の領域2(後輩育成・社会貢献)サービス */
	private final SocialContributionActual2FieldService socialContributionActual2FieldSrv;

	/** 活動実績(後輩育成・社会貢献)サービス */
	private final SocialContributionActualResultService socialContributionActualResultSrv;

	/** 活動実績の領域(発表・著作等)サービス */
	private final AuthorPresenterActualFieldService authorPresenterActualFieldSrv;

	/** 活動実績(発表・著作等)サービス */
	private final AuthorPresenterActualResultService authorPresenterActualResultSrv;

	/**
	 * 条件検索(活動実績の領域(後輩育成・社会貢献))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_social_contribution_actual_field")
	public ResponseEntity<List<SocialContributionActualFieldForm>> searchSocialContributionActualField(
			@RequestBody @Validated SocialContributionActualFieldForm socialContributionActualFieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		SocialContributionActualFieldSearchDto searchDto = new SocialContributionActualFieldSearchDto();
		BeanUtil.copyProperties(socialContributionActualFieldForm, searchDto, false); // 属性値
		searchDto.setResultIds(new Long[] { socialContributionActualFieldForm.getResultId() }); // 活動実績ID
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<SocialContributionActualFieldForm> forms = socialContributionActualFieldSrv
				.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, SocialContributionActualFieldForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(活動実績の領域(後輩育成・社会貢献))
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_social_contribution_actual_field")
	public ResponseEntity<String> enterSocialContributionActualField(
			@RequestBody @Validated SocialContributionActualFieldForm socialContributionActualFieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		socialContributionActualFieldSrv
				.enter(copyDto(socialContributionActualFieldForm, SocialContributionActualFieldDto.createSingle()));

		return ResponseEntity.ok("enterSocialContributionActualField ok");
	}

	/**
	 * 更新(活動実績の領域(後輩育成・社会貢献))<br>
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * 値がNULLの場合は更新対象とならないため、明示的に文字列を初期化したい場合は空文字を指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_social_contribution_actual_field")
	public ResponseEntity<String> updateSocialContributionActualField(
			@RequestBody @Validated SocialContributionActualFieldForm socialContributionActualFieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		socialContributionActualFieldSrv.update(copyDto(
				socialContributionActualFieldForm, socialContributionActualFieldSrv
						.getSearchResult(socialContributionActualFieldForm.getId()).getFirstEntity(),
				SocialContributionActualFieldDto.createSingle()));

		return ResponseEntity.ok("updateSocialContributionActualField ok");
	}

	/**
	 * 条件検索(活動実績の領域2(後輩育成・社会貢献))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_social_contribution_actual2_field")
	public ResponseEntity<List<SocialContributionActual2FieldForm>> searchSocialContributionActual2Field(
			@RequestBody @Validated SocialContributionActual2FieldForm socialContributionActual2FieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		SocialContributionActual2FieldSearchDto searchDto = new SocialContributionActual2FieldSearchDto();
		BeanUtil.copyProperties(socialContributionActual2FieldForm, searchDto, false); // 属性値
		searchDto.setResultIds(new Long[] { socialContributionActual2FieldForm.getResultId() }); // 活動実績ID
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<SocialContributionActual2FieldForm> forms = socialContributionActual2FieldSrv
				.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, SocialContributionActual2FieldForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(活動実績の領域2(後輩育成・社会貢献))
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_social_contribution_actual2_field")
	public ResponseEntity<String> enterSocialContributionActual2Field(
			@RequestBody @Validated SocialContributionActual2FieldForm socialContributionActual2FieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		socialContributionActual2FieldSrv
				.enter(copyDto(socialContributionActual2FieldForm, SocialContributionActual2FieldDto.createSingle()));

		return ResponseEntity.ok("enterSocialContributionActual2Field ok");
	}

	/**
	 * 更新(活動実績の領域2(後輩育成・社会貢献))<br>
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * 値がNULLの場合は更新対象とならないため、明示的に文字列を初期化したい場合は空文字を指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_social_contribution_actual2_field")
	public ResponseEntity<String> updateSocialContributionActual2Field(
			@RequestBody @Validated SocialContributionActual2FieldForm socialContributionActual2FieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		socialContributionActual2FieldSrv.update(copyDto(
				socialContributionActual2FieldForm, socialContributionActual2FieldSrv
						.getSearchResult(socialContributionActual2FieldForm.getId()).getFirstEntity(),
				SocialContributionActual2FieldDto.createSingle()));

		return ResponseEntity.ok("updateSocialContributionActual2Field ok");
	}

	/**
	 * 条件検索(活動実績(後輩育成・社会貢献))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_social_contribution_actual_result")
	public ResponseEntity<List<SocialContributionActualResultForm>> searchSocialContributionActualResult(
			@RequestBody @Validated SocialContributionActualResultForm socialContributionActualResultForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		SocialContributionActualResultSearchDto searchDto = new SocialContributionActualResultSearchDto();
		BeanUtil.copyProperties(socialContributionActualResultForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<SocialContributionActualResultForm> forms = socialContributionActualResultSrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createSocialContributionActualResultForm(E))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(活動実績(後輩育成・社会貢献))
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_social_contribution_actual_result")
	public ResponseEntity<String> enterSocialContributionActualResult(
			@RequestBody @Validated SocialContributionActualResultForm socialContributionActualResultForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		SocialContributionActualResultDto enterDto = SocialContributionActualResultDto.createSingle();
		SocialContributionActualResultCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 活動実績(後輩育成・社会貢献)
		SocialContributionActualResultEntity enterResultEntity = compositeEntity
				.getSocialContributionActualResultEntity();
		BeanUtil.copyProperties(socialContributionActualResultForm, enterResultEntity, false);

		// 活動実績の領域(後輩育成・社会貢献)
		compositeEntity.setSocialContributionActualFieldEntities(
				getEnterEntities(socialContributionActualResultForm.getSocialContributionActualFields(),
						SocialContributionActualFieldEntity.class));

		// 活動実績の領域2(後輩育成・社会貢献)
		compositeEntity.setSocialContributionActual2FieldEntities(
				getEnterEntities(socialContributionActualResultForm.getSocialContributionActual2Fields(),
						SocialContributionActual2FieldEntity.class));

		// 実行
		socialContributionActualResultSrv.enter(enterDto);

		return ResponseEntity.ok("enterSocialContributionActualResult ok");
	}

	/**
	 * 更新(活動実績(後輩育成・社会貢献))
	 * @return 実行結果
	 */
	@PostMapping(path = "update_social_contribution_actual_result")
	public ResponseEntity<String> updateSocialContributionActualResult(
			@RequestBody @Validated SocialContributionActualResultForm socialContributionActualResultForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		SocialContributionActualResultCompositeEntity compositeEntity = socialContributionActualResultSrv
				.getSearchResult(socialContributionActualResultForm.getId()).getFirstEntity();

		// 更新対象DTO生成
		SocialContributionActualResultDto updateDto = SocialContributionActualResultDto.createSingle();
		SocialContributionActualResultCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 活動実績(後輩育成・社会貢献)
		SocialContributionActualResultEntity updateResultEntity = updateCompositeEntity
				.getSocialContributionActualResultEntity();
		BeanUtil.copyProperties(compositeEntity.getSocialContributionActualResultEntity(), updateResultEntity, false); // 現在値
		BeanUtil.copyProperties(socialContributionActualResultForm, updateResultEntity, true); // 更新値(差分)

		// 活動実績の領域(後輩育成・社会貢献)
		updateCompositeEntity.setSocialContributionActualFieldEntities(
				getUpdateEntities(socialContributionActualResultForm.getSocialContributionActualFields(),
						compositeEntity.getSocialContributionActualFieldEntities(),
						SocialContributionActualFieldEntity.class));

		// 活動実績の領域2(後輩育成・社会貢献)
		updateCompositeEntity.setSocialContributionActual2FieldEntities(
				getUpdateEntities(socialContributionActualResultForm.getSocialContributionActual2Fields(),
						compositeEntity.getSocialContributionActual2FieldEntities(),
						SocialContributionActual2FieldEntity.class));

		// 実行
		socialContributionActualResultSrv.update(updateDto);

		return ResponseEntity.ok("updateSocialContributionActualResult ok");
	}

	/**
	 * 条件検索(活動実績の領域(発表・著作等))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_author_presenter_actual_field")
	public ResponseEntity<List<AuthorPresenterActualFieldForm>> searchAuthorPresenterActualField(
			@RequestBody @Validated AuthorPresenterActualFieldForm authorPresenterActualFieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		AuthorPresenterActualFieldSearchDto searchDto = new AuthorPresenterActualFieldSearchDto();
		BeanUtil.copyProperties(authorPresenterActualFieldForm, searchDto, false); // 属性値
		searchDto.setResultIds(new Long[] { authorPresenterActualFieldForm.getResultId() }); // 活動実績ID
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<AuthorPresenterActualFieldForm> forms = authorPresenterActualFieldSrv
				.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, AuthorPresenterActualFieldForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(活動実績の領域(発表・著作等))
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_author_presenter_actual_field")
	public ResponseEntity<String> enterAuthorPresenterActualField(
			@RequestBody @Validated AuthorPresenterActualFieldForm authorPresenterActualFieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		authorPresenterActualFieldSrv
				.enter(copyDto(authorPresenterActualFieldForm, AuthorPresenterActualFieldDto.createSingle()));

		return ResponseEntity.ok("enterAuthorPresenterActualField ok");
	}

	/**
	 * 更新(活動実績の領域(発表・著作等))<br>
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * 値がNULLの場合は更新対象とならないため、明示的に文字列を初期化したい場合は空文字を指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_author_presenter_actual_field")
	public ResponseEntity<String> updateAuthorPresenterActualField(
			@RequestBody @Validated AuthorPresenterActualFieldForm authorPresenterActualFieldForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		authorPresenterActualFieldSrv.update(copyDto(authorPresenterActualFieldForm,
				authorPresenterActualFieldSrv.getSearchResult(authorPresenterActualFieldForm.getId()).getFirstEntity(),
				AuthorPresenterActualFieldDto.createSingle()));

		return ResponseEntity.ok("updateAuthorPresenterActualField ok");
	}

	/**
	 * 条件検索(活動実績(発表・著作等))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_author_presenter_actual_result")
	public ResponseEntity<List<AuthorPresenterActualResultForm>> searchAuthorPresenterActualResult(
			@RequestBody @Validated AuthorPresenterActualResultForm authorPresenterActualResultForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		AuthorPresenterActualResultSearchDto searchDto = new AuthorPresenterActualResultSearchDto();
		BeanUtil.copyProperties(authorPresenterActualResultForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<AuthorPresenterActualResultForm> forms = authorPresenterActualResultSrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createAuthorPresenterActualResultForm(E))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(活動実績(発表・著作等))
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_author_presenter_actual_result")
	public ResponseEntity<String> enterAuthorPresenterActualResult(
			@RequestBody @Validated AuthorPresenterActualResultForm authorPresenterActualResultForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		AuthorPresenterActualResultDto enterDto = AuthorPresenterActualResultDto.createSingle();
		AuthorPresenterActualResultCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 活動実績(発表・著作等)
		AuthorPresenterActualResultEntity enterResultEntity = compositeEntity.getAuthorPresenterActualResultEntity();
		BeanUtil.copyProperties(authorPresenterActualResultForm, enterResultEntity, false);

		// 活動実績の領域(発表・著作等)
		compositeEntity.setAuthorPresenterActualFieldEntities(
				getEnterEntities(authorPresenterActualResultForm.getAuthorPresenterActualFields(),
						AuthorPresenterActualFieldEntity.class));

		// 実行
		authorPresenterActualResultSrv.enter(enterDto);

		return ResponseEntity.ok("enterAuthorPresenterActualResult ok");
	}

	/**
	 * 更新(活動実績(発表・著作等))
	 * @return 実行結果
	 */
	@PostMapping(path = "update_author_presenter_actual_result")
	public ResponseEntity<String> updateAuthorPresenterActualResult(
			@RequestBody @Validated AuthorPresenterActualResultForm authorPresenterActualResultForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		AuthorPresenterActualResultCompositeEntity compositeEntity = authorPresenterActualResultSrv
				.getSearchResult(authorPresenterActualResultForm.getId()).getFirstEntity();

		// 更新対象DTO生成
		AuthorPresenterActualResultDto updateDto = AuthorPresenterActualResultDto.createSingle();
		AuthorPresenterActualResultCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 活動実績(発表・著作等)
		AuthorPresenterActualResultEntity updateResultEntity = updateCompositeEntity
				.getAuthorPresenterActualResultEntity();
		BeanUtil.copyProperties(compositeEntity.getAuthorPresenterActualResultEntity(), updateResultEntity, false); // 現在値
		BeanUtil.copyProperties(authorPresenterActualResultForm, updateResultEntity, true); // 更新値(差分)

		// 活動実績の領域(発表・著作等)
		updateCompositeEntity.setAuthorPresenterActualFieldEntities(getUpdateEntities(
				authorPresenterActualResultForm.getAuthorPresenterActualFields(),
				compositeEntity.getAuthorPresenterActualFieldEntities(), AuthorPresenterActualFieldEntity.class));

		// 実行
		authorPresenterActualResultSrv.update(updateDto);

		return ResponseEntity.ok("updateAuthorPresenterActualResult ok");
	}

	/**
	 * 活動実績(後輩育成・社会貢献)フォーム生成
	 * @param compositeEntity 活動実績(後輩育成・社会貢献)エンティティ(複合)
	 * @return 活動実績(後輩育成・社会貢献)フォーム
	 */
	private SocialContributionActualResultForm createSocialContributionActualResultForm(
			SocialContributionActualResultCompositeEntity compositeEntity) {

		// 活動実績(後輩育成・社会貢献)
		SocialContributionActualResultForm resultForm = BeanUtil.createCopyProperties(
				compositeEntity.getSocialContributionActualResultEntity(), SocialContributionActualResultForm.class,
				false);

		// 活動実績の領域(後輩育成・社会貢献)
		List<SocialContributionActualFieldEntity> actualFieldEntities = compositeEntity
				.getSocialContributionActualFieldEntities();
		if (!CollectionUtils.isEmpty(actualFieldEntities)) {
			resultForm.setSocialContributionActualFields(actualFieldEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, SocialContributionActualFieldForm.class, false))
					.collect(Collectors.toList()));
		}

		// 活動実績の領域2(後輩育成・社会貢献)
		List<SocialContributionActual2FieldEntity> actual2FieldEntities = compositeEntity
				.getSocialContributionActual2FieldEntities();
		if (!CollectionUtils.isEmpty(actual2FieldEntities)) {
			resultForm.setSocialContributionActual2Fields(actual2FieldEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, SocialContributionActual2FieldForm.class, false))
					.collect(Collectors.toList()));
		}

		return resultForm;
	}

	/**
	 * 活動実績(発表・著作等)フォーム生成
	 * @param compositeEntity 活動実績(発表・著作等)エンティティ(複合)
	 * @return 活動実績(発表・著作等)フォーム
	 */
	private AuthorPresenterActualResultForm createAuthorPresenterActualResultForm(
			AuthorPresenterActualResultCompositeEntity compositeEntity) {

		// 活動実績(発表・著作等)
		AuthorPresenterActualResultForm resultForm = BeanUtil.createCopyProperties(
				compositeEntity.getAuthorPresenterActualResultEntity(), AuthorPresenterActualResultForm.class, false);

		// 活動実績の領域(発表・著作等)
		List<AuthorPresenterActualFieldEntity> actualFieldEntities = compositeEntity
				.getAuthorPresenterActualFieldEntities();
		if (!CollectionUtils.isEmpty(actualFieldEntities)) {
			resultForm.setAuthorPresenterActualFields(actualFieldEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, AuthorPresenterActualFieldForm.class, false))
					.collect(Collectors.toList()));
		}

		return resultForm;
	}

	// TODO
}
