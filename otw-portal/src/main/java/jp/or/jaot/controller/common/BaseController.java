package jp.or.jaot.controller.common;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import jp.or.jaot.core.exception.system.GeneralSystemException;
import jp.or.jaot.core.exception.validation.FormValidationException;
import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.core.model.BaseEntity;
import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ReflectionUtil;

/**
 * 基底コントローラクラス
 */
public class BaseController {

	/** ロガーインスタンス */
	protected final Logger logger;

	/** 型不一致メッセージ */
	@Autowired
	private TypeMismatchMessagesConfig messagesConfig;

	/**
	 * コンストラクタ
	 */
	public BaseController() {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * バリデーション実行
	 * @param errors エラー
	 * @throws FormValidationException フォームバリデーション例外
	 */
	protected void doValidation(Errors errors) {

		// エラーチェック
		if (!errors.hasErrors()) {
			return;
		}

		// エラーマップ生成(1つのキーに複数の値を紐付け)
		MultiValueMap<String, String> errorMap = new LinkedMultiValueMap<>();
		for (ObjectError error : errors.getGlobalErrors()) {
			String[] codes = error.getCodes();
			String code = (codes == null || codes.length < 3) ? ("") : codes[2];
			String value = messagesConfig.getMessage(code, error.getDefaultMessage());
			errorMap.add("global", value);
		}
		for (FieldError error : errors.getFieldErrors()) {
			String[] codes = error.getCodes();
			String code = (codes == null || codes.length < 3) ? ("") : codes[2];
			String value = messagesConfig.getMessage(code, error.getDefaultMessage());
			errorMap.add(error.getField(), value);
		}

		// フォームバリデーション例外
		ERROR_CAUSE errorCause = ERROR_CAUSE.FORM_BINDING;
		String detail = "フォームをバインドできませんでした";
		throw new FormValidationException(errorCause, detail, errors.toString(), errorMap);
	}

	/**
	 * 配列生成(Long)
	 * @param value 値
	 * @return Long配列(値がnullの場合はnullを返却)
	 */
	protected Long[] createArray(Long value) {
		return (value == null) ? (null) : (new Long[] { value });
	}

	/**
	 * 配列生成(Integer)
	 * @param value 値
	 * @return Integer配列(値がnullの場合はnullを返却)
	 */
	protected Integer[] createArray(Integer value) {
		return (value == null) ? (null) : (new Integer[] { value });
	}

	/**
	 * 配列生成(String)
	 * @param value 値
	 * @return String配列(値がnullの場合はnullを返却)
	 */
	protected String[] createArray(String value) {
		return (value == null) ? (null) : (new String[] { value });
	}

	/**
	 * 属性コピー(DTO)
	 * @param form 属性コピー元のフォーム
	 * @param dto 属性コピー先のDTO
	 * @return DTO
	 */
	@SuppressWarnings("rawtypes")
	protected <T> T copyDto(BaseForm form, T dto) {
		BaseEntity copyEntity = (BaseEntity) ((BaseDto) dto).getFirstEntity();
		BeanUtil.copyProperties(form, copyEntity, false);
		return dto;
	}

	/**
	 * 属性コピー(DTO)
	 * @param form 属性コピー元のフォーム
	 * @param entity 属性コピー元のエンティティ
	 * @param dto 属性コピー先のDTO
	 * @return DTO
	 */
	@SuppressWarnings("rawtypes")
	protected <T> T copyDto(BaseForm form, BaseEntity entity, T dto) {
		BaseEntity copyEntity = (BaseEntity) ((BaseDto) dto).getFirstEntity();
		BeanUtil.copyProperties(entity, copyEntity, false);
		BeanUtil.copyProperties(form, copyEntity, true); // NULL値除外
		return dto;
	}

	/**
	 * 属性コピー(DTO)
	 * @param form 属性コピー元のフォーム
	 * @param initProperties 初期化対象プロパティリスト
	 * @param entity 属性コピー元のエンティティ
	 * @param dto 属性コピー先のDTO
	 * @return DTO
	 */
	@SuppressWarnings("rawtypes")
	protected <T> T copyDto(BaseForm form, List<String> initProperties, BaseEntity entity, T dto) {
		BaseEntity copyEntity = (BaseEntity) ((BaseDto) dto).getFirstEntity();
		BeanUtil.copyProperties(entity, copyEntity, false);
		BeanUtil.copyProperties(form, copyEntity, true, initProperties.toArray(new String[0])); // NULL値除外
		return dto;
	}

	/**
	 * 登録エンティティリスト取得
	 * @param <F> フォーム
	 * @param <E> エンティティ
	 * @param forms フォームリスト
	 * @param entityClazz エンティティクラス名
	 * @return 登録エンティティリスト
	 */
	protected <F, E> List<E> getEnterEntities(List<F> forms, Class<E> entityClazz) {
		List<E> enterEntities = new ArrayList<E>();
		try {
			for (F form : forms) {
				E entity = entityClazz.getDeclaredConstructor().newInstance();
				BeanUtil.copyProperties(form, entity, true); // 更新値
				enterEntities.add(entity);
			}
		} catch (Exception e) {
			throw new GeneralSystemException(e);
		}

		return enterEntities;
	}

	/**
	 * 更新エンティティリスト取得
	 * @param <F> フォーム
	 * @param <E> エンティティ
	 * @param forms フォームリスト
	 * @param entities エンティティリスト
	 * @param entityClazz エンティティクラス名
	 * @return 更新エンティティリスト
	 */
	@SuppressWarnings("unchecked")
	protected <F, E> List<E> getUpdateEntities(List<F> forms, List<E> entities, Class<E> entityClazz) {
		List<E> updateEntities = new ArrayList<E>();
		Field field = null;
		List<String> formInitProperties = null;
		try {
			for (F form : forms) {

				// フィールドリスト取得(フォーム)
				List<Field> formFields = ReflectionUtil.getInheritedPrivateFields(form.getClass());
				AccessibleObject.setAccessible(formFields.toArray(new AccessibleObject[0]), true);

				// ID取得(フォーム)
				Long formId = null;
				if ((field = formFields.stream().filter(F -> F.getName().equals("id")).findFirst()
						.orElse(null)) != null) {
					formId = (Long) field.get(form);
				}

				// 初期化対象プロパティ取得(フォーム)
				String[] initProperties = null;
				if ((field = formFields.stream().filter(F -> F.getName().equals("initProperties")).findFirst()
						.orElse(null)) != null) {
					if ((formInitProperties = (List<String>) field.get(form)) != null) {
						initProperties = formInitProperties.toArray(new String[0]);
					}
				}

				// 追加
				if (formId == null) {
					E updateEntity = entityClazz.getDeclaredConstructor().newInstance();
					BeanUtil.copyProperties(form, updateEntity, false); // 追加値
					updateEntities.add(updateEntity);
				} else {
					for (E entity : entities) {

						// ID取得(エンティティ)
						Field fieldEntity = entity.getClass().getDeclaredField("id");
						fieldEntity.setAccessible(true);
						Long entityId = (Long) fieldEntity.get(entity);

						// 更新 or 削除
						if (formId.equals(entityId)) {
							E updateEntity = entityClazz.getDeclaredConstructor().newInstance();
							BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
							BeanUtil.copyProperties(form, updateEntity, true, initProperties); // 更新値(差分)
							updateEntities.add(updateEntity);
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			throw new GeneralSystemException(e);
		}

		return updateEntities;
	}
}
