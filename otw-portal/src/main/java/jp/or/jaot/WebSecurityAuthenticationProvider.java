package jp.or.jaot;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import jp.or.jaot.core.util.CipherUtil;
import jp.or.jaot.model.dto.MemberLoginHistoryDto;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberLoginHistoryEntity;
import jp.or.jaot.service.MemberLoginHistoryService;
import jp.or.jaot.service.MemberUserDetailsService;

/**
 * 認証プロバイダ
 */
@Configuration
public class WebSecurityAuthenticationProvider implements AuthenticationProvider {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(WebSecurityAuthenticationProvider.class);

	/** 会員サービス(詳細) */
	@Autowired
	private MemberUserDetailsService memberUserDetailsSrv;

	/** 会員ポータルログイン履歴サービス */
	@Autowired
	private MemberLoginHistoryService memberLoginHistoryService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		// ログイン情報取得
		String username = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();

		// ユーザ詳細取得
		UserDetails userDetails = null;
		try {
			userDetails = memberUserDetailsSrv.loadUserByUsername(username);
		} catch (Exception e) {
			String msg = String.format("認証対象のユーザが存在しません : username=%s", username);
			throw new BadCredentialsException(msg);
		}

		// パスワード認証(AES)
		String decrypt = CipherUtil.decrypt(userDetails.getPassword());
		if (!password.equals(decrypt)) {
			String msg = String.format("認証できません : username=%s", username);
			throw new BadCredentialsException(msg);
		}

		// ログイン履歴追加
		try {
			enterMemberLoginHistory(((MemberEntity) userDetails).getMemberNo());
		} catch (Exception e) {
			String msg = String.format("ログイン履歴を追加できません : username=%s", username);
			throw new BadCredentialsException(msg);
		}

		// ログ出力
		logger.info("authenticate() : username={}", username);

		return new UsernamePasswordAuthenticationToken(userDetails, authentication.getCredentials());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

	/**
	 * ログイン履歴追加
	 * @param memberNo 会員番号
	 */
	private void enterMemberLoginHistory(Integer memberNo) {

		// DTO生成
		MemberLoginHistoryDto enterDto = MemberLoginHistoryDto.createSingle();
		MemberLoginHistoryEntity enterEntity = enterDto.getFirstEntity();
		enterEntity.setMemberNo(memberNo); // 会員番号
		enterEntity.setLoginDatetime(new Timestamp(System.currentTimeMillis())); // ログイン日時(システム現在日時)

		// 登録
		memberLoginHistoryService.enter(enterDto);
	}
}
