package jp.or.jaot;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.ArrayUtils;

import jp.or.jaot.core.util.CipherUtil;

/**
 * 暗号化パスワードジェネレータ<br>
 * 引数を指定して暗号化(AES)されたパスワードを生成します。<br>
 * [引数]<br>
 * 暗号化対象のパスワード
 * [実行方法]<br>
 * 「コンテキストメニュー」->「実行」->「Java アプリケーション」<br>
 */
public class AESPasswordGenerator {

	static SecretKey secretKey = null;
	static IvParameterSpec ivParameterSpec = null;
	static final String KEY = "YKo83n14SWf7o8G5";
	static final String ALGORITHM = "AES";
	static final String passwd = "hogehoge";
	static final String DEF_PASSWD = "hogehogehogehogehogehogehogehoge";
	static final String DEF_IVV = "fugafugafugafuga";
	static final String DEF_TEXT = "zxds9dbjlvab";

	/**
	 * mainメソッド
	 * @param args 引数
	 */
	public static void main(String args[]) {
		if (ArrayUtils.isEmpty(args)) {
			System.out.println("Empty password!!");
			return;
		}
		String rawPassword = args[0];
		try {
			System.out.println(String.format("MaxAllowedKeyLength=%d", Cipher.getMaxAllowedKeyLength("AES")));
			secretKey = AESPasswordGenerator.generateKey();
			ivParameterSpec = AESPasswordGenerator.generateIV();
			System.out.println("-----------------------------------------");
			{
				String encPassword = encrypt(rawPassword);
				String decPassword = decrypt(encPassword);
				//				String decPassword = decrypt("UqNAMylynsIk5TNnh+20sw==");
				System.out.println(String.format("Raw : %s", rawPassword));
				System.out.println(String.format("Enc : %s", encPassword));
				System.out.println(String.format("Dec : %s", decPassword));
			}
			System.out.println("-----------------------------------------");
			{
				String encPassword = encrypt(rawPassword, KEY, ALGORITHM);
				String decPassword = decrypt(encPassword, KEY, ALGORITHM);
				System.out.println(String.format("Raw : %s", rawPassword));
				System.out.println(String.format("Enc : %s", encPassword));
				System.out.println(String.format("Dec : %s", decPassword));
			}
			System.out.println("-----------------------------------------");
			{
				String encPassword = encrypt(passwd, rawPassword);
				String decPassword = decrypt(passwd, encPassword);
				System.out.println(String.format("Raw : %s", rawPassword));
				System.out.println(String.format("Enc : %s", encPassword));
				System.out.println(String.format("Dec : %s", decPassword));
			}
			System.out.println("-----------------------------------------");
			{
				String encPassword = getEncrypt(DEF_PASSWD, DEF_IVV, DEF_TEXT);
				String decPassword = getDecrypt(DEF_PASSWD, DEF_IVV, encPassword);
				System.out.println(String.format("Raw : %s", rawPassword));
				System.out.println(String.format("Enc : %s", encPassword));
				System.out.println(String.format("Dec : %s", decPassword));
			}
			System.out.println("-----------------------------------------");
			{
				String encPassword = CipherUtil.encrypt(DEF_TEXT);
				String decPassword = CipherUtil.decrypt(encPassword);
				System.out.println(String.format("Raw : %s", rawPassword));
				System.out.println(String.format("Enc : %s", encPassword));
				System.out.println(String.format("Dec : %s", decPassword));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static SecretKey generateKey() throws Exception {
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(128);
		return keyGen.generateKey();
	}

	public static IvParameterSpec generateIV() {
		SecureRandom random = new SecureRandom();
		byte[] iv = new byte[16];
		random.nextBytes(iv);
		return new IvParameterSpec(iv);
	}

	public static String encrypt(String input) throws GeneralSecurityException {
		SecretKey key = secretKey;
		IvParameterSpec iv = ivParameterSpec;
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); // 書式:"アルゴリズム/ブロックモード/パディング方式"
		cipher.init(Cipher.ENCRYPT_MODE, key, iv);
		byte[] crypto = cipher.doFinal(input.getBytes());
		byte[] str64 = Base64.getEncoder().encode(crypto);

		return new String(str64);
	}

	public static String decrypt(String str64) throws GeneralSecurityException {
		SecretKey key = secretKey;
		IvParameterSpec iv = ivParameterSpec;
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding"); // 書式:"アルゴリズム/ブロックモード/パディング方式"
		cipher.init(Cipher.DECRYPT_MODE, key, iv);
		byte[] str = Base64.getDecoder().decode(str64);
		byte[] text = cipher.doFinal(str);

		return new String(text);
	}

	public static String encrypt(String source, String key, String algorithm) throws Exception {
		Cipher cipher = Cipher.getInstance(algorithm);
		cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key.getBytes(), algorithm));
		return new String(Base64.getEncoder().encode(cipher.doFinal(source.getBytes())));
	}

	public static String decrypt(String encryptSource, String key, String algorithm) throws Exception {
		Cipher cipher = Cipher.getInstance(algorithm);
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key.getBytes(), algorithm));
		return new String(cipher.doFinal(Base64.getDecoder().decode(encryptSource.getBytes())));
	}

	public static String encrypt(String passwd, String message) {
		try {
			byte[] key = passwd.getBytes();
			MessageDigest sha = MessageDigest.getInstance("SHA-256");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 32);
			SecretKeySpec keyObj = new SecretKeySpec(key, "AES");
			IvParameterSpec ivObj = new IvParameterSpec(Arrays.copyOf(key, 16));
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, keyObj, ivObj);
			byte[] byteCipherText = cipher.doFinal(message.getBytes());
			return Base64.getEncoder().encodeToString(byteCipherText);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String decrypt(String passwd, String cryptText) {
		try {
			byte[] key = passwd.getBytes();
			MessageDigest sha = MessageDigest.getInstance("SHA-256");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 32);
			SecretKeySpec keyObj = new SecretKeySpec(key, "AES");
			IvParameterSpec ivObj = new IvParameterSpec(Arrays.copyOf(key, 16));
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, keyObj, ivObj);
			byte[] cipherBytes = Base64.getDecoder().decode(cryptText);
			byte[] byteDecryptedText = cipher.doFinal(cipherBytes);
			return new String(byteDecryptedText);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String getEncrypt(String passwd, String ivv, String text) {
		try {
			byte[] key = passwd.getBytes();
			byte[] iv = ivv.getBytes();
			System.out.println(String.format("passwd=%s, ivv=%s, text=%s", passwd, ivv, text));

			// 暗号化
			SecretKeySpec keyObj = new SecretKeySpec(key, "AES");
			IvParameterSpec ivObj = new IvParameterSpec(iv);
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, keyObj, ivObj);
			byte[] byteCipherText = cipher.doFinal(text.getBytes());
			String cryptText = Base64.getEncoder().encodeToString(byteCipherText);
			return cryptText;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String getDecrypt(String passwd, String ivv, String cryptText) {
		try {
			byte[] key = passwd.getBytes();
			byte[] iv = ivv.getBytes();
			System.out.println(String.format("passwd=%s, ivv=%s, cryptText=%s", passwd, ivv, cryptText));

			// 復号化
			SecretKeySpec keyObj = new SecretKeySpec(key, "AES");
			IvParameterSpec ivObj = new IvParameterSpec(iv);
			Cipher cipher2 = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher2.init(Cipher.DECRYPT_MODE, keyObj, ivObj);
			byte[] cipherBytes = Base64.getDecoder().decode(cryptText);
			byte[] byteDecryptedText = cipher2.doFinal(cipherBytes);
			return new String(byteDecryptedText);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
