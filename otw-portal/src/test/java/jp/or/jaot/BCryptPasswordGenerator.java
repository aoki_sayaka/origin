package jp.or.jaot;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 暗号化パスワードジェネレータ<br>
 * 引数を指定して暗号化(BCrypt)されたパスワードを生成します。<br>
 * [引数]<br>
 * 暗号化対象のパスワード
 * [実行方法]<br>
 * 「コンテキストメニュー」->「実行」->「Java アプリケーション」<br>
 */
public class BCryptPasswordGenerator {

	/**
	 * mainメソッド
	 * @param args 引数
	 */
	public static void main(String args[]) {
		if (ArrayUtils.isEmpty(args)) {
			System.out.println("Empty password!!");
			return;
		}
		String rawPassword = args[0];
		try {
			BCryptPasswordEncoder bpe = new BCryptPasswordEncoder();
			String encPassword = bpe.encode(rawPassword);
			System.out.println(String.format("Raw : %s", rawPassword));
			System.out.println(String.format("Enc : %s", encPassword));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
