#!/bin/bash

#
# Define parameters.
#
USERNAME="99999999"
PASSWORD="NDdBL4r45U"
URL="https://localhost/wp"
ACCEPT="accept: application/json"
CTYPE="Content-Type: application/json"

#
# Authorization User & Extraction JWT
#
BEARER=$(curl -v -d "{ \"username\" : \"$USERNAME\", \"password\" : \"$PASSWORD\"}" -H "$ACCEPT" -H "$CTYPE" "$URL/login" -k 2>&1 | grep "Bearer")
# 検証環境だと謎の一文字が入るので末尾の一文字を取り除く
JWT="${BEARER:24:-1}"

#
# Post JSON requests.
#

# PortalProfileController
curl -v -d "{}" -H "$ACCEPT" -H "$CTYPE" -H "Authorization: Bearer $JWT" "$URL/profile/login_member" -k

# PortalFacilityController(facility)
curl -v -X POST -d "{\"target\" : \"1\", \"category\" : \"1\", \"priority\" : true, \"publishFrom\" : \"2019-01-01 12:34:00\", \"publishTo\" : \"2019-12-01 12:34:00\", \"systemMessage\" : true, \"subject\" : \"\u25cb\u25cb\u25cb\u306b\u95a2\u3059\u308b\u304a\u77e5\u3089\u305b\", \"content\" : \"\u30d1\u30b9\u30ef\u30fc\u30c9\u306e\u5909\u66f4\u3092\u304a\u9858\u3044\u3044\u305f\u3057\u307e\u3059\u3002\", \"destinationRangeFacility\" : true, \"destinationRangeTrainingSchool\" : true}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $JWT" "http://localhost:8080/wp/notice/enter_notice"
curl -v -X POST -d "{\"target\" : \"2\", \"category\" : \"1\", \"priority\" : true, \"publishFrom\" : \"2019-01-02 12:34:00\", \"publishTo\" : \"2019-12-01 12:34:00\", \"systemMessage\" : true, \"subject\" : \"\u25cb\u25cb\u25cb\u306b\u95a2\u3059\u308b\u304a\u77e5\u3089\u305b\", \"content\" : \"\u30d1\u30b9\u30ef\u30fc\u30c9\u306e\u5909\u66f4\u3092\u304a\u9858\u3044\u3044\u305f\u3057\u307e\u3059\u3002\", \"destinationRangeFacility\" : true, \"destinationRangeTrainingSchool\" : true}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $JWT" "http://localhost:8080/wp/notice/enter_notice"

curl -v -X POST -d "{\"target\" : \"1\", \"category\" : \"1\", \"priority\" : false, \"publishFrom\" : \"2019-08-30 12:34:00\", \"publishTo\" : \"2019-12-01 12:34:00\", \"systemMessage\" : false, \"subject\" : \"\u25c7\u25c7\u25c7\u25c7\u25c7\u25c7\u4f1a\u306b\u95a2\u3059\u308b\u5831\u544a\", \"content\" : \"\u25a1\u25a1\u25a1\u306b\u95a2\u3059\u308b\u5831\u544a\u3067\u3059\u3002\", \"destinationRangeFacility\" : true, \"destinationRangeTrainingSchool\" : true}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $JWT" "http://localhost:8080/wp/notice/enter_notice"
curl -v -X POST -d "{\"target\" : \"2\", \"category\" : \"1\", \"priority\" : false, \"publishFrom\" : \"2019-08-30 12:34:00\", \"publishTo\" : \"2019-12-01 12:34:00\", \"systemMessage\" : false, \"subject\" : \"\u25c7\u25c7\u25c7\u25c7\u25c7\u25c7\u4f1a\u306b\u95a2\u3059\u308b\u5831\u544a\", \"content\" : \"\u25a1\u25a1\u25a1\u306b\u95a2\u3059\u308b\u5831\u544a\u3067\u3059\u3002\", \"destinationRangeFacility\" : true, \"destinationRangeTrainingSchool\" : true}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer $JWT" "http://localhost:8080/wp/notice/enter_notice"
