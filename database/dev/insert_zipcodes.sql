insert into zipcodes (zipcode, address, prefecture_cd, prefecture_name)
select
	substr(a.zipcode, 1, 3) || '-' || substr(a.zipcode, 4, 4),
	a.city || CASE WHEN a.street = '以下に掲載がない場合' THEN '' ELSE a.street END,
	(select g.code from general_codes g where a.prefecture = g.content),
	ct.naiyo
from
	ken_all a inner join ct_todoufuken ct on a.prefecture = ct."naiyoKen"