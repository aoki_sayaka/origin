
/* Drop Tables */

DROP TABLE IF EXISTS public.application_progresses;
DROP TABLE IF EXISTS public.applications;
DROP TABLE IF EXISTS public.attendance_card_reissue_output_histories;
DROP TABLE IF EXISTS public.attend_update_histories;
DROP TABLE IF EXISTS public.author_presenter_actual_fields;
DROP TABLE IF EXISTS public.author_presenter_actual_results;
DROP TABLE IF EXISTS public.barcode_output_histories;
DROP TABLE IF EXISTS public.basic_ot_qualify_histories;
DROP TABLE IF EXISTS public.basic_ot_training_histories;
DROP TABLE IF EXISTS public.basic_ot_attending_summaries;
DROP TABLE IF EXISTS public.basic_ot_qualify_applications;
DROP TABLE IF EXISTS public.basic_point_training_histories;
DROP TABLE IF EXISTS public.billing_details;
DROP TABLE IF EXISTS public.reminder_print_histories;
DROP TABLE IF EXISTS public.transfer_paper_output_histories;
DROP TABLE IF EXISTS public.transfer_paper_print_histories;
DROP TABLE IF EXISTS public.billings;
DROP TABLE IF EXISTS public.billing_settings;
DROP TABLE IF EXISTS public.card_reissue_histories;
DROP TABLE IF EXISTS public.case_reports;
DROP TABLE IF EXISTS public.clinical_training_facility_requirement_members;
DROP TABLE IF EXISTS public.clinical_training_facility_applications;
DROP TABLE IF EXISTS public.clinical_training_facilities;
DROP TABLE IF EXISTS public.clinical_training_leader_qualify_histories;
DROP TABLE IF EXISTS public.clinical_training_leader_training_histories;
DROP TABLE IF EXISTS public.clinical_training_leader_summaries;
DROP TABLE IF EXISTS public.clinical_training_point_application_details;
DROP TABLE IF EXISTS public.clinical_training_point_applications;
DROP TABLE IF EXISTS public.department_histories;
DROP TABLE IF EXISTS public.deposit_details;
DROP TABLE IF EXISTS public.receipt_print_histories;
DROP TABLE IF EXISTS public.deposits;
DROP TABLE IF EXISTS public.dm_label_print_histories;
DROP TABLE IF EXISTS public.doc_deficiency_descriptions;
DROP TABLE IF EXISTS public.doc_deficiency_histories;
DROP TABLE IF EXISTS public.doc_deficiency_reasons;
DROP TABLE IF EXISTS public.dsk_receipts;
DROP TABLE IF EXISTS public.examination_applications;
DROP TABLE IF EXISTS public.examination_details;
DROP TABLE IF EXISTS public.exam_no_seqs;
DROP TABLE IF EXISTS public.exemption_reasons;
DROP TABLE IF EXISTS public.external_group_charges;
DROP TABLE IF EXISTS public.external_groups;
DROP TABLE IF EXISTS public.facility_categories;
DROP TABLE IF EXISTS public.facility_members;
DROP TABLE IF EXISTS public.facility_training_school_update_histories;
DROP TABLE IF EXISTS public.info_by_fiscal_year;
DROP TABLE IF EXISTS public.training_school_name_histories;
DROP TABLE IF EXISTS public.training_school_open_histories;
DROP TABLE IF EXISTS public.training_school_relations;
DROP TABLE IF EXISTS public.wfot_applications;
DROP TABLE IF EXISTS public.wfot_certified_training_schools;
DROP TABLE IF EXISTS public.wfot_info;
DROP TABLE IF EXISTS public.facilities;
DROP TABLE IF EXISTS public.facility_application_categories;
DROP TABLE IF EXISTS public.facility_applications;
DROP TABLE IF EXISTS public.facility_contacts;
DROP TABLE IF EXISTS public.fees;
DROP TABLE IF EXISTS public.general_codes;
DROP TABLE IF EXISTS public.handbook_migration_applications;
DROP TABLE IF EXISTS public.history_field_definitions;
DROP TABLE IF EXISTS public.history_table_definitions;
DROP TABLE IF EXISTS public.import_attendees;
DROP TABLE IF EXISTS public.import_trainings;
DROP TABLE IF EXISTS public.import_training_applicants;
DROP TABLE IF EXISTS public.import_training_histories;
DROP TABLE IF EXISTS public.import_training_points;
DROP TABLE IF EXISTS public.individual_billing_print_histories;
DROP TABLE IF EXISTS public.info_by_fiscal_year_temp;
DROP TABLE IF EXISTS public.invoice_print_histories;
DROP TABLE IF EXISTS public.library_attachment_files;
DROP TABLE IF EXISTS public.library_destinations;
DROP TABLE IF EXISTS public.library_destination_exclusions;
DROP TABLE IF EXISTS public.library_view_histories;
DROP TABLE IF EXISTS public.libraries;
DROP TABLE IF EXISTS public.lifelong_education_statuses;
DROP TABLE IF EXISTS public.lifelong_education_system_transition_data;
DROP TABLE IF EXISTS public.mail_attachments;
DROP TABLE IF EXISTS public.mail_condition_prefs;
DROP TABLE IF EXISTS public.mail_member_destinations;
DROP TABLE IF EXISTS public.mail_non_member_destinations;
DROP TABLE IF EXISTS public.mail_templates;
DROP TABLE IF EXISTS public.membership_fee_deliveries;
DROP TABLE IF EXISTS public.member_email_address_histories;
DROP TABLE IF EXISTS public.member_facility_categories;
DROP TABLE IF EXISTS public.member_local_activities;
DROP TABLE IF EXISTS public.member_login_histories;
DROP TABLE IF EXISTS public.member_password_histories;
DROP TABLE IF EXISTS public.member_qualifications;
DROP TABLE IF EXISTS public.member_remarks_histories;
DROP TABLE IF EXISTS public.member_returned_item_histories;
DROP TABLE IF EXISTS public.member_roles;
DROP TABLE IF EXISTS public.member_state_histories;
DROP TABLE IF EXISTS public.member_withdrawal_histories;
DROP TABLE IF EXISTS public.mtdlp_case_reports;
DROP TABLE IF EXISTS public.mtdlp_qualify_histories;
DROP TABLE IF EXISTS public.mtdlp_training_histories;
DROP TABLE IF EXISTS public.mtdlp_summaries;
DROP TABLE IF EXISTS public.officer_histories;
DROP TABLE IF EXISTS public.other_org_point_application_attachments;
DROP TABLE IF EXISTS public.other_org_point_applications;
DROP TABLE IF EXISTS public.paot_membership_fee_deliveries;
DROP TABLE IF EXISTS public.paot_membership_histories;
DROP TABLE IF EXISTS public.paot_officer_histories;
DROP TABLE IF EXISTS public.period_extension_applications;
DROP TABLE IF EXISTS public.professional_ot_qualify_histories;
DROP TABLE IF EXISTS public.professional_ot_test_histories;
DROP TABLE IF EXISTS public.professional_ot_training_histories;
DROP TABLE IF EXISTS public.professional_ot_attending_summaries;
DROP TABLE IF EXISTS public.ot_activity_results;
DROP TABLE IF EXISTS public.ot_case_report_registrations;
DROP TABLE IF EXISTS public.ot_clinical_training_reports;
DROP TABLE IF EXISTS public.ot_jaot_activity_results;
DROP TABLE IF EXISTS public.ot_junior_training_experiences;
DROP TABLE IF EXISTS public.ot_papers;
DROP TABLE IF EXISTS public.ot_paper_presentations;
DROP TABLE IF EXISTS public.ot_selected_trainings;
DROP TABLE IF EXISTS public.ot_training_reports;
DROP TABLE IF EXISTS public.qualificate_ot_applications;
DROP TABLE IF EXISTS public.qualification_ot_app_histories;
DROP TABLE IF EXISTS public.qualified_ot_completion_histories;
DROP TABLE IF EXISTS public.qualified_ot_training_histories;
DROP TABLE IF EXISTS public.qualified_ot_attending_summaries;
DROP TABLE IF EXISTS public.recess_histories;
DROP TABLE IF EXISTS public.reprint_applications;
DROP TABLE IF EXISTS public.reward_punish_histories;
DROP TABLE IF EXISTS public.social_contribution_actual2_fields;
DROP TABLE IF EXISTS public.social_contribution_actual_fields;
DROP TABLE IF EXISTS public.social_contribution_actual_results;
DROP TABLE IF EXISTS public.special_ot_applications;
DROP TABLE IF EXISTS public.special_ot_application_histories;
DROP TABLE IF EXISTS public.members;
DROP TABLE IF EXISTS public.member_app_facility_categories;
DROP TABLE IF EXISTS public.member_app_local_activities;
DROP TABLE IF EXISTS public.member_app_qualifications;
DROP TABLE IF EXISTS public.member_applications;
DROP TABLE IF EXISTS public.member_revoked_authorities;
DROP TABLE IF EXISTS public.member_update_histories;
DROP TABLE IF EXISTS public.monthly_processes;
DROP TABLE IF EXISTS public.notice_condition_prefs;
DROP TABLE IF EXISTS public.notice_destinations;
DROP TABLE IF EXISTS public.notices;
DROP TABLE IF EXISTS public.point_application_sampling_settings;
DROP TABLE IF EXISTS public.print_names;
DROP TABLE IF EXISTS public.qualifications;
DROP TABLE IF EXISTS public.qualification_no_seqs;
DROP TABLE IF EXISTS public.questionnaire_destinations;
DROP TABLE IF EXISTS public.questionnaires;
DROP TABLE IF EXISTS public.region_related_large_classifications;
DROP TABLE IF EXISTS public.region_related_middle_classifications;
DROP TABLE IF EXISTS public.region_related_small_classifications;
DROP TABLE IF EXISTS public.remarks_histories;
DROP TABLE IF EXISTS public.send_mails;
DROP TABLE IF EXISTS public.sig_other_group_histories;
DROP TABLE IF EXISTS public.sig_other_groups;
DROP TABLE IF EXISTS public.support_members;
DROP TABLE IF EXISTS public.system_transition_graph_data;
DROP TABLE IF EXISTS public.temporary_deposit_details;
DROP TABLE IF EXISTS public.total_receipt_print_histories;
DROP TABLE IF EXISTS public.training_application_histories;
DROP TABLE IF EXISTS public.training_applications;
DROP TABLE IF EXISTS public.training_attendance_priorities;
DROP TABLE IF EXISTS public.training_attendance_requirements;
DROP TABLE IF EXISTS public.training_attendance_results;
DROP TABLE IF EXISTS public.training_exemption_requirements;
DROP TABLE IF EXISTS public.training_files;
DROP TABLE IF EXISTS public.training_instructors;
DROP TABLE IF EXISTS public.training_operators;
DROP TABLE IF EXISTS public.training_organizers;
DROP TABLE IF EXISTS public.training_program_instructors;
DROP TABLE IF EXISTS public.training_programs;
DROP TABLE IF EXISTS public.training_reports;
DROP TABLE IF EXISTS public.training_advance_payments;
DROP TABLE IF EXISTS public.training_instructor_rewards;
DROP TABLE IF EXISTS public.training_operator_rewards;
DROP TABLE IF EXISTS public.training_results;
DROP TABLE IF EXISTS public.training_send_mail_detail_histories;
DROP TABLE IF EXISTS public.training_send_mail_histories;
DROP TABLE IF EXISTS public.training_temp_applications;
DROP TABLE IF EXISTS public.trainings;
DROP TABLE IF EXISTS public.training_category_members;
DROP TABLE IF EXISTS public.training_category_priorities;
DROP TABLE IF EXISTS public.training_category_requirements;
DROP TABLE IF EXISTS public.training_categories;
DROP TABLE IF EXISTS public.training_no_conversions;
DROP TABLE IF EXISTS public.training_numbers;
DROP TABLE IF EXISTS public.user_login_histories;
DROP TABLE IF EXISTS public.user_roles;
DROP TABLE IF EXISTS public.users;
DROP TABLE IF EXISTS public.user_revoked_authorities;
DROP TABLE IF EXISTS public.zipcodes;
DROP TABLE IF EXISTS public.member_entry_requests;




/* Create Tables */

CREATE TABLE public.applications
(
	id bigserial NOT NULL,
	branch_no int,
	-- 更新連番フィールドをここに充てる
	seq_no int,
	-- 1:WEB
	-- 2:事務局
	register_category_cd varchar(1),
	member_no bigint NOT NULL,
	-- 1:各種届
	-- 2:再発行
	application_type_cd varchar(1),
	-- 01:退会届
	-- 02:休会届
	-- 03:復会届
	-- 04:WFOT個人会員申込書
	-- 05:WFOT個人会員退会申請
	-- 06:会費免除
	-- 07:領収書
	-- 08:認定証フォルダー
	-- 09:徽章再発行
	-- 10:研修受講カード再発行
	-- など
	application_kind_cd varchar(2),
	-- 1:印刷
	-- 2:送付依頼
	-- 3:WEB申込
	application_category_cd varchar(1),
	apply_date date,
	-- 1:自宅
	-- 2:施設
	-- 3:海外
	-- 
	send_category_cd varchar(1),
	address_zipcode varchar(8),
	-- 東コロ様DB定義のサイズを指定
	address1 varchar(32),
	-- 東コロ様DB定義のサイズを指定
	address2 varchar(32),
	-- 東コロ様DB定義のサイズを指定
	address3 varchar(32),
	-- 画面定義書によると
	-- [退会]
	-- 出産・育児
	-- 家事・育児
	-- 休職中
	-- 退職
	-- 他職種へ転職
	-- 結婚退職
	-- 病気療養
	-- 家庭の事情
	-- ...など。
	-- 
	-- [休会]
	-- 出産
	-- 育児
	-- 出産・育児
	-- 病気療養
	-- 退職のため
	-- 育児による退職
	-- その他
	-- ...など。
	reason_cd varchar(2),
	-- 東コロ様DB定義ではvarchar(100)になっているが、
	-- 画面設計の方では特にサイズ制限を設けていない。
	-- 会員が自由に記述する欄であるため長めにとっておく
	withdrawal_other_reason text,
	recess_start_date date,
	recess_end_date date,
	-- 東コロ様DB定義によると
	-- 1:削除する
	-- 2:削除しない
	-- 
	work_place_delete_cd varchar(1),
	reissue_fee int,
	-- DB定義には無いが、画面で以下のような使われ方をしている
	-- ・新規送付登録画面等で住所を入力
	remarks text,
	-- WFOT個人会員申込で入力する。
	-- 東コロ様定義のDBになかったので追加
	wfot_english_full_nm varchar(100),
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.application_progresses
(
	id bigserial NOT NULL,
	application_no bigint NOT NULL,
	branch_no int,
	seq_no int,
	-- 1:WEB
	-- 2:事務局
	register_category_cd varchar(1),
	member_no int,
	-- 1:各種届
	-- 2:再発行
	application_type_cd varchar(1),
	-- 01:退会届
	-- 02:休会届
	-- 03:復会届
	-- 04:WFOT個人会員申込書
	-- 05:WFOT個人会員退会申請
	-- 06:会費免除
	-- 07:領収書
	-- 08:認定証フォルダー
	-- 09:徽章再発行
	-- 10:研修受講カード再発行
	-- など
	application_kind_cd varchar(2),
	-- 1:印刷
	-- 2:送付依頼
	-- 3:WEB申込
	application_category_cd varchar(1),
	apply_date date,
	send_date date,
	receipt_date date,
	approval_date date,
	-- 状況＝進捗。
	-- 申請種類の選択により内容が変わる。
	-- 画面設計書「ap_06_ap10120100_各種受付一覧_v25.xlsx」のシート「６－１－４．その他の補足説明」にここに設定する値の設定ロジックが定義されている。
	progress_cd varchar(3),
	-- 状態＝結果
	-- 申請の結果がどうなったかを保持するテーブル
	status_cd varchar(3),
	-- 1:自宅
	-- 2:施設
	-- 3:海外
	-- 
	send_category_cd varchar(1),
	send_count int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.attendance_card_reissue_output_histories
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	member_nm varchar(50),
	-- 帳票定義の方で「チェックデジットなしの１２桁」と定義されている
	member_cd varchar(12),
	-- カラム長が不明だが画面定義のサンプルデータが先頭0埋め3桁で書かれていたのでvarchar(3)とする
	security_cd varchar(3),
	member_nm_kana varchar(50),
	-- 会員テーブルの電話番号長に合わせた
	phone_number varchar(36),
	zipcode varchar(8),
	home_address1 varchar(32),
	home_address2 varchar(32),
	home_address3 varchar(32),
	facility_address1 varchar(128),
	facility_address2 varchar(128),
	facility_address3 varchar(128),
	facility_nm1 varchar(128),
	facility_nm2 varchar(128),
	department varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.attend_update_histories
(
	id bigserial NOT NULL UNIQUE,
	update_no int,
	attend_update_datetime timestamp,
	update_user_id varchar(32),
	member_no int,
	paot_cd smallint,
	update_category_cd smallint,
	update_date timestamp with time zone,
	table_cd smallint,
	table_nm varchar,
	field_cd smallint,
	field_nm varchar,
	content_before varchar,
	content_after varchar,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.author_presenter_actual_fields
(
	id bigserial NOT NULL,
	result_id bigint NOT NULL,
	-- 身体障害
	-- 精神障害
	-- 発達障害
	-- 老年期生涯
	field_cd varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.author_presenter_actual_results
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- １：WEB
	-- ２：事務局
	register_category_cd char(1),
	activity_content_cd varchar(2),
	activity_detail varchar(255),
	author_presenter_category_cd varchar(2),
	-- yyyymm
	publication_ym int,
	volume varchar(4),
	issue varchar(4),
	start_page int,
	end_page int,
	publisher varchar(128),
	journal_name varchar(128),
	convention_name varchar(128),
	held_times int,
	venue varchar(4),
	-- 団体/学会/研修会/会議/等
	group_name varchar(128),
	other_start_page int,
	other_end_page int,
	isbn_issn varchar(32),
	publish_category_cd char(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.barcode_output_histories
(
	id bigserial NOT NULL,
	billing_id bigint NOT NULL,
	-- 10桁の連番 + 6桁の会員番号
	billing_no varchar(16),
	payment_period_date date,
	amount int,
	-- 0固定
	reissue_cd varchar(1),
	qualification varchar(20),
	member_no int,
	-- ※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく
	last_name varchar(20),
	-- ※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく
	first_name varchar(20),
	total_amount int,
	zipcode varchar(8),
	address1 varchar(32),
	address2 varchar(32),
	address3 varchar(32),
	facility_nm1 varchar(32),
	facility_nm2 varchar(32),
	billing_fiscal_year int,
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option1 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option2 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option3 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option4 varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.basic_ot_attending_summaries
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL UNIQUE,
	qualification_date date,
	qualification_period_date date,
	renewal_count int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.basic_ot_qualify_applications
(
	id bigserial NOT NULL,
	application_date date,
	-- 01:会員 02:事務局
	applicant_cd varchar(2),
	-- 申請者の会員番号
	member_no int NOT NULL,
	-- 申請区分（コード設計50140）
	-- 01 新規申請 02 更新申請
	category_cd varchar(2),
	fiscal_year int,
	-- 申請状況（コード設計50150）
	-- 101:申請中 102:承認 103:否認 104:保留
	status_cd varchar(2),
	renewal_times int,
	carry_over_point int,
	secretariat_confirm_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.basic_ot_qualify_histories
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	seq int,
	qualification_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (member_no, seq)
) WITHOUT OIDS;


CREATE TABLE public.basic_ot_training_histories
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	-- YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス
	training_cd varchar(16),
	fiscal_year varchar(4),
	promoter_cd varchar(2),
	category_cd varchar(2),
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field_cd varchar(2),
	group_cd varchar(2) NOT NULL,
	class_cd varchar(2) NOT NULL,
	branch_cd varchar(2),
	seq int NOT NULL,
	training_name varchar(255),
	attend_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.basic_point_training_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	attend_date date,
	content varchar(100),
	-- 参加、発表、講師、裁量
	point_type varchar(1),
	days int,
	point int,
	use_point boolean DEFAULT 'FALSE' NOT NULL,
	promoter_cd varchar(2),
	others_sig_cd varchar(2),
	facility_no varchar(20),
	collation_member_name varchar(20),
	collation_rate decimal,
	start_date date,
	end_date date,
	remarks text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.billings
(
	id bigserial NOT NULL,
	billing_fiscal_year int,
	-- 会費種別のこと。
	-- 0:正会員
	-- 1:賛助会員
	-- 2:入会
	-- 3:研修会
	-- 4:生涯教育
	-- 
	billing_type_cd varchar(1),
	seq_no int,
	modify_count int,
	-- 0:通常データ
	-- 1:削除データ
	billing_delete_flag varchar(1),
	billing_update_date timestamp,
	billing_update_user_id varchar(32),
	billing_update_seq_no int,
	-- 0:通常データ
	-- 1:仮データ
	temporary_billing_flag varchar(1),
	-- ・バーコードと振込用紙の定義では0固定とある
	-- ・業者向け振込用紙データでは3などの値を設定するとある
	-- どちらにしろ１桁のコードがあれば済みそうなので定義する
	reissue_cd varchar(1),
	reissue_count int,
	billing_issue_datetime timestamp,
	total_fee int,
	payment_period_date date,
	-- コード値は不明。おそらく
	-- 1:自宅
	-- 2:施設
	-- 3:海外
	-- 
	shipping_category_cd varchar(1),
	remarks varchar(32),
	-- ※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく
	last_name varchar(20),
	-- ※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく
	first_name varchar(20),
	-- 東コロ様DB定義のサイズではなく、会員の郵便番号に合わせる(ハイフン込み)
	zipcode varchar(8),
	address1 varchar(32),
	address2 varchar(32),
	address3 varchar(32),
	-- 施設養成校テーブルでのサイズが128になっているが、連携先や振込用紙の都合に合わせて32にする
	facility_nm1 varchar(32),
	-- 施設養成校テーブルでのサイズが128になっているが、連携先や振込用紙の都合に合わせて32にする
	facility_nm2 varchar(32),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option1 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option2 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option3 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option4 varchar(128),
	last_payment_date date,
	-- 請求種別と同じ値が入ると思われる
	last_billing_type_cd varchar(1),
	total_amount int,
	-- 請求種別の変更日時のことか
	billing_type_change_date date,
	billing_type_change_cd varchar(1),
	billing_type_change_amount int,
	-- 「cs_補足資料_請求番号設計v1.xlsx」に定義されている44桁の番号
	-- 今のところ使い道が不明
	invoice_no varchar(44),
	-- 「振込用紙連携データ_update1.pptx」で定義されている。
	-- サンプルデータを見ると単純な連番ではないようで、請求明細テーブルのIDをそのまま使えるわけではなさそう
	billing_no varchar(16),
	bulletin_import_date timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.billing_details
(
	id bigserial NOT NULL,
	billing_id bigint NOT NULL,
	-- コードテーブルに定義されている
	-- 0001:正会員年会費
	-- 0002:WFOT個人年会費
	-- 0003:前年度WFOT個人年会費
	-- 0004:前受金
	-- 
	detail_type varchar(4),
	amount int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.billing_settings
(
	id bigserial NOT NULL,
	billing_fiscal_year int,
	setting_update_date timestamp,
	setting_update_user_id varchar(32),
	setting_create_date timestamp,
	-- 用途不明。請求年度がkeyになるので常に1なのではないか
	seq_no int,
	carry_over_flag varchar(1),
	criteria_date date,
	entry_fee int,
	attend_permission_deposit_period_date date,
	entry_fee_last_billing_date date,
	annual_fee int,
	annual_fee_wfot_dollar int,
	dollar_to_yen_rate decimal(6,2),
	rate_set_date date,
	annual_fee_wfot_yen int,
	annual_fee_payment_period date,
	annual_fee_last_billing_date date,
	support_annual_fee int,
	support_annual_fee_payment_period date,
	support_annual_fee_last_billing_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.card_reissue_histories
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	seq_no int,
	apply_date date,
	reissue_date date,
	-- コード値が不明。
	reissue_category_cd varchar(1),
	reissue_reason text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.case_reports
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	receipt_number int NOT NULL,
	presentation_cd varchar(2),
	title varchar(100),
	overall_judge_date date,
	withdrawn boolean DEFAULT 'FALSE' NOT NULL,
	publish_date date,
	judge_start_date date,
	email_sent boolean DEFAULT 'FALSE' NOT NULL,
	pass_status_cd smallint,
	withdraw_status_cd smallint,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.clinical_training_facilities
(
	id bigserial NOT NULL,
	facility_no varchar(20) NOT NULL UNIQUE,
	facility_nm varchar(128) NOT NULL,
	qualification_date date NOT NULL,
	qualification_number int NOT NULL,
	qualification_period_date date NOT NULL,
	responsible_member_no int NOT NULL,
	requirement_member_no1 int NOT NULL,
	requirement_member_no2 int NOT NULL,
	requirement_member_no3 int NOT NULL,
	declining_date date NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.clinical_training_facility_applications
(
	id bigserial NOT NULL,
	facility_no varchar(20) NOT NULL,
	application_date date NOT NULL,
	-- １：新規
	-- ２：更新
	application_type varchar(1) NOT NULL,
	member_no int NOT NULL,
	application_file_name varchar(255),
	application_file bytea,
	application_file_invalid boolean DEFAULT 'FALSE' NOT NULL,
	requirement_member_no int,
	requirement_member_certfile_name varchar(255),
	requirement_member_certfile bytea,
	requirement_member_certfile_invalid boolean DEFAULT 'FALSE' NOT NULL,
	acceptance_confirmed boolean DEFAULT 'FALSE' NOT NULL,
	acceptance_coordinator boolean DEFAULT 'FALSE' NOT NULL,
	acceptance_coordine_meeting boolean DEFAULT 'FALSE' NOT NULL,
	hold_instructor_meeting boolean DEFAULT 'FALSE' NOT NULL,
	inter_facility_communication boolean DEFAULT 'FALSE' NOT NULL,
	instructor_workshop_participation boolean DEFAULT 'FALSE' NOT NULL,
	instructor_meeting_participation boolean DEFAULT 'FALSE' NOT NULL,
	instructor_system_exist boolean DEFAULT 'FALSE' NOT NULL,
	forced_dispaly_end_date date,
	forced_display boolean DEFAULT 'FALSE' NOT NULL,
	acceptance_certfile1_name varchar(255),
	acceptance_certfile1 bytea,
	acceptance_certfile1_invalid boolean DEFAULT 'FALSE' NOT NULL,
	acceptance_certfile2_name varchar(255),
	acceptance_certfile2 bytea,
	acceptance_certfile2_invalid boolean DEFAULT 'FALSE' NOT NULL,
	acceptance_certfile3_name varchar(255),
	acceptance_certfile3 bytea,
	acceptance_certfile3_invalid boolean DEFAULT 'FALSE' NOT NULL,
	acceptance_certfile4_name varchar(255),
	acceptance_certfile4 bytea,
	acceptance_certfile4_invalid boolean,
	acceptance_certfile5_name varchar(255),
	acceptance_certfile5 bytea,
	acceptance_certfile5_invalid boolean,
	acceptance_certfile6_name varchar(255),
	acceptance_certfile6 bytea,
	acceptance_certfile6_invalid boolean,
	acceptance_certfile7_name varchar(255),
	acceptance_certfile7 bytea,
	acceptance_certfile7_invalid boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.clinical_training_facility_requirement_members
(
	id bigserial NOT NULL,
	clinical_training_facility_app_id bigint NOT NULL,
	member_no int NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.clinical_training_leader_qualify_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- 研修、講習会、厚生労働省認定
	training_type varchar(2),
	seq int,
	qualification_date date,
	qualification_number int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (member_no, training_type, seq)
) WITHOUT OIDS;


CREATE TABLE public.clinical_training_leader_summaries
(
	id bigserial NOT NULL,
	member_no int NOT NULL UNIQUE,
	training_qualification_number int,
	training_qualification_date date,
	session_attend_qualification_date date,
	session_attend_qualification_number int,
	ministry_qualification_date date,
	ministry_qualification_number int,
	beginner_course_attended boolean DEFAULT 'FALSE',
	intermediate_course_attended boolean DEFAULT 'FALSE',
	advanced_course_attended boolean DEFAULT 'FALSE',
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.clinical_training_leader_training_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス
	training_cd varchar(16),
	fiscal_year varchar(4),
	promoter_cd varchar(2),
	category_cd varchar(2),
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field_cd varchar(2),
	group_cd varchar(2),
	class_cd varchar(2),
	branch_cd varchar(2),
	seq int NOT NULL,
	training_name varchar(255),
	attend_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.clinical_training_point_applications
(
	id bigserial NOT NULL,
	application_date date,
	-- 申請操作を行った者の会員番号
	application_member_no int,
	facility_no varchar(20),
	status_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.clinical_training_point_application_details
(
	id bigserial NOT NULL,
	app_id bigint NOT NULL,
	trainer_member_no int,
	trainer_kana_name varchar(50),
	-- 照合時点での会員姓カナ
	verify_last_nm_kana varchar(20),
	-- 照合時点での会員名カナ
	verify_first_nm_kana varchar(20),
	verify_maiden_name varchar(10),
	-- 指導者氏名と会員氏名の一致率％
	name_verification_rate decimal,
	training_start_date date,
	training_end_date date,
	point int,
	-- 申請中、付与済、却下？（要確認とのこと）
	statuc_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.department_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	department_cd varchar(6),
	job_cd varchar(4),
	start_appoint_date date,
	end_appoint_date date,
	remarks varchar(128),
	release_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.deposits
(
	id bigserial NOT NULL,
	-- 10桁の連番 + 6桁の会員番号
	billing_no varchar(16),
	billing_fiscal_year int,
	member_no int,
	-- 会費種別のこと。
	-- 0:正会員
	-- 1:賛助会員
	-- 2:入会
	-- 3:研修会
	-- 4:生涯教育
	-- 
	billing_type_cd varchar(1),
	seq_no int,
	-- 請求データが分割されている場合の番号
	-- 
	-- 分割なし：0
	-- 分割あり：0～
	deposit_division_no int,
	deposit_delete_flag varchar(1),
	deposit_update_date timestamp,
	deposit_update_user_id varchar(32),
	deposit_update_seq_no int,
	deposit_create_date timestamp,
	deposit_date date,
	-- [東コロ様テーブル定義書の記述]
	-- 1:郵便振替
	-- 2:コンビニ
	-- 3:現金
	-- ↑こっちを採用。確報、速報は確報フラグを持つことで対応。
	-- 
	-- [コード設計書の記述]
	-- 0001:コンビニ入金速報
	-- 0002:コンビニ入金確報
	-- 0003:ペーパーレス確報
	-- 0004:郵便振込速報
	-- 0005:郵便振込確報
	-- 0006:郵便振込
	-- 0007:銀行振込
	-- 0008:現金
	-- 
	deposit_type varchar(1),
	-- 東コロ様のテーブル定義に「入金区分」があり、その内容が"0:確定データ"であるため、このフィールドが確報、速報、取消を表すと考える。
	-- 
	-- 1:速報
	-- 2:確報
	-- 3:取消
	-- とする。
	confirmation_type varchar(1),
	reissue_count int,
	deposit_amount int,
	remarks varchar(32),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.deposit_details
(
	id bigserial NOT NULL,
	deposit_id bigint NOT NULL,
	seq_no int,
	deposit_date date,
	-- コードテーブルに定義されている
	-- 0001:正会員年会費
	-- 0002:WFOT個人年会費
	-- 0003:前年度WFOT個人年会費
	-- 0004:前受金
	-- 
	detail_type varchar(4),
	deposit_amount int,
	-- [東コロ様テーブル定義書の記述]
	-- 1:郵便振替
	-- 2:コンビニ
	-- 3:現金
	-- ↑こっちを採用。確報、速報は確報フラグを持つことで対応。
	-- 
	-- [コード設計書の記述]
	-- 0001:コンビニ入金速報
	-- 0002:コンビニ入金確報
	-- 0003:ペーパーレス確報
	-- 0004:郵便振込速報
	-- 0005:郵便振込確報
	-- 0006:郵便振込
	-- 0007:銀行振込
	-- 0008:現金
	-- 
	deposit_type varchar(1),
	-- 会費種別を親コードにもつ入金対象を保持する
	-- [会費種別：新入会]
	-- 入会金、年会費、再入会手数料
	-- [会費種別：正会員]
	-- 年会費、ＷＦＯＴ会費、前年度ＷＦＯＴ会費、前受金
	-- [会費種別：研修会]
	-- 研修会費
	-- [会費種別：生涯教育]
	-- フォルダ再送付、徽章再送付、認定書再発行、試験料、認定料、受講カード再発行、手数料
	-- [会費種別：賛助会員]
	-- 年会費
	deposit_target varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.dm_label_print_histories
(
	id bigserial NOT NULL,
	-- 発行するラベルの種類を保持する。
	-- コード値は未定だが、現状では以下のものがある
	-- 
	-- ＜会員管理、生涯教育、請求収納関連から連携＞
	-- ・【自宅用】
	-- ・【海外用】
	-- ・【施設用】
	-- ・【施設養成校用】
	-- ＜入会管理から連携＞
	-- ・【自宅用】
	-- ・【海外用】
	-- ・【施設用】
	-- ・【施設養成校用】
	-- ＜施設一覧から連携＞
	-- ・【施設用】
	-- ・【施設責任者用】
	-- ・【養成校用】
	-- ・【養成校情報責任者用】
	-- ＜賛助会員一覧から連携＞
	-- ・【基本情報用】
	-- ・【基本情報代表者用】
	-- ・【請求担当者用】
	-- ・【送付担当者用】
	-- ＜外部団体一覧から連携＞
	-- ・【外部団体用】
	-- ・【外部団体担当者用】
	-- ＜DMラベル／送り状個別登録から連携＞
	-- ・共通
	label_kind_cd varchar(2),
	zipcode varchar(8),
	-- 7桁で先頭0埋めする
	member_no_text varchar(7),
	-- DBの住所１フィールドのサイズと異なるので注意
	address1 varchar(22),
	-- DBの住所２フィールドのサイズと異なるので注意
	address2 varchar(66),
	-- DBの住所３フィールドのサイズと異なるので注意
	address3 varchar(44),
	-- サイズがDBの姓と異なるので注意
	last_nm varchar(9),
	-- サイズがDBの名と異なるので注意
	first_nm varchar(10),
	-- サイズがDBの摘要と異なるので注意
	remarks varchar(22),
	foreign_address varchar(110),
	-- 帳票定義では7桁になっているが、番号の省略はあり得ない。
	-- DB定義の施設番号サイズ分確保する
	facility_no varchar(20),
	-- サイズがDBの施設名１と異なるので注意
	facility_nm1 varchar(22),
	-- サイズがDBの施設名２と異なるので注意
	facility_nm2 varchar(22),
	-- サイズがDBの養成校名と異なるので注意
	training_school_nm varchar(22),
	department varchar(22),
	-- サイズがDBの姓、名と異なるので注意
	fullname varchar(14),
	support_member_nm varchar(22),
	-- サイズがDBの個人氏名・代表者名と異なるので注意
	primary_nm varchar(20),
	-- サイズがDBの所属と異なるので注意
	billing_section_nm varchar(10),
	honorifics_cd varchar(2),
	title varchar(7),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.doc_deficiency_descriptions
(
	id bigserial NOT NULL,
	history_id bigint NOT NULL,
	seq_no int,
	-- 申請詳細画面で不備チェックされた項目を示すコード値
	deficiency_cd varchar(2),
	deficiency_description text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.doc_deficiency_histories
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	seq_no int,
	-- 新たにコード値を定義する。
	-- 1:メール
	-- 2:印刷
	-- 
	hisotry_type varchar(1),
	-- 会員のメールアドレスと同じ型桁にする
	email_address varchar(128),
	resubmit_period date,
	next_examination_date date,
	additional_text text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.doc_deficiency_reasons
(
	id bigserial NOT NULL,
	-- どの不備に対応する理由なのかを保持するカラム
	-- 
	category varchar(2),
	reason_cd varchar(3),
	-- 書類不備画面のプルダウンに表示する件名
	-- ※画面定義では説明文そのものがプルダウンになっているがそれは実現が難しいと思われる。「項目」と「説明文」の間に件名(概略)を表示するプルダウンを用意して、それが選択されたら「説明文」が変わるような作りにする
	title varchar(100),
	description text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.dsk_receipts
(
	id bigserial NOT NULL,
	-- 01：速報 02 ：確定 03 ：速報取消
	-- 12：郵便振替 22：郵便振替訂正
	-- 81：リアルデータ
	type_cd varchar(2) NOT NULL,
	-- 店舗での収納日
	receipted_date date NOT NULL,
	-- 店舗での収納時刻
	-- (データ種別が 12 , 22 の場合のときはNULL)
	-- 
	receipted_time time,
	-- EAN + バーコード情報(44桁)をそのまま設定
	barcode varchar(47) NOT NULL,
	-- バーコード情報の１４桁目～１９桁目
	user_data1 varchar(6),
	-- バーコード情報の２０桁目～３０桁目
	-- （３０桁目は再発行区分とする）
	user_data2 varchar(11),
	-- バーコード情報の37桁目
	stamp boolean DEFAULT 'FALSE' NOT NULL,
	-- バーコード情報の３８桁目～４３桁目
	-- ※データ種別が22（郵便振替訂正）の場合は、訂正後の金額がセットされます
	-- ※頭「0」は省略されます。例：001000→1000
	amount_of_money int NOT NULL,
	convenience_head_cd varchar(3) NOT NULL,
	convenience_name varchar(12) NOT NULL,
	convenience_cd varchar(6) NOT NULL,
	-- データ種別が 02 , 12 , 22 の場合、資金の振込日
	-- 上記以外のデータ種別の場合NULL
	transfer_date date,
	creation_date date NOT NULL,
	-- 0埋め10桁
	billing_id varchar(10),
	-- 0埋め6桁
	member_no varchar(6),
	-- 再発行回数
	number_of_reissues int,
	-- 支払期限yy/MM/dd
	payment_due_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.examination_applications
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	application_date date,
	-- 01:認定作業療法士取得臨床実践能力試験
	-- 02:認定作業療法士資格再認定審査（試験）
	-- 03:専門作業療法士資格認定審査（試験）
	-- 
	examination_cd varchar(2),
	-- 画面定義にない項目。
	-- 会員の専門分野であると仮定して定義する
	-- 
	-- 01:身体障害
	-- 02:精神障害
	-- 03:発達障害
	-- 04:老年期障害
	-- 05:その他
	-- 
	field_cd varchar(2),
	fiscal_year varchar(4),
	examination_date date,
	-- 701:申請中
	-- 702:受験受付許可
	-- 703:書類不備
	-- 704:要件不足
	-- 705:保留
	-- 706:取下げ
	-- 707:合格
	-- 
	application_result_cd varchar(3),
	-- 701:事務局承認待ち
	-- 702:許可済み
	-- 703:不許可
	application_status_cd varchar(3),
	secretariat_approval_date date,
	-- 画面定義では20になっているが、施設名1に128文字、施設名2に256文字入力される可能性がある。よってサイズを可変にしておく
	facility_nm varchar,
	-- 自宅 or 連絡先
	contact_cd varchar(1),
	zipcode varchar(8),
	-- 会員の住所1～3がそれぞれ32文字なので100あれば格納可能
	address varchar(100),
	-- カラム名と長さは会員(members)に合わせた
	phone_number varchar(36),
	-- 会員(members)の定義に合わせた
	email_address varchar(128),
	affiliation_certificate_file_nm varchar(50),
	affiliation_certificate_file bytea,
	-- 証明写真ファイル名
	id_photo_file_nm varchar(50),
	-- 証明写真ファイル
	id_photo_file bytea,
	attachment_file_confirm_flag varchar(1),
	issue_date date,
	examination_fee int,
	transfer_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.examination_details
(
	id bigserial NOT NULL,
	-- 01:認定作業療法士取得臨床実践能力試験
	-- 02:認定作業療法士資格再認定審査（試験）
	-- 03:専門作業療法士資格認定審査（試験）
	-- 
	examination_cd varchar(1),
	fiscal_year int,
	-- 4桁のパターンもあるが、既存のsyogaiDBを見ると
	-- mt_senmonテーブルがあり、それぞれコード値が１桁になっているので、2桁で決定する
	field_cd varchar(2),
	examination_date date,
	hold_count int,
	reception_start_date date,
	reception_end_date date,
	examination_start_time time,
	examination_end_time time,
	opening_time time,
	orientation_start_time time,
	place1 varchar(50),
	place2 varchar(50),
	-- フリーテキスト
	examination_type varchar(50),
	bring1 varchar(50),
	bring2 varchar(50),
	fee int,
	-- 口座番号だけ？不明
	bank_account varchar(20),
	transfer_date date,
	other text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.exam_no_seqs
(
	id bigserial NOT NULL,
	category_cd varchar(2),
	-- コード設計[30010]から専門分野の値を抽出
	-- 
	-- 00:なし
	-- 01:福祉用具
	-- 02:認知症
	-- 03:手外科
	-- 04:特別支援
	-- 05:高次脳
	-- 06:精神急性
	-- 07:摂食嚥下
	-- 08:訪問
	-- 09:がん
	-- 10:就労支援
	-- 
	field_cd varchar(2),
	group_cd varchar(2),
	class_cd varchar(2),
	seq_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.exemption_reasons
(
	id bigserial NOT NULL,
	-- 00： コード
	-- 他：研修種別
	type varchar(2),
	reason_no varchar(8),
	branch_no int,
	content varchar(100),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.external_groups
(
	id bigserial NOT NULL,
	-- 01:団体
	-- 02:団体・個人
	-- 03:都道府県士会事務局
	-- 04:関係省庁
	-- 
	group_cd varchar(2),
	company_nm varchar(64),
	zipcode varchar(8),
	prefecture_cd varchar(2),
	address1 varchar(128),
	address2 varchar(128),
	address3 varchar(128),
	-- 01:機関誌のみ
	-- 02:機関紙+学術誌
	-- 03:学術誌のみ
	-- 04:不要
	-- 
	journal_send_cd varchar(2),
	-- 東コロ様DB定義が15桁になっている。
	-- 外部団体が国内とは限らないので15桁にする
	phone_number varchar(15),
	representative_nm varchar(50),
	representative_department_nm varchar(128),
	representative_position_nm varchar(64),
	-- 01:様
	-- 02:御中
	-- →画面定義書には"殿"も定義されている。コード設計書には存在しない
	representative_honorifics_cd varchar(2),
	representative_mail_address varchar(256),
	remarks text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.external_group_charges
(
	id bigserial NOT NULL,
	external_group_id bigint NOT NULL,
	seq_no int,
	charge_nm varchar(64),
	charge_department_nm varchar(128),
	charge_position_nm varchar(64),
	-- 01:様
	-- 02:御中
	-- →画面定義書には"殿"も定義されている。コード設計書には存在しない
	charge_honorifics_cd varchar(2),
	charge_mail_address varchar(256),
	charge_phone_number varchar(15),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.facilities
(
	id bigserial NOT NULL,
	-- 初期登録時の施設番号
	facility_base_no varchar(20),
	-- 旧システムにのみ存在するカラム。
	-- 移行データ格納のため残しておく
	facility_seq_no int,
	-- 1:養成校（大学）
	-- 2:養成校（短期大学）
	-- 3:養成校（専門学校）
	-- 4:養成校（大学院）
	-- 5:開設準備室
	-- 9:施設
	facility_category varchar(1),
	-- 帳票定義では7桁になっているが、番号の省略はあり得ない。
	-- DB定義の施設番号サイズ分確保する
	facility_no varchar(20) NOT NULL,
	name varchar(128),
	name2 varchar(128),
	name_kana varchar(128),
	-- サイズは施設登録申請のフィールドに合わせた
	name_kana2 varchar(256),
	zipcode varchar(8),
	prefecture_cd varchar(2),
	address1 varchar(128),
	address2 varchar(128),
	address3 varchar(128),
	tel_no varchar(20),
	fax_number varchar(20),
	corporate_nm varchar(128),
	corporate_nm_kana varchar(128),
	opener_type_cd char(4),
	opener_nm varchar(128),
	opener_nm_kana varchar(255),
	responsible_email_address varchar(128),
	count_ot int,
	count_member int,
	count_license_3year int,
	count_license_5year int,
	responsible_member_no int,
	responsible_member_nm varchar(128),
	responsible_member_nm_kana varchar(128),
	remarks text,
	training_school_category_cd varchar(2),
	department varchar(128),
	enrollment boolean,
	course_years varchar(1),
	english_notation varchar(128),
	-- 0:なし
	-- 1:あり
	-- 2:大学院のみあり
	-- ※「2:大学院のみあり」は現行データには存在しないが、DB定義書で必要とある
	master_course_flag varchar(1),
	-- 0:なし
	-- 1:あり
	-- 2:大学院のみあり
	-- ※「2:大学院のみあり」は現行データには存在しないが、DB定義書で必要とある
	doctor_course_flag varchar(1),
	-- 1:昼間
	-- 2:夜間
	day_night_flag varchar(1),
	wfot_certified_status smallint,
	origin_school_no int,
	-- 詳細検索画面で使用する
	mtdlp_cooperator_flag boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.facility_applications
(
	id bigserial NOT NULL,
	-- 1:養成校（大学）
	-- 2:養成校（短期大学）
	-- 3:養成校（専門学校）
	-- 4:養成校（大学院）
	-- 5:開設準備室
	-- 9:施設
	facility_category varchar(1),
	-- 00：正会員
	-- 01：仮会員
	member_category_cd char(2) NOT NULL,
	member_no int,
	temporary_member_no int,
	status smallint,
	opener_type_cd char(4) NOT NULL,
	opener_nm varchar(128),
	opener_nm_kana varchar(255),
	responsible_email_address varchar(128),
	facility_nm varchar(128) NOT NULL,
	facility_nm2 varchar(256),
	facility_nm_kana varchar(128) NOT NULL,
	facility_nm_kana2 varchar(256),
	zipcode varchar(20) NOT NULL,
	-- 01:北海道 ・・・ 47:沖縄、49:海外、50:不明
	facility_prefecture_cd char(2) NOT NULL,
	address1 varchar(128) NOT NULL,
	address2 varchar(128),
	address3 varchar(128),
	primary_phone_number varchar(36) NOT NULL,
	fax_number varchar(36),
	corporate_nm varchar(128),
	corporate_nm_kana varchar(128),
	department varchar(128),
	training_school_category_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.facility_application_categories
(
	id bigserial NOT NULL,
	applicant_id bigint NOT NULL,
	-- 1:医療関連
	-- 2:介護関連
	-- 3:障害総合支援法関連
	-- 4:その他
	category smallint,
	-- 1:大分類
	-- 2:中分類
	-- 3:小分類
	category_division smallint,
	parent_code varchar(32),
	category_code varchar(32),
	other varchar(100),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.facility_categories
(
	id bigserial NOT NULL,
	facility_id bigint NOT NULL,
	-- 1:医療関連
	-- 2:介護関連
	-- 3:障害総合支援法関連
	-- 4:その他
	category smallint,
	-- 1:大分類
	-- 2:中分類
	-- 3:小分類
	category_division smallint,
	parent_code varchar(32),
	category_code varchar(32),
	other varchar(100),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.facility_contacts
(
	id bigserial NOT NULL,
	-- 1:施設
	-- 2:養成校
	type smallint,
	member_no int,
	member_last_nm varchar(20),
	member_first_nm varchar(20),
	member_last_nm_kana varchar(20),
	member_first_nm_kana varchar(20),
	member_tel_no varchar(12),
	member_mail_address varchar(128),
	contact_category smallint,
	content text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.facility_members
(
	id bigserial NOT NULL,
	facility_id bigint NOT NULL,
	member_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.facility_training_school_update_histories
(
	id bigserial NOT NULL UNIQUE,
	facility_id bigint NOT NULL,
	update_no int,
	training_school_update_datetime timestamp,
	update_user_id varchar(32),
	-- 更新した処理の種類
	-- １：施設情報登録
	-- ２：施設情報変更
	-- ３：施設情報削除
	-- ４：養成校情報登録
	-- ５：養成校情報変更
	-- ６：養成校情報削除
	update_kind_cd smallint,
	process_content varchar(128),
	update_category_cd smallint,
	update_category_name varchar,
	table_cd smallint,
	table_nm varchar,
	field_cd smallint,
	field_nm varchar,
	update_date timestamp with time zone,
	content_before varchar,
	content_after varchar,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.fees
(
	id bigserial NOT NULL,
	type varchar(2),
	price int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.general_codes
(
	id varchar(32) NOT NULL,
	code varchar(32) NOT NULL,
	content varchar(128) NOT NULL,
	simple_content varchar(128),
	parent_id varchar(32),
	parent_code varchar(32),
	sort_order int DEFAULT 0 NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id, code)
) WITHOUT OIDS;


CREATE TABLE public.handbook_migration_applications
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL UNIQUE,
	application_date date,
	-- 00：正会員
	-- 01：仮会員
	member_category_cd char(2),
	-- 01:手帳移行
	-- 02:基礎ポイント移行
	migration_type_cd varchar(2),
	status_cd varchar(2),
	introduction_date date,
	introduction_check boolean,
	collaboration_date date,
	collaboration_check boolean,
	professional_ethics_date date,
	professional_ethics_check boolean,
	health_medical_care_date date,
	health_medical_care_check boolean,
	study_for_practice_date date,
	study_for_practice_check boolean,
	potential_date date,
	potential_check boolean,
	trend_date date,
	trend_check boolean,
	case_report_and_study_date date,
	case_report_and_study_check boolean,
	case_consider_date date,
	case_consider_check boolean,
	case_report_date date,
	case_report_check boolean,
	mtdlp_basic_date date,
	mtdlp_basic_check boolean,
	disability_field_date date,
	disability_field_check boolean,
	mental_disorder_field_date date,
	mental_disorder_field_check boolean,
	developmental_disorder_field_date date,
	developmental_disorder_field_check boolean,
	old_age_field_date date,
	old_age_field_check boolean,
	basic_training_point int,
	attachment_p3_file_nm varchar(50),
	attachment_p3_file bytea,
	attachment_p4_file_nm varchar(50),
	attachment_p4_file bytea,
	attachment_p5_file_nm varchar(50),
	attachment_p5_file bytea,
	attachment_last_page_file_nm varchar(50),
	attachment_last_page_file bytea,
	re_enrollment_flag varchar(1),
	sampling_flag varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.history_field_definitions
(
	id bigserial NOT NULL UNIQUE,
	table_definition_id bigint NOT NULL,
	field_cd smallint UNIQUE,
	field_nm varchar,
	field_physical_nm varchar,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.history_table_definitions
(
	id bigserial NOT NULL UNIQUE,
	table_cd smallint UNIQUE,
	table_nm varchar,
	table_physical_nm varchar UNIQUE,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.import_attendees
(
	id bigserial NOT NULL,
	member_no int,
	fullname varchar(50),
	attendance_recept1 varchar(50),
	leave_confirm1 varchar(50),
	attendance_recept2 varchar(50),
	leave_confirm2 varchar(50),
	attendance_recept3 varchar(50),
	leave_confirm3 varchar(50),
	pay_on_the_day varchar(50),
	entry_fee int,
	-- 「済」か、空欄かのいずれか？
	deposit varchar(50),
	-- 帳票レイアウトに「修了〇」、「未修了×」と書いてある
	completion_status varchar(50),
	-- 帳票レイアウトに「合格〇」と「不合格×」
	test_result varchar(50),
	-- 画面やレイアウト定義にないけど、エラーが出たときに分かるようにしておく必要があるのではないか、と思い追加した
	error_message text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.import_trainings
(
	id bigserial NOT NULL,
	-- ※ファイル取込者の会員番号。画面に出てこないが、データが迷子にならないようにフィールドとして持っておく
	member_no int,
	fiscal_year int,
	-- コード設計[30010]の内容から抽出(現行のコード値？)
	-- 01:基礎研修
	-- 02:認定OT
	-- 03:専門OT
	-- 04:重点
	-- 05:臨床指導者
	-- 21:特別
	-- 99:その他
	category_cd varchar(2),
	course_nm varchar(200),
	-- 研修会一覧画面の検索条件になっている。
	-- 
	-- コード設計[10011]
	-- 01:北海道
	-- ......
	-- 47:沖縄県
	-- 48:海外
	venue_pref_cd varchar(2),
	venue_place varchar(128),
	address1 varchar(32),
	address2 varchar(32),
	address3 varchar(32),
	days int,
	event_date1 date,
	-- 開催日２日目
	event_date2 date,
	-- 開催日３日目
	event_date3 date,
	end_date date,
	-- 新システムでの主催コードは2桁。
	-- 画面定義書にある帳票定義を見る限り、取込の場合には主催者コードを複数件セットすることは出来ないようだ（共催に出来ない）。
	organizer_cd varchar(2),
	-- 桁数の定義が不明なので適当なサイズにしておく
	organizer_nm varchar(128),
	-- 帳票項目定義によると「必須」か「任意」かの2種類が入るとのこと。
	entry_fee_deposit varchar(50),
	-- 帳票項目定義によると「あり」か「なし」の２種類。
	reception_on_the_day varchar(50),
	-- 帳票レイアウトを見ると「あり」か「なし」が入りそう
	acceptance varchar(50),
	-- おそらく「初日のみ」か「全日」の２パターン。
	-- （コード設計[30240]）
	attendance_recept varchar(50),
	-- コード設計によると「なし」か「あり」。
	leave_confirm varchar(50),
	capacity_count int,
	entry_fee int,
	instructor1_member_no int,
	instructor1_nm varchar(50),
	instructor1_point int,
	instructor2_member_no int,
	instructor2_nm varchar(50),
	instructor2_point int,
	instructor3_member_no int,
	instructor3_nm varchar(50),
	instructor3_point int,
	instructor4_member_no int,
	instructor4_nm varchar(50),
	instructor4_point int,
	instructor5_member_no int,
	instructor5_nm varchar(50),
	instructor5_point int,
	-- 取り込んだファイルのエラー内容が格納される
	error_message text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.import_training_applicants
(
	id bigserial NOT NULL,
	member_no int,
	fullname varchar(50),
	-- 画面レイアウトによると、完全一致は「登録されている会員の氏名と完全に一致しています。」という状況のようだ。
	match_flag boolean,
	-- 画面設計に以下の記述がある。
	-- 
	-- 「・100%・・・文字が同じで、名前の並び順が違う場合、表示されます。
	-- ・それ以降は一致している文字の割合（%）を表示します。
	-- 　（例）4文字中、3文字一致している ⇒ 75%
	-- 」
	-- 
	collation_rate decimal,
	-- 取り込んだファイルのエラー内容が格納される
	error_message text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.import_training_histories
(
	id bigserial NOT NULL,
	seq_no int,
	process_date date,
	-- ファイル名の機能的な制限は250文字だが、画面定義に合わせる
	excel_file_nm varchar(50),
	prefecture_cd varchar(2),
	-- 01:現職者　共通研修
	-- 02:現職者　選択研修
	-- 03:基礎ポイント研修
	-- 04:認定作業療法士 共通研修
	-- 05:認定作業療法士 選択研修
	-- 06:専門作業療法士 専門基礎研修
	-- 07:専門作業療法士 専門応用研修
	-- 08:専門作業療法士 専門研究・開発研修
	-- 09:基礎研修修了者　認定年月日
	-- 10:認定作業療法士　認定年月日
	-- 11:専門作業療法士　認定年月日
	-- 12:事例報告登録
	-- 13:臨床実習指導者研修修了者　認定年月日
	-- 14:臨床実習指導者講習会受講者　認定年月日
	-- 15:MTDLP基礎研修
	-- 16:士会研修会
	-- 17:士会裁量ポイント
	-- 18:講師(協会以外の研修会の講師）
	-- 19:eラーニング
	-- 20:全国研修会
	-- 21:学会
	-- 
	training_type varchar(2),
	attending_date date,
	member_count int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.import_training_points
(
	id bigserial NOT NULL,
	member_no int,
	fullname varchar(50),
	-- 画面レイアウトによると、完全一致は「登録されている会員の氏名と完全に一致しています。」という状況のようだ。
	match_flag boolean,
	collation_rate decimal,
	point int,
	-- 取り込んだファイルのエラー内容が格納される
	error_message text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.individual_billing_print_histories
(
	id bigserial NOT NULL,
	-- 画面案に入力箇所がないが、帳票レイアウトの出力順が「会員番号順」となっていたのでカラムとして入れた。
	-- 恐らく設計書のミスだが、検索性を考えると画面とDBに会員番号があっても良いような気がするので、念のため入れておく
	member_no int,
	billing_date date,
	-- 画面定義でサイズ指定がない。
	destination text,
	-- 01:様
	-- 02:御中
	honorifics_cd varchar(2),
	-- 画面設計でサイズが指定されていないのでtextにしておく
	proviso text,
	amount int,
	-- 英語で「理事」と「理事長」が同じDirectorになってしまう・・・
	director_nm varchar(50),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.info_by_fiscal_year
(
	id bigserial NOT NULL,
	facility_id bigint NOT NULL,
	-- 初期登録時の施設番号
	facility_base_no varchar(20),
	fiscal_year int NOT NULL,
	fiscal_year_update_user_id varchar(32),
	fiscal_year_update_date timestamp,
	update_no int,
	fiscal_year_create_date date,
	fiscal_year_delete_date date,
	admission_count int,
	-- 卒業者数の定員という言葉に違和感。
	-- 画面定義書を見ると入学定員より少ない値が入っていたりする
	graduate_count int,
	entrance_exam_count int,
	pass_exam_count int,
	pass_exam_rate numeric(4,1),
	join_rate numeric(4,1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.info_by_fiscal_year_temp
(
	ID bigserial NOT NULL,
	-- 同一の処理内で取り込まれたレコードに共通する番号を採番して設定する
	process_id int,
	fiscal_year int,
	admission_count int,
	facility_base_no varchar(20),
	entrance_exam_count int,
	pass_exam_count int,
	error_message text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE public.invoice_print_histories
(
	id bigserial NOT NULL,
	-- 発行する送り状の種類を保持する。
	-- コード値は未定だが、現状では以下のものがある
	-- 
	-- ＜会員管理関連、生涯教育関連、請求収納関連から連携＞
	-- ・発送先が自宅の場合
	-- ・発送先が施設の場合
	-- ・発送先が養成校の場合
	-- ＜施設一覧から連携した場合＞
	-- ・発送先が施設、施設情報責任者の場合
	-- ＜養成校一覧から連携した場合＞
	-- ・発送先が養成校、養成校情報責任者の場合
	-- ＜賛助会員一覧から連携した場合＞
	-- ・発送先が基本情報、基本情報代表者の場合
	-- ・発送先が請求担当者の場合
	-- ・発送先が送付担当者の場合
	-- ＜外部団体一覧から連携した場合＞
	-- ・発送先が外部団体一覧で選択された発送先の場合
	-- 
	invoice_kind_cd varchar(2),
	zipcode varchar(8),
	member_no_text varchar(6),
	prefecture_nm varchar(3),
	-- DBの住所１フィールドのサイズと異なるので注意
	address1 varchar(22),
	-- DBの住所２フィールドのサイズと異なるので注意
	address2 varchar(66),
	-- DBの住所３フィールドのサイズと異なるので注意
	address3 varchar(44),
	-- フリーフォーマット
	address4 varchar(50),
	-- サイズは帳票定義の長さに合わせた
	phone_number varchar(11),
	-- 名前や"作業療法士"といった肩書なども出力される。
	address_nm varchar(50),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.libraries
(
	id bigserial NOT NULL,
	title varchar(256),
	-- 公開範囲が「ファイル指定」の場合、公開対象の会員が「ライブラリ宛先」に格納される
	publish_range varchar(2),
	publish_range_file_id varchar(100),
	publish_range_file_nm varchar(512),
	category varchar(2),
	contents text,
	publish_date_from timestamp,
	publish_date_to timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.library_attachment_files
(
	id bigserial NOT NULL,
	library_id bigint NOT NULL,
	file_nm varchar(512),
	file bytea,
	display_file_nm varchar(256),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.library_destinations
(
	id bigserial NOT NULL,
	library_id bigint NOT NULL,
	member_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.library_destination_exclusions
(
	id bigserial NOT NULL,
	library_id bigint NOT NULL,
	member_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.library_view_histories
(
	id bigserial NOT NULL,
	library_id bigint NOT NULL,
	member_no int,
	-- 監査項目である共通項目とは別に、閲覧日時を保持する。
	view_date timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.lifelong_education_statuses
(
	id bigserial NOT NULL,
	member_no int NOT NULL UNIQUE,
	point int,
	used_point int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.lifelong_education_system_transition_data
(
	id bigserial NOT NULL,
	member_no_start int,
	member_no_end int,
	member_count int,
	real_member_count int,
	unsubscribe_count int,
	uncomplete_count int,
	in_basic_training_count int,
	basic_training_valid_count int,
	basic_training_expired_count int,
	qualification_valid_count int,
	qualification_expired_count int,
	qualification_graced_count int,
	qualification_count int,
	special_qualification_count int,
	new_qualification_count int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mail_attachments
(
	id bigserial NOT NULL,
	send_mail_id bigint NOT NULL,
	seq smallint,
	file_name varchar,
	mime_type varchar,
	content bytea,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mail_condition_prefs
(
	id bigserial NOT NULL,
	send_mail_id bigint NOT NULL,
	pref_code varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mail_member_destinations
(
	id bigserial NOT NULL,
	send_mail_id bigint NOT NULL,
	member_no int,
	email_address varchar(128),
	code varchar(20),
	error_cause text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mail_non_member_destinations
(
	id bigserial NOT NULL,
	send_mail_id bigint NOT NULL,
	email_address varchar(128),
	full_name varchar,
	error_cause text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mail_templates
(
	id bigserial NOT NULL UNIQUE,
	from_category int,
	subject varchar(128),
	content text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.members
(
	id bigserial NOT NULL,
	member_no int NOT NULL UNIQUE,
	status_no smallint NOT NULL,
	-- 01:北海道 ・・・ 47:沖縄、49:海外、50:不明
	affiliated_prefecture_cd char(2),
	-- パスワード再発行などで本人確認を行うためのコード。ユーザが自身で入力して設定する値
	member_cd varchar(13),
	last_name varchar(20),
	first_name varchar(20),
	last_kana_name varchar(20),
	first_kana_name varchar(20),
	-- 入会年月日
	entry_date date,
	web_member_name varchar(20),
	web_member_id varchar(50),
	web_member_password varchar(255),
	maiden_name varchar(10),
	-- 0：（未選択）
	-- 1：男性
	-- 2：女性
	gender smallint,
	-- 1：WFOT加入
	-- 2：WFOT未加入
	wfot_entry_status smallint,
	-- 会費残高
	membership_fee_balance int DEFAULT 0,
	-- 0：（未選択）
	-- 1：自宅
	-- 2：施設
	shipping_category_no smallint,
	age smallint,
	birth_day date,
	home_zipcode varchar(8),
	home_prefecture_cd char(2),
	home_address1 varchar(32),
	home_address2 varchar(32),
	home_address3 varchar(32),
	home_phone_number varchar(36),
	mobile_phone_number varchar(36),
	-- 発送区分が「海外」の場合
	foreign_address text,
	bill_to_japan_address boolean DEFAULT 'FALSE' NOT NULL,
	work_phone_number varchar(36),
	-- チェックしたデータは名簿に住所・電話番号が印刷されません
	print_enabled boolean DEFAULT 'FALSE' NOT NULL,
	email_address varchar(128),
	-- 1：受取OK
	-- 2：受取NG
	ot_association_mail_receive_cd smallint DEFAULT 0,
	training_school_no int,
	alma_mater_name varchar(20),
	qualification_year int,
	license_number varchar(20),
	-- TRUE：外免
	foreign_license boolean DEFAULT 'FALSE' NOT NULL,
	-- 生涯教育研修所得単位数の合計
	education_credits int DEFAULT 0,
	-- 再入会情報など
	remarks text,
	-- ０１：働いている
	-- ０２：働いていないが大学院生等
	-- ０３：働いていない
	working_state_cd char(2),
	-- 勤務先の都道府県番号
	working_prefecture_cd char(2),
	pre_working_prefecture_cd char(2),
	working_facility_cd varchar(20),
	facility_auth_category_cd varchar(4),
	working_facility_unit_name varchar(128),
	-- 11：常勤
	-- 12：非常勤
	working_condition varchar(2),
	-- データ移行（2.0次未使用）
	working_condition_facility_count int,
	-- データ移行（2.0次未使用）
	working_condition_facility_name varchar(40),
	-- １：医療関連
	-- ２：介護関連
	-- ３：障害総合支援関連
	-- ４：その他関連
	-- 
	facility_domain_cd varchar(2),
	-- １：医療関連
	-- ２：介護関連
	-- ３：障害総合支援関連
	-- ４：その他関連
	-- 
	facility_sub_domain_cd varchar(2),
	facility_handicap_type varchar(4),
	facility_target_disease varchar(7),
	facility_sub_handicap_type varchar(4),
	facility_sub_target_disease varchar(7),
	other_facility1_name varchar(20),
	other_facility1_domain varchar(10),
	other_facility2_name varchar(20),
	other_facility2_domain varchar(10),
	-- WEB掲載フラグ
	web_published boolean DEFAULT 'FALSE' NOT NULL,
	update_from_portal_date date,
	pref_association_no smallint,
	-- 1：受取OK
	-- 2：受取NG
	pref_association_mail_receive_cd smallint DEFAULT 0,
	pref_association_prefecture_cd char(2),
	pref_association_branch_cd int,
	pref_association_shipping_category_no smallint,
	member_list boolean DEFAULT 'FALSE' NOT NULL,
	-- チェックされたときは県士会に会員情報を開示しない
	pref_association_member_info_hide boolean DEFAULT 'FALSE' NOT NULL,
	-- チェックされたときは県士会に生涯教育情報を開示しない
	-- 
	pref_association_education_info_hide boolean DEFAULT 'FALSE' NOT NULL,
	-- 県士会会費残高
	pref_association_fee_balance int,
	-- チェックされたときは名簿に施設を出力しない
	-- 
	facility_info_hide boolean,
	-- 県士会で使用する、使用方法は県士会で考える
	pref_association_value int NOT NULL,
	-- 県士会で使用する、県士会未入会者はチェックする
	pref_association_nonmember boolean DEFAULT 'FALSE' NOT NULL,
	transfer_confirmation boolean DEFAULT 'FALSE' NOT NULL,
	membership_certificate_issuanced boolean DEFAULT 'FALSE' NOT NULL,
	has_degree boolean DEFAULT 'FALSE' NOT NULL,
	pref_association_update_date date,
	target_independence_support_category_cd char(2),
	target_primary_purpose varchar(4),
	target_age int,
	target_ot_day_count int,
	honorary_membership boolean DEFAULT 'FALSE' NOT NULL,
	honorary_membership_acquired_year int,
	previous_member_no int,
	child_welfare_facility char(1),
	child_welfare_service_category_cd char(4),
	disaster_support_volunteerable boolean DEFAULT 'FALSE' NOT NULL,
	belong_pref_association boolean DEFAULT 'FALSE' NOT NULL,
	-- 退会年月日
	withdrawal_date date,
	-- １：電子
	-- ２：製本
	-- ３：発送不要
	journal_send_category_cd char(1),
	mtdlp_rehabilitation_addition boolean DEFAULT 'FALSE' NOT NULL,
	degree1_no int,
	degree1_field_name varchar(64),
	degree1_award_year int,
	degree1_education_facility_name varchar(64),
	degree2_no int,
	degree2_field_name varchar(64),
	degree2_award_year int,
	degree2_education_facility_name varchar(64),
	degree3_no int,
	degree3_field_name varchar(64),
	degree3_award_year int,
	degree3_education_facility_name varchar(64),
	degree4_no int,
	degree4_field_name varchar(64),
	degree4_award_year int,
	degree4_education_facility_name varchar(64),
	degree5_no int,
	degree5_field_name varchar(64),
	degree5_award_year int,
	degree5_education_facility_name varchar(64),
	-- パスワード再発行で本人確認用に使うコード。ユーザが自身で手入力して設定する
	security_cd varchar(3),
	disclosed boolean DEFAULT 'FALSE',
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.membership_fee_deliveries
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	fiscal_year int,
	-- 1：入会金
	-- 2：年会費
	-- 3：WFOT
	-- 4：再入会手数料
	category_cd varchar(2),
	billing_amount int,
	payment_amount int,
	payment_date date,
	import_date date,
	billing_balance int,
	remarks varchar(128),
	-- １：郵便振替
	-- ２：コンビニエンス
	-- ９：その他
	payment_type varchar(1),
	-- １：速報
	-- ２：確報
	-- ９：その他
	data_type varchar(1),
	application_date date,
	-- １：会費免除
	-- ２：分納
	-- ３：支払い猶予
	application_content varchar(1),
	application_status varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_applications
(
	id bigserial NOT NULL,
	temporary_member_no int NOT NULL UNIQUE,
	-- １：WEB
	-- ２：事務局
	register_category_cd char(1) NOT NULL,
	last_name varchar(20),
	first_name varchar(20),
	last_kana_name varchar(20),
	first_kana_name varchar(20),
	-- 0：（未選択）
	-- 1：男性
	-- 2：女性
	gender smallint,
	-- 0：（未選択）
	-- 1：自宅
	-- 2：施設
	shipping_category_no smallint,
	age smallint,
	birth_day date,
	training_school_no int,
	qualification_year int,
	license_number varchar(20),
	-- TRUE：外免
	foreign_license boolean DEFAULT 'FALSE' NOT NULL,
	degree_no int,
	home_zipcode varchar(8),
	home_prefecture_cd char(2),
	home_address1 varchar(32),
	home_address2 varchar(32),
	home_address3 varchar(32),
	-- 発送区分が「海外」の場合
	foreign_address text,
	bill_to_japan_address boolean DEFAULT 'FALSE' NOT NULL,
	home_phone_number varchar(36),
	mobile_phone_number varchar(36),
	email_address varchar(128),
	-- ０１：働いている
	-- ０２：働いていないが大学院生等
	-- ０３：働いていない
	working_state_cd char(2) NOT NULL,
	working_facility_cd varchar(20),
	working_facility_unit_name varchar(128),
	-- 11：常勤
	-- 12：非常勤
	working_condition varchar(2),
	-- １：医療関連
	-- ２：介護関連
	-- ３：障害総合支援関連
	-- ４：その他関連
	-- 
	facility_domain_cd varchar(2),
	-- １：医療関連
	-- ２：介護関連
	-- ３：障害総合支援関連
	-- ４：その他関連
	-- 
	facility_sub_domain_cd varchar(2),
	facility_handicap_type varchar(4),
	facility_sub_handicap_type varchar(4),
	facility_target_disease varchar(7),
	facility_sub_target_disease varchar(7),
	-- コード設計には存在しない
	-- 現行1.5次のDBにある値は以下のもの
	-- 0:登録
	-- 8:処理済
	-- 9:削除
	status_cd char(1) NOT NULL,
	-- ０：再
	-- １：旧再
	-- ２：新
	-- ３：喪失
	entry_category_cd char(1),
	member_no int,
	temporary_password varchar(255),
	billing_date date,
	deposit_date date,
	-- 入会年月日
	entry_date date,
	fiscal_year int,
	on_hold boolean DEFAULT 'FALSE' NOT NULL,
	license_image_file_nm varchar(128),
	license_image_file bytea,
	entry_fee int,
	annual_fee int,
	setting_create_date timestamp,
	billing_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_app_facility_categories
(
	id bigserial NOT NULL,
	temporary_member_no int NOT NULL,
	-- 1:大分類
	-- 2:中分類（主）
	-- 3:小分類
	-- 4:中分類（従）
	category_division smallint,
	-- 1:主
	-- 2:従
	primary_division smallint,
	parent_code varchar(32),
	category_code varchar(32),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_app_local_activities
(
	id bigserial NOT NULL,
	temporary_member_no int NOT NULL,
	-- 入会申し込みの自治体活動内容の連番
	activity_seq int NOT NULL,
	participation boolean DEFAULT 'FALSE' NOT NULL,
	municipality_name varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_app_qualifications
(
	id bigserial NOT NULL,
	temporary_member_no int NOT NULL,
	qualification_cd varchar(4) NOT NULL,
	-- １：国家資格
	-- ２：その他関連資格
	qualification_category_cd varchar(2) NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_email_address_histories
(
	id bigserial NOT NULL,
	member_no int,
	email_address varchar(128),
	temp_flag boolean DEFAULT 'FALSE' NOT NULL,
	-- URLからの呼び出しパラメータと照合するキー
	temp_key varchar(32),
	temp_expiration_datetime timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_facility_categories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- 1:大分類
	-- 2:中分類（主）
	-- 3:小分類
	-- 4:中分類（従）
	category_division smallint,
	-- 1:主
	-- 2:従
	primary_division smallint,
	parent_code varchar(32),
	category_code varchar(32),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_local_activities
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- 入会申し込みの自治体活動内容の連番
	activity_seq int NOT NULL,
	participation boolean DEFAULT 'FALSE' NOT NULL,
	municipality_name varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_login_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	login_datetime timestamp,
	logout_datetime timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_password_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	password varchar(255) NOT NULL,
	temp_password_flag boolean DEFAULT 'FALSE' NOT NULL,
	temp_password varchar(32),
	temp_password_expiration_datetime timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_qualifications
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	qualification_cd varchar(4) NOT NULL,
	-- １：国家資格
	-- ２：その他関連資格
	qualification_category_cd varchar(2) NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_remarks_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	seq int NOT NULL,
	remarks text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_returned_item_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	publication_cd varchar(4),
	process_date date,
	postcard_send_date date,
	returned_reason_cd varchar(2),
	resend_date date,
	remarks varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


-- 権限コード値に対応するパス、操作のキー値をブラックリスト形式で保持する。
CREATE TABLE public.member_revoked_authorities
(
	id bigserial NOT NULL,
	code_id varchar(32) DEFAULT '10100' NOT NULL,
	code varchar(32) NOT NULL,
	path_or_key varchar(255) NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_roles
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	role_code_id varchar(32) DEFAULT '10100' NOT NULL,
	role_code varchar(32) NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (member_no, role_code_id, role_code)
) WITHOUT OIDS;


CREATE TABLE public.member_state_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	state_cd varchar(1),
	new_member_no int,
	old_member_no int,
	-- 退会年月日
	withdrawal_date date,
	-- 入会年月日
	entry_date date,
	restoration_date date,
	renewal_date date,
	disposed boolean DEFAULT 'FALSE' NOT NULL,
	remarks varchar(128),
	disqualification_date date,
	last_enrollment_fiscal_year int,
	unpaid_fee int,
	re_entry_fee int,
	disqualification_practice_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_update_histories
(
	id bigserial NOT NULL UNIQUE,
	update_no int,
	member_update_datetime timestamp,
	update_user_id varchar(32),
	member_no int,
	paot_cd smallint,
	update_category_cd smallint,
	update_date timestamp with time zone,
	table_cd smallint,
	table_nm varchar,
	field_cd smallint,
	field_nm varchar,
	content_before varchar,
	content_after varchar,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_withdrawal_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	withdrawal_type varchar(1),
	fiscal_year int,
	state_cd varchar(1),
	reception_date date,
	-- 退会年月日
	withdrawal_date date,
	-- 会費残高
	member_fee int,
	withdrawal_reason varchar(4),
	cancellation_date date,
	remarks varchar(128),
	print_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.monthly_processes
(
	id bigserial NOT NULL,
	-- 01:MTDLP研修修了者
	-- 02:MTDLP指導者
	-- 03:認定作業療法士終身対象者
	-- 
	extraction_type_cd varchar(2),
	-- YYYYMM
	process_month varchar(6),
	process_date date,
	member_no int,
	-- 会員の氏名が姓:20 名:20なので、間に入れるスペースもあることだし、余裕をもって50で定義
	member_nm varchar(50),
	extraction_date date,
	qualification_issue_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mtdlp_case_reports
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	receipt_number int NOT NULL,
	presentation_cd varchar(2),
	title varchar(100),
	overall_judge_date date,
	withdrawn boolean DEFAULT 'FALSE' NOT NULL,
	publish_date date,
	judge_start_date date,
	email_sent boolean DEFAULT 'FALSE' NOT NULL,
	pass_status_cd smallint,
	withdraw_status_cd smallint,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mtdlp_qualify_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- 研修、講習会、厚生労働省認定
	training_type varchar(2),
	qualification_date date,
	qualification_number int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mtdlp_summaries
(
	id bigserial NOT NULL,
	member_no int NOT NULL UNIQUE,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.mtdlp_training_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス
	training_cd varchar(16),
	fiscal_year varchar(4),
	promoter_cd varchar(2),
	category_cd varchar(2),
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field_cd varchar(2),
	group_cd varchar(2),
	class_cd varchar(2),
	branch_cd varchar(2),
	seq int NOT NULL,
	attend_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.notices
(
	id bigserial NOT NULL,
	target smallint,
	category smallint,
	priority boolean,
	publish_from timestamp,
	publish_to timestamp,
	system_message boolean,
	destination int,
	subject varchar(128),
	content text,
	destination_type int,
	destination_file_nm varchar(128),
	destination_range_facility boolean DEFAULT 'FALSE' NOT NULL,
	destination_range_training_school boolean DEFAULT 'FALSE' NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.notice_condition_prefs
(
	id bigserial NOT NULL,
	notice_id bigint NOT NULL,
	pref_code varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.notice_destinations
(
	id bigserial NOT NULL,
	notice_id bigint NOT NULL,
	member_no int,
	read_flg boolean,
	read_time timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.officer_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	position_cd varchar(4),
	position_type smallint,
	-- 役職が代議員の場合のみ使用
	prefecture_cd char(2),
	start_appoint_date date,
	end_appoint_date date,
	remarks varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.other_org_point_applications
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	application_date date,
	-- 01:会員
	-- 02:事務局
	applicant_cd varchar(2),
	status_cd varchar(2),
	attending_date_from date,
	attending_date_to date,
	-- 重複チェックとのことだが、何の重複なのか不明
	attending_date_check boolean,
	attending_days int,
	attending_days_check boolean,
	organizer_cd varchar(2),
	organizer_check boolean,
	organizer_tel_no varchar(15),
	-- 他団体・SIGマスタのIDを持つ。東コロ様のテーブル定義書ではserialで定義されているが、他と合わせてbigserialと想定し、bigint型の外部キーとする
	other_organization_sig_cd bigint,
	other_organization_sig_check boolean,
	thema varchar(100),
	point_category_cd varchar(2),
	point_category_check boolean,
	point int,
	point_check boolean,
	attachment_file_check boolean,
	-- 一度でも承認した場合は"1"
	approval_flag varchar(1),
	secretariat_confirm_date date,
	-- 1:仮登録
	temp_flag varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.other_org_point_application_attachments
(
	id bigserial NOT NULL,
	application_id bigint NOT NULL,
	-- ファイルの順序
	file_order smallint,
	file_nm varchar(50),
	file bytea,
	-- コードマスタ
	file_reject_cd varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_activity_results
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	present_date date,
	-- 01:単著
	-- 02:単独
	-- 03:共著（主）
	-- 04:共同（主）
	-- 05:共著（共）
	-- 06:共同（共）
	-- 07:単訳
	-- 08:共訳
	-- 
	presenter_category_cd varchar(2),
	title varchar(50),
	-- A:Ａ：脳血管疾患等
	-- B:Ｂ：心大血管疾患
	-- C:Ｃ：呼吸器疾患
	-- D:Ｄ：運動器疾患
	-- E:Ｅ：神経難病
	-- F:Ｆ：がん
	-- G:Ｇ：内科疾患
	-- H:Ｈ：精神障害
	-- I:Ｉ：発達障害
	-- J:Ｊ：高齢期
	-- K:Ｋ：認知障害（高次脳機能障害を含む）
	-- L:Ｌ：援助機器
	-- M:Ｍ：生活行為向上マネジメント
	-- N:Ｎ：地域
	-- O:Ｏ：理論
	-- P:Ｐ：基礎研究
	-- Q:Ｑ：管理運営
	-- R:Ｒ：教育
	-- S:Ｓ：その他
	-- 
	category varchar(2),
	select_check boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_case_report_registrations
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	seq_no int,
	theme varchar(50),
	report_date date,
	report_select_check boolean,
	deficiency_check boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_clinical_training_reports
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	report_file_nm varchar(50),
	report_file bytea,
	select_check boolean,
	deficiency_check boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_jaot_activity_results
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	activity_date date,
	content varchar(50),
	select_check boolean,
	deficiency_check boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_junior_training_experiences
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	implementation_date date,
	theme varchar(50),
	attachment_file_nm varchar(50),
	attachment_file bytea,
	select_check boolean,
	deficiency_check boolean,
	file_deficiency_check boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_papers
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	-- 01:日本作業療法学会
	-- 02:ISSN/ISBN登録の雑誌・書籍
	-- 03:学術誌作業療法（研究論文，実践報告）
	-- 04:WFOT学会
	-- 05:APOTEC学会
	-- 06:WFOT加盟国の協会が発行する機関誌（原著論文）
	-- 
	paper_presentation_cd varchar(2),
	remarks varchar(100),
	other varchar(100),
	select_check boolean,
	deficiency_check boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_paper_presentations
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	attachment_file_nm varchar(50),
	attachment_file bytea,
	select_check boolean,
	deficiency_check boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_selected_trainings
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	seq_no int,
	content_cd varchar(1),
	attending_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.ot_training_reports
(
	id bigserial NOT NULL,
	qualificate_ot_application_id bigint NOT NULL,
	training_date date,
	theme varchar(50),
	select_check boolean,
	deficiency_check boolean,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.paot_membership_fee_deliveries
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- 表示順制御に使ってください
	seq_no int,
	paot_cd smallint NOT NULL,
	fisical_year int,
	-- 1：入会金
	-- 2：年会費
	-- 3：WFOT
	-- 4：再入会手数料
	category_cd varchar(2),
	billing_amount int,
	payment_amount int,
	billing_balance int,
	payment_date date,
	remarks varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.paot_membership_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	update_seq_no int,
	paot_cd smallint,
	start_enroll_date date,
	end_enroll_date date,
	remarks varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.paot_officer_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	paot_cd smallint NOT NULL,
	position_cd int,
	department_cd int,
	start_appoint_date date,
	end_appoint_date date,
	remarks varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.period_extension_applications
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	application_date date,
	-- 01:基礎研修修了者
	-- 02:認定作業療法士
	qualification_cd varchar(2),
	-- 01:留学
	-- 02:海外勤務
	-- 03:出産・育児休暇
	-- 04:介護休暇
	-- 05:長期病気療養
	-- 06:その他
	-- 
	application_reason_cd varchar(2),
	application_reason_other varchar(100),
	-- 501:申請中
	-- 502:許可
	-- 503:書類不備
	-- 504:保留
	-- 505:取下げ
	-- 
	application_result_cd varchar(3),
	-- 501:事務局承認待ち
	-- 502:許可済み
	-- 503:不許可
	-- 
	application_status_cd varchar(3),
	secretariat_confirm_date date,
	rest_period_from date,
	rest_period_to date,
	certificate_file_nm varchar(50),
	certificate_file bytea,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.point_application_sampling_settings
(
	id bigserial NOT NULL,
	-- 01:他団体・SIGポイント申請 02:手帳移行申請 03:臨床実習ポイント申請
	application_type_cd varchar(2),
	-- 次回サンプリング実施予定日時
	next_exec_datetime timestamp,
	-- サンプリング調査する対象を抽出する割合（％）
	extraction_ratio decimal,
	-- Not Null, DEFAULT=FALSE
	executed boolean DEFAULT 'FALSE' NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.print_names
(
	id bigserial NOT NULL,
	-- 確証はないがコード値はこれか
	-- 01:印刷待ち
	-- 02:印刷済み
	-- 03:取下げ
	-- 
	status_cd varchar(2),
	use_start_date date,
	use_end_date date,
	print_position_nm varchar(20),
	print_nm varchar(20),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.professional_ot_attending_summaries
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field varchar(2) NOT NULL,
	qualification_date date,
	qualification_period_date date,
	qualification_number int,
	renewal_count int,
	dementia_care_specialist boolean DEFAULT 'FALSE' NOT NULL,
	dementia_care_trained boolean NOT NULL,
	teacher_license boolean DEFAULT 'FALSE' NOT NULL,
	qualified_visiting_ot boolean DEFAULT 'FALSE' NOT NULL,
	visit_rehabilitation_training123 boolean DEFAULT 'FALSE' NOT NULL,
	lymphedema_therapist boolean DEFAULT 'FALSE' NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (member_no, professional_field)
) WITHOUT OIDS;


CREATE TABLE public.professional_ot_qualify_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field varchar(2) NOT NULL,
	seq int,
	qualification_date date,
	qualification_number int,
	qualification_period_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (member_no, professional_field, seq)
) WITHOUT OIDS;


CREATE TABLE public.professional_ot_test_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field varchar(2) NOT NULL,
	test_date date,
	acceptance_cd varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.professional_ot_training_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス
	training_cd varchar(16),
	fiscal_year varchar(4),
	promoter_cd varchar(2),
	category_cd varchar(2),
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field varchar(2) NOT NULL,
	group_cd varchar(2),
	class_cd varchar(2),
	branch_cd varchar(2),
	seq int,
	training_name varchar(255),
	attend_date date,
	professional_rd_category_cd varchar(2),
	remission_cause_cd varchar(2),
	remission_qualification_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.qualificate_ot_applications
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	application_date date,
	-- 01:会員
	-- 02:事務局
	applicant_cd varchar(2),
	-- １：新規
	-- ２：更新
	application_type varchar(1),
	application_fiscal_year int,
	-- 201:委員会審査待ち
	-- 202:理事会承認待ち
	-- 203:要件充足アップ待ち
	-- 204:合格
	-- 
	application_status_cd varchar(3),
	-- 他と合わせて3桁で定義
	application_result_cd varchar(3),
	renewal_count int,
	point int,
	secretariat_confirm_date date,
	committee_approval_date date,
	director_approval_date date,
	doc_deficiency_mail_address varchar(50),
	-- コードマスタに存在しない項目。他と合わせて2桁
	doc_deficiency_reason_cd varchar(2),
	doc_deficiency_mail_content text,
	education_law_attending_date date,
	exemption_education_law_file_nm varchar(50),
	exemption_education_law_file bytea,
	exemption_education_file_check boolean,
	management_attending_date date,
	study_law_attending_date date,
	exemption_study_law_file_nm varchar(50),
	exemption_study_law_file bytea,
	exemption_study_law_file_check boolean,
	instructor_attending_date date,
	affiliation_certificate_file_nm varchar(50),
	affiliation_certificate_file bytea,
	affiliation_certificate_file_check boolean,
	work_experience_certificate_file_nm varchar(50),
	work_experience_certificate_file bytea,
	work_experience_certificate_file_check boolean,
	-- コード値の詳細は不明
	application_pattern_cd varchar(1),
	clinical_ability_examination_date date,
	other_sig_qualification_cd varchar(4),
	other_sig_qualification_date date,
	other_sig_qualification_file_name varchar(50),
	other_sig_qualification_file_check boolean,
	force_display_end_date date,
	-- 画面の「更新申請中、資格あり」チェックと対応
	force_display_check boolean,
	next_examination_date date,
	resubmit_period date,
	notification_flag varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.qualifications
(
	id bigserial NOT NULL,
	code varchar(4) NOT NULL,
	-- 01：国家資格
	-- 02：福祉系国家資格
	-- 02：その他リハビリテーション関連資格
	category_cd varchar(2) NOT NULL,
	name varchar(50),
	old_code varchar(4),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT code_qualification_category_cd UNIQUE (code, category_cd)
) WITHOUT OIDS;


CREATE TABLE public.qualification_no_seqs
(
	id bigserial NOT NULL,
	category_cd varchar(2),
	-- コード設計[30010]から専門分野の値を抽出
	-- 
	-- 00:なし
	-- 01:福祉用具
	-- 02:認知症
	-- 03:手外科
	-- 04:特別支援
	-- 05:高次脳
	-- 06:精神急性
	-- 07:摂食嚥下
	-- 08:訪問
	-- 09:がん
	-- 10:就労支援
	-- 
	field_cd varchar(2),
	group_cd varchar(2),
	class_cd varchar(2),
	seq_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.qualification_ot_app_histories
(
	id bigserial NOT NULL,
	member_no int,
	-- No1から5の連番
	seq int,
	application_pattern varchar(1),
	case_report1_theme varchar(100),
	case_report1_date varchar(100),
	case_report2_theme varchar(100),
	case_report2_date varchar(100),
	case_report3_theme varchar(100),
	case_report3_date varchar(100),
	activity_result_jaot1_date date,
	activity_result_jaot1_content varchar(100),
	activity_result_jaot2_date date,
	activity_result_jaot2_content varchar(100),
	activity_result_jaot3_date date,
	activity_result_jaot3_content varchar(100),
	activity_result_jaot4_date date,
	activity_result_jaot4_content varchar(100),
	activity_result_jaot5_date date,
	activity_result_jaot5_content varchar(100),
	activity_result1_presentation_date date,
	activity_result1_role_cd varchar(2),
	activity_result1_title varchar(100),
	activity_result1_category_cd varchar(2),
	activity_result2_presentation_date date,
	activity_result2_role_cd varchar(2),
	activity_result2_title varchar(100),
	activity_result2_category_cd varchar(2),
	activity_result3_presentation_date date,
	activity_result3_role_cd varchar(2),
	activity_result3_title varchar(100),
	activity_result3_category_cd varchar(2),
	activity_result4_presentation_date date,
	activity_result4_role_cd varchar(2),
	activity_result4_title varchar(100),
	activity_result4_category_cd varchar(2),
	activity_result5_presentation_date date,
	activity_result5_role_cd varchar(2),
	activity_result5_title varchar(100),
	activity_result5_category_cd varchar(2),
	clinical_training_capacity_test_date date,
	other_sig_qualification_cd varchar(2),
	other_sig_qualification_date date,
	other_sig_qualification_file_name varchar(50),
	other_sig_qualification_file_data bytea,
	clinical_training_report1_member_no int,
	clinical_training_report1_file_name varchar(50),
	clinical_training_report1_file_data bytea,
	clinical_training_report2_member_no int,
	clinical_training_report2_file_name varchar(50),
	clinical_training_report2_file_data bytea,
	clinical_training_report3_member_no int,
	clinical_training_report3_file_name varchar(50),
	clinical_training_report3_file_data bytea,
	clinical_training_report4_member_no int,
	clinical_training_report4_file_name varchar(50),
	clinical_training_report4_file_data bytea,
	clinical_training_report5_member_no int,
	clinical_training_report5_file_name varchar(50),
	clinical_training_report5_file_data bytea,
	paper1_presentation_cd varchar(2),
	paper1_remarks varchar(100),
	paper1_other varchar(100),
	paper2_presentation_cd varchar(2),
	paper2_remarks varchar(100),
	paper2_other varchar(100),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (member_no, seq)
) WITHOUT OIDS;


CREATE TABLE public.qualified_ot_attending_summaries
(
	id bigserial NOT NULL,
	member_no int NOT NULL UNIQUE,
	qualification_date date,
	qualification_period_date date,
	qualification_number int,
	renewal_count int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.qualified_ot_completion_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	seq int,
	completion_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (member_no, seq)
) WITHOUT OIDS;


CREATE TABLE public.qualified_ot_training_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス
	training_cd varchar(16),
	fiscal_year varchar(4),
	promoter_cd varchar(2),
	category_cd varchar(2),
	-- 福祉用具、認知症、手外科、特別支援教育・・
	professional_field_cd varchar(2),
	group_cd varchar(2),
	class_cd varchar(2),
	branch_cd varchar(2),
	seq int NOT NULL,
	training_name varchar(255),
	attend_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.questionnaires
(
	ID bigserial NOT NULL UNIQUE,
	subject varchar(128),
	content text,
	link text,
	term_from timestamp,
	term_to timestamp,
	publish_target smallint,
	category smallint,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE public.questionnaire_destinations
(
	ID bigserial NOT NULL,
	questionnaire_id bigint NOT NULL,
	member_no int,
	read_flg boolean,
	read_time timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (ID)
) WITHOUT OIDS;


CREATE TABLE public.receipt_print_histories
(
	id bigserial NOT NULL,
	deposit_id bigint NOT NULL,
	seq_no int,
	member_no int,
	receipt_no varchar(10),
	last_name varchar(20),
	first_name varchar(20),
	fiscal_year int,
	deposit_date date,
	deposit_amount int,
	receipt_amount int,
	receipt_issue_count int,
	receipt_issue_date_text varchar(8),
	receipt_address1 varchar(30),
	receipt_address2 varchar(30),
	receipt_date date,
	receipt_proviso1 varchar(30),
	receipt_proviso2 varchar(30),
	-- 領収書の宛先種別の選択状態を保持する
	-- コード定義が存在しないが画面には以下の値がある
	-- ・会員名
	-- ・施設名
	-- ・施設名＋会員名
	-- ・明細部入力値
	receipt_address_cd varchar(1),
	-- 領収書の日付種別の選択状態を保持する
	-- コード定義が存在しないが画面には以下の値がある
	-- ・入金日
	-- ・明細部入力値
	-- ・一括指定日
	-- 
	receipt_date_cd varchar(1),
	-- 領収書の領収書但し書き種別の選択状態を保持する
	-- コード定義が存在しないが画面には以下の値がある
	-- ・領収書種別
	-- ・明細部入力値
	-- ・一括指定値
	receipt_proviso_cd varchar(1),
	-- 領収書の領収書金額種別の選択状態を保持する
	-- コード定義が存在しないが画面には以下の値がある
	-- ・入金額
	-- ・明細部入力値
	receipt_amount_cd varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.recess_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	times smallint NOT NULL,
	process_status varchar(1),
	recess_reason varchar(2),
	reject_reason varchar(1),
	start_recess_date date,
	end_recess_date date,
	withdrawal_process_status varchar(1),
	withdrawal_date date,
	withdrawal_reason varchar(64),
	comeback_date date,
	print_date date,
	work_place_data_deletion boolean DEFAULT 'FALSE' NOT NULL,
	certificate_received boolean DEFAULT 'FALSE' NOT NULL,
	remarks varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.region_related_large_classifications
(
	id bigserial NOT NULL,
	code_id varchar(32) NOT NULL,
	code varchar(32) NOT NULL,
	content varchar(128) NOT NULL,
	simple_content varchar(128),
	sort_order int DEFAULT 0 NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.region_related_middle_classifications
(
	id bigserial NOT NULL,
	code_id varchar(32) NOT NULL,
	code varchar(32) NOT NULL,
	content varchar(128) NOT NULL,
	simple_content varchar(128),
	parent_code varchar(32),
	sort_order int DEFAULT 0 NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.region_related_small_classifications
(
	id bigserial NOT NULL,
	code_id varchar(32) NOT NULL,
	code varchar(32) NOT NULL,
	content varchar(128) NOT NULL,
	simple_content varchar(128),
	parent_code varchar(32),
	sort_order int DEFAULT 0 NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.remarks_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	remarks text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.reminder_print_histories
(
	id bigserial NOT NULL,
	billing_id bigint NOT NULL,
	-- 10桁の連番 + 6桁の会員番号
	billing_no varchar(16),
	issue_date date,
	president_nm varchar(50),
	director_nm varchar(50),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.reprint_applications
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	application_date date,
	-- 01:会員
	-- 02:事務局
	applicant_cd varchar(2),
	-- 01:基礎研修修了証
	-- 02:認定作業療法士認定証（終身含む）
	-- 03:専門作業療法士認定証
	-- 04:臨床実習指導者研修修了者修了証
	-- 05:臨床実習指導施設認定証
	-- 06:理学療法士作業療法士臨床実習指導者講習会修了証
	-- 07:WFOT認定証(OT協会)
	-- 08:MTDLP研修修了者修了証
	-- 09:MTDLP指導者認定証　
	-- 10:領収証
	-- 
	print_category_cd varchar(2),
	-- 会員の氏名が姓:20 名:20なので、間に入れるスペースもあることだし、余裕をもって50で定義
	member_nm varchar(50),
	member_birthday date,
	-- 01:身体障害
	-- 02:精神障害
	-- 03:発達障害
	-- 04:老年期障害
	-- 05:その他
	-- 
	field_cd varchar(2),
	origin_school_no int,
	facility_no varchar(20),
	-- 施設名１(128)と２(128)を足したサイズ
	facility_nm varchar(256),
	qualification_no int,
	qualification_date date,
	print_fee int,
	-- 01:印刷待ち
	-- 02:印刷済み
	-- 03:取下げ
	-- 
	print_status_cd varchar(1),
	-- 何の登録日なのかはっきりしない。
	-- 画面でも表示している申請日と何が違うのか。
	create_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.reward_punish_histories
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	reward_punish_cd char(2),
	-- １：表彰
	-- ２：罰則
	reward_punish_type smallint,
	fiscal_year int,
	remarks varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.send_mails
(
	id bigserial NOT NULL,
	from_category int,
	destination int,
	subject varchar(128),
	content text,
	send_status smallint,
	send_reserve_date timestamp,
	send_date timestamp,
	result_date timestamp,
	mail_process_id bigint,
	-- コード設計には存在しない（画面定義にあるコード値）
	-- 1:手動
	-- 2:自動
	mail_type_cd int,
	-- メールテンプレートのIDが設定される
	-- 旧システムはphpファイルでテンプレートが定義されていて、t_mailにはそのテンプレートを示すIDが保持されている
	auto_send_id int,
	destination_type int,
	destination_file_nm varchar(128),
	destination_range_facility boolean DEFAULT 'FALSE' NOT NULL,
	destination_range_training_school boolean DEFAULT 'FALSE' NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.sig_other_groups
(
	id bigserial NOT NULL,
	-- 01:学会・研究会
	-- 02:SIG
	-- 03:関連・職能団体
	-- 04:国際学会等関連・職能団体
	-- 05:養成校
	-- 06:同窓会
	-- 07:その他
	-- 
	sig_type varchar(2),
	-- 01:認定
	-- 02:取消
	-- 03:休止
	-- 
	status_cd varchar(2),
	sig_other_nm varchar(200),
	sig_other_nm_kana varchar(600),
	course_nm varchar(200),
	event_date date,
	-- qualification:能力、経験
	-- certification:認可　←こっちをカラム名にする
	certification_date date,
	-- 01:協会
	-- 02:47士会
	confirmer_cd varchar(2),
	application_file_nm varchar(512),
	application_file bytea,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.sig_other_group_histories
(
	id bigserial NOT NULL,
	sig_other_group_id bigint NOT NULL,
	-- 東コロ様DB定義より。他団体SIGの「状態」カラムとは値が異なるようだ。
	-- 01:初回
	-- 02:取消
	-- 03:休止
	-- 04:再開
	-- 05:復活
	-- 06:変更
	status_cd varchar(2),
	sig_other_nm varchar(200),
	-- qualification:能力、経験
	-- certification:認可　←こっちをカラム名にする
	certification_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.social_contribution_actual2_fields
(
	id bigserial NOT NULL,
	result_id bigint NOT NULL,
	-- 身体障害
	-- 精神障害
	-- 発達障害
	-- 老年期生涯
	field_cd varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.social_contribution_actual_fields
(
	id bigserial NOT NULL,
	result_id bigint NOT NULL,
	-- 身体障害
	-- 精神障害
	-- 発達障害
	-- 老年期生涯
	field_cd varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.social_contribution_actual_results
(
	id bigserial NOT NULL,
	member_no int NOT NULL,
	-- １：WEB
	-- ２：事務局
	register_category_cd char(1),
	activity_content_cd varchar(2),
	start_date date,
	end_date date,
	type varchar(2),
	-- 団体/学会/研修会/会議/等
	group_name varchar(128),
	-- 他団体/他学会の詳細
	group_detail varchar(128),
	training_name varchar(128),
	held_times int,
	venue varchar(4),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.special_ot_applications
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	application_date date,
	-- １：新規
	-- ２：更新
	application_type varchar(1),
	field_cd varchar(2),
	-- YYYYMM
	fiscal_year varchar(6),
	-- 301:審査委員会承認待ち
	-- 302:理事会承認待ち
	-- 
	status_cd varchar(3),
	renewal_count int,
	reject_flag varchar(1),
	secretariat_confirm_date date,
	committee_approval_date date,
	director_approval_date date,
	examination_date date,
	-- 値の内容は不明
	examination_result_cd varchar(1),
	annual_fee int,
	remarks varchar(100),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.special_ot_application_histories
(
	id bigserial NOT NULL,
	member_no bigint NOT NULL,
	seq_no int NOT NULL,
	application_date date,
	-- １：新規
	-- ２：更新
	application_type varchar(1),
	field_cd varchar(2),
	-- YYYYMM
	fiscal_year varchar(6),
	-- 301:審査委員会承認待ち
	-- 302:理事会承認待ち
	-- 
	status_cd varchar(3),
	renewal_count int,
	reject_flag varchar(1),
	secretariat_confirm_date date,
	committee_approval_date date,
	director_approval_date date,
	examination_date date,
	-- 値の内容は不明
	examination_result_cd varchar(1),
	annual_fee int,
	remarks varchar(100),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.support_members
(
	id bigserial NOT NULL,
	-- １：A会員
	-- ２：B会員
	-- ３：C会員
	type_cd varchar(2),
	personal_corp_type_cd varchar(2),
	member_name varchar(255),
	member_kana_name varchar(255),
	department_name varchar(255),
	department_kana_name varchar(255),
	primary_name varchar(255),
	primary_kana_name varchar(255),
	role_name varchar(128),
	primary_phone_number varchar(36),
	email_address varchar(128),
	zipcode varchar(20),
	prefecture_cd char(2),
	address1 varchar(128),
	address2 varchar(128),
	address3 varchar(128),
	billing_depart_name varchar(255),
	billing_depart_kana_name varchar(255),
	billing_section_name varchar(255),
	billing_section_kana_name varchar(255),
	-- 01:北海道 ・・・ 47:沖縄、49:海外、50:不明
	billing_prefecture_cd char(2),
	billing_zipcode varchar(8),
	billing_address1 varchar(128),
	billing_address2 varchar(128),
	billing_address3 varchar(128),
	billing_phone_number varchar(36),
	billing_fax_number varchar(36),
	billing_person_in_charge varchar(128),
	billing_publication_return_date date,
	billing_remarks text,
	unit_of_contribution int,
	-- 入会年月日
	entry_date date,
	recess_date date,
	-- 退会年月日
	withdrawal_date date,
	shipping_depart_name varchar(255),
	shipping_depart_kana_name varchar(255),
	shipping_section_name varchar(255),
	shipping_section_kana_name varchar(255),
	-- 01:北海道 ・・・ 47:沖縄、49:海外、50:不明
	shipping_prefecture_cd char(2),
	shipping_zipcode varchar(8),
	shipping_address1 varchar(128),
	shipping_address2 varchar(128),
	shipping_address3 varchar(128),
	shipping_phone_number varchar(36),
	shipping_fax_number varchar(36),
	shipping_person_in_charge varchar(128),
	shipping_publication_return_date date,
	shipping_remarks text,
	-- １：担当者宛
	-- ２：部署所属宛
	-- 
	billing_send_category_cd varchar(2),
	-- １：担当者宛
	-- ２：部署所属宛
	-- 
	shipping_send_category_cd varchar(2),
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.system_transition_graph_data
(
	id bigserial NOT NULL,
	fiscal_year int,
	qualified_member_count int,
	member_count int,
	training_school_count int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.temporary_deposit_details
(
	id bigserial NOT NULL,
	-- 画面定義によるとこんな感じ
	-- 1:コンビニ入金
	-- 2:郵便振込入金
	-- 3:ペーパーレス入金
	-- 
	deposit_data_type varchar(1),
	-- 10桁の連番 + 6桁の会員番号
	billing_no varchar(16),
	billing_fiscal_year int,
	member_no int,
	-- 会費種別のこと。
	-- 0:正会員
	-- 1:賛助会員
	-- 2:入会
	-- 3:研修会
	-- 4:生涯教育
	-- 
	billing_type_cd varchar(1),
	seq_no int,
	-- 請求データが分割されている場合の番号
	-- 
	-- 分割なし：0
	-- 分割あり：0～
	deposit_division_no int,
	deposit_date date,
	-- [東コロ様テーブル定義書の記述]
	-- 1:郵便振替
	-- 2:コンビニ
	-- 3:現金
	-- ↑こっちを採用。確報、速報は確報フラグを持つことで対応。
	-- 
	-- [コード設計書の記述]
	-- 0001:コンビニ入金速報
	-- 0002:コンビニ入金確報
	-- 0003:ペーパーレス確報
	-- 0004:郵便振込速報
	-- 0005:郵便振込確報
	-- 0006:郵便振込
	-- 0007:銀行振込
	-- 0008:現金
	-- 
	deposit_type varchar(1),
	-- 東コロ様のテーブル定義に「入金区分」があり、その内容が"0:確定データ"であるため、このフィールドが確報、速報、取消を表すと考える。
	-- 
	-- 1:速報
	-- 2:確報
	-- 3:取消
	-- とする。
	confirmation_type varchar(1),
	deposit_amount int,
	remarks varchar(32),
	-- 請求の費目と合わせた
	-- 0001:正会員年会費
	-- 0002:WFOT個人年会費
	-- 0003:前年度WFOT個人年会費
	-- 0004:前受金
	-- 
	expense_cd varchar(4),
	import_error_cd varchar(4),
	import_error_description varchar(255),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.total_receipt_print_histories
(
	id bigserial NOT NULL,
	billing_date date,
	-- 画面定義でサイズ指定がない。
	destination text,
	-- 01:様
	-- 02:御中
	honorifics_cd varchar(2),
	-- 画面設計でサイズが指定されていないのでtextにしておく
	proviso text,
	receipt_amount int,
	president_nm varchar(50),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.trainings
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL UNIQUE,
	-- 旧システムでの研修会番号(6桁)を保持するテーブル。
	-- 移行データの格納先となる。
	old_training_no varchar(6),
	hold_fiscal_year int,
	-- 画面のプルダウンに表示される値は以下の6種類。コード定義がないのでコード値不明
	-- 1
	-- 2
	-- 3
	-- 1(ﾌﾘｰ)
	-- 2(ﾌﾘｰ)
	-- 3(ﾌﾘｰ)
	hold_days int,
	-- 画面定義では"05"という先頭0埋めの数値で表示しているが、
	-- 1.5次では数値で保持している。
	-- コード値ではなく回数なので意味的にもintで良い
	hold_count int,
	training_type_cd varchar(6),
	course_category_cd varchar(8),
	-- 1.5次のDBではvarchar(200)になっているのでそちらに合わせる
	course_nm varchar(200),
	-- 01:受付前
	-- 02:受付中
	-- 03:受付終了
	-- 
	reception_status_cd varchar(2),
	-- 10:新規
	-- 11:コピー
	-- 12:仮登録
	-- 20:保留中
	-- 30:運営更新
	-- 40:本登録
	-- →ここに「入金待ち」を入れる？
	-- 
	update_status_cd varchar(2),
	-- コード設計[30220]
	-- 00:正会員のみ
	-- 10:一般公開（学生除く）
	-- 11:一般公開（学生含む）
	-- 
	target_cd varchar(2),
	capacity_count int,
	entry_fee int,
	student_discount_entry_fee int,
	attending_point int,
	elearning_flag boolean DEFAULT 'FALSE',
	attendance_recept_cd varchar(1),
	leave_confirm_cd varchar(1),
	acceptance_cd varchar(1),
	overview text,
	-- 研修会開催日１日目
	-- 
	-- 以下の特徴があり、子テーブルに分けるメリットが少ないので
	-- 研修会テーブルに持つ
	-- 
	-- ・業務的に最大3日までしか存在しない
	-- ・1日目のみ開始時間を持っている
	event_date1 date,
	event_date1_start_time time,
	-- 開催日２日目
	event_date2 date,
	-- 開催日３日目
	event_date3 date,
	event_date_free text,
	-- e-learningの配信開始日
	delivery_start_date date,
	-- e-learningの配信終了日
	delivery_end_date date,
	reception_start_date date,
	reception_deadline_date date,
	training_end_date date,
	zipcode varchar(8),
	-- 開催場所フリー入力の場合の選択値はここに保持する
	prefecture_cd varchar(2),
	address1 varchar(32),
	address2 varchar(32),
	address3 varchar(32),
	venue varchar(32),
	-- 【開催場所】の「フリー入力」に対応
	venue_free_flag boolean DEFAULT 'FALSE',
	-- 既存の研修会テーブル(t_kenshukai)では開催場所1,2について256文字で入力できる。その２つのフィールドの値はここに設定する
	venue_free text,
	-- 研修会一覧画面の検索条件になっている。
	-- 
	-- コード設計[10011]
	-- 01:北海道
	-- ......
	-- 47:沖縄県
	-- 48:海外
	venue_pref_cd varchar(2),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	operator_doc_no_to_superiors varchar(20),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	operator_doc_no_to_person varchar(20),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	-- コード設計の「30390」が該当
	-- 10:PDF(全体)
	-- 20:EXCEL
	operator_doc_print_format_cd varchar(2),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	instructor_doc_no_to_superiors varchar(20),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	instructor_doc_no_to_person varchar(20),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	-- 
	-- コード設計の「30400」が該当
	-- 11:PDF（全体）
	-- 12:PDF（個別）
	-- 20:EXCEL
	-- 
	instructor_doc_print_format_cd varchar(2),
	-- 画面の【研修会プログラム】の「フリー入力」と対応
	training_program_free_flag boolean DEFAULT 'FALSE',
	training_program_free text,
	remarks text,
	attend_permission_doc varchar(20),
	attend_permission_doc_delivery_date date,
	attend_permission_deposit_period_date date,
	-- 論理名からして不明すぎるカラム。
	other6 text,
	message text,
	-- 一覧画面で「研修会終了」として使われている
	end_flag boolean DEFAULT 'FALSE',
	end_date date,
	-- 一覧画面の「中止」チェックボックスとして使われている
	cancel_flag boolean DEFAULT 'FALSE',
	cancel_date date,
	-- 画面の「協会HP掲載」と対応
	hp_posted_flag boolean DEFAULT 'FALSE',
	hp_posted_user_id varchar(32),
	hp_posted_date date,
	training_update_datetime timestamp,
	training_update_user varchar(50),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_advance_payments
(
	id bigserial NOT NULL,
	training_result_id bigint NOT NULL,
	seq_no int,
	payment_date date,
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	-- 
	payment_use_cd varchar(2),
	-- サイズは東コロ様DB定義に合わせた
	remarks varchar(100),
	amount int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_applications
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16),
	-- 会員番号が設定される。一般申込者はシステムで扱われない
	member_no int,
	-- 画面定義書「申込者一覧タブ」の整理番号に対応する項目。申込順に採番するのだろう。
	-- 仮申込一覧には表示されていないので、仮登録データには持たないと思われる
	queue_no int,
	-- コード設計[30120]
	-- 01:会員
	-- 02:他職種
	-- 03:学生
	-- 
	applicant_category_cd varchar(2),
	-- コード設計[30180]
	-- 00:申込前
	-- 10:仮申込
	-- 20:本申込
	-- 30:受講許可
	-- 40:受講不許可
	-- 50:受講決定
	application_status_cd varchar(2),
	-- コード設計[30190]
	-- 00:削除
	-- 10:取消
	-- 20:辞退
	-- 30:要再提出
	-- 40:再提出済
	-- 50:督促
	-- 51:打診中
	-- 52:打診回答済
	-- 
	procedure_status_cd varchar(2),
	fullname varchar(50),
	fullname_kana varchar(50),
	-- コード設計[30090]
	-- 01:WEB
	-- 02:郵送
	-- 
	contact_type varchar(2),
	-- 画面の「疾患・障害名」。
	-- 項目名「対象疾患」は東コロ様DB定義を使った。
	-- そちらの方がより正確に表現されていると思ったため。
	target_disease varchar(30),
	-- コード設計[30140]
	-- 01:可能
	-- 02:場合による
	-- 03:できない
	-- 
	case_provide_cd varchar(1),
	experience_year int,
	resubmission_date date,
	resubmission_count int,
	notification_date date,
	deposit_period_date date,
	declining_date date,
	remarks text,
	-- OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する
	-- 
	-- コード設計[30080]
	-- ※同じコード値で別の内容が指定されている。
	-- 　本当にこのコードでいいのか要確認
	-- 
	-- ・画面定義書では以下の３種類と指定されている
	-- 大学教授
	-- 学生
	-- 厚生労働省職員
	-- 
	job_category_cd varchar(2),
	-- OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する
	work_place_nm varchar(32),
	-- OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する
	work_place_zipcode varchar(8),
	-- OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する
	work_place_prefecture_cd varchar(2),
	-- OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する
	work_place_address1 varchar(32),
	-- OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する
	work_place_address2 varchar(32),
	-- OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する
	work_place_address3 varchar(32),
	-- OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する
	phone_number varchar(36),
	-- コード設計[30210]
	-- 0:なし
	-- 1:あり
	-- 
	reminder_flag varchar(1),
	-- コード設計[30200]
	-- 0:回答なし
	-- 1:受講する
	-- 2:受講できない
	-- 
	advanced_win_consult_flag varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_application_histories
(
	id bigserial NOT NULL,
	training_application_id bigint NOT NULL,
	-- 会員番号が設定される。
	-- 
	member_no int,
	application_count int,
	-- コード設計[30180]
	-- 00:申込前
	-- 10:仮申込
	-- 20:本申込
	-- 30:受講許可
	-- 40:受講不許可
	-- 50:受講決定
	application_status_cd varchar(2),
	-- コード設計[30190]
	-- 00:削除
	-- 10:取消
	-- 20:辞退
	-- 30:要再提出
	-- 40:再提出済
	-- 50:督促
	-- 51:打診中
	-- 52:打診回答済
	-- 
	procedure_status_cd varchar(2),
	-- コード設計[30090]
	-- 01:WEB
	-- 02:郵送
	-- 
	contact_type varchar(2),
	-- 画面の「疾患・障害名」。
	-- 項目名「対象疾患」は東コロ様DB定義を使った。
	-- そちらの方がより正確に表現されていると思ったため。
	target_disease varchar(30),
	-- コード設計[30140]
	-- 01:可能
	-- 02:場合による
	-- 03:できない
	-- 
	case_provide_cd varchar(1),
	experience_year int,
	-- コード設計[30200]
	-- 0:回答なし
	-- 1:受講する
	-- 2:受講できない
	-- 
	advanced_win_consult_flag varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_attendance_priorities
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL,
	seq_no int,
	-- コード設計[30450]
	priority_item_cd varchar(3),
	-- コード設計[30460]
	priority_point_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_attendance_requirements
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL,
	seq_no int,
	-- コード値不明。サイズも分からないので6桁と仮置きする
	-- 
	-- 画面定義書に以下のような値が例として挙げられている
	-- 日本作業療法士協会正会員
	-- 都道府県作業療法士会正会員
	-- 生涯教育制度基礎研修修了者
	-- 認定作業療法士取得研修　選択研修　受講資格保有者
	-- 
	requirement_cd varchar(6),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_attendance_results
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16),
	-- 会員番号が設定される。
	-- 
	member_no int,
	fullname varchar(50),
	-- 子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。
	-- 
	-- 値はコード設計[30260]
	-- 00:欠席
	-- 10:出席
	-- 20:免除
	attendance_status1_cd varchar(2),
	-- 子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。
	-- 
	-- 値はコード設計[30270]
	-- 00:退席なし
	-- 10:途中退席
	-- 20:退席
	leave_status1_cd varchar(2),
	-- 子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。
	-- 
	-- 値はコード設計[30260]
	-- 00:欠席
	-- 10:出席
	-- 20:免除
	attendance_status2_cd varchar(2),
	-- 子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。
	-- 
	-- 値はコード設計[30270]
	-- 00:退席なし
	-- 10:途中退席
	-- 20:退席
	leave_status2_cd varchar(2),
	-- 子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。
	-- 
	-- 値はコード設計[30260]
	-- 00:欠席
	-- 10:出席
	-- 20:免除
	attendance_status3_cd varchar(2),
	-- 子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。
	-- 
	-- 値はコード設計[30270]
	-- 00:退席なし
	-- 10:途中退席
	-- 20:退席
	leave_status3_cd varchar(2),
	-- コード設計[30280]
	-- 00:未修了
	-- 10:修了
	completion_status_cd varchar(2),
	completion_date date,
	-- コード設計[30290]
	-- 00:不合格
	-- 10:合格
	-- ※コード設計にはないが、画面で「試験免除者数」を表示する必要があるので、「20:試験免除」といった値が必要そう
	test_result_cd varchar(2),
	test_pass_date date,
	-- コード設計[30290]
	-- 00:不合格
	-- 10:合格
	-- ※コード設計にはないが、画面で「試験免除者数」を表示する必要があるので、「20:試験免除」といった値が必要そう
	retest_result_cd varchar(2),
	retest_date date,
	retest_pass_date date,
	-- 申込出席者一覧画面の、検索結果の明細行の「当日収納」をクリックすることで切り替えるフラグ
	-- 
	-- true:当日収納
	-- false:-
	pay_on_the_day_flag boolean DEFAULT 'FALSE',
	-- 研修会の参加費を格納する。
	-- （請求と紐づけるという手もあるが、取り回しが面倒そうなのでここに持ってしまう）
	-- 研修会の参加費は、通常の金額と学割金額があるので、どちらを適用するか判定してこのフィールドに格納する
	entry_fee int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_categories
(
	id bigserial NOT NULL,
	-- 管理対象を示すコード値
	-- 
	-- コード設計[30110]
	-- 01:受講要件
	-- 02:免除要件
	-- 03:優先順位
	-- 04:講師
	-- 05:運営担当者
	-- 06:講師条件
	-- 
	type_cd varchar(2),
	-- コード設計[30010]
	-- ※この研修会種別は新システムでのコード体系で、
	-- 旧システムの(「研修会カテゴリ(2桁)」+「専門分野(2桁)」+「研修会グループ(2桁)])=新システムの「研修会種別(6桁)」となっているようだ。なので旧システムでの３カラムを１カラムで扱う
	-- 
	-- 研修会に紐づける受講要件や免除要件はこのコード値が格納される
	training_type_cd varchar(6),
	-- コード設計[30010]の内容から抽出(現行のコード値？)
	-- 01:基礎研修
	-- 02:認定OT
	-- 03:専門OT
	-- 04:重点
	-- 05:臨床指導者
	-- 21:特別
	-- 99:その他
	category_cd varchar(2),
	-- コード設計[30010]から専門分野の値を抽出
	-- 
	-- 00:なし
	-- 01:福祉用具
	-- 02:認知症
	-- 03:手外科
	-- 04:特別支援
	-- 05:高次脳
	-- 06:精神急性
	-- 07:摂食嚥下
	-- 08:訪問
	-- 09:がん
	-- 10:就労支援
	-- 
	field_cd varchar(2),
	-- 研修会カテゴリと専門分野の組み合わせによって値の意味が変わる。
	-- コード設計[30010]や、参照。
	group_cd varchar(2),
	class_cd varchar(2),
	-- 画面にはないし必要とは思えないが、東コロ様DB定義に存在しているので追加
	-- 
	-- コード設計[30030]
	-- 00:OT協会
	-- 01:北海道士会
	-- ...
	-- 47:沖縄県士会
	-- 99:共催
	organizer_cd varchar(2),
	-- 東コロ様DB定義にあるので残してあるが、この分類に紐づく要件、優先順位や講師などはサブテーブルに持つので実質不要
	content varchar(100),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_category_members
(
	id bigserial NOT NULL,
	training_category_id bigint NOT NULL,
	seq_no int,
	member_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_category_priorities
(
	id bigserial NOT NULL,
	training_category_id bigint NOT NULL,
	seq_no int,
	-- コード設計[30450]
	priority_item_cd varchar(3),
	-- コード設計[30460]
	-- 
	priority_point_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_category_requirements
(
	id bigserial NOT NULL,
	training_category_id bigint NOT NULL,
	seq_no int,
	-- コード設計にはないが、
	-- 01:受講要件
	-- 02:免除要件
	-- を想定している
	requirement_type_cd varchar(2),
	-- コード値不明。サイズも分からないので6桁と仮置きする
	-- 
	-- 画面定義書に以下のような値が例として挙げられている
	-- 日本作業療法士協会正会員
	-- 都道府県作業療法士会正会員
	-- 生涯教育制度基礎研修修了者
	-- 認定作業療法士取得研修　選択研修　受講資格保有者
	-- 
	requirement_cd varchar(6),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_exemption_requirements
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL,
	seq_no int,
	-- コード値不明。サイズも分からないので6桁と仮置きする
	-- 
	-- 画面定義書に以下のような値が例として挙げられている
	-- 日本作業療法士協会正会員
	-- 都道府県作業療法士会正会員
	-- 生涯教育制度基礎研修修了者
	-- 認定作業療法士取得研修　選択研修　受講資格保有者
	-- 
	requirement_cd varchar(6),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_files
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL,
	send_file_nm varchar(50),
	send_file bytea,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_instructors
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL,
	-- 講師は会員以外ありえないのでmember_no
	member_no int,
	-- スナップショットとしての名前が不要（あるいは無い方がいい）なら、このカラムを削除する
	instructor_nm varchar(50),
	display_seq_no int,
	-- コード設計の「30420」と対応
	-- 0:講師
	-- 1:講師助手
	-- 
	instructor_type_cd varchar(1),
	-- コード設計[30050]に該当
	-- 
	-- 01:会員講師
	-- 02:外部講師
	-- 
	instructor_category_cd varchar(2),
	-- コード設計の「30410」が該当
	-- 
	-- 0:なし
	-- 1:あり
	-- 
	doc_to_superiors_cd varchar(1),
	-- 画面の公文書情報のチェックボックス。
	-- チェックがtrueの場合、公文書印刷対象となる
	doc_to_superiors_print_flag boolean DEFAULT 'FALSE',
	doc_to_superiors_facility_nm varchar(128),
	doc_to_superiors_nm varchar(50),
	doc_to_superiors_send_date date,
	-- コード設計の「30410」が該当
	-- 
	-- 0:なし
	-- 1:あり
	-- 
	doc_to_person_cd varchar(1),
	-- 画面の公文書情報のチェックボックス。
	-- チェックがtrueの場合、公文書印刷対象となる
	doc_to_person_print_flag boolean DEFAULT 'FALSE',
	doc_to_person_facility_nm varchar(128),
	doc_to_person_nm varchar(50),
	doc_to_person_send_date date,
	-- 公文書に謝金について記載するか否か
	-- コード設計[30430]に該当
	-- 
	-- 0:なし
	-- 1:あり
	reward_description_category_cd varchar(1),
	reward_description text,
	-- コード設計[30100]に対応
	-- 01:A
	-- 02:B
	-- 03:C
	-- 04:D
	-- 
	reward_category_cd varchar(2),
	instructor_point int,
	-- 画面にない項目だが、東コロ様DB定義にあったので追加
	-- 「実績なし/実績あり」という値が備考に書かれている
	instructor_achievement_flag varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_instructor_rewards
(
	id bigserial NOT NULL,
	training_result_id bigint NOT NULL,
	-- 会員番号が設定される
	-- 
	member_no varchar(7),
	-- 研修会講師実績から参照できるはず。不要かもしれない。
	-- コード設計[30100]に対応
	-- 01:A
	-- 02:B
	-- 03:C
	-- 04:D
	reward_category_cd varchar(2),
	-- 研修会講師実績から取得できるはず。不要かもしれない。
	-- コード設計の「30420」と対応
	-- 0:講師
	-- 1:講師助手
	-- 
	instructor_type_cd varchar(1),
	-- 時間が整数なのか小数なのか。30分とかの講義はどうする。
	lecture_hour decimal(5,2),
	lecture_fee int,
	-- 講義(時間)と同様に30分単位の扱いをどうするか
	exercise_hour decimal(5,2),
	exercise_fee int,
	-- 講義(時間)と同様に30分単位の扱いをどうするか
	assistant_hour decimal(5,2),
	assistant_fee int,
	total_instructor_reward int,
	-- 日新という旅行会社を使った費用と考え、ローマ字で命名する
	-- サイト：https://www.nissin-trvl.co.jp/
	travel_expenses_nissin int,
	travel_expenses_other int,
	total_travel_expenses int,
	total_all int,
	withholding_amount int,
	payment_amount int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_no_conversions
(
	id bigserial NOT NULL,
	fiscal_year int,
	old_no varchar(8),
	old_branch_no int,
	new_no varchar(8),
	new_branch_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_numbers
(
	id bigserial NOT NULL,
	fiscal_year int,
	-- 8桁の内訳(それぞれ２桁ずつ)
	-- 1-2桁目:カテゴリ
	-- 3-4桁目:専門分野
	-- 5-6桁目:グループ
	-- 7-8桁目:クラス
	training_no varchar(8),
	content varchar(100),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_operators
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL,
	-- 運営担当者の会員番号を格納する
	member_no int,
	member_nm varchar(50),
	display_seq_no int,
	-- 画面の「担当区分」に該当
	-- 0:当日担当者権限
	-- 1:運営担当者権限
	category_cd varchar(1),
	-- 画面の「問合せ」と対応
	-- コード設計の「30380」が該当すると思われる。
	-- 
	-- 0:なし
	-- 1:問合せ担当者権限
	-- 
	contact_category_cd varchar(1),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	-- コード設計の「30410」が該当
	-- 
	-- 0:なし
	-- 1:あり
	-- 
	doc_to_superiors_cd varchar(1),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	-- 画面の公文書情報のチェックボックス。
	-- チェックがtrueの場合、公文書印刷対象となる
	doc_to_superiors_print_flag boolean DEFAULT 'FALSE',
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	doc_to_superiors_facility_nm varchar(128),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	doc_to_superiors_nm varchar(50),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	doc_to_superiors_send_date date,
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	-- コード設計の「30410」が該当
	-- 
	-- 0:なし
	-- 1:あり
	-- 
	doc_to_person_cd varchar(1),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	-- 画面の公文書情報のチェックボックス。
	-- チェックがtrueの場合、公文書印刷対象となる
	doc_to_person_print_flag boolean DEFAULT 'FALSE',
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	doc_to_person_facility_nm varchar(128),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	doc_to_person_nm varchar(50),
	-- 公文書に関する情報は別システムで管理する。
	-- 仕様が確定次第、カラムは削除する
	-- 
	doc_to_person_send_date date,
	-- 東コロ様DB定義の「T_研修会士会当日担当者」テーブルは基本的にわざわざテーブルを分ける必要がないと判断したが、
	-- このカラムはもしかしたら必要かもしれないので一応入れておく。
	secretariat_user_id varchar(32),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_operator_rewards
(
	id bigserial NOT NULL,
	training_result_id bigint NOT NULL,
	member_no int,
	-- 日新という旅行会社を使った費用と考え、ローマ字で命名する
	-- サイト：https://www.nissin-trvl.co.jp/
	travel_expenses_nissin int,
	travel_expenses_other int,
	total_travel_expenses int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_organizers
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL,
	seq_no int,
	-- コード設計[30030]
	-- 00:OT協会
	-- 01:北海道士会
	-- ...
	-- 47:沖縄県士会
	-- 99:共催
	-- 
	-- ※「99:共催」とあるが、共催とは主催者が複数いる状態のことで、主催者コードの全てを子テーブルに格納しておく必要がある。
	organizer_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_programs
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16) NOT NULL,
	seq_no int,
	-- 開催日1～3のどの日のプログラムかを保持
	event_date date,
	-- コード設計[30350]
	-- 00:0時
	-- 01:1時
	-- 02:2時
	-- 03:3時
	-- 04:4時
	-- 05:5時
	-- ......
	-- 23:23時
	start_hour_cd varchar(2),
	-- コード設計[30360]
	-- 00:0分
	-- 05:5分
	-- 10:10分
	-- 15:15分
	-- 20:20分
	-- 25:25分
	-- 30:30分
	-- 35:35分
	-- 40:40分
	-- 45:45分
	-- 50:50分
	-- 55:55分
	-- 
	start_minute_cd varchar(2),
	-- コード設計[30350]
	-- 00:0時
	-- 01:1時
	-- 02:2時
	-- 03:3時
	-- 04:4時
	-- 05:5時
	-- ......
	-- 23:23時
	end_hour_cd varchar(2),
	-- コード設計[30360]
	-- 00:0分
	-- 05:5分
	-- 10:10分
	-- 15:15分
	-- 20:20分
	-- 25:25分
	-- 30:30分
	-- 35:35分
	-- 40:40分
	-- 45:45分
	-- 50:50分
	-- 55:55分
	-- 
	end_minute_cd varchar(2),
	-- 東コロ様DB定義のサイズに合わせた
	content varchar(100),
	-- コード設計[30440]
	-- 10:講義
	-- 20:演習
	-- 
	lecture_style_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_program_instructors
(
	id bigserial NOT NULL,
	training_program_id bigint NOT NULL,
	-- 講師は会員以外ありえないのでmember_no
	member_no int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_reports
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16),
	-- コード設計[30220]
	-- 00:正会員のみ
	-- 10:一般公開（学生除く）
	-- 11:一般公開（学生含む）
	target_cd varchar(2),
	budget int,
	balance int,
	expenditure int,
	-- コード設計[30530]
	-- 1:満足
	-- 2:やや満足
	-- 3:普通
	-- 4:やや不満
	-- 5:不満
	-- 
	participants_evaluation_cd varchar(1),
	operator_member_no int,
	operator_fullname varchar(50),
	-- 画面の申込者数が編集可能なので報告書のフィールドに保持する
	applicant_count int,
	-- コード設計[30540]
	-- 1:優良（100%～80%）
	-- 2:可（79%～50%）
	-- 3:要改善・再検討（49%～）
	-- 
	effect_judge1_cd varchar(2),
	effect_judge1_content text,
	-- コード設計[30540]
	-- 1:優良（100%～80%）
	-- 2:可（79%～50%）
	-- 3:要改善・再検討（49%～）
	-- 
	effect_judge2_cd varchar(2),
	effect_judge2_content text,
	-- コード設計[30540]
	-- 1:優良（100%～80%）
	-- 2:可（79%～50%）
	-- 3:要改善・再検討（49%～）
	-- 
	cooperation_evaluation_cd varchar(1),
	cooperation_evaluation_content text,
	workplace_phone_use_count int,
	workplace_phone_use_hour int,
	-- 画面や東コロ様DB定義のカラム名をそのまま採用。送信件数のことか。
	workplace_fax_send_count int,
	workplace_fax_receive_count int,
	personal_phone_use_count int,
	personal_phone_use_hour int,
	summary text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_results
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	salary_account_cd varchar(2),
	salary_budget int,
	salary_settlement int,
	-- 東コロ様DB定義のサイズに合わせた
	salary_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	welfare_account_cd varchar(2),
	welfare_budget int,
	welfare_settlement int,
	welfare_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	meeting_account_cd varchar(2),
	meeting_budget int,
	meeting_settlement int,
	meeting_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	travel_expenses_other_account_cd varchar(2),
	travel_expenses_other_budget int,
	travel_expenses_other_settlement int,
	travel_expenses_other_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	travel_expenses_nissin_account_cd varchar(2),
	travel_expenses_nissin_budget int,
	travel_expenses_nissin_settlement int,
	travel_expenses_nissin_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	consumables_account_cd varchar(2),
	consumables_budget int,
	consumables_settlement int,
	consumables_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	print_account_cd varchar(2),
	print_budget int,
	print_settlement int,
	print_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	rent_account_cd varchar(2),
	rent_budget int,
	rent_settlement int,
	rent_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	utility_account_cd varchar(2),
	utility_budget int,
	utility_settlement int,
	utility_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	external_account_cd varchar(2),
	external_budget int,
	external_settlement int,
	external_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	commission_account_cd varchar(2),
	commission_budget int,
	commission_settlement int,
	commission_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	reward_account_cd varchar(2),
	reward_budget int,
	reward_settlement int,
	reward_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	withholding_account_cd varchar(2),
	withholding_budget int,
	withholding_settlement int,
	withholding_overview varchar(100),
	-- コード設計[30550]
	-- 01:給与手当て
	-- 02:福利厚生費
	-- 03:会議費
	-- 04:旅費交通費（日新以外）
	-- 05:旅費交通費（日新分）
	-- 06:消耗品費
	-- 07:印刷製本費
	-- 08:賃借料
	-- 09:光熱費
	-- 10:渉外費
	-- 11:通信運搬費
	-- 12:委託費
	-- 13:諸謝金
	total_account_cd varchar(2),
	total_budget int,
	total_settlement int,
	total_overview varchar(100),
	total_expenditure int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_school_name_histories
(
	id bigserial NOT NULL,
	facility_id bigint NOT NULL,
	-- 初期登録時の施設番号
	facility_base_no varchar(20),
	seq_no int,
	fiscal_year int,
	name_update_user_id varchar(32),
	name_update_date timestamp,
	name_update_no int,
	name_create_date date,
	name_delete_date date,
	pref_code varchar(2),
	facility_category_cd varchar(1),
	-- 都道府県コード 、施設区分ごとの連番
	facility_seq_no varchar(20),
	-- 都道府県コード+施設区分+施設連番
	facility_no varchar(20),
	training_school_category_cd varchar(2),
	origin_school_no int,
	training_school_nm varchar(128),
	training_school_nm_kana varchar(128),
	memo varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_school_open_histories
(
	id bigserial NOT NULL,
	facility_id bigint NOT NULL,
	-- 初期登録時の施設番号
	facility_base_no varchar(20),
	seq_no int,
	open_update_user_id varchar(32),
	open_update_date timestamp,
	open_fiscal_year int,
	last_recuriting_fiscal_year int,
	last_fiscal_year int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_school_relations
(
	id bigserial NOT NULL,
	facility_id bigint NOT NULL,
	-- 初期登録時の施設番号
	facility_base_no varchar(20),
	-- 関連施設を登録するたびにカウントアップするフィールド
	-- 1番目に登録した関連施設なら1が入り、
	-- 2番目に追加で登録した関連施設なら2が入るのでは。
	seq_no int,
	relation_update_user_id varchar(32),
	relation_update_date timestamp,
	update_seq_no int,
	relation_create_date date,
	relation_delete_date date,
	-- 都道府県コード+施設区分+施設連番
	relation_no varchar(20),
	origin_school_no int,
	-- 既存システムでは1桁だが、既に1...7の値が使われているので、桁を増やしておく
	relation_reason_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_send_mail_detail_histories
(
	id bigserial NOT NULL,
	training_send_mail_history_id bigint NOT NULL,
	-- ※一般申込者、外部講師などはシステムで扱わないという話なので、ひょっとしたらこのフィールドは不要になるかも
	-- コード設計[30510]
	-- 01:会員
	-- 02:一般申込者
	-- 03:外部講師
	-- 
	destination_type_cd varchar(2),
	-- コード設計[30500]
	-- 0:なし
	-- 1:あり
	mail_address_presense_cd varchar(1),
	-- 会員番号が設定される。
	-- 
	member_no int,
	fullname varchar(50),
	email_address varchar(128),
	-- コード値不明
	-- 他システムから連携されたステータスを格納する予定
	send_status varchar(1),
	error_cause text,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_send_mail_histories
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16),
	-- コード設計[30480]
	-- 01:書類不備通知
	-- 02:受講許可
	-- 03:受講不許可
	-- 04:請求書送付
	-- 05:督促
	-- 06:未入金取消
	-- 06:受講許可
	-- 07:受講不許可
	-- 07:未入金取消
	-- 07:督促
	-- 07:受講決定
	-- 10:テンプレートなし
	-- 
	template_cd varchar(2),
	subject varchar(128),
	content text,
	send_status smallint,
	send_reserve_date timestamp,
	send_date timestamp,
	result_date timestamp,
	mail_process_id bigint,
	-- コード設計[30490]
	-- 01:運営依頼
	-- 02:講師依頼
	-- 03:講師助手依頼
	-- 04:本申込確認
	-- 05:仮申込取消
	-- 06:仮申込削除
	-- 07:書類不備
	-- 08:本申込完了
	-- 09:本申込取消
	-- 10:受講許可
	-- 11:受講不許可
	-- 12:未入金取消
	-- 13:督促
	-- 14:受講決定
	-- 15:辞退受付
	-- 15:辞退完了
	-- 16:返金申請書
	-- 16:返金取下
	-- 17:返金完了
	-- 18:振替取下
	-- 19:振替完了
	-- 20:繰上打診
	-- 21:繰上承認
	-- 22:繰上否認
	-- 23:開催日案内
	-- 24:合格通知
	-- 25:不合格通知
	-- 26:修了通知
	-- 27:欠席通知
	-- 
	training_mail_type_cd varchar(2),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.training_temp_applications
(
	id bigserial NOT NULL,
	-- 1.5次では6桁で定義されている。
	-- 東コロ様のDB定義のサイズを採用した。
	-- 画面定義の「講座番号」がこれに該当すると思われる
	-- →
	-- 2次では16桁になる。ユニークになるのでKeyとして使える
	training_no varchar(16),
	member_no int,
	mail_address varchar(256),
	-- 画面の「疾患・障害名」。
	-- 項目名「対象疾患」は東コロ様DB定義を使った。
	-- そちらの方がより正確に表現されていると思ったため。
	target_disease varchar(30),
	-- コード設計[30140]
	-- 01:可能
	-- 02:場合による
	-- 03:できない
	-- 
	case_provide_cd varchar(1),
	experience_year int,
	-- 東コロ様DB定義に「登録日時の24時間後」と定義されている
	application_deadline_datetime timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.transfer_paper_output_histories
(
	id bigserial NOT NULL,
	billing_id bigint NOT NULL,
	-- 請求連番10桁 + 会員番号6桁
	seq_no int,
	payment_period_date date,
	total_amount int,
	reissue_count int,
	-- 東コロ様のDB定義にロジックが書かれているので要注意。
	-- ”発送区分が自宅の場合は空白、施設の場合は「作業療法
	-- 士」を設定”
	qualification varchar(20),
	-- 6桁未満の場合先頭を0埋めして6桁にする
	member_no_text varchar(6),
	-- ※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく
	last_name varchar(20),
	-- ※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく
	first_name varchar(20),
	-- 東コロ様DB定義のサイズではなく、会員の郵便番号に合わせる(ハイフン込み)
	zipcode varchar(8),
	address1 varchar(32),
	address2 varchar(32),
	address3 varchar(32),
	facility_nm1 varchar(32),
	-- 施設養成校テーブルでのサイズが128になっているが、連携先や振込用紙の都合に合わせて32にする
	facility_nm2 varchar(32),
	billing_fiscal_year int,
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option1 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option2 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option3 varchar(128),
	-- 請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。
	option4 varchar(128),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.transfer_paper_print_histories
(
	id bigserial NOT NULL,
	billing_id bigint NOT NULL,
	-- 10桁の連番 + 6桁の会員番号
	billing_no varchar(16),
	total_amount1 int,
	total_amount2 int,
	-- 会員番号＆会員氏名＆住所＆会費年度など
	description text,
	fullname varchar(50),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


-- 事務局のログインユーザーを管理するテーブル。
CREATE TABLE public.users
(
	id bigserial NOT NULL,
	-- ユーザーID
	login_id varchar(32) NOT NULL,
	password varchar(255),
	name varchar(128),
	email_address varchar(128),
	paot_user boolean DEFAULT 'FALSE' NOT NULL,
	section_cd varchar(10),
	section_name varchar(128),
	position_cd varchar(10),
	position_name varchar(128),
	training_operator boolean DEFAULT 'FALSE',
	retirement_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.user_login_histories
(
	id bigserial NOT NULL,
	user_id bigint NOT NULL,
	login_datetime timestamp,
	logout_datetime timestamp,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


-- 権限コード値に対応するパス、操作のキー値をブラックリスト形式で保持する。
CREATE TABLE public.user_revoked_authorities
(
	id bigserial NOT NULL,
	code_id varchar(32) DEFAULT '10101' NOT NULL,
	code varchar(32) NOT NULL,
	path_or_key varchar(255) NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.user_roles
(
	id bigserial NOT NULL,
	user_id bigint NOT NULL,
	role_code_id varchar(32) DEFAULT '10101' NOT NULL,
	role_code varchar(32) NOT NULL,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (user_id, role_code_id, role_code)
) WITHOUT OIDS;


CREATE TABLE public.wfot_applications
(
	id bigserial NOT NULL,
	training_school_no bigint NOT NULL,
	training_school_nm varchar(128),
	training_school_nm_kana varchar(128),
	department varchar(128),
	open_fiscal_year int,
	close_fiscal_year date,
	-- 新規
	-- 継続
	application_type varchar(1),
	application_date date,
	-- 601:委員会確認中
	-- 602:理事会承認待ち
	-- 603:合格
	-- 604:不合格
	-- 
	application_status_cd varchar(3),
	-- 601:申請中
	-- 602:承認
	-- 603:未認定
	-- 604:要件不足
	-- 605:取下げ
	-- 
	application_result_cd varchar(3),
	committee_approval_date date,
	director_approval_date date,
	reject_date date,
	judge1_member_no int,
	judge2_member_no int,
	judge3_member_no int,
	process_date date,
	-- 画面設計書によると最終番号+1とのこと。
	-- SEQオブジェクトを使うか。
	guide_doc_no int,
	guide_issue_date date,
	guide_deadline_date date,
	guide_judge_date date,
	-- 最終番号+1
	certification_doc_no int,
	certification_date date,
	certification_judge_date date,
	certification_issue_date date,
	-- 最終番号＋1
	uncertification_doc_no int,
	uncertification_judge_date date,
	uncertification_issue_date date,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.wfot_certified_training_schools
(
	id bigserial NOT NULL,
	training_school_no bigint NOT NULL,
	seq_no int,
	training_school_nm varchar(128),
	training_school_nm_kana varchar(128),
	wfot_application_date date,
	wfot_qualification_date date,
	renewal_count int,
	wfot_qualification_period_from date,
	wfot_qualification_period_to date,
	-- 0:（未入力）
	-- 1:合格
	-- 2:不合格
	-- 
	acceptance_cd varchar(1),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.wfot_info
(
	id bigserial NOT NULL,
	facility_id bigint NOT NULL,
	-- 初期登録時の施設番号
	facility_base_no varchar(20) NOT NULL,
	wfot_update_user_id varchar(32) NOT NULL,
	wfot_update_date timestamp NOT NULL,
	seq_no int NOT NULL,
	wfot_create_date date,
	wfot_delete_date date,
	wfot_no varchar(10),
	-- 英文名称
	school_nm varchar(128),
	-- 郵便番号
	zipcode varchar(8),
	-- 住所１
	address1 varchar(128),
	-- 住所２
	address2 varchar(128),
	-- 住所３
	address3 varchar(128),
	-- 県名
	prefecture_nm varchar(128),
	educational_program varchar(128),
	email_address varchar(128),
	-- WEBサイトURL
	website_address varchar(128),
	-- 認定資格種別
	qualification_awarded varchar(128),
	duration_of_course varchar(128),
	-- 設立年度
	year_course_commenced int,
	-- 国認可年度
	year_course_accredited_by_NA int,
	-- 最終審査年
	year_course_last_reviewed_by_NA int,
	-- 最終認可年
	year_course_first_last_approved_by_wfot int,
	-- 次回審査年
	year_course_next_review_monitoring int,
	year_course_discontinued int,
	post_graduate_course_available int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.zipcodes
(
	id bigserial NOT NULL,
	zipcode varchar(8),
	address varchar(100),
	prefecture_cd char(2),
	prefecture_name varchar(3),
	PRIMARY KEY (id)
) WITHOUT OIDS;


CREATE TABLE public.member_entry_requests
(
	id bigserial NOT NULL,
	auth_key varchar(255) NOT NULL UNIQUE,
	email_address varchar(128),
	last_name varchar(20),
	first_name varchar(20),
	last_kana_name varchar(20),
	first_kana_name varchar(20),
	birth_day date,
	registration_number varchar(6),
	-- TRUE：外免
	foreign_license boolean DEFAULT 'FALSE' NOT NULL,
	-- コード設計には存在しない
	-- 現行1.5次のDBにある値は以下のもの
	-- 0:登録
	-- 8:処理済
	-- 9:削除
	status_cd char(1),
	phone_number varchar(36),
	maiden_kana_name1 varchar(128),
	maiden_kana_name2 varchar(128),
	suspended boolean DEFAULT 'FALSE',
	-- 再入会情報など
	remarks text,
	member_no int,
	mail_send_datetime timestamp,
	fiscal_year int,
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	CONSTRAINT member_entry_requests_pkey PRIMARY KEY (id)
) WITHOUT OIDS;



/* Comments */

COMMENT ON COLUMN public.applications.seq_no IS '更新連番フィールドをここに充てる';
COMMENT ON COLUMN public.applications.register_category_cd IS '1:WEB
2:事務局';
COMMENT ON COLUMN public.applications.application_type_cd IS '1:各種届
2:再発行';
COMMENT ON COLUMN public.applications.application_kind_cd IS '01:退会届
02:休会届
03:復会届
04:WFOT個人会員申込書
05:WFOT個人会員退会申請
06:会費免除
07:領収書
08:認定証フォルダー
09:徽章再発行
10:研修受講カード再発行
など';
COMMENT ON COLUMN public.applications.application_category_cd IS '1:印刷
2:送付依頼
3:WEB申込';
COMMENT ON COLUMN public.applications.send_category_cd IS '1:自宅
2:施設
3:海外
';
COMMENT ON COLUMN public.applications.address1 IS '東コロ様DB定義のサイズを指定';
COMMENT ON COLUMN public.applications.address2 IS '東コロ様DB定義のサイズを指定';
COMMENT ON COLUMN public.applications.address3 IS '東コロ様DB定義のサイズを指定';
COMMENT ON COLUMN public.applications.reason_cd IS '画面定義書によると
[退会]
出産・育児
家事・育児
休職中
退職
他職種へ転職
結婚退職
病気療養
家庭の事情
...など。

[休会]
出産
育児
出産・育児
病気療養
退職のため
育児による退職
その他
...など。';
COMMENT ON COLUMN public.applications.withdrawal_other_reason IS '東コロ様DB定義ではvarchar(100)になっているが、
画面設計の方では特にサイズ制限を設けていない。
会員が自由に記述する欄であるため長めにとっておく';
COMMENT ON COLUMN public.applications.work_place_delete_cd IS '東コロ様DB定義によると
1:削除する
2:削除しない
';
COMMENT ON COLUMN public.applications.remarks IS 'DB定義には無いが、画面で以下のような使われ方をしている
・新規送付登録画面等で住所を入力';
COMMENT ON COLUMN public.applications.wfot_english_full_nm IS 'WFOT個人会員申込で入力する。
東コロ様定義のDBになかったので追加';
COMMENT ON COLUMN public.applications.professional_field_cd IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.application_progresses.register_category_cd IS '1:WEB
2:事務局';
COMMENT ON COLUMN public.application_progresses.application_type_cd IS '1:各種届
2:再発行';
COMMENT ON COLUMN public.application_progresses.application_kind_cd IS '01:退会届
02:休会届
03:復会届
04:WFOT個人会員申込書
05:WFOT個人会員退会申請
06:会費免除
07:領収書
08:認定証フォルダー
09:徽章再発行
10:研修受講カード再発行
など';
COMMENT ON COLUMN public.application_progresses.application_category_cd IS '1:印刷
2:送付依頼
3:WEB申込';
COMMENT ON COLUMN public.application_progresses.progress_cd IS '状況＝進捗。
申請種類の選択により内容が変わる。
画面設計書「ap_06_ap10120100_各種受付一覧_v25.xlsx」のシート「６－１－４．その他の補足説明」にここに設定する値の設定ロジックが定義されている。';
COMMENT ON COLUMN public.application_progresses.status_cd IS '状態＝結果
申請の結果がどうなったかを保持するテーブル';
COMMENT ON COLUMN public.application_progresses.send_category_cd IS '1:自宅
2:施設
3:海外
';
COMMENT ON COLUMN public.attendance_card_reissue_output_histories.member_cd IS '帳票定義の方で「チェックデジットなしの１２桁」と定義されている';
COMMENT ON COLUMN public.attendance_card_reissue_output_histories.security_cd IS 'カラム長が不明だが画面定義のサンプルデータが先頭0埋め3桁で書かれていたのでvarchar(3)とする';
COMMENT ON COLUMN public.attendance_card_reissue_output_histories.phone_number IS '会員テーブルの電話番号長に合わせた';
COMMENT ON COLUMN public.author_presenter_actual_fields.field_cd IS '身体障害
精神障害
発達障害
老年期生涯';
COMMENT ON COLUMN public.author_presenter_actual_results.register_category_cd IS '１：WEB
２：事務局';
COMMENT ON COLUMN public.author_presenter_actual_results.publication_ym IS 'yyyymm';
COMMENT ON COLUMN public.author_presenter_actual_results.group_name IS '団体/学会/研修会/会議/等';
COMMENT ON COLUMN public.barcode_output_histories.billing_no IS '10桁の連番 + 6桁の会員番号';
COMMENT ON COLUMN public.barcode_output_histories.reissue_cd IS '0固定';
COMMENT ON COLUMN public.barcode_output_histories.last_name IS '※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく';
COMMENT ON COLUMN public.barcode_output_histories.first_name IS '※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく';
COMMENT ON COLUMN public.barcode_output_histories.option1 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.barcode_output_histories.option2 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.barcode_output_histories.option3 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.barcode_output_histories.option4 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.basic_ot_qualify_applications.applicant_cd IS '01:会員 02:事務局';
COMMENT ON COLUMN public.basic_ot_qualify_applications.member_no IS '申請者の会員番号';
COMMENT ON COLUMN public.basic_ot_qualify_applications.category_cd IS '申請区分（コード設計50140）
01 新規申請 02 更新申請';
COMMENT ON COLUMN public.basic_ot_qualify_applications.status_cd IS '申請状況（コード設計50150）
101:申請中 102:承認 103:否認 104:保留';
COMMENT ON COLUMN public.basic_ot_training_histories.training_cd IS 'YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス';
COMMENT ON COLUMN public.basic_ot_training_histories.professional_field_cd IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.basic_point_training_histories.point_type IS '参加、発表、講師、裁量';
COMMENT ON COLUMN public.billings.billing_type_cd IS '会費種別のこと。
0:正会員
1:賛助会員
2:入会
3:研修会
4:生涯教育
';
COMMENT ON COLUMN public.billings.billing_delete_flag IS '0:通常データ
1:削除データ';
COMMENT ON COLUMN public.billings.temporary_billing_flag IS '0:通常データ
1:仮データ';
COMMENT ON COLUMN public.billings.reissue_cd IS '・バーコードと振込用紙の定義では0固定とある
・業者向け振込用紙データでは3などの値を設定するとある
どちらにしろ１桁のコードがあれば済みそうなので定義する';
COMMENT ON COLUMN public.billings.shipping_category_cd IS 'コード値は不明。おそらく
1:自宅
2:施設
3:海外
';
COMMENT ON COLUMN public.billings.last_name IS '※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく';
COMMENT ON COLUMN public.billings.first_name IS '※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく';
COMMENT ON COLUMN public.billings.zipcode IS '東コロ様DB定義のサイズではなく、会員の郵便番号に合わせる(ハイフン込み)';
COMMENT ON COLUMN public.billings.facility_nm1 IS '施設養成校テーブルでのサイズが128になっているが、連携先や振込用紙の都合に合わせて32にする';
COMMENT ON COLUMN public.billings.facility_nm2 IS '施設養成校テーブルでのサイズが128になっているが、連携先や振込用紙の都合に合わせて32にする';
COMMENT ON COLUMN public.billings.option1 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.billings.option2 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.billings.option3 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.billings.option4 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.billings.last_billing_type_cd IS '請求種別と同じ値が入ると思われる';
COMMENT ON COLUMN public.billings.billing_type_change_date IS '請求種別の変更日時のことか';
COMMENT ON COLUMN public.billings.invoice_no IS '「cs_補足資料_請求番号設計v1.xlsx」に定義されている44桁の番号
今のところ使い道が不明';
COMMENT ON COLUMN public.billings.billing_no IS '「振込用紙連携データ_update1.pptx」で定義されている。
サンプルデータを見ると単純な連番ではないようで、請求明細テーブルのIDをそのまま使えるわけではなさそう';
COMMENT ON COLUMN public.billing_details.detail_type IS 'コードテーブルに定義されている
0001:正会員年会費
0002:WFOT個人年会費
0003:前年度WFOT個人年会費
0004:前受金
';
COMMENT ON COLUMN public.billing_settings.seq_no IS '用途不明。請求年度がkeyになるので常に1なのではないか';
COMMENT ON COLUMN public.card_reissue_histories.reissue_category_cd IS 'コード値が不明。';
COMMENT ON COLUMN public.clinical_training_facility_applications.application_type IS '１：新規
２：更新';
COMMENT ON COLUMN public.clinical_training_leader_qualify_histories.training_type IS '研修、講習会、厚生労働省認定';
COMMENT ON COLUMN public.clinical_training_leader_training_histories.training_cd IS 'YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス';
COMMENT ON COLUMN public.clinical_training_leader_training_histories.professional_field_cd IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.clinical_training_point_applications.application_member_no IS '申請操作を行った者の会員番号';
COMMENT ON COLUMN public.clinical_training_point_application_details.verify_last_nm_kana IS '照合時点での会員姓カナ';
COMMENT ON COLUMN public.clinical_training_point_application_details.verify_first_nm_kana IS '照合時点での会員名カナ';
COMMENT ON COLUMN public.clinical_training_point_application_details.name_verification_rate IS '指導者氏名と会員氏名の一致率％';
COMMENT ON COLUMN public.clinical_training_point_application_details.statuc_cd IS '申請中、付与済、却下？（要確認とのこと）';
COMMENT ON COLUMN public.deposits.billing_no IS '10桁の連番 + 6桁の会員番号';
COMMENT ON COLUMN public.deposits.billing_type_cd IS '会費種別のこと。
0:正会員
1:賛助会員
2:入会
3:研修会
4:生涯教育
';
COMMENT ON COLUMN public.deposits.deposit_division_no IS '請求データが分割されている場合の番号

分割なし：0
分割あり：0～';
COMMENT ON COLUMN public.deposits.deposit_type IS '[東コロ様テーブル定義書の記述]
1:郵便振替
2:コンビニ
3:現金
↑こっちを採用。確報、速報は確報フラグを持つことで対応。

[コード設計書の記述]
0001:コンビニ入金速報
0002:コンビニ入金確報
0003:ペーパーレス確報
0004:郵便振込速報
0005:郵便振込確報
0006:郵便振込
0007:銀行振込
0008:現金
';
COMMENT ON COLUMN public.deposits.confirmation_type IS '東コロ様のテーブル定義に「入金区分」があり、その内容が"0:確定データ"であるため、このフィールドが確報、速報、取消を表すと考える。

1:速報
2:確報
3:取消
とする。';
COMMENT ON COLUMN public.deposit_details.detail_type IS 'コードテーブルに定義されている
0001:正会員年会費
0002:WFOT個人年会費
0003:前年度WFOT個人年会費
0004:前受金
';
COMMENT ON COLUMN public.deposit_details.deposit_type IS '[東コロ様テーブル定義書の記述]
1:郵便振替
2:コンビニ
3:現金
↑こっちを採用。確報、速報は確報フラグを持つことで対応。

[コード設計書の記述]
0001:コンビニ入金速報
0002:コンビニ入金確報
0003:ペーパーレス確報
0004:郵便振込速報
0005:郵便振込確報
0006:郵便振込
0007:銀行振込
0008:現金
';
COMMENT ON COLUMN public.deposit_details.deposit_target IS '会費種別を親コードにもつ入金対象を保持する
[会費種別：新入会]
入会金、年会費、再入会手数料
[会費種別：正会員]
年会費、ＷＦＯＴ会費、前年度ＷＦＯＴ会費、前受金
[会費種別：研修会]
研修会費
[会費種別：生涯教育]
フォルダ再送付、徽章再送付、認定書再発行、試験料、認定料、受講カード再発行、手数料
[会費種別：賛助会員]
年会費';
COMMENT ON COLUMN public.dm_label_print_histories.label_kind_cd IS '発行するラベルの種類を保持する。
コード値は未定だが、現状では以下のものがある

＜会員管理、生涯教育、請求収納関連から連携＞
・【自宅用】
・【海外用】
・【施設用】
・【施設養成校用】
＜入会管理から連携＞
・【自宅用】
・【海外用】
・【施設用】
・【施設養成校用】
＜施設一覧から連携＞
・【施設用】
・【施設責任者用】
・【養成校用】
・【養成校情報責任者用】
＜賛助会員一覧から連携＞
・【基本情報用】
・【基本情報代表者用】
・【請求担当者用】
・【送付担当者用】
＜外部団体一覧から連携＞
・【外部団体用】
・【外部団体担当者用】
＜DMラベル／送り状個別登録から連携＞
・共通';
COMMENT ON COLUMN public.dm_label_print_histories.member_no_text IS '7桁で先頭0埋めする';
COMMENT ON COLUMN public.dm_label_print_histories.address1 IS 'DBの住所１フィールドのサイズと異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.address2 IS 'DBの住所２フィールドのサイズと異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.address3 IS 'DBの住所３フィールドのサイズと異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.last_nm IS 'サイズがDBの姓と異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.first_nm IS 'サイズがDBの名と異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.remarks IS 'サイズがDBの摘要と異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.facility_no IS '帳票定義では7桁になっているが、番号の省略はあり得ない。
DB定義の施設番号サイズ分確保する';
COMMENT ON COLUMN public.dm_label_print_histories.facility_nm1 IS 'サイズがDBの施設名１と異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.facility_nm2 IS 'サイズがDBの施設名２と異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.training_school_nm IS 'サイズがDBの養成校名と異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.fullname IS 'サイズがDBの姓、名と異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.primary_nm IS 'サイズがDBの個人氏名・代表者名と異なるので注意';
COMMENT ON COLUMN public.dm_label_print_histories.billing_section_nm IS 'サイズがDBの所属と異なるので注意';
COMMENT ON COLUMN public.doc_deficiency_descriptions.deficiency_cd IS '申請詳細画面で不備チェックされた項目を示すコード値';
COMMENT ON COLUMN public.doc_deficiency_histories.hisotry_type IS '新たにコード値を定義する。
1:メール
2:印刷
';
COMMENT ON COLUMN public.doc_deficiency_histories.email_address IS '会員のメールアドレスと同じ型桁にする';
COMMENT ON COLUMN public.doc_deficiency_reasons.category IS 'どの不備に対応する理由なのかを保持するカラム
';
COMMENT ON COLUMN public.doc_deficiency_reasons.title IS '書類不備画面のプルダウンに表示する件名
※画面定義では説明文そのものがプルダウンになっているがそれは実現が難しいと思われる。「項目」と「説明文」の間に件名(概略)を表示するプルダウンを用意して、それが選択されたら「説明文」が変わるような作りにする';
COMMENT ON COLUMN public.dsk_receipts.type_cd IS '01：速報 02 ：確定 03 ：速報取消
12：郵便振替 22：郵便振替訂正
81：リアルデータ';
COMMENT ON COLUMN public.dsk_receipts.receipted_date IS '店舗での収納日';
COMMENT ON COLUMN public.dsk_receipts.receipted_time IS '店舗での収納時刻
(データ種別が 12 , 22 の場合のときはNULL)
';
COMMENT ON COLUMN public.dsk_receipts.barcode IS 'EAN + バーコード情報(44桁)をそのまま設定';
COMMENT ON COLUMN public.dsk_receipts.user_data1 IS 'バーコード情報の１４桁目～１９桁目';
COMMENT ON COLUMN public.dsk_receipts.user_data2 IS 'バーコード情報の２０桁目～３０桁目
（３０桁目は再発行区分とする）';
COMMENT ON COLUMN public.dsk_receipts.stamp IS 'バーコード情報の37桁目';
COMMENT ON COLUMN public.dsk_receipts.amount_of_money IS 'バーコード情報の３８桁目～４３桁目
※データ種別が22（郵便振替訂正）の場合は、訂正後の金額がセットされます
※頭「0」は省略されます。例：001000→1000';
COMMENT ON COLUMN public.dsk_receipts.transfer_date IS 'データ種別が 02 , 12 , 22 の場合、資金の振込日
上記以外のデータ種別の場合NULL';
COMMENT ON COLUMN public.dsk_receipts.billing_id IS '0埋め10桁';
COMMENT ON COLUMN public.dsk_receipts.member_no IS '0埋め6桁';
COMMENT ON COLUMN public.dsk_receipts.number_of_reissues IS '再発行回数';
COMMENT ON COLUMN public.dsk_receipts.payment_due_date IS '支払期限yy/MM/dd';
COMMENT ON COLUMN public.examination_applications.examination_cd IS '01:認定作業療法士取得臨床実践能力試験
02:認定作業療法士資格再認定審査（試験）
03:専門作業療法士資格認定審査（試験）
';
COMMENT ON COLUMN public.examination_applications.field_cd IS '画面定義にない項目。
会員の専門分野であると仮定して定義する

01:身体障害
02:精神障害
03:発達障害
04:老年期障害
05:その他
';
COMMENT ON COLUMN public.examination_applications.application_result_cd IS '701:申請中
702:受験受付許可
703:書類不備
704:要件不足
705:保留
706:取下げ
707:合格
';
COMMENT ON COLUMN public.examination_applications.application_status_cd IS '701:事務局承認待ち
702:許可済み
703:不許可';
COMMENT ON COLUMN public.examination_applications.facility_nm IS '画面定義では20になっているが、施設名1に128文字、施設名2に256文字入力される可能性がある。よってサイズを可変にしておく';
COMMENT ON COLUMN public.examination_applications.contact_cd IS '自宅 or 連絡先';
COMMENT ON COLUMN public.examination_applications.address IS '会員の住所1～3がそれぞれ32文字なので100あれば格納可能';
COMMENT ON COLUMN public.examination_applications.phone_number IS 'カラム名と長さは会員(members)に合わせた';
COMMENT ON COLUMN public.examination_applications.email_address IS '会員(members)の定義に合わせた';
COMMENT ON COLUMN public.examination_applications.id_photo_file_nm IS '証明写真ファイル名';
COMMENT ON COLUMN public.examination_applications.id_photo_file IS '証明写真ファイル';
COMMENT ON COLUMN public.examination_details.examination_cd IS '01:認定作業療法士取得臨床実践能力試験
02:認定作業療法士資格再認定審査（試験）
03:専門作業療法士資格認定審査（試験）
';
COMMENT ON COLUMN public.examination_details.field_cd IS '4桁のパターンもあるが、既存のsyogaiDBを見ると
mt_senmonテーブルがあり、それぞれコード値が１桁になっているので、2桁で決定する';
COMMENT ON COLUMN public.examination_details.examination_type IS 'フリーテキスト';
COMMENT ON COLUMN public.examination_details.bank_account IS '口座番号だけ？不明';
COMMENT ON COLUMN public.exam_no_seqs.field_cd IS 'コード設計[30010]から専門分野の値を抽出

00:なし
01:福祉用具
02:認知症
03:手外科
04:特別支援
05:高次脳
06:精神急性
07:摂食嚥下
08:訪問
09:がん
10:就労支援
';
COMMENT ON COLUMN public.exemption_reasons.type IS '00： コード
他：研修種別';
COMMENT ON COLUMN public.external_groups.group_cd IS '01:団体
02:団体・個人
03:都道府県士会事務局
04:関係省庁
';
COMMENT ON COLUMN public.external_groups.journal_send_cd IS '01:機関誌のみ
02:機関紙+学術誌
03:学術誌のみ
04:不要
';
COMMENT ON COLUMN public.external_groups.phone_number IS '東コロ様DB定義が15桁になっている。
外部団体が国内とは限らないので15桁にする';
COMMENT ON COLUMN public.external_groups.representative_honorifics_cd IS '01:様
02:御中
→画面定義書には"殿"も定義されている。コード設計書には存在しない';
COMMENT ON COLUMN public.external_group_charges.charge_honorifics_cd IS '01:様
02:御中
→画面定義書には"殿"も定義されている。コード設計書には存在しない';
COMMENT ON COLUMN public.facilities.facility_base_no IS '初期登録時の施設番号';
COMMENT ON COLUMN public.facilities.facility_seq_no IS '旧システムにのみ存在するカラム。
移行データ格納のため残しておく';
COMMENT ON COLUMN public.facilities.facility_category IS '1:養成校（大学）
2:養成校（短期大学）
3:養成校（専門学校）
4:養成校（大学院）
5:開設準備室
9:施設';
COMMENT ON COLUMN public.facilities.facility_no IS '帳票定義では7桁になっているが、番号の省略はあり得ない。
DB定義の施設番号サイズ分確保する';
COMMENT ON COLUMN public.facilities.name_kana2 IS 'サイズは施設登録申請のフィールドに合わせた';
COMMENT ON COLUMN public.facilities.master_course_flag IS '0:なし
1:あり
2:大学院のみあり
※「2:大学院のみあり」は現行データには存在しないが、DB定義書で必要とある';
COMMENT ON COLUMN public.facilities.doctor_course_flag IS '0:なし
1:あり
2:大学院のみあり
※「2:大学院のみあり」は現行データには存在しないが、DB定義書で必要とある';
COMMENT ON COLUMN public.facilities.day_night_flag IS '1:昼間
2:夜間';
COMMENT ON COLUMN public.facilities.mtdlp_cooperator_flag IS '詳細検索画面で使用する';
COMMENT ON COLUMN public.facility_applications.facility_category IS '1:養成校（大学）
2:養成校（短期大学）
3:養成校（専門学校）
4:養成校（大学院）
5:開設準備室
9:施設';
COMMENT ON COLUMN public.facility_applications.member_category_cd IS '00：正会員
01：仮会員';
COMMENT ON COLUMN public.facility_applications.facility_prefecture_cd IS '01:北海道 ・・・ 47:沖縄、49:海外、50:不明';
COMMENT ON COLUMN public.facility_application_categories.category IS '1:医療関連
2:介護関連
3:障害総合支援法関連
4:その他';
COMMENT ON COLUMN public.facility_application_categories.category_division IS '1:大分類
2:中分類
3:小分類';
COMMENT ON COLUMN public.facility_categories.category IS '1:医療関連
2:介護関連
3:障害総合支援法関連
4:その他';
COMMENT ON COLUMN public.facility_categories.category_division IS '1:大分類
2:中分類
3:小分類';
COMMENT ON COLUMN public.facility_contacts.type IS '1:施設
2:養成校';
COMMENT ON COLUMN public.facility_training_school_update_histories.update_kind_cd IS '更新した処理の種類
１：施設情報登録
２：施設情報変更
３：施設情報削除
４：養成校情報登録
５：養成校情報変更
６：養成校情報削除';
COMMENT ON COLUMN public.handbook_migration_applications.member_category_cd IS '00：正会員
01：仮会員';
COMMENT ON COLUMN public.handbook_migration_applications.migration_type_cd IS '01:手帳移行
02:基礎ポイント移行';
COMMENT ON COLUMN public.import_attendees.deposit IS '「済」か、空欄かのいずれか？';
COMMENT ON COLUMN public.import_attendees.completion_status IS '帳票レイアウトに「修了〇」、「未修了×」と書いてある';
COMMENT ON COLUMN public.import_attendees.test_result IS '帳票レイアウトに「合格〇」と「不合格×」';
COMMENT ON COLUMN public.import_attendees.error_message IS '画面やレイアウト定義にないけど、エラーが出たときに分かるようにしておく必要があるのではないか、と思い追加した';
COMMENT ON COLUMN public.import_trainings.member_no IS '※ファイル取込者の会員番号。画面に出てこないが、データが迷子にならないようにフィールドとして持っておく';
COMMENT ON COLUMN public.import_trainings.category_cd IS 'コード設計[30010]の内容から抽出(現行のコード値？)
01:基礎研修
02:認定OT
03:専門OT
04:重点
05:臨床指導者
21:特別
99:その他';
COMMENT ON COLUMN public.import_trainings.venue_pref_cd IS '研修会一覧画面の検索条件になっている。

コード設計[10011]
01:北海道
......
47:沖縄県
48:海外';
COMMENT ON COLUMN public.import_trainings.event_date2 IS '開催日２日目';
COMMENT ON COLUMN public.import_trainings.event_date3 IS '開催日３日目';
COMMENT ON COLUMN public.import_trainings.organizer_cd IS '新システムでの主催コードは2桁。
画面定義書にある帳票定義を見る限り、取込の場合には主催者コードを複数件セットすることは出来ないようだ（共催に出来ない）。';
COMMENT ON COLUMN public.import_trainings.organizer_nm IS '桁数の定義が不明なので適当なサイズにしておく';
COMMENT ON COLUMN public.import_trainings.entry_fee_deposit IS '帳票項目定義によると「必須」か「任意」かの2種類が入るとのこと。';
COMMENT ON COLUMN public.import_trainings.reception_on_the_day IS '帳票項目定義によると「あり」か「なし」の２種類。';
COMMENT ON COLUMN public.import_trainings.acceptance IS '帳票レイアウトを見ると「あり」か「なし」が入りそう';
COMMENT ON COLUMN public.import_trainings.attendance_recept IS 'おそらく「初日のみ」か「全日」の２パターン。
（コード設計[30240]）';
COMMENT ON COLUMN public.import_trainings.leave_confirm IS 'コード設計によると「なし」か「あり」。';
COMMENT ON COLUMN public.import_trainings.error_message IS '取り込んだファイルのエラー内容が格納される';
COMMENT ON COLUMN public.import_training_applicants.match_flag IS '画面レイアウトによると、完全一致は「登録されている会員の氏名と完全に一致しています。」という状況のようだ。';
COMMENT ON COLUMN public.import_training_applicants.collation_rate IS '画面設計に以下の記述がある。

「・100%・・・文字が同じで、名前の並び順が違う場合、表示されます。
・それ以降は一致している文字の割合（%）を表示します。
　（例）4文字中、3文字一致している ⇒ 75%
」
';
COMMENT ON COLUMN public.import_training_applicants.error_message IS '取り込んだファイルのエラー内容が格納される';
COMMENT ON COLUMN public.import_training_histories.excel_file_nm IS 'ファイル名の機能的な制限は250文字だが、画面定義に合わせる';
COMMENT ON COLUMN public.import_training_histories.training_type IS '01:現職者　共通研修
02:現職者　選択研修
03:基礎ポイント研修
04:認定作業療法士 共通研修
05:認定作業療法士 選択研修
06:専門作業療法士 専門基礎研修
07:専門作業療法士 専門応用研修
08:専門作業療法士 専門研究・開発研修
09:基礎研修修了者　認定年月日
10:認定作業療法士　認定年月日
11:専門作業療法士　認定年月日
12:事例報告登録
13:臨床実習指導者研修修了者　認定年月日
14:臨床実習指導者講習会受講者　認定年月日
15:MTDLP基礎研修
16:士会研修会
17:士会裁量ポイント
18:講師(協会以外の研修会の講師）
19:eラーニング
20:全国研修会
21:学会
';
COMMENT ON COLUMN public.import_training_points.match_flag IS '画面レイアウトによると、完全一致は「登録されている会員の氏名と完全に一致しています。」という状況のようだ。';
COMMENT ON COLUMN public.import_training_points.error_message IS '取り込んだファイルのエラー内容が格納される';
COMMENT ON COLUMN public.individual_billing_print_histories.member_no IS '画面案に入力箇所がないが、帳票レイアウトの出力順が「会員番号順」となっていたのでカラムとして入れた。
恐らく設計書のミスだが、検索性を考えると画面とDBに会員番号があっても良いような気がするので、念のため入れておく';
COMMENT ON COLUMN public.individual_billing_print_histories.destination IS '画面定義でサイズ指定がない。';
COMMENT ON COLUMN public.individual_billing_print_histories.honorifics_cd IS '01:様
02:御中';
COMMENT ON COLUMN public.individual_billing_print_histories.proviso IS '画面設計でサイズが指定されていないのでtextにしておく';
COMMENT ON COLUMN public.individual_billing_print_histories.director_nm IS '英語で「理事」と「理事長」が同じDirectorになってしまう・・・';
COMMENT ON COLUMN public.info_by_fiscal_year.facility_base_no IS '初期登録時の施設番号';
COMMENT ON COLUMN public.info_by_fiscal_year.graduate_count IS '卒業者数の定員という言葉に違和感。
画面定義書を見ると入学定員より少ない値が入っていたりする';
COMMENT ON COLUMN public.info_by_fiscal_year_temp.process_id IS '同一の処理内で取り込まれたレコードに共通する番号を採番して設定する';
COMMENT ON COLUMN public.invoice_print_histories.invoice_kind_cd IS '発行する送り状の種類を保持する。
コード値は未定だが、現状では以下のものがある

＜会員管理関連、生涯教育関連、請求収納関連から連携＞
・発送先が自宅の場合
・発送先が施設の場合
・発送先が養成校の場合
＜施設一覧から連携した場合＞
・発送先が施設、施設情報責任者の場合
＜養成校一覧から連携した場合＞
・発送先が養成校、養成校情報責任者の場合
＜賛助会員一覧から連携した場合＞
・発送先が基本情報、基本情報代表者の場合
・発送先が請求担当者の場合
・発送先が送付担当者の場合
＜外部団体一覧から連携した場合＞
・発送先が外部団体一覧で選択された発送先の場合
';
COMMENT ON COLUMN public.invoice_print_histories.address1 IS 'DBの住所１フィールドのサイズと異なるので注意';
COMMENT ON COLUMN public.invoice_print_histories.address2 IS 'DBの住所２フィールドのサイズと異なるので注意';
COMMENT ON COLUMN public.invoice_print_histories.address3 IS 'DBの住所３フィールドのサイズと異なるので注意';
COMMENT ON COLUMN public.invoice_print_histories.address4 IS 'フリーフォーマット';
COMMENT ON COLUMN public.invoice_print_histories.phone_number IS 'サイズは帳票定義の長さに合わせた';
COMMENT ON COLUMN public.invoice_print_histories.address_nm IS '名前や"作業療法士"といった肩書なども出力される。';
COMMENT ON COLUMN public.libraries.publish_range IS '公開範囲が「ファイル指定」の場合、公開対象の会員が「ライブラリ宛先」に格納される';
COMMENT ON COLUMN public.library_view_histories.view_date IS '監査項目である共通項目とは別に、閲覧日時を保持する。';
COMMENT ON COLUMN public.members.affiliated_prefecture_cd IS '01:北海道 ・・・ 47:沖縄、49:海外、50:不明';
COMMENT ON COLUMN public.members.member_cd IS 'パスワード再発行などで本人確認を行うためのコード。ユーザが自身で入力して設定する値';
COMMENT ON COLUMN public.members.entry_date IS '入会年月日';
COMMENT ON COLUMN public.members.gender IS '0：（未選択）
1：男性
2：女性';
COMMENT ON COLUMN public.members.wfot_entry_status IS '1：WFOT加入
2：WFOT未加入';
COMMENT ON COLUMN public.members.membership_fee_balance IS '会費残高';
COMMENT ON COLUMN public.members.shipping_category_no IS '0：（未選択）
1：自宅
2：施設';
COMMENT ON COLUMN public.members.foreign_address IS '発送区分が「海外」の場合';
COMMENT ON COLUMN public.members.print_enabled IS 'チェックしたデータは名簿に住所・電話番号が印刷されません';
COMMENT ON COLUMN public.members.ot_association_mail_receive_cd IS '1：受取OK
2：受取NG';
COMMENT ON COLUMN public.members.foreign_license IS 'TRUE：外免';
COMMENT ON COLUMN public.members.education_credits IS '生涯教育研修所得単位数の合計';
COMMENT ON COLUMN public.members.remarks IS '再入会情報など';
COMMENT ON COLUMN public.members.working_state_cd IS '０１：働いている
０２：働いていないが大学院生等
０３：働いていない';
COMMENT ON COLUMN public.members.working_prefecture_cd IS '勤務先の都道府県番号';
COMMENT ON COLUMN public.members.working_condition IS '11：常勤
12：非常勤';
COMMENT ON COLUMN public.members.working_condition_facility_count IS 'データ移行（2.0次未使用）';
COMMENT ON COLUMN public.members.working_condition_facility_name IS 'データ移行（2.0次未使用）';
COMMENT ON COLUMN public.members.facility_domain_cd IS '１：医療関連
２：介護関連
３：障害総合支援関連
４：その他関連
';
COMMENT ON COLUMN public.members.facility_sub_domain_cd IS '１：医療関連
２：介護関連
３：障害総合支援関連
４：その他関連
';
COMMENT ON COLUMN public.members.web_published IS 'WEB掲載フラグ';
COMMENT ON COLUMN public.members.pref_association_mail_receive_cd IS '1：受取OK
2：受取NG';
COMMENT ON COLUMN public.members.pref_association_member_info_hide IS 'チェックされたときは県士会に会員情報を開示しない';
COMMENT ON COLUMN public.members.pref_association_education_info_hide IS 'チェックされたときは県士会に生涯教育情報を開示しない
';
COMMENT ON COLUMN public.members.pref_association_fee_balance IS '県士会会費残高';
COMMENT ON COLUMN public.members.facility_info_hide IS 'チェックされたときは名簿に施設を出力しない
';
COMMENT ON COLUMN public.members.pref_association_value IS '県士会で使用する、使用方法は県士会で考える';
COMMENT ON COLUMN public.members.pref_association_nonmember IS '県士会で使用する、県士会未入会者はチェックする';
COMMENT ON COLUMN public.members.withdrawal_date IS '退会年月日';
COMMENT ON COLUMN public.members.journal_send_category_cd IS '１：電子
２：製本
３：発送不要';
COMMENT ON COLUMN public.members.security_cd IS 'パスワード再発行で本人確認用に使うコード。ユーザが自身で手入力して設定する';
COMMENT ON COLUMN public.membership_fee_deliveries.category_cd IS '1：入会金
2：年会費
3：WFOT
4：再入会手数料';
COMMENT ON COLUMN public.membership_fee_deliveries.payment_type IS '１：郵便振替
２：コンビニエンス
９：その他';
COMMENT ON COLUMN public.membership_fee_deliveries.data_type IS '１：速報
２：確報
９：その他';
COMMENT ON COLUMN public.membership_fee_deliveries.application_content IS '１：会費免除
２：分納
３：支払い猶予';
COMMENT ON COLUMN public.member_applications.register_category_cd IS '１：WEB
２：事務局';
COMMENT ON COLUMN public.member_applications.gender IS '0：（未選択）
1：男性
2：女性';
COMMENT ON COLUMN public.member_applications.shipping_category_no IS '0：（未選択）
1：自宅
2：施設';
COMMENT ON COLUMN public.member_applications.foreign_license IS 'TRUE：外免';
COMMENT ON COLUMN public.member_applications.foreign_address IS '発送区分が「海外」の場合';
COMMENT ON COLUMN public.member_applications.working_state_cd IS '０１：働いている
０２：働いていないが大学院生等
０３：働いていない';
COMMENT ON COLUMN public.member_applications.working_condition IS '11：常勤
12：非常勤';
COMMENT ON COLUMN public.member_applications.facility_domain_cd IS '１：医療関連
２：介護関連
３：障害総合支援関連
４：その他関連
';
COMMENT ON COLUMN public.member_applications.facility_sub_domain_cd IS '１：医療関連
２：介護関連
３：障害総合支援関連
４：その他関連
';
COMMENT ON COLUMN public.member_applications.status_cd IS 'コード設計には存在しない
現行1.5次のDBにある値は以下のもの
0:登録
8:処理済
9:削除';
COMMENT ON COLUMN public.member_applications.entry_category_cd IS '０：再
１：旧再
２：新
３：喪失';
COMMENT ON COLUMN public.member_applications.entry_date IS '入会年月日';
COMMENT ON COLUMN public.member_app_facility_categories.category_division IS '1:大分類
2:中分類（主）
3:小分類
4:中分類（従）';
COMMENT ON COLUMN public.member_app_facility_categories.primary_division IS '1:主
2:従';
COMMENT ON COLUMN public.member_app_local_activities.activity_seq IS '入会申し込みの自治体活動内容の連番';
COMMENT ON COLUMN public.member_app_qualifications.qualification_category_cd IS '１：国家資格
２：その他関連資格';
COMMENT ON COLUMN public.member_email_address_histories.temp_key IS 'URLからの呼び出しパラメータと照合するキー';
COMMENT ON COLUMN public.member_facility_categories.category_division IS '1:大分類
2:中分類（主）
3:小分類
4:中分類（従）';
COMMENT ON COLUMN public.member_facility_categories.primary_division IS '1:主
2:従';
COMMENT ON COLUMN public.member_local_activities.activity_seq IS '入会申し込みの自治体活動内容の連番';
COMMENT ON COLUMN public.member_qualifications.qualification_category_cd IS '１：国家資格
２：その他関連資格';
COMMENT ON TABLE public.member_revoked_authorities IS '権限コード値に対応するパス、操作のキー値をブラックリスト形式で保持する。';
COMMENT ON COLUMN public.member_state_histories.withdrawal_date IS '退会年月日';
COMMENT ON COLUMN public.member_state_histories.entry_date IS '入会年月日';
COMMENT ON COLUMN public.member_withdrawal_histories.withdrawal_date IS '退会年月日';
COMMENT ON COLUMN public.member_withdrawal_histories.member_fee IS '会費残高';
COMMENT ON COLUMN public.monthly_processes.extraction_type_cd IS '01:MTDLP研修修了者
02:MTDLP指導者
03:認定作業療法士終身対象者
';
COMMENT ON COLUMN public.monthly_processes.process_month IS 'YYYYMM';
COMMENT ON COLUMN public.monthly_processes.member_nm IS '会員の氏名が姓:20 名:20なので、間に入れるスペースもあることだし、余裕をもって50で定義';
COMMENT ON COLUMN public.mtdlp_qualify_histories.training_type IS '研修、講習会、厚生労働省認定';
COMMENT ON COLUMN public.mtdlp_training_histories.training_cd IS 'YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス';
COMMENT ON COLUMN public.mtdlp_training_histories.professional_field_cd IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.officer_histories.prefecture_cd IS '役職が代議員の場合のみ使用';
COMMENT ON COLUMN public.other_org_point_applications.applicant_cd IS '01:会員
02:事務局';
COMMENT ON COLUMN public.other_org_point_applications.attending_date_check IS '重複チェックとのことだが、何の重複なのか不明';
COMMENT ON COLUMN public.other_org_point_applications.other_organization_sig_cd IS '他団体・SIGマスタのIDを持つ。東コロ様のテーブル定義書ではserialで定義されているが、他と合わせてbigserialと想定し、bigint型の外部キーとする';
COMMENT ON COLUMN public.other_org_point_applications.approval_flag IS '一度でも承認した場合は"1"';
COMMENT ON COLUMN public.other_org_point_applications.temp_flag IS '1:仮登録';
COMMENT ON COLUMN public.other_org_point_application_attachments.file_order IS 'ファイルの順序';
COMMENT ON COLUMN public.other_org_point_application_attachments.file_reject_cd IS 'コードマスタ';
COMMENT ON COLUMN public.ot_activity_results.presenter_category_cd IS '01:単著
02:単独
03:共著（主）
04:共同（主）
05:共著（共）
06:共同（共）
07:単訳
08:共訳
';
COMMENT ON COLUMN public.ot_activity_results.category IS 'A:Ａ：脳血管疾患等
B:Ｂ：心大血管疾患
C:Ｃ：呼吸器疾患
D:Ｄ：運動器疾患
E:Ｅ：神経難病
F:Ｆ：がん
G:Ｇ：内科疾患
H:Ｈ：精神障害
I:Ｉ：発達障害
J:Ｊ：高齢期
K:Ｋ：認知障害（高次脳機能障害を含む）
L:Ｌ：援助機器
M:Ｍ：生活行為向上マネジメント
N:Ｎ：地域
O:Ｏ：理論
P:Ｐ：基礎研究
Q:Ｑ：管理運営
R:Ｒ：教育
S:Ｓ：その他
';
COMMENT ON COLUMN public.ot_papers.paper_presentation_cd IS '01:日本作業療法学会
02:ISSN/ISBN登録の雑誌・書籍
03:学術誌作業療法（研究論文，実践報告）
04:WFOT学会
05:APOTEC学会
06:WFOT加盟国の協会が発行する機関誌（原著論文）
';
COMMENT ON COLUMN public.paot_membership_fee_deliveries.seq_no IS '表示順制御に使ってください';
COMMENT ON COLUMN public.paot_membership_fee_deliveries.category_cd IS '1：入会金
2：年会費
3：WFOT
4：再入会手数料';
COMMENT ON COLUMN public.period_extension_applications.qualification_cd IS '01:基礎研修修了者
02:認定作業療法士';
COMMENT ON COLUMN public.period_extension_applications.application_reason_cd IS '01:留学
02:海外勤務
03:出産・育児休暇
04:介護休暇
05:長期病気療養
06:その他
';
COMMENT ON COLUMN public.period_extension_applications.application_result_cd IS '501:申請中
502:許可
503:書類不備
504:保留
505:取下げ
';
COMMENT ON COLUMN public.period_extension_applications.application_status_cd IS '501:事務局承認待ち
502:許可済み
503:不許可
';
COMMENT ON COLUMN public.point_application_sampling_settings.application_type_cd IS '01:他団体・SIGポイント申請 02:手帳移行申請 03:臨床実習ポイント申請';
COMMENT ON COLUMN public.point_application_sampling_settings.next_exec_datetime IS '次回サンプリング実施予定日時';
COMMENT ON COLUMN public.point_application_sampling_settings.extraction_ratio IS 'サンプリング調査する対象を抽出する割合（％）';
COMMENT ON COLUMN public.point_application_sampling_settings.executed IS 'Not Null, DEFAULT=FALSE';
COMMENT ON COLUMN public.print_names.status_cd IS '確証はないがコード値はこれか
01:印刷待ち
02:印刷済み
03:取下げ
';
COMMENT ON COLUMN public.professional_ot_attending_summaries.professional_field IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.professional_ot_qualify_histories.professional_field IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.professional_ot_test_histories.professional_field IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.professional_ot_training_histories.training_cd IS 'YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス';
COMMENT ON COLUMN public.professional_ot_training_histories.professional_field IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.qualificate_ot_applications.applicant_cd IS '01:会員
02:事務局';
COMMENT ON COLUMN public.qualificate_ot_applications.application_type IS '１：新規
２：更新';
COMMENT ON COLUMN public.qualificate_ot_applications.application_status_cd IS '201:委員会審査待ち
202:理事会承認待ち
203:要件充足アップ待ち
204:合格
';
COMMENT ON COLUMN public.qualificate_ot_applications.application_result_cd IS '他と合わせて3桁で定義';
COMMENT ON COLUMN public.qualificate_ot_applications.doc_deficiency_reason_cd IS 'コードマスタに存在しない項目。他と合わせて2桁';
COMMENT ON COLUMN public.qualificate_ot_applications.application_pattern_cd IS 'コード値の詳細は不明';
COMMENT ON COLUMN public.qualificate_ot_applications.force_display_check IS '画面の「更新申請中、資格あり」チェックと対応';
COMMENT ON COLUMN public.qualifications.category_cd IS '01：国家資格
02：福祉系国家資格
02：その他リハビリテーション関連資格';
COMMENT ON COLUMN public.qualification_no_seqs.field_cd IS 'コード設計[30010]から専門分野の値を抽出

00:なし
01:福祉用具
02:認知症
03:手外科
04:特別支援
05:高次脳
06:精神急性
07:摂食嚥下
08:訪問
09:がん
10:就労支援
';
COMMENT ON COLUMN public.qualification_ot_app_histories.seq IS 'No1から5の連番';
COMMENT ON COLUMN public.qualified_ot_training_histories.training_cd IS 'YYYY + 主催 + カテゴリ + 専門分野 + グループ + クラス';
COMMENT ON COLUMN public.qualified_ot_training_histories.professional_field_cd IS '福祉用具、認知症、手外科、特別支援教育・・';
COMMENT ON COLUMN public.receipt_print_histories.receipt_address_cd IS '領収書の宛先種別の選択状態を保持する
コード定義が存在しないが画面には以下の値がある
・会員名
・施設名
・施設名＋会員名
・明細部入力値';
COMMENT ON COLUMN public.receipt_print_histories.receipt_date_cd IS '領収書の日付種別の選択状態を保持する
コード定義が存在しないが画面には以下の値がある
・入金日
・明細部入力値
・一括指定日
';
COMMENT ON COLUMN public.receipt_print_histories.receipt_proviso_cd IS '領収書の領収書但し書き種別の選択状態を保持する
コード定義が存在しないが画面には以下の値がある
・領収書種別
・明細部入力値
・一括指定値';
COMMENT ON COLUMN public.receipt_print_histories.receipt_amount_cd IS '領収書の領収書金額種別の選択状態を保持する
コード定義が存在しないが画面には以下の値がある
・入金額
・明細部入力値';
COMMENT ON COLUMN public.reminder_print_histories.billing_no IS '10桁の連番 + 6桁の会員番号';
COMMENT ON COLUMN public.reprint_applications.applicant_cd IS '01:会員
02:事務局';
COMMENT ON COLUMN public.reprint_applications.print_category_cd IS '01:基礎研修修了証
02:認定作業療法士認定証（終身含む）
03:専門作業療法士認定証
04:臨床実習指導者研修修了者修了証
05:臨床実習指導施設認定証
06:理学療法士作業療法士臨床実習指導者講習会修了証
07:WFOT認定証(OT協会)
08:MTDLP研修修了者修了証
09:MTDLP指導者認定証　
10:領収証
';
COMMENT ON COLUMN public.reprint_applications.member_nm IS '会員の氏名が姓:20 名:20なので、間に入れるスペースもあることだし、余裕をもって50で定義';
COMMENT ON COLUMN public.reprint_applications.field_cd IS '01:身体障害
02:精神障害
03:発達障害
04:老年期障害
05:その他
';
COMMENT ON COLUMN public.reprint_applications.facility_nm IS '施設名１(128)と２(128)を足したサイズ';
COMMENT ON COLUMN public.reprint_applications.print_status_cd IS '01:印刷待ち
02:印刷済み
03:取下げ
';
COMMENT ON COLUMN public.reprint_applications.create_date IS '何の登録日なのかはっきりしない。
画面でも表示している申請日と何が違うのか。';
COMMENT ON COLUMN public.reward_punish_histories.reward_punish_type IS '１：表彰
２：罰則';
COMMENT ON COLUMN public.send_mails.mail_type_cd IS 'コード設計には存在しない（画面定義にあるコード値）
1:手動
2:自動';
COMMENT ON COLUMN public.send_mails.auto_send_id IS 'メールテンプレートのIDが設定される
旧システムはphpファイルでテンプレートが定義されていて、t_mailにはそのテンプレートを示すIDが保持されている';
COMMENT ON COLUMN public.sig_other_groups.sig_type IS '01:学会・研究会
02:SIG
03:関連・職能団体
04:国際学会等関連・職能団体
05:養成校
06:同窓会
07:その他
';
COMMENT ON COLUMN public.sig_other_groups.status_cd IS '01:認定
02:取消
03:休止
';
COMMENT ON COLUMN public.sig_other_groups.certification_date IS 'qualification:能力、経験
certification:認可　←こっちをカラム名にする';
COMMENT ON COLUMN public.sig_other_groups.confirmer_cd IS '01:協会
02:47士会';
COMMENT ON COLUMN public.sig_other_group_histories.status_cd IS '東コロ様DB定義より。他団体SIGの「状態」カラムとは値が異なるようだ。
01:初回
02:取消
03:休止
04:再開
05:復活
06:変更';
COMMENT ON COLUMN public.sig_other_group_histories.certification_date IS 'qualification:能力、経験
certification:認可　←こっちをカラム名にする';
COMMENT ON COLUMN public.social_contribution_actual2_fields.field_cd IS '身体障害
精神障害
発達障害
老年期生涯';
COMMENT ON COLUMN public.social_contribution_actual_fields.field_cd IS '身体障害
精神障害
発達障害
老年期生涯';
COMMENT ON COLUMN public.social_contribution_actual_results.register_category_cd IS '１：WEB
２：事務局';
COMMENT ON COLUMN public.social_contribution_actual_results.group_name IS '団体/学会/研修会/会議/等';
COMMENT ON COLUMN public.social_contribution_actual_results.group_detail IS '他団体/他学会の詳細';
COMMENT ON COLUMN public.special_ot_applications.application_type IS '１：新規
２：更新';
COMMENT ON COLUMN public.special_ot_applications.fiscal_year IS 'YYYYMM';
COMMENT ON COLUMN public.special_ot_applications.status_cd IS '301:審査委員会承認待ち
302:理事会承認待ち
';
COMMENT ON COLUMN public.special_ot_applications.examination_result_cd IS '値の内容は不明';
COMMENT ON COLUMN public.special_ot_application_histories.application_type IS '１：新規
２：更新';
COMMENT ON COLUMN public.special_ot_application_histories.fiscal_year IS 'YYYYMM';
COMMENT ON COLUMN public.special_ot_application_histories.status_cd IS '301:審査委員会承認待ち
302:理事会承認待ち
';
COMMENT ON COLUMN public.special_ot_application_histories.examination_result_cd IS '値の内容は不明';
COMMENT ON COLUMN public.support_members.type_cd IS '１：A会員
２：B会員
３：C会員';
COMMENT ON COLUMN public.support_members.billing_prefecture_cd IS '01:北海道 ・・・ 47:沖縄、49:海外、50:不明';
COMMENT ON COLUMN public.support_members.entry_date IS '入会年月日';
COMMENT ON COLUMN public.support_members.withdrawal_date IS '退会年月日';
COMMENT ON COLUMN public.support_members.shipping_prefecture_cd IS '01:北海道 ・・・ 47:沖縄、49:海外、50:不明';
COMMENT ON COLUMN public.support_members.billing_send_category_cd IS '１：担当者宛
２：部署所属宛
';
COMMENT ON COLUMN public.support_members.shipping_send_category_cd IS '１：担当者宛
２：部署所属宛
';
COMMENT ON COLUMN public.temporary_deposit_details.deposit_data_type IS '画面定義によるとこんな感じ
1:コンビニ入金
2:郵便振込入金
3:ペーパーレス入金
';
COMMENT ON COLUMN public.temporary_deposit_details.billing_no IS '10桁の連番 + 6桁の会員番号';
COMMENT ON COLUMN public.temporary_deposit_details.billing_type_cd IS '会費種別のこと。
0:正会員
1:賛助会員
2:入会
3:研修会
4:生涯教育
';
COMMENT ON COLUMN public.temporary_deposit_details.deposit_division_no IS '請求データが分割されている場合の番号

分割なし：0
分割あり：0～';
COMMENT ON COLUMN public.temporary_deposit_details.deposit_type IS '[東コロ様テーブル定義書の記述]
1:郵便振替
2:コンビニ
3:現金
↑こっちを採用。確報、速報は確報フラグを持つことで対応。

[コード設計書の記述]
0001:コンビニ入金速報
0002:コンビニ入金確報
0003:ペーパーレス確報
0004:郵便振込速報
0005:郵便振込確報
0006:郵便振込
0007:銀行振込
0008:現金
';
COMMENT ON COLUMN public.temporary_deposit_details.confirmation_type IS '東コロ様のテーブル定義に「入金区分」があり、その内容が"0:確定データ"であるため、このフィールドが確報、速報、取消を表すと考える。

1:速報
2:確報
3:取消
とする。';
COMMENT ON COLUMN public.temporary_deposit_details.expense_cd IS '請求の費目と合わせた
0001:正会員年会費
0002:WFOT個人年会費
0003:前年度WFOT個人年会費
0004:前受金
';
COMMENT ON COLUMN public.total_receipt_print_histories.destination IS '画面定義でサイズ指定がない。';
COMMENT ON COLUMN public.total_receipt_print_histories.honorifics_cd IS '01:様
02:御中';
COMMENT ON COLUMN public.total_receipt_print_histories.proviso IS '画面設計でサイズが指定されていないのでtextにしておく';
COMMENT ON COLUMN public.trainings.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.trainings.old_training_no IS '旧システムでの研修会番号(6桁)を保持するテーブル。
移行データの格納先となる。';
COMMENT ON COLUMN public.trainings.hold_days IS '画面のプルダウンに表示される値は以下の6種類。コード定義がないのでコード値不明
1
2
3
1(ﾌﾘｰ)
2(ﾌﾘｰ)
3(ﾌﾘｰ)';
COMMENT ON COLUMN public.trainings.hold_count IS '画面定義では"05"という先頭0埋めの数値で表示しているが、
1.5次では数値で保持している。
コード値ではなく回数なので意味的にもintで良い';
COMMENT ON COLUMN public.trainings.course_nm IS '1.5次のDBではvarchar(200)になっているのでそちらに合わせる';
COMMENT ON COLUMN public.trainings.reception_status_cd IS '01:受付前
02:受付中
03:受付終了
';
COMMENT ON COLUMN public.trainings.update_status_cd IS '10:新規
11:コピー
12:仮登録
20:保留中
30:運営更新
40:本登録
→ここに「入金待ち」を入れる？
';
COMMENT ON COLUMN public.trainings.target_cd IS 'コード設計[30220]
00:正会員のみ
10:一般公開（学生除く）
11:一般公開（学生含む）
';
COMMENT ON COLUMN public.trainings.event_date1 IS '研修会開催日１日目

以下の特徴があり、子テーブルに分けるメリットが少ないので
研修会テーブルに持つ

・業務的に最大3日までしか存在しない
・1日目のみ開始時間を持っている';
COMMENT ON COLUMN public.trainings.event_date2 IS '開催日２日目';
COMMENT ON COLUMN public.trainings.event_date3 IS '開催日３日目';
COMMENT ON COLUMN public.trainings.delivery_start_date IS 'e-learningの配信開始日';
COMMENT ON COLUMN public.trainings.delivery_end_date IS 'e-learningの配信終了日';
COMMENT ON COLUMN public.trainings.prefecture_cd IS '開催場所フリー入力の場合の選択値はここに保持する';
COMMENT ON COLUMN public.trainings.venue_free_flag IS '【開催場所】の「フリー入力」に対応';
COMMENT ON COLUMN public.trainings.venue_free IS '既存の研修会テーブル(t_kenshukai)では開催場所1,2について256文字で入力できる。その２つのフィールドの値はここに設定する';
COMMENT ON COLUMN public.trainings.venue_pref_cd IS '研修会一覧画面の検索条件になっている。

コード設計[10011]
01:北海道
......
47:沖縄県
48:海外';
COMMENT ON COLUMN public.trainings.operator_doc_no_to_superiors IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.trainings.operator_doc_no_to_person IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.trainings.operator_doc_print_format_cd IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する

コード設計の「30390」が該当
10:PDF(全体)
20:EXCEL';
COMMENT ON COLUMN public.trainings.instructor_doc_no_to_superiors IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.trainings.instructor_doc_no_to_person IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.trainings.instructor_doc_print_format_cd IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する


コード設計の「30400」が該当
11:PDF（全体）
12:PDF（個別）
20:EXCEL
';
COMMENT ON COLUMN public.trainings.training_program_free_flag IS '画面の【研修会プログラム】の「フリー入力」と対応';
COMMENT ON COLUMN public.trainings.other6 IS '論理名からして不明すぎるカラム。';
COMMENT ON COLUMN public.trainings.end_flag IS '一覧画面で「研修会終了」として使われている';
COMMENT ON COLUMN public.trainings.cancel_flag IS '一覧画面の「中止」チェックボックスとして使われている';
COMMENT ON COLUMN public.trainings.hp_posted_flag IS '画面の「協会HP掲載」と対応';
COMMENT ON COLUMN public.training_advance_payments.payment_use_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金
';
COMMENT ON COLUMN public.training_advance_payments.remarks IS 'サイズは東コロ様DB定義に合わせた';
COMMENT ON COLUMN public.training_applications.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_applications.member_no IS '会員番号が設定される。一般申込者はシステムで扱われない';
COMMENT ON COLUMN public.training_applications.queue_no IS '画面定義書「申込者一覧タブ」の整理番号に対応する項目。申込順に採番するのだろう。
仮申込一覧には表示されていないので、仮登録データには持たないと思われる';
COMMENT ON COLUMN public.training_applications.applicant_category_cd IS 'コード設計[30120]
01:会員
02:他職種
03:学生
';
COMMENT ON COLUMN public.training_applications.application_status_cd IS 'コード設計[30180]
00:申込前
10:仮申込
20:本申込
30:受講許可
40:受講不許可
50:受講決定';
COMMENT ON COLUMN public.training_applications.procedure_status_cd IS 'コード設計[30190]
00:削除
10:取消
20:辞退
30:要再提出
40:再提出済
50:督促
51:打診中
52:打診回答済
';
COMMENT ON COLUMN public.training_applications.contact_type IS 'コード設計[30090]
01:WEB
02:郵送
';
COMMENT ON COLUMN public.training_applications.target_disease IS '画面の「疾患・障害名」。
項目名「対象疾患」は東コロ様DB定義を使った。
そちらの方がより正確に表現されていると思ったため。';
COMMENT ON COLUMN public.training_applications.case_provide_cd IS 'コード設計[30140]
01:可能
02:場合による
03:できない
';
COMMENT ON COLUMN public.training_applications.job_category_cd IS 'OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する

コード設計[30080]
※同じコード値で別の内容が指定されている。
　本当にこのコードでいいのか要確認

・画面定義書では以下の３種類と指定されている
大学教授
学生
厚生労働省職員
';
COMMENT ON COLUMN public.training_applications.work_place_nm IS 'OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する';
COMMENT ON COLUMN public.training_applications.work_place_zipcode IS 'OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する';
COMMENT ON COLUMN public.training_applications.work_place_prefecture_cd IS 'OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する';
COMMENT ON COLUMN public.training_applications.work_place_address1 IS 'OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する';
COMMENT ON COLUMN public.training_applications.work_place_address2 IS 'OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する';
COMMENT ON COLUMN public.training_applications.work_place_address3 IS 'OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する';
COMMENT ON COLUMN public.training_applications.phone_number IS 'OT-1170 不要なカラムである。スナップショットとして必要でない限り削除する';
COMMENT ON COLUMN public.training_applications.reminder_flag IS 'コード設計[30210]
0:なし
1:あり
';
COMMENT ON COLUMN public.training_applications.advanced_win_consult_flag IS 'コード設計[30200]
0:回答なし
1:受講する
2:受講できない
';
COMMENT ON COLUMN public.training_application_histories.member_no IS '会員番号が設定される。
';
COMMENT ON COLUMN public.training_application_histories.application_status_cd IS 'コード設計[30180]
00:申込前
10:仮申込
20:本申込
30:受講許可
40:受講不許可
50:受講決定';
COMMENT ON COLUMN public.training_application_histories.procedure_status_cd IS 'コード設計[30190]
00:削除
10:取消
20:辞退
30:要再提出
40:再提出済
50:督促
51:打診中
52:打診回答済
';
COMMENT ON COLUMN public.training_application_histories.contact_type IS 'コード設計[30090]
01:WEB
02:郵送
';
COMMENT ON COLUMN public.training_application_histories.target_disease IS '画面の「疾患・障害名」。
項目名「対象疾患」は東コロ様DB定義を使った。
そちらの方がより正確に表現されていると思ったため。';
COMMENT ON COLUMN public.training_application_histories.case_provide_cd IS 'コード設計[30140]
01:可能
02:場合による
03:できない
';
COMMENT ON COLUMN public.training_application_histories.advanced_win_consult_flag IS 'コード設計[30200]
0:回答なし
1:受講する
2:受講できない
';
COMMENT ON COLUMN public.training_attendance_priorities.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_attendance_priorities.priority_item_cd IS 'コード設計[30450]';
COMMENT ON COLUMN public.training_attendance_priorities.priority_point_cd IS 'コード設計[30460]';
COMMENT ON COLUMN public.training_attendance_requirements.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_attendance_requirements.requirement_cd IS 'コード値不明。サイズも分からないので6桁と仮置きする

画面定義書に以下のような値が例として挙げられている
日本作業療法士協会正会員
都道府県作業療法士会正会員
生涯教育制度基礎研修修了者
認定作業療法士取得研修　選択研修　受講資格保有者
';
COMMENT ON COLUMN public.training_attendance_results.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_attendance_results.member_no IS '会員番号が設定される。
';
COMMENT ON COLUMN public.training_attendance_results.attendance_status1_cd IS '子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。

値はコード設計[30260]
00:欠席
10:出席
20:免除';
COMMENT ON COLUMN public.training_attendance_results.leave_status1_cd IS '子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。

値はコード設計[30270]
00:退席なし
10:途中退席
20:退席';
COMMENT ON COLUMN public.training_attendance_results.attendance_status2_cd IS '子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。

値はコード設計[30260]
00:欠席
10:出席
20:免除';
COMMENT ON COLUMN public.training_attendance_results.leave_status2_cd IS '子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。

値はコード設計[30270]
00:退席なし
10:途中退席
20:退席';
COMMENT ON COLUMN public.training_attendance_results.attendance_status3_cd IS '子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。

値はコード設計[30260]
00:欠席
10:出席
20:免除';
COMMENT ON COLUMN public.training_attendance_results.leave_status3_cd IS '子テーブルに分けない。OT協会では研修会は３日までと決まっているようだ。

値はコード設計[30270]
00:退席なし
10:途中退席
20:退席';
COMMENT ON COLUMN public.training_attendance_results.completion_status_cd IS 'コード設計[30280]
00:未修了
10:修了';
COMMENT ON COLUMN public.training_attendance_results.test_result_cd IS 'コード設計[30290]
00:不合格
10:合格
※コード設計にはないが、画面で「試験免除者数」を表示する必要があるので、「20:試験免除」といった値が必要そう';
COMMENT ON COLUMN public.training_attendance_results.retest_result_cd IS 'コード設計[30290]
00:不合格
10:合格
※コード設計にはないが、画面で「試験免除者数」を表示する必要があるので、「20:試験免除」といった値が必要そう';
COMMENT ON COLUMN public.training_attendance_results.pay_on_the_day_flag IS '申込出席者一覧画面の、検索結果の明細行の「当日収納」をクリックすることで切り替えるフラグ

true:当日収納
false:-';
COMMENT ON COLUMN public.training_attendance_results.entry_fee IS '研修会の参加費を格納する。
（請求と紐づけるという手もあるが、取り回しが面倒そうなのでここに持ってしまう）
研修会の参加費は、通常の金額と学割金額があるので、どちらを適用するか判定してこのフィールドに格納する';
COMMENT ON COLUMN public.training_categories.type_cd IS '管理対象を示すコード値

コード設計[30110]
01:受講要件
02:免除要件
03:優先順位
04:講師
05:運営担当者
06:講師条件
';
COMMENT ON COLUMN public.training_categories.training_type_cd IS 'コード設計[30010]
※この研修会種別は新システムでのコード体系で、
旧システムの(「研修会カテゴリ(2桁)」+「専門分野(2桁)」+「研修会グループ(2桁)])=新システムの「研修会種別(6桁)」となっているようだ。なので旧システムでの３カラムを１カラムで扱う

研修会に紐づける受講要件や免除要件はこのコード値が格納される';
COMMENT ON COLUMN public.training_categories.category_cd IS 'コード設計[30010]の内容から抽出(現行のコード値？)
01:基礎研修
02:認定OT
03:専門OT
04:重点
05:臨床指導者
21:特別
99:その他';
COMMENT ON COLUMN public.training_categories.field_cd IS 'コード設計[30010]から専門分野の値を抽出

00:なし
01:福祉用具
02:認知症
03:手外科
04:特別支援
05:高次脳
06:精神急性
07:摂食嚥下
08:訪問
09:がん
10:就労支援
';
COMMENT ON COLUMN public.training_categories.group_cd IS '研修会カテゴリと専門分野の組み合わせによって値の意味が変わる。
コード設計[30010]や、参照。';
COMMENT ON COLUMN public.training_categories.organizer_cd IS '画面にはないし必要とは思えないが、東コロ様DB定義に存在しているので追加

コード設計[30030]
00:OT協会
01:北海道士会
...
47:沖縄県士会
99:共催';
COMMENT ON COLUMN public.training_categories.content IS '東コロ様DB定義にあるので残してあるが、この分類に紐づく要件、優先順位や講師などはサブテーブルに持つので実質不要';
COMMENT ON COLUMN public.training_category_priorities.priority_item_cd IS 'コード設計[30450]';
COMMENT ON COLUMN public.training_category_priorities.priority_point_cd IS 'コード設計[30460]
';
COMMENT ON COLUMN public.training_category_requirements.requirement_type_cd IS 'コード設計にはないが、
01:受講要件
02:免除要件
を想定している';
COMMENT ON COLUMN public.training_category_requirements.requirement_cd IS 'コード値不明。サイズも分からないので6桁と仮置きする

画面定義書に以下のような値が例として挙げられている
日本作業療法士協会正会員
都道府県作業療法士会正会員
生涯教育制度基礎研修修了者
認定作業療法士取得研修　選択研修　受講資格保有者
';
COMMENT ON COLUMN public.training_exemption_requirements.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_exemption_requirements.requirement_cd IS 'コード値不明。サイズも分からないので6桁と仮置きする

画面定義書に以下のような値が例として挙げられている
日本作業療法士協会正会員
都道府県作業療法士会正会員
生涯教育制度基礎研修修了者
認定作業療法士取得研修　選択研修　受講資格保有者
';
COMMENT ON COLUMN public.training_files.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_instructors.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_instructors.member_no IS '講師は会員以外ありえないのでmember_no';
COMMENT ON COLUMN public.training_instructors.instructor_nm IS 'スナップショットとしての名前が不要（あるいは無い方がいい）なら、このカラムを削除する';
COMMENT ON COLUMN public.training_instructors.instructor_type_cd IS 'コード設計の「30420」と対応
0:講師
1:講師助手
';
COMMENT ON COLUMN public.training_instructors.instructor_category_cd IS 'コード設計[30050]に該当

01:会員講師
02:外部講師
';
COMMENT ON COLUMN public.training_instructors.doc_to_superiors_cd IS 'コード設計の「30410」が該当

0:なし
1:あり
';
COMMENT ON COLUMN public.training_instructors.doc_to_superiors_print_flag IS '画面の公文書情報のチェックボックス。
チェックがtrueの場合、公文書印刷対象となる';
COMMENT ON COLUMN public.training_instructors.doc_to_person_cd IS 'コード設計の「30410」が該当

0:なし
1:あり
';
COMMENT ON COLUMN public.training_instructors.doc_to_person_print_flag IS '画面の公文書情報のチェックボックス。
チェックがtrueの場合、公文書印刷対象となる';
COMMENT ON COLUMN public.training_instructors.reward_description_category_cd IS '公文書に謝金について記載するか否か
コード設計[30430]に該当

0:なし
1:あり';
COMMENT ON COLUMN public.training_instructors.reward_category_cd IS 'コード設計[30100]に対応
01:A
02:B
03:C
04:D
';
COMMENT ON COLUMN public.training_instructors.instructor_achievement_flag IS '画面にない項目だが、東コロ様DB定義にあったので追加
「実績なし/実績あり」という値が備考に書かれている';
COMMENT ON COLUMN public.training_instructor_rewards.member_no IS '会員番号が設定される
';
COMMENT ON COLUMN public.training_instructor_rewards.reward_category_cd IS '研修会講師実績から参照できるはず。不要かもしれない。
コード設計[30100]に対応
01:A
02:B
03:C
04:D';
COMMENT ON COLUMN public.training_instructor_rewards.instructor_type_cd IS '研修会講師実績から取得できるはず。不要かもしれない。
コード設計の「30420」と対応
0:講師
1:講師助手
';
COMMENT ON COLUMN public.training_instructor_rewards.lecture_hour IS '時間が整数なのか小数なのか。30分とかの講義はどうする。';
COMMENT ON COLUMN public.training_instructor_rewards.exercise_hour IS '講義(時間)と同様に30分単位の扱いをどうするか';
COMMENT ON COLUMN public.training_instructor_rewards.assistant_hour IS '講義(時間)と同様に30分単位の扱いをどうするか';
COMMENT ON COLUMN public.training_instructor_rewards.travel_expenses_nissin IS '日新という旅行会社を使った費用と考え、ローマ字で命名する
サイト：https://www.nissin-trvl.co.jp/';
COMMENT ON COLUMN public.training_numbers.training_no IS '8桁の内訳(それぞれ２桁ずつ)
1-2桁目:カテゴリ
3-4桁目:専門分野
5-6桁目:グループ
7-8桁目:クラス';
COMMENT ON COLUMN public.training_operators.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_operators.member_no IS '運営担当者の会員番号を格納する';
COMMENT ON COLUMN public.training_operators.category_cd IS '画面の「担当区分」に該当
0:当日担当者権限
1:運営担当者権限';
COMMENT ON COLUMN public.training_operators.contact_category_cd IS '画面の「問合せ」と対応
コード設計の「30380」が該当すると思われる。

0:なし
1:問合せ担当者権限
';
COMMENT ON COLUMN public.training_operators.doc_to_superiors_cd IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する

コード設計の「30410」が該当

0:なし
1:あり
';
COMMENT ON COLUMN public.training_operators.doc_to_superiors_print_flag IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する

画面の公文書情報のチェックボックス。
チェックがtrueの場合、公文書印刷対象となる';
COMMENT ON COLUMN public.training_operators.doc_to_superiors_facility_nm IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.training_operators.doc_to_superiors_nm IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.training_operators.doc_to_superiors_send_date IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.training_operators.doc_to_person_cd IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する

コード設計の「30410」が該当

0:なし
1:あり
';
COMMENT ON COLUMN public.training_operators.doc_to_person_print_flag IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する

画面の公文書情報のチェックボックス。
チェックがtrueの場合、公文書印刷対象となる';
COMMENT ON COLUMN public.training_operators.doc_to_person_facility_nm IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.training_operators.doc_to_person_nm IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.training_operators.doc_to_person_send_date IS '公文書に関する情報は別システムで管理する。
仕様が確定次第、カラムは削除する
';
COMMENT ON COLUMN public.training_operators.secretariat_user_id IS '東コロ様DB定義の「T_研修会士会当日担当者」テーブルは基本的にわざわざテーブルを分ける必要がないと判断したが、
このカラムはもしかしたら必要かもしれないので一応入れておく。';
COMMENT ON COLUMN public.training_operator_rewards.travel_expenses_nissin IS '日新という旅行会社を使った費用と考え、ローマ字で命名する
サイト：https://www.nissin-trvl.co.jp/';
COMMENT ON COLUMN public.training_organizers.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_organizers.organizer_cd IS 'コード設計[30030]
00:OT協会
01:北海道士会
...
47:沖縄県士会
99:共催

※「99:共催」とあるが、共催とは主催者が複数いる状態のことで、主催者コードの全てを子テーブルに格納しておく必要がある。';
COMMENT ON COLUMN public.training_programs.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_programs.event_date IS '開催日1～3のどの日のプログラムかを保持';
COMMENT ON COLUMN public.training_programs.start_hour_cd IS 'コード設計[30350]
00:0時
01:1時
02:2時
03:3時
04:4時
05:5時
......
23:23時';
COMMENT ON COLUMN public.training_programs.start_minute_cd IS 'コード設計[30360]
00:0分
05:5分
10:10分
15:15分
20:20分
25:25分
30:30分
35:35分
40:40分
45:45分
50:50分
55:55分
';
COMMENT ON COLUMN public.training_programs.end_hour_cd IS 'コード設計[30350]
00:0時
01:1時
02:2時
03:3時
04:4時
05:5時
......
23:23時';
COMMENT ON COLUMN public.training_programs.end_minute_cd IS 'コード設計[30360]
00:0分
05:5分
10:10分
15:15分
20:20分
25:25分
30:30分
35:35分
40:40分
45:45分
50:50分
55:55分
';
COMMENT ON COLUMN public.training_programs.content IS '東コロ様DB定義のサイズに合わせた';
COMMENT ON COLUMN public.training_programs.lecture_style_cd IS 'コード設計[30440]
10:講義
20:演習
';
COMMENT ON COLUMN public.training_program_instructors.member_no IS '講師は会員以外ありえないのでmember_no';
COMMENT ON COLUMN public.training_reports.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_reports.target_cd IS 'コード設計[30220]
00:正会員のみ
10:一般公開（学生除く）
11:一般公開（学生含む）';
COMMENT ON COLUMN public.training_reports.participants_evaluation_cd IS 'コード設計[30530]
1:満足
2:やや満足
3:普通
4:やや不満
5:不満
';
COMMENT ON COLUMN public.training_reports.applicant_count IS '画面の申込者数が編集可能なので報告書のフィールドに保持する';
COMMENT ON COLUMN public.training_reports.effect_judge1_cd IS 'コード設計[30540]
1:優良（100%～80%）
2:可（79%～50%）
3:要改善・再検討（49%～）
';
COMMENT ON COLUMN public.training_reports.effect_judge2_cd IS 'コード設計[30540]
1:優良（100%～80%）
2:可（79%～50%）
3:要改善・再検討（49%～）
';
COMMENT ON COLUMN public.training_reports.cooperation_evaluation_cd IS 'コード設計[30540]
1:優良（100%～80%）
2:可（79%～50%）
3:要改善・再検討（49%～）
';
COMMENT ON COLUMN public.training_reports.workplace_fax_send_count IS '画面や東コロ様DB定義のカラム名をそのまま採用。送信件数のことか。';
COMMENT ON COLUMN public.training_results.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_results.salary_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.salary_overview IS '東コロ様DB定義のサイズに合わせた';
COMMENT ON COLUMN public.training_results.welfare_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.meeting_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.travel_expenses_other_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.travel_expenses_nissin_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.consumables_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.print_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.rent_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.utility_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.external_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.commission_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.reward_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.withholding_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_results.total_account_cd IS 'コード設計[30550]
01:給与手当て
02:福利厚生費
03:会議費
04:旅費交通費（日新以外）
05:旅費交通費（日新分）
06:消耗品費
07:印刷製本費
08:賃借料
09:光熱費
10:渉外費
11:通信運搬費
12:委託費
13:諸謝金';
COMMENT ON COLUMN public.training_school_name_histories.facility_base_no IS '初期登録時の施設番号';
COMMENT ON COLUMN public.training_school_name_histories.facility_seq_no IS '都道府県コード 、施設区分ごとの連番';
COMMENT ON COLUMN public.training_school_name_histories.facility_no IS '都道府県コード+施設区分+施設連番';
COMMENT ON COLUMN public.training_school_open_histories.facility_base_no IS '初期登録時の施設番号';
COMMENT ON COLUMN public.training_school_relations.facility_base_no IS '初期登録時の施設番号';
COMMENT ON COLUMN public.training_school_relations.seq_no IS '関連施設を登録するたびにカウントアップするフィールド
1番目に登録した関連施設なら1が入り、
2番目に追加で登録した関連施設なら2が入るのでは。';
COMMENT ON COLUMN public.training_school_relations.relation_no IS '都道府県コード+施設区分+施設連番';
COMMENT ON COLUMN public.training_school_relations.relation_reason_cd IS '既存システムでは1桁だが、既に1...7の値が使われているので、桁を増やしておく';
COMMENT ON COLUMN public.training_send_mail_detail_histories.destination_type_cd IS '※一般申込者、外部講師などはシステムで扱わないという話なので、ひょっとしたらこのフィールドは不要になるかも
コード設計[30510]
01:会員
02:一般申込者
03:外部講師
';
COMMENT ON COLUMN public.training_send_mail_detail_histories.mail_address_presense_cd IS 'コード設計[30500]
0:なし
1:あり';
COMMENT ON COLUMN public.training_send_mail_detail_histories.member_no IS '会員番号が設定される。
';
COMMENT ON COLUMN public.training_send_mail_detail_histories.send_status IS 'コード値不明
他システムから連携されたステータスを格納する予定';
COMMENT ON COLUMN public.training_send_mail_histories.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_send_mail_histories.template_cd IS 'コード設計[30480]
01:書類不備通知
02:受講許可
03:受講不許可
04:請求書送付
05:督促
06:未入金取消
06:受講許可
07:受講不許可
07:未入金取消
07:督促
07:受講決定
10:テンプレートなし
';
COMMENT ON COLUMN public.training_send_mail_histories.training_mail_type_cd IS 'コード設計[30490]
01:運営依頼
02:講師依頼
03:講師助手依頼
04:本申込確認
05:仮申込取消
06:仮申込削除
07:書類不備
08:本申込完了
09:本申込取消
10:受講許可
11:受講不許可
12:未入金取消
13:督促
14:受講決定
15:辞退受付
15:辞退完了
16:返金申請書
16:返金取下
17:返金完了
18:振替取下
19:振替完了
20:繰上打診
21:繰上承認
22:繰上否認
23:開催日案内
24:合格通知
25:不合格通知
26:修了通知
27:欠席通知
';
COMMENT ON COLUMN public.training_temp_applications.training_no IS '1.5次では6桁で定義されている。
東コロ様のDB定義のサイズを採用した。
画面定義の「講座番号」がこれに該当すると思われる
→
2次では16桁になる。ユニークになるのでKeyとして使える';
COMMENT ON COLUMN public.training_temp_applications.target_disease IS '画面の「疾患・障害名」。
項目名「対象疾患」は東コロ様DB定義を使った。
そちらの方がより正確に表現されていると思ったため。';
COMMENT ON COLUMN public.training_temp_applications.case_provide_cd IS 'コード設計[30140]
01:可能
02:場合による
03:できない
';
COMMENT ON COLUMN public.training_temp_applications.application_deadline_datetime IS '東コロ様DB定義に「登録日時の24時間後」と定義されている';
COMMENT ON COLUMN public.transfer_paper_output_histories.seq_no IS '請求連番10桁 + 会員番号6桁';
COMMENT ON COLUMN public.transfer_paper_output_histories.qualification IS '東コロ様のDB定義にロジックが書かれているので要注意。
”発送区分が自宅の場合は空白、施設の場合は「作業療法
士」を設定”';
COMMENT ON COLUMN public.transfer_paper_output_histories.member_no_text IS '6桁未満の場合先頭を0埋めして6桁にする';
COMMENT ON COLUMN public.transfer_paper_output_histories.last_name IS '※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく';
COMMENT ON COLUMN public.transfer_paper_output_histories.first_name IS '※業者向け振込用紙データの連携資料で姓20 名20という指定があるので念のためカラムを分けておく';
COMMENT ON COLUMN public.transfer_paper_output_histories.zipcode IS '東コロ様DB定義のサイズではなく、会員の郵便番号に合わせる(ハイフン込み)';
COMMENT ON COLUMN public.transfer_paper_output_histories.facility_nm2 IS '施設養成校テーブルでのサイズが128になっているが、連携先や振込用紙の都合に合わせて32にする';
COMMENT ON COLUMN public.transfer_paper_output_histories.option1 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.transfer_paper_output_histories.option2 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.transfer_paper_output_histories.option3 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.transfer_paper_output_histories.option4 IS '請求年度2～5は不要になったので代わりにオプション1～4フィールドを追加することになった。';
COMMENT ON COLUMN public.transfer_paper_print_histories.billing_no IS '10桁の連番 + 6桁の会員番号';
COMMENT ON COLUMN public.transfer_paper_print_histories.description IS '会員番号＆会員氏名＆住所＆会費年度など';
COMMENT ON TABLE public.users IS '事務局のログインユーザーを管理するテーブル。';
COMMENT ON COLUMN public.users.login_id IS 'ユーザーID';
COMMENT ON TABLE public.user_revoked_authorities IS '権限コード値に対応するパス、操作のキー値をブラックリスト形式で保持する。';
COMMENT ON COLUMN public.wfot_applications.application_type IS '新規
継続';
COMMENT ON COLUMN public.wfot_applications.application_status_cd IS '601:委員会確認中
602:理事会承認待ち
603:合格
604:不合格
';
COMMENT ON COLUMN public.wfot_applications.application_result_cd IS '601:申請中
602:承認
603:未認定
604:要件不足
605:取下げ
';
COMMENT ON COLUMN public.wfot_applications.guide_doc_no IS '画面設計書によると最終番号+1とのこと。
SEQオブジェクトを使うか。';
COMMENT ON COLUMN public.wfot_applications.certification_doc_no IS '最終番号+1';
COMMENT ON COLUMN public.wfot_applications.uncertification_doc_no IS '最終番号＋1';
COMMENT ON COLUMN public.wfot_certified_training_schools.acceptance_cd IS '0:（未入力）
1:合格
2:不合格
';
COMMENT ON COLUMN public.wfot_info.facility_base_no IS '初期登録時の施設番号';
COMMENT ON COLUMN public.wfot_info.school_nm IS '英文名称';
COMMENT ON COLUMN public.wfot_info.zipcode IS '郵便番号';
COMMENT ON COLUMN public.wfot_info.address1 IS '住所１';
COMMENT ON COLUMN public.wfot_info.address2 IS '住所２';
COMMENT ON COLUMN public.wfot_info.address3 IS '住所３';
COMMENT ON COLUMN public.wfot_info.prefecture_nm IS '県名';
COMMENT ON COLUMN public.wfot_info.website_address IS 'WEBサイトURL';
COMMENT ON COLUMN public.wfot_info.qualification_awarded IS '認定資格種別';
COMMENT ON COLUMN public.wfot_info.year_course_commenced IS '設立年度';
COMMENT ON COLUMN public.wfot_info.year_course_accredited_by_NA IS '国認可年度';
COMMENT ON COLUMN public.wfot_info.year_course_last_reviewed_by_NA IS '最終審査年';
COMMENT ON COLUMN public.wfot_info.year_course_first_last_approved_by_wfot IS '最終認可年';
COMMENT ON COLUMN public.wfot_info.year_course_next_review_monitoring IS '次回審査年';
COMMENT ON COLUMN public.member_entry_requests.foreign_license IS 'TRUE：外免';
COMMENT ON COLUMN public.member_entry_requests.status_cd IS 'コード設計には存在しない
現行1.5次のDBにある値は以下のもの
0:登録
8:処理済
9:削除';
COMMENT ON COLUMN public.member_entry_requests.remarks IS '再入会情報など';



