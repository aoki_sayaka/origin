DROP VIEW IF EXISTS point_applications;

CREATE VIEW  point_applications
AS
 		SELECT
		       a.application_date
		      ,a.member_no::int                                                            -- a.member_no(bigint)
		      ,NULL::varchar(20) as facility_no
		      ,a.attending_date_from
		      ,a.attending_date_to
		      ,a.organizer_cd
		      ,a.other_organization_sig_cd
		      ,a.thema
		      ,a.attending_days
		      ,a.point_category_cd
		      ,a.point
		      ,NULL::varchar(2) as point_application_type_cd
		      ,a.status_cd::varchar(2)
		FROM  other_org_point_applications a
	    WHERE a.temp_flag <> '1' AND
	          a.deleted = false

	UNION ALL
		SELECT 
		       b.application_date
		      ,b.application_member_no AS member_no
		      ,b.facility_no
		      ,NULL::date
		      ,NULL::date
		      ,NULL::varchar(2)
		      ,NULL::bigint
		      ,NULL::varchar(100)
		      ,NULL::int
		      ,NULL::varchar(2)
 		      ,NULL::int
 		      ,NULL::varchar(2) as point_application_type_cd
		      ,b.status_cd
	  	FROM  clinical_training_point_applications b
	  	WHERE b.deleted = false

	UNION ALL
		SELECT
		       c.application_date
		      ,c.member_no::int                                                            -- c.member_no(bigint)
		      ,NULL::varchar(20)
		      ,NULL::date
		      ,NULL::date
		      ,NULL::varchar(2)
		      ,NULL::bigint
		      ,NULL::varchar(100)
		      ,NULL::int
		      ,NULL::varchar(2)
		      ,c.basic_training_point
		      ,NULL::varchar(2) as point_application_type_cd
		      ,c.status_cd::varchar(2)
		FROM  handbook_migration_applications c
		WHERE c.deleted = false
;

comment on view point_applications is 'ポイント申請ビュー';
comment on column point_applications.application_date is '申請日';
comment on column point_applications.member_no is '申請者 会員番号';
comment on column point_applications.facility_no is '養成校番号';
comment on column point_applications.attending_date_from is '受講日開始';
comment on column point_applications.attending_date_to is '受講日終了';
comment on column point_applications.organizer_cd is '主催者コード';
comment on column point_applications.other_organization_sig_cd is '他団体・SIGコード';
comment on column point_applications.thema is '受講テーマ';
comment on column point_applications.attending_days is '日数';
comment on column point_applications.point_category_cd is 'ポイント種別';
comment on column point_applications.point is 'ポイント';
comment on column point_applications.point_application_type_cd is 'ポイント申請種別';
comment on column point_applications.status_cd is '申請状態';
