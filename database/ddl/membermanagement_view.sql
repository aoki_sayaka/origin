DROP VIEW IF EXISTS facility_applications;                    -- 施設登録申請
DROP VIEW IF EXISTS facility_application_categories;          -- 施設登録申請分類
DROP VIEW IF EXISTS region_related_large_classifications;
DROP VIEW IF EXISTS region_related_middle_classifications;
DROP VIEW IF EXISTS region_related_small_classifications;


-- 施設登録申請
CREATE VIEW facility_applications AS
SELECT * FROM dblink(
	'dbname=' || :'DB_NAME' ||
	' user=' || :'USER_NAME' ||
	' password=' || :'DB_PASSWORD',
	'select * FROM facility_applications')
AS FA(
	id bigint,
	facility_category varchar(1),
	member_category_cd char(2),
	member_no int,
	temporary_member_no int,
	status smallint,
	opener_type_cd char(4),
	opener_nm varchar(128),
	opener_nm_kana varchar(255),
	responsible_email_address varchar(128),
	facility_nm varchar(128),
	facility_nm2 varchar(256),
	facility_nm_kana varchar(128),
	facility_nm_kana2 varchar(256),
	zipcode varchar(20),
	facility_prefecture_cd char(2),
	address1 varchar(128),
	address2 varchar(128),
	address3 varchar(128),
	primary_phone_number varchar(36),
	fax_number varchar(36),
	corporate_nm varchar(128),
	corporate_nm_kana varchar(128),
	department varchar(128),
	training_school_category_cd varchar(2),
	create_datetime timestamp,
	create_user varchar(50),
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean,
	version_no int
);

-- 施設登録申請分類
CREATE VIEW facility_application_categories AS
SELECT * FROM dblink(
	'dbname=' || :'DB_NAME' ||
	' user=' || :'USER_NAME' ||
	' password=' || :'DB_PASSWORD',
	'select * FROM facility_application_categories')
AS (
	id bitint,
	applicant_id bigint,
	category smallint,
	category_division smallint,
	parent_code varchar(32),
	category_code varchar(32),
	other varchar(100),
	create_datetime timestamp,
	create_user varchar(50),
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean,
	version_no int
);

-- 領域関連大分類
CREATE VIEW region_related_large_classifications AS
SELECT * FROM dblink(
	'dbname=' || :'DB_NAME' ||
	' user=' || :'USER_NAME' ||
	' password=' || :'DB_PASSWORD',
	'select * FROM region_related_large_classifications')
AS (
	id bigint,
	code_id VARCHAR(32),
	code VARCHAR(32),
	content VARCHAR(128),
	simple_content VARCHAR(128),
	sort_order INT,
	create_datetime TIMESTAMP,
	create_user VARCHAR(50),
	update_datetime TIMESTAMP,
	update_user VARCHAR(50),
	deleted boolean,
	version_no INT
);

-- 領域関連中分類
CREATE VIEW region_related_middle_classifications AS
SELECT * FROM dblink(
	'dbname=' || :'DB_NAME' ||
	' user=' || :'USER_NAME' ||
	' password=' || :'DB_PASSWORD',
	'select * FROM region_related_middle_classifications')
AS (
	id bigint,
	code_id varchar(32),
	code varchar(32),
	content varchar(128),
	simple_content varchar(128),
	parent_code varchar(32),
	sort_order int,
	create_datetime timestamp,
	create_user varchar(50),
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean,
	version_no int
);


-- 領域関連小分類
CREATE VIEW region_related_small_classifications AS
SELECT * FROM dblink(
	'dbname=' || :'DB_NAME' ||
	' user=' || :'USER_NAME' ||
	' password=' || :'DB_PASSWORD',
	'select * FROM region_related_small_classifications')
AS (
	id bigint,
	code_id varchar(32),
	code varchar(32),
	content varchar(128),
	simple_content varchar(128),
	parent_code varchar(32),
	sort_order int,
	create_datetime timestamp,
	create_user varchar(50),
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean,
	version_no int
);
