
/* Drop Tables */

DROP TABLE IF EXISTS public.member_app_facility_categories;  -- 入会申込勤務先業務分類

-- 入会申込勤務先業務分類
CREATE TABLE public.member_app_facility_categories
(
	id bigserial NOT NULL,
	temporary_member_no int NOT NULL,
	-- 1:大分類
	-- 2:中分類（主）
	-- 3:小分類
	-- 4:中分類（従）
	category_division smallint,
	-- 1:主
	-- 2:従
	primary_division smallint,
	parent_code varchar(32),
	category_code varchar(32),
	create_datetime timestamp NOT NULL,
	create_user varchar(50) NOT NULL,
	update_datetime timestamp,
	update_user varchar(50),
	deleted boolean DEFAULT 'FALSE' NOT NULL,
	version_no int DEFAULT 1 NOT NULL,
	PRIMARY KEY (id)
) WITHOUT OIDS;


COMMENT ON COLUMN public.member_app_facility_categories.category_division IS '1:大分類
2:中分類（主）
3:小分類
4:中分類（従）';
COMMENT ON COLUMN public.member_app_facility_categories.primary_division IS '1:主
2:従';
