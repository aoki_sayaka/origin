package jp.or.jaot.core.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * メール送信機能のDTO
 */
@Getter
@Setter
public final class MailContentDto {

	/** 送信先(To)のメールアドレス(複数指定可能) */
	private List<String> toAddressList;

	/** 送信先(Cc)のメールアドレス(複数指定可能) */
	private List<String> ccAddressList;

	/*** 送信先(Bcc)のメールアドレス(複数指定可能)	 */
	private List<String> bccAddressList;

	/** 送信者(From)のメールアドレス */
	private String fromAddress;

	/** メールのタイトル */
	private String subject;

	/** メールの本文 */
	private String body;
}
