package jp.or.jaot.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * メールの添付ファイルの情報を保持するDTO
 */
@Getter
@Setter
public final class MailAttachFileDto {

	/** ファイル名 */
	private String fileName;

	/** ファイルの配置先のパス */
	private String filePath;

	/** 実ファイル名 */
	private String physicalFileName;
}
