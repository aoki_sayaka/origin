package jp.or.jaot.core.exception.system;

import jp.or.jaot.core.exception.common.SystemException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_KIND;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_LEVEL;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import lombok.Getter;

/**
 * Hibernateシステム例外のクラス
 */
@Getter
public class HibernateSystemException extends SystemException {

	/**
	 * コンストラクタ
	 * @param original オリジナルの例外
	 */
	public HibernateSystemException(Throwable original) {
		super(ERROR_LEVEL.ERROR, ERROR_KIND.SYSTEM, ERROR_CAUSE.HIBERNATE, ERROR_PLACE.DATASOURCE, null,
				original.getMessage(), original);
	}

	/**
	 * コンストラクタ
	 * @param errorCause エラー原因(ERROR_CAUSE.HIBERNATE_*)
	 * @param message 例外メッセージ
	 * @param original オリジナルの例外
	 */
	public HibernateSystemException(ERROR_CAUSE errorCause, String message, Throwable original) {
		super(ERROR_LEVEL.ERROR, ERROR_KIND.SYSTEM, errorCause, ERROR_PLACE.DATASOURCE, null, message, original);
	}

}
