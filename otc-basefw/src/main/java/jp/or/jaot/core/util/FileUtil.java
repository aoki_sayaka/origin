package jp.or.jaot.core.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import jp.or.jaot.core.exception.system.FileSystemException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;

/**
 * ファイル関連ユーティリティ
 */
public class FileUtil {

	/** ロガーインスタンス */
	private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

	/** ファイルセパレータ */
	private static final String SEPARATOR = File.separator;

	/** ZIPファイル拡張子 */
	private static final String ZIP_EXTENSION = ".zip";

	/**
	 * アップロードファイルを格納するディレクトリを作成する
	 * @param filePath ファイルパス
	 * @param mainPath メインパス
	 * @param subPath サブパス
	 * @return ファイルオブジェクト
	 */
	public static File makeDirectory(StringBuffer filePath, String mainPath, String subPath) {

		String child = "";

		// サブディレクトリのパスを生成
		if (StringUtils.isNotBlank(mainPath)) {
			child += appendSeparator(mainPath);
		}
		if (StringUtils.isNotBlank(subPath)) {
			child += appendSeparator(subPath);
		}

		// ファイルオブジェクト生成
		File uploadDir = new File(appendSeparator(filePath).toString(), child);
		logger.debug("mainFilePath=" + appendSeparator(filePath).toString() + ", child=" + child);
		if (uploadDir.exists()) {
			logger.debug("uploadDir exists.");
			return uploadDir;
		}

		// フォルダ作成
		logger.debug("uploadDir not exists so mkdirs.");
		boolean mkdirsResult = uploadDir.mkdirs();
		if (!mkdirsResult) {
			logger.error("Failed to mkdirs.path=" + uploadDir.getAbsolutePath());
		} else {
			logger.debug("Dir maked.path=" + uploadDir.getAbsolutePath());
		}

		return uploadDir;
	}

	/**
	 * ファイルパスの末尾にセパレーターを付ける
	 * @param path ファイルパス
	 * @return ファイルパス
	 */
	public static StringBuffer appendSeparator(StringBuffer path) {
		StringBuffer retStr = path;
		if (!path.substring(path.length() - 1, path.length()).equals(SEPARATOR)) {
			retStr.append(SEPARATOR);
		}

		return retStr;
	}

	/**
	 * ファイルパスの末尾にセパレーターを付ける
	 * @param path ファイルパス
	 * @return ファイルパス
	 */
	public static StringBuffer appendSeparator(String path) {
		StringBuffer retStr = new StringBuffer(path);
		return appendSeparator(retStr);
	}

	/**
	 * サーバーへファイルをアップロード
	 * @param path ファイルパス
	 * @param fileName ファイル名
	 * @param multipartFile アップロードするファイル
	 */
	public static void uploadFile(String path, String fileName, MultipartFile multipartFile) {

		try {
			// ファイルの内容を取得
			byte[] bytes = multipartFile.getBytes();
			String outputFile = FilenameUtils.concat(path, fileName);
			logger.info(String.format("uploadFile=%s", outputFile));

			// 指定されたアップロード先にファイルを作成
			try (BufferedOutputStream uploadFileStream = new BufferedOutputStream(new FileOutputStream(outputFile))) {
				uploadFileStream.write(bytes);
			}
		} catch (Exception e) {
			String detail = String.format("path=%s, fileName=%s, multipartFile(null)=%b", path, fileName,
					(multipartFile == null));
			String message = String.format("ファイルのアップロードに失敗しました : %s", detail);
			throw new FileSystemException(ERROR_CAUSE.FILE_WRITE, detail, message, e);
		}
	}

	/**
	 * 指定したファイルパスにて、文字列をテキストファイルに出力する。
	 * @param path ファイルパス
	 * @param fileName ファイル名
	 * @param text 文字列
	 */
	public static void writeTextFile(String path, String fileName, String text) {
		try {
			String outputFile = FilenameUtils.concat(path, fileName);
			logger.info(String.format("writeTextFile=%s", outputFile));
			try (FileWriter fw = new FileWriter(outputFile)) {
				fw.write(text);
			}
		} catch (Exception e) {
			String detail = String.format("path=%s, fileName=%s, text=%s", path, fileName, text);
			String message = String.format("テキストファイルの出力に失敗しました : %s", detail);
			throw new FileSystemException(ERROR_CAUSE.FILE_WRITE, detail, message, e);
		}
	}

	/**
	 * アップロードファイルを格納するディレクトリを削除する
	 * @param path ファイルパス
	 * @param fileName ファイル名
	 * @return 実行結果
	 */
	public static boolean deleteFile(String path, String fileName) {

		String deleteFileName = FilenameUtils.concat(path, fileName);
		File deleteFile = new File(deleteFileName);
		// 既に存在する場合は生成しない
		if (!deleteFile.exists()) {
			return false;
		}

		return deleteFile.delete();
	}

	/**
	 * ディレクトリ削除
	 * @param path ファイルパス
	 * @return 実行結果
	 */
	public static boolean deleteDir(String path) {
		File deleteDir = new File(path);
		// 既に存在する場合は生成しない
		if (!deleteDir.exists()) {
			return false;
		}

		// ファイルの場合は、削除
		if (deleteDir.isFile()) {
			return deleteDir.delete();
		}

		// ディレクトリの場合は、配下のファイルを削除
		if (deleteDir.isDirectory()) {
			File[] files = deleteDir.listFiles();
			if (ArrayUtils.isNotEmpty(files)) {
				for (int i = 0; i < files.length; i++) {
					if (files[i].exists() == false) {
						continue;
					}

					if (!files[i].delete()) {
						return false;
					}
				}
			}

			// ディレクトリも削除
			return deleteDir.delete();
		}

		return true;

	}

	/**
	 * ZIP圧縮ファイルの作成<br>
	 * @param zipFileName ZIP圧縮ファイル名
	 * @param fromFilePath ZIP圧縮元ファイルパス
	 * @param toFilePath ZIP圧縮先ファイルパス
	 * @param subPathList ZIP圧縮元ファイルパス(サブ)
	 * @return String ZIPファイル名
	 * @throws IOException
	 */
	public static String makeZip(String zipFileName, String fromFilePath, String toFilePath, List<String> subPathList)
			throws IOException {

		String zipFullpathname = appendSeparator(toFilePath).toString() + zipFileName + ZIP_EXTENSION;
		try (ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFullpathname)),
				Charset.forName("Shift-JIS"))) {

			for (String subPath : subPathList) {

				File dir = null;
				if (fromFilePath.length() > 0) {
					// 指定フォルダがある場合
					dir = new File(fromFilePath, subPath);
				} else {
					// ない場合
					dir = new File(subPath);
				}
				if (!dir.exists()) {
					continue;
				}
				File[] files = dir.listFiles();

				createZip(zos, subPath, files);

			}
			zos.closeEntry();
		} catch (Exception e) {
			String detail = String.format("zipFileName=%s", zipFileName);
			String message = String.format("Zipファイルの作成に失敗しました : %s", detail);
			throw new FileSystemException(detail, message, e);
		}

		return zipFullpathname;
	}

	/**
	 * １ディレクトリをZIP圧縮に追加
	 * @param zos
	 * @param path
	 * @param files
	 * @throws IOException
	 */
	private static void createZip(ZipOutputStream zos, String path, File[] files) throws IOException {
		byte[] buf = new byte[1024];
		try {
			for (File file : files) {
				if (!file.getName().endsWith(".zip")) {
					String filename = appendSeparator(path) + file.getName();
					ZipEntry entry = new ZipEntry(filename);
					zos.putNextEntry(entry);
					try (InputStream is = new BufferedInputStream(new FileInputStream(file))) {
						int len = 0;
						while ((len = is.read(buf)) != -1) {
							zos.write(buf, 0, len);
						}
					}
				}
			}
		} catch (Exception e) {
			String detail = String.format("path=%s", path);
			String message = String.format("Zipファイルの追加に失敗しました : %s", detail);
			throw new FileSystemException(detail, message, e);
		}
	}

	/**
	 * リソースファイル取得
	 * @param clazz クラス
	 * @param path ファイルパス(/src/main/resources以下の相対パス。先頭は必ず'/'から始まること)
	 * @return リソースファイル
	 */
	public static File getResourceFile(Class<?> clazz, String path) {
		try {
			File file = new File(FilenameUtils.getName(path));
			IOUtils.copy(clazz.getResourceAsStream(path), new FileOutputStream(file));
			return file;
		} catch (Exception e) {
			String detail = String.format("clazz=%s, name=%s", clazz, path);
			String message = String.format("リソースファイルの取得に失敗しました : %s", detail);
			throw new FileSystemException(ERROR_CAUSE.FILE_OPEN, detail, message, e);
		}
	}

	/**
	 * リソースファイル取得
	 * @param clazz クラス
	 * @param path ファイルパス(/src/main/resources以下の相対パス。先頭は必ず'/'から始まること)
	 * @return バイト入力ストリーム
	 */
	public static ByteArrayInputStream getResourceFileAsInputStream(Class<?> clazz, String path) {
		return new ByteArrayInputStream(getResourceFileAsOutputStream(clazz, path).toByteArray());
	}

	/**
	 * リソースファイル取得
	 * @param clazz クラス
	 * @param path ファイルパス(/src/main/resources以下の相対パス。先頭は必ず'/'から始まること)
	 * @return バイト出力ストリーム
	 */
	public static ByteArrayOutputStream getResourceFileAsOutputStream(Class<?> clazz, String path) {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			IOUtils.copy(clazz.getResourceAsStream(path), baos);
			return baos;
		} catch (Exception e) {
			String detail = String.format("clazz=%s, name=%s", clazz, path);
			String message = String.format("リソースファイルの取得に失敗しました : %s", detail);
			throw new FileSystemException(ERROR_CAUSE.FILE_OPEN, detail, message, e);
		}
	}
}
