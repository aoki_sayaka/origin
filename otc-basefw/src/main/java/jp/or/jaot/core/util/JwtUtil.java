package jp.or.jaot.core.util;

import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

/**
 * JWTユーティリティクラス
 */
public class JwtUtil {

	/** ロガーインスタンス */
	private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);

	/** 有効期限(msec) */
	public static final long EXPIRATION_TIME = 4 * 60 * 60 * 1000; // 4 * 60 Minutes(msec)

	/** 秘密鍵(128ビットユニーク値:システム起動毎に変更) */
	public static final String SECRET_KEY = UUID.randomUUID().toString();

	/** トークンプレフィックス */
	public static final String TOKEN_PREFIX = "Bearer ";

	/** HTTP追加ヘッダ */
	public static final String HEADER_STRING = "Authorization";

	/** アクセス公開ヘッダ */
	public static final String EXPOSE_HEADERS = "Access-Control-Expose-Headers";

	/** ログインID */
	public static final String LOGIN_ID = "username"; // defalut:username

	/** パスワード */
	public static final String PASSWORD = "password"; // default:password

	/**
	 * トークン生成<br>
	 * (トークンに含める内容は、セキュリティ等を考慮して最低限の内容に留めること)
	 * @param user 利用者詳細情報
	 * @return トークン
	 */
	public static String createToken(UserDetails user) {
		String token = null;
		if (user != null) {
			Date exp = new Date(System.currentTimeMillis() + EXPIRATION_TIME);
			token = TOKEN_PREFIX + Jwts.builder()
					.setSubject(user.getUsername())
					.setExpiration(exp)
					.signWith(SignatureAlgorithm.HS512, SECRET_KEY.getBytes())
					.compact();
		}
		return token;
	}

	/**
	 * 認証情報取得
	 * @param token トークン
	 * @return 認証情報文字列
	 */
	public static String getPrincipal(String token) {
		String principal = null;
		try {
			principal = Jwts.parser()
					.setSigningKey(SECRET_KEY.getBytes())
					.parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
					.getBody()
					.getSubject();
		} catch (SignatureException e) {// 署名アンマッチ例外
			logger.warn("トークン例外(署名アンマッチ) : {}", e.getMessage());
		} catch (ExpiredJwtException e) { // 期限切れ例外
			logger.warn("トークン例外(有効期限切れ) : {}", e.getMessage());
		} catch (MalformedJwtException e) { // フォーマット例外
			logger.warn("トークン例外(フォーマット不正) : {}", e.getMessage());
		}

		return principal;
	}

	/**
	 * 有効トークン判定
	 * @param token トークン
	 * @return true=有効, false=無効
	 */
	public static boolean isValidToken(String token) {
		if (StringUtils.isNotEmpty(token) && StringUtils.isNotEmpty(getPrincipal(token))) {
			return true;
		} else {
			return false;
		}
	}
}
