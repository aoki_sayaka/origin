package jp.or.jaot.core.util;

import java.lang.reflect.Field;
import java.util.Arrays;

import jp.or.jaot.core.model.LoggingElementDto;
import jp.or.jaot.core.model.LoggingUpdateEntityDto;

/**
 * ロギングユーティリティクラス
 */
public class LoggingUtil {

	/** ID */
	private static final String DEF_ID = "id";

	/** 更新バージョン */
	private static final String DEF_VERSION_NO = "versionNo";

	/** 除外対象プロパティ */
	public static final String[] DEF_IGNORES = { "createDatetime", "createUser", "updateDatetime", "updateUser",
			"deleted", "updateIgnores" };

	/**
	 * 更新エンティティのログ情報DTO取得<br>
	 * 比較対象となるオブジェクトの属性同士を比較して、ログ情報DTOを返します。
	 * @param before 比較対象オブジェクト(変更前)
	 * @param after 比較対象オブジェクト(変更後)
	 * @return 更新エンティティログ情報DTO
	 */
	public static LoggingUpdateEntityDto getLoggingUpdateEntityDto(Object before, Object after) {

		// チェック
		if (before == null && after == null) {
			return new LoggingUpdateEntityDto(null); // 空DTO
		}

		// 比較処理
		Class<?> clazz = ((before != null) ? before.getClass() : after.getClass());
		String className = clazz.getName();
		String fieldName = null;
		String fieldType = null;
		LoggingUpdateEntityDto dto = new LoggingUpdateEntityDto(className);
		try {
			for (Field field : ReflectionUtil.getInheritedPrivateFields(clazz)) {

				// 取得チェック
				if (field == null) {
					continue;
				}

				// フィールド型を取得
				fieldType = field.getType().getSimpleName();

				// フィールド名を取得
				fieldName = field.getName();

				// 値を取得
				field.setAccessible(true);
				String beforeValue = (before != null) ? LoggingUtil.createValueString(field.get(before)) : null;
				String afterValue = (after != null) ? LoggingUtil.createValueString(field.get(after)) : null;
				String value = (before != null) ? beforeValue : afterValue;

				// ログ情報に追加
				if (DEF_ID.equals(fieldName)) {
					dto.setId(value); // ID
				} else if (DEF_VERSION_NO.equals(fieldName)) {
					dto.setVersionNo(value); // 更新バージョン
				} else if (!Arrays.asList(DEF_IGNORES).contains(fieldName)) {
					if (before == null) {
						dto.getLoggingElements().add(new LoggingElementDto(fieldType, fieldName, null, afterValue)); // 追加
					} else if (after == null) {
						dto.getLoggingElements().add(new LoggingElementDto(fieldType, fieldName, beforeValue, null)); // 削除
					} else if (!beforeValue.equals(afterValue)) {
						dto.getLoggingElements()
								.add(new LoggingElementDto(fieldType, fieldName, beforeValue, afterValue)); // 変更(差分のみ)
					}
				}
			}
		} catch (Exception e) {
			return new LoggingUpdateEntityDto(className); // 空DTO
		}

		return dto;
	}

	/**
	 * 名称変換(エンティティ -> テーブル(物理))
	 * @param name エンティティ名
	 * @return テーブル名(物理)
	 */
	public static String cnvNameEntityToTable(String name) {
		return InflectorUtil.pluralize(StringUtil.camelToSnake(name.replaceAll("Entity", "")));

	}

	/**
	 * 属性の内容を表現した文字列を返す
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static String createValueString(Object value) {
		if (value == null) {
			return null;
		} else if (value instanceof Iterable) {
			return String.join(",", (Iterable) value);
		} else {
			return value.toString();
		}
	}
}
