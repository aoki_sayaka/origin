package jp.or.jaot.core.exception.system;

import jp.or.jaot.core.exception.common.SystemException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_KIND;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_LEVEL;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import lombok.Getter;

/**
 * ファイルシステム例外のクラス
 */
@Getter
public class FileSystemException extends SystemException {

	/**
	 * コンストラクタ
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 * @param original オリジナルの例外
	 */
	public FileSystemException(String detail, String message, Throwable original) {
		super(ERROR_LEVEL.ERROR, ERROR_KIND.SYSTEM, ERROR_CAUSE.FILE, ERROR_PLACE.COMMON, detail, message, original);
	}

	/**
	 * コンストラクタ
	 * @param errorCause エラー原因(ERROR_CAUSE.FILE_*)
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 * @param original オリジナルの例外
	 */
	public FileSystemException(ERROR_CAUSE errorCause, String detail, String message, Throwable original) {
		super(ERROR_LEVEL.ERROR, ERROR_KIND.SYSTEM, errorCause, ERROR_PLACE.COMMON, detail, message, original);
	}
}
