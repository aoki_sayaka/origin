package jp.or.jaot.core.handler.common;

import java.lang.annotation.Annotation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import jp.or.jaot.core.util.ExceptionHelper;
import jp.or.jaot.model.dto.error.ErrorInfoDto;

/**
 * グローバル例外のハンドラクラス
 */
@Component
@Order(-1) // 最優先(全例外を処理対象)
public class GlobalExceptionHandler implements HandlerExceptionResolver {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		ErrorInfoDto errorInfo = ExceptionHelper.createErrorInfo(ex);
		BaseExceptionHandler exceptionHandler = ExceptionHelper.getHandler(errorInfo);
		boolean isContentTypeJson = isContentTypeJson(handler);

		return exceptionHandler.handle(isContentTypeJson);
	}

	/**
	 * JSONコンテンツタイプ判定
	 * @param handler ハンドラ
	 * @return Postされた時のコンテンツタイプが未指定 or JSONか否か
	 */
	private boolean isContentTypeJson(Object handler) {

		if (!(handler instanceof HandlerMethod)) {
			return false;
		}

		for (Annotation annotation : ((HandlerMethod) handler).getMethod().getDeclaredAnnotations()) {
			try {
				Class<? extends Annotation> annotationType = annotation.annotationType();
				String[] consumes = (String[]) annotationType.getMethod("consumes").invoke(annotation);
				if (annotationType.equals(PostMapping.class)) {
					if (ArrayUtils.isEmpty(consumes) ||
							(ArrayUtils.isNotEmpty(consumes) && MediaType.APPLICATION_JSON_VALUE.equals(consumes[0]))) {
						return true;
					}
				}
			} catch (Exception e) {
				return false;
			}
		}

		return false;
	}
}
