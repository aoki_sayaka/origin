package jp.or.jaot.core.datasource;

import java.lang.reflect.ParameterizedType;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseEntity;
import jp.or.jaot.core.model.BaseEntity.ORDER_ASC;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.core.model.BaseSearchDto.RANGE_CONDITION;
import jp.or.jaot.core.model.BaseSearchDto.SearchOrder;
import jp.or.jaot.core.model.Entity;
import jp.or.jaot.core.model.LoggingUpdateEntityDto;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.LoggingUtil;

/**
 * DAOの共通クラス
 * @param <E> 取り扱うエンティティのクラス
 */
public abstract class BaseDao<E> {

	/** ロガーインスタンス */
	protected final Logger logger;

	/** IN句要素最大数(SQL最大:1000) */
	private final int DEF_IN_MAX = 1000;

	/**
	 * エンティティマネージャ
	 */
	@Autowired
	private EntityManager entityManager;

	/**
	 * コンストラクタ
	 */
	public BaseDao() {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * エンティティマネージャを取得する
	 * @return エンティティマネージャ
	 */
	protected EntityManager getEntityManager() {
		return this.entityManager;
	}

	/**
	 * DBコネクション(session)情報を取得する
	 * @return DBコネクション
	 */
	protected Session getSession() {
		return this.entityManager.unwrap(Session.class);
	}

	/**
	 * エンティティに定義されたデータを追加対象にする
	 * @param entity 追加するエンティティ
	 * @return 追加したエンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public BaseEntity insertEntity(BaseEntity entity) {
		try {
			entityManager.persist(entity);
			return entity;
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * エンティティに定義されたデータを更新対象にする<br>
	 * 更新時に各エンティティが保持するバージョンを比較して楽観ロック動作を行う
	 * @param entity 更新するエンティティ
	 * @param primaryKey プライマリキーの値
	 * @return 更新エンティティログ情報DTO
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public LoggingUpdateEntityDto updateEntity(BaseEntity entity, Long primaryKey) {
		BaseEntity updateEntity = null;
		try {
			updateEntity = (BaseEntity) entityManager.find(entity.getClass(), primaryKey);
			if (!updateEntity.getVersionNo().equals(entity.getVersionNo())) {
				throw new ObjectOptimisticLockingFailureException(entity.getClass(), primaryKey);
			}
			LoggingUpdateEntityDto loggingDto = LoggingUtil.getLoggingUpdateEntityDto(updateEntity, entity);
			BeanUtil.copyProperties(entity, updateEntity, BaseEntity.updateIgnores); // 自動更新する属性は対象外
			entityManager.lock(updateEntity, LockModeType.OPTIMISTIC); // 楽観ロック
			entityManager.merge(updateEntity);
			return loggingDto;
		} catch (NullPointerException e) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_NULL_POINTER;
			String message = String.format("更新時(楽観ロック)の排他制御に失敗しました : entity(upd, now)=(%b, %b), primaryKey=%s",
					(entity != null), (updateEntity != null), primaryKey);
			throw new HibernateSystemException(errorCause, message, e);
		} catch (ObjectOptimisticLockingFailureException e) {
			String simpleName = (entity == null) ? ("") : entity.getClass().getSimpleName();
			int upd = (entity == null) ? (0) : entity.getVersionNo();
			int now = (updateEntity == null) ? (0) : updateEntity.getVersionNo();
			ERROR_CAUSE errorCause = ERROR_CAUSE.HIBERNATE_OPTIMISTIC_LOCKING;
			String message = String.format(
					"更新時(楽観ロック)の排他制御に失敗しました : class=%s, primaryKey=%s, version(upd, now)=(%d, %d)",
					simpleName, primaryKey, upd, now);
			throw new HibernateSystemException(errorCause, message, e);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * エンティティに定義されたデータを削除対象(物理)にする
	 * @param entity 削除するエンティティ
	 * @param primaryKey プライマリキーの値
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public void deleteEntity(BaseEntity entity, Long primaryKey) {
		try {
			BaseEntity deleteEntity = (BaseEntity) entityManager.find(entity.getClass(), primaryKey);
			entityManager.remove(deleteEntity);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * エンティティに定義されたデータの状態を設定する
	 * @param primaryKey プライマリキーの値
	 * @param updateUser 更新者
	 * @param enabled 状態(true=使用可能, false=使用不可)
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public void setEnableEntity(Long primaryKey, String updateUser, boolean enabled) {
		try {
			// 対象取得(排他ロック)
			BaseEntity enableEntity = null;
			if ((enableEntity = (BaseEntity) find(primaryKey, true)) == null) {
				String message = String.format("キーに該当するエンティティが存在しません : primaryKey=%s", primaryKey);
				throw new HibernateSystemException(ERROR_CAUSE.CODE_INVALID_PARAM, message, null);
			}

			// 更新
			enableEntity.setUpdateUser(updateUser); // 更新者
			enableEntity.setDeleted(!enabled); // 削除フラグ
			update(enableEntity);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * エンティティに定義されたテーブルにデータを登録する
	 * @param entity 登録するエンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public void insert(Entity entity) {
		Session session = getSession();
		try {
			session.save(entity);
			session.flush();
		} catch (Exception e) {
			session.clear();
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * エンティティに定義されたテーブルのデータを更新する
	 * @param entity 更新するエンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public void update(Entity entity) {
		Session session = getSession();
		try {
			session.update(entity);
			session.flush();
		} catch (Exception e) {
			session.clear();
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * エンティティに定義されたテーブルのデータを削除(物理)する
	 * @param entity 削除するエンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public void delete(Entity entity) {
		Session session = getSession();
		try {
			session.delete(entity);
			session.flush();
		} catch (Exception e) {
			session.clear();
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * プライマリキーを指定してエンティティを1件取得する
	 * @param primaryKey プライマリキーの値
	 * @param isExclusiveLock 排他ロックフラグ(true=排他ロックあり, false=排他ロックなし)
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public E find(Long primaryKey, boolean isExclusiveLock) {
		try {
			if (isExclusiveLock) {
				return getEntityManager().find(getEntityClazz(), primaryKey, LockModeType.PESSIMISTIC_WRITE);
			} else {
				return getEntityManager().find(getEntityClazz(), primaryKey);
			}
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * プライマリキーを複数指定してエンティティをn件取得する
	 * @param primaryKeys プライマリキーの値配列
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<E> find(Long[] primaryKeys) {
		try {
			return getSession().byMultipleIds(getEntityClazz()).multiLoad(Arrays.asList(primaryKeys));
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 全件数を取得する
	 * @return 全件数
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public Long countAll() {
		try {
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<Long> query = builder.createQuery(Long.class);
			query.select(builder.count(query.from(getEntityClazz())));

			return entityManager.createQuery(query).getSingleResult();
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * エンティティに定義されたテーブルから全件取得してリストで返す<br>
	 * 取得対象のエンティティを引数に与える<br>
	 * @return 検索結果リスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<E> findAll() {
		try {
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<E> query = builder.createQuery(getEntityClazz());
			query.select(query.from(getEntityClazz()));

			return entityManager.createQuery(query).getResultList();
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 前方一致条件値
	 * @param arg 値
	 * @return 検索条件
	 */
	protected String fwdMatch(final String arg) {
		return ((StringUtils.isEmpty(arg)) ? "" : String.format("%s%%", arg));
	}

	/**
	 * 後方一致条件値
	 * @param arg 値
	 * @return 検索条件
	 */
	protected String bwdMatch(final String arg) {
		return ((StringUtils.isEmpty(arg)) ? "" : String.format("%%%s", arg));
	}

	/**
	 * 部分一致条件値
	 * @param arg 値
	 * @return 検索条件
	 */
	protected String parMatch(final String arg) {
		return ((StringUtils.isEmpty(arg)) ? "" : String.format("%%%s%%", arg));
	}

	/**
	 * 日時取得(FROM)
	 * @param time 日時
	 * @return 日時(YYYY-MM-DD 00:00:00.00)
	 */
	protected Timestamp getFromTimestamp(Timestamp time) {
		return (time == null) ? null : new Timestamp(DateUtil.getFromDate(new Date(time.getTime())).getTime());
	}

	/**
	 * 日時取得(TO)
	 * @param time 日時
	 * @return 日付(yyyy-mm-dd 23:59:59)
	 */
	protected Timestamp getToTimestamp(Timestamp time) {
		return (time == null) ? null : new Timestamp(DateUtil.getToDate(new Date(time.getTime())).getTime());
	}

	/**
	 * 検索結果リスト取得
	 * @param resultQuery 結果クエリー
	 * @param searchDto 検索DTO
	 * @return 検索結果リスト
	 */
	protected List<E> getResultList(CriteriaQuery<E> resultQuery, BaseSearchDto searchDto) {

		// 検索結果の件数(全体)
		Long allResultCount = null;
		if ((searchDto.getMaxResult() != 0) && ((allResultCount = getAllResultCount(searchDto)) != null)
				&& allResultCount == 0L) {
			return new ArrayList<E>(); // 空リスト
		}

		// 検索結果リスト取得
		List<E> entities = null;
		if (searchDto.getMaxResult() != 0) {
			entities = getEntityManager().createQuery(resultQuery)
					.setFirstResult(searchDto.getStartPosition()) // 検索結果の開始位置
					.setMaxResults(searchDto.getMaxResult()) // 検索結果の取得件数
					.getResultList();
		} else {
			entities = getEntityManager().createQuery(resultQuery)
					.setFirstResult(searchDto.getStartPosition()) // 検索結果の開始位置(先頭)
					.getResultList();
		}
		searchDto.setResultCount(entities.size()); // 検索結果の件数
		searchDto.setAllResultCount(((allResultCount == null) ? 0L : allResultCount)); // 検索結果の件数(全体)

		return entities;
	}

	/**
	 * 検索結果数取得(全体)<br>
	 * @param searchDto 検索DTO
	 * @return 件数(全体)
	 */
	protected Long getAllResultCount(BaseSearchDto searchDto) {

		// クエリー生成
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		Root<E> root = (Root<E>) query.from(getEntityClazz());
		Predicate where = getWhere(builder, root, searchDto);

		// 実行
		if (where != null) {
			return getEntityManager().createQuery(query.select(builder.count(root)).where(where))
					.getSingleResult().longValue();
		}

		return null;
	}

	/**
	 * WHERE句取得<br>
	 * 全体の検索結果数を取得する必要がある場合は継承先で本メソッドをオーバライドします。
	 * @param builder ビルダー
	 * @param root ルート
	 * @param baseSearchDto 検索DTO
	 * @return WHERE句
	 */
	protected Predicate getWhere(CriteriaBuilder builder, Root<E> root, BaseSearchDto baseSearchDto) {
		return null;
	}

	/**
	 * IN句生成<br>
	 * IN句要素上限数毎に分割したIN句を生成して返します。
	 * @param builder ビルダー
	 * @param root ルート
	 * @param attributeName 属性名
	 * @param ids ID配列
	 * @return IN句
	 */
	protected Predicate createInPhrase(CriteriaBuilder builder, Root<?> root, String attributeName, Long[] ids) {

		// ソート(昇順)
		List<Long> sortIds = Arrays.asList(ids);
		Collections.sort(sortIds);

		return createInPhrase(builder, root, attributeName, sortIds);
	}

	/**
	 * IN句生成<br>
	 * IN句要素上限数毎に分割したIN句を生成して返します。
	 * @param builder ビルダー
	 * @param root ルート
	 * @param attributeName 属性名
	 * @param values 値配列
	 * @return IN句
	 */
	protected Predicate createInPhrase(CriteriaBuilder builder, Root<?> root, String attributeName, Integer[] values) {

		// ソート(昇順)
		List<Integer> sortIds = Arrays.asList(values);
		Collections.sort(sortIds);

		return createInPhrase(builder, root, attributeName, sortIds);
	}

	/**
	 * IN句生成<br>
	 * IN句要素上限数毎に分割したIN句を生成して返します。
	 * @param builder ビルダー
	 * @param root ルート
	 * @param attributeName 属性名
	 * @param texts 文字列配列
	 * @return IN句
	 */
	protected Predicate createInPhrase(CriteriaBuilder builder, Root<?> root, String attributeName, String[] texts) {

		// ソート(昇順)
		List<String> sortTexts = Arrays.asList(texts);
		Collections.sort(sortTexts);

		return createInPhrase(builder, root, attributeName, sortTexts);
	}

	/**
	 * IN句生成<br>
	 * IN句要素上限数毎に分割したIN句を生成して返します。
	 * @param builder ビルダー
	 * @param root ルート
	 * @param attributeName 属性名
	 * @param values 値リスト
	 * @return IN句
	 */
	protected Predicate createInPhrase(CriteriaBuilder builder, Root<?> root, String attributeName, List<?> values) {

		// IN句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				for (int cnt = 0; cnt < ((values.size() + DEF_IN_MAX - 1) / DEF_IN_MAX); cnt++) {
					int fromIdx = cnt * DEF_IN_MAX;
					int toIdx = (fromIdx + DEF_IN_MAX <= values.size()) ? (fromIdx + DEF_IN_MAX) : (values.size());
					add(root.get(attributeName).in(values.subList(fromIdx, toIdx)));
				}
			}
		};

		return builder.or(predicates.toArray(new Predicate[predicates.size()]));
	}

	/**
	 * 範囲条件生成<br>
	 * @param builder ビルダー
	 * @param root ルート
	 * @param attributeName 属性名
	 * @param value 値
	 * @param condition 範囲条件
	 * @return 範囲条件
	 */
	protected Predicate createtRangePredicate(CriteriaBuilder builder, Root<?> root, String attributeName,
			Integer value, RANGE_CONDITION condition) {

		// 条件
		Predicate predicate = null;
		if (condition != null && condition.equals(RANGE_CONDITION.GE)) {
			predicate = builder.greaterThanOrEqualTo(root.get(attributeName), value);
		} else if (condition != null && condition.equals(RANGE_CONDITION.LE)) {
			predicate = builder.lessThanOrEqualTo(root.get(attributeName), value);
		} else {
			predicate = builder.equal(root.get(attributeName), 0);
		}

		return predicate;
	}

	/**
	 * 単一検索サブクエリ生成(全件)
	 * @param builder ビルダー
	 * @param clazz 検索対象エンティティクラス
	 * @param attributeName 検索対象属性名
	 * @return サブクエリ
	 */
	protected Subquery<?> createtSelectSingleSubquery(CriteriaBuilder builder, Class<?> clazz, String attributeName) {
		Subquery<?> subQuery = builder.createQuery(clazz).subquery(clazz);
		return subQuery.select(subQuery.from(clazz).get(attributeName)).distinct(true);
	}

	/**
	 * ソート順序リスト取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return ソート順序リスト
	 */
	protected List<Order> getOrderList(CriteriaBuilder builder, Root<?> root, BaseSearchDto searchDto) {
		List<Order> orders = new ArrayList<Order>();
		for (SearchOrder order : searchDto.getOrders()) {
			Path<?> path = root.get(order.getColumn());
			orders.add((order.getAsc() == ORDER_ASC.ASC) ? builder.asc(path) : builder.desc(path));
		}
		return orders;
	}

	/**
	 * エンティティクラス取得
	 * @return ジェネリクスで指定された型パラメータのエンティティクラス
	 */
	@SuppressWarnings("unchecked")
	private Class<E> getEntityClazz() {
		return (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
}
