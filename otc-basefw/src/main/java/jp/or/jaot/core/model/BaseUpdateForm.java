package jp.or.jaot.core.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 更新フォームの基底クラス
 */
@Getter
@Setter
public abstract class BaseUpdateForm extends BaseForm {

	/** 差分更新フラグ(true=NULL値の属性を更新対象から除外する, false=NULL値の属性を更新対象から除外しない) */
	private Boolean isIncrementalUpdate;
}
