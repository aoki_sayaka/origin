package jp.or.jaot.core.util.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.or.jaot.core.model.MailAttachFileDto;

/**
 * 受信したメール内容を保持するVo<br>
 */
public class MailReceiveDto {

	/** メールヘッダ情報 */
	public Map<String, String> headerMap;

	/** メッセージ番号 */
	public int messageNumber;

	/** コンテンツタイプ */
	public String contentType;

	/**
	 * 差出人(From)のメールアドレス.
	 */
	public List<String> fromAddressList;

	/**
	 * 送信先(To)のメールアドレス.
	 */
	public List<String> toAddressList;

	/**
	 * 送信先(Cc)のメールアドレス.
	 */
	public List<String> ccAddressList;

	/**
	 * 送信先(Bcc)のメールアドレス.
	 */
	public List<String> bccAddressList;

	/**
	 * 返信先(ReplyTo)のメールアドレス.
	 */
	public List<String> replyToAddressList;

	/** 受信日時 */
	public Date receivedDate;

	/** 送信日時 */
	public Date sentDate;

	/** 件名 */
	public String subject;

	/** 本文 */
	public String body;

	/** 添付ファイル情報 */
	public List<MailAttachFileDto> attachFiles;

	/**
	 * ログ出力用の文字列を生成して返します メソッド概要<br>
	 * メソッド説明<br>
	 * @return
	 */
	public String getLogString() {
		StringBuilder sb = new StringBuilder();

		// メールヘッダ情報
		sb.append("\n------------------------------------------------------------\n");
		sb.append("headerMap:\n");
		if (headerMap != null) {
			Set<String> headerKeySet = headerMap.keySet();
			for (String key : headerKeySet) {
				sb.append(nonNull(key) + "=" + nonNull(headerMap.get(key)));
				sb.append("\n");
			}
		} else {
			sb.append("(null)\n");
		}

		// メッセージ番号
		sb.append("messageNumber:" + messageNumber + "\n");

		// コンテンツタイプ
		sb.append("contentType:" + nonNull(contentType) + "\n");

		// 差出人(From)のメールアドレス.
		sb.append(listToString("fromAddressList", "from", fromAddressList));

		// 送信先(To)のメールアドレス.
		sb.append(listToString("toAddressList", "to", toAddressList));

		// 送信先(Cc)のメールアドレス.
		sb.append(listToString("ccAddressList", "cc", ccAddressList));

		// 送信先(Bcc)のメールアドレス.
		sb.append(listToString("bccAddressList", "bcc", bccAddressList));

		// 返信先(ReplyTo)のメールアドレス.
		sb.append(listToString("replyToAddressList", "replyTo", replyToAddressList));

		// 受信日時
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (receivedDate != null) {
			sb.append("receivedDate:" + sf.format(receivedDate) + "\n");
		} else {
			sb.append("receivedDate:(null)\n");
		}

		// 送信日時
		if (sentDate != null) {
			sb.append("sentDate:" + sf.format(sentDate) + "\n");
		} else {
			sb.append("sentDate:(null)\n");
		}

		// 件名
		sb.append("subject:" + nonNull(subject) + "\n");

		// 本文
		sb.append("body:\n");
		sb.append(nonNull(body));
		sb.append("\n");

		// 添付ファイル情報
		sb.append("attachFiles:\n");
		if (attachFiles != null) {
			for (MailAttachFileDto vo : attachFiles) {
				sb.append("file=" + nonNull(vo.getFileName()) + "\n");
			}
		} else {
			sb.append("null\n");
		}

		sb.append("------------------------------------------------------------\n");

		// 文字列を返す
		return sb.toString();
	}

	private String nonNull(String str) {
		if (str == null) {
			return "(null)";
		}
		return str;
	}

	private String listToString(String header, String elemName, List<String> list) {
		StringBuilder sb = new StringBuilder();
		sb.append(nonNull(header) + ":");
		if (list != null) {
			sb.append("size=" + list.size() + "\n");
			for (int i = 0; i < list.size(); i++) {
				sb.append("[" + i + "] " + nonNull(elemName) + "=" + nonNull(list.get(i)) + "\n");
			}
		} else {
			sb.append("\n");
			sb.append("(null)\n");
		}
		return sb.toString();
	}
}
