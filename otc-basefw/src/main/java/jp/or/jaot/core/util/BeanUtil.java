package jp.or.jaot.core.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.BeanUtils;

/**
 * Beanに関するユーティリティクラス
 */
public class BeanUtil {

	/**
	 * オブジェクトの属性をコピーします。<br>
	 * 初期化対象プロパティを指定した場合、NULL値の場合もコピー対象(初期化)とします。<br>
	 * @param source コピー元
	 * @param target コピー先
	 * @param isNullIgnore NULL値の属性をコピー対象とするか否か(true=除外する, false=除外しない)
	 * @param initProperties 初期化対象プロパティ配列
	 */
	public static void copyProperties(final Object source, final Object target, final boolean isNullIgnore,
			final String[] initProperties) {

		// コピー
		if (isNullIgnore && ArrayUtils.isNotEmpty(initProperties)) {

			// クラス または NULLかつ初期化対象のプロパティを除外
			List<String> ignoreProperties = new ArrayList<String>();
			try {
				Map<String, Object> keyValueMap = PropertyUtils.describe(source);
				for (Map.Entry<String, Object> keyValue : keyValueMap.entrySet()) {
					String key = keyValue.getKey();
					Object value = keyValue.getValue();
					if (value instanceof Class
							|| (null == value && Arrays.stream(initProperties).noneMatch(P -> P.equals(key)))) {
						ignoreProperties.add(key);
					}
				}
			} catch (Exception e) {
				return; // 例外発生時は何もしない
			}

			// 除外してコピー
			BeanUtils.copyProperties(source, target, ignoreProperties.toArray(new String[0]));

		} else {

			// オブジェクトの属性をコピー
			BeanUtil.copyProperties(source, target, isNullIgnore);
		}
	}

	/**
	 * オブジェクトの属性をコピーします。<br>
	 * @param source コピー元
	 * @param target コピー先
	 * @param isNullIgnore NULL値の属性をコピー対象とするか否か(true=除外する, false=除外しない)
	 */
	public static void copyProperties(final Object source, final Object target, final boolean isNullIgnore) {

		// コピー
		if (isNullIgnore) {

			// NULLまたはクラスのプロパティを除外
			List<String> ignoreProperties = new ArrayList<String>();
			try {
				Map<String, Object> keyValueMap = PropertyUtils.describe(source);
				for (Map.Entry<String, Object> keyValue : keyValueMap.entrySet()) {
					if (null == keyValue.getValue() || keyValue.getValue() instanceof Class) {
						ignoreProperties.add(keyValue.getKey());
					}
				}
			} catch (Exception e) {
				return; // 例外発生時は何もしない
			}

			// 除外してコピー
			BeanUtils.copyProperties(source, target, ignoreProperties.toArray(new String[0]));

		} else {

			// 除外せずコピー
			BeanUtils.copyProperties(source, target);
		}
	}

	/**
	 * オブジェクトの属性をコピーします。<br>
	 * @param source コピー元
	 * @param target コピー先
	 * @param ignores 除外対象のプロパティ名
	 */
	public static void copyProperties(final Object source, final Object target, final String[] ignores) {

		// コピー
		if (ArrayUtils.isNotEmpty(ignores)) {

			// クラスまたは指定のプロパティを除外
			List<String> ignoreProperties = new ArrayList<String>();
			try {
				Map<String, Object> keyValueMap = PropertyUtils.describe(source);
				for (Map.Entry<String, Object> keyValue : keyValueMap.entrySet()) {
					Object value = keyValue.getValue();
					String key = keyValue.getKey();
					if (value instanceof Class || Arrays.asList(ignores).contains(key)) {
						ignoreProperties.add(key);
					}
				}
			} catch (Exception e) {
				return; // 例外発生時は何もしない
			}

			// 除外してコピー
			BeanUtils.copyProperties(source, target, ignoreProperties.toArray(new String[0]));

		} else {

			// 除外せずコピー
			BeanUtils.copyProperties(source, target);
		}
	}

	/**
	 * オブジェクトを生成して属性をコピーします。<br>
	 * @param source コピー元
	 * @param targetClazz コピー先のクラス
	 * @param isNullIgnore NULL値の属性をコピー対象とするか否か(true=除外する, false=除外しない)
	 * @return 属性コピーオブジェクト
	 */
	@SuppressWarnings("unchecked")
	public static <T> T createCopyProperties(final Object source, final Class<T> targetClazz,
			final boolean isNullIgnore) {
		T target = null;
		try {
			target = (T) Class.forName(targetClazz.getName()).getDeclaredConstructor().newInstance();
			BeanUtil.copyProperties(source, target, isNullIgnore);
		} catch (Exception e) {
			return null; // 例外発生時はnullを返却
		}
		return target;
	}

	/**
	 * テンプレート出力時に使うオブジェクトについて、内容を整える
	 */
	public static void adjustForTemplate(Object target) {
		@SuppressWarnings("rawtypes")
		Class clazz = target.getClass();
		Field[] fields = clazz.getDeclaredFields();

		// フィールドごとにnull埋め
		for (Field field : fields) {
			// 値を得てnullでなければ次へ
			try {
				field.setAccessible(true);
				Object obj = field.get(target);
				if (obj != null) {
					continue;
				}
			} catch (Exception e) {
				// 失敗したら無視して次へ
				continue;
			}

			try {
				String fieldType = field.getType().getName();
				// 文字列の場合は空文字をセット
				String stringType = String.class.getTypeName();
				if (fieldType.equals(stringType)) {
					field.set(target, "");
				}

				// Integerの場合は 0 をセット
				String integerType = Integer.class.getTypeName();
				if (fieldType.equals(integerType)) {
					field.set(target, Integer.valueOf(0));
				}

				// 日付の場合は0秒をセット
				String dateType = Date.class.getTypeName();
				if (fieldType.equals(dateType)) {
					Calendar cal = GregorianCalendar.getInstance();
					cal.setTimeInMillis(0);
					Date defaultDate = cal.getTime();
					field.set(target, defaultDate);
				}

				// Booleanの場合はfalseをセット
				String booleanType = Boolean.class.getTypeName();
				if (fieldType.equals(booleanType)) {
					field.set(target, Boolean.valueOf(false));
				}
			} catch (Exception e) {
				// 失敗しても無視して次へ
			}
		}
	}
}
