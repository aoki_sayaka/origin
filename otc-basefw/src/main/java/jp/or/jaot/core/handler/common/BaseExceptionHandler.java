package jp.or.jaot.core.handler.common;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.or.jaot.model.dto.error.ErrorInfoDto;
import lombok.Getter;

/**
 * 全例外ハンドラの基底クラス
 */
@Getter
@Component
public abstract class BaseExceptionHandler {

	/** ロガーインスタンス */
	protected final Logger logger;

	/** エラー情報 */
	protected ErrorInfoDto errorInfoDto = new ErrorInfoDto();

	/** HTTPステータス(デフォルト) */
	protected HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

	/**
	 * コンストラクタ
	 */
	public BaseExceptionHandler() {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * 例外のハンドリングを行う<br>
	 * ・ログ出力<br/>
	 * ・遷移先の画面を設定<br
	 * @param isContentTypeJson コンテントタイプがJSONか否か
	 * @return 遷移先の情報
	 */
	@SuppressWarnings("unchecked")
	public ModelAndView handle(boolean isContentTypeJson) {
		ErrorInfoDto errorInfo = this.getErrorInfoDto();
		// ログ出力
		outputLog(errorInfo);

		// 遷移先情報を作成する
		ModelAndView mav = new ModelAndView();
		if (isContentTypeJson) {

			// 出力対象外(スタックトレース)
			errorInfo.setOriginalExp(null);
			errorInfo.setStackTrace(null);

			// JSON生成
			Map<String, Object> jsonMap = null;
			MappingJackson2JsonView jsonView = new MappingJackson2JsonView();
			try {
				ObjectMapper mapper = new ObjectMapper();
				jsonMap = mapper.readValue(mapper.writeValueAsString(errorInfo), Map.class);
				jsonView.setAttributesMap(jsonMap);
			} catch (Exception e) {
				logger.debug(e.getMessage());
			}
			mav.setView(jsonView);
			mav.setStatus(httpStatus);
		} else {
			mav.setViewName(getViewName());
			mav.addObject("errorInfo", errorInfo);
		}

		return mav;
	}

	/**
	 * 必要に応じてログ出力をします
	 */
	protected abstract void outputLog(ErrorInfoDto errorInfo);

	/**
	 * 遷移先の画面を返します
	 */
	protected abstract String getViewName();

}
