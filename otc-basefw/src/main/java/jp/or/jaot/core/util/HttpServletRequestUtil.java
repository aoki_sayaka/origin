package jp.or.jaot.core.util;

import javax.servlet.http.HttpServletRequest;

/**
 * HttpServletRequestに関するユーティリティクラス
 */
public class HttpServletRequestUtil {

	/**
	 * コンテキストURLを取得します。<br>
	 * @param request httpリクエスト
	 * @return コンテキストURL
	 */
	public static String getContextUrl(HttpServletRequest request, String protocol) {
		StringBuffer requestUrl = request.getRequestURL();
		String requestUri = request.getRequestURI();
		String domainUrl = requestUrl.substring(0, requestUrl.length() - requestUri.length());
		String result = String.format("%s%s", domainUrl, request.getContextPath());
		if ((protocol != null) && (!protocol.equals(""))) {
			result = result.replace("http", protocol);
		}
		return result;
	}

	/**
	 * URLを取得します。<br>
	 * @param request httpリクエスト
	 * @param protocol プロトコル
	 * @return URL
	 */
	public static String getUrl(HttpServletRequest request, String protocol) {
		StringBuffer requestUrl = request.getRequestURL();
		String reqUrl = requestUrl.toString();
		if (!StringUtil.isEmpty(protocol)) {
			reqUrl = reqUrl.replaceAll("^.*://", protocol + "://");
		}

		String queryString = request.getQueryString();
		if (!StringUtil.isEmpty(queryString)) {
			queryString = String.format("?%s", queryString);
		} else if (queryString == null) {
			queryString = "";
		}

		return String.format("%s%s", reqUrl, queryString);
	}
}
