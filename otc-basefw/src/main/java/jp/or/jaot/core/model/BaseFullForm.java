package jp.or.jaot.core.model;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

/**
 * フォームの基底クラス(全て)
 */
@Getter
@Setter
public abstract class BaseFullForm extends BaseUpdateForm {

	/** 作成日時 */
	private Timestamp createDatetime;

	/** 作成者 */
	private String createUser;

	/** 更新日時 */
	private Timestamp updateDatetime;

	/** 更新者 */
	private String updateUser;

	/** 削除フラグ */
	private Boolean deleted;
}
