package jp.or.jaot.core.exception.validation;

import org.springframework.util.MultiValueMap;

import jp.or.jaot.core.exception.common.ValidationException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_KIND;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_LEVEL;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import lombok.Getter;

/**
 * フォームバリデーション例外のクラス
 */
@Getter
public class FormValidationException extends ValidationException {

	/**
	 * コンストラクタ
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 * @param errorMap エラーマップ
	 */
	public FormValidationException(String detail, String message, MultiValueMap<String, String> errorMap) {
		super(ERROR_LEVEL.WARN, ERROR_KIND.VALIDATION, ERROR_CAUSE.FORM, ERROR_PLACE.PAGE, detail, message, errorMap);
	}

	/**
	 * コンストラクタ
	 * @param errorCause エラー原因(ERROR_CAUSE.FORM_*)
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 * @param errorMap エラーマップ
	 */
	public FormValidationException(ERROR_CAUSE errorCause, String detail, String message,
			MultiValueMap<String, String> errorMap) {
		super(ERROR_LEVEL.WARN, ERROR_KIND.VALIDATION, errorCause, ERROR_PLACE.PAGE, detail, message, errorMap);
	}
}
