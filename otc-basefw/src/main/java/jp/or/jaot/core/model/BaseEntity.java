package jp.or.jaot.core.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import jp.or.jaot.core.util.StringUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * エンティティの基底クラス
 */
@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity implements Entity {

	/** 更新時の除外対象プロパティ */
	public static final String updateIgnores[] = { "createDatetime", "createUser", "updateDatetime", "versionNo" };

	/** orderの昇順、降順指定の定義 */
	public enum ORDER_ASC {
		ASC, DESC;

		/**
		 * 文字列から値に変換する
		 * @param str 文字列
		 * @return 変換したEnum値
		 */
		public static ORDER_ASC fromString(String str) {
			ORDER_ASC value = ASC;
			switch (StringUtil.nullToEmpty(str).toUpperCase()) {
			case "ASC":
				value = ASC;
				break;
			case "DESC":
				value = DESC;
				break;
			default:
				value = ASC;
				break;
			}
			return value;
		}
	};

	/** 作成日時 */
	@Column(name = "create_datetime")
	private Timestamp createDatetime;

	/** 作成者 */
	@Column(name = "create_user")
	private String createUser;

	/** 更新日時 */
	@Column(name = "update_datetime")
	private Timestamp updateDatetime;

	/** 更新者 */
	@Column(name = "update_user")
	private String updateUser;

	/** 削除フラグ */
	@Column(name = "deleted")
	private Boolean deleted;

	/** 更新バージョン */
	@Version
	@Column(name = "version_no")
	private Integer versionNo;

	/**
	 * 追加処理前にフィールド(作成日時, 更新日時, 削除フラグ)の初期値を設定
	 */
	@PrePersist
	public void onPrePersist() {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		setCreateDatetime(now);
		setUpdateDatetime(now);
		onCreated();
	}

	/**
	 * 更新処理前にフィールド(更新日時)の初期値を設定
	 */
	@PreUpdate
	public void onPreUpdate() {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		setUpdateDatetime(now);
	}

	/**
	 * 追加時の状態を設定
	 */
	public void onCreated() {
		setDeleted(false);
		setVersionNo(1);
	}

	/**
	 * 削除時(論理)の状態を設定
	 */
	public void onDeleted() {
		setDeleted(true);
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
