package jp.or.jaot.core.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jp.or.jaot.core.exception.common.BusinessException;
import jp.or.jaot.core.exception.common.SystemException;
import jp.or.jaot.core.exception.common.ValidationException;
import jp.or.jaot.core.handler.BusinessErrorHandler;
import jp.or.jaot.core.handler.GeneralErrorHandler;
import jp.or.jaot.core.handler.SystemErrorHandler;
import jp.or.jaot.core.handler.UnknownErrorHandler;
import jp.or.jaot.core.handler.ValidationErrorHandler;
import jp.or.jaot.core.handler.common.BaseExceptionHandler;
import jp.or.jaot.model.dto.error.ErrorInfoDto;

/**
 * 例外ヘルパクラス
 */
public class ExceptionHelper {

	/**
	 * エラーメッセージのプロパティファイル 名
	 */
	private static final String PROPERTY_NAME = "error.message.properties";

	/**
	 * プロパティファイル
	 */
	private static final Properties properties = PropertyUtility.read(PROPERTY_NAME);

	/**
	 * コンストラクタ
	 */
	private ExceptionHelper() {
	}

	/**
	 * エラーレベル(種別)の列挙体
	 */
	public enum ERROR_LEVEL {
		/** 致命的なエラー */
		FATAL("F"),
		/** エラー */
		ERROR("E"),
		/** 警告 */
		WARN("W"),
		/** 情報 */
		INFO("I"),
		/** デバッグ情報 */
		DEBUG("D"),
		/** トレース情報 */
		TRACE("T");

		private final String value;

		private ERROR_LEVEL(final String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * エラー内容の列挙体
	 */
	public enum ERROR_KIND {
		/** 正常 */
		SUCCESS("000"),
		/** 未知のエラー */
		UNKNOWN("001"),
		/** システムエラー */
		SYSTEM("002"),
		/** 業務エラー */
		BUSINESS("003"),
		/** バリデーションエラー */
		VALIDATION("004");

		private final String value;

		private ERROR_KIND(final String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * エラー原因の列挙体
	 */
	public enum ERROR_CAUSE {
		/** 正常 */
		SUCCESS("000"),

		/** コード.[定義開始] */
		CODE("100"),
		/** コード.Null参照 */
		CODE_NULL_POINTER("101"),
		/** コード.ゼロ除算 */
		CODE_ZERO_DIVIDE("102"),
		/** コード.不正な引数 */
		CODE_INVALID_PARAM("103"),
		/** コード.不正なデータ */
		CODE_INVALID_DATA("104"),
		/** コード.[定義終了] */
		_CODE_("199"),

		/** メール.[定義開始] */
		MAIL("200"),
		/** メール.送信 */
		MAIL_SEND("201"),
		/** メール.受信 */
		MAIL_RECEIVE("202"),
		/** メール.[定義終了] */
		_MAIL_("299"),

		/** Hibernate.[定義開始] */
		HIBERNATE("300"),
		/** Hibernate.楽観ロック */
		HIBERNATE_OPTIMISTIC_LOCKING("301"),
		/** Hibernate.[定義終了] */
		_HIBERNATE_("399"),

		/** JSON.[定義開始] */
		JSON("400"),
		/** JSON.パース */
		JSON_PARSE("401"),
		/** JSON.[定義終了] */
		_JSON_("499"),

		/** フォーム.[定義開始] */
		FORM("500"),
		/** フォーム.バインディング */
		FORM_BINDING("501"),
		/** フォーム.[定義終了] */
		_FORM_("599"),

		/** ファイル.[定義開始] */
		FILE("600"),
		/** ファイル.オープン */
		FILE_OPEN("601"),
		/** ファイル.出力 */
		FILE_WRITE("602"),
		/** ファイル.Csv */
		FILE_CSV("603"),
		/** ファイル.Excel */
		FILE_EXCEL("604"),
		/** ファイル.Pdf */
		FILE_PDF("605"),
		/** ファイル.[定義終了] */
		_FILE_("699"),

		/** 未知のエラー */
		UNKNOWN("999");

		private final String value;

		private ERROR_CAUSE(final String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public Integer getValueInt() {
			return Integer.valueOf(value);
		}
	}

	/**
	 * エラー発生箇所の列挙体
	 */
	public enum ERROR_PLACE {

		/** 共通.[定義開始] */
		COMMON("100"),
		/** 共通.[定義終了] */
		_COMMON_("199"),

		/** 認証.[定義開始] */
		AUTH("200"),
		/** 認証.[定義終了] */
		_AUTH_("299"),

		/** 画面表示.[定義開始] */
		PAGE("300"),
		/** 画面表示.[定義終了] */
		_PAGE_("399"),

		/** データソース.[定義開始] */
		DATASOURCE("400"),
		/** データソース.[定義終了] */
		_DATASOURCE_("499"),

		/** ファイル.[定義開始] */
		FILE("500"),
		/** ファイル.[定義終了] */
		_FILE_("599"),

		/** 業務ロジック.[定義開始] */
		LOGIC("600"),
		/** 業務ロジック.共通 */
		LOGIC_COMMON("601"),
		/** 業務ロジック.会員関連 */
		LOGIC_MEMBER("602"),
		/** 業務ロジック.事務局関連 */
		LOGIC_SECRETARIAT("603"),
		/** 業務ロジック.受講履歴 */
		LOGIC_ATTENDING_HISTORY("604"),
		/** 業務ロジック.[定義終了] */
		_LOGIC_("699"),

		/** 連携.[定義開始] */
		LINK("700"),
		/** 連携.内部 */
		LINK_INTERNAL("701"),
		/** 連携.外部 */
		LINK_EXTERNAL("702"),
		/** 連携.[定義終了] */
		_LINK_("799"),

		/** バッチ.[定義開始] */
		BATCH("800"),
		/** バッチ.DSK_POST取得関連 */
		BATCH_DSK("801"),
		/** バッチ.[定義終了] */
		_BATCH_("899"),

		/** 不明 */
		UNKNOWN("999");

		private final String value;

		private ERROR_PLACE(final String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	/**
	 * 例外情報を元にエラー情報を生成する.<br>
	 * @param exp 例外情報
	 * @return エラー情報
	 */
	public static ErrorInfoDto createErrorInfo(Exception exp) {
		ErrorInfoDto errorInfo = new ErrorInfoDto();

		// オリジナルのエラー情報を保持
		errorInfo.setOriginalExp(exp);

		// エラー発生時刻を設定
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		errorInfo.setDate(sdf.format(new Date()));

		// エラーの種類に応じたエラー情報を作成
		String stackTraceString = createStackTraceString(exp);

		if (SystemException.class.isInstance(exp)) {
			// システム例外
			SystemException se = (SystemException) exp;
			errorInfo.setLevel(se.getLevel().getValue());
			errorInfo.setKind(se.getKind().getValue());
			errorInfo.setPlace(se.getPlace().getValue());
			errorInfo.setCause(se.getErrorCause().getValue());
			errorInfo.setOriginalExp(se);
			errorInfo.setWorkaround(getPropertyText(errorInfo));
			errorInfo.setDetail(se.getDetail());
			errorInfo.setDetailJson(null);
			errorInfo.setStackTrace(stackTraceString);
		} else if (BusinessException.class.isInstance(exp)) {
			// 業務例外
			BusinessException be = (BusinessException) exp;
			errorInfo.setLevel(be.getLevel().getValue());
			errorInfo.setKind(be.getKind().getValue());
			errorInfo.setPlace(be.getPlace().getValue());
			errorInfo.setCause(be.getErrorCause().getValue());
			errorInfo.setOriginalExp(null);
			errorInfo.setWorkaround(be.getMessage());
			errorInfo.setDetail(be.getDetail());
			errorInfo.setDetailJson(null);
			errorInfo.setStackTrace(null);
		} else if (ValidationException.class.isInstance(exp)) {
			// バリデーション例外
			ValidationException ve = (ValidationException) exp;
			errorInfo.setLevel(ve.getLevel().getValue());
			errorInfo.setKind(ve.getKind().getValue());
			errorInfo.setPlace(ve.getPlace().getValue());
			errorInfo.setCause(ve.getErrorCause().getValue());
			errorInfo.setOriginalExp(null);
			errorInfo.setWorkaround(properties.getProperty("error.workaround.validation"));
			errorInfo.setDetail(ve.getDetail());
			errorInfo.setDetailJson(mvMap2Json(ve.getErrorMap()));
			errorInfo.setStackTrace(null);
		} else {
			// 未知のエラー
			errorInfo.setLevel(ERROR_LEVEL.ERROR.getValue());
			errorInfo.setKind(ERROR_KIND.UNKNOWN.getValue());
			errorInfo.setPlace(ERROR_PLACE.UNKNOWN.getValue());
			errorInfo.setCause(ERROR_CAUSE.UNKNOWN.getValue());
			errorInfo.setOriginalExp(exp);
			errorInfo.setWorkaround(properties.getProperty("error.workaround.unknown"));
			errorInfo.setDetail(null);
			errorInfo.setDetailJson(null);
			errorInfo.setStackTrace(stackTraceString);
		}

		return errorInfo;
	}

	/**
	 * スタックトレースを文字列化する
	 * @param exp 例外クラス
	 * @return スタックトレース
	 */
	private static String createStackTraceString(Exception exp) {
		String stackTraceString = "";
		StringWriter sw = null;
		PrintWriter pw = null;

		try {
			sw = new StringWriter();
			pw = new PrintWriter(sw);
			exp.printStackTrace(pw);
			pw.flush();
			stackTraceString = sw.toString();
		} catch (Exception e) {
			stackTraceString = "";
		}

		return stackTraceString;
	}

	/**
	 * 例外処理を行うハンドラを返す.
	 * @param errorInfo エラー情報
	 * @return 例外を処理するハンドラ
	 */
	public static BaseExceptionHandler getHandler(ErrorInfoDto errorInfoDto) {
		// エラー種別
		ERROR_KIND kind = getEnumKind(errorInfoDto.getKind());
		// エラー原因
		ERROR_CAUSE cause = getEnumCause(errorInfoDto.getCause());

		// エラー種別が判別できない場合、未知のエラーとして扱う
		if (kind == null) {
			kind = ERROR_KIND.UNKNOWN;
		}

		// エラー原因が判別できない場合、未知の原因として扱う
		if (cause == null) {
			cause = ERROR_CAUSE.UNKNOWN;
		}

		// エラー種別とエラー原因から処理するハンドラを返す
		BaseExceptionHandler handler = null;
		switch (kind) {
		case SUCCESS:
			handler = new GeneralErrorHandler(errorInfoDto);
			break;
		case SYSTEM:
			handler = new SystemErrorHandler(errorInfoDto);
			break;
		case BUSINESS:
			handler = new BusinessErrorHandler(errorInfoDto);
			break;
		case VALIDATION:
			handler = new ValidationErrorHandler(errorInfoDto);
			break;
		default:
			// 未知のエラー
			handler = new UnknownErrorHandler(errorInfoDto);
			break;
		}
		return handler;
	}

	/**
	 * 引数のエラー種別文字列に対応するENUM値を取得する
	 * @param kind エラー種別文字列
	 * @return ENUM値
	 */
	private static ERROR_KIND getEnumKind(String kind) {
		for (ERROR_KIND k : ERROR_KIND.values()) {
			if (k.getValue().equals(kind)) {
				return k;
			}
		}
		// 該当するENUMが存在しない
		return null;
	}

	/**
	 * 引数のエラー原因文字列に対応するENUM値を取得する
	 * @param kind エラー原因文字列
	 * @return ENUM値
	 */
	private static ERROR_CAUSE getEnumCause(String cause) {
		for (ERROR_CAUSE c : ERROR_CAUSE.values()) {
			if (c.getValue().equals(cause)) {
				return c;
			}
		}
		// 該当するENUMが存在しない
		return null;
	}

	/**
	 * MultiValueMapをJSON文字列化する
	 * @param mvMap MultiValueMap
	 * @return JSON文字列
	 */
	private static String mvMap2Json(MultiValueMap<String, String> mvMap) {
		ObjectMapper mapper = new ObjectMapper();
		String json = null;
		try {
			json = mapper.writeValueAsString(mvMap);
		} catch (JsonProcessingException e) {
			json = "";
		}
		return json;
	}

	/**
	 * プロパティからエラー情報をキーにして取得
	 * @param errorInfo エラー情報
	 * @return プロパティ定義
	 */
	private static String getPropertyText(ErrorInfoDto errorInfo) {

		// エラー原因
		int cause = Integer.valueOf(errorInfo.getCause());

		// プロパティキー
		String key = String.format("error.cause.%d", cause);

		// 存在チェック
		if (ERROR_CAUSE.CODE.getValueInt() < cause && cause < ERROR_CAUSE._CODE_.getValueInt()) {
			// コード
			if (!properties.containsKey(key)) {
				key = "error.workaround.unknown";
			}
		} else if (ERROR_CAUSE.MAIL.getValueInt() < cause && cause < ERROR_CAUSE._MAIL_.getValueInt()) {
			// メール
			if (!properties.containsKey(key)) {
				key = "error.workaround.system";
			}
		} else if (ERROR_CAUSE.HIBERNATE.getValueInt() < cause && cause < ERROR_CAUSE._HIBERNATE_.getValueInt()) {
			// HIBERNATE
			if (!properties.containsKey(key)) {
				key = "error.workaround.unknown";
			}
		} else if (ERROR_CAUSE.JSON.getValueInt() < cause && cause < ERROR_CAUSE._JSON_.getValueInt()) {
			// JSON
			if (!properties.containsKey(key)) {
				key = "error.workaround.unknown";
			}
		} else if (ERROR_CAUSE.FORM.getValueInt() < cause && cause < ERROR_CAUSE._FORM_.getValueInt()) {
			// フォーム
			if (!properties.containsKey(key)) {
				key = "error.workaround.unknown";
			}
		} else if (ERROR_CAUSE.FILE.getValueInt() < cause && cause < ERROR_CAUSE._FILE_.getValueInt()) {
			// ファイル
			if (!properties.containsKey(key)) {
				key = "error.workaround.unknown";
			}
		} else {
			key = "error.workaround.unknown";
		}

		// プロパティ定義取得
		String value = properties.getProperty(key);

		return value;
	}
}
