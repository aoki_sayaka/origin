package jp.or.jaot.core.util;

/**
 * 数値に関するユーティリティクラス
 */
public class NumberUtil {

	/**
	 * 値があるかを返す。0の場合は無しと判別する。
	 * @param Value 数値
	 * @param allowZero 0に対してtrueを返す場合にtrue
	 * @return 値があればtrue
	 */
	public static Boolean hasValue(Integer value, boolean allowZero) {
		if (value == null) {
			return false;
		}
		if (allowZero && (value == 0)) {
			return true;
		}
		return value > 0;
	}

	/**
	 * 値があるかを返す。0の場合は無しと判別する。
	 * @param Value 数値
	 * @param allowZero 0に対してtrueを返す場合にtrue
	 * @return 値があればtrue
	 */
	public static Boolean hasValue(Long value, boolean allowZero) {
		if (value == null) {
			return false;
		}
		if (allowZero && (value == 0)) {
			return true;
		}
		return value > 0;
	}
}
