package jp.or.jaot.core.handler;

import jp.or.jaot.core.handler.common.BaseExceptionHandler;
import jp.or.jaot.model.dto.error.ErrorInfoDto;

/**
 * 一般的なエラーを処理するハンドラクラス
 */
public class GeneralErrorHandler extends BaseExceptionHandler {

	/**
	 * コンストラクタ<br>
	 * @param errorInfoDto エラー情報DTO
	 */
	public GeneralErrorHandler(ErrorInfoDto errorInfoDto) {
		this.errorInfoDto = errorInfoDto;
	}

	@Override
	protected void outputLog(ErrorInfoDto errorInfo) {
		logger.error("一般エラーが発生しました : {}", errorInfo.getErrorCode(), errorInfo.getOriginalExp());
	}

	@Override
	protected String getViewName() {
		return "otError";
	}
}
