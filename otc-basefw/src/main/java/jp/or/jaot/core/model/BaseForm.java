package jp.or.jaot.core.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import lombok.Getter;
import lombok.Setter;

/**
 * フォームの基底クラス
 */
@Getter
@Setter
public abstract class BaseForm implements Serializable {

	/** 更新バージョン */
	private Integer versionNo;

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
