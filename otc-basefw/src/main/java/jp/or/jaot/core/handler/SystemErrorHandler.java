package jp.or.jaot.core.handler;

import jp.or.jaot.core.handler.common.BaseExceptionHandler;
import jp.or.jaot.model.dto.error.ErrorInfoDto;

/**
 * システムエラーを処理するハンドラクラス
 */
public class SystemErrorHandler extends BaseExceptionHandler {

	/**
	 * コンストラクタ<br>
	 * @param errorInfoDto エラー情報DTO
	 */
	public SystemErrorHandler(ErrorInfoDto errorInfoDto) {
		this.errorInfoDto = errorInfoDto;
	}

	@Override
	protected void outputLog(ErrorInfoDto errorInfo) {
		logger.error("システムエラーが発生しました : {}", errorInfo.getErrorCode(), errorInfo.getOriginalExp());
	}

	@Override
	protected String getViewName() {
		return "otError";
	}
}
