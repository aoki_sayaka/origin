package jp.or.jaot.core.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.search.FlagTerm;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.NullArgumentException;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.smime.SMIMEEncryptionKeyPreferenceAttribute;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.SignerInfoGenerator;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoGeneratorBuilder;
import org.bouncycastle.mail.smime.SMIMESignedGenerator;
import org.bouncycastle.mail.smime.SMIMEUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.or.jaot.core.model.MailAttachFileDto;
import jp.or.jaot.core.model.MailContentDto;
import jp.or.jaot.core.util.dto.MailReceiveDto;

/**
 * メールに関するユーティリティクラス
 * @see https://javamail.java.net/nonav/docs/api/
 */
public final class MailUtility {

	/** ロギングクラス */
	private static final Logger logger = LoggerFactory.getLogger(MailUtility.class);

	/** メールの文字コード */
	private static final String MAIL_CHAR_SET = "UTF-8";

	/** メールのプロパティファイル */
	private static final String PROPERTY_NAME = "mail.properties";

	/** 会社ごとメール設定プロパティファイル名の書式 */
	//	private static final String PROPERTY_FILE_NAME_FORMAT = "mail.%s.properties";

	/** メール送信のプロパティファイルの内容 */
	private static final Properties properties = PropertyUtility.read(PROPERTY_NAME);

	/** 証明書リスト */
	private List<X509Certificate> certificateList;

	/** 秘密鍵 */
	private PrivateKey privateKey;

	/** 証明書ファイル */
	private String certFile;

	/** 証明書のパスフレーズ */
	private String passphrase;

	/**
	 * デフォルトコンストラクタ(隠蔽)
	 */
	private MailUtility() {
		certificateList = null;
		privateKey = null;
	}

	/**
	 * インスタンスを生成します.<br>
	 * @return インスタンス.
	 */
	public static MailUtility createInstance() {
		return new MailUtility();
	}

	/**
	 * 指定したメールの内容で、メールの送信処理を行います.<br>
	 * メソッド説明<br>
	 * @param mailDto メール送信機能のDTOクラス
	 * @param attachFiles 添付ファイルのリスト
	 * @return 送信に成功したら trueを返します. それ以外は falseを返します.
	 */
	public boolean send(MailContentDto mailDto, List<MailAttachFileDto> attachFiles) {
		// 引数チェック
		if (mailDto == null) {
			throw new NullArgumentException("vo : 送信するメールの内容");
		}

		// 各メンバーのチェック
		List<String> toAddressList = mailDto.getToAddressList();
		List<String> ccAddressList = mailDto.getCcAddressList();
		List<String> bccAddressList = mailDto.getBccAddressList();
		String fromAddress = mailDto.getFromAddress();
		String subject = mailDto.getSubject();
		String body = mailDto.getBody();
		certFile = properties.getProperty("mail.certFile");
		passphrase = properties.getProperty("mail.passphrase");

		// Fromアドレスの指定がない場合はプロパティファイルから取得したFromアドレスを指定する
		if (StringUtils.isEmpty(fromAddress)) {
			fromAddress = properties.getProperty("mail.smtp.from.default");
		}

		// --------------------------------------------------------------
		// 例外をスローするチェック
		// 送信先(To)
		if (toAddressList == null) {
			throw new NullPointerException("送信先(To)のメールアドレス(複数指定可能)");
		}
		if (toAddressList.size() <= 0) {
			throw new IllegalArgumentException("送信先(To)のメールアドレス(複数指定可能)が設定されていません.");
		}
		// 送信者
		if (StringUtils.isEmpty(fromAddress)) {
			throw new NullPointerException("送信者(From)のメールアドレス");
		}

		// --------------------------------------------------------------
		// 以降は、値を再設定して処理を続行する
		if (ccAddressList == null) {
			ccAddressList = new ArrayList<String>();
		}
		if (bccAddressList == null) {
			bccAddressList = new ArrayList<String>();
		}
		if (subject == null) {
			subject = "";
		}
		if (body == null) {
			body = "";
		}

		return send(toAddressList, ccAddressList, bccAddressList, fromAddress, subject, body, attachFiles,
				properties);
	}

	/**
	 * 指定された引数に従い、メールを送信します.
	 * @param toAddressList 送信先(To)のメールアドレス
	 * @param ccAddressList 送信先(Cc)のメールアドレス
	 * @param bccAddressList 送信先(Bcc)のメールアドレス
	 * @param fromAddress 送信者(From)のメールアドレス
	 * @param subject メールのタイトル
	 * @param text メールの本文
	 * @param prop メール設定プロパティ
	 * @return メールの送信に成功したら trueを返します. そうでないなら falseを返します.
	 */
	private boolean send(List<String> toAddressList, List<String> ccAddressList, List<String> bccAddressList,
			String fromAddress, String subject, String text, Properties prop) {
		// アドレスリストから "*" を除く
		toAddressList = adjustAddressList(toAddressList);
		ccAddressList = adjustAddressList(ccAddressList);
		bccAddressList = adjustAddressList(bccAddressList);

		// 送信先が皆無ならスキップ
		int addressCount = toAddressList.size() + ccAddressList.size() + bccAddressList.size();
		if (addressCount <= 0) {
			logger.info("to, cc, bccのアドレスがすべて0件なのでスキップします");
			return true;
		}

		// 証明書の読み込み
		try {
			if (StringUtils.isNotEmpty(certFile) && StringUtils.isNotEmpty(passphrase)) {
				loadCertificateFile(certFile, passphrase);
			}
		} catch (IllegalArgumentException iae) {
			logger.error("証明書からのパラメータ取得にて例外が発生しました。", iae);
		} catch (Exception e) {
			logger.error("証明書の読み込みにて例外が発生しました。", e);
		}

		// 送信メッセージを生成する
		MimeMessage mimeMessage = createMimeMessage(prop);

		boolean res = false;
		try {
			Address address = new InternetAddress(fromAddress);
			setRecipients(mimeMessage, RecipientType.TO, toAddressList); // TO
			setRecipients(mimeMessage, RecipientType.CC, ccAddressList); // CC
			setRecipients(mimeMessage, RecipientType.BCC, bccAddressList); // BCC
			mimeMessage.setFrom(address); // 送信者
			mimeMessage.setSubject(subject, MAIL_CHAR_SET); // 件名
			mimeMessage.setText(text, MAIL_CHAR_SET); // 本文
			mimeMessage.setSentDate(new Date());// 現在日時を設定

			MimeBodyPart body = new MimeBodyPart();
			body.setText(text, MAIL_CHAR_SET);

			try {
				if (StringUtils.isNotEmpty(certFile) && StringUtils.isNotEmpty(passphrase)
						&& CollectionUtils.isNotEmpty(certificateList)) {
					// 証明書付きにする
					SMIMESignedGenerator generator = new SMIMESignedGenerator();
					generator.addSignerInfoGenerator(createSignerInfoGenerator());

					// List<X509Certificate> certificateList = new ArrayList<>();
					// certificateList.add(certificate);
					generator.addCertificates(new JcaCertStore(certificateList));

					mimeMessage.setContent(generator.generate(body));
				}
			} catch (AddressException ae) {
				logger.error("アドレスが不正です。message = ", ae.getMessage());
				return false;
			} catch (CertificateEncodingException cee) {
				logger.error("証明書の符号化でエラーが発生しました。", cee);
				return false;
			} catch (Exception e) {
				logger.error("証明書の追加処理でエラーが発生しました。", e);
				return false;
			}

			// メールの情報をデバッグログに出力
			boolean realySend = Boolean.valueOf(prop.getProperty("mail.realy.send"));
			logger.debug("------------------------------------------------------------------------");
			logger.debug(String.format("To ： %s", StringUtils.join(toAddressList, ", ")));
			logger.debug(String.format("Cc ： %s", StringUtils.join(ccAddressList, ", ")));
			logger.debug(String.format("Bcc ： %s", StringUtils.join(bccAddressList, ", ")));
			logger.debug(String.format("From ： %s", fromAddress));
			logger.debug(String.format("Subject ： %s", subject));
			logger.debug(String.format("Body ： %s", text));
			writeLogSMTPProperties(prop);
			logger.debug("------------------------------------------------------------------------");
			logger.debug(String.format("mail.realy.send ： %b", realySend));

			if (!realySend) {
				// 実際には送信しない
				return true;
			}

			// メールの送信
			Transport.send(mimeMessage);
			res = true;
		} catch (MessagingException e) {
			logger.error("メールの送信処理にて例外が発生しました。", e);
		}

		return res;
	}

	/**
	 * メール送信先のアドレスリストに「*」があったら、それぞ除去します
	 * @param toAddressList 送信先アドレスのリスト
	 * @return "*"を外したアドレスのリスト。0件になる場合は要素の件数が0件のArrayListオブジェクトが返る。
	 */
	private List<String> adjustAddressList(List<String> toAddressList) {
		List<String> newList = new ArrayList<String>();
		for (String address : toAddressList) {
			if (StringUtil.isEmpty(address)) {
				// 空文字はスキップ
				continue;
			}
			if (address.equals("*")) {
				// "*"をスキップ
				continue;
			}

			// メールフォーマットに合致しているかをチェック
			String regex = "(^[^@]+@[^@]+\\.[^@]+$)|(^$)";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(address);
			Boolean mailEnable = m.find();
			if (!mailEnable) {
				// メールのフォーマットに合致しなかったので無視
				continue;
			}

			// リストに追加する
			newList.add(address);
		}

		// 結果を返す
		return newList;
	}

	/**
	 * 指定された引数に従い、メールを送信します.
	 * @param toAddressList 送信先(To)のメールアドレス
	 * @param ccAddressList 送信先(Cc)のメールアドレス
	 * @param bccAddressList 送信先(Bcc)のメールアドレス
	 * @param fromAddress 送信者(From)のメールアドレス
	 * @param subject メールのタイトル
	 * @param text メールの本文
	 * @param attachFiles 添付ファイル
	 * @param prop メール設定プロパティ
	 * @return メールの送信に成功したら trueを返します. そうでないなら falseを返します.
	 */
	private boolean send(List<String> toAddressList, List<String> ccAddressList, List<String> bccAddressList,
			String fromAddress, String subject, String text, List<MailAttachFileDto> attachFiles,
			Properties prop) {

		// 添付ファイルが無い場合はmultipartで送信しない
		if (CollectionUtils.isEmpty(attachFiles)) {
			return send(toAddressList, ccAddressList, bccAddressList, fromAddress, subject, text, prop);
		}

		// 証明書の読み込み
		try {
			if (StringUtils.isNotEmpty(certFile) && StringUtils.isNotEmpty(passphrase)) {
				loadCertificateFile(certFile, passphrase);
			}
		} catch (IllegalArgumentException iae) {
			logger.error("証明書からのパラメータ取得にて例外が発生しました。", iae);
		} catch (Exception e) {
			logger.error("証明書の読み込みにて例外が発生しました。", e);
		}

		// 送信メッセージを生成する
		MimeMessage mimeMessage = createMimeMessage(prop);

		boolean res = false;
		try {
			Address address = new InternetAddress(fromAddress);
			setRecipients(mimeMessage, RecipientType.TO, toAddressList); // TO
			setRecipients(mimeMessage, RecipientType.CC, ccAddressList); // CC
			setRecipients(mimeMessage, RecipientType.BCC, bccAddressList); // BCC
			mimeMessage.setFrom(address); // 送信者
			mimeMessage.setSubject(subject, MAIL_CHAR_SET); // 件名
			mimeMessage.setSentDate(new Date());

			Multipart multipart = new MimeMultipart();
			// 本文
			{
				MimeBodyPart bodyPart = new MimeBodyPart();
				bodyPart.setText(text, MAIL_CHAR_SET);
				multipart.addBodyPart(bodyPart);
			}

			// 添付ファイル
			for (MailAttachFileDto attachFile : attachFiles) {
				MimeBodyPart bodyPart = new MimeBodyPart();
				FileDataSource fileDataSource = new FileDataSource(attachFile.getFilePath());
				bodyPart.setDataHandler(new DataHandler(fileDataSource));
				bodyPart.setFileName(MimeUtility.encodeWord(attachFile.getFileName()));
				multipart.addBodyPart(bodyPart);
			}

			if (StringUtils.isNotEmpty(certFile) && StringUtils.isNotEmpty(passphrase)
					&& CollectionUtils.isNotEmpty(certificateList)) {
				// 証明書付きにする
				SMIMESignedGenerator generator = new SMIMESignedGenerator();
				generator.addSignerInfoGenerator(createSignerInfoGenerator());

				// List<X509Certificate> certificateList = new ArrayList<>();
				// certificateList.add(certificate);
				generator.addCertificates(new JcaCertStore(certificateList));

				MimeBodyPart allBody = new MimeBodyPart();
				allBody.setContent(multipart);

				// mimeMessage.setContent(multipart);
				mimeMessage.setContent(generator.generate(allBody));
			}

			// メールの情報をデバッグログに出力
			logger.debug("------------------------------------------------------------------------");
			logger.debug(String.format("To ： %s", StringUtils.join(toAddressList, ", ")));
			logger.debug(String.format("Cc ： %s", StringUtils.join(ccAddressList, ", ")));
			logger.debug(String.format("Bcc ： %s", StringUtils.join(bccAddressList, ", ")));
			logger.debug(String.format("From ： %s", fromAddress));
			logger.debug(String.format("Subject ： %s", subject));
			logger.debug(String.format("Body(text) ： %s", text));
			writeLogSMTPProperties(prop);
			logger.debug("------------------------------------------------------------------------");

			boolean realySend = Boolean.valueOf(prop.getProperty("mail.realy.send"));
			if (!realySend) {
				// 実際には送信しない
				return true;
			}

			// メールの送信
			Transport.send(mimeMessage);
			res = true;
		} catch (MessagingException me) {
			logger.error("メールの送信処理にて例外が発生しました。", me);
		} catch (UnsupportedEncodingException uex) {
			logger.error("メールのファイル添付処理にて例外が発生しました。", uex);
		} catch (CertificateEncodingException cee) {
			logger.error("証明書の符号化でエラーが発生しました。", cee);
		} catch (Exception e) {
			logger.error("証明書の追加処理でエラーが発生しました。", e);
		}

		return res;
	}

	/**
	 * 引数の送信メッセージオブジェクトに送信先のメールアドレスを指定します.
	 * @param mimeMessage 送信メッセージオブジェクト
	 * @param recipientType 受け取り先のタイプ
	 * @param mailAddressList メールアドレスのリスト
	 * @throws MessagingException メッセージングエラー
	 */
	private void setRecipients(MimeMessage mimeMessage, RecipientType recipientType, List<String> mailAddressList)
			throws MessagingException {
		// 複数の宛先を指定するために InternetAddressクラスに変換する
		int length = mailAddressList.size();
		InternetAddress[] addresses = new InternetAddress[length];
		for (int i = 0; i < mailAddressList.size(); i++) {
			addresses[i] = new InternetAddress(mailAddressList.get(i));
		}
		mimeMessage.setRecipients(recipientType, addresses);
	}

	/**
	 * 指定されたプロパティを元に、送信メッセージオブジェクトを作成します.<br>
	 * @param prop メール設定プロパティ
	 * @return 送信メッセージオブジェクトを返します. nullは返しません
	 */
	private MimeMessage createMimeMessage(Properties prop) {
		// 送信メッセージを生成する
		return new MimeMessage(createSession(prop));
	}

	/**
	 * 指定されたプロパティを元に、メールセッションオブジェクトを作成します.<br>
	 * @param prop メール設定プロパティ
	 * @return メールセッションオブジェクトを返します. nullは返しません.
	 */
	private Session createSession(Properties prop) {
		// 認証の有無で、メールセッションの作成の仕方が異なる
		Session session = null;
		String authValue = prop.getProperty("mail.smtp.auth");
		if (StringUtils.isNotEmpty(authValue) && Boolean.parseBoolean(authValue)) {
			String userName = prop.getProperty("mail.smtp.user");
			String password = prop.getProperty("mail.smtp.password");
			session = Session.getInstance(prop, new LocalAuthenticator(userName, password));
		} else {
			session = Session.getInstance(prop);
		}
		return session;
	}

	/**
	 * ユーザ識別クラス
	 * @author $Author$
	 * @version $Revision$
	 */
	public static class LocalAuthenticator extends javax.mail.Authenticator {
		/** ユーザ名 */
		private final String user;

		/** パスワード */
		private final String password;

		/**
		 * コンストラクタ
		 * @param user ユーザ名
		 * @param password パスワード
		 */
		public LocalAuthenticator(final String user, final String password) {
			this.user = user;
			this.password = password;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(this.user, this.password);
		}
	}

	/**
	 * 接続先のメールサーバにあるメールの数を返します.<br>
	 * @param protocol メール受信のプロトコル
	 * @return メールの受信に成功したら、接続先のメールサーバにあるメールの数を返します. それ以外は nullを返します.
	 * @see https://javamail.java.net/nonav/docs/api/
	 */
	public Integer receiveMessageCount(ReceiveProtocolEnum protocol) {

		// メールを受信するための接続先の情報を取得
		String host = getReceiveHost(protocol);
		int port = getReceivePort(protocol);
		String username = getReceiveUser(protocol);
		String password = getReceivePassword(protocol);

		// メール受信のセッションを作成.
		properties.setProperty("java.net.preferIPv4Stack", "true");
		Session session = Session.getDefaultInstance(properties, null);

		// サーバにあるメールの数を取得する
		Integer count = null;
		Store store = null;
		try {
			store = session.getStore(protocol.value);
			store.connect(host, port, username, password);

			Folder folder = store.getFolder("INBOX");
			folder.open(Folder.READ_ONLY);

			count = folder.getMessageCount();
			folder.close(false);// メッセージは消さない.
		} catch (MessagingException e) {
			logger.error("メッセージング エラーが発生しました。", e);
		} catch (RuntimeException e) {
			logger.error("実行時例外が発生しました。", e);
		} finally {
			try {
				if (store != null) {
					store.close();
				}
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		return count;
	}

	/**
	 * 接続先のメールサーバにあるメール情報を全て受信します.<br>
	 * @param protocol メール受信のプロトコル
	 * @return メールの受信に成功したら、そのメールの内容を返します. それ以外は nullを返します.
	 * @see https://javamail.java.net/nonav/docs/api/
	 */
	public List<MailReceiveDto> receive(ReceiveProtocolEnum protocol) {
		return _receive(protocol, (int[]) null);
	}

	/**
	 * 接続先のメールサーバにあるメール情報を全て受信します.<br>
	 * @param protocol メール受信のプロトコル
	 * @param msgnums メールメッセージ情報を取得するメッセージ番号の配列.
	 * @return メールの受信に成功したら、そのメールの内容を返します. それ以外は nullを返します.
	 * @see https://javamail.java.net/nonav/docs/api/
	 */
	public List<MailReceiveDto> receive(ReceiveProtocolEnum protocol, int[] msgnums) {
		return _receive(protocol, msgnums);
	}

	/**
	 * 接続先のメールサーバにあるメール情報を全て受信します.<br>
	 * @param protocol メール受信のプロトコル
	 * @param msgnums メールメッセージ情報を取得するメッセージ番号の配列.
	 * @return メールの受信に成功したら、そのメールの内容を返します. それ以外は nullを返します.
	 * @see https://javamail.java.net/nonav/docs/api/
	 */
	private List<MailReceiveDto> _receive(ReceiveProtocolEnum protocol, int[] msgnums) {

		// メールを受信するための接続先の情報を取得
		String host = getReceiveHost(protocol);
		int port = getReceivePort(protocol);
		String username = getReceiveUser(protocol);
		String password = getReceivePassword(protocol);

		// メール受信のセッションを作成.
		properties.setProperty("java.net.preferIPv4Stack", "true");
		Session session = Session.getDefaultInstance(properties, null);

		// 一時フォルダのパスを取得する
		String tempDirPath = properties.getProperty("mail.temp.dirPath", "");

		// メールを受信処理
		List<MailReceiveDto> l = null;
		Store store = null;
		try {
			store = session.getStore(protocol.value);
			store.connect(host, port, username, password);

			Folder folder = store.getFolder("INBOX");
			folder.open(Folder.READ_WRITE);
			FlagTerm unread = new FlagTerm(new Flags(Flags.Flag.SEEN), false);
			// メッセージ番号で分岐
			Message[] msgs = null;
			if (msgnums != null) {
				Message[] msgsFull = folder.getMessages(msgnums);
				msgs = folder.search(unread, msgsFull);
			} else {
				msgs = folder.search(unread);
			}
			l = new ArrayList<MailReceiveDto>();
			for (Message msg : msgs) {
				MailReceiveDto vo = null;
				try {
					if (msg.getFlags().contains(Flags.Flag.SEEN)) {
						continue;
					}
					vo = createMailReceiveVo(msg, tempDirPath);
					logger.info(vo.getLogString());
					l.add(vo);

					// 既読フラグを設定
					msg.setFlag(Flags.Flag.DELETED, true); // 既読Flagを付ける
				} catch (Exception e) {
					logger.error("メール取得処理にて例外が発生しました。", e);
					try {
						// 取り込み失敗したメールを削除しないよう、既読（削除）フラグを解除
						msg.setFlag(Flags.Flag.DELETED, false);
						// 不完全なValueObjectが結果リストに入っていたら削除
						if (vo != null && l.contains(vo)) {
							l.remove(vo);
						}
					} catch (Exception ex) {
						logger.error("メール取得時の例外リカバリ処理にて例外が発生しました。", ex);
					}
				}
			}
			folder.close(true);// メッセージを消す
			return l;
		} catch (MessagingException e) {
			logger.error("メッセージング エラーが発生しました。", e);
		} catch (RuntimeException e) {
			logger.error("実行時例外が発生しました。", e);
		} finally {
			try {
				if (store != null) {
					store.close();
				}
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	/**
	 * メールメッセージ情報から受信情報Voを作成します.<br>
	 * @param msg メールメッセージ情報
	 * @param tempDirPath 添付ファイルの出力先となるフォルダパス
	 * @return 受信情報Voを返します.
	 * @throws MessagingException
	 * @throws IOException
	 */
	private MailReceiveDto createMailReceiveVo(Message msg, String tempDirPath) throws MessagingException, IOException {
		//
		MailReceiveDto vo = new MailReceiveDto();
		// メールヘッダ
		Map<String, String> headerMap = new TreeMap<String, String>();
		@SuppressWarnings("rawtypes")
		Enumeration headers = msg.getAllHeaders();
		while (headers.hasMoreElements()) {
			Header h = (Header) headers.nextElement();
			headerMap.put(h.getName(), h.getValue());
		}
		vo.headerMap = headerMap;
		//
		vo.messageNumber = msg.getMessageNumber();
		vo.contentType = msg.getContentType();
		vo.subject = msg.getSubject();
		vo.body = getMailBody(msg);
		// メールアドレス
		vo.fromAddressList = getMailAddress(msg.getFrom());
		vo.toAddressList = getMailAddress(msg.getRecipients(RecipientType.TO));
		vo.ccAddressList = getMailAddress(msg.getRecipients(RecipientType.CC));
		vo.bccAddressList = getMailAddress(msg.getRecipients(RecipientType.BCC));
		vo.replyToAddressList = getMailAddress(msg.getReplyTo());
		// 日時
		// ※JavaMailのAPIの仕様として、POP時は受信日時が入らないので、受信日時がnullの場合は現在日時を設定する
		// http://www.oracle.com/technetwork/java/faq-135477.html#rcvdate
		vo.receivedDate = (msg.getReceivedDate() != null) ? msg.getReceivedDate() : new Date();
		vo.sentDate = msg.getSentDate();

		// 添付ファイル の構築
		// http://d.hatena.ne.jp/sy-2010/20120518/1337303597
		vo.attachFiles = getMailAttachFile(msg, tempDirPath);
		return vo;
	}

	/**
	 * メール本文を取得します.<br>
	 * @param msg メールメッセージ情報
	 * @return メール本文を取得します. 取得に失敗したら nullを返します.
	 * @throws IOException
	 * @throws MessagingException
	 */
	private String getMailBody(Message msg) throws IOException, MessagingException {
		String body = null;
		Object obj = msg.getContent();
		//
		if (obj instanceof Multipart) {
			body = getMailBody((Multipart) obj);
		} else if (obj instanceof String) {
			if (msg.isMimeType("text/html")) {
				body = removeHtmlTag((String) obj);
			} else {
				body = (String) obj;
			}
		} else {
			// その他
			logger.warn("想定していないクラスです。処理続行します. クラス名 : " + obj.getClass().getName());
		}
		return body;
	}

	/**
	 * コンテンツが MultiPartのメール本文を取得します.<br>
	 * @param msg メール本文情報(タイプがMultipart)
	 * @return メール本文を取得します. 取得に失敗したら nullを返します.
	 * @throws IOException
	 * @throws MessagingException
	 */
	private String getMailBody(Multipart multiPart) throws IOException, MessagingException {
		String body = null;
		String textPart = null;
		String htmlPart = null;
		int countPart = multiPart.getCount();
		for (int i = 0; i < countPart; i++) {
			Part bodyPart = multiPart.getBodyPart(i);
			logger.debug("bodyPart[{}] Content-Type : {}", i, bodyPart.getContentType());
			if (bodyPart.isMimeType("text/plain")) {
				// プレーンテキストパート取得
				logger.debug("プレーンテキストパート取得");
				textPart = (String) bodyPart.getContent();
			} else if (bodyPart.isMimeType("text/html")) {
				// HTMLパート取得
				logger.debug("HTMLパート取得");
				htmlPart = (String) bodyPart.getContent();
			} else if (bodyPart.isMimeType("multipart/*")) {
				// multipartの場合は再帰呼び出しを行う
				logger.debug("multipart解析（再帰呼出）");
				body = getMailBody((Multipart) bodyPart.getContent());
			}
		}

		// 戻り値の優先順位は、プレーンテキストパート＞HTMLパート＞multipart解析（再帰呼出）結果とする。
		if (textPart != null) {
			logger.debug("プレーンテキストパートから取得した本文を返します。");
			body = textPart;
		} else if (htmlPart != null) {
			logger.debug("HTMLパートから取得した本文を返します。");
			body = removeHtmlTag(htmlPart);
		}

		logger.debug("getMailBody returns : {}", body);
		return body;
	}

	/**
	 * 文字列からHTMLタグを除去します。<br>
	 * HTMLメールの本文HTMLパートからテキスト部分のみ取得する際の使用を想定しています。<br>
	 * @param html HTMLタグを含む文字列
	 * @return HTMLタグが除去された文字列
	 */
	public static String removeHtmlTag(String html) {
		if (html == null) {
			throw new IllegalArgumentException("removeHtmlTag : html must not be null.");
		}
		// HTMLタグの除去およびHTMLエンコードされた文字をデコード（"&gt;" → ">"、"&lt;" → "<" 等）して返す
		String result = "";
		try {
			// コメントタグ(<!-- -->)を除去した後、他のタグを除去している（ユーザに無意味なスタイル記述が残る問題の対策）
			// HTMLタグが複数行に渡って記述された場合を考慮し、sフラグを設定(?s)
			// タグ除去の正規表現は、属性値(attribute/property)に">"が記述された場合を考慮したもの
			result = StringEscapeUtils.unescapeHtml(
					html.replaceAll("<!--(?s:.)*?-->", "").replaceAll("<(?s:\".*?\"|'.*?'|[^'\"])*?>", ""));
		} catch (Throwable t) {
			// HTMLタグ除去に失敗した場合は、元のHTML文字列をそのまま返す
			logger.error("HTMLタグ除去処理時に例外発生", t);
			result = html;
		}
		return result;
	}

	/**
	 * メールに添付された添付ファイルの情報を取得します.<br>
	 * @param msg メールメッセージ情報
	 * @param tempDirPath 添付ファイルの出力先となるフォルダパス
	 * @return 添付ファイルの情報を取得します.
	 * @throws IOException
	 * @throws MessagingException
	 */
	private List<MailAttachFileDto> getMailAttachFile(Message msg, String tempDirPath)
			throws IOException, MessagingException {
		List<MailAttachFileDto> l = new ArrayList<MailAttachFileDto>();
		Object obj = msg.getContent();
		//
		if (obj instanceof Multipart) {
			Multipart multiPart = (Multipart) obj;
			//
			int partNum = multiPart.getCount();
			for (int i = 0; i < partNum; i++) {
				//
				Part part = multiPart.getBodyPart(i);
				// logger.debug(String.format("[添付 %02d] [contentType]:%s", i, part.getContentType()));
				String disposition = part.getDisposition();
				if (StringUtils.equals(disposition, Part.ATTACHMENT) || StringUtils.equals(disposition, Part.INLINE)) {
					String filename = part.getFileName();
					if (filename != null) {
						filename = MimeUtility.decodeText(filename);
						InputStream is = part.getInputStream();
						MailAttachFileDto vo = createMailAttachFileVo(is, tempDirPath, filename);
						if (vo != null) {
							l.add(vo);
						}
					}
				}
			}
		} else if (obj instanceof InputStream) {
			// 添付ファイル
			InputStream is = (InputStream) obj;
			String filename = msg.getFileName();
			filename = MimeUtility.decodeText(filename);
			MailAttachFileDto vo = createMailAttachFileVo(is, tempDirPath, filename);
			if (vo != null) {
				l.add(vo);
			}
		} else if (obj instanceof String) {
			// テキストのみ(何もしない)
		} else {
			// その他
			logger.warn("想定していないクラスです。処理続行します. クラス名 : " + obj.getClass().getName());
		}

		return l;
	}

	/**
	 * 添付ファイルの情報を作成します.<br>
	 * 実際に入力ストリームから読み込み、dirpath下にファイルを作成します. 出力されたときのファイルパスは、【引数:dirpath】/【UUID】
	 * @param is 添付ファイルの入力ストリーム
	 * @param dirpath 添付ファイルの出力先となるフォルダパス
	 * @param filename 添付ファイル名
	 * @return 添付ファイルの情報を返します. 作成に失敗したら nullを返します.
	 */
	private MailAttachFileDto createMailAttachFileVo(InputStream is, String dirpath, String filename) {
		MailAttachFileDto vo = null;
		// 添付ファイル名が長い場合に例外が発生するため、出力（一時保存）ファイル名にはUUIDのみ使用する
		// ※添付ファイル名から取得した拡張子の付与を検討したが、拡張子が異常に長いケースを考慮して取り止め
		File file = new File(dirpath, UUID.randomUUID().toString());
		if (createFileWithInputStream(is, file)) {
			vo = new MailAttachFileDto();
			vo.setFilePath(file.getAbsolutePath());
			vo.setFileName(filename);
			vo.setPhysicalFileName(file.getName());
			logger.debug(String.format("【ファイル名】: %s 【パス】: %s", vo.getFileName(), vo.getFilePath()));
		}
		return vo;
	}

	/**
	 * 入力ストリームから指定したファイルに出力します.<br>
	 * @param is 入力ストリーム
	 * @param destFile 出力先となるファイル情報.
	 * @return ファイルの出力に成功した場合は trueを返します. それ以外の場合は falseを返します.
	 */
	private boolean createFileWithInputStream(InputStream is, File destFile) {
		boolean ret = false;
		byte[] buffer = new byte[1024];
		int length = 0;
		try (FileOutputStream fos = new FileOutputStream(destFile)) {
			while ((length = is.read(buffer)) >= 0) {
				fos.write(buffer, 0, length);
			}
			ret = true;
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return ret;
	}

	/**
	 * 受信サーバにアクセスする際のホスト名を返します.<br>
	 * @param protocol 受信時のプロトコル
	 * @return 受信サーバにアクセスする際のホスト名を返します. 未設定なら空文字列を返します.
	 */
	// ホスト
	private String getReceiveHost(ReceiveProtocolEnum protocol) {
		String key = "mail." + protocol.value + ".host";
		return properties.getProperty(key, "");
	}

	/**
	 * 受信サーバにアクセスする際のポート番号を返します.<br>
	 * @param protocol 受信時のプロトコル
	 * @return 受信サーバにアクセスする際のポート番号を返します. 未設定なら -1を返します.
	 */
	private int getReceivePort(ReceiveProtocolEnum protocol) {
		String key = "mail." + protocol.value + ".port";
		return Integer.parseInt(properties.getProperty(key, "-1"));
	}

	/**
	 * 受信サーバにアクセスする際のユーザ名を返します.<br>
	 * @param protocol 受信時のプロトコル
	 * @return 受信サーバにアクセスする際のユーザ名を返します. 未設定なら空文字列を返します.
	 */
	private String getReceiveUser(ReceiveProtocolEnum protocol) {
		String key = "mail." + protocol.value + ".user";
		return properties.getProperty(key, "");
	}

	/**
	 * 受信サーバにアクセスする際のパスワードを返します.<br>
	 * @param protocol 受信時のプロトコル
	 * @return 受信サーバにアクセスする際のパスワードを返します. 未設定なら空文字列を返します.
	 */
	private String getReceivePassword(ReceiveProtocolEnum protocol) {
		String key = "mail." + protocol.value + ".password";
		return properties.getProperty(key, "");
	}

	/**
	 * アドレスクラスからメールアドレスを取得します.<br>
	 * 引数が nullなら 空のリストを返します.<br>
	 * @param addrs アドレスのリスト
	 * @return メールアドレスのリストを返します.
	 */
	private List<String> getMailAddress(Address[] addrs) {
		List<String> l = new ArrayList<String>();
		if (addrs != null) {
			for (Address addr : addrs) {
				l.add(((InternetAddress) addr).getAddress());
			}
		}
		return l;
	}

	/**
	 * メール送信設定をデバッグログに出力します.<br>
	 * キー名が"mail.smtp."で始まり、かつ、末尾が"password"でないプロパティをデバッグログ出力します.<br>
	 * @param prop メール設定Properties
	 */
	private static void writeLogSMTPProperties(Properties prop) {
		for (String key : prop.stringPropertyNames()) {
			if (key.startsWith("mail.smtp.") && !key.endsWith("password")) {
				logger.debug(String.format("%s ： %s", key, prop.getProperty(key)));
			}
		}
	}

	/**
	 * 受信プロトコルのEnum<br>
	 * @author dtakahashi
	 * @version $Revision$
	 */
	public enum ReceiveProtocolEnum {
		/** pop3 */
		POP3("pop3"),
		/** imap */
		IMAP("imap");

		/** 値 */
		public String value;

		private ReceiveProtocolEnum(String value) {
			this.value = value;
		}

		/**
		 * 渡された文字列と同じ値を持つEnumを返します。<br>
		 * @param value 検索対象の文字列
		 * @return 渡されたvalueと同じ値を持つEnumを返します。見つからない場合は null を返します.
		 */
		public static ReceiveProtocolEnum getByValue(String val) {
			for (ReceiveProtocolEnum e : ReceiveProtocolEnum.values()) {
				if (StringUtils.equals(e.value, val)) {
					return e;
				}
			}
			return null;
		}
	}

	/**
	 * メソッド概要<br>
	 * メソッド説明<br>
	 * @return
	 * @throws Exception
	 */
	private SignerInfoGenerator createSignerInfoGenerator() throws Exception {
		ASN1EncodableVector encodableVector = new ASN1EncodableVector();

		X509Certificate certificate = null;
		if (certificateList != null && certificateList.size() > 0) {
			certificate = certificateList.get(0);
		}

		IssuerAndSerialNumber issuerAndSerialNumberFor = null;
		if (certificate != null) {
			issuerAndSerialNumberFor = SMIMEUtil.createIssuerAndSerialNumberFor(certificate);
		}
		if (issuerAndSerialNumberFor != null) {
			encodableVector.add(new SMIMEEncryptionKeyPreferenceAttribute(issuerAndSerialNumberFor));
		}

		AttributeTable attributeTable = new AttributeTable(encodableVector);
		JcaSimpleSignerInfoGeneratorBuilder simpleSignerInfoGeneratorBuilder = new JcaSimpleSignerInfoGeneratorBuilder();
		simpleSignerInfoGeneratorBuilder.setSignedAttributeGenerator(attributeTable);

		return simpleSignerInfoGeneratorBuilder.build("SHA1withRSA", privateKey, certificate);
	}

	/**
	 * 証明書から情報を取得する メソッド概要<br>
	 * メソッド説明<br>
	 * @param certificateFile 証明書ファイル
	 * @param passphrase 証明書のパスフレーズ
	 * @throws Exception
	 */
	private void loadCertificateFile(String certificateFile, String passphrase) throws Exception {

		Certificate[] certificate;
		KeyStore keyStore;
		//		try (InputStream stream = new FileInputStream(certificateFile)) {
		try (InputStream stream = this.getClass().getResourceAsStream(certificateFile)) {
			keyStore = KeyStore.getInstance("pkcs12");
			keyStore.load(stream, passphrase.toCharArray());
		}

		// キーエントリのエイリアス名を取得
		List<String> aliasNames = getAliasName(keyStore);
		if (aliasNames.size() > 0) {
			privateKey = (PrivateKey) keyStore.getKey(aliasNames.get(0), passphrase.toCharArray());
		}

		if (certificateList == null) {
			certificateList = new ArrayList<X509Certificate>();
		}

		for (int i = 0; i < aliasNames.size(); i++) {
			certificate = keyStore.getCertificateChain(aliasNames.get(i));
			for (int j = 0; j < certificate.length; j++) {
				certificateList.add((X509Certificate) certificate[j]);
			}
		}
	}

	/**
	 * エリアス名を取得する。 メソッド概要<br>
	 * メソッド説明<br>
	 * @param keyStore キーストア
	 * @return エリアス名
	 * @throws KeyStoreException
	 */
	private List<String> getAliasName(KeyStore keyStore) throws KeyStoreException {
		Enumeration<String> aliases = keyStore.aliases();
		List<String> lists = new ArrayList<String>();
		while (aliases.hasMoreElements()) {
			String alias = aliases.nextElement();
			if (keyStore.isKeyEntry(alias)) {
				lists.add(alias);
			}
		}
		return lists;
		// throw new IllegalArgumentException("key entry was not found.");
	}

	/**
	 * システム管理者用メールアドレスを取得
	 * @return
	 */
	public String getSystemMailAddress() {
		String systemAddress = properties.getProperty("mail.systemadministrator");

		return systemAddress;
	}
}
