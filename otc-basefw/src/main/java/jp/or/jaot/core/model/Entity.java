package jp.or.jaot.core.model;

import java.io.Serializable;

/**
 * Entityのマーカインタフェース
 */
public interface Entity extends Serializable {
	// Nothing to define.
}
