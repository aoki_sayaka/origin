package jp.or.jaot.core.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * リフレクションに関するユーティリティクラス
 */
public class ReflectionUtil {

	/**
	 * フィールド値を取得する
	 * @param obj フィールドオブジェクト
	 * @param name フィールド名
	 * @return フィールド値
	 */
	public static Object getValue(Object obj, String name) {
		try {
			Field field = obj.getClass().getDeclaredField(name);
			field.setAccessible(true);
			return field.get(obj);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * クラスに保持する全ての属性(親クラス含む)を返す
	 * @param targetClazz クラスタイプ
	 * @return 属性リスト
	 */
	public static List<Field> getInheritedPrivateFields(Class<?> targetClazz) {
		List<Field> result = new ArrayList<Field>();
		Class<?> clazz = targetClazz;
		while (clazz != null && clazz != Object.class) {
			Collections.addAll(result, clazz.getDeclaredFields());
			clazz = clazz.getSuperclass();
		}
		return result;
	}
}
