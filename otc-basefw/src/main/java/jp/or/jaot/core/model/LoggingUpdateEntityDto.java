package jp.or.jaot.core.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.core.util.LoggingUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * 更新エンティティのログ情報を保持するDTO
 */
@Getter
@Setter
public class LoggingUpdateEntityDto {

	/**
	 * 更新区分の列挙体
	 */
	public enum CATEGORY {

		/** 追加 */
		ADD(1),
		/** 削除 */
		DEL(2),
		/** 変更 */
		MOD(3);

		/** 値 */
		private final Integer value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private CATEGORY(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}
	}

	/** クラス名 */
	private String className;

	/** 日付 */
	private Date date;

	/** ID */
	private String id;

	/** 更新バージョン */
	private String versionNo;

	/** ログ情報(要素) */
	private List<LoggingElementDto> loggingElements = new ArrayList<LoggingElementDto>();

	/**
	 * コンストラクタ
	 * @param className クラス名
	 */
	public LoggingUpdateEntityDto(String className) {
		this.className = className; // クラス名
		this.date = new Date(); // 日付(現在時刻)
	}

	/**
	 * クラス名取得(簡易)
	 * @return クラス名(簡易)
	 */
	public String getSimpleClassName() {
		List<String> splits = Arrays.asList(className.split("\\."));
		return splits.stream().skip(splits.size() - 1).findFirst().get();
	}

	/**
	 * テーブル名(物理)取得
	 * @return  テーブル名(物理)
	 */
	public String getPhysicalName() {
		return LoggingUtil.cnvNameEntityToTable(getSimpleClassName());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		int cnt = 0;
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("className=%s", getSimpleClassName()));
		sb.append(String.format(", physicalName=%s", getPhysicalName()));
		sb.append(String.format(", date=%s", DateUtil.formatDBTimeString(getDate())));
		sb.append(String.format(", id=%s", getId()));
		sb.append(String.format(", versionNo=%s", getVersionNo()));
		for (LoggingElementDto element : loggingElements) {
			sb.append(String.format(", [%03d]%s(%s) : (%s -> %s)", cnt++, element.getFieldName(),
					element.getFieldType(), element.getBefore(), element.getAfter()));
		}
		return sb.toString();
	}
}
