package jp.or.jaot.core.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.or.jaot.core.util.BeanUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * エンティティを主管するドメインの基底クラス<br>
 * 共通的な業務ロジックを実装する
 * @param <E>
 */
@Getter
@Setter
public abstract class BaseDomain<E> implements Domain, EntityAware<E> {

	/** ロガーインスタンス */
	protected final Logger logger;

	/** エンティティ */
	private E entity;

	/**
	 * コンストラクタ
	 */
	public BaseDomain() {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * 引数のエンティティの値をコピーする
	 * entityのセットは参照が変わりそうで怖いので値コピーを用意した
	 * @param entity
	 */
	public void copyValue(E entity) {
		BeanUtil.copyProperties(this, entity, false);
	}
}
