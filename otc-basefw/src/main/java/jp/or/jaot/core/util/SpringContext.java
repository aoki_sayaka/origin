package jp.or.jaot.core.util;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * SpringのApplicationContextを取得するためのUtilクラス<br/>
 * アプリケーションの起動時に setApplicationContext() で値を設定する。
 */
@Component
public class SpringContext {

	/**
	 * アプリケーションコンテキストのオブジェクト
	 */
	private ApplicationContext applicationContext;

	/**
	 * Beanを取得する
	 * @param clazz 取得するBeanのクラス名
	 * @return 生成したBean
	 */
	public <T> T getBean(Class<T> clazz) {
		return (T) (applicationContext.getBean(clazz));
	}

	/**
	 * アプリケーションコンテキストを取得する
	 * @return アプリケーションコンテキストのオブジェクト
	 */
	public final ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * アプリケーションコンテキストを設定する
	 */
	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}