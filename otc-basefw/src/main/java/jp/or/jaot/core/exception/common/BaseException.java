package jp.or.jaot.core.exception.common;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_KIND;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_LEVEL;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import lombok.Getter;

/**
 * 全例外の基底クラス
 */
@Getter
public abstract class BaseException extends RuntimeException {

	/** エラーレベル */
	private ERROR_LEVEL level;

	/** エラー内容 */
	private ERROR_KIND kind;

	/** エラー原因 */
	private ERROR_CAUSE errorCause;

	/** エラー発生箇所 */
	private ERROR_PLACE place;

	/** エラー詳細 */
	private String detail;

	/**
	 * コンストラクタ
	 * @param level エラー種別
	 * @param kind エラー内容
	 * @param errorCause エラー原因
	 * @param place エラー発生箇所
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 * @param original オリジナルの例外
	 */
	public BaseException(ERROR_LEVEL level, ERROR_KIND kind, ERROR_CAUSE errorCause, ERROR_PLACE place, String detail,
			String message, Throwable original) {
		super(message, original);
		this.level = level;
		this.kind = kind;
		this.errorCause = errorCause;
		this.place = place;
		this.detail = detail;
	}
}
