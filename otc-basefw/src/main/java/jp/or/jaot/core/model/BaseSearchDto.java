package jp.or.jaot.core.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import jp.or.jaot.core.model.BaseEntity.ORDER_ASC;
import lombok.Getter;
import lombok.Setter;

/**
 * 検索DTOの基底クラス
 */
@Getter
@Setter
public abstract class BaseSearchDto {

	/**
	 * 抽出条件の列挙体
	 */
	public enum MATCH_CONDITION {

		/** 前方 */
		FWD(1),
		/** 後方 */
		BWD(2),
		/** 完全 */
		EQ(3),
		/** 部分 */
		PAR(4),
		/** 指定なし */
		NONE(5),
		/** 空白 */
		BLANK(6);

		/** 値 */
		private final Integer value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private MATCH_CONDITION(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static MATCH_CONDITION valueOf(Integer value) {
			return Arrays.stream(MATCH_CONDITION.values()).filter(V -> V.getValue().equals(value)).findFirst()
					.orElse(null);
		}
	}

	/**
	 * 分類条件の列挙体
	 */
	public enum CLASS_CONDITION {

		/** 含む */
		OR(1),
		/** 含まない */
		NOT(2),
		/** AND */
		AND(3);

		/** 値 */
		private final Integer value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private CLASS_CONDITION(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static CLASS_CONDITION valueOf(Integer value) {
			return Arrays.stream(CLASS_CONDITION.values()).filter(V -> V.getValue().equals(value)).findFirst()
					.orElse(null);
		}
	}

	/**
	 * 範囲条件の列挙体
	 */
	public enum RANGE_CONDITION {

		/** 以上 */
		GE(1),
		/** 以下 */
		LE(2),
		/** ゼロ */
		ZERO(3);

		/** 値 */
		private final Integer value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private RANGE_CONDITION(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static RANGE_CONDITION valueOf(Integer value) {
			return Arrays.stream(RANGE_CONDITION.values()).filter(V -> V.getValue().equals(value)).findFirst()
					.orElse(null);
		}
	}

	/**
	 * 分類区分の列挙体
	 */
	public enum CATEGORY_DIVISION {

		/** 大分類 */
		LARGE(1),
		/** 中分類 */
		MIDDLE(2),
		/** 小分類 */
		SMALL(3);

		/** 値 */
		private final Integer value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private CATEGORY_DIVISION(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static CATEGORY_DIVISION valueOf(Integer value) {
			return Arrays.stream(CATEGORY_DIVISION.values()).filter(V -> V.getValue().equals(value)).findFirst()
					.orElse(null);
		}
	}

	/**
	 * 検索ソート順序クラス
	 */
	public static class SearchOrder {

		/** ソート順序 */
		private ORDER_ASC order;

		/** ソート対象カラム名 */
		private String column;

		/**
		 * ソート順序取得
		 * @return ソート順序
		 */
		public ORDER_ASC getAsc() {
			return order;
		}

		/**
		 * ソート対象カラム名取得
		 * @return ソート対象カラム名
		 */
		public String getColumn() {
			return column;
		}

		/**
		 * コンストラクタ
		 * @param order ソート順序
		 * @param column ソート対象カラム名
		 */
		public SearchOrder(ORDER_ASC order, String column) {
			this.order = order;
			this.column = column;
		}
	}

	/** ソート順序 */
	private List<SearchOrder> orders = new ArrayList<SearchOrder>();

	/** 検索結果の開始位置(初期位置:0) */
	private int startPosition = 0;

	/** 検索結果の取得件数(初期件数:0(指定なし)) */
	private int maxResult = 0;

	/** 検索結果の件数 */
	private long resultCount = 0L;

	/** 検索結果の件数(全体) */
	private long allResultCount = 0L;

	/** 削除フラグ使用(true=使用, false=不使用) */
	private boolean useDeleted = true;

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
