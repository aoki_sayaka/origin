package jp.or.jaot.core.model;

import lombok.Getter;

/**
 * ログ情報(要素)を保持するDTO
 */
@Getter
public class LoggingElementDto {

	/** フィールド型 */
	private final String fieldType;

	/** フィールド名 */
	private final String fieldName;

	/** 変更前 */
	private final String before;

	/** 変更後 */
	private final String after;

	/**
	 * コンストラクタ
	 * @param fieldType フィールド型
	 * @param fieldName フィールド名
	 * @param before 変更前
	 * @param after 変更後
	 */
	public LoggingElementDto(String fieldType, String fieldName, String before, String after) {
		this.fieldType = fieldType;
		this.fieldName = fieldName;
		this.before = before;
		this.after = after;
	}
}
