package jp.or.jaot.core.exception.common;

import org.springframework.util.MultiValueMap;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_KIND;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_LEVEL;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import lombok.Getter;

/**
 * バリデーション例外の基底クラス
 */
@Getter
public abstract class ValidationException extends BaseException {

	/** エラー */
	private MultiValueMap<String, String> errorMap;

	/**
	 * コンストラクタ
	 * @param level エラー種別
	 * @param kind エラー内容
	 * @param errorCause エラー原因
	 * @param place エラー発生箇所
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 * @param errorMap エラーマップ
	 */
	public ValidationException(ERROR_LEVEL level, ERROR_KIND kind, ERROR_CAUSE errorCause, ERROR_PLACE place,
			String detail, String message, MultiValueMap<String, String> errorMap) {
		super(level, kind, errorCause, place, detail, message, null);
		this.errorMap = errorMap;
	}
}
