package jp.or.jaot.core.domain;

/**
 * エンティティを主管する事を表すインターフェース
 * @param <E> エンティティ
 */
public interface EntityAware<E> {

	/**
	 * エンティティをセットする
	 * @param entity エンティティ
	 */
	void setEntity(E entity);

	/**
	 * エンティティを返す
	 * @return エンティティ
	 */
	E getEntity();

}
