package jp.or.jaot.core.util;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 文字列に関するユーティリティクラス
 */
public class StringUtil {

	/** 改行文字(CR+LF) */
	public static final String CRLF = "\r\n";

	/**
	 * 空文字列が入力された場合はnullに変換する。それ以外は、そのまま入力された値を返す。
	 * @param string 入力文字列
	 * @return 変換後の文字列
	 */
	public static String emptyToNull(String string) {
		if (string.length() == 0) {
			return null;
		}
		return string;
	}

	/**
	 * Long値を文字列に変換する。nullが入力されたときはnullが返る。
	 * @param value 変換する値
	 * @return 変換した文字列
	 */
	public static String toString(Long value) {
		if (value == null) {
			return null;
		}
		return value.toString();
	}

	/**
	 * Integer値を文字列に変換する。nullが入力されたときはnullが返る。
	 * @param value 変換する値
	 * @return 変換した文字列
	 */
	public static String toString(Integer value) {
		if (value == null) {
			return null;
		}
		return value.toString();
	}

	/**
	 * Boolean値を文字列に変換する。nullが入力された場合はnullが返る。
	 * @param value 変換する値
	 * @return null, "true", "false"のいずれか
	 */
	public static String toString(Boolean value) {
		if (value == null) {
			return null;
		}

		if (value) {
			return "true";
		} else {
			return "false";
		}
	}

	/**
	 * nullか空文字列ならtrueを返す
	 * @param string 入力文字列
	 * @return Emptyならtrue
	 */
	public static boolean isEmpty(String string) {
		if ((string == null) || (string.length() == 0)) {
			return true;
		}
		return false;
	}

	/**
	 * 日付を日付文字列に変換する。
	 * @param date 日付
	 * @return 日付文字列
	 */
	public static String fromDate(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");
		String result = sf.format(date);
		return result;
	}

	/**
	 * 日付を日付文字列に変換する。
	 * @param date 日付
	 * @return 日付文字列
	 */
	public static String fromDateTime(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String result = sf.format(date);
		return result;
	}

	/**
	 * Long値を文字列に変換する。nullが入力された場合はnullを返す。
	 * @param value 値
	 * @return 文字列に変換した値
	 */
	public static String fromLong(Long value) {
		if (value == null) {
			return null;
		}
		return value.toString();
	}

	/**
	 * 前方一致のワイルドカードを挿入する
	 * @param string 検索値
	 * @return ワイルドカードが挿入された検索値
	 */
	public static String toBeginWithMatch(String string) {
		if (string == null) {
			return string;
		}
		return escapeToLikeClause(string) + "%";
	}

	/**
	 * 部分一致のワイルドカードを挿入する
	 * @param string 検索値
	 * @return ワイルドカードが挿入された検索値
	 */
	public static String toPartialMatch(String string) {
		if (string == null) {
			return string;
		}
		return "%" + escapeToLikeClause(string) + "%";
	}

	/**
	 * ワイルドカード文字をエスケープする<br>
	 * ￥→￥￥<br>
	 * ％→￥％<br>
	 * ＿→￥＿<br>
	 * @param string
	 * @return
	 */
	public static String escapeToLikeClause(String string) {
		if (string == null) {
			return string;
		}

		return string.replace("\\", "\\\\").replace("%", "\\%").replace("_", "\\_");
	}

	/**
	 * null文字列を空文字列("")に変換する。null以外が渡された場合は、そのまま返す
	 * @param string 入力文字列
	 * @return 変換された文字列
	 */
	public static String nullToEmpty(String string) {
		return (string != null) ? string : "";
	}

	/**
	 * 文字列を二重引用符(")で囲んで返す。入力文字列に二重引用符が含まれる場合、2連続の二重引用符に変換する<br>
	 * nullが渡された場合は二重引用符で囲まず、空文字列を返す<br>
	 * CSVファイルに文字列を出力する場合に使用されることを想定<br>
	 * @param string 入力文字列
	 * @return 変換された文字列
	 */
	public static String toDoubleQuotedString(String string) {
		if (string == null) {
			return "";
		}
		return "\"" + string.replace("\"", "\"\"") + "\"";
	}

	/**
	 * StringBuilderインスタンスにオブジェクトを追加（追記）する
	 * @param builder StringBuilderのインスタンス
	 * @param obj 追加（追記）するオブジェクト
	 * @param delimiter 区切り文字
	 */
	public static void appendObjectWithDelimiter(StringBuilder builder, Object obj, String delimiter) {
		if (builder != null) {
			if (builder.length() > 0 && delimiter != null) {
				builder.append(delimiter);
			}
			builder.append(obj);
		}
	}

	/**
	 * 文字列配列を区切り文字を入れて連結させる<br>
	 * @param objects 文字列配列
	 * @param delimiter 区切り文字
	 * @return 区切り文字つなぎの文字列
	 */
	public static String appendStringArrayWithDelimiter(List<String> objects, String delimiter) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < objects.size(); i++) {
			appendObjectWithDelimiter(builder, nullToEmpty(objects.get(i)), delimiter);
		}
		return builder.toString();
	}

	/**
	 * Shift-JIS（MS932:CP932のMicrosoft拡張）Charsetを取得する
	 * @return Charsetオブジェクト
	 */
	public static Charset getMS932Charset() {
		return Charset.forName("MS932");
	}

	/**
	 * 文字列のエスケープを処理する
	 * @param string 変換前文字列
	 * @return 変換後文字列
	 */
	public static String escapeString(String string) {
		return string.replaceAll("\\\\", "\\\\\\\\");
	}

	/**
	 * 文字列の配列を 1 つのパスに結合
	 * @param args パスの構成要素の配列
	 * @return 結合されたパス
	 */
	public static String combinePath(String... args) {
		String path = "";
		for (String arg : args) {
			path = FilenameUtils.concat(path, arg);
		}
		return path;
	}

	/**
	 * キャメルケース -> スネークケース変換<br>
	 * 例：abcDef -> abc_def
	 * @param camel キャメルケース
	 * @return スネークケース(全て小文字)
	 */
	public static final String camelToSnake(final String camel) {
		if (StringUtils.isEmpty(camel)) {
			return "";
		}
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < camel.length(); i++) {
			final char c = camel.charAt(i);
			if (Character.isUpperCase(c)) {
				sb.append(sb.length() != 0 ? '_' : "").append(Character.toLowerCase(c));
			} else {
				sb.append(Character.toLowerCase(c));
			}
		}
		return sb.toString();
	}

	/**
	 * スネークケース -> キャメルケース変換<br>
	 * 例：abc_def(ABC_DEF) -> abcDef
	 * @param snake スネークケース
	 * @return キャメルケース
	 */
	public static final String snakeToCamel(final String snake) {
		if (StringUtils.isEmpty(snake)) {
			return "";
		}
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < snake.length(); i++) {
			final char c = snake.charAt(i);
			if ('_' == c) {
				sb.append((i + 1) < snake.length() ? Character.toUpperCase(snake.charAt(++i)) : "");
			} else {
				sb.append(Character.toLowerCase(c));
			}
		}
		return sb.toString();
	}
}
