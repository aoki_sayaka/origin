package jp.or.jaot.core.handler;

import org.springframework.http.HttpStatus;

import jp.or.jaot.core.handler.common.BaseExceptionHandler;
import jp.or.jaot.model.dto.error.ErrorInfoDto;

/**
 * 業務エラーを処理するハンドラクラス
 */
public class BusinessErrorHandler extends BaseExceptionHandler {

	/**
	 * コンストラクタ<br>
	 * @param errorInfoDto エラー情報DTO
	 */
	public BusinessErrorHandler(ErrorInfoDto errorInfoDto) {
		this.errorInfoDto = errorInfoDto;
		this.httpStatus = HttpStatus.OK; // 正常終了
	}

	@Override
	protected void outputLog(ErrorInfoDto errorInfo) {
		logger.info("業務エラーが発生しました : {}", errorInfo.getErrorCode());
		logger.info(errorInfo.getWorkaround());
		logger.info(errorInfo.getDetail());
	}

	@Override
	protected String getViewName() {
		return "otError";
	}
}
