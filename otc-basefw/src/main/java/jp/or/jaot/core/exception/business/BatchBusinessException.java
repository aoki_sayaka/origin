package jp.or.jaot.core.exception.business;

import jp.or.jaot.core.exception.common.BusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_KIND;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_LEVEL;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import lombok.Getter;

/**
 * バッチ業務例外のクラス
 */
@Getter
public class BatchBusinessException extends BusinessException {

	/**
	 * コンストラクタ
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 * @param original オリジナルの例外
	 */
	public BatchBusinessException(String detail, String message, Throwable original) {
		super(ERROR_LEVEL.INFO, ERROR_KIND.BUSINESS, ERROR_CAUSE.CODE, ERROR_PLACE.BATCH, detail, message, original);
	}

	/**
	 * コンストラクタ
	 * @param place 発生箇所(ERROR_PLACE.BATCH_*)
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 */
	public BatchBusinessException(ERROR_PLACE place, String detail, String message) {
		super(ERROR_LEVEL.INFO, ERROR_KIND.BUSINESS, ERROR_CAUSE.CODE, place, detail, message, null);
	}

	/**
	 * コンストラクタ
	 * @param errorCause エラー原因(ERROR_CAUSE.CODE_*)
	 * @param place 発生箇所(ERROR_PLACE.BATCH_*)
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 */
	public BatchBusinessException(ERROR_CAUSE errorCause, ERROR_PLACE place, String detail, String message) {
		super(ERROR_LEVEL.INFO, ERROR_KIND.BUSINESS, errorCause, place, detail, message, null);
	}
}
