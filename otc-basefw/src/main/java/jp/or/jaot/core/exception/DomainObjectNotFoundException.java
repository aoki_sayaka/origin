package jp.or.jaot.core.exception;

/**
 * ドメインの取得や生成に失敗した場合にスローされる例外クラス
 */
public class DomainObjectNotFoundException extends RuntimeException {

	/**
	 * ドメインクラスオブジェクト
	 */
	private final Class<?> domainClass;

	/**
	 * コンストラクタ
	 * @param domainClass ドメインクラスオブジェクト
	 */
	public DomainObjectNotFoundException(Class<?> domainClass) {
		super();
		this.domainClass = domainClass;
	}

	/**
	 * 取得できなかったドメインクラスオブジェクトを取得する
	 * @return ドメインクラスオブジェクト
	 */
	public Class<?> getDomainClass() {
		return domainClass;
	}

}
