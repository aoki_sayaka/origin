package jp.or.jaot.core.domain;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.model.BaseEntity;
import jp.or.jaot.core.model.Entity;
import jp.or.jaot.core.model.LoggingUpdateEntityDto;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.core.util.SpringContext;

/**
 * リポジトリの基底クラス
 * @param <D> リポジトリが取り扱うドメインクラス
 * @param <E> リポジトリが取り扱うエンティティクラス
 */
public abstract class BaseRepository<D extends BaseDomain<E>, E> {

	/** ロガーインスタンス */
	protected final Logger logger;

	/** Springコンテキスト */
	@Autowired
	protected SpringContext springContext;

	/** BeanFactory */
	@Autowired
	protected AutowireCapableBeanFactory beanFactory;

	/** エラー発生箇所 */
	protected final ERROR_PLACE errorPlace;

	/** DAO */
	private final BaseDao<E> baseDao;

	/**
	 * コンストラクタ
	 * @param errorPlace エラー発生個所
	 * @param baseDao DAO
	 */
	public BaseRepository(ERROR_PLACE errorPlace, BaseDao<E> baseDao) {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
		this.errorPlace = errorPlace;
		this.baseDao = baseDao;
	}

	/**
	 * 新規にドメインオブジェクトを構築し、Autowireを行って返す。
	 * @return 生成したドメインオブジェクト
	 */
	protected D newDomain() {
		@SuppressWarnings("unchecked")
		D domain = (D) (beanFactory.createBean(//
				getParameterDomainClass(), //
				AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, false));
		return domain;
	}

	/**
	 * ドメインオブジェクトを構築し、エンティティを詰めて返す。
	 * @param entity エンティティのオブジェクト
	 * @return 構築したドメインオブジェクト
	 */
	protected D createDomain(E entity) {
		D domain = this.newDomain();
		domain.setEntity(entity);
		return domain;
	}

	/**
	 * 型パラメータとして指定されたドメインクラスを返す。<br/>
	 * ドメインクラスが取得出来ない場合は例外をスローする。
	 * @return 型パラメータのドメインクラス
	 */
	private Class<?> getParameterDomainClass() {
		Class<?> repositoryClass = this.getClass();
		while (repositoryClass != null) {
			Type type = repositoryClass.getGenericSuperclass();
			Type[] types = getParameterTypesIfClassMatch(type);
			if (types != null) {
				return (Class<?>) types[0];
			}
			repositoryClass = repositoryClass.getSuperclass();
		}
		throw new Error("ドメインクラスが特定できません。");
	}

	/**
	 * 指定のTypeがリポジトリ基底クラスと一致した場合、その型パラメータを返す。<br/>
	 * 型パラメータが取得出来ない場合はnullを返す。
	 * @param 型パラメータを取得するクラスのType
	 * @return リポジトリ基底クラスの型パラメータ
	 */
	private Type[] getParameterTypesIfClassMatch(Type type) {
		if (type != null //
				&& type instanceof ParameterizedType //
				&& BaseRepository.class.equals(((ParameterizedType) type).getRawType())) {
			return ((ParameterizedType) type).getActualTypeArguments();
		}
		return null;
	}

	/**
	 * エンティティ追加
	 * @param entity 追加するエンティティ
	 * @return 追加したエンティティ
	 */
	public BaseEntity insertEntity(BaseEntity entity) {
		return baseDao.insertEntity(entity);
	}

	/**
	 * エンティティ更新
	 * @param entity 更新するエンティティ
	 * @param primaryKey プライマリキーの値
	 * @return 更新エンティティログ情報DTO
	 */
	public LoggingUpdateEntityDto updateEntity(BaseEntity entity, Long primaryKey) {
		return baseDao.updateEntity(entity, primaryKey);
	}

	/**
	 * エンティティ削除(物理)
	 * @param entity 削除するエンティティ
	 * @param primaryKey プライマリキーの値
	 */
	public void deleteEntity(BaseEntity entity, Long primaryKey) {
		baseDao.deleteEntity(entity, primaryKey);
	}

	/**
	 * 追加
	 * @param entity エンティティ
	 */
	public void insert(Entity entity) {
		baseDao.insert(entity);
	}

	/**
	 * 更新
	 * @param entity エンティティ
	 */
	public void update(Entity entity) {
		baseDao.update(entity);
	}

	/** 削除(物理)
	 * @param entity エンティティ
	 */
	public void delete(Entity entity) {
		baseDao.delete(entity);
	}

	/**
	 * 使用可能, 使用不可状態設定
	 * @param id ID
	 * @param updateUser 更新者
	 * @param enabled 状態(true=使用可能, false=使用不可)
	 */
	public void setEnable(Long id, String updateUser, boolean enabled) {
		baseDao.setEnableEntity(id, updateUser, enabled);
	}

	/**
	 * プライマリキーを指定してエンティティを1件取得する
	 * @param primaryKey プライマリキーの値
	 * @param isExclusiveLock 排他ロックフラグ(true=排他ロックあり, false=排他ロックなし)
	 * @return エンティティ
	 */
	public E find(Long primaryKey, boolean isExclusiveLock) {
		return baseDao.find(primaryKey, isExclusiveLock);
	}

	/**
	 * プライマリキーを指定してドメインを1件取得する
	 * @param primaryKey プライマリキーの値
	 * @param isExclusiveLock 排他ロックフラグ(true=排他ロックあり, false=排他ロックなし)
	 * @return ドメイン
	 */
	public D findDomain(Long primaryKey, boolean isExclusiveLock) {
		return this.createDomain(this.find(primaryKey, isExclusiveLock));
	}
}
