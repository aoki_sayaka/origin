package jp.or.jaot.core.handler;

import org.springframework.http.HttpStatus;

import jp.or.jaot.core.handler.common.BaseExceptionHandler;
import jp.or.jaot.model.dto.error.ErrorInfoDto;

/**
 * バリデーションエラーを処理するハンドラクラス
 */
public class ValidationErrorHandler extends BaseExceptionHandler {

	/**
	 * コンストラクタ<br>
	 * @param errorInfoDto エラー情報DTO
	 */
	public ValidationErrorHandler(ErrorInfoDto errorInfoDto) {
		this.errorInfoDto = errorInfoDto;
		this.httpStatus = HttpStatus.BAD_REQUEST; // リクエストエラー
	}

	@Override
	protected void outputLog(ErrorInfoDto errorInfo) {
		logger.warn("バリデーションエラーが発生しました : {}", errorInfo.getErrorCode());
		logger.warn(errorInfo.getDetailJson());
	}

	@Override
	protected String getViewName() {
		return "otError";
	}
}
