package jp.or.jaot.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.or.jaot.core.exception.common.SystemException;

/**
 * 日付型に関するユーティリティクラス
 */

public class DateUtil {

	/**
	 * ロガーインスタンス
	 */
	private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

	/**
	 * 日にちを足す
	 * @param date 元の日付
	 * @param day 追加する日数
	 * @return 追加された日付
	 */
	public static Date addDay(Date date, Integer day) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.add(Calendar.DATE, day);
		return calender.getTime();
	}

	/**
	 * 月を足す
	 * @param date 元の日付
	 * @param day 追加する日数
	 * @return 追加された日付
	 */
	public static Date addMonth(Date date, Integer month) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.add(Calendar.MONTH, month);
		return calender.getTime();
	}

	/**
	 * 時間（H)を足す
	 * @param date 元の日付
	 * @param day 追加する日数
	 * @return 追加された日付
	 */
	public static Date addHour(Date date, Integer hour) {
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.add(Calendar.HOUR, hour);
		return calender.getTime();
	}

	/**
	 * 文字列を日付型に変換する
	 * @param date
	 * @return
	 */
	public static Date parseDate(String dates) {
		if (dates == null) {
			return null;
		}
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");
			Date date = sf.parse(dates);
			return date;
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * 文字列を日付型に変換する(DB形式)
	 * @param date
	 * @return
	 */
	public static Date parseDateDB(String dates) {
		if (dates == null) {
			return null;
		}
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sf.parse(dates);
			return date;
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * 文字列を時刻まで保持した日付型に変換する
	 * @param string 日時を表す文字列
	 * @return
	 */
	public static Date parseDateTime(String string) {
		if (StringUtil.isEmpty(string)) {
			return null;
		}
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = sf.parse(string);
			return date;
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * 文字列を時刻まで保持した日付型に変換する
	 * @param string 日時を表す文字列
	 * @return
	 */
	public static Date parseDateTimeDB(String string) {
		if (StringUtil.isEmpty(string)) {
			return null;
		}
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = sf.parse(string);
			return date;
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}
	/**
	 * 文字列を時刻に変換する
	 * @param string 時刻を表す文字列
	 * @return
	 */
	public static Date parseTimeDB(String string) {
		if (StringUtil.isEmpty(string)) {
			return null;
		}
		try {
			SimpleDateFormat sf = new SimpleDateFormat("HH:mm");
			Date date = sf.parse(string);
			return date;
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * 文字列を時刻まで保持した日付型に変換する(UNIX形式)
	 * @param string 日時を表す文字列
	 * @return
	 */
	public static Date parseDateTimeUnix(String string) {
		if (StringUtil.isEmpty(string)) {
			return null;
		}
		try {
			SimpleDateFormat sf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS");
			Date date = sf.parse(string);
			return date;
		} catch (ParseException e) {
			logger.info(e.getMessage());
			return null;
		}
	}

	/**
	 * 文字列を時刻まで保持した日付型に変換する(海外形式)
	 * @param string 日時を表す文字列
	 * @return
	 */
	public static Date parseDateAbroad(String string) {
		if (StringUtil.isEmpty(string)) {
			return null;
		}
		try {
			SimpleDateFormat sf = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy", Locale.ENGLISH);
			Date date = sf.parse(string);
			return date;
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * yyyy/mm形式の文字列を日付型に変換する
	 * @param datem yyyy/mm形式の文字列
	 * @return 日付
	 */
	public static Date parseDatem(String datem) {
		if (datem == null) {
			return null;
		}
		String dates = datem + "/01";
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");
			Date date = sf.parse(dates);
			return date;
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * DBで読み取れる形(yyyy-mm-dd)に変換する
	 * @param date 日付
	 * @return 日付文字列
	 */
	public static String formatDBString(Date date) {
		if (null == date) {
			return null;
		}
		String date_pattern = "yyyy-MM-dd";
		return (new SimpleDateFormat(date_pattern)).format(date);
	}

	/**
	 * DBで読み取れる形(yyyymm)に変換する
	 * @param date 日付
	 * @return 日付文字列
	 */
	public static String formatYmDBString(Date date) {
		if (null == date) {
			return null;
		}
		String date_pattern = "yyyyMM";
		return (new SimpleDateFormat(date_pattern)).format(date);
	}

	/**
	 * ミリ秒を落としたシステム時間を取得する。
	 * @param date 日付
	 * @return システム日時（ミリ秒なし）
	 */
	public static Date getSystemDate() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.MILLISECOND, 0);
		return new Date(cal.getTimeInMillis());
	}

	/**
	 * 時分秒を0に設定した現在日付を返す<br>
	 * 現在時とDate型の比較の時に精度を揃えるために使う
	 * @return 時分秒が0になった現在日
	 */
	public static Date getSystemDateZeroHour() {
		// 時分秒をtrucateする
		return DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
	}

	/**
	 * ミリ秒を含むシステム時間を取得する。
	 * @param date 日付
	 * @return システム日時（ミリ秒なし）
	 */
	public static Date getSystemDateMilli() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return new Date(cal.getTimeInMillis());
	}

	/**
	 * ミリ秒をHH:MM:ss SSS形式にする
	 * @param time
	 * @return
	 */
	public static String formMillisecondHMS(long millis) {
		long hour = (millis / (1000 * 60 * 60)) % 24;
		long minute = (millis / (1000 * 60)) % 60;
		long second = (millis / 1000) % 60;
		long millisSec = millis % 1000;

		String time = String.format("%02d:%02d:%02d:%03d", hour, minute, second, millisSec);
		return time;
	}

	/**
	 * 引数のDateの時分秒を0にした値を返す<br>
	 * @param d 時分秒が含まれる日付
	 * @return 時分秒が0になった日付
	 */
	public static Date getDateZeroHour(Date d) {
		return DateUtils.truncate(d, Calendar.DAY_OF_MONTH);
	}

	/**
	 * formatと変換対象のdateを与えるとformat変換して結果を返す<br>
	 * 変換できない場合は"-"を返す
	 * @param format
	 * @param date
	 * @return 引数のformatで整形した日付文字列
	 * @throws SystemException
	 */
	public static String dateFormat(String format, Date date) {
		SimpleDateFormat sd = new SimpleDateFormat(format);
		String data;
		try {
			data = sd.format(date);
		} catch (Exception e) {
			return "-";
		}
		return data;
	}

	/**
	 * メソッド概要<br>
	 * yyyy/MM/dd形式の日付文字列と時間から、日付型に変換する
	 * @param dates yyyy/MM/dd 形式の文字列
	 * @param hour HH形式の文字列(24時間表記)
	 * @return 日付
	 */
	public static Date parseDateTime(String dates, String hour) {
		// 引数チェック
		if (dates == null) {
			return null;
		}
		// 日付書式に合わせた文字列を作成する
		StringBuilder builder = new StringBuilder();
		builder.append(dates);
		builder.append(" ");
		builder.append(hour);
		builder.append(":00:00");
		try {
			SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = sf.parse(builder.toString());
			return date;
		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * 現在日から年度を取得して返す<br>
	 * @return 現在年度
	 */
	public static Integer getCurrentFiscalYear() {
		return getFiscalYear(new Date());
	}

	/**
	 * 引数の日付から年度を取得して返す<br>
	 * @param date 年度を取得する日付
	 * @return 年度
	 */
	public static Integer getFiscalYear(Date date) {
		if (date == null) {
			return null;
		}

		// 引数の日付から年と月を取り出す
		Calendar calDate = GregorianCalendar.getInstance();
		calDate.setTime(date);
		Integer year = calDate.get(Calendar.YEAR);
		Integer month = calDate.get(Calendar.MONTH) + 1;// Calendar.Monthは実際の月は＋１する。

		// 1月～3月の場合は前年を返す
		if (month < 4) {
			return year - 1;
		}

		// 4月から12月は当年を返す
		return year;
	}

	/**
	 * 引数の日付から年度を取得して返す<br>
	 * @param String YYYYMM
	 * @return 年度
	 */
	public static Integer getFiscalYear(String YYYYMM) {

		if (StringUtil.isEmpty(YYYYMM)) {
			Integer year = Integer.parseInt(YYYYMM.substring(0, 4));
			Integer month = Integer.parseInt(YYYYMM.substring(4, 6));

			// 1月～3月の場合は前年を返す
			if (month < 4) {
				return year - 1;
			}

			// 4月から12月は当年を返す
			return year;
		}
		return null;

	}

	/**
	 * 引数の日付からその年度の月数を取得して返す<br>
	 * @param date 月数を取得する日付
	 * @return 月数
	 */
	public static Integer getFiscalYearMonthCount(Date date) {
		// 引数の日付から年と月を取り出す
		SimpleDateFormat sdfMonth = new SimpleDateFormat("M");
		Integer month = Integer.parseInt(sdfMonth.format(date));

		Integer checkMonth = month;

		// 1月～3月の場合は12を足す
		if (month <= 3) {
			checkMonth = checkMonth + 12;
		}
		// (4月+12カ月)-checkMonth
		return 16 - checkMonth;
	}

	/**
	 * 指定した年度の年月日を取得
	 * @param year
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static Date getFiscalYearDate(Integer year, Date date) throws ParseException {
		SimpleDateFormat sdfMonth = new SimpleDateFormat("M");
		Integer month = Integer.parseInt(sdfMonth.format(date));
		SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
		Integer day = Integer.parseInt(sdfDay.format(date));

		Integer setYear = year;
		if (month <= 3) {
			setYear += 1;
		}

		SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");
		Date setDate = sf.parse(setYear.toString() + "/" + month.toString() + "/" + day.toString());

		return setDate;
	}

	/**
	 * 現在日時から年度開始月をYYYYMMで返す
	 * @return YYYYMM
	 */
	public static String getStartFiscalYearMonth() {
		Integer year = getCurrentFiscalYear();
		return year + "04";
	}

	/**
	 * 現在日時から年度終了月をYYYYMMで返す
	 * @return YYYYMM
	 */
	public static String getEndFiscalYearMonth() {
		Integer year = getCurrentFiscalYear();
		return (year + 1) + "03";
	}

	/**
	 * 引数数から年度の中で何カ月目かを返す<br>
	 * 201604⇒1<br>
	 * 201701⇒10<br>
	 * @param YYYYMMの文字列
	 * @return 年度の中で何カ月目
	 */
	public static Integer getFiscalYearMonthToCount(String YYYYMM) {

		Integer month = Integer.valueOf(YYYYMM.substring(4));

		// 1月～3月の場合は9を足す
		if (month <= 3) {
			return month + 9;
		}

		return month - 3;
	}

	/**
	 * YYYYMMDDの文字列をYYYY/MM/DDに変換して返す
	 * @param YYYYMMDD 変換したい文字列
	 * @return 変換結果
	 */
	public static String getDateString_YYYYMMDD(String YYYYMMDD) {

		if (!StringUtil.isEmpty(YYYYMMDD)) {
			String str = YYYYMMDD.substring(0, 4) + "/" + YYYYMMDD.substring(4, 6) + "/" + YYYYMMDD.substring(6, 8);
			return str;
		}
		return null;

	}

	/**
	 * 現在日が間もなく翌年度に切り替わるタイミングなら<br>
	 * 3月10日以上ならtrue それ以外false
	 * @return Boolean 3月10日以上ならtrue それ以外false
	 */
	public static Boolean isShortlyNextFiscalYear(Integer switchDay) {
		SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
		SimpleDateFormat sdfday = new SimpleDateFormat("dd");

		Date nowDate = new Date();
		Integer month = Integer.parseInt(sdfMonth.format(nowDate));
		Integer day = Integer.parseInt(sdfday.format(nowDate));

		// ３月であること
		if (month == 3) {
			// １０日以上であること
			// 切替日は、リソース設定より取得
			if (day >= switchDay) {
				return true;
			}
		}
		return false;

	}

	/**
	 * 日付を月初日(1日)にして返す
	 * @return 月初日
	 */
	public static Date getFirstDate(Date date) {

		// エラーチェック
		if (null == date) {
			return null;
		}

		// 変換
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH)); // 日
		c.set(Calendar.HOUR_OF_DAY, c.getActualMinimum(Calendar.HOUR_OF_DAY)); // 時
		c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE)); // 分
		c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND)); // 秒
		return c.getTime();
	}

	/**
	 * 日付を月末日(月の最終日)にして返す
	 * @return 月末日
	 */
	public static Date getLastDate(Date date) {

		// エラーチェック
		if (null == date) {
			return null;
		}

		// 変換
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH)); // 日
		c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY)); // 時
		c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE)); // 分
		c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND)); // 秒
		return c.getTime();
	}

	/**
	 * 日付を初期化(From)して返す
	 * @param date 日付
	 * @return 日付(yyyy-mm-dd 00:00:00)
	 */
	public static Date getFromDate(Date date) {

		// エラーチェック
		if (null == date) {
			return null;
		}

		// 変換
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		return cal.getTime();
	}

	/**
	 * 日付を初期化(To)して返す
	 * @param date 日付
	 * @return 日付(yyyy-mm-dd 23:59:59)
	 */
	public static Date getToDate(Date date) {

		// エラーチェック
		if (null == date) {
			return null;
		}

		// 変換
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal = DateUtils.truncate(cal, Calendar.DAY_OF_MONTH);
		cal.add(Calendar.DATE, 1);
		cal.add(Calendar.MINUTE, -1);
		cal.set(Calendar.SECOND, 59);
		return cal.getTime();
	}

	/**
	 * 日付を月初日文字列にして返す
	 * @return 月初日文字列(yyyy/MM/01)
	 */
	public static String getFirstDateStr(Date date) {
		return StringUtil.fromDate(getFirstDate(date));
	}

	/**
	 * 日付を月末日文字列にして返す
	 * @return 月末日文字列(yyyy/MM/dd)
	 */
	public static String getLastDateStr(Date date) {
		return StringUtil.fromDate(getLastDate(date));
	}

	/**
	 * 時間プルダウン作成用の情報を返す<br>
	 * @return 時間プルダウンのMap
	 */
	public static final Map<String, String> createHoursMap() {
		return Collections.unmodifiableMap(new LinkedHashMap<String, String>() {
			{
				put("00", "00");
				put("01", "01");
				put("02", "02");
				put("03", "03");
				put("04", "04");
				put("05", "05");
				put("06", "06");
				put("07", "07");
				put("08", "08");
				put("09", "09");
				put("10", "10");
				put("11", "11");
				put("12", "12");
				put("13", "13");
				put("14", "14");
				put("15", "15");
				put("16", "16");
				put("17", "17");
				put("18", "18");
				put("19", "19");
				put("20", "20");
				put("21", "21");
				put("22", "22");
				put("23", "23");
			}
		});
	}

	/**
	 * 分、秒プルダウン作成用の情報を返す<br>
	 * @return 分、秒プルダウンのMap
	 */
	public static final Map<String, String> createMinutesSecondsMap() {
		return Collections.unmodifiableMap(new LinkedHashMap<String, String>() {
			{
				put("00", "00");
				put("01", "01");
				put("02", "02");
				put("03", "03");
				put("04", "04");
				put("05", "05");
				put("06", "06");
				put("07", "07");
				put("08", "08");
				put("09", "09");
				put("10", "10");
				put("11", "11");
				put("12", "12");
				put("13", "13");
				put("14", "14");
				put("15", "15");
				put("16", "16");
				put("17", "17");
				put("18", "18");
				put("19", "19");
				put("20", "20");
				put("21", "21");
				put("22", "22");
				put("23", "23");
				put("24", "24");
				put("25", "25");
				put("26", "26");
				put("27", "27");
				put("28", "28");
				put("29", "29");
				put("30", "30");
				put("31", "31");
				put("32", "32");
				put("33", "33");
				put("34", "34");
				put("35", "35");
				put("36", "36");
				put("37", "37");
				put("38", "38");
				put("39", "39");
				put("40", "40");
				put("41", "41");
				put("42", "42");
				put("43", "43");
				put("44", "44");
				put("45", "45");
				put("46", "46");
				put("47", "47");
				put("48", "48");
				put("49", "49");
				put("50", "50");
				put("51", "51");
				put("52", "52");
				put("53", "53");
				put("54", "54");
				put("55", "55");
				put("56", "56");
				put("57", "57");
				put("58", "58");
				put("59", "59");
			}
		});
	}

	/**
	 *
	 */

	/**
	 * ミリ秒を時分秒へ変換
	 * @param ms ミリ秒
	 * @return hh:mm:ss 文字列
	 */
	public static String getUnixDateToTime(Long ms) {

		if (ms <= 0) {
			return "";
		}

		Long h, m, s;

		h = ms / 3600000;
		ms %= 3600000;

		m = ms / 60000;
		ms %= 60000;

		s = ms / 1000;

		return String.valueOf(h) + ':' + String.valueOf(m) + ':' + String.valueOf(s);
	}

	/**
	 * 当月の年月を取得
	 * @return YearDtoのリスト
	 */
	public static String getThisMonthYm() {
		// 現在の年度を取得
		Date date = new Date();
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMM");
		String yearString = sf.format(date);

		return yearString;
	}

	/**
	 * システム日付から指定日数、加算した日付
	 * @param days
	 * @return
	 */
	public static Date getNowPastDate(Integer days) {

		// 当日
		Date nowDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH); // 時分を切り捨て

		Calendar cal = Calendar.getInstance();
		cal.setTime(nowDate);
		cal.add(Calendar.DAY_OF_MONTH, days);

		return DateUtils.truncate(cal.getTime(), Calendar.DAY_OF_MONTH);
	}

	/**
	 * ２つの日付の差分（時間）を取得
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static long differenceHours(Date date1, Date date2) {
		long datetime1 = date1.getTime();
		long datetime2 = date2.getTime();
		long one_date_time = 1000 * 60 * 60;
		long diffDays = (datetime1 - datetime2) / one_date_time;
		return diffDays;
	}

	/**
	 * 時間の加算
	 * @param date
	 * @param hours
	 * @return
	 */
	public static Date addHours(Date date, Integer hours) {
		Date addDate = new Date(date.getTime() + 1000 * 60 * 60 * hours);

		return addDate;
	}

	/**
	 * DBのDate形式（yyyy-MM-dd HH:mm:ss）によるDate型に変換
	 * @param date
	 * @return
	 */
	public static Date formatDBDate(Date date) {

		try {
			Date dbDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(formatDBTimeString(date));
			return dbDate;

		} catch (ParseException e) {
			logger.error(e.getMessage());
			return null;
		}
	}

	/**
	 * DBで読み取れる形(yyyy-mm-dd HH:mm:ss)に変換する
	 * @param date 日付
	 * @return 日付文字列
	 */
	public static String formatDBTimeString(Date date) {
		if (null == date) {
			return null;
		}
		String date_pattern = "yyyy-MM-dd HH:mm:ss";
		return (new SimpleDateFormat(date_pattern)).format(date);
	}

	/**
	 * DBで読み取れる形(yyyy/MM/dd)に変換する
	 * @param date 日付
	 * @return 日付文字列
	 */
	public static String formatYmdString(Date date) {
		if (null == date) {
			return null;
		}
		String date_pattern = "yyyy/MM/dd";
		return (new SimpleDateFormat(date_pattern)).format(date);
	}

	/**
	 * 年月日(曜日)の形(yyyy/MM/dd(E))に変換する
	 * @param date 日付
	 * @return 日付文字列
	 */
	public static String formatYmdeString(Date date) {
		if (null == date) {
			return null;
		}
		String date_pattern = "yyyy/MM/dd(E)";
		return (new SimpleDateFormat(date_pattern)).format(date);
	}
}
