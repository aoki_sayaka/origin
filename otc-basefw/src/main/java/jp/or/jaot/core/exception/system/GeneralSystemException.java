package jp.or.jaot.core.exception.system;

import jp.or.jaot.core.exception.common.SystemException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_KIND;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_LEVEL;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import lombok.Getter;

/**
 * 一般システム例外のクラス
 */
@Getter
public class GeneralSystemException extends SystemException {

	/**
	 * コンストラクタ
	 * @param original オリジナルの例外
	 */
	public GeneralSystemException(Throwable original) {
		super(ERROR_LEVEL.ERROR, ERROR_KIND.SYSTEM, ERROR_CAUSE.UNKNOWN, ERROR_PLACE.COMMON,
				original.getCause().getMessage(), original.getMessage(), original);
	}
}
