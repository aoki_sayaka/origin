package jp.or.jaot.core.model;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

/**
 * ファイルの基底クラス
 */
@Getter
@Setter
public abstract class BaseFile {

	/** ロガーインスタンス */
	protected final Logger logger;

	/** 出力対象 */
	private final String[] targets;

	/**
	 * コンストラクタ
	 */
	public BaseFile(String[] targets) {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
		this.targets = targets;
	}

	/**
	 * 出力対象チェック
	 * @param target チェック対象
	 * @return true=出力対象である、false=出力対象でない
	 */
	public boolean isTarget(String target) {
		return ArrayUtils.contains(targets, target);
	}
}
