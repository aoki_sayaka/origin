package jp.or.jaot.core.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import lombok.Getter;
import lombok.Setter;

/**
 * DTOの基底クラス
 * @param <D> DTOが取り扱うドメインクラス
 * @param <E> DTOが取り扱うエンティティクラス
 */
@Getter
@Setter
public abstract class BaseDto<D, E> {

	/** エンティティリスト */
	private List<E> entities = new ArrayList<E>();

	/** ユーザID */
	private String userId;

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	protected BaseDto() {
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	protected BaseDto(E entity) {
		this.entities.add(entity);
	}

	/**
	 * エンティティリスト設定
	 * @param entities エンティティリスト
	 * @return 自分自身
	 */
	@SuppressWarnings("unchecked")
	public D setEntities(List<E> entities) {
		this.entities.addAll(entities);
		return (D) this;
	}

	/**
	 * エンティティ設定
	 * @param entity エンティティ
	 * @return 自分自身
	 */
	@SuppressWarnings("unchecked")
	public D setEntity(E entity) {
		this.entities.add(entity);
		return (D) this;
	}

	/**
	 * エンティティ取得(先頭)
	 * @return エンティティ
	 */
	public E getFirstEntity() {
		return (CollectionUtils.isNotEmpty(entities)) ? (entities.get(0)) : (null);
	}

	/**
	 * エンティティリスト取得
	 * @return entities エンティティリスト
	 */
	public List<E> getEntities() {

		return this.entities;
	}

	/**
	 * 識別子取得
	 * @return 識別子文字列
	 */
	public String getIdentifier() {
		return this.getClass().getSimpleName();
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
