package jp.or.jaot.core.domain;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.or.jaot.core.exception.system.MailSystemException;
import jp.or.jaot.core.model.MailAttachFileDto;
import jp.or.jaot.core.model.MailContentDto;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.MailUtility;

/**
 * メールドメインの基底クラス
 */
public abstract class BaseMailDomain {

	/** ロガーインスタンス */
	protected final Logger logger;

	/**
	 * コンストラクタ
	 */
	public BaseMailDomain() {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * メール送信処理<br>
	 * @param mailDto メールDTO
	 * @param attachFiles 添付ファイルのリスト
	 * @throws MailSystemException メールシステム例外 (メール送信でシステム例外画発生した場合)
	 */
	protected void sendMail(MailContentDto mailDto, List<MailAttachFileDto> attachFiles) {
		try {
			Boolean response = MailUtility.createInstance().send(mailDto, attachFiles);
			// メール送信で失敗した時は、falseで例外を発生させる。
			if (response == false) {
				ERROR_CAUSE errorCause = ERROR_CAUSE.MAIL_SEND;
				String message = String.format("メール送信に失敗しました");
				String detail = "";
				throw new MailSystemException(errorCause, detail, message, null);
			}
		} catch (Exception e) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.MAIL_SEND;
			String message = String.format("メール送信に失敗しました");
			String detail = "";
			throw new MailSystemException(errorCause, detail, message, e);
		}
	}
}
