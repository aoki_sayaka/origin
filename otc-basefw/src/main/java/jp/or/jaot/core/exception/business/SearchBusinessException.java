package jp.or.jaot.core.exception.business;

import jp.or.jaot.core.exception.common.BusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_KIND;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_LEVEL;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import lombok.Getter;

/**
 * 検索業務例外のクラス
 */
@Getter
public class SearchBusinessException extends BusinessException {

	/**
	 * コンストラクタ
	 * @param place 発生箇所(ERROR_PLACE.LOGIC_*)
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 */
	public SearchBusinessException(ERROR_PLACE place, String detail, String message) {
		super(ERROR_LEVEL.INFO, ERROR_KIND.BUSINESS, ERROR_CAUSE.CODE, place, detail, message, null);
	}

	/**
	 * コンストラクタ
	 * @param errorCause エラー原因(ERROR_CAUSE.CODE_*)
	 * @param place 発生箇所(ERROR_PLACE.LOGIC_*)
	 * @param detail エラー詳細
	 * @param message 例外メッセージ
	 */
	public SearchBusinessException(ERROR_CAUSE errorCause, ERROR_PLACE place, String detail, String message) {
		super(ERROR_LEVEL.INFO, ERROR_KIND.BUSINESS, errorCause, place, detail, message, null);
	}
}
