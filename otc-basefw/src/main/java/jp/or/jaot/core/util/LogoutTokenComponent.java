package jp.or.jaot.core.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

/**
 * ログアウトトークンコンポーネントクラス
 */
@Component
@Scope("prototype")
@AllArgsConstructor
public class LogoutTokenComponent {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(LogoutTokenComponent.class);

	/** トークンマップ(全スレッド共通) */
	private static final Map<String, String> tokenMap = new HashMap<>();

	/**
	 * トークン設定
	 * @param key キー
	 * @param value 値
	 */
	public void setToken(String key, String value) {
		tokenMap.put(key, value);
	}

	/**
	 * トークン取得
	 * @param key キー
	 */
	public String getToken(String key) {
		return tokenMap.get(key);
	}

	/**
	 * トークン存在チェック
	 * @param key キー
	 * @return true=存在する, false=存在しない
	 */
	public boolean containsKey(String key) {
		return tokenMap.containsKey(key);
	}

	/**
	 * トークン正規化<br>
	 * 有効期限切れのトークンを全て除去
	 */
	public void normalizeToken() {
		logger.info("normalize(Before) : size={}", tokenMap.size());
		for (Iterator<String> it = tokenMap.keySet().iterator(); it.hasNext();) {
			String key = it.next();
			if (!JwtUtil.isValidToken(key)) {
				logger.trace("remove : {}", tokenMap.get(key));
				it.remove();
			}
		}
		logger.info("normalize(After)  : size={}", tokenMap.size());
	}
}
