package jp.or.jaot.core.handler;

import jp.or.jaot.core.handler.common.BaseExceptionHandler;
import jp.or.jaot.model.dto.error.ErrorInfoDto;

/**
 * 不明エラーを処理するハンドラクラス
 */
public class UnknownErrorHandler extends BaseExceptionHandler {

	/**
	 * コンストラクタ<br>
	 * @param errorInfoDto エラー情報DTO
	 */
	public UnknownErrorHandler(ErrorInfoDto errorInfoDto) {
		this.errorInfoDto = errorInfoDto;
	}

	@Override
	protected void outputLog(ErrorInfoDto errorInfo) {
		logger.error("不明なエラーが発生しました : {}", errorInfo.getErrorCode(), errorInfo.getOriginalExp());
	}

	@Override
	protected String getViewName() {
		return "otError";
	}

}
