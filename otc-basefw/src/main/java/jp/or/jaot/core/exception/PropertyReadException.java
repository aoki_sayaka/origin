package jp.or.jaot.core.exception;

/**
 * プロパティ読込の例外クラス
 */
public class PropertyReadException extends RuntimeException {

	/**
	 * コンストラクタ<br>
	 * @param cause キャッチした例外
	 */
	public PropertyReadException(Throwable cause) {
		super(cause);
	}

	/**
	 * コンストラクタ<br>
	 * @param message 例外発生時のメッセージ
	 */
	public PropertyReadException(String message) {
		super(message);
	}
}
