package jp.or.jaot.core.util;

import java.security.Security;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.util.StringUtils;

/**
 * 暗号化に関するユーティリティクラス<br>
 * [暗号化方式]<br>
 * AES256<br>
 */
public class CipherUtil {

	/** 暗号方式 */
	private static final String DEF_TRANSFORMATION = "AES/CBC/PKCS5PADDING";

	/** 暗号方式 */
	private static final String DEF_ALGORITHM = "AES";

	/** パスワード */
	private static final String DEF_PASSWD = "aYDUSiGVriMkKBQXR9kVepawrjw58AUa";

	/** 初期ベクトル */
	private static final String DEF_IV = "KeTHyTxXMMR5qh6S";

	/**
	 * 暗号化
	 * @param input 入力文字列
	 * @return 暗号化文字列
	 */
	public static String encrypt(String input) {

		// 入力チェック
		if (!StringUtils.hasText(input)) {
			return null;
		}

		// 暗号化
		try {
			Security.setProperty("crypto.policy", "unlimited");
			Cipher cipher = Cipher.getInstance(DEF_TRANSFORMATION);
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(DEF_PASSWD.getBytes(), DEF_ALGORITHM),
					new IvParameterSpec(DEF_IV.getBytes()));
			return Base64.getEncoder().encodeToString(cipher.doFinal(input.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
			return null; // 例外発生時はnullを返却
		}
	}

	/**
	 * 複合化
	 * @param input 入力文字列
	 * @return 複合化文字列
	 */
	public static String decrypt(String input) {

		// 入力チェック
		if (!StringUtils.hasText(input)) {
			return null;
		}

		// 複合化
		try {
			Security.setProperty("crypto.policy", "unlimited");
			Cipher cipher = Cipher.getInstance(DEF_TRANSFORMATION);
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(DEF_PASSWD.getBytes(), DEF_ALGORITHM),
					new IvParameterSpec(DEF_IV.getBytes()));
			return new String(cipher.doFinal(Base64.getDecoder().decode(input)));
		} catch (Exception e) {
			e.printStackTrace();
			return null; // 例外発生時はnullを返却
		}
	}
}
