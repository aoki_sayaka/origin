package jp.or.jaot.core.util;

import java.io.File;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.or.jaot.core.exception.PropertyReadException;

/**
 * プロパティファイルへのアクセスに関するユーティリティクラス<br/>
 * インスタンス生成不可
 */
public final class PropertyUtility {

	/** ロガーインスタンス */
	private static final Logger logger = LoggerFactory.getLogger(PropertyUtility.class);

	/**
	 * 指定したプロパティファイルを読み込み、プロパティセットを返します. ファイルの読み込みには、ResourceBundleクラスを用いています.
	 * @param filename 読み込むプロパティファイルのパス.
	 * @return 読み込んだファイルの内容が登録されたプロパティセットを返します. null は返しません.
	 */
	public static Properties read(String filename) {

		// ファイル読み込み
		PropertiesConfiguration config = null;
		try {
			config = new PropertiesConfiguration(filename);
			logger.debug("プロパティファイル : filename={}", filename);

			// Propertiesへ変換
			Properties properties = new Properties();
			Iterator<String> keys = config.getKeys();
			while (keys.hasNext()) {
				String key = keys.next();
				String val = config.getString(key);
				properties.setProperty(key, val);
			}
			return properties;
		} catch (ConfigurationException e) {
			logger.error(e.getMessage());
			throw new PropertyReadException(e);
		}
	}

	/**
	 * プロパティファイルの存在チェックを行います.<br>
	 * @param fileName プロパティファイル名
	 * @return true:指定されたファイル名のプロパティファイルが存在する, false:存在しない
	 */
	public static boolean exists(String fileName) {
		boolean result = false;
		if (StringUtils.isNotBlank(fileName)) {
			File file = ConfigurationUtils.fileFromURL(ConfigurationUtils.locate(fileName));
			result = (file != null && file.exists() && file.isFile());
		}
		return result;
	}

	/**
	 * デフォルトコンストラクタ(隠ぺい)<br>
	 */
	private PropertyUtility() {
	}
}
