package jp.or.jaot.core.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import lombok.Getter;
import lombok.Setter;

/**
 * 検索結果リストフォームの基底クラス
 */
@Getter
@Setter
public abstract class BaseListForm implements Serializable {

	/** 検索結果の件数 */
	private Long resultCount = 0L;

	/** 検索結果の件数(全体) */
	private Long allResultCount = 0L;

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
