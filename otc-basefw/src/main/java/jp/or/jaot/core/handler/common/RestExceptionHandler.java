package jp.or.jaot.core.handler.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.JsonMappingException.Reference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import jp.or.jaot.core.exception.validation.FormValidationException;
import jp.or.jaot.core.util.ExceptionHelper;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.model.dto.error.ErrorInfoDto;

/**
 * Restコントローラ例外のハンドラクラス
 */
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	/** ロガーインスタンス */
	protected final Logger logger;

	/**
	 * コンストラクタ
	 */
	public RestExceptionHandler() {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		// 内部サーバエラー
		status = HttpStatus.INTERNAL_SERVER_ERROR;

		// フォーマット例外
		if (ex.getCause() instanceof InvalidFormatException) {

			// エラーマップ生成(1つのキーに複数の値を紐付け)
			InvalidFormatException invalidFormatEx = (InvalidFormatException) ex.getCause();
			MultiValueMap<String, String> errorMap = new LinkedMultiValueMap<>();
			for (Reference ref : invalidFormatEx.getPath()) {
				errorMap.add(ref.getFieldName(), "正しい値にしてください");
			}

			// フォームバリデーション例外エラー情報生成
			ERROR_CAUSE errorCause = ERROR_CAUSE.JSON_PARSE;
			String detail = "JSONデータをパースして、フォームにバインドできませんでした";
			ErrorInfoDto errorInfo = ExceptionHelper.createErrorInfo(new FormValidationException(errorCause, detail,
					ex.getMessage(), errorMap));

			// JSON生成
			try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonStr = mapper.writeValueAsString(errorInfo);
				body = jsonStr;
			} catch (Exception e) {
				logger.debug(e.getMessage());
			}

			// ログ
			logger.warn("フォーマットエラーが発生しました : {}", errorInfo.getErrorCode());
			logger.warn(errorInfo.getDetailJson());
		}

		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
}
