package jp.or.jaot.core.util;

import java.util.ArrayList;
import java.util.List;

/**
 * DB上のIDに関する便利クラス
 */
public class IdUtil {
	/**
	 * 変更前状態のID一覧と、変更後のID一覧を引き渡して、削除されたID（変更前に存在し、変更後に無いID）を抽出する。
	 * @param originIds 変更前のID一覧
	 * @param newIds 変更後のID一覧
	 * @return 削除されたIDの一覧
	 */
	public static List<Integer> chooseVanishedIds(List<Integer> originIds, List<Integer> newIds) {
		List<Integer> vanishedList = new ArrayList<Integer>();
		for (Integer origin : originIds) {
			if (!newIds.contains(origin)) {
				vanishedList.add(origin);
			}
		}
		return vanishedList;
	}

	/**
	 * 文字列を解釈してIDの番号に変換する。null, 空文字の場合はnullを返す。
	 * @param str 変換する値
	 */
	public static Integer toId(String str) {
		if (str == null) {
			return null;
		}
		if (str.length() == 0) {
			return null;
		}

		Integer result = null;
		try {
			result = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			result = null;
		}

		return result;
	}
}
