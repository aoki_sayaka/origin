package jp.or.jaot.model.dto.presentation;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * ページングに関する情報を取り扱うDTO
 */
@Getter
@Setter
public class PaginationDto<E> {
	/** 全件数 */
	private Integer totalCount;

	/** ページ番号 */
	private Integer page;

	/** 件数表示情報From */
	private Integer countFrom;

	/** 件数表示情報To */
	private Integer countTo;

	/** ページングのページ番号From */
	private Integer paginationFrom;

	/** ページングのページ番号To */
	private Integer paginationTo;

	/** Prevが有効か否か */
	private Boolean prevEnabled;

	/** Nextが有効か否か */
	private Boolean nextEnabled;

	/** 検索結果のオブジェクト */
	private List<E> entityList;

	/**
	 * 初期化する メソッド概要<br/>
	 * メソッド説明<br/>
	 */
	public PaginationDto(Integer totalCount, Integer paginationUnit, Integer page) {
		// 全件数と、ページ，Entityの最大インデックス（0始まり）を得る
		Integer maxEntityIndex = totalCount - 1;
		Integer maxPageIndex = maxEntityIndex / paginationUnit;

		// ページ，先頭のインデックス，最後のインデックスを返す
		if ((page == null) || (page <= 0)) {
			page = 1;
		}
		Integer pageIndex = page - 1;
		if (pageIndex > maxPageIndex) {
			pageIndex = maxPageIndex;
		}
		Integer fromIndex = pageIndex * paginationUnit;
		Integer toIndex = ((pageIndex + 1) * paginationUnit) - 1;
		if (toIndex > maxEntityIndex) {
			toIndex = maxEntityIndex;
		}

		// ページネーションについての情報を得る
		Integer fromPageIndex = pageIndex - 3;
		if (fromPageIndex < 0) {
			fromPageIndex = 0;
		}
		Integer toPageIndex = fromPageIndex + 6;
		if (toPageIndex > maxPageIndex) {
			toPageIndex = maxPageIndex;
		}
		Boolean prevEnabled = true;
		Boolean nextEnabled = true;
		if (pageIndex <= 0) {
			prevEnabled = false;
		}
		if (pageIndex >= maxPageIndex) {
			nextEnabled = false;
		}

		// DTOに詰めて返す
		this.setTotalCount(totalCount);
		this.setPage(pageIndex + 1);
		this.setCountFrom(fromIndex + 1);
		this.setCountTo(toIndex + 1);
		this.setPaginationFrom(fromPageIndex + 1);
		this.setPaginationTo(toPageIndex + 1);
		this.setPrevEnabled(prevEnabled);
		this.setNextEnabled(nextEnabled);
	}
}
