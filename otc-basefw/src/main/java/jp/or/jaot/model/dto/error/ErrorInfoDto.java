package jp.or.jaot.model.dto.error;

import lombok.Getter;
import lombok.Setter;

/**
 * エラー情報を保持するDTOクラス
 */
@Getter
@Setter
public class ErrorInfoDto {

	/** エラー種別(エラーレベル) */
	private String level;

	/** エラー内容 */
	private String kind;

	/** エラー原因 */
	private String cause;

	/** エラー発生箇所 */
	private String place;

	/** 対応方法 */
	private String workaround;

	/** 発生日時 */
	private String date;

	/** オリジナルのエラー情報 */
	private Exception originalExp;

	/** スタックトレース */
	private String stackTrace;

	/** エラー詳細 */
	private String detail;

	/** エラー詳細(JSON形式) */
	private String detailJson;

	/** エラーコードを取得する */
	public String getErrorCode() {
		return level + kind + cause + place + "_" + date;
	}
}
