package jp.or.jaot.model.form;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;
import lombok.Setter;

/**
 * ログイン情報
 */
@Getter
@Setter
public class LoginForm implements Serializable {

	/** ユーザ名 */
	private String username;

	/** パスワード */
	private String password;

	/**
	 *  デフォルトコンストラクタ
	 */
	public LoginForm() {
	}

	/**
	 *  コンストラクタ
	 * @param username ユーザ名
	 * @param password パスワード
	 */
	public LoginForm(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * 有効チェック
	 * @return true=有効, false=無効
	 */
	public boolean isValid() {
		return (StringUtils.isNotEmpty(username) && StringUtils.isNotEmpty(password));
	}

	@Override
	public String toString() {
		return String.format("LoginForm { username='%s', password='%s' }", username, password);
	}
}
