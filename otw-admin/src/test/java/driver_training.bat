echo off

rem Login
curl -v -X POST -d "{ \"username\" : \"9999\", \"password\" : \"pass\"}" -H "accept: application/json" -H "Content-Type: application/json" "http://localhost:8081/wa/login" > login.txt 2>&1

rem Get jwt from login.txt
for /f "usebackq tokens=*" %%i in (`findstr "Bearer" login.txt`) do @set RESULT=%%i
set JWT=%RESULT:~24%

rem Request post with jwt

rem AdminTrainingController(Training)
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training"
rem curl -v -X POST -d "{ \"ids\": [ 1 ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training"
rem curl -v -X POST -d "{ \"eventDate1StartTime\": \"12:34:00\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training"
rem curl -v -X POST -d "{ \"ids\": [ 275 ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training"
rem curl -v -X POST -d "{ \"organizerCds\":[ \"00\", \"01\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10, \"holdFiscalYear\": 2018, \"organizerCds\":[\"00\", \"01\"], \"trainingTypeCds\":[\"010001\"], \"courseCategoryCds\":[\"01000101\"], \"courseNm\": \"courseNm\", \"receptionStatusCds\":[\"01\"], \"updateStatusCds\":[\"10\"], \"venuePrefCds\":[\"01\"] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10, \"organizerCds\":[ \"00\", \"01\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10, \"eventDateFrom\": \"2019-01-01\", \"eventDateTo\": \"2019-12-31\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training"
rem curl -v -X POST -d "{ \"id\": 246, \"versionNo\": 3, \"holdDays\": 4, \"eventDate1\": \"2019-01-02\", \"eventDate2\": \"2019-01-02\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/update_training"
rem curl -v -X POST -d "{ \"id\": 246 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/disable_training"
rem curl -v -X POST -d "{ \"courseCategoryCd\":\"01000901\", \"holdFiscalYear\":2019 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/enter_training"
rem curl -v -X POST -d "{ \"courseCategoryCd\":\"01000901\", \"holdFiscalYear\":2019, \"trainingOrganizers\": [ { \"organizerCd\": \"01\" }, { \"organizerCd\": \"02\" } ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/enter_training"
rem curl -v -X POST -d "{ \"courseCategoryCd\":\"01000901\", \"holdFiscalYear\":2019, \"trainingInstructors\": [ { \"memberNo\": 1001 }, { \"memberNo\": 1002 } ], \"trainingOperators\": [ { \"memberNo\": 2001 }, { \"memberNo\": 2002 } ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/enter_training"
rem curl -v -X POST -d "{ \"id\": 3, \"versionNo\": 1, \"trainingInstructors\": [ { \"memberNo\": 1003 }, { \"id\": 2, \"versionNo\": 1, \"memberNo\": 1011 }, { \"id\": 3, \"versionNo\": 1, \"deleted\": true } ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/update_training"
rem curl -v -X POST -d "{ \"id\": 3, \"versionNo\": 2, \"trainingOperators\": [ { \"memberNo\": 2003 }, { \"id\": 2, \"versionNo\": 1, \"memberNo\": 2011 }, { \"id\": 3, \"versionNo\": 1, \"deleted\": true } ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/update_training"
curl -v -X POST -d "{ \"id\": 3 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/disable_training"

rem AdminTrainingController(TrainingApplication)
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_application"
rem curl -v -X POST -d "{ \"trainingNos\": [ \"2019000100010101\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_application"
rem curl -v -X POST -d "{ \"memberNo\": \"10\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_application"
rem curl -v -X POST -d "{ \"fullname\": \"ma\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_application"
rem curl -v -X POST -d "{ \"workPlaceNm\": \"gaya\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_application"
rem curl -v -X POST -d "{ \"workPlacePrefectureCds\": [ \"01\", \"02\", \"03\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_application"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training_application"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10, \"trainingNos\": [ \"2019000100010101\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training_application"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10, \"workPlacePrefectureCds\": [ \"01\", \"02\", \"03\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training_application"

rem AdminTrainingController(TrainingOrganizer)
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_organizer"

rem AdminTrainingController(TrainingAttendanceResult)
rem curl -v -X POST -d "{ \"trainingNo\": \"2019000100010101\", \"memberNo\": 9999, \"attendanceStatus1Cd\": \"00\", \"attendanceStatus2Cd\": \"00\", \"attendanceStatus3Cd\": \"00\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/enter_training_attendance_result"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_attendance_result"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training_attendance_result"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":9999, \"joinApplicationFlag\":true }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training_attendance_result"
rem curl -v -X POST -d "{ \"id\": 4, \"versionNo\": 6, \"attendanceStatus1Cd\": \"10\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/update_training_attendance_result"
rem curl -v -X POST -d "{ \"trainingNo\": \"2019000100010101\", \"memberNo\": 10002, \"attendanceStatus1Cd\": \"00\", \"attendanceStatus2Cd\": \"10\", \"attendanceStatus3Cd\": \"20\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/update_training_attendance_result"

rem AdminTrainingController(TrainingTempApplication)
rem curl -v -X POST -d "{ \"trainingNo\": \"2019000100010101\", \"memberNo\": 9999 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/enter_training_temp_application"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_temp_application"
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":9999, \"trainingNos\": [ \"2019000100010102\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training_temp_application"
rem curl -v -X POST -d "{ \"ids\": [ 4 ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/delete_training_temp_application"

rem AdminTrainingController(TrainingReport)
rem curl -v -X POST -d "{ \"trainingNo\": \"2019000100010101\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/enter_training_report"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_report"
rem curl -v -X POST -d "{ \"trainingNos\": [ \"2019000100010101\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_report"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_unit_item_training_report"
rem curl -v -X POST -d "{ \"trainingNos\": [ \"2019000100010101\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_unit_item_training_report"
rem curl -v -X POST -d "{ \"trainingNos\": [ \"2019000100010101\", \"2019000100010102\" ] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_unit_item_training_report"
rem curl -v -X POST -d "{ \"id\": 2, \"versionNo\": 3, \"trainingNo\": \"2019000100010103\", \"targetCd\": \"10\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/update_training_report"

rem AdminTrainingController(TrainingMember)
rem curl -v -X POST -d "{ \"startPosition\":0, \"maxResult\":10 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_list_training_member"

rem AdminTrainingController(TrainingInstructor)
rem curl -v -X POST -d "{ \"trainingNo\": \"2019000100010101\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/enter_training_instructor"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_instructor"

rem AdminTrainingController(TrainingOperator)
rem curl -v -X POST -d "{ \"trainingNo\": \"2019000100010101\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/enter_training_operator"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/training/search_training_operator"

rem Remove login file
set JWT=
set RESULT=
if exist login.txt del login.txt