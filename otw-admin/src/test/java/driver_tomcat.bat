echo off

rem Login
curl -v -X POST -d "{ \"username\" : \"9999\", \"password\" : \"pass\"}" -H "accept: application/json" -H "Content-Type: application/json" "http://localhost:8080/wa/login" > login.txt 2>&1

rem Get jwt from login.txt
for /f "usebackq tokens=*" %%i in (`findstr "Bearer" login.txt`) do @set RESULT=%%i
set JWT=%RESULT:~24%

rem Request post with jwt

rem OtwAdminController
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8080/wa/search"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8080/wa/search_error"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8080/wa/search_exception"

rem AdminUserController
curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8080/wa/user/login_user"

rem AdminAttendingHistoryController(attendingHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8080/wa/attending_History/search_attending_History"

rem Remove login file
set JWT=
set RESULT=
if exist login.txt del login.txt