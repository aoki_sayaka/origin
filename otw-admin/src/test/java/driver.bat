echo off

rem Login
curl -v -X POST -d "{ \"username\" : \"9999\", \"password\" : \"pass\"}" -H "accept: application/json" -H "Content-Type: application/json" "http://localhost:8081/wa/login" > login.txt 2>&1

rem Get jwt from login.txt
for /f "usebackq tokens=*" %%i in (`findstr "Bearer" login.txt`) do @set RESULT=%%i
set JWT=%RESULT:~24%

rem Request post with jwt

rem OtwAdminController
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/search"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/search_error"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/search_exception"

rem AdminUserController
curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/user/login_user"
rem curl -v -X POST -d "{\"id\" : 2, \"loginId\" : \"2\", \"password\" : \"\", \"name\" : \"22name\", \"emailAddress\" : \"2a@a.a\", \"paotUser\" : \"true\", \"sectionCd\" : \"02\", \"sectionName\" : \"sectionName2\", \"positionCode\" : \"01\", \"positionName\" : \"positionName2\", \"trainingOperator\" : \"true\", \"retirementDate\" : \"1970-01-01\"}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/user/update_user"
rem curl -v -X POST -d "{\"id\" : 25}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/user/disable_user"

rem AdminUserController(userRole)
rem curl -v -X POST -d "{\"userId\":\"9999\", \"roleCodeId\":\"10100\", \"roleCode\":\"1\"}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/user/enter_user_role"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/user/search_user_role"

rem AdminUserController(userRevokedAuthority)
rem curl -v -X POST -d "{\"userId\":\"9999\", \"codeId\":\"10100\", \"code\":\"1\", \"pathOrKey\":\"/wa/user/enter_user_role\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/user/enter_user_revoked_authority"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/user/search_user_revoked_authority"

rem AdminAttendingHistoryController(attendingHistory)
rem curl -v -X POST -d "{ \"startPosition\" : 0, \"maxResult\" : 5 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_list_member"
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_attending_history_member"
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_attending_history"
rem curl -v -X POST -d "{ \"id\" : 2, \"versionNo\" : 3, \"isIncrementalUpdate\" : true, \"renewalCount\" : 5, \"basicOtQualifyHistories\":[{\"versionNo\":2, \"id\":2, \"memberNo\":1188, \"seq\": 2}]},{ \"id\" : 2, \"versionNo\" : 1, \"isIncrementalUpdate\" : true, \"point\" : 5 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/update_attending_history"

rem BasicOtAttendingSummaryController(basicOtAttendingSummary)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_basic_ot_attending_summary"
rem curl -v -X POST -d "{ \"id\" : 2, \"versionNo\" : 2, \"isIncrementalUpdate\" : true, \"renewalCount\" : 1, \"basicOtQualifyHistories\":[{\"versionNo\":1, \"id\":2, \"memberNo\":1188, \"seq\": 9}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/update_basic_ot_attending_summary"
rem curl -v -X POST -d "{ \"id\" : 2, \"versionNo\" : 3, \"isIncrementalUpdate\" : true, \"renewalCount\" : 1, \"basicOtTrainingHistories\":[{\"versionNo\":2, \"id\":2, \"memberNo\":1188, \"seq\":3}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/update_basic_ot_attending_summary"

rem BasicOtAttendingSummaryController(basicOtQualifyHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_basic_ot_qualify_history"

rem BasicOtAttendingSummaryController(basicOtTrainingHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"9999999\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_basic_ot_training_history"

rem AdminAttendingHistoryController(BasicPointTrainingHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_basic_point_training_history"
rem curl -v -X POST -d "{ \"id\" : 2, \"versionNo\" : 2 }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/update_basic_point_training_history"

rem AdminAttendingHistoryController(QualificationOtAttendingHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_qualification_ot_attending_history"

rem AdminAttendingHistoryController(ProfessionalOtAttendingSummary)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_professional_ot_attending_summary"
rem curl -v -X POST -d "{ \"id\" : 8888, \"versionNo\" : 2, \"isIncrementalUpdate\" : true, \"renewalCount\" : 1, \"professionalOtAttendingHistories\":[{\"versionNo\":2, \"id\":2, \"memberNo\":1188}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/update_professional_ot_attending_summary"

rem AdminAttendingHistoryController(QualificationOtHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_qualification_ot_history"

rem AdminAttendingHistoryController(QualifiedOtAttendingSummary)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_qualified_ot_attending_summary"
rem curl -v -X POST -d "{ \"id\" : 8888, \"versionNo\" : 1, \"isIncrementalUpdate\" : true, \"renewalCount\" : 123, \"qualifiedOtAttendingHistories\":[{\"versionNo\":2, \"id\":2, \"memberNo\":1188}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/update_qualified_ot_attending_summary"

rem AdminAttendingHistoryController(QualifiedOtCompletionHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_qualified_ot_completion_history"

rem AdminAttendingHistoryController(QualifiedOtTrainingHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_qualified_ot_training_history"

rem AdminAttendingHistoryController(ClinicalTrainingLeaderSummary)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_clinical_training_leader_summary"
rem curl -v -X POST -d "{ \"id\" : 8888, \"versionNo\" : 1, \"isIncrementalUpdate\" : true,  \"clinicalTrainingLeaderSummary\":[{\"versionNo\":, \"id\":2, \"memberNo\":1188, seq\": 1}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/update_clinical_training_leader_summary"

rem AdminAttendingHistoryController(ClinicalTrainingLeaderQualifyHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_clinical_training_leader_qualify_history"

rem AdminAttendingHistoryController(ClinicalTrainingLeaderTrainingHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_clinical_training_leader_training_history"

rem AdminOtherOrgPointApplicationController(VariousApplication)
rem curl -v -X POST -d "{ \"id\" : \"8888\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/various_application/search_other_org_point_application"
rem curl -v -X POST -d "{ \"memberNo\":1234567, \"id\":8888, \"versionNo\" : 1, \"isIncrementalUpdate\" : true,  \"otherOrgPointApplications\":[{\"versionNo\":2, \"id\":7777, \"memberNo\":1234567}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/various_application/update_other_org_point_application"

rem AdminAttendingHistoryController(MtdlpSummary)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_mtdlp_summary"
rem curl -v -X POST -d "{ \"id\" : 8888, \"versionNo\" : 3, \"isIncrementalUpdate\" : true,  \"mtdlpSummary\":[{\"versionNo\":2, \"id\":2, \"memberNo\":1188}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/update_mtdlp_summary"

rem AdminAttendingHistoryController(MtdlpQualifyHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_mtdlp_qualify_history"

rem AdminAttendingHistoryController(MtdlpTrainingHistory)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_mtdlp_training_history"

rem AdminAttendingHistoryController(MtdlpCase)
rem curl -v -X POST -d "{ \"memberNo\" : \"1234567\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/attending_history/search_mtdlp_case"

rem AdminMemberController
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/member/search_member"

rem AdminCommonController
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_general_code"
rem curl -v -X POST -d "{ \"id\" : \"50110\", \"code\" : \"02\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_general_code"
rem curl -v -X POST -d "{ \"id\" : \"10011\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_general_code"
rem curl -v -X POST -d "{ \"id\" : \"20070\", \"code\" : \"01\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_general_code"
rem curl -v -X POST -d "{ \"id\" : \"20070\", \"code\" : \"1120\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_general_code"
rem curl -v -X POST -d "{ \"id\" : \"20070\", \"code\" : \"112030\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_general_code"
rem curl -v -X POST -d "{ \"code\" : \"0101\", \"categoryCd\" : \"01\" }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_qualification"
rem curl -v -X POST -d "{}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_multiple_general_code"
rem curl -v -X POST -d "{ \"generalCodes\" : [{\"id\" : \"50090\", \"code\" : \"01\"}] }" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_multiple_general_code"
rem curl -v -X POST -d "{\"generalCodes\" : [{\"id\":\"10011\"}, {\"id\":\"20100\"}, {\"id\":\"60060\"}, {\"id\":\"60361\"}, {\"id\":\"60130\"}, {\"id\":\"60190\", \"code\":\"05\"}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_multiple_general_code"
rem curl -v -X POST -d "{\"generalCodes\" : [{\"id\":\"10011\", \"code\":\"05\"}, {\"id\":\"10011\", \"code\":\"06\"}]}" -H "accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer %JWT%" "http://localhost:8081/wa/common/search_multiple_general_code"

rem Remove login file
set JWT=
set RESULT=
if exist login.txt del login.txt