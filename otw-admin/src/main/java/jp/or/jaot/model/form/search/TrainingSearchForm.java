package jp.or.jaot.model.form.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.form.TrainingForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会検索フォーム
 */
@Getter
@Setter
public class TrainingSearchForm extends TrainingForm {

	/** ID */
	private List<Long> ids = new ArrayList<Long>();

	/** 主催者 */
	private List<String> organizerCds = new ArrayList<String>();

	/** 研修会種別 */
	private List<String> trainingTypeCds = new ArrayList<String>();

	/** 講座分類 */
	private List<String> courseCategoryCds = new ArrayList<String>();

	/** 受付状況 */
	private List<String> receptionStatusCds = new ArrayList<String>();

	/** 更新状況 */
	private List<String> updateStatusCds = new ArrayList<String>();

	/** 開催地 */
	private List<String> venuePrefCds = new ArrayList<String>();

	/** 開催日(From) */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date eventDateFrom;

	/** 開催日(To) */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date eventDateTo;

	/** 通知ありフラグ */
	private Boolean notificationFlag;
}
