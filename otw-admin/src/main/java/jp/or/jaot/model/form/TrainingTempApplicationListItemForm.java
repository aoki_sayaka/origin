package jp.or.jaot.model.form;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.TrainingTempApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込リスト項目フォーム
 * @see TrainingTempApplicationEntity
 * @see MemberEntity
 * @see BasicOtAttendingSummaryEntity
 */
@Getter
@Setter
public class TrainingTempApplicationListItemForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 会員番号 */
	private Integer memberNo;

	/** 氏名 */
	private String fullname;

	/** 現在日時 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp currentDatetime;

	/** 登録日時 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp registeredDatetime;

	/** 締切日時 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp applicationDeadlineDatetime;

	/** 対象疾患 */
	private String targetDisease;

	/** 症例提供 */
	private String caseProvideCd;

	/** 経験年数 */
	private Integer experienceYear;

	/** 勤務先都道府県コード */
	private String workingPrefectureCd;

	/** 勤務条件（施設名） */
	private String workingConditionFacilityName;

	/** 認定期間満了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationPeriodDate;

	// TODO
}
