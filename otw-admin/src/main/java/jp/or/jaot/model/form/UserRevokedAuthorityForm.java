package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.UserRevokedAuthorityEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局権限フォーム
 * @see UserRevokedAuthorityEntity
 */
@Getter
@Setter
public class UserRevokedAuthorityForm extends BaseForm {

	/** ID */
	private Long id;

	/** コード識別ID */
	private String codeId;

	/** コード値 */
	private String code;

	/** パス/キー値 */
	private String pathOrKey;
}
