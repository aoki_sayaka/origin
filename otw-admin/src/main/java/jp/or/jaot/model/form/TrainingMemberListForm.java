package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会会員リストフォーム
 */
@Getter
@Setter
public class TrainingMemberListForm extends BaseListForm {

	/** 研修会会員リスト項目 */
	List<TrainingMemberListItemForm> trainingMembers = new ArrayList<TrainingMemberListItemForm>();
}
