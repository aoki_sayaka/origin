package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込リストフォーム
 */
@Getter
@Setter
public class TrainingTempApplicationListForm extends BaseListForm {

	/** 研修会仮申込リスト項目 */
	List<TrainingTempApplicationListItemForm> trainingTempApplications = new ArrayList<TrainingTempApplicationListItemForm>();
}
