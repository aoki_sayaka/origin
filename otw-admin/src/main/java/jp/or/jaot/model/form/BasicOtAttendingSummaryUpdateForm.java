package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修履歴検索結果フォーム
 * @see BasicOtAttendingSummaryEntity
 */
@Getter
@Setter
public class BasicOtAttendingSummaryUpdateForm extends BaseFullForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 認定年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationDate;

	/** 認定期間満了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationPeriodDate;

	/** 更新回数 */
	private Integer renewalCount;

	/** 会員関連資格情報 */
	private List<BasicOtQualifyHistoryForm> basicOtQualifyHistories = new ArrayList<BasicOtQualifyHistoryForm>();

	/** 会員自治体参画情報 */
	private List<BasicOtTrainingHistoryForm> basicOtTrainingHistories = new ArrayList<BasicOtTrainingHistoryForm>();

	/** 期間延長申請 */
	private List<PeriodExtensionApplicationForm> periodExtensionApplications = new ArrayList<PeriodExtensionApplicationForm>();
}
