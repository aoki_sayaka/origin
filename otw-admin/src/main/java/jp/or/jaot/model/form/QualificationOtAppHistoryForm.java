package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.QualificationOtAppHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士申請履歴フォーム
 * @see QualificationOtAppHistoryEntity
 */
@Getter
@Setter
public class QualificationOtAppHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 回数 */
	private Integer seq;

	/** 申請パターン */
	private String applicationPattern;

	/** 実例報告登録制度1テーマ */
	private String caseReport1Theme;

	/** 実例報告登録制度1年月日 */
	private String caseReport1Date;

	/** 実例報告登録制度2テーマ */
	private String caseReport2Theme;

	/** 実例報告登録制度2年月日 */
	private String caseReport2Date;

	/** 実例報告登録制度3テーマ */
	private String caseReport3Theme;

	/** 実例報告登録制度3年月日 */
	private String caseReport3Date;

	/** 活動実績（協会主催）1年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot1Date;

	/** 活動実績（協会主催）1内容*/
	private String activityResultJaot1Content;

	/** 活動実績（協会主催）2年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot2Date;

	/** 活動実績（協会主催）2内容 */
	private String activityResultJaot2Content;

	/** 活動実績（協会主催）3年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot3Date;

	/** 活動実績（協会主催）3内容 */
	private String activityResultJaot3Content;

	/** 活動実績（協会主催）4年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot4Date;

	/** 活動実績（協会主催）4内容 */
	private String activityResultJaot4Content;

	/** 活動実績（協会主催）5年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot5Date;

	/** 活動実績（協会主催）5内容 */
	private String activityResultJaot5Content;

	/** 活動実績1発表日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult1PresentationDate;

	/** 活動実績1発表者区分コード */
	private String activityResult1RoleCd;

	/** 活動実績1タイトル */
	private String activityResult1Title;

	/** 活動実績1カテゴリコード */
	private String activityResult1CategoryCd;

	/** 活動実績2発表日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult2PresentationDate;

	/** 活動実績2発表者区分コード */
	private String activityResult2RoleCd;

	/** 活動実績2タイトル */
	private String activityResult2Title;

	/** 活動実績2カテゴリコード */
	private String activityResult2CategoryCd;

	/** 活動実績3発表日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult3PresentationDate;

	/** 活動実績3発表者区分コード */
	private String activityResult3RoleCd;

	/** 活動実績3タイトル */
	private String activityResult3Title;

	/** 活動実績3カテゴリコード */
	private String activityResult3CategoryCd;

	/** 活動実績4発表日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult4PresentationDate;

	/** 活動実績4発表者区分コード */
	private String activityResult4RoleCd;

	/** 活動実績4タイトル */
	private String activityResult4Title;

	/** 活動実績4カテゴリコード */
	private String activityResult4CategoryCd;

	/** 活動実績5発表日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult5PresentationDate;

	/** 活動実績5発表者区分コード */
	private String activityResult5RoleCd;

	/** 活動実績5タイトル */
	private String activityResult5Title;

	/** 活動実績5カテゴリコード */
	private String activityResult5CategoryCd;

	/** 臨床実習能力試験受験日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date clinicalTrainingCapacityTestDate;

	/** 他団体・SIG等の認定資格コード */
	private String otherSigQualificationCd;

	/** 他団体・SIG等の認定資格　取得日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date otherSigQualificationDate;

	/** 他団体・SIG等の認定資格　証明書ファイル名 */
	private String otherSigQualificationFileName;

	/** 他団体・SIG等の認定資格　証明書 */
	private String otherSigQualificationFileData;

	/** 臨床実践報告書1指導会員番号 */
	private Integer clinicalTrainingReport1MemberNo;

	/** 臨床実践報告書1報告書ファイル名 */
	private String clinicalTrainingReport1FileName;

	/** 臨床実践報告書1報告書 */
	private String clinicalTrainingReport1FileData;

	/** 臨床実践報告書2指導会員番号 */
	private Integer clinicalTrainingReport2MemberNo;

	/** 臨床実践報告書2報告書ファイル名 */
	private String clinicalTrainingReport2FileName;

	/** 臨床実践報告書2報告書 */
	private String clinicalTrainingReport2FileData;

	/** 臨床実践報告書3指導会員番号 */
	private Integer clinicalTrainingReport3MemberNo;

	/** 臨床実践報告書3報告書ファイル名 */
	private String clinicalTrainingReport3FileName;

	/** 臨床実践報告書3報告書 */
	private String clinicalTrainingReport3FileData;

	/** 臨床実践報告書4指導会員番号 */
	private Integer clinicalTrainingReport4MemberNo;

	/** 臨床実践報告書4報告書ファイル名 */
	private String clinicalTrainingReport4FileName;

	/** 臨床実践報告書4報告書 */
	private String clinicalTrainingReport4FileData;

	/** 臨床実践報告書5指導会員番号 */
	private Integer clinicalTrainingReport5MemberNo;

	/** 臨床実践報告書5報告書ファイル名 */
	private String clinicalTrainingReport5FileName;

	/** 臨床実践報告書5報告書 */
	private String clinicalTrainingReport5FileData;

	/** 論文1発表先コード */
	private String paper1PresentationCd;

	/** 論文1備考 */
	private String paper1Remarks;

	/** 論文1その他 */
	private String paper1Other;

	/** 論文2発表先コード */
	private String paper2PresentationCd;

	/** 論文2備考 */
	private String paper2Remarks;

	/** 論文2その他 */
	private String paper2Other;

}
