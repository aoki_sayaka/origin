package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingOrganizerEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会主催者フォーム
 * @see TrainingOrganizerEntity
 */
@Getter
@Setter
public class TrainingOrganizerForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 連番 */
	private Integer seqNo;

	/** 主催者コード */
	private String organizerCd;
}
