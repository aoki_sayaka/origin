package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.entity.AttendingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 受講履歴フォーム
 * @see AttendingHistoryEntity
 */
@Getter
@Setter
public class AttendingHistoryForm extends AttendingHistoryUpdateForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	//	@JsonFormat(pattern = "yyyy-MM-dd")
	//	private Date attendDate;
	//
	//	private String category;
	//
	//	private String professionalField;
	//
	//	private String attendGroup;
	//
	//	private String attendClass;
	//
	//	private Integer seq;
	//
	//	private String remissionCauseCd;
	//
	//	private String remissionQualificationCd;
	//
	//	private String acceptanceCd;
	//
	//	private String promoterCd;
	//
	//	private String othersSigCd;
	//
	//	private String content;
	//
	//	private String pointTypeCd;
	//
	//	private Integer days;
	//
	//	private Integer point;
	//
	//	private String facilityNo;
	//
	//	private boolean usePoint;
	//
	//	private String collationMemberName;
	//
	//	private String collationRate;
	//
	//	@JsonFormat(pattern = "yyyy-MM-dd")
	//	private Date startDate;
	//
	//	@JsonFormat(pattern = "yyyy-MM-dd")
	//	private Date endDate;
	//
	//	private boolean checkMarked;
	//
	//	private String remarks;
	//
	private List<BasicOtAttendingSummaryForm> basicOtAttendingSummaryForm = new ArrayList<BasicOtAttendingSummaryForm>();

	private List<MemberForm> memberForm = new ArrayList<MemberForm>();

	/** 会員関連資格情報 */
	private List<MemberQualificationForm> memberQualifications = new ArrayList<MemberQualificationForm>();

	/** 会員自治体参画情報 */
	private List<MemberLocalActivityForm> memberLocalActivities = new ArrayList<MemberLocalActivityForm>();

	/** 休会履歴 */
	private List<RecessHistoryForm> recessHistories = new ArrayList<RecessHistoryForm>();

	/** 退会履歴 */
	private List<MemberWithdrawalHistoryForm> memberWithdrawalHistories = new ArrayList<MemberWithdrawalHistoryForm>();

}
