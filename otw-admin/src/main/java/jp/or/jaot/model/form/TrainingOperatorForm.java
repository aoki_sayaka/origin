package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingOperatorEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会運営担当者フォーム
 * @see TrainingOperatorEntity
 */
@Getter
@Setter
public class TrainingOperatorForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 会員番号 */
	private Integer memberNo;

	/** 会員氏名 */
	private String memberNm;

	/** 表示順 */
	private Integer displaySeqNo;

	/** 運営担当者区分 */
	private String categoryCd;

	/** 運営担当者問合せ担当区分 */
	private String contactCategoryCd;

	/** 運営担当者上長宛公文書有無 */
	private String docToSuperiorsCd;

	/** 運営担当者上長宛公文書印刷フラグ */
	private Boolean docToSuperiorsPrintFlag;

	/** 運営担当者上長施設名 */
	private String docToSuperiorsFacilityNm;

	/** 運営担当者上長宛名 */
	private String docToSuperiorsNm;

	/** 運営担当者上長宛公文書発送日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date docToSuperiorsSendDate;

	/** 運営担当者本人宛公文書有無 */
	private String docToPersonCd;

	/** 運営担当者本人宛公文書印刷フラグ */
	private Boolean docToPersonPrintFlag;

	/** 運営担当者本人施設名 */
	private String docToPersonFacilityNm;

	/** 運営担当者本人宛名 */
	private String docToPersonNm;

	/** 運営担当者本人公文書発送日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date docToPersonSendDate;

	/** 士会事務局ユーザID */
	private String secretariatUserId;

	/** 削除フラグ */
	private Boolean deleted;
}
