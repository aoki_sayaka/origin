package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修受講履歴フォーム
 * @see BasicOtTrainingHistoryEntity
 */
@Getter
@Setter
public class BasicOtTrainingHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	private String trainignCd;
	//
	private String fiscalYear;

	/** グループコード */
	private String groupCd;

	/** クラスコード */
	private String classCd;
	//
	private Integer seq;

	private String trainingName;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendDate;

	/** 削除フラグ */
	private Boolean deleted;

	/** バージョン */
	private Integer versionNo;
}
