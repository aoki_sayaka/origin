package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員関連資格情報フォーム
 * @see MemberLocalActivityEntity
 */
@Getter
@Setter
public class MemberLocalActivityForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 活動番号 */
	private Integer activitySeq;

	/** 参画フラグ */
	private Boolean participation;

	/** 自治体名 */
	private String municipalityName;
}
