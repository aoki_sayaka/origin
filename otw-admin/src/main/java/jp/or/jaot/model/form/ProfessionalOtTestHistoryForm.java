package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士試験結果履歴フォーム
 * @see ProfessionalOtTestHistoryEntity
 */
@Getter
@Setter
public class ProfessionalOtTestHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 専門分野 */
	private String professionalField;

	/** 試験日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date testDate;

	/** 合否判定コード */
	private String acceptanceCd;

}
