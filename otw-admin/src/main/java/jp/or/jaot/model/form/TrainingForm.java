package jp.or.jaot.model.form;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会フォーム
 * @see TrainingEntity
 */
@Getter
@Setter
public class TrainingForm extends BaseForm {

	/** ID */
	private Long id;

	/** 合否判定 */
	private String acceptanceCd;

	/** 住所1 */
	private String address1;

	/** 住所2 */
	private String address2;

	/** 住所3 */
	private String address3;

	/** 受講許可振込期限日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendPermissionDepositPeriodDate;

	/** 受講許可公文書 */
	private String attendPermissionDoc;

	/** 受講許可公文書発送日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendPermissionDocDeliveryDate;

	/** 出席受付 */
	private String attendanceReceptCd;

	/** 受講ポイント */
	private Integer attendingPoint;

	/** 中止日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date cancelDate;

	/** キャンセルフラグ */
	private Boolean cancelFlag;

	/** 定員 */
	private Integer capacityCount;

	/** 講座分類 */
	private String courseCategoryCd;

	/** 講座名 */
	private String courseNm;

	/** 配信終了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date deliveryEndDate;

	/** 配信開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date deliveryStartDate;

	/** e-learningフラグ */
	private Boolean elearningFlag;

	/** 終了日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endDate;

	/** 終了フラグ */
	private Boolean endFlag;

	/** 参加費 */
	private Integer entryFee;

	/** 開催日フリー */
	private String eventDateFree;

	/** 開催日1 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date eventDate1;

	/** 開催日1開始時間 */
	@JsonFormat(pattern = "HH:mm:ss")
	private Time eventDate1StartTime;

	/** 開催日2 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date eventDate2;

	/** 開催日3 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date eventDate3;

	/** 開催回数 */
	private Integer holdCount;

	/** 開催日数 */
	private Integer holdDays;

	/** 開催年度 */
	private Integer holdFiscalYear;

	/** 協会HP掲載日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date hpPostedDate;

	/** 協会HP掲載フラグ */
	private Boolean hpPostedFlag;

	/** 協会HP掲載ユーザID */
	private String hpPostedUserId;

	/** 講師本人宛公文書番号 */
	private String instructorDocNoToPerson;

	/** 講師上司宛公文書番号 */
	private String instructorDocNoToSuperiors;

	/** 講師公文書出力形式 */
	private String instructorDocPrintFormatCd;

	/** 退席確認 */
	private String leaveConfirmCd;

	/** 連絡欄 */
	private String message;

	/** 旧研修番号 */
	private String oldTrainingNo;

	/** 運営担当者本人宛公文書番号 */
	private String operatorDocNoToPerson;

	/** 運営担当者上長宛公文書番号 */
	private String operatorDocNoToSuperiors;

	/** 運営担当者公文書出力形式 */
	private String operatorDocPrintFormatCd;

	/** 本人宛その他6 */
	private String other6;

	/** 概要および目的 */
	private String overview;

	/** 都道府県コード */
	private String prefectureCd;

	/** 受付締切日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date receptionDeadlineDate;

	/** 受付開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date receptionStartDate;

	/** 受付状況 */
	private String receptionStatusCd;

	/** 備考 */
	private String remarks;

	/** 学割参加費 */
	private Integer studentDiscountEntryFee;

	/** 参加対象 */
	private String targetCd;

	/** 研修会終了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date trainingEndDate;

	/** 研修会番号 */
	private String trainingNo;

	/** 研修会プログラムフリー */
	private String trainingProgramFree;

	/** 研修会プログラムフリーフラグ */
	private Boolean trainingProgramFreeFlag;

	/** 研修会種別 */
	private String trainingTypeCd;

	/** 研修会更新日時 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp trainingUpdateDatetime;

	/** 研修会更新者名 */
	private String trainingUpdateUser;

	/** 更新状況 */
	private String updateStatusCd;

	/** 開催場所 */
	private String venue;

	/** 開催場所フリー */
	private String venueFree;

	/** 開催場所フリーフラグ */
	private Boolean venueFreeFlag;

	/** 開催地 */
	private String venuePrefCd;

	/** 郵便番号 */
	private String zipcode;

	/** 研修会主催者 */
	private List<TrainingOrganizerForm> trainingOrganizers = new ArrayList<TrainingOrganizerForm>();

	/** 研修会講師実績 */
	private List<TrainingInstructorForm> trainingInstructors = new ArrayList<TrainingInstructorForm>();

	/** 研修会運営担当者 */
	private List<TrainingOperatorForm> trainingOperators = new ArrayList<TrainingOperatorForm>();
}
