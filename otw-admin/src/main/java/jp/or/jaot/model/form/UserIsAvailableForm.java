package jp.or.jaot.model.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 事務局利用可能状態取得フォーム
 */
@Getter
@Setter
public class UserIsAvailableForm {

	/** パス/キー値 */
	private String pathOrKey;
}
