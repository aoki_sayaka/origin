package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.MtdlpSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP履歴検索検索結果フォーム
 * @see MtdlpSummaryEntity
 */
@Getter
@Setter
public class MtdlpSummaryUpdateForm extends BaseFullForm {

	private Long id;

	/** MTDLP認定履歴 */
	private List<MtdlpQualifyHistoryForm> mtdlpQualifyHistories = new ArrayList<MtdlpQualifyHistoryForm>();

	/** MTDLP研修受講履歴 */
	private List<MtdlpTrainingHistoryForm> mtdlpTrainingHistories = new ArrayList<MtdlpTrainingHistoryForm>();

	/** MTDLP事例 */
	private List<MtdlpCaseForm> mtdlpCases = new ArrayList<MtdlpCaseForm>();

	/** MTDLP事例報告 */
	private List<MtdlpCaseReportForm> mtdlpCaseReports = new ArrayList<MtdlpCaseReportForm>();

}
