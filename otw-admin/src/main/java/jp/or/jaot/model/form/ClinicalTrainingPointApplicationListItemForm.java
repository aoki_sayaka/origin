package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.ClinicalTrainingPointApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習ポイント申請検索結果フォーム
 * @see ClinicalTrainingPointApplicationEntity
 */
@Getter
@Setter
public class ClinicalTrainingPointApplicationListItemForm extends BaseForm {
	
	/** 申請日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date applicationDate;
	
	/** 申請者 会員番号 */
	private Integer applicationMemberNo;
	
	/** 申請者 氏名 */
	private List<String> memberName = new ArrayList<String>();
	
	/** 養成校番号 */
	private String facilityNo;
	
	/** 養成校名 */
	private List<String> facilityName = new ArrayList<String>();
	
	/** ポイント種別 */
	// private String pointType;
	
	/** 申請状態 */
	private String statusCd;
}
