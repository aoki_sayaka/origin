package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 手帳移行申請検索結果リストフォーム
 */
@Getter
@Setter
public class HandbookMigrationApplicationListForm extends BaseListForm {
	
	/** 手帳移行申請検索結果 */
	List<HandbookMigrationApplicationListItemForm> handBookResults = new ArrayList<HandbookMigrationApplicationListItemForm>();
}