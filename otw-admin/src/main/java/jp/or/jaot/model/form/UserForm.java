package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ユーザフォーム
 * @see UserEntity
 */
@Getter
@Setter
public class UserForm extends BaseFullForm {

	/** ID */
	private Long id;

	/** ログインID */
	private String loginId;

	/** パスワード */
	private String password;

	/** ユーザー名 */
	private String name;

	/** メールアドレス */
	private String emailAddress;

	/** 士会ユーザフラグ */
	private Boolean paotUser;

	/** 所属部門コード */
	private String sectionCd;

	/** 所属部門名 */
	private String sectionName;

	/** 役職コード */
	private String positionCd;

	/** 役職名 */
	private String positionName;

	/** 研修会当日担当者フラグ */
	private Boolean trainingOperator;

	/** 退職日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date retirementDate;
}
