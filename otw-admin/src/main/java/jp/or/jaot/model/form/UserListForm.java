package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ユーザリストフォーム
 */
@Getter
@Setter
public class UserListForm extends BaseListForm {

	/** 事務局ユーザ */
	List<UserForm> users = new ArrayList<>();

}
