package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.MemberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * ポータルサイト会員編集フォーム
 * @see MemberEntity
 */
@Getter
@Setter
public class MemberProfileUpdateForm extends BaseFullForm {

	/** ID */
	private Long id;

	/** 出身校名 */
	private String almaMaterName;

	/** 請求書送付先国内フラグ */
	private Boolean billToJapanAddress;

	/** 請求書送付先国内フラグ */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date birthDay;

	/** 学位１ 授与年 */
	private Integer degree1AwardYear;

	/** 学位１ 教育施設名 */
	private String degree1EducationFacilityName;

	/** 学位１ 分野名 */
	private String degree1FieldName;

	/** 学位１ 区分番号 */
	private Integer degree1No;

	/** 学位２ 授与年 */
	private Integer degree2AwardYear;

	/** 学位２ 教育施設名 */
	private String degree2EducationFacilityName;

	/** 学位２ 分野名 */
	private String degree2FieldName;

	/** 学位２ 区分番号 */
	private Integer degree2No;

	/** 学位３ 授与年 */
	private Integer degree3AwardYear;

	/** 学位３ 教育施設名 */
	private String degree3EducationFacilityName;

	/** 学位３ 分野名 */
	private String degree3FieldName;

	/** 学位３ 区分番号 */
	private Integer degree3No;

	/** 学位４ 授与年 */
	private Integer degree4AwardYear;

	/** 学位４ 教育施設名 */
	private String degree4EducationFacilityName;

	/** 学位４ 分野名 */
	private String degree4FieldName;

	/** 学位４ 区分番号 */
	private Integer degree4No;

	/** 学位５ 授与年 */
	private Integer degree5AwardYear;

	/** 学位５ 教育施設名 */
	private String degree5EducationFacilityName;

	/** 学位５ 分野名 */
	private String degree5FieldName;

	/** 学位５ 区分番号 */
	private Integer degree5No;

	/** 災害支援ボランティア希望 */
	private Boolean disasterSupportVolunteerable;

	/** Email address */
	private String emailAddress;

	/** 主業務領域コード */
	private String facilityDomainCd;

	/** 主障害種別 */
	private String facilityHandicapType;

	/** 従障害種別 */
	private String facilitySubHandicapType;

	/** 従対象疾患 */
	private String facilitySubTargetDisease;

	/** 主対象疾患 */
	private String facilityTargetDisease;

	/** フリガナ(名) */
	private String firstKanaName;

	/** 姓名(名) */
	private String firstName;

	/** 海外の住所情報 */
	private String foreignAddress;

	/** 性別 */
	private Integer gender;

	/** 自宅住所１ */
	private String homeAddress1;

	/** 自宅住所２ */
	private String homeAddress2;

	/** 自宅住所３ */
	private String homeAddress3;

	/** 自宅電話番号 */
	private String homePhoneNumber;

	/** 自宅都道府県コード */
	private String homePrefectureCd;

	/** 自宅郵便番号 */
	private String homeZipcode;

	/** フリガナ(姓) */
	private String lastKanaName;

	/** 姓名(姓) */
	private String lastName;

	/** 免許番号 */
	private String licenseNumber;

	/** 旧姓 */
	private String maidenName;

	/** 会員番号 */
	private Integer memberNo;

	/** 携帯電話番号 */
	private String mobilePhoneNumber;

	/** その他勤務施設１業務内容 */
	private String otherFacility1Domain;

	/** その他勤務施設１ */
	private String otherFacility1Name;

	/** その他勤務施設２業務内容 */
	private String otherFacility2Domain;

	/** その他勤務施設２ */
	private String otherFacility2Name;

	/** 県士会都道府県コード */
	private String prefAssociationPrefectureCd;

	/** 資格取得年 */
	private Integer qualificationYear;

	/** 発送区分番号 */
	private Integer shippingCategoryNo;

	/** 加入WFOT番号 */
	private Integer wfotEntryStatus;

	/** 勤務条件 */
	private String workingCondition;

	/** 勤務先施設コード */
	private String workingFacilityCd;

	/** 勤務先部署名 */
	private String workingFacilityUnitName;

	/** 就業状況区分コード */
	private String workingStateCd;

	/** 会員関連資格情報 */
	private List<MemberQualificationForm> memberQualifications = new ArrayList<MemberQualificationForm>();

	/** 会員自治体参画情報 */
	private List<MemberLocalActivityForm> memberLocalActivities = new ArrayList<MemberLocalActivityForm>();

	/** 休会履歴 */
	private RecessHistoryForm recessHistory = new RecessHistoryForm();

	/** 退会履歴 */
	private MemberWithdrawalHistoryForm memberWithdrawalHistory = new MemberWithdrawalHistoryForm();

	/** 生涯教育ステータス */
	private LifelongEducationStatusForm lifelongEducationStatuses = new LifelongEducationStatusForm();
}