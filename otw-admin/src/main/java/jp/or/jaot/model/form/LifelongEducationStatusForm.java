package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LifelongEducationStatusForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 現在のポイント */
	private Integer point;

	/** 使用済みポイント */
	private Integer usedPoint;
}
