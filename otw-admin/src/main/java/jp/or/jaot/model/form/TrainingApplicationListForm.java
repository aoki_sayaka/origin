package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会申込リストフォーム
 */
@Getter
@Setter
public class TrainingApplicationListForm extends BaseListForm {

	/** 研修会申込リスト項目 */
	List<TrainingApplicationListItemForm> trainingApplications = new ArrayList<TrainingApplicationListItemForm>();
}
