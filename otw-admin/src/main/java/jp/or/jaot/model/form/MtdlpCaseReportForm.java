package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MtdlpCaseReportEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP事例報告フォーム
 * @see MtdlpCaseReportEntity
 */
@Getter
@Setter
public class MtdlpCaseReportForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 受付番号 */
	private Integer receiptNumber;

	/** 発表先コード */
	private String presentationCd;

	/** 表題 */
	private String title;

	/** 総合判定日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date overallJudgeDate;

	/** 取り下げフラグ */
	private boolean withdrawn;

	/** 公開日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date publishDate;

	/** 審査開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date judgestartDate;

	/** メール送信フラグ */
	private Boolean emailSent;
}
