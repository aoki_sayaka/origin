package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修履歴フォーム
 * @see BasicOtAttendingSummaryEntity
 */
@Getter
@Setter
public class BasicOtAttendingSummaryForm extends BasicOtAttendingSummaryUpdateForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 認定年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationDate;

	/** 認定期間満了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationPeriodDate;

	/** 更新回数 */
	private Integer renewalCount;
}
