package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP認定履歴フォーム
 * @see MtdlpQualifyHistoryEntity
 */
@Getter
@Setter
public class MtdlpQualifyHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 研修種別 */
	private String trainingType;

	/** 回数 */
	private Integer seq;

	/** 認定年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationDate;

	/** 認定番号 */
	private Integer qualificationNumber;

	/** 作成日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date createDateTime;

	/** 作成者名 */
	private String createUser;

	/** 更新日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date updateDateTime;

	/** 更新者名 */
	private String updateUser;

	/** 削除フラグ */
	private Boolean deleted;

	/** バージョン */
	private Integer versionNo;

}
