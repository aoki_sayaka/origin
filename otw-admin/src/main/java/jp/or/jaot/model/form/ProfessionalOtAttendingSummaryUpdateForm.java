package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士受講履歴フォーム
 * @see ProfessionalOtAttendingSummaryEntity
 */
@Getter
@Setter
public class ProfessionalOtAttendingSummaryUpdateForm extends BaseFullForm {

	private Long id;

	/** 専門作業療法士認定更新履歴 */
	private List<ProfessionalOtQualifyHistoryForm> professionalOtQualifyHistories = new ArrayList<ProfessionalOtQualifyHistoryForm>();

	/** 専門作業療法士試験結果履歴 */
	private List<ProfessionalOtTestHistoryForm> professionalOtTestHistories = new ArrayList<ProfessionalOtTestHistoryForm>();

	/** 専門作業療法士研修受講履歴 */
	private List<ProfessionalOtTrainingHistoryForm> professionalOtTrainingHistories = new ArrayList<ProfessionalOtTrainingHistoryForm>();

}
