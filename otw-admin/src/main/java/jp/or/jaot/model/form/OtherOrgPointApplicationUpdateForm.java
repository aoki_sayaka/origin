package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * ポイント申請検索結果フォーム
 * @see OtherOrgPointApplicationEntity
 */
@Getter
@Setter
public class OtherOrgPointApplicationUpdateForm extends BaseFullForm {

	private Long id;

	/** 他団体・SIGポイント添付ファイル */
	private List<OtherOrgPointApplicationAttachmentForm> otherOrgPointApplicationAttachments = new ArrayList<OtherOrgPointApplicationAttachmentForm>();


}
