package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.UserRoleEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ロールフォーム
 * @see UserRoleEntity
 */
@Getter
@Setter
public class UserRoleForm extends BaseForm {

	/** ID */
	private Long id;

	/** 事務局ユーザID */
	private Long userId;

	/** 権限コード識別ID */
	private String roleCodeId;

	/** 権限コード値 */
	private String roleCode;
}
