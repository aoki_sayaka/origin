package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.HandbookMigrationApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 手帳移行申請検索結果フォーム
 * @see HandbookMigrationApplicationEntity
 */
@Getter
@Setter
public class HandbookMigrationApplicationListItemForm extends BaseForm {
	
	/** 申請日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date applicationDate;
	
	/** 申請者 会員番号 */
	private Integer memberNo;
	
	/** 申請者 氏名 */
	private List<String> memberName = new ArrayList<String>();
	
	/** ポイント数 */
	private Integer basicTrainingPoint;
	
	/** 申請状態 */
	private String statusCd;
}
