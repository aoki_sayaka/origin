package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会出欠合否リスト項目フォーム
 * @see TrainingEntity
 * @see TrainingAttendanceResultEntity
 */
@Getter
@Setter
public class TrainingAttendanceResultListItemForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 申込者番号 */
	private Integer memberNo;

	/** 氏名 */
	private String fullname;

	/** 開催日1 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date eventDate1;

	/** 開催日2 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date eventDate2;

	/** 開催日3 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date eventDate3;

	/** 開催日１日目出席状況コード */
	private String attendanceStatus1Cd;

	/** 開催日２日目出席状況コード */
	private String attendanceStatus2Cd;

	/** 開催日３日目出席状況コード */
	private String attendanceStatus3Cd;

	/** 修了判定 */
	private String completionStatusCd;

	/** 試験合否 */
	private String testResultCd;

	/** 再試験合否 */
	private String retestResultCd;

	/** 再試験日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date retestDate;

	// TODO
}
