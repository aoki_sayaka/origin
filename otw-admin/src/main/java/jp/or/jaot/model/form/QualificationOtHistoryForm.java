package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.QualificationOtHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴フォーム
 * @see QualificationOtHistoryEntity
 */
@Getter
@Setter
public class QualificationOtHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	private Integer seq;

	private String applicationPattern;

	private String caseReport1Theme;

	private String caseReport1Date;

	private String caseReport2Theme;

	private String caseReport2Date;

	private String caseReport3Theme;

	private String caseReport3Date;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot1Date;

	private String activityResultJaot1Content;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot2Date;

	private String activityResultJaot2Content;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot3Date;

	private String activityResultJaot3Content;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot4Date;

	private String activityResultJaot4Content;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResultJaot5Date;

	private String activityResultJaot5Content;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult1PresentationDate;

	private String activityResult1RoleCd;

	private String activityResult1Title;

	private String activityResult1CategoryCd;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult2PresentationDate;

	private String activityResult2RoleCd;

	private String activityResult2Title;

	private String activityResult2CategoryCd;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult3PresentationDate;

	private String activityResult3RoleCd;

	private String activityResult3Title;

	private String activityResult3CategoryCd;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult4PresentationDate;

	private String activityResult4RoleCd;

	private String activityResult4Title;

	private String activityResult4CategoryCd;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date activityResult5PresentationDate;

	private String activityResult5RoleCd;

	private String activityResult5Title;

	private String activityResult5CategoryCd;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date clinicalTrainingCapacityTestDate;

	private String otherSigQualificationCd;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date otherSigQualificationDate;

	private String otherSigQualificationFileName;

	private String otherSigQualificationFileData;

	private Integer clinicalTrainingReport1MemberNo;

	private String clinicalTrainingReport1FileName;

	private String clinicalTrainingReport1FileData;

	private Integer clinicalTrainingReport2MemberNo;

	private String clinicalTrainingReport2FileName;

	private String clinicalTrainingReport2FileData;

	private Integer clinicalTrainingReport3MemberNo;

	private String clinicalTrainingReport3FileName;

	private String clinicalTrainingReport3FileData;

	private Integer clinicalTrainingReport4MemberNo;

	private String clinicalTrainingReport4FileName;

	private String clinicalTrainingReport4FileData;

	private Integer clinicalTrainingReport5MemberNo;

	private String clinicalTrainingReport5FileName;

	private String clinicalTrainingReport5FileData;

	private String paper1PresentationCd;

	private String paper1Remarks;

	private String paper1Other;

	private String paper2PresentationCd;

	private String paper2Remarks;

	private String paper2Other;

}
