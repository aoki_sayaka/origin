package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士受講履歴フォーム
 * @see ProfessionalOtAttendingSummaryEntity
 */
@Getter
@Setter
public class ProfessionalOtAttendingSummaryForm extends ProfessionalOtAttendingSummaryUpdateForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 専門分野 */
	private String professionalField;

	/** 認定年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationDate;

	/** 認定期間満了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationPeriodDate;

	/** 認定番号 */
	private Integer qualificationNumber;

	/** 更新回数 */
	private Integer renewalCount;

	/** 認知症ケア専門士 */
	private boolean dementiaCareSpecialist;

	/** 認知症ケア実務研修修了 */
	private boolean dementiaCareTrained;

	/** 教員免許取得 */
	private boolean teacherLicense;

	/** 認定訪問療法士 */
	private boolean qualifiedVisitingOt;

	/** 訪問リハビリテーション */
	private boolean visitRehabilitationTraining123;

	/** リンパ浮腫セラピスト */
	private boolean lymphedemaTherapist;

}
