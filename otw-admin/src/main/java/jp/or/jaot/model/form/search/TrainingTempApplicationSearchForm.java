package jp.or.jaot.model.form.search;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.TrainingAttendanceResultForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込検索フォーム
 */
@Getter
@Setter
public class TrainingTempApplicationSearchForm extends TrainingAttendanceResultForm {

	/** ID */
	private List<Long> ids = new ArrayList<Long>();

	/** 研修会番号 */
	private List<String> trainingNos = new ArrayList<String>();

	// TODO
}
