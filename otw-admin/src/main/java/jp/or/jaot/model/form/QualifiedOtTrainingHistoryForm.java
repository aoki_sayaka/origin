package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴フォーム
 * @see QualifiedOtTrainingHistoryEntity
 */
@Getter
@Setter
public class QualifiedOtTrainingHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 研修会コード */
	private String trainingCd;

	/** 年度 */
	private String fiscalYear;

	/** 主催者コード */
	private String promoterCd;

	/** カテゴリコード */
	private String categoryCd;

	/** 専門分野コード */
	private String professionalFieldCd;

	/** グループコード */
	private String groupCd;

	/** クラスコード */
	private String classCd;

	/** 枝番 */
	private String branchCd;

	/** 回数 */
	private Integer seq;

	/** 研修名 */
	private String trainingName;

	/** 研修受講年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendDate;

}
