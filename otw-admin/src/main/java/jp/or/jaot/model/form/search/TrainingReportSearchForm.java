package jp.or.jaot.model.form.search;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.TrainingReportForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会報告書検索フォーム
 */
@Getter
@Setter
public class TrainingReportSearchForm extends TrainingReportForm {

	/** 研修会番号 */
	private List<String> trainingNos = new ArrayList<String>();

	// TODO
}
