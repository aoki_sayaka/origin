package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント申請検索結果リストフォーム
 */
@Getter
@Setter
public class OtherOrgPointApplicationListForm extends BaseListForm {
	
	/** 他団体・SIGポイント申請検索結果 */
	List<OtherOrgPointApplicationListItemForm> otherOrgResults = new ArrayList<OtherOrgPointApplicationListItemForm>();
}