package jp.or.jaot.model.form.search;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.TrainingAttendanceResultForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会出欠合否検索フォーム
 */
@Getter
@Setter
public class TrainingAttendanceResultSearchForm extends TrainingAttendanceResultForm {

	/** 研修会申込結合フラグ(true=外部結合する, false=外部結合しない) */
	private Boolean joinApplicationFlag = false;

	/** 研修会番号 */
	private List<String> trainingNos = new ArrayList<String>();

	// TODO
}
