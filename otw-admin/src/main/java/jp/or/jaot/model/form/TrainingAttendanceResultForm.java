package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会出欠合否フォーム
 * @see TrainingAttendanceResultEntity
 */
@Getter
@Setter
public class TrainingAttendanceResultForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 申込者番号 */
	private Integer memberNo;

	/** 開催日１日目出席状況コード */
	private String attendanceStatus1Cd;

	/** 開催日２日目出席状況コード */
	private String attendanceStatus2Cd;

	/** 開催日３日目出席状況コード */
	private String attendanceStatus3Cd;

	/** 修了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date completionDate;

	/** 修了判定 */
	private String completionStatusCd;

	/** 参加費 */
	private Integer entryFee;

	/** 氏名 */
	private String fullname;

	/** 開催日１日目退席状況コード */
	private String leaveStatus1Cd;

	/** 開催日２日目退席状況コード */
	private String leaveStatus2Cd;

	/** 開催日３日目退席状況コード */
	private String leaveStatus3Cd;

	/** 当日収納フラグ */
	private Boolean payOnTheDayFlag;

	/** 再試験日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date retestDate;

	/** 再試験合格日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date retestPassDate;

	/** 再試験合否 */
	private String retestResultCd;

	/** 試験合格日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date testPassDate;

	/** 試験合否 */
	private String testResultCd;
}
