package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会申込リスト項目フォーム
 * @see TrainingEntity
 * @see TrainingApplicationEntity
 */
@Getter
@Setter
public class TrainingApplicationListItemForm extends BaseForm {

	/** ID */
	private Long id;

	/** 申込者番号 */
	private Integer memberNo;

	/** 氏名 */
	private String fullname;

	/** 研修会種別 */
	private String trainingTypeCd;

	/** 講座名 */
	private String courseNm;

	/** 開催回数 */
	private Integer holdCount;

	/** 講座分類 */
	private String courseCategoryCd;

	/** 申込状況 */
	private String applicationStatusCd;

	/** 更新状況 */
	private String updateStatusCd;

	/** 手続状況 */
	private String procedureStatusCd;

	/** 参加費 */
	private Integer entryFee;

	/** 入金状況 */
	// TODO

	/** 開催地 */
	private String venuePrefCd;

	/** 開催日(文字列) */
	private String eventDateText;

	/** 整理番号 */
	private Integer queueNo;

	/** 優先順位 */
	// TODO

	/** 連絡方法 */
	private String contactType;

	/** 取消辞退日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date decliningDate;

	/** 入金期限日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date depositPeriodDate;

	/** 通知日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date notificationDate;

	/** 再提出回数 */
	private Integer resubmissionCount;

	/** 再提出日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date resubmissionDate;

	/** 対象疾患 */
	private String targetDisease;

	/** 症例提供 */
	private String caseProvideCd;

	/** 経験年数 */
	private Integer experienceYear;

	/** 勤務先都道府県 */
	private String workPlacePrefectureCd;

	/** 勤務先名 */
	private String workPlaceNm;

	/** 送付日 */
	// TODO

	/** 登録日 */
	// TODO

	/** 入金日 */
	// TODO

	/** 基礎満了日 */
	// TODO

	/** 備考 */
	private String remarks;
}
