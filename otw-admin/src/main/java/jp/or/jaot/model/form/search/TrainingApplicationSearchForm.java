package jp.or.jaot.model.form.search;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.TrainingApplicationForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会申込検索フォーム
 */
@Getter
@Setter
public class TrainingApplicationSearchForm extends TrainingApplicationForm {

	/** 研修会番号 */
	private List<String> trainingNos = new ArrayList<String>();

	/** 勤務先都道府県 */
	private List<String> workPlacePrefectureCds = new ArrayList<String>();

	// TODO
}
