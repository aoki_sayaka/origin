package jp.or.jaot.model.form;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingTempApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込フォーム
 * @see TrainingTempApplicationEntity
 */
@Getter
@Setter
public class TrainingTempApplicationForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 会員番号 */
	private Integer memberNo;

	/** メールアドレス */
	private String mailAddress;

	/** 対象疾患 */
	private String targetDisease;

	/** 症例提供 */
	private String caseProvideCd;

	/** 経験年数 */
	private Integer experienceYear;

	/** 締切日時 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp applicationDeadlineDatetime;
}
