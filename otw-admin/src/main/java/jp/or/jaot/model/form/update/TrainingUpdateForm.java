package jp.or.jaot.model.form.update;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.model.form.TrainingForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会フォーム(更新)
 */
@Getter
@Setter
public class TrainingUpdateForm extends TrainingForm {

	/** 初期化対象プロパティ */
	private List<String> initProperties = new ArrayList<String>() {
		{
			add("eventDate1"); // 開催日1
			add("eventDate2"); // 開催日2
			add("eventDate3"); // 開催日3
			add("receptionStartDate"); // 受付開始日
			add("receptionDeadlineDate"); // 受付締切日
			add("trainingEndDate"); // 研修会終了日
			add("deliveryStartDate"); // 配信開始日
			add("deliveryEndDate"); // 配信終了日
		}
	};
}
