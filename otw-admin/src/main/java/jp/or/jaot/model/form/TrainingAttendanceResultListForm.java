package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会出欠合否リストフォーム
 */
@Getter
@Setter
public class TrainingAttendanceResultListForm extends BaseListForm {

	/** 研修会出欠合否リスト項目 */
	List<TrainingAttendanceResultListItemForm> trainingAttendanceResults = new ArrayList<TrainingAttendanceResultListItemForm>();
}
