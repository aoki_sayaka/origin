package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント申請検索結果フォーム
 * @see OtherOrgPointApplicationEntity
 */
@Getter
@Setter
public class OtherOrgPointApplicationListItemForm extends BaseForm {
	
	/** 申請日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date applicationDate;
	
	/** 申請者 会員番号 */
	private Integer memberNo;
	
	/** 申請者 氏名 */
	private List<String> memberName = new ArrayList<String>();
	
	/** 受講日開始 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendingDateFrom;
	
	/** 受講日終了 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendingDateTo;
	
	/** 主催者 */
	private String organizerCd;
	
	/** 他団体SIG名 */
	private Integer otherOrganizationSigCd;
	
	/** 受講テーマ */
	private String thema;
	
	/** 日数 */
	private Integer attendingDays;
	
	/** ポイント数 */
	private Integer point;
	
	/** ポイント種別 */
	private String pointCategoryCd;
	
	/** 申請状態 */
	private String statusCd;
}
