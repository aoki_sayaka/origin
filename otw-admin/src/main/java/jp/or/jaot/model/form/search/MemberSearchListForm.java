package jp.or.jaot.model.form.search;

import jp.or.jaot.model.form.MemberForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員検索リストフォーム
 */
@Getter
@Setter
public class MemberSearchListForm extends MemberForm {

	/** 会員番号(範囲from) */
	private Integer memberFrom;

	/** 会員番号(範囲to) */
	private Integer memberTo;

	/** 氏名(フルネーム) */
	private String fullName;

	/** 検索結果の開始位置(初期位置:0) */
	private Integer startPosition = 0;

	/** 検索結果の取得件数(初期件数:0(指定なし)) */
	private Integer maxResult = 0;
}
