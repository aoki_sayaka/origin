package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会申込フォーム
 * @see TrainingApplicationEntity
 */
@Getter
@Setter
public class TrainingApplicationForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 繰上打診回答フラグ */
	private String advancedWinConsultFlag;

	/** 申込者分類 */
	private String applicantCategoryCd;

	/** 申込者番号 */
	private Integer memberNo;

	/** 申込状況 */
	private String applicationStatusCd;

	/** 症例提供 */
	private String caseProvideCd;

	/** 連絡方法 */
	private String contactType;

	/** 取消辞退日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date decliningDate;

	/** 入金期限日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date depositPeriodDate;

	/** 経験年数 */
	private Integer experienceYear;

	/** 氏名 */
	private String fullname;

	/** 氏名(カナ) */
	private String fullnameKana;

	/** 職種 */
	private String jobCategoryCd;

	/** 通知日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date notificationDate;

	/** 電話番号 */
	private String phoneNumber;

	/** 手続状況 */
	private String procedureStatusCd;

	/** 整理番号 */
	private Integer queueNo;

	/** 備考 */
	private String remarks;

	/** 督促フラグ */
	private String reminderFlag;

	/** 再提出回数 */
	private Integer resubmissionCount;

	/** 再提出日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date resubmissionDate;

	/** 対象疾患 */
	private String targetDisease;

	/** 勤務先住所1 */
	private String workPlaceAddress1;

	/** 勤務先住所2 */
	private String workPlaceAddress2;

	/** 勤務先住所3 */
	private String workPlaceAddress3;

	/** 勤務先名 */
	private String workPlaceNm;

	/** 勤務先都道府県 */
	private String workPlacePrefectureCd;

	/** 郵便番号 */
	private String workPlaceZipcode;
}
