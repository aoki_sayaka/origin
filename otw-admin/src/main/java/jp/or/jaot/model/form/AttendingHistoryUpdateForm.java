package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseFullForm;
import jp.or.jaot.model.entity.AttendingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 受講履歴フォーム(更新)
 * @see AttendingHistoryEntity
 */
@Getter
@Setter
public class AttendingHistoryUpdateForm extends BaseFullForm {

	/** ID */
	private Long id;

	/** 基礎研修履歴検索結果 */
	private List<BasicOtAttendingSummaryUpdateForm> basicOtAttendingSummaries = new ArrayList<BasicOtAttendingSummaryUpdateForm>();

	/** 臨床実習指導者履歴検索結果 */
	private List<ClinicalTrainingLeaderSummaryUpdateForm> clinicalTrainingLeaderSummaries = new ArrayList<ClinicalTrainingLeaderSummaryUpdateForm>();

	/** MTDLP履歴検索結果 */
	private List<MtdlpSummaryUpdateForm> mtdlpSummaries = new ArrayList<MtdlpSummaryUpdateForm>();

	/** 専門作業療法士受講履歴 */
	private List<ProfessionalOtAttendingSummaryUpdateForm> professionalOtAttendingSummaries = new ArrayList<ProfessionalOtAttendingSummaryUpdateForm>();

	/** 認定作業療法士研修履歴検索結果 */
	private List<QualifiedOtAttendingSummaryUpdateForm> qualifiedOtAttendingSummaries = new ArrayList<QualifiedOtAttendingSummaryUpdateForm>();

}
