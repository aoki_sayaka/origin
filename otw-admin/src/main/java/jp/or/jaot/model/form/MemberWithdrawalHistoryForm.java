package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.RecessHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 退会履歴フォーム
 * @see RecessHistoryEntity
 */
@Getter
@Setter
public class MemberWithdrawalHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 退会種類 */
	private String withdrawalType;

	/** 年度 */
	private Integer fiscalYear;

	/** 退会状況コード */
	private String stateCd;

	/** 届出受付日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date receptionDate;

	/** 退会年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date withdrawalDate;

	/** 会費状況 */
	private Integer memberFee;

	/** 退会理由 */
	private String withdrawalReason;

	/** 取下日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date cancellationDate;

	/** 備考 */
	private String remarks;

	/** 印刷年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date printDate;

}
