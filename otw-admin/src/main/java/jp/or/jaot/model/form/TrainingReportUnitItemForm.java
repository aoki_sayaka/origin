package jp.or.jaot.model.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 研修会報告書単一項目フォーム
 */
@Getter
@Setter
public class TrainingReportUnitItemForm extends TrainingReportForm {

	/** 定員数 */
	public Integer capacityCount;

	/** 確定数 */
	public Integer decisionCount;

	/** 参加人数(開催日1) */
	public Integer attendanceStatus1Count;

	/** 参加人数(開催日2) */
	public Integer attendanceStatus2Count;

	/** 参加人数(開催日3) */
	public Integer attendanceStatus3Count;

	/** 参加費 */
	private Integer entryFee;
}
