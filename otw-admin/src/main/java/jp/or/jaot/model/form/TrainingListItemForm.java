package jp.or.jaot.model.form;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会リスト項目フォーム
 * @see TrainingEntity
 */
@Getter
@Setter
public class TrainingListItemForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会種別 */
	private String trainingTypeCd;

	/** 講座名 */
	private String courseNm;

	/** 開催回数 */
	private Integer holdCount;

	/** 講座分類 */
	private String courseCategoryCd;

	/** 受付状況 */
	private String receptionStatusCd;

	/** 更新状況 */
	private String updateStatusCd;

	/** e-learningフラグ */
	private Boolean elearningFlag;

	/** 通知 */
	private String notification;

	/** 開催地 */
	private String venuePrefCd;

	/** 定員数 */
	private Integer capacityCount;

	/** 確定数 */
	private Integer decisionCount;

	/** 申込数 */
	private Integer subscriptionCount;

	/** 開催日(文字列) */
	private String eventDateText;

	/** 受付開始日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date receptionStartDate;

	/** 受付締切日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date receptionDeadlineDate;

	/** 研修会終了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date trainingEndDate;

	/** 中止日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date cancelDate;

	/** 協会HP掲載日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date hpPostedDate;

	/** 研修会更新者名 */
	private String trainingUpdateUser;

	/** 研修会更新日時 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp trainingUpdateDatetime;
}
