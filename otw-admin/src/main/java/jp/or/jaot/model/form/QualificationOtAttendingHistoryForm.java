package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.QualificationOtAttendingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴フォーム
 * @see QualificationOtAttendingHistoryEntity
 */
@Getter
@Setter
public class QualificationOtAttendingHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendDate;

	private String category;

	private String professionalField;

	private String trainingType;

	private String trainingName;

	private Integer seq;

	private String remissionCauseCd;

	private String remissionQualificationCd;

	private String acceptanceCd;

	private String promoterCd;

	private String othersSigCd;

	private String content;

	private String pointTypeCd;

	private Integer days;

	private Integer point;

	private String facilityNo;

	private boolean usePoint;

	private String collationMemberName;

	private String collationRate;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date startDate;

	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endDate;

	private boolean checkMarked;

	private String remarks;

}
