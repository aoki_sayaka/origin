package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会リストフォーム
 */
@Getter
@Setter
public class TrainingListForm extends BaseListForm {

	/** 研修会リスト項目 */
	List<TrainingListItemForm> trainings = new ArrayList<TrainingListItemForm>();
}
