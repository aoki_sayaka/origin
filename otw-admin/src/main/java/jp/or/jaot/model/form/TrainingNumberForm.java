package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingNumberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修番号マスタフォーム
 * @see TrainingNumberEntity
 */
@Getter
@Setter
public class TrainingNumberForm extends BaseForm {

	/** ID */
	private Long id;

	/** 年度 */
	private Integer fiscalYear;

	/** 研修番号 */
	private String trainingNo;

	/** 内容 */
	private String content;
}
