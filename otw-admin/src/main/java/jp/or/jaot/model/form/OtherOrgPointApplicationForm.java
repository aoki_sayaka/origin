package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント申請検索結果フォーム
 * @see OtherOrgPointApplicationEntity
 */
@Getter
@Setter
public class OtherOrgPointApplicationForm extends OtherOrgPointApplicationUpdateForm {
	
	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 申請者コード */
	private String applicantCd;

	/** 申請井状況コード */
	private String applicationStatusCd;

	/** 受講日（自） */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendingDateFrom;
	
	/** 受講日（至） */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendingDateTo;	
	
	/** 受講日チェック */
	private boolean attendingDateCheck;

	/** 研修日数 */
	private String attendingDays;
	
	/** 研修日数チェック */
	private boolean attendingDaysCheck;
	
	/** 主催者コード */
	private String organizerCd;
	
	/** 主催者チェック */
	private boolean organizerCheck;
	
	/** 主催者連絡先 */
	private Integer organizerTelNo;
	
	/** 他団体・SIGコード */
	private Integer otherOrganizationSigCd;
	
	/** 他団体・SIGチェック */
	private boolean otherOrganizationSigCheck;
	
	/** 受講テーマ */
	private String thema;
	
	/** ポイント種別 */
	private String pointCategoryCd;
	
	/** ポイント種別チェック */
	private boolean pointCategoryCheck;
	
	/** ポイント */
	private Integer point;
	
	/** ポイントチェック */
	private boolean pointCheck;
	
	/** 添付ファイルチェック */
	// private boolean attachmentFileCheck;
	
	/** 承認フラグ */
	private Integer approvalFlag;
	
	/** 事務局確認日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date secretariatConfirmDate;
	
	/** 仮登録フラグ */
	private Integer tempFlag;
	
	/** 作成日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date createDateTime;

	/** 作成者名 */
	private String createUser;

	/** 更新日時 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date updateDateTime;

	/** 更新者名 */
	private String updateUser;

	/** 削除フラグ */
	private Boolean deleted;

	/** バージョン */
	private Integer versionNo;

	

}
