package jp.or.jaot.model.form;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.AttendingHistoryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会会員リスト項目フォーム
 * @see MemberEntity
 * @see QualifiedOtAttendingSummaryEntity
 * @see AttendingHistoryEntity
 */
@Getter
@Setter
public class TrainingMemberListItemForm extends BaseForm {

	/** 会員.ID */
	private Long id;

	/** 会員.会員番号 */
	private Integer memberNo;

	/** 会員.氏名(フルネーム:半角空白区切り) */
	private String fullNameFormat;

	/** 会員.勤務先部署名 */
	private String workingFacilityUnitName;

	/** 認定作業療法士研修履歴.認定年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationDate;

	/** 認定作業療法士研修履歴.認定期間満了日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date qualificationPeriodDate;

	/** 受講履歴.件数 */
	private Integer attendingHistoryCount;

	/** 現在日時 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Timestamp currentDatetime;

	// TODO
}
