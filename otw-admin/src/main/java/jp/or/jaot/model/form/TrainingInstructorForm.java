package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingInstructorEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会講師実績フォーム
 * @see TrainingInstructorEntity
 */
@Getter
@Setter
public class TrainingInstructorForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 講師番号 */
	private Integer memberNo;

	/** 講師名 */
	private String instructorNm;

	/** 表示順 */
	private Integer displaySeqNo;

	/** 講師区分 */
	private String instructorTypeCd;

	/** 講師分類 */
	private String instructorCategoryCd;

	/** 講師上長宛公文書有無 */
	private String docToSuperiorsCd;

	/** 講師上長宛公文書印刷フラグ */
	private Boolean docToSuperiorsPrintFlag;

	/** 講師上長施設名 */
	private String docToSuperiorsFacilityNm;

	/** 講師上長宛名 */
	private String docToSuperiorsNm;

	/** 講師上長宛公文書発送日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date docToSuperiorsSendDate;

	/** 講師本人宛公文書有無 */
	private String docToPersonCd;

	/** 講師本人宛公文書印刷フラグ */
	private Boolean docToPersonPrintFlag;

	/** 講師本人施設名 */
	private String docToPersonFacilityNm;

	/** 講師本人宛名 */
	private String docToPersonNm;

	/** 講師本人公文書発送日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date docToPersonSendDate;

	/** 謝金記載区分 */
	private String rewardDescriptionCategoryCd;

	/** 謝金記載 */
	private String rewardDescription;

	/** 謝金区分 */
	private String rewardCategoryCd;

	/** 講師ポイント */
	private Integer instructorPoint;

	/** 講師実績フラグ */
	private String instructorAchievementFlag;

	/** 削除フラグ */
	private Boolean deleted;
}
