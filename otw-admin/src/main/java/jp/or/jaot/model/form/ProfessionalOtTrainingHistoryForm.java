package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士研修受講履歴フォーム
 * @see ProfessionalOtQualifyHistoryEntity
 */
@Getter
@Setter
public class ProfessionalOtTrainingHistoryForm extends BaseForm {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	/** 研修会コード */
	private String trainingCd;

	/** 年度 */
	private String fiscalYear;

	/** 主催者コード */
	private String promoterCd;

	/** カテゴリコード */
	private String categoryCd;

	/** 専門分野 */
	private String professionalField;

	/** グループコード */
	private String groupCd;

	/** クラスコード */
	private String classCd;

	private String branchCd;

	/** 枝番 */
	private Integer seq;

	/** 研修名 */
	private String trainingName;

	/** 研修受講年月日 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date attendDate;

	/** 専門研究・開発区分コード */
	private String professionalRdCategoryCd;

	/** 免除理由コード */
	private String remissionCauseCd;

	/** 免除資格コード */
	private String remissionQualificationCd;

}
