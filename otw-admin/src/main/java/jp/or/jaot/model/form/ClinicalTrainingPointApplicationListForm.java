package jp.or.jaot.model.form;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.BaseListForm;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習ポイント申請検索結果リストフォーム
 */
@Getter
@Setter
public class ClinicalTrainingPointApplicationListForm extends BaseListForm {
	
	/** 臨床実習ポイント申請検索結果 */
	List<ClinicalTrainingPointApplicationListItemForm> clinicalResults = new ArrayList<ClinicalTrainingPointApplicationListItemForm>();
}