package jp.or.jaot.model.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.HandbookMigrationApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 手帳移行申請検索条件フォーム
 * @see HandbookMigrationApplicationEntity
 */
@Getter
@Setter
public class HandbookMigrationApplicationSearchForm extends BaseForm {
		
	/** 申請日(From) */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date applicationDateFrom;

	/** 申請日(To) */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date applicationDateTo;

	/** 申請状態 */
	private String applicationStatusCd;

	/** 申請者 会員番号(From) */
	private Integer memberNoFrom;

	/** 申請者 会員番号(To) */
	private Integer memberNoTo;
	
	/** 検索結果の開始位置(初期位置:0) */
	private Integer startPosition = 0;

	/** 検索結果の取得件数(初期件数:0(指定なし)) */
	private Integer maxResult = 0;
}
