package jp.or.jaot.model.form.search;

import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込検索リストフォーム
 */
@Getter
@Setter
public class TrainingTempApplicationSearchListForm extends TrainingTempApplicationSearchForm {

	/** 検索結果の開始位置(初期位置:0) */
	private Integer startPosition = 0;

	/** 検索結果の取得件数(初期件数:0(指定なし)) */
	private Integer maxResult = 0;
}
