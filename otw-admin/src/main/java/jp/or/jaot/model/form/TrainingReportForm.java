package jp.or.jaot.model.form;

import jp.or.jaot.core.model.BaseForm;
import jp.or.jaot.model.entity.TrainingReportEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会報告書フォーム
 * @see TrainingReportEntity
 */
@Getter
@Setter
public class TrainingReportForm extends BaseForm {

	/** ID */
	private Long id;

	/** 研修会番号 */
	private String trainingNo;

	/** 対象 */
	private String targetCd;

	/** 予算 */
	private Integer budget;

	/** 収支 */
	private Integer balance;

	/** 支出 */
	private Integer expenditure;

	/** 参加者の評価 */
	private String participantsEvaluationCd;

	/** 運営担当者会員番号 */
	private Integer operatorMemberNo;

	/** 運営担当者氏名 */
	private String operatorFullname;

	/** 申込者数 */
	private Integer applicantCount;

	/** 効果判定1評価 */
	private String effectJudge1Cd;

	/** 効果判定1 */
	private String effectJudge1Content;

	/** 効果判定2評価 */
	private String effectJudge2Cd;

	/** 効果判定2 */
	private String effectJudge2Content;

	/** 各連絡・協力評価 */
	private String cooperationEvaluationCd;

	/** 各連絡・協力 */
	private String cooperationEvaluationContent;

	/** 職場電話話回数 */
	private Integer workplacePhoneUseCount;

	/** 職場電話通話総時間 */
	private Integer workplacePhoneUseHour;

	/** 職場FAX信件数 */
	private Integer workplaceFaxReceiveCount;

	/** 職場FAX受信件数 */
	private Integer workplaceFaxSendCount;

	/** 個人電話話回数 */
	private Integer personalPhoneUseCount;

	/** 個人電話通話総時間 */
	private Integer personalPhoneUseHour;

	/** 総括 */
	private String summary;
}
