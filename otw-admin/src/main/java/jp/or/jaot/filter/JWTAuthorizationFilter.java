package jp.or.jaot.filter;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.ExpiredJwtException;
import jp.or.jaot.core.util.JwtUtil;
import jp.or.jaot.model.entity.UserEntity;
import jp.or.jaot.service.SecretariatUserDetailsService;

/**
 * JWT認可フィルタ
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

	/** インメモリ認証フラグ */
	@Value("${jwt.authentication.memory}")
	private Boolean inMemory;

	/** 事務局ユーザサービス(詳細) */
	@Autowired
	private SecretariatUserDetailsService secretariatUserDetailsSrv;

	/**
	 * コンストラクタ
	 * @param authenticationManager
	 */
	public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		// 認証情報設定
		try {
			String header = req.getHeader(JwtUtil.HEADER_STRING);
			if (header != null && header.startsWith(JwtUtil.TOKEN_PREFIX)) {
				SecurityContextHolder.getContext().setAuthentication(getAuthentication(req));
				logger.info("doFilterInternal()  : header={}", header);
			}
		} catch (ExpiredJwtException e) {
			logger.trace("doFilterInternal()  : Security exception {}", e);
		}

		chain.doFilter(req, res);
	}

	/**
	 * 認証
	 * @param request HttpServletRequest
	 * @return ユーザパスワード認証情報
	 */
	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest req) {

		// 認証情報取得
		String token = req.getHeader(JwtUtil.HEADER_STRING);
		String principal = JwtUtil.getPrincipal(token);

		// ログ出力
		StringBuffer url = req.getRequestURL();
		String uri = req.getRequestURI();
		logger.info("getAuthentication(inMemory={}) : principal={}, url={}, uri={}", inMemory, principal, url, uri);
		if (StringUtils.isNotEmpty(principal)) {

			// 事務局ユーザエンティティ取得
			UserEntity userEntity = null;
			if (inMemory) {
				userEntity = new UserEntity();
				userEntity.setLoginId(principal);
			} else {
				// エンティティ取得
				if ((userEntity = (UserEntity) secretariatUserDetailsSrv.loadUserByUsername(principal)) != null) {
					// 利用可能状態取得
					if (!secretariatUserDetailsSrv.isAvailablePathOrKey(userEntity.getId(), uri)) {
						return null; // 利用不可
					}
				}
			}

			// 返却
			return new UsernamePasswordAuthenticationToken(userEntity, null, new ArrayList<>());
		}

		return null;
	}
}