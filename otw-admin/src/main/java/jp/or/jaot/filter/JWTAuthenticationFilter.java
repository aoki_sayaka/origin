package jp.or.jaot.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.or.jaot.core.util.JwtUtil;
import jp.or.jaot.model.form.LoginForm;

/**
 * JWT認証フィルタ
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

	/**
	 * コンストラクタ
	 * @param authenticationManager
	 */
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.setAuthenticationManager(authenticationManager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException {

		try {
			LoginForm loginForm = new LoginForm();

			// GET形式
			loginForm.setUsername(req.getParameter("username"));
			loginForm.setPassword(req.getParameter("password"));

			// JSON形式
			if (!loginForm.isValid()) {
				loginForm = new ObjectMapper().readValue(req.getInputStream(), LoginForm.class);
			}

			// ログ出力
			logger.info("attemptAuthentication() : url={}, username={}", req.getRequestURL(), loginForm.getUsername());

			// 認証
			return getAuthenticationManager().authenticate(
					new UsernamePasswordAuthenticationToken(loginForm.getUsername(), loginForm.getPassword()));

		} catch (BadCredentialsException e) {
			res.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // HTTPステータスコード設定(認証失敗)
			logger.error(e.getMessage());
		} catch (Exception e) {
			res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR); // HTTPステータスコード設定(内部エラー)
			logger.error(e.getMessage());
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {

		// ヘッダ設定
		UserDetails user = (UserDetails) auth.getPrincipal();
		String token = null;
		if (user != null) {

			// トークン取得
			token = JwtUtil.createToken(user);

			// ヘッダ設定
			if (StringUtils.isNotEmpty(token)) {
				res.addHeader(JwtUtil.HEADER_STRING, token); // トークン設定
				res.addHeader(JwtUtil.EXPOSE_HEADERS, JwtUtil.HEADER_STRING); // トークン許可設定
			}
		}

		// ログ出力
		logger.info("successfulAuthentication() : url={}, user={}, token={}", req.getRequestURL(), user, token);
	}
}