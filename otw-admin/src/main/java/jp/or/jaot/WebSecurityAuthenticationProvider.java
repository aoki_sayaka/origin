package jp.or.jaot;

import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import jp.or.jaot.core.util.CipherUtil;
import jp.or.jaot.model.dto.UserLoginHistoryDto;
import jp.or.jaot.model.entity.UserEntity;
import jp.or.jaot.model.entity.UserLoginHistoryEntity;
import jp.or.jaot.service.SecretariatUserDetailsService;
import jp.or.jaot.service.UserLoginHistoryService;

/**
 * 認証プロバイダ
 */
@Configuration
public class WebSecurityAuthenticationProvider implements AuthenticationProvider {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(WebSecurityAuthenticationProvider.class);

	/** 事務局ユーザサービス(詳細) */
	@Autowired
	private SecretariatUserDetailsService secretariatUserDetailsSrv;

	/** 事務局サイトログイン履歴サービス */
	@Autowired
	private UserLoginHistoryService userLoginHistoryService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		// ログイン情報取得
		String username = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();

		// ユーザ詳細取得
		UserDetails userDetails = null;
		try {
			userDetails = secretariatUserDetailsSrv.loadUserByUsername(username);
		} catch (Exception e) {
			String msg = String.format("認証対象のユーザが存在しません : username=%s", username);
			throw new BadCredentialsException(msg);
		}

		// パスワード認証(AES)
		String decrypt = CipherUtil.decrypt(userDetails.getPassword());
		if (!password.equals(decrypt)) {
			String msg = String.format("認証できません : username=%s", username);
			throw new BadCredentialsException(msg);
		}

		// ログイン履歴追加
		try {
			enterUserLoginHistory(((UserEntity) userDetails).getId());
		} catch (Exception e) {
			String msg = String.format("ログイン履歴を追加できません : username=%s", username);
			throw new BadCredentialsException(msg);
		}

		// ログ出力
		logger.info("authenticate() : username={}", username);

		return new UsernamePasswordAuthenticationToken(userDetails, authentication.getCredentials());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

	/**
	 * ログイン履歴追加
	 * @param userId 事務局ユーザID
	 */
	private void enterUserLoginHistory(Long userId) {

		// DTO生成
		UserLoginHistoryDto enterDto = UserLoginHistoryDto.createSingle();
		UserLoginHistoryEntity enterEntity = enterDto.getFirstEntity();
		enterEntity.setUserId(userId); // 事務局ユーザID
		enterEntity.setLoginDatetime(new Timestamp(System.currentTimeMillis())); // ログイン日時(システム現在日時)

		// 登録
		userLoginHistoryService.enter(enterDto);
	}
}
