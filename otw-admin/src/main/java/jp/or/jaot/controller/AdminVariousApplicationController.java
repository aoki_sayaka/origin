package jp.or.jaot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.OtherOrgPointApplicationDto;
import jp.or.jaot.model.dto.search.ClinicalTrainingPointApplicationSearchDto;
import jp.or.jaot.model.dto.search.HandbookMigrationApplicationSearchDto;
import jp.or.jaot.model.dto.search.OtherOrgPointApplicationSearchDto;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;
import jp.or.jaot.model.entity.composite.ClinicalTrainingPointApplicationCompositeEntity;
import jp.or.jaot.model.entity.composite.HandbookMigrationApplicationCompositeEntity;
import jp.or.jaot.model.entity.composite.OtherOrgPointApplicationCompositeEntity;
import jp.or.jaot.model.form.ClinicalTrainingPointApplicationListForm;
import jp.or.jaot.model.form.ClinicalTrainingPointApplicationListItemForm;
import jp.or.jaot.model.form.ClinicalTrainingPointApplicationSearchForm;
import jp.or.jaot.model.form.HandbookMigrationApplicationListForm;
import jp.or.jaot.model.form.HandbookMigrationApplicationListItemForm;
import jp.or.jaot.model.form.HandbookMigrationApplicationSearchForm;
import jp.or.jaot.model.form.OtherOrgPointApplicationAttachmentForm;
import jp.or.jaot.model.form.OtherOrgPointApplicationListForm;
import jp.or.jaot.model.form.OtherOrgPointApplicationListItemForm;
import jp.or.jaot.model.form.OtherOrgPointApplicationSearchForm;
import jp.or.jaot.model.form.OtherOrgPointApplicationUpdateForm;
import jp.or.jaot.service.ClinicalTrainingPointApplicationService;
import jp.or.jaot.service.HandbookMigrationApplicationService;
import jp.or.jaot.service.OtherOrgPointApplicationService;
import lombok.AllArgsConstructor;

/**
 * 各種申請コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "various_application", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class AdminVariousApplicationController extends BaseController {
	

	/** 他団体・SIGポイント申請 */
	private final OtherOrgPointApplicationService otherOrgPointApplicationSrv;

	/** 手帳移行申請 */
	private final HandbookMigrationApplicationService handbookMigrationApplicationSrv;
	
	/** 臨床実習ポイント申請申請 */
	private final ClinicalTrainingPointApplicationService clinicalTrainingPointApplicationSrv;

	/**
	 * 条件検索(他団体・SIGポイント申請)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_other_org_point_application")
	public ResponseEntity<OtherOrgPointApplicationListForm> searchOtherOrgPointApplication(
			@RequestBody @Validated OtherOrgPointApplicationSearchForm otherOrgPointApplicationSearchForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		OtherOrgPointApplicationSearchDto searchDto = new OtherOrgPointApplicationSearchDto();
		BeanUtil.copyProperties(otherOrgPointApplicationSearchForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		OtherOrgPointApplicationListForm form = new OtherOrgPointApplicationListForm();
		form.setOtherOrgResults(otherOrgPointApplicationSrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createOtherOrgPointApplicationListItemForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)
		return ResponseEntity.ok(form);
	}

	/**
	 * 更新<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "update_other_org_point_application")
	public ResponseEntity<String> updateOtherOrgPointApplication(
			@RequestBody @Validated OtherOrgPointApplicationUpdateForm OtherOrgPointApplicationUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		OtherOrgPointApplicationCompositeEntity compositeEntity = otherOrgPointApplicationSrv
				.getSearchResult(OtherOrgPointApplicationUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		OtherOrgPointApplicationDto updateDto = OtherOrgPointApplicationDto.createSingle();
		OtherOrgPointApplicationCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils
				.isTrue(OtherOrgPointApplicationUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// 他団体・SIGポイント申請
		OtherOrgPointApplicationEntity updateOtherOrgPointApplicationEntity = updateCompositeEntity
				.getOtherOrgPointApplicationEntity();
		BeanUtil.copyProperties(compositeEntity.getOtherOrgPointApplicationEntity(),
				updateOtherOrgPointApplicationEntity, false); // 現在値
		if (isIncrementalUpdate) {
			BeanUtil.copyProperties(OtherOrgPointApplicationUpdateForm, updateOtherOrgPointApplicationEntity,
					true); // 更新値(差分)
		} else {
			BeanUtil.copyProperties(OtherOrgPointApplicationUpdateForm, updateOtherOrgPointApplicationEntity,
					false); // 更新値(上書)
		}
		updateCompositeEntity.setOtherOrgPointApplicationEntity(updateOtherOrgPointApplicationEntity);

		// 他団体・SIGポイント添付ファイル
		List<OtherOrgPointApplicationAttachmentEntity> updateOtherOrgPointApplicationAttachmentEntities = new ArrayList<OtherOrgPointApplicationAttachmentEntity>();
		List<OtherOrgPointApplicationAttachmentEntity> qualificationEntities = compositeEntity
				.getOtherOrgPointApplicationAttachmentEntities();
		for (OtherOrgPointApplicationAttachmentForm form : OtherOrgPointApplicationUpdateForm
				.getOtherOrgPointApplicationAttachments()) {

			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(qualificationEntities)) {
				for (OtherOrgPointApplicationAttachmentEntity entity : qualificationEntities) {
					if (entity.getId().equals(form.getId())) {
						OtherOrgPointApplicationAttachmentEntity updateEntity = new OtherOrgPointApplicationAttachmentEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateOtherOrgPointApplicationAttachmentEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				OtherOrgPointApplicationAttachmentEntity insertEntity = new OtherOrgPointApplicationAttachmentEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateOtherOrgPointApplicationAttachmentEntities.add(insertEntity);
			}
		}

		// 他団体・SIGポイント添付ファイル(処理対象)
		updateCompositeEntity
				.setOtherOrgPointApplicationAttachmentEntities(updateOtherOrgPointApplicationAttachmentEntities);

		// 実行
		otherOrgPointApplicationSrv.update(updateDto);

		return ResponseEntity.ok("updateOtherOrgPointApplications ok");
	}

	/**
	 * 他団体・SIGポイント申請フォーム生成
	 * @param compositeEntity 他団体・SIGポイント申請エンティティ(複合)
	 * @return 他団体・SIGポイント申請フォーム
	 */
	/*
	private OtherOrgPointApplicationForm createOtherOrgPointApplicationForm(
			OtherOrgPointApplicationCompositeEntity compositeEntity) {

		// 他団体・SIGポイント申請フォーム
		OtherOrgPointApplicationForm otherOrgPointApplicationForm = BeanUtil.createCopyProperties(
				compositeEntity.getOtherOrgPointApplicationEntity(), OtherOrgPointApplicationForm.class,
				false);

		// 他団体・SIGポイント添付ファイル
		List<OtherOrgPointApplicationAttachmentEntity> otherOrgPointApplicationAttachmentEntities = compositeEntity
				.getOtherOrgPointApplicationAttachmentEntities();
		if (!ListUtils.isEmpty(otherOrgPointApplicationAttachmentEntities)) {
			otherOrgPointApplicationForm
					.setOtherOrgPointApplicationAttachments(otherOrgPointApplicationAttachmentEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, OtherOrgPointApplicationAttachmentForm.class,
									false))
							.collect(Collectors.toList()));
		}

		return otherOrgPointApplicationForm;

	}
	*/
	private OtherOrgPointApplicationListItemForm createOtherOrgPointApplicationListItemForm(OtherOrgPointApplicationCompositeEntity compositeEntity) {

		// 他団体・SIGポイント申請フォーム
		OtherOrgPointApplicationListItemForm form = BeanUtil.createCopyProperties(compositeEntity.getOtherOrgPointApplicationEntity(),
				OtherOrgPointApplicationListItemForm.class, false);

		// 会員情報
		List<MemberEntity> memberEntities = compositeEntity.getMemberEntities();
		if (!CollectionUtils.isEmpty(memberEntities)) {
			form.setMemberName(memberEntities.stream()
					.map(E -> E.getFullName())
					.collect(Collectors.toList()));
		}

		return form;
	}

	/**
	 * 条件検索(手帳移行申請)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_handbook_migration_application")
	public ResponseEntity<HandbookMigrationApplicationListForm> searchHandbookMigrationApplication(
			@RequestBody @Validated HandbookMigrationApplicationSearchForm handbookMigrationApplicationSearchForm, Errors errors) {
		// バリデーション実行
		doValidation(errors);
		
		// 検索条件DTO生成
		HandbookMigrationApplicationSearchDto searchDto = new HandbookMigrationApplicationSearchDto();
		BeanUtil.copyProperties(handbookMigrationApplicationSearchForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		// 検索結果フォーム取得
		HandbookMigrationApplicationListForm form = new HandbookMigrationApplicationListForm();
		form.setHandBookResults(handbookMigrationApplicationSrv
				.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> createHandbookMigrationApplicationListItemForm(E))
				.collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)
		return ResponseEntity.ok(form);
	}
	
	private HandbookMigrationApplicationListItemForm createHandbookMigrationApplicationListItemForm(HandbookMigrationApplicationCompositeEntity compositeEntity) {

		// 手帳移行申請フォーム
		HandbookMigrationApplicationListItemForm form = BeanUtil.createCopyProperties(compositeEntity.getHandbookMigrationApplicationEntity(),
				HandbookMigrationApplicationListItemForm.class, false);

		// 会員情報
		List<MemberEntity> memberEntities = compositeEntity.getMemberEntities();
		if (!CollectionUtils.isEmpty(memberEntities)) {
			form.setMemberName(memberEntities.stream()
					.map(E -> E.getFullName())
					.collect(Collectors.toList()));
		}

		return form;
	}
	
	/**
	 * 条件検索(臨床実習ポイント申請)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_clinical_training_point_application")
	public ResponseEntity<ClinicalTrainingPointApplicationListForm> searchClinicalTrainingPointApplication(
			@RequestBody @Validated ClinicalTrainingPointApplicationSearchForm clinicalTrainingPointApplicationSearchForm, Errors errors) {
		// バリデーション実行
		doValidation(errors);
		
		// 検索条件DTO生成
		ClinicalTrainingPointApplicationSearchDto searchDto = new ClinicalTrainingPointApplicationSearchDto();
		BeanUtil.copyProperties(clinicalTrainingPointApplicationSearchForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		// 検索結果フォーム取得
		ClinicalTrainingPointApplicationListForm form = new ClinicalTrainingPointApplicationListForm();
		form.setClinicalResults(clinicalTrainingPointApplicationSrv
				.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> createClinicalTrainingPointApplicationListItemForm(E))
				.collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)
		return ResponseEntity.ok(form);
	}
	
	private ClinicalTrainingPointApplicationListItemForm createClinicalTrainingPointApplicationListItemForm(ClinicalTrainingPointApplicationCompositeEntity compositeEntity) {

		// 臨床実習ポイント申請フォーム
		ClinicalTrainingPointApplicationListItemForm form = BeanUtil.createCopyProperties(compositeEntity.getClinicalTrainingPointApplicationEntity(),
				ClinicalTrainingPointApplicationListItemForm.class, false);

		// 会員情報
		List<MemberEntity> memberEntities = compositeEntity.getMemberEntities();
		if (!CollectionUtils.isEmpty(memberEntities)) {
			form.setMemberName(memberEntities.stream()
					.map(E -> E.getFullName())
					.collect(Collectors.toList()));
		}
		
		// 養成校情報
		List<FacilityEntity> facilityEntities = compositeEntity.getFacilityEntities();
		if (!CollectionUtils.isEmpty(facilityEntities)) {
			form.setFacilityName(facilityEntities.stream()
					.map(E -> E.getName())
					.collect(Collectors.toList()));
		}

		return form;
	}
}
