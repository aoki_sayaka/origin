package jp.or.jaot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.GeneralCodeDto;
import jp.or.jaot.model.dto.search.GeneralCodeSearchDto;
import jp.or.jaot.model.dto.search.ZipcodeSearchDto;
import jp.or.jaot.model.entity.GeneralCodeEntity;
import jp.or.jaot.model.entity.GeneralCodeId;
import jp.or.jaot.model.form.GeneralCodeForm;
import jp.or.jaot.model.form.ZipcodeForm;
import jp.or.jaot.model.form.ZipcodeListForm;
//import jp.or.jaot.model.form.QualificationForm;
import jp.or.jaot.model.form.search.GeneralCodeSearchForm;
import jp.or.jaot.model.form.search.GeneralCodeSearchMultipleForm;
import jp.or.jaot.model.form.search.ZipcodeSearchListForm;
import jp.or.jaot.service.GeneralCodeService;
import jp.or.jaot.service.ZipcodeService;
import lombok.AllArgsConstructor;

/**
 * 共通コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "common", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class AdminCommonController extends BaseController {

	/** 汎用コードサービス */
	private final GeneralCodeService generalCodeSrv;

	/** 郵便番号サービス */
	private final ZipcodeService zipcodeSrv;

	/**
	 * 条件検索(汎用コード)<br>
	 * [実装メモ]<br>
	 * 汎用コードエンティティのアノテーションで子要素リストを持たせた場合、複数回のDBアクセスが発生してしまうため、
	 * 対象となるエンティティを一括取得後に子要素のフォームリストを追加する。<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "search_general_code")
	public ResponseEntity<List<GeneralCodeForm>> searchGeneralCode(
			@RequestBody @Validated GeneralCodeSearchForm generalCodeForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		String id = generalCodeForm.getId();
		String code = generalCodeForm.getCode();
		GeneralCodeId[] generalCodeIds = { new GeneralCodeId(id, null) }; // コードID
		GeneralCodeSearchDto searchDto = new GeneralCodeSearchDto();
		searchDto.setGeneralCodeIds(generalCodeIds); // 汎用コードID配列

		// 検索結果取得(条件)
		GeneralCodeDto dto = generalCodeSrv.getSearchResultByCondition(searchDto);
		List<GeneralCodeForm> forms = new ArrayList<GeneralCodeForm>();
		for (GeneralCodeEntity entity : dto.getEntities()) {

			// コードID指定あり
			if (StringUtils.isNotEmpty(id) && !id.equals(entity.getGeneralCodeId().getId())) {
				continue;
			}

			// コード値指定あり
			if (StringUtils.isNotEmpty(code) && !code.equals(entity.getGeneralCodeId().getCode())) {
				continue;
			}

			// フォーム追加
			forms.add(createGeneralCodeForm(dto.getEntities(), entity));
		}

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(汎用コード(複数指定))<br>
	 * [実装メモ]<br>
	 * 汎用コードエンティティのアノテーションで子要素リストを持たせた場合、複数回のDBアクセスが発生してしまうため、
	 * 対象となるエンティティを一括取得後に子要素のフォームリストを追加する。<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "search_multiple_general_code")
	public ResponseEntity<List<GeneralCodeForm>> searchMultipleGeneralCode(
			@RequestBody @Validated GeneralCodeSearchMultipleForm generalCodeMultipleForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		GeneralCodeId[] generalCodeIds = generalCodeMultipleForm.getGeneralCodes().stream()
				.map(E -> new GeneralCodeId(E.getId(), null)).toArray(GeneralCodeId[]::new);
		GeneralCodeSearchDto searchDto = new GeneralCodeSearchDto();
		searchDto.setGeneralCodeIds(generalCodeIds); // 汎用コードID配列

		// 検索結果取得(条件)
		GeneralCodeDto dto = generalCodeSrv.getSearchResultByCondition(searchDto);
		List<GeneralCodeForm> forms = new ArrayList<GeneralCodeForm>();
		for (GeneralCodeEntity entity : dto.getEntities()) {
			for (GeneralCodeSearchForm form : generalCodeMultipleForm.getGeneralCodes()) {

				// コードID指定あり
				String id = form.getId();
				if (StringUtils.isNotEmpty(id) && !id.equals(entity.getGeneralCodeId().getId())) {
					continue;
				}

				// コード値指定あり
				String code = form.getCode();
				if (StringUtils.isNotEmpty(code) && !code.equals(entity.getGeneralCodeId().getCode())) {
					continue;
				}

				// フォーム追加
				forms.add(createGeneralCodeForm(dto.getEntities(), entity));
			}
		}

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(郵便番号リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_zipcode")
	public ResponseEntity<ZipcodeListForm> searchListZipcode(@RequestBody @Validated ZipcodeSearchListForm zipcodeForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ZipcodeSearchDto searchDto = new ZipcodeSearchDto();
		BeanUtil.copyProperties(zipcodeForm, searchDto, false);

		// 検索結果フォーム取得
		ZipcodeListForm form = new ZipcodeListForm();
		form.setZipcodes(zipcodeSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
				.map(E -> BeanUtil.createCopyProperties(E, ZipcodeForm.class, false)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 汎用コードフォーム生成
	 * @param entities 汎用コードエンティティリスト
	 * @param entity 汎用コードエンティティ
	 * @return 汎用コードフォーム
	 */
	private GeneralCodeForm createGeneralCodeForm(List<GeneralCodeEntity> entities, GeneralCodeEntity entity) {

		// 属性
		GeneralCodeForm form = new GeneralCodeForm();
		BeanUtil.copyProperties(entity, form, false);
		form.setId(entity.getGeneralCodeId().getId()); // コードID
		form.setCode(entity.getGeneralCodeId().getCode()); // コード値

		// 子要素
		for (GeneralCodeEntity childEntity : entities) {
			if (entity.getGeneralCodeId().getCode().equals(childEntity.getParentCode())) {
				form.getChildren().add(createGeneralCodeForm(entities, childEntity));
			}
		}

		return form;
	}

	// TODO
}
