package jp.or.jaot.controller.common;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import lombok.Getter;

/**
 * 型不一致メッセージクラス
 */
@Configuration
@PropertySource("classpath:TypeMismatchMessages.properties")
@Getter
public class TypeMismatchMessagesConfig {

	/** チェック対象 */
	private static final String[] DEF_TYPE_MISMATCHES = { "typeMismatch.java.math.BigDecimal",
			"typeMismatch.java.lang.Integer", "typeMismatch.java.lang.Long", "typeMismatch.java.time.LocalDate",
			"typeMismatch.java.time.LocalDateTime", "typeMismatch.java.time.LocalTime", "typeMismatch.int" };

	/** 外部設定値 */
	private final Environment env;

	/** ペアマップ */
	private final Map<String, String> pairMap = new HashMap<String, String>();

	/**
	 * コンストラクタ(インジェクション)
	 * @param 外部設定値
	 */
	@Autowired
	public TypeMismatchMessagesConfig(Environment env) {
		this.env = env;
		for (String key : DEF_TYPE_MISMATCHES) {
			pairMap.put(key, env.getProperty(key));
		}
	}

	/**
	 * メッセージ取得
	 * @param target チェック対象
	 * @param defaultMessage デフォルトメッセージ
	 * @return メッセージ
	 */
	public String getMessage(String target, String defaultMessage) {
		for (Map.Entry<String, String> entry : pairMap.entrySet()) {
			if (StringUtils.contains(target, entry.getKey())) {
				return entry.getValue();
			}
		}
		return defaultMessage;
	}
}
