package jp.or.jaot.controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.dto.TrainingApplicationDto;
import jp.or.jaot.model.dto.TrainingAttendanceResultDto;
import jp.or.jaot.model.dto.TrainingDto;
import jp.or.jaot.model.dto.TrainingInstructorDto;
import jp.or.jaot.model.dto.TrainingOperatorDto;
import jp.or.jaot.model.dto.TrainingOrganizerDto;
import jp.or.jaot.model.dto.TrainingReportDto;
import jp.or.jaot.model.dto.TrainingTempApplicationDto;
import jp.or.jaot.model.dto.search.TrainingApplicationSearchDto;
import jp.or.jaot.model.dto.search.TrainingAttendanceResultSearchDto;
import jp.or.jaot.model.dto.search.TrainingInstructorSearchDto;
import jp.or.jaot.model.dto.search.TrainingMemberSearchDto;
import jp.or.jaot.model.dto.search.TrainingOperatorSearchDto;
import jp.or.jaot.model.dto.search.TrainingOrganizerSearchDto;
import jp.or.jaot.model.dto.search.TrainingReportSearchDto;
import jp.or.jaot.model.dto.search.TrainingSearchDto;
import jp.or.jaot.model.dto.search.TrainingTempApplicationSearchDto;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import jp.or.jaot.model.entity.TrainingInstructorEntity;
import jp.or.jaot.model.entity.TrainingOperatorEntity;
import jp.or.jaot.model.entity.TrainingOrganizerEntity;
import jp.or.jaot.model.entity.TrainingReportEntity;
import jp.or.jaot.model.entity.TrainingTempApplicationEntity;
import jp.or.jaot.model.entity.composite.TrainingApplicationCompositeEntity;
import jp.or.jaot.model.entity.composite.TrainingAttendanceResultCompositeEntity;
import jp.or.jaot.model.entity.composite.TrainingCompositeEntity;
import jp.or.jaot.model.entity.composite.TrainingMemberCompositeEntity;
import jp.or.jaot.model.entity.composite.TrainingReportCompositeEntity;
import jp.or.jaot.model.entity.composite.TrainingTempApplicationCompositeEntity;
import jp.or.jaot.model.form.TrainingApplicationForm;
import jp.or.jaot.model.form.TrainingApplicationListForm;
import jp.or.jaot.model.form.TrainingApplicationListItemForm;
import jp.or.jaot.model.form.TrainingAttendanceResultForm;
import jp.or.jaot.model.form.TrainingAttendanceResultListForm;
import jp.or.jaot.model.form.TrainingAttendanceResultListItemForm;
import jp.or.jaot.model.form.TrainingForm;
import jp.or.jaot.model.form.TrainingInstructorForm;
import jp.or.jaot.model.form.TrainingListForm;
import jp.or.jaot.model.form.TrainingListItemForm;
import jp.or.jaot.model.form.TrainingMemberListForm;
import jp.or.jaot.model.form.TrainingMemberListItemForm;
import jp.or.jaot.model.form.TrainingOperatorForm;
import jp.or.jaot.model.form.TrainingOrganizerForm;
import jp.or.jaot.model.form.TrainingReportForm;
import jp.or.jaot.model.form.TrainingReportUnitItemForm;
import jp.or.jaot.model.form.TrainingTempApplicationForm;
import jp.or.jaot.model.form.TrainingTempApplicationListForm;
import jp.or.jaot.model.form.TrainingTempApplicationListItemForm;
import jp.or.jaot.model.form.search.TrainingApplicationSearchForm;
import jp.or.jaot.model.form.search.TrainingApplicationSearchListForm;
import jp.or.jaot.model.form.search.TrainingAttendanceResultSearchForm;
import jp.or.jaot.model.form.search.TrainingAttendanceResultSearchListForm;
import jp.or.jaot.model.form.search.TrainingMemberSearchListForm;
import jp.or.jaot.model.form.search.TrainingReportSearchForm;
import jp.or.jaot.model.form.search.TrainingSearchForm;
import jp.or.jaot.model.form.search.TrainingSearchListForm;
import jp.or.jaot.model.form.search.TrainingTempApplicationSearchForm;
import jp.or.jaot.model.form.search.TrainingTempApplicationSearchListForm;
import jp.or.jaot.model.form.update.TrainingUpdateForm;
import jp.or.jaot.service.TrainingApplicationService;
import jp.or.jaot.service.TrainingAttendanceResultService;
import jp.or.jaot.service.TrainingInstructorService;
import jp.or.jaot.service.TrainingMemberService;
import jp.or.jaot.service.TrainingOperatorService;
import jp.or.jaot.service.TrainingOrganizerService;
import jp.or.jaot.service.TrainingReportService;
import jp.or.jaot.service.TrainingService;
import jp.or.jaot.service.TrainingTempApplicationService;
import lombok.AllArgsConstructor;

/**
 * 研修会コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "training", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class AdminTrainingController extends BaseController {

	/** 研修会サービス */
	private final TrainingService trainingSrv;

	/** 研修会申込サービス */
	private final TrainingApplicationService trainingApplicationSrv;

	/** 研修会主催者サービス */
	private final TrainingOrganizerService trainingOrganizerSrv;

	/** 研修会出欠合否サービス */
	private final TrainingAttendanceResultService trainingAttendanceResultSrv;

	/** 研修会仮申込サービス */
	private final TrainingTempApplicationService trainingTempApplicationSrv;

	/** 研修会報告書サービス */
	private final TrainingReportService trainingReportSrv;

	/** 研修会会員サービス */
	private final TrainingMemberService trainingMemberSrv;

	/** 研修会講師実績サービス */
	private final TrainingInstructorService trainingInstructorSrv;

	/** 研修会運営担当者サービス */
	private final TrainingOperatorService trainingOperatorSrv;

	/**
	 * 条件検索(研修会)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training")
	public ResponseEntity<List<TrainingForm>> searchTraining(@RequestBody @Validated TrainingSearchForm trainingForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingSearchDto searchDto = createTrainingSearchDto(trainingForm);
		searchDto.setIds(trainingForm.getIds().stream().toArray(V -> new Long[V])); // ID配列

		// 検索結果フォーム取得
		List<TrainingForm> forms = trainingSrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createTrainingForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(研修会リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_training")
	public ResponseEntity<TrainingListForm> searchListTraining(
			@RequestBody @Validated TrainingSearchListForm trainingForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingSearchDto searchDto = createTrainingSearchDto(trainingForm);

		// 検索結果フォーム取得
		TrainingListForm form = new TrainingListForm();
		form.setTrainings(trainingSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
				.map(E -> createTrainingListItemForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録(研修会)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_training")
	public ResponseEntity<String> enterTraining(@RequestBody @Validated TrainingForm trainingForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		TrainingDto enterDto = TrainingDto.createSingle();
		TrainingCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 研修会
		TrainingEntity enterEntity = compositeEntity.getTrainingEntity();
		BeanUtil.copyProperties(trainingForm, enterEntity, false);

		// 研修会主催者
		compositeEntity.setTrainingOrganizerEntities(
				getEnterEntities(trainingForm.getTrainingOrganizers(), TrainingOrganizerEntity.class));

		// 研修会講師実績
		compositeEntity.setTrainingInstructorEntities(
				getEnterEntities(trainingForm.getTrainingInstructors(), TrainingInstructorEntity.class));

		// 研修会運営担当者
		compositeEntity.setTrainingOperatorEntities(
				getEnterEntities(trainingForm.getTrainingOperators(), TrainingOperatorEntity.class));

		// TODO

		// 実行
		trainingSrv.enter(enterDto);

		return ResponseEntity.ok("enterTraining ok");
	}

	/**
	 * 更新(研修会)
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * NULLを許容する要素を更新対象とする場合は、明示的に初期化対象プロパティを指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_training")
	public ResponseEntity<String> updateTraining(@RequestBody @Validated TrainingUpdateForm trainingForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		TrainingCompositeEntity compositeEntity = trainingSrv.getSearchResult(trainingForm.getId()).getFirstEntity();

		// 更新対象DTO生成
		TrainingDto updateDto = TrainingDto.createSingle();
		TrainingCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 研修会
		TrainingEntity updateTrainingEntity = updateCompositeEntity.getTrainingEntity();
		BeanUtil.copyProperties(compositeEntity.getTrainingEntity(), updateTrainingEntity, false); // 現在値
		BeanUtil.copyProperties(trainingForm, updateTrainingEntity, true,
				trainingForm.getInitProperties().toArray(new String[0])); // 更新値(差分)

		// 研修会講師実績
		updateCompositeEntity.setTrainingInstructorEntities(getUpdateEntities(
				trainingForm.getTrainingInstructors(), compositeEntity.getTrainingInstructorEntities(),
				TrainingInstructorEntity.class));

		// 研修会運営担当者
		updateCompositeEntity.setTrainingOperatorEntities(getUpdateEntities(
				trainingForm.getTrainingOperators(), compositeEntity.getTrainingOperatorEntities(),
				TrainingOperatorEntity.class));

		// TODO

		// 実行
		trainingSrv.update(updateDto);

		return ResponseEntity.ok("updateTraining ok");
	}

	/**
	 * 利用不可(研修会)
	 * @return 実行結果
	 */
	@PostMapping(path = "disable_training")
	public ResponseEntity<String> disableTraining(@RequestBody @Validated TrainingForm trainingForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 研修会
		trainingSrv.setEnable(trainingSrv.getSearchResult(trainingForm.getId()), false); // 使用不可

		return ResponseEntity.ok("disableTraining ok");
	}

	/**
	 * 条件検索(研修会申込)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training_application")
	public ResponseEntity<List<TrainingApplicationForm>> searchTrainingApplication(
			@RequestBody @Validated TrainingApplicationSearchForm trainingApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingApplicationSearchDto searchDto = new TrainingApplicationSearchDto();
		BeanUtil.copyProperties(trainingApplicationForm, searchDto, false); // 属性値

		// 複数要素
		searchDto.setTrainingNos(createArrayString(trainingApplicationForm.getTrainingNos())); // 研修会番号
		searchDto.setWorkPlacePrefectureCds(createArrayString(trainingApplicationForm.getWorkPlacePrefectureCds())); // 勤務先都道府県

		// 検索結果フォーム取得
		List<TrainingApplicationForm> forms = trainingApplicationSrv.getSearchResultByCondition(searchDto)
				.getEntities().stream().map(E -> createTrainingApplicationForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(研修会申込リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_training_application")
	public ResponseEntity<TrainingApplicationListForm> searchListTrainingApplication(
			@RequestBody @Validated TrainingApplicationSearchListForm trainingApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingApplicationSearchDto searchDto = new TrainingApplicationSearchDto();
		BeanUtil.copyProperties(trainingApplicationForm, searchDto, false); // 属性値

		// 複数要素
		searchDto.setTrainingNos(createArrayString(trainingApplicationForm.getTrainingNos())); // 研修会番号
		searchDto.setWorkPlacePrefectureCds(createArrayString(trainingApplicationForm.getWorkPlacePrefectureCds())); // 勤務先都道府県

		// 検索結果フォーム取得
		TrainingApplicationListForm form = new TrainingApplicationListForm();
		form.setTrainingApplications(
				trainingApplicationSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
						.map(E -> createTrainingApplicationListItemForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録(研修会申込)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_training_application")
	public ResponseEntity<String> enterTrainingApplication(
			@RequestBody @Validated TrainingApplicationForm trainingApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		TrainingApplicationDto enterDto = TrainingApplicationDto.createSingle();
		TrainingApplicationCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 研修会
		TrainingApplicationEntity enterEntity = compositeEntity.getTrainingApplicationEntity();
		BeanUtil.copyProperties(trainingApplicationForm, enterEntity, false);

		// 実行
		trainingApplicationSrv.enter(enterDto);

		return ResponseEntity.ok("enterTrainingApplication ok");
	}

	/**
	 * 条件検索(研修会主催者)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training_organizer")
	public ResponseEntity<List<TrainingOrganizerForm>> searchTrainingOrganizer(
			@RequestBody @Validated TrainingOrganizerForm trainingOrganizerForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingOrganizerSearchDto searchDto = new TrainingOrganizerSearchDto();
		BeanUtil.copyProperties(trainingOrganizerForm, searchDto, false); // 属性値

		// 検索結果フォーム取得
		List<TrainingOrganizerForm> forms = trainingOrganizerSrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, TrainingOrganizerForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(研修会主催者)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_training_organizer")
	public ResponseEntity<String> enterTrainingOrganizer(
			@RequestBody @Validated TrainingOrganizerForm trainingOrganizerForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		trainingOrganizerSrv.enter(copyDto(trainingOrganizerForm, TrainingOrganizerDto.createSingle()));

		return ResponseEntity.ok("enterTrainingOrganizer ok");
	}

	/**
	 * 条件検索(研修会出欠合否)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training_attendance_result")
	public ResponseEntity<List<TrainingAttendanceResultForm>> searchTrainingAttendanceResult(
			@RequestBody @Validated TrainingAttendanceResultSearchForm trainingAttendanceResultForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingAttendanceResultSearchDto searchDto = new TrainingAttendanceResultSearchDto();
		BeanUtil.copyProperties(trainingAttendanceResultForm, searchDto, false); // 属性値

		// 複数要素
		searchDto.setTrainingNos(createArrayString(trainingAttendanceResultForm.getTrainingNos())); // 研修会番号

		// 検索結果フォーム取得
		List<TrainingAttendanceResultForm> forms = trainingAttendanceResultSrv.getSearchResultByCondition(searchDto)
				.getEntities().stream().map(E -> createTrainingAttendanceResultForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(研修会出欠合否リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_training_attendance_result")
	public ResponseEntity<TrainingAttendanceResultListForm> searchListTrainingAttendanceResult(
			@RequestBody @Validated TrainingAttendanceResultSearchListForm trainingAttendanceResultForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingAttendanceResultSearchDto searchDto = new TrainingAttendanceResultSearchDto();
		BeanUtil.copyProperties(trainingAttendanceResultForm, searchDto, false); // 属性値

		// 複数要素
		searchDto.setTrainingNos(createArrayString(trainingAttendanceResultForm.getTrainingNos())); // 研修会番号

		// 検索結果フォーム取得
		TrainingAttendanceResultListForm form = new TrainingAttendanceResultListForm();
		form.setTrainingAttendanceResults(
				trainingAttendanceResultSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
						.map(E -> createTrainingAttendanceResultListItemForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録(研修会出欠合否)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_training_attendance_result")
	public ResponseEntity<String> enterTrainingAttendanceResult(
			@RequestBody @Validated TrainingAttendanceResultForm trainingAttendanceResultForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		TrainingAttendanceResultDto enterDto = TrainingAttendanceResultDto.createSingle();
		TrainingAttendanceResultCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 研修会
		TrainingAttendanceResultEntity enterEntity = compositeEntity.getTrainingAttendanceResultEntity();
		BeanUtil.copyProperties(trainingAttendanceResultForm, enterEntity, false);

		// 実行
		trainingAttendanceResultSrv.enter(enterDto);

		return ResponseEntity.ok("enterTrainingAttendanceResult ok");
	}

	/**
	 * 更新(研修会出欠合否)
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * 新規要素を追加する場合は、明示的にidをNULLで指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_training_attendance_result")
	public ResponseEntity<TrainingAttendanceResultForm> updateTrainingAttendanceResult(
			@RequestBody @Validated TrainingAttendanceResultForm trainingAttendanceResultForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 更新対象DTO生成
		TrainingAttendanceResultDto updateDto = TrainingAttendanceResultDto.createSingle();
		TrainingAttendanceResultCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();
		TrainingAttendanceResultEntity updateEntity = updateCompositeEntity.getTrainingAttendanceResultEntity();

		// 研修会出欠合否
		Long id = null;
		if ((id = trainingAttendanceResultForm.getId()) == null) {
			// 追加
			BeanUtil.copyProperties(trainingAttendanceResultForm, updateEntity, false);
		} else {
			// 更新
			TrainingAttendanceResultCompositeEntity compositeEntity = trainingAttendanceResultSrv.getSearchResult(id)
					.getFirstEntity(); // 検索結果取得(キー)
			BeanUtil.copyProperties(compositeEntity.getTrainingAttendanceResultEntity(), updateEntity, false); // 現在値
			BeanUtil.copyProperties(trainingAttendanceResultForm, updateEntity, true); // 更新値(差分)
		}

		// 実行
		trainingAttendanceResultSrv.update(updateDto);

		return ResponseEntity.ok(createTrainingAttendanceResultForm(updateCompositeEntity));
	}

	/**
	 * 条件検索(研修会仮申込)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training_temp_application")
	public ResponseEntity<List<TrainingTempApplicationForm>> searchTrainingTempApplication(
			@RequestBody @Validated TrainingTempApplicationSearchForm trainingTempApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingTempApplicationSearchDto searchDto = new TrainingTempApplicationSearchDto();
		BeanUtil.copyProperties(trainingTempApplicationForm, searchDto, false); // 属性値

		// 複数要素
		searchDto.setTrainingNos(createArrayString(trainingTempApplicationForm.getTrainingNos())); // 研修会番号

		// 検索結果フォーム取得
		List<TrainingTempApplicationForm> forms = trainingTempApplicationSrv.getSearchResultByCondition(searchDto)
				.getEntities().stream().map(E -> createTrainingTempApplicationForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(研修会仮申込リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_training_temp_application")
	public ResponseEntity<TrainingTempApplicationListForm> searchListTrainingTempApplication(
			@RequestBody @Validated TrainingTempApplicationSearchListForm trainingTempApplicationForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingTempApplicationSearchDto searchDto = new TrainingTempApplicationSearchDto();
		BeanUtil.copyProperties(trainingTempApplicationForm, searchDto, false); // 属性値

		// 複数要素
		searchDto.setTrainingNos(createArrayString(trainingTempApplicationForm.getTrainingNos())); // 研修会番号

		// 検索結果フォーム取得
		TrainingTempApplicationListForm form = new TrainingTempApplicationListForm();
		form.setTrainingTempApplications(
				trainingTempApplicationSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
						.map(E -> createTrainingTempApplicationListItemForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録(研修会仮申込)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_training_temp_application")
	public ResponseEntity<String> enterTrainingTempApplication(
			@RequestBody @Validated TrainingTempApplicationForm trainingTempApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		TrainingTempApplicationDto enterDto = TrainingTempApplicationDto.createSingle();
		TrainingTempApplicationCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 研修会
		TrainingTempApplicationEntity enterEntity = compositeEntity.getTrainingTempApplicationEntity();
		BeanUtil.copyProperties(trainingTempApplicationForm, enterEntity, false);

		// 実行
		trainingTempApplicationSrv.enter(enterDto);

		return ResponseEntity.ok("enterTrainingTempApplication ok");
	}

	/**
	 * 削除(研修会仮申込)
	 * @return 実行結果
	 */
	@PostMapping(path = "delete_training_temp_application")
	public ResponseEntity<String> deleteTrainingTempApplication(
			@RequestBody @Validated TrainingTempApplicationSearchForm trainingTempApplicationForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingTempApplicationSearchDto searchDto = new TrainingTempApplicationSearchDto();
		searchDto.setIds(createArrayLong(trainingTempApplicationForm.getIds()));

		// 実行
		trainingTempApplicationSrv.delete(searchDto);

		return ResponseEntity.ok("deleteTrainingTempApplication ok");
	}

	/**
	 * 条件検索(研修会報告書)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training_report")
	public ResponseEntity<List<TrainingReportForm>> searchTrainingReport(
			@RequestBody @Validated TrainingReportSearchForm trainingForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingReportSearchDto searchDto = new TrainingReportSearchDto();
		searchDto.setTrainingNos(trainingForm.getTrainingNos().stream().toArray(V -> new String[V])); // 研修会番号配列

		// 検索結果フォーム取得
		List<TrainingReportForm> forms = trainingReportSrv.getSearchResultByCondition(searchDto)
				.getEntities().stream().map(E -> createTrainingReportForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(研修会報告書(単一項目))
	 * @return 実行結果
	 */
	@PostMapping(path = "search_unit_item_training_report")
	public ResponseEntity<List<TrainingReportUnitItemForm>> searchUnitItemTrainingReport(
			@RequestBody @Validated TrainingReportSearchForm trainingForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingReportSearchDto searchDto = new TrainingReportSearchDto();
		searchDto.setTrainingNos(trainingForm.getTrainingNos().stream().toArray(V -> new String[V])); // 研修会番号配列

		// 検索結果フォーム取得
		List<TrainingReportUnitItemForm> forms = trainingReportSrv.getSearchResultUnitItemByCondition(searchDto)
				.getEntities().stream().map(E -> createTrainingReportUnitItemForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(研修会報告書)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_training_report")
	public ResponseEntity<String> enterTrainingReport(
			@RequestBody @Validated TrainingReportForm trainingForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		TrainingReportDto enterDto = TrainingReportDto.createSingle();
		TrainingReportCompositeEntity compositeEntity = enterDto.getFirstEntity();

		// 研修会報告書
		TrainingReportEntity enterEntity = compositeEntity.getTrainingReportEntity();
		BeanUtil.copyProperties(trainingForm, enterEntity, false);

		// 実行
		trainingReportSrv.enter(enterDto);

		return ResponseEntity.ok("enterTrainingReport ok");
	}

	/**
	 * 更新(研修会報告書)
	 * [補足事項]<br>
	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	 * 新規要素を追加する場合は、明示的にidをNULLで指定する。
	 * @return 実行結果
	 */
	@PostMapping(path = "update_training_report")
	public ResponseEntity<TrainingReportForm> updateTrainingReport(
			@RequestBody @Validated TrainingReportForm trainingReportForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 更新対象DTO生成
		TrainingReportDto updateDto = TrainingReportDto.createSingle();
		TrainingReportCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();
		TrainingReportEntity updateEntity = updateCompositeEntity.getTrainingReportEntity();

		// 研修会報告書
		Long id = null;
		if ((id = trainingReportForm.getId()) == null) {
			// 追加
			BeanUtil.copyProperties(trainingReportForm, updateEntity, false);
		} else {
			// 更新
			TrainingReportCompositeEntity compositeEntity = trainingReportSrv.getSearchResult(id).getFirstEntity(); // 検索結果取得(キー)
			BeanUtil.copyProperties(compositeEntity.getTrainingReportEntity(), updateEntity, false); // 現在値
			BeanUtil.copyProperties(trainingReportForm, updateEntity, true); // 更新値(差分)
		}

		// 実行
		trainingReportSrv.update(updateDto);

		return ResponseEntity.ok(createTrainingReportForm(updateCompositeEntity));
	}

	/**
	 * 条件検索(研修会会員リスト)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_training_member")
	public ResponseEntity<TrainingMemberListForm> searchListTrainingMember(
			@RequestBody @Validated TrainingMemberSearchListForm trainingMemberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingMemberSearchDto searchDto = new TrainingMemberSearchDto();
		BeanUtil.copyProperties(trainingMemberForm, searchDto, false); // 属性値

		// 検索結果フォーム取得
		TrainingMemberListForm form = new TrainingMemberListForm();
		form.setTrainingMembers(
				trainingMemberSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
						.map(E -> createTrainingMemberListItemForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 条件検索(研修会講師実績)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training_instructor")
	public ResponseEntity<List<TrainingInstructorForm>> searchTrainingInstructor(
			@RequestBody @Validated TrainingInstructorForm trainingInstructorForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingInstructorSearchDto searchDto = new TrainingInstructorSearchDto();
		BeanUtil.copyProperties(trainingInstructorForm, searchDto, false); // 属性値

		// 検索結果フォーム取得
		List<TrainingInstructorForm> forms = trainingInstructorSrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, TrainingInstructorForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(研修会講師実績)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_training_instructor")
	public ResponseEntity<String> enterTrainingInstructor(
			@RequestBody @Validated TrainingInstructorForm trainingInstructorForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		trainingInstructorSrv.enter(copyDto(trainingInstructorForm, TrainingInstructorDto.createSingle()));

		return ResponseEntity.ok("enterTrainingInstructor ok");
	}

	/**
	 * 条件検索(研修会運営担当者)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training_operator")
	public ResponseEntity<List<TrainingOperatorForm>> searchTrainingOperator(
			@RequestBody @Validated TrainingOperatorForm trainingOperatorForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingOperatorSearchDto searchDto = new TrainingOperatorSearchDto();
		BeanUtil.copyProperties(trainingOperatorForm, searchDto, false); // 属性値

		// 検索結果フォーム取得
		List<TrainingOperatorForm> forms = trainingOperatorSrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, TrainingOperatorForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(研修会運営担当者)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_training_operator")
	public ResponseEntity<String> enterTrainingOperator(
			@RequestBody @Validated TrainingOperatorForm trainingOperatorForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		trainingOperatorSrv.enter(copyDto(trainingOperatorForm, TrainingOperatorDto.createSingle()));

		return ResponseEntity.ok("enterTrainingOperator ok");
	}

	/**
	 * 研修会検索DTO生成
	 * @param trainingForm  研修会検索フォーム
	 * @return 研修会検索DTO
	 */
	private TrainingSearchDto createTrainingSearchDto(TrainingSearchForm trainingForm) {

		TrainingSearchDto searchDto = new TrainingSearchDto();
		BeanUtil.copyProperties(trainingForm, searchDto, false);

		// 複数要素
		searchDto.setOrganizerCds(createArrayString(trainingForm.getOrganizerCds())); // 主催者
		searchDto.setTrainingTypeCds(createArrayString(trainingForm.getTrainingTypeCds())); // 研修会種別
		searchDto.setCourseCategoryCds(createArrayString(trainingForm.getCourseCategoryCds())); // 講座分類
		searchDto.setReceptionStatusCds(createArrayString(trainingForm.getReceptionStatusCds())); // 受付状況
		searchDto.setUpdateStatusCds(createArrayString(trainingForm.getUpdateStatusCds())); // 更新状況
		searchDto.setVenuePrefCds(createArrayString(trainingForm.getVenuePrefCds())); // 開催地

		// 開催日
		searchDto.setEventDateFrom(DateUtil.getFromDate(trainingForm.getEventDateFrom()));
		searchDto.setEventDateTo(DateUtil.getToDate(trainingForm.getEventDateTo()));

		return searchDto;
	}

	/**
	 * 研修会フォーム生成
	 * @param compositeEntity 研修会エンティティ(複合)
	 * @return 研修会フォーム
	 */
	private TrainingForm createTrainingForm(TrainingCompositeEntity compositeEntity) {

		// 研修会
		TrainingForm form = BeanUtil.createCopyProperties(compositeEntity.getTrainingEntity(),
				TrainingForm.class, false);

		// 研修会主催者
		List<TrainingOrganizerEntity> organizerEntities = compositeEntity.getTrainingOrganizerEntities();
		if (!CollectionUtils.isEmpty(organizerEntities)) {
			form.setTrainingOrganizers(organizerEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, TrainingOrganizerForm.class, false))
					.collect(Collectors.toList()));
		}

		// 研修会講師実績
		List<TrainingInstructorEntity> instructorEntities = compositeEntity.getTrainingInstructorEntities();
		if (!CollectionUtils.isEmpty(instructorEntities)) {
			form.setTrainingInstructors(instructorEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, TrainingInstructorForm.class, false))
					.collect(Collectors.toList()));
		}

		// 研修会運営担当者
		List<TrainingOperatorEntity> operatorEntities = compositeEntity.getTrainingOperatorEntities();
		if (!CollectionUtils.isEmpty(operatorEntities)) {
			form.setTrainingOperators(operatorEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, TrainingOperatorForm.class, false))
					.collect(Collectors.toList()));
		}

		return form;
	}

	/**
	 * 研修会リスト項目フォーム生成
	 * @param compositeEntity 研修会エンティティ(複合)
	 * @return 研修会リスト項目フォーム
	 */
	private TrainingListItemForm createTrainingListItemForm(TrainingCompositeEntity compositeEntity) {

		// 研修会リスト項目
		TrainingListItemForm form = BeanUtil.createCopyProperties(compositeEntity.getTrainingEntity(),
				TrainingListItemForm.class, false);

		// 個別
		form.setNotification(compositeEntity.getNotification()); // 通知
		form.setDecisionCount(compositeEntity.getDecisionCount()); // 確定数
		form.setSubscriptionCount(compositeEntity.getSubscriptionCount()); // 申込数
		form.setEventDateText(compositeEntity.getEventDateText()); // 開催日(文字列)

		return form;
	}

	/**
	 * 研修会申込フォーム生成
	 * @param compositeEntity 研修会申込エンティティ(複合)
	 * @return 研修会申込フォーム
	 */
	private TrainingApplicationForm createTrainingApplicationForm(TrainingApplicationCompositeEntity compositeEntity) {

		// 研修会申込
		TrainingApplicationForm form = BeanUtil.createCopyProperties(
				compositeEntity.getTrainingApplicationEntity(), TrainingApplicationForm.class, false);

		return form;
	}

	/**
	 * 研修会申込リスト項目フォーム生成
	 * @param compositeEntity 研修会申込エンティティ(複合)
	 * @return 研修会申込リスト項目フォーム
	 */
	private TrainingApplicationListItemForm createTrainingApplicationListItemForm(
			TrainingApplicationCompositeEntity compositeEntity) {

		// 研修会申込リスト項目
		TrainingApplicationListItemForm form = new TrainingApplicationListItemForm();
		BeanUtil.copyProperties(compositeEntity.getTrainingEntity(), form, true); // 研修会
		BeanUtil.copyProperties(compositeEntity.getTrainingApplicationEntity(), form, true); // 研修会申込

		// 個別
		form.setEventDateText(compositeEntity.getEventDateText()); // 開催日(文字列)

		return form;
	}

	/**
	 * 研修会出欠合否フォーム生成
	 * @param compositeEntity 研修会出欠合否エンティティ(複合)
	 * @return 研修会出欠合否フォーム
	 */
	private TrainingAttendanceResultForm createTrainingAttendanceResultForm(
			TrainingAttendanceResultCompositeEntity compositeEntity) {

		// 研修会出欠合否
		TrainingAttendanceResultForm form = BeanUtil.createCopyProperties(
				compositeEntity.getTrainingAttendanceResultEntity(), TrainingAttendanceResultForm.class, false);

		return form;
	}

	/**
	 * 研修会出欠合否リスト項目フォーム生成
	 * @param compositeEntity 研修会出欠合否エンティティ(複合)
	 * @return 研修会出欠合否リスト項目フォーム
	 */
	private TrainingAttendanceResultListItemForm createTrainingAttendanceResultListItemForm(
			TrainingAttendanceResultCompositeEntity compositeEntity) {

		// 研修会出欠合否リスト項目
		TrainingAttendanceResultListItemForm form = new TrainingAttendanceResultListItemForm();
		BeanUtil.copyProperties(compositeEntity.getTrainingEntity(), form, true); // 研修会
		BeanUtil.copyProperties(compositeEntity.getTrainingAttendanceResultEntity(), form, false); // 研修会出欠合否

		return form;
	}

	/**
	 * 研修会仮申込フォーム生成
	 * @param compositeEntity 研修会仮申込エンティティ(複合)
	 * @return 研修会仮申込フォーム
	 */
	private TrainingTempApplicationForm createTrainingTempApplicationForm(
			TrainingTempApplicationCompositeEntity compositeEntity) {

		// 研修会仮申込
		TrainingTempApplicationForm form = BeanUtil.createCopyProperties(
				compositeEntity.getTrainingTempApplicationEntity(), TrainingTempApplicationForm.class, false);

		return form;
	}

	/**
	 * 研修会仮申込リスト項目フォーム生成
	 * @param compositeEntity 研修会仮申込エンティティ(複合)
	 * @return 研修会仮申込リスト項目フォーム
	 */
	private TrainingTempApplicationListItemForm createTrainingTempApplicationListItemForm(
			TrainingTempApplicationCompositeEntity compositeEntity) {

		// 研修会仮申込リスト項目
		TrainingTempApplicationListItemForm form = new TrainingTempApplicationListItemForm();
		BeanUtil.copyProperties(compositeEntity.getTrainingTempApplicationEntity(), form, false); // 研修会仮申込

		// 個別
		form.setFullname(compositeEntity.getFullname()); // 氏名
		form.setCurrentDatetime(new Timestamp(System.currentTimeMillis())); // 現在日時(システム現在日時)
		form.setRegisteredDatetime(compositeEntity.getRegisteredDatetime());// 登録日時
		form.setWorkingPrefectureCd(compositeEntity.getWorkingPrefectureCd()); // 勤務先都道府県コード
		form.setWorkingConditionFacilityName(compositeEntity.getWorkingConditionFacilityName()); // 勤務条件（施設名）
		form.setQualificationPeriodDate(compositeEntity.getQualificationPeriodDate()); // 認定期間満了日

		return form;
	}

	/**
	 * 研修会報告書フォーム生成
	 * @param compositeEntity 研修会報告書エンティティ(複合)
	 * @return 研修会報告書フォーム
	 */
	private TrainingReportForm createTrainingReportForm(TrainingReportCompositeEntity compositeEntity) {

		// 研修会報告書
		TrainingReportForm form = BeanUtil.createCopyProperties(
				compositeEntity.getTrainingReportEntity(), TrainingReportForm.class, false);

		return form;
	}

	/**
	 * 研修会報告書単一項目フォーム生成
	 * @param compositeEntity 研修会報告書エンティティ(複合)
	 * @return 研修会報告書単一項目フォーム
	 */
	private TrainingReportUnitItemForm createTrainingReportUnitItemForm(TrainingReportCompositeEntity compositeEntity) {

		// 研修会報告書
		TrainingReportUnitItemForm form = BeanUtil.createCopyProperties(
				compositeEntity.getTrainingReportEntity(), TrainingReportUnitItemForm.class, false);

		// 単一項目属性
		form.setTargetCd(compositeEntity.getTargetCd()); // 対象
		form.setCapacityCount(compositeEntity.getCapacityCount()); // 定員数
		form.setDecisionCount(compositeEntity.getDecisionCount()); // 確定数
		form.setAttendanceStatus1Count(compositeEntity.getAttendanceStatus1Count()); // 参加人数(開催日1)
		form.setAttendanceStatus2Count(compositeEntity.getAttendanceStatus2Count()); // 参加人数(開催日2)
		form.setAttendanceStatus3Count(compositeEntity.getAttendanceStatus3Count()); // 参加人数(開催日3)
		form.setEntryFee(compositeEntity.getEntryFee()); // 参加費

		return form;
	}

	/**
	 * 研修会会員リスト項目フォーム生成
	 * @param compositeEntity 研修会会員エンティティ(複合)
	 * @return 研修会会員リスト項目フォーム
	 */
	private TrainingMemberListItemForm createTrainingMemberListItemForm(TrainingMemberCompositeEntity compositeEntity) {

		// 研修会会員リスト項目
		TrainingMemberListItemForm form = new TrainingMemberListItemForm();
		BeanUtil.copyProperties(compositeEntity.getMemberEntity(), form, false); // 会員

		// TODO

		return form;
	}

	// TODO
}
