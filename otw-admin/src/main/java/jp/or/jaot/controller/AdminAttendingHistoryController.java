package jp.or.jaot.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.ListUtils;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.model.BaseEntity.ORDER_ASC;
import jp.or.jaot.core.model.BaseSearchDto.SearchOrder;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.BasicOtAttendingSummaryDto;
import jp.or.jaot.model.dto.BasicPointTrainingHistoryDto;
import jp.or.jaot.model.dto.ClinicalTrainingLeaderSummaryDto;
import jp.or.jaot.model.dto.MtdlpSummaryDto;
import jp.or.jaot.model.dto.ProfessionalOtAttendingSummaryDto;
import jp.or.jaot.model.dto.QualificationOtAttendingHistoryDto;
import jp.or.jaot.model.dto.QualifiedOtAttendingSummaryDto;
import jp.or.jaot.model.dto.search.BasicOtAttendingSummarySearchDto;
import jp.or.jaot.model.dto.search.BasicPointTrainingHistorySearchDto;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderSummarySearchDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.dto.search.MtdlpSummarySearchDto;
import jp.or.jaot.model.dto.search.ProfessionalOtAttendingSummarySearchDto;
import jp.or.jaot.model.dto.search.QualificationOtAttendingHistorySearchDto;
import jp.or.jaot.model.dto.search.QualifiedOtAttendingSummarySearchDto;
import jp.or.jaot.model.dto.search.TrainingNumberSearchDto;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.BasicPointTrainingHistoryEntity;
import jp.or.jaot.model.entity.CaseReportEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderQualifyHistoryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;
import jp.or.jaot.model.entity.LifelongEducationStatusEntity;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;
import jp.or.jaot.model.entity.MemberQualificationEntity;
import jp.or.jaot.model.entity.MemberWithdrawalHistoryEntity;
import jp.or.jaot.model.entity.MtdlpCaseReportEntity;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;
import jp.or.jaot.model.entity.MtdlpSummaryEntity;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;
import jp.or.jaot.model.entity.PeriodExtensionApplicationEntity;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.RecessHistoryEntity;
import jp.or.jaot.model.entity.composite.BasicOtAttendingSummaryCompositeEntity;
import jp.or.jaot.model.entity.composite.BasicPointTrainingHistoryCompositeEntity;
import jp.or.jaot.model.entity.composite.ClinicalTrainingLeaderSummaryCompositeEntity;
import jp.or.jaot.model.entity.composite.MemberCompositeEntity;
import jp.or.jaot.model.entity.composite.MtdlpCompositeEntity;
import jp.or.jaot.model.entity.composite.ProfessionalOtAttendingSummaryCompositeEntity;
import jp.or.jaot.model.entity.composite.QualifiedOtAttendingSummaryCompositeEntity;
import jp.or.jaot.model.form.BasicOtAttendingSummaryForm;
import jp.or.jaot.model.form.BasicOtAttendingSummaryUpdateForm;
import jp.or.jaot.model.form.BasicOtQualifyHistoryForm;
import jp.or.jaot.model.form.BasicOtTrainingHistoryForm;
import jp.or.jaot.model.form.BasicPointTrainingHistoryForm;
import jp.or.jaot.model.form.BasicPointTrainingHistoryUpdateForm;
import jp.or.jaot.model.form.CaseReportForm;
import jp.or.jaot.model.form.ClinicalTrainingLeaderQualifyHistoryForm;
import jp.or.jaot.model.form.ClinicalTrainingLeaderSummaryForm;
import jp.or.jaot.model.form.ClinicalTrainingLeaderSummaryUpdateForm;
import jp.or.jaot.model.form.ClinicalTrainingLeaderTrainingHistoryForm;
import jp.or.jaot.model.form.LifelongEducationStatusForm;
import jp.or.jaot.model.form.MemberForm;
import jp.or.jaot.model.form.MemberListForm;
import jp.or.jaot.model.form.MemberLocalActivityForm;
import jp.or.jaot.model.form.MemberQualificationForm;
import jp.or.jaot.model.form.MemberWithdrawalHistoryForm;
import jp.or.jaot.model.form.MtdlpCaseReportForm;
import jp.or.jaot.model.form.MtdlpQualifyHistoryForm;
import jp.or.jaot.model.form.MtdlpSummaryForm;
import jp.or.jaot.model.form.MtdlpSummaryUpdateForm;
import jp.or.jaot.model.form.MtdlpTrainingHistoryForm;
import jp.or.jaot.model.form.PeriodExtensionApplicationForm;
import jp.or.jaot.model.form.ProfessionalOtAttendingSummaryForm;
import jp.or.jaot.model.form.ProfessionalOtAttendingSummaryUpdateForm;
import jp.or.jaot.model.form.ProfessionalOtQualifyHistoryForm;
import jp.or.jaot.model.form.ProfessionalOtTestHistoryForm;
import jp.or.jaot.model.form.ProfessionalOtTrainingHistoryForm;
import jp.or.jaot.model.form.QualificationOtAttendingHistoryForm;
import jp.or.jaot.model.form.QualifiedOtAttendingSummaryForm;
import jp.or.jaot.model.form.QualifiedOtAttendingSummaryUpdateForm;
import jp.or.jaot.model.form.QualifiedOtCompletionHistoryForm;
import jp.or.jaot.model.form.QualifiedOtTrainingHistoryForm;
import jp.or.jaot.model.form.RecessHistoryForm;
import jp.or.jaot.model.form.TrainingNumberForm;
import jp.or.jaot.model.form.search.MemberSearchListForm;
import jp.or.jaot.service.BasicOtAttendingSummaryService;
import jp.or.jaot.service.BasicPointTrainingHistoryService;
import jp.or.jaot.service.ClinicalTrainingLeaderSummaryService;
import jp.or.jaot.service.MemberService;
import jp.or.jaot.service.MtdlpSummaryService;
import jp.or.jaot.service.ProfessionalOtAttendingSummaryService;
import jp.or.jaot.service.QualificationOtAttendingHistoryService;
import jp.or.jaot.service.QualifiedOtAttendingSummaryService;
import jp.or.jaot.service.TrainingNumberService;
import lombok.AllArgsConstructor;

/**
* 事務局受講履歴コントローラ(WebAPI)
*/
@RestController
@RequestMapping(path = "attending_history", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class AdminAttendingHistoryController extends BaseController {

	/** 会員サービス */
	private final MemberService memberSrv;

	/** 受講履歴サービス */
	//	private final AttendingHistoryService attendingHistorySrv;

	/** 基礎研修履歴サービス */
	private final BasicOtAttendingSummaryService basicOtAttendingSummarySrv;

	/** 基礎ポイント研修履歴サービス */
	private final BasicPointTrainingHistoryService basicPointTrainingHistorySrv;

	/** 認定作業療法士研修受講履歴サービス */
	private final QualificationOtAttendingHistoryService qualificationOtAttendingHistorySrv;

	/** 専門作業療法士受講履歴サービス */
	private final ProfessionalOtAttendingSummaryService professionalOtAttendingSummarySrv;

	/** 認定作業療法士研修履歴サービス */
	private final QualifiedOtAttendingSummaryService qualifiedOtAttendingSummarySrv;

	/** 臨床実習指導者履歴サービス */
	private final ClinicalTrainingLeaderSummaryService clinicalTrainingLeaderSummarySrv;

	/** MTDLP履歴サービス */
	private final MtdlpSummaryService mtdlpSummarySrv;

	/** 研修番号マスタサービス */
	private final TrainingNumberService trainingNumberSrv;

	//	/**
	//	 * 条件検索（受講履歴検索画面）
	//	 * @return 実行結果
	//	 */
	//	@PostMapping(path = "search_attending_history_member")
	//	public ResponseEntity<List<MemberForm>> searchMember(@RequestBody @Validated MemberForm memberForm, Errors errors) {
	//
	//		// バリデーション実行
	//		doValidation(errors);
	//
	//		// 検索条件DTO生成
	//		MemberSearchDto searchDto = new MemberSearchDto();
	//		BeanUtil.copyProperties(memberForm, searchDto, false); // 属性値
	//		searchDto.setStartPosition(0); // 開始位置
	//		searchDto.setMaxResult(0); // 取得件数(全件)
	//
	//		// 検索結果フォーム取得
	//		List<MemberForm> forms = memberSrv.getSearchResultByCondition(searchDto).getEntities().stream()
	//				.map(E -> createMemberForm(E)).collect(Collectors.toList());
	//
	//		return ResponseEntity.ok(forms);
	//	}

	/**
	 * 条件検索(会員リスト：受講履歴検索画面）
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_member")
	public ResponseEntity<MemberListForm> searchListMember(@RequestBody @Validated MemberSearchListForm memberForm,
			Errors errors) {
		// バリデーション実行
		doValidation(errors);
		// 検索条件DTO生成
		MemberSearchDto searchDto = new MemberSearchDto();
		BeanUtil.copyProperties(memberForm, searchDto, false);
		SearchOrder order = new SearchOrder(ORDER_ASC.ASC, "memberNo");
		List<SearchOrder> orders = new ArrayList<SearchOrder>();
		orders.add(order);
		searchDto.setOrders(orders);

		// 検索結果フォーム取得
		MemberListForm form = new MemberListForm();
		form.setMembers(memberSrv.getSearchResultListByCondition(searchDto).getEntities().stream()
				.map(E -> createMemberForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 会員番号検索(受講履歴詳細画面)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_attending_history")
	public ResponseEntity<List<MemberForm>> Attendinghistory(
			@RequestBody @Validated MemberForm memberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberSearchDto searchDto = new MemberSearchDto();
		BeanUtil.copyProperties(memberForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberForm> memberForms = memberSrv.getSearchResultByMemberNo(searchDto.getMemberNo()).getEntities()
				.stream()
				.map(E -> createMemberForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(memberForms);
	}

	//	/**
	//	 * 会員番号検索(受講履歴詳細画面)
	//	 * @return 実行結果
	//	 */
	//	@PostMapping(path = "search_attending_history")
	//	public ResponseEntity<List<MemberForm>> searchMemberInfo(
	//			@RequestBody @Validated MemberForm memberForm, Errors errors) {
	//
	//		// バリデーション実行
	//		doValidation(errors);
	//
	//		// 検索結果フォーム取得（会員情報）
	//		List<MemberForm> memberforms = memberSrv.getSearchResultByMemberNo(memberForm.getMemberNo()).getEntities().stream()
	//				.map(E -> createMemberForm(E)).collect(Collectors.toList());
	//
	//		return ResponseEntity.ok(memberforms);
	//	}

	/**
	 * 会員フォーム生成
	 * @param compositeEntity 会員エンティティ(複合)
	 * @return 会員フォーム
	 */
	private MemberForm createMemberForm(MemberCompositeEntity compositeEntity) {

		// 会員フォーム
		MemberForm memberForm = BeanUtil.createCopyProperties(compositeEntity.getMemberEntity(), MemberForm.class,
				false);

		// 会員関連資格情報
		List<MemberQualificationEntity> qualificationEntities = compositeEntity.getMemberQualificationEntities();
		if (!CollectionUtils.isEmpty(qualificationEntities)) {
			memberForm.setMemberQualifications(qualificationEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MemberQualificationForm.class, false))
					.collect(Collectors.toList()));
		}

		// 会員自治体参画情報
		List<MemberLocalActivityEntity> localActivityEntities = compositeEntity.getMemberLocalActivityEntities();
		if (!CollectionUtils.isEmpty(localActivityEntities)) {
			memberForm.setMemberLocalActivities(localActivityEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MemberLocalActivityForm.class, false))
					.collect(Collectors.toList()));
		}

		// 休会履歴
		RecessHistoryEntity recessHistoryEntity = compositeEntity.getRecessHistoryEntity();
		if (recessHistoryEntity != null) {

			// 休会履歴フォーム生成
			RecessHistoryForm recessHistoryForm = BeanUtil.createCopyProperties(
					recessHistoryEntity, RecessHistoryForm.class, false);

			// 設定
			memberForm.setRecessHistory(recessHistoryForm);
		}

		// 休会履歴
		MemberWithdrawalHistoryEntity memberWithdrawalHistoryEntity = compositeEntity
				.getMemberWithdrawalHistoryEntity();
		if (memberWithdrawalHistoryEntity != null) {

			// 休会履歴フォーム生成
			MemberWithdrawalHistoryForm memberWithdrawalHistoryForm = BeanUtil.createCopyProperties(
					memberWithdrawalHistoryEntity, MemberWithdrawalHistoryForm.class, false);

			// 設定
			memberForm.setMemberWithdrawalHistory(memberWithdrawalHistoryForm);
		}

		//生涯教育ステータス
		LifelongEducationStatusEntity lifelongEducationStatusEntity = compositeEntity
				.getLifelongEducationStatusEntity();
		if (lifelongEducationStatusEntity != null) {

			// 生涯教育フォーム生成
			LifelongEducationStatusForm lifelongEducationStatusForm = BeanUtil.createCopyProperties(
					lifelongEducationStatusEntity, LifelongEducationStatusForm.class, false);

			// 設定
			memberForm.setLifelongEducationStatuses(lifelongEducationStatusForm);
		}

		return memberForm;
	}

	//	/**
	//	 * 更新(受講履歴)<br>
	//	 * @return 実行結果
	//	 */
	//	@PostMapping(path = "update_attending_history")
	//	//検討中
	//	public ResponseEntity<String> updateAttendinghistory(@RequestBody @Validated AttendingHistoryUpdateForm attendingHistoryUpdateForm,
	//			@RequestBody @Validated BasicOtAttendingSummaryUpdateForm basicOtAttendingSummaryUpdateForm,
	//			@RequestBody @Validated BasicPointTrainingHistoryForm basicPointTrainingHistoryForm,
	//			Errors errors) {
	//
	//		// バリデーション実行
	//		doValidation(errors);
	//
	//
	//		return ResponseEntity.ok("updateAttendinghistory ok");
	//	}

	//	/**
	//	 * 更新(受講履歴)<br>
	//	 * [補足事項]<br>
	//	 * 本処理は最新の検索結果に対してフォームで指定された値を差分更新している<br>
	//	 * 値がNULLの場合は更新対象とならないため、明示的に文字列を初期化したい場合は空文字を指定する。
	//	 * @return 実行結果
	//	 */
	//	@PostMapping(path = "update_attending_history")
	//	public ResponseEntity<String> updateAttendingHistory(
	//			@RequestBody @Validated AttendingHistoryUpdateForm attendingHistoryUpdateForm, Errors errors) {
	//
	//		// バリデーション実行
	//		doValidation(errors);
	//
	//		// 検索結果取得(キー)
	//		AttendingHistoryDto dto = attendingHistorySrv.getSearchResult(attendingHistoryUpdateForm.getId());
	//		AttendingHistoryEntity entity = dto.getFirstEntity();
	//
	//		// 更新対象DTO生成
	//		AttendingHistoryDto updateDto = AttendingHistoryDto.createSingle();
	//		AttendingHistoryEntity updateEntity = updateDto.getFirstEntity();
	//		BeanUtil.copyProperties(entity, updateEntity, false);
	//		BeanUtil.copyProperties(attendingHistoryUpdateForm, updateEntity, true);
	//
	//		// 実行
	//		attendingHistorySrv.update(updateDto);
	//
	//		return ResponseEntity.ok("updateAttendingHistory ok");
	//	}

	/**
	 * 条件検索(研修番号マスタ)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_training_numbers")
	public ResponseEntity<List<TrainingNumberForm>> searchTrainingNumber(
			@RequestBody @Validated TrainingNumberForm trainingNumberForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		TrainingNumberSearchDto searchDto = new TrainingNumberSearchDto();
		BeanUtil.copyProperties(trainingNumberForm, searchDto, false); // 属性値

		// 検索結果フォーム取得
		Integer year = LocalDate.now().minusMonths(3).getYear();
		List<TrainingNumberForm> forms = trainingNumberSrv.getSearchResultListByFiscalYear(year)
				.getEntities().stream()
				.map(E -> BeanUtil.createCopyProperties(E, TrainingNumberForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索(基礎研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_basic_ot_attending_summary")
	public ResponseEntity<List<BasicOtAttendingSummaryForm>> searchBasicOtAttendingSummary(
			@RequestBody @Validated BasicOtAttendingSummaryForm basicOtAttendingSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		BasicOtAttendingSummarySearchDto searchDto = new BasicOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(basicOtAttendingSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<BasicOtAttendingSummaryForm> forms = basicOtAttendingSummarySrv.getSearchResultByCondition(searchDto)
				.getEntities().stream()
				.map(E -> createBasicOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	//	/**
	//	 * 登録(基礎研修履歴)
	//	 * @return 実行結果
	//	 */
	//	@PostMapping(path = "enter_basic_ot_attending_summary")
	//	public ResponseEntity<String> enterBasicOtAttendingSummary(@RequestBody @Validated BasicOtAttendingSummaryForm basicOtAttendingSummaryForm, Errors errors) {
	//
	//		// バリデーション実行
	//		doValidation(errors);
	//
	//		// 追加対象DTO生成
	//		BasicOtAttendingSummaryDto enterDto = BasicOtAttendingSummaryDto.createSingle();
	//		BasicOtAttendingSummaryCompositeEntity enterCompositeEntity = enterDto.getFirstEntity();
	//
	//		// 基礎研修履歴
	//		BasicOtAttendingSummaryEntity enterBasicOtAttendingSummaryEntity = enterCompositeEntity.getBasicOtAttendingSummaryEntity();
	//		BeanUtil.copyProperties(basicOtAttendingSummaryForm, enterBasicOtAttendingSummaryEntity, false);
	//
	//		// 基礎研修修了認定更新履歴
	//		List<BasicOtQualifyHistoryEntity> enterBasicOtQualifyHistoryEntities = new ArrayList<BasicOtQualifyHistoryEntity>();
	//		List<BasicOtQualifyHistoryForm> basicOtQualifyHistoryForms = basicOtAttendingSummaryForm.getBasicOtQualifyHistories();
	//		if (!CollectionUtils.isEmpty(basicOtQualifyHistoryForms)) {
	//			for (BasicOtQualifyHistoryForm form : basicOtQualifyHistoryForms) {
	//				BasicOtQualifyHistoryEntity entity = new BasicOtQualifyHistoryEntity();
	//				BeanUtil.copyProperties(form, entity, true); // 更新値
	//				entity.setMemberNo(basicOtAttendingSummaryForm.getMemberNo());// 会員番号
	//				enterBasicOtQualifyHistoryEntities.add(entity);
	//			}
	//		}
	//		enterCompositeEntity.setBasicOtQualifyHistoryEntities(enterBasicOtQualifyHistoryEntities);
	//
	//		// 基礎研修受講履歴
	//		List<BasicOtTrainingHistoryEntity> enterBasicOtTrainingHistoryEntities = new ArrayList<BasicOtTrainingHistoryEntity>();
	//		List<BasicOtTrainingHistoryForm> basicOtTrainingHistoryForms = basicOtAttendingSummaryForm.getBasicOtTrainingHistories();
	//		if (!CollectionUtils.isEmpty(basicOtTrainingHistoryForms)) {
	//			for (BasicOtTrainingHistoryForm form : basicOtTrainingHistoryForms) {
	//				BasicOtTrainingHistoryEntity entity = new BasicOtTrainingHistoryEntity();
	//				BeanUtil.copyProperties(form, entity, true); // 更新値
	//				entity.setMemberNo(basicOtAttendingSummaryForm.getMemberNo());// 会員番号
	//				enterBasicOtTrainingHistoryEntities.add(entity);
	//			}
	//		}
	//		enterCompositeEntity.setBasicOtTrainingHistoryEntities(enterBasicOtTrainingHistoryEntities);
	//
	//		// 実行
	//		basicOtAttendingSummarySrv.enter(enterDto);
	//
	//		return ResponseEntity.ok("enterBasicOtAttendingSummary ok");
	//	}

	/**
	 * 更新<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "update_basic_ot_attending_summary")
	public ResponseEntity<String> updateBasicOtAttendingSummary(
			@RequestBody @Validated BasicOtAttendingSummaryUpdateForm basicOtAttendingSummaryUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		BasicOtAttendingSummaryCompositeEntity compositeEntity = basicOtAttendingSummarySrv
				.getSearchResult(basicOtAttendingSummaryUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		BasicOtAttendingSummaryDto updateDto = BasicOtAttendingSummaryDto.createSingle();
		BasicOtAttendingSummaryCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils.isTrue(basicOtAttendingSummaryUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// 基礎研修履歴
		BasicOtAttendingSummaryEntity updateBasicOtAttendingSummaryEntity = updateCompositeEntity
				.getBasicOtAttendingSummaryEntity();
		BeanUtil.copyProperties(compositeEntity.getBasicOtAttendingSummaryEntity(), updateBasicOtAttendingSummaryEntity,
				false); // 現在値
		if (isIncrementalUpdate) {
			BeanUtil.copyProperties(basicOtAttendingSummaryUpdateForm, updateBasicOtAttendingSummaryEntity, true); // 更新値(差分)
		} else {
			BeanUtil.copyProperties(basicOtAttendingSummaryUpdateForm, updateBasicOtAttendingSummaryEntity, false); // 更新値(上書)
		}
		updateCompositeEntity.setBasicOtAttendingSummaryEntity(updateBasicOtAttendingSummaryEntity);

		// 基礎研修修了認定更新履歴
		List<BasicOtQualifyHistoryEntity> updateBasicOtQualifyHistoryEntities = new ArrayList<BasicOtQualifyHistoryEntity>();
		List<BasicOtQualifyHistoryEntity> qualificationEntities = compositeEntity.getBasicOtQualifyHistoryEntities();
		for (BasicOtQualifyHistoryForm form : basicOtAttendingSummaryUpdateForm.getBasicOtQualifyHistories()) {

			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(qualificationEntities)) {
				for (BasicOtQualifyHistoryEntity entity : qualificationEntities) {
					logger.debug("エンティティ : {}", entity.getId());
					logger.debug("フォーム : {}", form.getId());
					if (entity.getId().equals(form.getId())) {
						BasicOtQualifyHistoryEntity updateEntity = new BasicOtQualifyHistoryEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateBasicOtQualifyHistoryEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				BasicOtQualifyHistoryEntity insertEntity = new BasicOtQualifyHistoryEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateBasicOtQualifyHistoryEntities.add(insertEntity);
			}
		}

		// 基礎研修修了認定更新履歴(処理対象)
		updateCompositeEntity.setBasicOtQualifyHistoryEntities(updateBasicOtQualifyHistoryEntities);

		// 基礎研修受講履歴
		List<BasicOtTrainingHistoryEntity> updateBasicOtTrainingHistoryEntities = new ArrayList<BasicOtTrainingHistoryEntity>();
		List<BasicOtTrainingHistoryEntity> localActivityEntities = compositeEntity.getBasicOtTrainingHistoryEntities();
		for (BasicOtTrainingHistoryForm form : basicOtAttendingSummaryUpdateForm.getBasicOtTrainingHistories()) {
			for (BasicOtTrainingHistoryEntity entity : localActivityEntities) {
				if (entity.getId().equals(form.getId())) {
					BasicOtTrainingHistoryEntity updateEntity = new BasicOtTrainingHistoryEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateBasicOtTrainingHistoryEntities.add(updateEntity);
					break;
				}
			}
		}

		// 基礎研修受講履歴(処理対象)
		updateCompositeEntity.setBasicOtTrainingHistoryEntities(updateBasicOtTrainingHistoryEntities);

		// 実行
		basicOtAttendingSummarySrv.update(updateDto);

		return ResponseEntity.ok("updateBasicOtAttendingSummary ok");
	}

	/**
	 * 基礎研修履歴フォーム生成
	 * @param compositeEntity 基礎研修履歴エンティティ(複合)
	 * @return 基礎研修履歴フォーム
	 */
	private BasicOtAttendingSummaryForm createBasicOtAttendingSummaryForm(
			BasicOtAttendingSummaryCompositeEntity compositeEntity) {

		// 基礎研修履歴フォーム
		BasicOtAttendingSummaryForm basicOtAttendingSummaryForm = BeanUtil.createCopyProperties(
				compositeEntity.getBasicOtAttendingSummaryEntity(), BasicOtAttendingSummaryForm.class,
				false);

		// 基礎研修修了認定更新履歴
		List<BasicOtQualifyHistoryEntity> basicOtQualifyHistoryEntities = compositeEntity
				.getBasicOtQualifyHistoryEntities();
		if (!ListUtils.isEmpty(basicOtQualifyHistoryEntities)) {
			basicOtAttendingSummaryForm.setBasicOtQualifyHistories(basicOtQualifyHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, BasicOtQualifyHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// 基礎研修受講履歴
		List<BasicOtTrainingHistoryEntity> basicOtTrainingHistoryEntities = compositeEntity
				.getBasicOtTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(basicOtTrainingHistoryEntities)) {
			basicOtAttendingSummaryForm.setBasicOtTrainingHistories(basicOtTrainingHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, BasicOtTrainingHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// 期間延長申請
		List<PeriodExtensionApplicationEntity> periodExtensionApplicationEntities = compositeEntity
				.getPeriodExtensionApplicationEntities();
		if (!CollectionUtils.isEmpty(periodExtensionApplicationEntities)) {
			basicOtAttendingSummaryForm.setPeriodExtensionApplications(periodExtensionApplicationEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, PeriodExtensionApplicationForm.class, false))
					.collect(Collectors.toList()));
		}

		return basicOtAttendingSummaryForm;
	}

	/**
	 * 条件検索(基礎ポイント研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_basic_point_training_history")
	public ResponseEntity<List<BasicPointTrainingHistoryForm>> searchBasicPointTrainingHistory(
			@RequestBody @Validated BasicPointTrainingHistoryForm basicPointTrainingHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		BasicPointTrainingHistorySearchDto searchDto = new BasicPointTrainingHistorySearchDto();
		BeanUtil.copyProperties(basicPointTrainingHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<BasicPointTrainingHistoryForm> forms = basicPointTrainingHistorySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> createBasicPointTrainingHistoryForm(E))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(基礎ポイント研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_basic_point_training_history")
	public ResponseEntity<String> enterBasicPointTrainingHistory(
			@RequestBody @Validated BasicPointTrainingHistoryForm basicPointTrainingHistoryForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		basicPointTrainingHistorySrv
				.enter(copyDto(basicPointTrainingHistoryForm, BasicPointTrainingHistoryDto.createSingle()));

		return ResponseEntity.ok("enterBasicPointTrainingHistory ok");
	}

	/**
	 * 更新(基礎ポイント研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "update_basic_point_training_history")
	public ResponseEntity<String> updateBasicPointTrainingHistory(
			@RequestBody @Validated BasicPointTrainingHistoryUpdateForm basicPointTrainingHistoryUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		BasicPointTrainingHistoryCompositeEntity compositeEntity = basicPointTrainingHistorySrv
				.getSearchResult(basicPointTrainingHistoryUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		BasicPointTrainingHistoryDto updateDto = BasicPointTrainingHistoryDto.createSingle();
		BasicPointTrainingHistoryCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils.isTrue(basicPointTrainingHistoryUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// 基礎ポイント研修履歴
		List<BasicPointTrainingHistoryEntity> updateBasicPointTrainingHistoryEntities = new ArrayList<BasicPointTrainingHistoryEntity>();
		List<BasicPointTrainingHistoryEntity> localActivityEntities = compositeEntity
				.getBasicPointTrainingHistoryEntities();
		for (BasicPointTrainingHistoryForm form : basicPointTrainingHistoryUpdateForm
				.getBasicPointTrainingHistories()) {
			for (BasicPointTrainingHistoryEntity entity : localActivityEntities) {
				if (entity.getId().equals(form.getId())) {
					BasicPointTrainingHistoryEntity updateEntity = new BasicPointTrainingHistoryEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateBasicPointTrainingHistoryEntities.add(updateEntity);
					break;
				}
			}
		}

		// 基礎ポイント研修履歴(処理対象)
		updateCompositeEntity.setBasicPointTrainingHistoryEntities(updateBasicPointTrainingHistoryEntities);

		// 実行
		basicPointTrainingHistorySrv.update(updateDto);

		return ResponseEntity.ok("updateBasicPointTrainingHistoryok");
	}

	/**
	 * 基礎研修履歴フォーム生成
	 * @param compositeEntity 基礎研修履歴エンティティ(複合)
	 * @return 基礎研修履歴フォーム
	 */
	private BasicPointTrainingHistoryForm createBasicPointTrainingHistoryForm(
			BasicPointTrainingHistoryCompositeEntity compositeEntity) {
		// 基礎ポイント研修履歴フォーム
		BasicPointTrainingHistoryForm basicPointTrainingHistoryForm = BeanUtil.createCopyProperties(
				compositeEntity.getBasicPointTrainingHistoryEntities(), BasicPointTrainingHistoryForm.class,
				false);

		// 基礎ポイント研修履歴
		List<BasicPointTrainingHistoryEntity> basicPointTrainingHistoryEntities = compositeEntity
				.getBasicPointTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(basicPointTrainingHistoryEntities)) {
			basicPointTrainingHistoryForm.setBasicPointTrainingHistories(basicPointTrainingHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, BasicPointTrainingHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		return basicPointTrainingHistoryForm;
	}

	/**
	 * 条件検索(認定作業療法士研修受講履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_qualification_ot_attending_history")
	public ResponseEntity<List<QualificationOtAttendingHistoryForm>> searchQualificationOtAttendingHistory(
			@RequestBody @Validated QualificationOtAttendingHistoryForm qualificationOtAttendingHistoryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		QualificationOtAttendingHistorySearchDto searchDto = new QualificationOtAttendingHistorySearchDto();
		BeanUtil.copyProperties(qualificationOtAttendingHistoryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<QualificationOtAttendingHistoryForm> forms = qualificationOtAttendingHistorySrv
				.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, QualificationOtAttendingHistoryForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 更新(認定作業療法士研修受講履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "update_qualification_ot_attending_history")
	public ResponseEntity<String> updateQualificationOtAttendingHistory(
			@RequestBody @Validated QualificationOtAttendingHistoryForm qualificationOtAttendingHistoryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		qualificationOtAttendingHistorySrv.update(
				copyDto(qualificationOtAttendingHistoryForm, QualificationOtAttendingHistoryDto.createSingle()));

		return ResponseEntity.ok("updateQualificationOtAttendingHistory ok");
	}

	/**
	 * 条件検索(認定作業療法士研修履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_qualified_ot_attending_summary")
	public ResponseEntity<List<QualifiedOtAttendingSummaryForm>> searchQualifiedOtAttendingSummary(
			@RequestBody @Validated QualifiedOtAttendingSummaryForm qualifiedOtAttendingSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		QualifiedOtAttendingSummarySearchDto searchDto = new QualifiedOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(qualifiedOtAttendingSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<QualifiedOtAttendingSummaryForm> forms = qualifiedOtAttendingSummarySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createQualifiedOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	//	/**
	//	 * 登録(認定作業療法士研修履歴)
	//	 * @return 実行結果
	//	 */
	//	@PostMapping(path = "enter_qualified_ot_attending_summary")
	//	public ResponseEntity<String> enterQualifiedOtAttendingSummary(
	//			@RequestBody @Validated QualifiedOtAttendingSummaryForm qualifiedOtAttendingSummaryForm, Errors errors) {
	//
	//		// バリデーション実行
	//		doValidation(errors);
	//
	//		// 実行
	//		qualifiedOtAttendingSummarySrv.enter(copyDto(qualifiedOtAttendingSummaryForm, QualifiedOtAttendingSummaryDto.createSingle()));
	//
	//		return ResponseEntity.ok("enterQualifiedOtAttendingSummary ok");
	//	}

	/**
	 * 更新<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "update_qualified_ot_attending_summary")
	public ResponseEntity<String> updateQualifiedOtAttendingSummary(
			@RequestBody @Validated QualifiedOtAttendingSummaryUpdateForm QualifiedOtAttendingSummaryUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		QualifiedOtAttendingSummaryCompositeEntity compositeEntity = qualifiedOtAttendingSummarySrv
				.getSearchResult(QualifiedOtAttendingSummaryUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		QualifiedOtAttendingSummaryDto updateDto = QualifiedOtAttendingSummaryDto.createSingle();
		QualifiedOtAttendingSummaryCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils
				.isTrue(QualifiedOtAttendingSummaryUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// 認定作業療法士研修履歴
		QualifiedOtAttendingSummaryEntity updateQualifiedOtAttendingSummaryEntity = updateCompositeEntity
				.getQualifiedOtAttendingSummaryEntity();
		BeanUtil.copyProperties(compositeEntity.getQualifiedOtAttendingSummaryEntity(),
				updateQualifiedOtAttendingSummaryEntity, false); // 現在値
		if (isIncrementalUpdate) {
			BeanUtil.copyProperties(QualifiedOtAttendingSummaryUpdateForm, updateQualifiedOtAttendingSummaryEntity,
					true); // 更新値(差分)
		} else {
			BeanUtil.copyProperties(QualifiedOtAttendingSummaryUpdateForm, updateQualifiedOtAttendingSummaryEntity,
					false); // 更新値(上書)
		}
		updateCompositeEntity.setQualifiedOtAttendingSummaryEntity(updateQualifiedOtAttendingSummaryEntity);

		// 認定作業療法士研修修了認定更新履歴
		List<QualifiedOtCompletionHistoryEntity> updateQualifiedOtCompletionHistoryEntities = new ArrayList<QualifiedOtCompletionHistoryEntity>();
		List<QualifiedOtCompletionHistoryEntity> qualificationEntities = compositeEntity
				.getQualifiedOtCompletionHistoryEntities();
		for (QualifiedOtCompletionHistoryForm form : QualifiedOtAttendingSummaryUpdateForm
				.getQualifiedOtCompletionHistories()) {

			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(qualificationEntities)) {
				for (QualifiedOtCompletionHistoryEntity entity : qualificationEntities) {
					if (entity.getId().equals(form.getId())) {
						QualifiedOtCompletionHistoryEntity updateEntity = new QualifiedOtCompletionHistoryEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateQualifiedOtCompletionHistoryEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				QualifiedOtCompletionHistoryEntity insertEntity = new QualifiedOtCompletionHistoryEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateQualifiedOtCompletionHistoryEntities.add(insertEntity);
			}
		}

		// 認定作業療法士研修修了認定更新履歴(処理対象)
		updateCompositeEntity.setQualifiedOtCompletionHistoryEntities(updateQualifiedOtCompletionHistoryEntities);

		// 認定作業療法士研修受講履歴
		List<QualifiedOtTrainingHistoryEntity> updateQualifiedOtTrainingHistoryEntities = new ArrayList<QualifiedOtTrainingHistoryEntity>();
		List<QualifiedOtTrainingHistoryEntity> localActivityEntities = compositeEntity
				.getQualifiedOtTrainingHistoryEntities();
		for (QualifiedOtTrainingHistoryForm form : QualifiedOtAttendingSummaryUpdateForm
				.getQualifiedOtTrainingHistories()) {
			for (QualifiedOtTrainingHistoryEntity entity : localActivityEntities) {
				if (entity.getId().equals(form.getId())) {
					QualifiedOtTrainingHistoryEntity updateEntity = new QualifiedOtTrainingHistoryEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateQualifiedOtTrainingHistoryEntities.add(updateEntity);
					break;
				}
			}
		}

		//認定作業療法士研修受講履歴(処理対象)
		updateCompositeEntity.setQualifiedOtTrainingHistoryEntities(updateQualifiedOtTrainingHistoryEntities);

		// 事例報告
		List<CaseReportEntity> updateCaseReportEntities = new ArrayList<CaseReportEntity>();
		List<CaseReportEntity> caseReportEntities = compositeEntity
				.getCaseReportEntities();
		for (CaseReportForm form : QualifiedOtAttendingSummaryUpdateForm
				.getCaseReports()) {
			for (CaseReportEntity entity : caseReportEntities) {
				if (entity.getId().equals(form.getId())) {
					CaseReportEntity updateEntity = new CaseReportEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateCaseReportEntities.add(updateEntity);
					break;
				}
			}
		}

		// 事例報告(処理対象)
		updateCompositeEntity.setCaseReportEntities(updateCaseReportEntities);

		// 実行
		qualifiedOtAttendingSummarySrv.update(updateDto);

		return ResponseEntity.ok("updateQualifiedOtAttendingSummary ok");
	}

	/**
	 * 認定作業療法士研修履歴フォーム生成
	 * @param compositeEntity 基礎研修履歴エンティティ(複合)
	 * @return 基礎研修履歴フォーム
	 */
	private QualifiedOtAttendingSummaryForm createQualifiedOtAttendingSummaryForm(
			QualifiedOtAttendingSummaryCompositeEntity compositeEntity) {

		// 認定作業療法士研修履歴フォーム
		QualifiedOtAttendingSummaryForm qualifiedOtAttendingSummaryForm = BeanUtil.createCopyProperties(
				compositeEntity.getQualifiedOtAttendingSummaryEntity(), QualifiedOtAttendingSummaryForm.class,
				false);

		// 認定作業療法士研修修了認定更新履歴
		List<QualifiedOtCompletionHistoryEntity> qualifiedOtCompletionHistoryEntities = compositeEntity
				.getQualifiedOtCompletionHistoryEntities();
		if (!ListUtils.isEmpty(qualifiedOtCompletionHistoryEntities)) {
			qualifiedOtAttendingSummaryForm
					.setQualifiedOtCompletionHistories(qualifiedOtCompletionHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, QualifiedOtCompletionHistoryForm.class, false))
							.collect(Collectors.toList()));
		}

		// 認定作業療法士研修受講履歴
		List<QualifiedOtTrainingHistoryEntity> qualifiedOtTrainingHistoryEntities = compositeEntity
				.getQualifiedOtTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(qualifiedOtTrainingHistoryEntities)) {
			qualifiedOtAttendingSummaryForm.setQualifiedOtTrainingHistories(qualifiedOtTrainingHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, QualifiedOtTrainingHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// 事例報告
		List<CaseReportEntity> caseReportEntities = compositeEntity
				.getCaseReportEntities();
		if (!CollectionUtils.isEmpty(caseReportEntities)) {
			qualifiedOtAttendingSummaryForm.setCaseReports(caseReportEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, CaseReportForm.class, false))
					.collect(Collectors.toList()));
		}

		return qualifiedOtAttendingSummaryForm;
	}

	/**
	 * 条件検索(専門作業療法士受講履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_professional_ot_attending_summary")
	public ResponseEntity<List<ProfessionalOtAttendingSummaryForm>> searchProfessionalOtAttendingSummary(
			@RequestBody @Validated ProfessionalOtAttendingSummaryForm professionalOtAttendingSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ProfessionalOtAttendingSummarySearchDto searchDto = new ProfessionalOtAttendingSummarySearchDto();
		BeanUtil.copyProperties(professionalOtAttendingSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<ProfessionalOtAttendingSummaryForm> forms = professionalOtAttendingSummarySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createProfessionalOtAttendingSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	//	/**
	//	 * 登録
	//	 * @return 実行結果
	//	 */
	//	@PostMapping(path = "enter_professional_ot_attending_summary")
	//	public ResponseEntity<String> enterProfessionalOtAttendingSummary(@RequestBody @Validated ProfessionalOtAttendingSummaryForm professionalOtAttendingSummaryForm, Errors errors) {
	//
	//		// バリデーション実行
	//		doValidation(errors);
	//
	//		// 追加対象DTO生成
	//		ProfessionalOtAttendingSummaryDto enterDto = ProfessionalOtAttendingSummaryDto.createSingle();
	//		ProfessionalOtAttendingSummaryCompositeEntity enterCompositeEntity = enterDto.getFirstEntity();
	//
	//		// 専門作業療法士受講履歴履歴
	//		ProfessionalOtAttendingSummaryEntity enterProfessionalOtAttendingSummaryEntity = enterCompositeEntity.getProfessionalOtAttendingSummaryEntity();
	//		BeanUtil.copyProperties(professionalOtAttendingSummaryForm, enterProfessionalOtAttendingSummaryEntity, false);
	//
	//		// 専門作業療法士認定更新履歴
	//		List<ProfessionalOtQualifyHistoryEntity> enterProfessionalOtQualifyHistoryEntities = new ArrayList<ProfessionalOtQualifyHistoryEntity>();
	//		List<ProfessionalOtQualifyHistoryForm> professionalOtQualifyHistoryForms = professionalOtAttendingSummaryForm.getProfessionalOtQualifyHistories();
	//		if (!CollectionUtils.isEmpty(professionalOtQualifyHistoryForms)) {
	//			for (ProfessionalOtQualifyHistoryForm form : professionalOtQualifyHistoryForms) {
	//				ProfessionalOtQualifyHistoryEntity entity = new ProfessionalOtQualifyHistoryEntity();
	//				BeanUtil.copyProperties(form, entity, true); // 更新値
	//				entity.setMemberNo(professionalOtAttendingSummaryForm.getMemberNo());// 会員番号
	//				enterProfessionalOtQualifyHistoryEntities.add(entity);
	//			}
	//		}
	//		enterCompositeEntity.setProfessionalOtQualifyHistoryEntities(enterProfessionalOtQualifyHistoryEntities);
	//
	//		// 専門作業療法士試験結果履歴
	//		List<ProfessionalOtTestHistoryEntity> enterProfessionalOtTestHistoryEntities = new ArrayList<ProfessionalOtTestHistoryEntity>();
	//		List<ProfessionalOtTestHistoryForm> professionalOtTestHistoryForms = professionalOtAttendingSummaryForm.getProfessionalOtTestHistories();
	//		if (!CollectionUtils.isEmpty(professionalOtTestHistoryForms)) {
	//			for (ProfessionalOtTestHistoryForm form : professionalOtTestHistoryForms) {
	//				ProfessionalOtTestHistoryEntity entity = new ProfessionalOtTestHistoryEntity();
	//				BeanUtil.copyProperties(form, entity, true); // 更新値
	//				entity.setMemberNo(professionalOtAttendingSummaryForm.getMemberNo());// 会員番号
	//				enterProfessionalOtTestHistoryEntities.add(entity);
	//			}
	//		}
	//		enterCompositeEntity.setProfessionalOtTestHistoryEntities(enterProfessionalOtTestHistoryEntities);
	//
	//
	//		// 専門作業療法士研修受講履歴
	//		List<ProfessionalOtTrainingHistoryEntity> enterProfessionalOtTrainingHistoryEntities = new ArrayList<ProfessionalOtTrainingHistoryEntity>();
	//		List<ProfessionalOtTrainingHistoryForm> professionalOtTrainingHistoryForms = professionalOtAttendingSummaryForm.getProfessionalOtTrainingHistories();
	//		if (!CollectionUtils.isEmpty(professionalOtTrainingHistoryForms)) {
	//			for (ProfessionalOtTrainingHistoryForm form : professionalOtTrainingHistoryForms) {
	//				ProfessionalOtTrainingHistoryEntity entity = new ProfessionalOtTrainingHistoryEntity();
	//				BeanUtil.copyProperties(form, entity, true); // 更新値
	//				entity.setMemberNo(professionalOtAttendingSummaryForm.getMemberNo());// 会員番号
	//				enterProfessionalOtTrainingHistoryEntities.add(entity);
	//			}
	//		}
	//		enterCompositeEntity.setProfessionalOtTrainingHistoryEntities(enterProfessionalOtTrainingHistoryEntities);
	//
	//		// 実行
	//		professionalOtAttendingSummarySrv.enter(enterDto);
	//
	//		return ResponseEntity.ok("enterProfessionalOtAttendingSummary ok");
	//	}

	/**
	 * 更新<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "update_professional_ot_attending_summary")
	public ResponseEntity<String> updateProfessionalOtAttendingSummary(
			@RequestBody @Validated ProfessionalOtAttendingSummaryUpdateForm ProfessionalOtAttendingSummaryUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		ProfessionalOtAttendingSummaryCompositeEntity compositeEntity = professionalOtAttendingSummarySrv
				.getSearchResult(ProfessionalOtAttendingSummaryUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		ProfessionalOtAttendingSummaryDto updateDto = ProfessionalOtAttendingSummaryDto.createSingle();
		ProfessionalOtAttendingSummaryCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils
				.isTrue(ProfessionalOtAttendingSummaryUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// 専門作業療法士受講履歴
		ProfessionalOtAttendingSummaryEntity updateProfessionalOtAttendingSummaryEntity = updateCompositeEntity
				.getProfessionalOtAttendingSummaryEntity();
		BeanUtil.copyProperties(compositeEntity.getProfessionalOtAttendingSummaryEntity(),
				updateProfessionalOtAttendingSummaryEntity, false); // 現在値
		if (isIncrementalUpdate) {
			BeanUtil.copyProperties(ProfessionalOtAttendingSummaryUpdateForm,
					updateProfessionalOtAttendingSummaryEntity, true); // 更新値(差分)
		} else {
			BeanUtil.copyProperties(ProfessionalOtAttendingSummaryUpdateForm,
					updateProfessionalOtAttendingSummaryEntity, false); // 更新値(上書)
		}
		updateCompositeEntity.setProfessionalOtAttendingSummaryEntity(updateProfessionalOtAttendingSummaryEntity);

		// 専門作業療法士認定更新履歴
		List<ProfessionalOtQualifyHistoryEntity> updateProfessionalOtQualifyHistoryEntities = new ArrayList<ProfessionalOtQualifyHistoryEntity>();
		List<ProfessionalOtQualifyHistoryEntity> qualificationEntities = compositeEntity
				.getProfessionalOtQualifyHistoryEntities();
		for (ProfessionalOtQualifyHistoryForm form : ProfessionalOtAttendingSummaryUpdateForm
				.getProfessionalOtQualifyHistories()) {

			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(qualificationEntities)) {
				for (ProfessionalOtQualifyHistoryEntity entity : qualificationEntities) {
					if (entity.getId().equals(form.getId())) {
						ProfessionalOtQualifyHistoryEntity updateEntity = new ProfessionalOtQualifyHistoryEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateProfessionalOtQualifyHistoryEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				ProfessionalOtQualifyHistoryEntity insertEntity = new ProfessionalOtQualifyHistoryEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateProfessionalOtQualifyHistoryEntities.add(insertEntity);
			}
		}

		// 専門作業療法士認定更新履歴(処理対象)
		updateCompositeEntity.setProfessionalOtQualifyHistoryEntities(updateProfessionalOtQualifyHistoryEntities);

		// 専門作業療法士試験結果履歴
		List<ProfessionalOtTestHistoryEntity> updateProfessionalOtTestHistoryEntities = new ArrayList<ProfessionalOtTestHistoryEntity>();
		List<ProfessionalOtTestHistoryEntity> localActivityEntities = compositeEntity
				.getProfessionalOtTestHistoryEntities();
		for (ProfessionalOtTestHistoryForm form : ProfessionalOtAttendingSummaryUpdateForm
				.getProfessionalOtTestHistories()) {
			for (ProfessionalOtTestHistoryEntity entity : localActivityEntities) {
				if (entity.getId().equals(form.getId())) {
					ProfessionalOtTestHistoryEntity updateEntity = new ProfessionalOtTestHistoryEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateProfessionalOtTestHistoryEntities.add(updateEntity);
					break;
				}
			}
		}

		//専門作業療法士試験結果履歴(処理対象)
		updateCompositeEntity.setProfessionalOtTestHistoryEntities(updateProfessionalOtTestHistoryEntities);

		// 専門作業療法士研修受講履歴
		List<ProfessionalOtTrainingHistoryEntity> updateProfessionalOtTrainingHistoryEntities = new ArrayList<ProfessionalOtTrainingHistoryEntity>();
		List<ProfessionalOtTrainingHistoryEntity> localActivityEntities1 = compositeEntity
				.getProfessionalOtTrainingHistoryEntities();
		for (ProfessionalOtTrainingHistoryForm form : ProfessionalOtAttendingSummaryUpdateForm
				.getProfessionalOtTrainingHistories()) {
			for (ProfessionalOtTrainingHistoryEntity entity : localActivityEntities1) {
				if (entity.getId().equals(form.getId())) {
					ProfessionalOtTrainingHistoryEntity updateEntity = new ProfessionalOtTrainingHistoryEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateProfessionalOtTrainingHistoryEntities.add(updateEntity);
					break;
				}
			}
		}

		// 専門作業療法士研修受講履歴(処理対象)
		updateCompositeEntity.setProfessionalOtTrainingHistoryEntities(updateProfessionalOtTrainingHistoryEntities);

		// 実行
		professionalOtAttendingSummarySrv.update(updateDto);

		return ResponseEntity.ok("updateProfessionalOtAttendingSummary ok");
	}

	/**
	 * 専門作業療法士受講履歴フォーム生成
	 * @param compositeEntity 専門作業療法士受講履歴エンティティ(複合)
	 * @return 専門作業療法士受講履歴フォーム
	 */
	private ProfessionalOtAttendingSummaryForm createProfessionalOtAttendingSummaryForm(
			ProfessionalOtAttendingSummaryCompositeEntity compositeEntity) {

		// 専門作業療法士受講履歴フォーム
		ProfessionalOtAttendingSummaryForm professionalOtAttendingSummaryForm = BeanUtil.createCopyProperties(
				compositeEntity.getProfessionalOtAttendingSummaryEntity(), ProfessionalOtAttendingSummaryForm.class,
				false);

		// 専門作業療法士認定更新履歴
		List<ProfessionalOtQualifyHistoryEntity> professionalOtQualifyHistoryEntities = compositeEntity
				.getProfessionalOtQualifyHistoryEntities();
		if (!ListUtils.isEmpty(professionalOtQualifyHistoryEntities)) {
			professionalOtAttendingSummaryForm
					.setProfessionalOtQualifyHistories(professionalOtQualifyHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, ProfessionalOtQualifyHistoryForm.class, false))
							.collect(Collectors.toList()));
		}

		// 専門作業療法士試験結果履歴
		List<ProfessionalOtTestHistoryEntity> professionalOtTestHistoryEntities = compositeEntity
				.getProfessionalOtTestHistoryEntities();
		if (!CollectionUtils.isEmpty(professionalOtTestHistoryEntities)) {
			professionalOtAttendingSummaryForm.setProfessionalOtTestHistories(professionalOtTestHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, ProfessionalOtTestHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// 専門作業療法士研修受講履歴
		List<ProfessionalOtTrainingHistoryEntity> professionalOtTrainingHistoryEntities = compositeEntity
				.getProfessionalOtTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(professionalOtTrainingHistoryEntities)) {
			professionalOtAttendingSummaryForm
					.setProfessionalOtTrainingHistories(professionalOtTrainingHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, ProfessionalOtTrainingHistoryForm.class, false))
							.collect(Collectors.toList()));
		}

		return professionalOtAttendingSummaryForm;
	}

	/**
	 * 条件検索(臨床実習指導者履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_clinical_training_leader_summary")
	public ResponseEntity<List<ClinicalTrainingLeaderSummaryForm>> searchClinicalTrainingLeaderSummary(
			@RequestBody @Validated ClinicalTrainingLeaderSummaryForm clinicalTrainingLeaderSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		ClinicalTrainingLeaderSummarySearchDto searchDto = new ClinicalTrainingLeaderSummarySearchDto();
		BeanUtil.copyProperties(clinicalTrainingLeaderSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<ClinicalTrainingLeaderSummaryForm> forms = clinicalTrainingLeaderSummarySrv
				.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createClinicalTrainingLeaderSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	//	/**
	//	 * 登録(臨床実習指導者履歴)
	//	 * @return 実行結果
	//	 */
	//	@PostMapping(path = "enter_clinical_training_leader_summary")
	//	public ResponseEntity<String> enterClinicalTrainingLeaderSummary(
	//			@RequestBody @Validated ClinicalTrainingLeaderSummaryForm clinicalTrainingLeaderSummaryForm, Errors errors) {
	//
	//		// バリデーション実行
	//		doValidation(errors);
	//
	//		// 実行
	//		clinicalTrainingLeaderSummarySrv.enter(copyDto(clinicalTrainingLeaderSummaryForm, ClinicalTrainingLeaderSummaryDto.createSingle()));
	//
	//		return ResponseEntity.ok("enterClinicalTrainingLeaderSummary ok");
	//	}

	/**
	 * 更新<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "update_clinical_training_leader_summary")
	public ResponseEntity<String> updateClinicalTrainingLeaderSummary(
			@RequestBody @Validated ClinicalTrainingLeaderSummaryUpdateForm ClinicalTrainingLeaderSummaryUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		ClinicalTrainingLeaderSummaryCompositeEntity compositeEntity = clinicalTrainingLeaderSummarySrv
				.getSearchResult(ClinicalTrainingLeaderSummaryUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		ClinicalTrainingLeaderSummaryDto updateDto = ClinicalTrainingLeaderSummaryDto.createSingle();
		ClinicalTrainingLeaderSummaryCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils
				.isTrue(ClinicalTrainingLeaderSummaryUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// 臨床作業療法士受講履歴
		ClinicalTrainingLeaderSummaryEntity updateClinicalTrainingLeaderSummaryEntity = updateCompositeEntity
				.getClinicalTrainingLeaderSummaryEntity();
		BeanUtil.copyProperties(compositeEntity.getClinicalTrainingLeaderSummaryEntity(),
				updateClinicalTrainingLeaderSummaryEntity, false); // 現在値
		if (isIncrementalUpdate) {
			BeanUtil.copyProperties(ClinicalTrainingLeaderSummaryUpdateForm, updateClinicalTrainingLeaderSummaryEntity,
					true); // 更新値(差分)
		} else {
			BeanUtil.copyProperties(ClinicalTrainingLeaderSummaryUpdateForm, updateClinicalTrainingLeaderSummaryEntity,
					false); // 更新値(上書)
		}
		updateCompositeEntity.setClinicalTrainingLeaderSummaryEntity(updateClinicalTrainingLeaderSummaryEntity);

		// 臨床作業療法士認定更新履歴
		List<ClinicalTrainingLeaderQualifyHistoryEntity> updateClinicalTrainingLeaderQualifyHistoryEntities = new ArrayList<ClinicalTrainingLeaderQualifyHistoryEntity>();
		List<ClinicalTrainingLeaderQualifyHistoryEntity> qualificationEntities = compositeEntity
				.getClinicalTrainingLeaderQualifyHistoryEntities();
		for (ClinicalTrainingLeaderQualifyHistoryForm form : ClinicalTrainingLeaderSummaryUpdateForm
				.getClinicalTrainingLeaderQualifyHistories()) {

			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(qualificationEntities)) {
				for (ClinicalTrainingLeaderQualifyHistoryEntity entity : qualificationEntities) {
					if (entity.getId().equals(form.getId())) {
						ClinicalTrainingLeaderQualifyHistoryEntity updateEntity = new ClinicalTrainingLeaderQualifyHistoryEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateClinicalTrainingLeaderQualifyHistoryEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				ClinicalTrainingLeaderQualifyHistoryEntity insertEntity = new ClinicalTrainingLeaderQualifyHistoryEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateClinicalTrainingLeaderQualifyHistoryEntities.add(insertEntity);
			}
		}

		// 臨床作業療法士認定更新履歴(処理対象)
		updateCompositeEntity
				.setClinicalTrainingLeaderQualifyHistoryEntities(updateClinicalTrainingLeaderQualifyHistoryEntities);

		// 臨床作業療法士試験結果履歴
		List<ClinicalTrainingLeaderTrainingHistoryEntity> updateClinicalTrainingLeaderTrainingHistoryEntities = new ArrayList<ClinicalTrainingLeaderTrainingHistoryEntity>();
		List<ClinicalTrainingLeaderTrainingHistoryEntity> localActivityEntities1 = compositeEntity
				.getClinicalTrainingLeaderTrainingHistoryEntities();
		for (ClinicalTrainingLeaderTrainingHistoryForm form : ClinicalTrainingLeaderSummaryUpdateForm
				.getClinicalTrainingLeaderTrainingHistories()) {
			for (ClinicalTrainingLeaderTrainingHistoryEntity entity : localActivityEntities1) {
				if (entity.getId().equals(form.getId())) {
					ClinicalTrainingLeaderTrainingHistoryEntity updateEntity = new ClinicalTrainingLeaderTrainingHistoryEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateClinicalTrainingLeaderTrainingHistoryEntities.add(updateEntity);
					break;
				}
			}
		}

		//臨床作業療法士試験結果履歴(処理対象)
		updateCompositeEntity
				.setClinicalTrainingLeaderTrainingHistoryEntities(updateClinicalTrainingLeaderTrainingHistoryEntities);

		// 実行
		clinicalTrainingLeaderSummarySrv.update(updateDto);

		return ResponseEntity.ok("updateClinicalTrainingLeaderSummary ok");
	}

	/**
	 * 臨床作業療法士受講履歴フォーム生成
	 * @param compositeEntity 専門作業療法士受講履歴エンティティ(複合)
	 * @return 臨床作業療法士受講履歴フォーム
	 */
	private ClinicalTrainingLeaderSummaryForm createClinicalTrainingLeaderSummaryForm(
			ClinicalTrainingLeaderSummaryCompositeEntity compositeEntity) {

		// 臨床作業療法士受講履歴フォーム
		ClinicalTrainingLeaderSummaryForm clinicalTrainingLeaderSummaryForm = BeanUtil.createCopyProperties(
				compositeEntity.getClinicalTrainingLeaderSummaryEntity(), ClinicalTrainingLeaderSummaryForm.class,
				false);

		// 臨床作業療法士認定更新履歴
		List<ClinicalTrainingLeaderQualifyHistoryEntity> clinicalTrainingLeaderQualifyHistoryEntities = compositeEntity
				.getClinicalTrainingLeaderQualifyHistoryEntities();
		if (!ListUtils.isEmpty(clinicalTrainingLeaderQualifyHistoryEntities)) {
			clinicalTrainingLeaderSummaryForm
					.setClinicalTrainingLeaderQualifyHistories(clinicalTrainingLeaderQualifyHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, ClinicalTrainingLeaderQualifyHistoryForm.class,
									false))
							.collect(Collectors.toList()));
		}

		// 臨床作業療法士試験結果履歴
		List<ClinicalTrainingLeaderTrainingHistoryEntity> clinicalTrainingLeaderTrainingHistoryEntities = compositeEntity
				.getClinicalTrainingLeaderTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(clinicalTrainingLeaderTrainingHistoryEntities)) {
			clinicalTrainingLeaderSummaryForm
					.setClinicalTrainingLeaderTrainingHistories(clinicalTrainingLeaderTrainingHistoryEntities.stream()
							.map(E -> BeanUtil.createCopyProperties(E, ClinicalTrainingLeaderTrainingHistoryForm.class,
									false))
							.collect(Collectors.toList()));
		}

		return clinicalTrainingLeaderSummaryForm;
	}

	/**
	 * 条件検索(MTDLP履歴)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_mtdlp_summary")
	public ResponseEntity<List<MtdlpSummaryForm>> searchMtdlpSummary(
			@RequestBody @Validated MtdlpSummaryForm mtdlpSummaryForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MtdlpSummarySearchDto searchDto = new MtdlpSummarySearchDto();
		BeanUtil.copyProperties(mtdlpSummaryForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MtdlpSummaryForm> forms = mtdlpSummarySrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createMtdlpSummaryForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 更新<br>
	 * @return 実行結果
	 */
	@PostMapping(path = "update_mtdlp_summary")
	public ResponseEntity<String> updateMtdlpSummary(
			@RequestBody @Validated MtdlpSummaryUpdateForm MtdlpSummaryUpdateForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		MtdlpCompositeEntity compositeEntity = mtdlpSummarySrv.getSearchResult(MtdlpSummaryUpdateForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		MtdlpSummaryDto updateDto = MtdlpSummaryDto.createSingle();
		MtdlpCompositeEntity updateCompositeEntity = updateDto.getFirstEntity();

		// 差分更新フラグ
		boolean isIncrementalUpdate = BooleanUtils.isTrue(MtdlpSummaryUpdateForm.getIsIncrementalUpdate());
		logger.debug("差分更新フラグ : {}", isIncrementalUpdate);

		// MTDLP履歴
		MtdlpSummaryEntity updateMtdlpSummaryEntity = updateCompositeEntity.getMtdlpSummaryEntity();
		BeanUtil.copyProperties(compositeEntity.getMtdlpSummaryEntity(), updateMtdlpSummaryEntity, false); // 現在値
		if (isIncrementalUpdate) {
			BeanUtil.copyProperties(MtdlpSummaryUpdateForm, updateMtdlpSummaryEntity, true); // 更新値(差分)
		} else {
			BeanUtil.copyProperties(MtdlpSummaryUpdateForm, updateMtdlpSummaryEntity, false); // 更新値(上書)
		}
		updateCompositeEntity.setMtdlpSummaryEntity(updateMtdlpSummaryEntity);

		// MTDLP認定履歴
		List<MtdlpQualifyHistoryEntity> updateMtdlpQualifyHistoryEntities = new ArrayList<MtdlpQualifyHistoryEntity>();
		List<MtdlpQualifyHistoryEntity> qualificationEntities = compositeEntity.getMtdlpQualifyHistoryEntities();
		for (MtdlpQualifyHistoryForm form : MtdlpSummaryUpdateForm.getMtdlpQualifyHistories()) {

			// 更新 or 削除
			boolean isExist = false;
			if (!CollectionUtils.isEmpty(qualificationEntities)) {
				for (MtdlpQualifyHistoryEntity entity : qualificationEntities) {
					if (entity.getId().equals(form.getId())) {
						MtdlpQualifyHistoryEntity updateEntity = new MtdlpQualifyHistoryEntity();
						BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
						if (isIncrementalUpdate) {
							BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
						} else {
							BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
						}
						updateMtdlpQualifyHistoryEntities.add(updateEntity);
						isExist = true;
						break;
					}
				}
			}

			// 追加
			if (!isExist && BooleanUtils.isFalse(form.getDeleted())) {
				MtdlpQualifyHistoryEntity insertEntity = new MtdlpQualifyHistoryEntity();
				BeanUtil.copyProperties(form, insertEntity, false); // 追加値
				insertEntity.setId(null);// ID(自動採番)を明示的に初期化
				updateMtdlpQualifyHistoryEntities.add(insertEntity);
			}
		}

		// MTDLP認定履歴(処理対象)
		updateCompositeEntity.setMtdlpQualifyHistoryEntities(updateMtdlpQualifyHistoryEntities);

		// MTDLP研修受講履歴
		List<MtdlpTrainingHistoryEntity> updateMtdlpTrainingHistoryEntities = new ArrayList<MtdlpTrainingHistoryEntity>();
		List<MtdlpTrainingHistoryEntity> localActivityEntities1 = compositeEntity.getMtdlpTrainingHistoryEntities();
		for (MtdlpTrainingHistoryForm form : MtdlpSummaryUpdateForm.getMtdlpTrainingHistories()) {
			for (MtdlpTrainingHistoryEntity entity : localActivityEntities1) {
				if (entity.getId().equals(form.getId())) {
					MtdlpTrainingHistoryEntity updateEntity = new MtdlpTrainingHistoryEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateMtdlpTrainingHistoryEntities.add(updateEntity);
					break;
				}
			}
		}

		//MTDLP研修受講履歴(処理対象)
		updateCompositeEntity.setMtdlpTrainingHistoryEntities(updateMtdlpTrainingHistoryEntities);

		// MTDLP事例
		//		List<MtdlpCaseEntity> updateMtdlpCaseEntities = new ArrayList<MtdlpCaseEntity>();
		//		List<MtdlpCaseEntity> localActivityEntities2 = compositeEntity.getMtdlpCaseEntities();
		//		for (MtdlpCaseForm form : MtdlpSummaryUpdateForm.getMtdlpCases()) {
		//			for (MtdlpCaseEntity entity : localActivityEntities2) {
		//				if (entity.getId().equals(form.getId())) {
		//					MtdlpCaseEntity updateEntity = new MtdlpCaseEntity();
		//					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
		//					if (isIncrementalUpdate) {
		//						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
		//					} else {
		//						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
		//					}
		//					updateMtdlpCaseEntities.add(updateEntity);
		//					break;
		//				}
		//			}
		//		}
		//
		// MTDLP事例(処理対象)
		//		updateCompositeEntity.setMtdlpCaseEntities(updateMtdlpCaseEntities);

		// MTDLP事例報告
		List<MtdlpCaseReportEntity> updateMtdlpCaseReportEntities = new ArrayList<MtdlpCaseReportEntity>();
		List<MtdlpCaseReportEntity> localActivityEntities3 = compositeEntity.getMtdlpCaseReportEntities();
		for (MtdlpCaseReportForm form : MtdlpSummaryUpdateForm.getMtdlpCaseReports()) {
			for (MtdlpCaseReportEntity entity : localActivityEntities3) {
				if (entity.getId().equals(form.getId())) {
					MtdlpCaseReportEntity updateEntity = new MtdlpCaseReportEntity();
					BeanUtil.copyProperties(entity, updateEntity, false); // 現在値
					if (isIncrementalUpdate) {
						BeanUtil.copyProperties(form, updateEntity, true); // 更新値(差分)
					} else {
						BeanUtil.copyProperties(form, updateEntity, false); // 更新値(上書)
					}
					updateMtdlpCaseReportEntities.add(updateEntity);
					break;
				}
			}
		}

		// MTDLP事例報告(処理対象)
		updateCompositeEntity.setMtdlpCaseReportEntities(updateMtdlpCaseReportEntities);

		// 実行
		mtdlpSummarySrv.update(updateDto);

		return ResponseEntity.ok("updateMtdlpSummary ok");
	}

	/**
	 * MTDLP履歴フォーム生成
	 * @param compositeEntity MTDLP履歴エンティティ(複合)
	 * @return MTDLP履歴フォーム
	 */
	private MtdlpSummaryForm createMtdlpSummaryForm(MtdlpCompositeEntity compositeEntity) {

		// MTDLP履歴フォーム
		MtdlpSummaryForm mtdlpSummaryForm = BeanUtil.createCopyProperties(compositeEntity.getMtdlpSummaryEntity(),
				MtdlpSummaryForm.class,
				false);

		// MTDLP認定履歴
		List<MtdlpQualifyHistoryEntity> mtdlpQualifyHistoryEntities = compositeEntity.getMtdlpQualifyHistoryEntities();
		if (!ListUtils.isEmpty(mtdlpQualifyHistoryEntities)) {
			mtdlpSummaryForm.setMtdlpQualifyHistories(mtdlpQualifyHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MtdlpQualifyHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// MTDLP研修受講履歴
		List<MtdlpTrainingHistoryEntity> mtdlpTrainingHistoryEntities = compositeEntity
				.getMtdlpTrainingHistoryEntities();
		if (!CollectionUtils.isEmpty(mtdlpTrainingHistoryEntities)) {
			mtdlpSummaryForm.setMtdlpTrainingHistories(mtdlpTrainingHistoryEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MtdlpTrainingHistoryForm.class, false))
					.collect(Collectors.toList()));
		}

		// MTDLP事例
		//		List<MtdlpCaseEntity> mtdlpCaseEntities = compositeEntity.getMtdlpCaseEntities();
		//		if (!CollectionUtils.isEmpty(mtdlpCaseEntities)) {
		//			mtdlpSummaryForm.setMtdlpCases(mtdlpCaseEntities.stream()
		//					.map(E -> BeanUtil.createCopyProperties(E, MtdlpCaseForm.class, false))
		//					.collect(Collectors.toList()));
		//		}

		// MTDLP事例報告
		List<MtdlpCaseReportEntity> mtdlpCaseReportEntities = compositeEntity.getMtdlpCaseReportEntities();
		if (!CollectionUtils.isEmpty(mtdlpCaseReportEntities)) {
			mtdlpSummaryForm.setMtdlpCaseReports(mtdlpCaseReportEntities.stream()
					.map(E -> BeanUtil.createCopyProperties(E, MtdlpCaseReportForm.class, false))
					.collect(Collectors.toList()));
		}

		return mtdlpSummaryForm;
	}

	//	/**
	//	 * 受講履歴フォーム生成
	//	 * @param compositeEntity 受講履歴エンティティ(複合)
	//	 * @return 受講履歴フォーム
	//	 */
	//	private AttendingHistoryCompositeEntity createAttendingHistoryForm(AttendingHistoryCompositeEntity compositeEntity) {
	//
	//		AttendingHistoryCompositeEntity attendingHistoryForm;
	//
	////		// 基礎研修履歴フォーム
	////		List<BasicOtAttendingSummaryForm> basicOtAttendingSummaryForm = (List<BasicOtAttendingSummaryForm>) BeanUtil.createCopyProperties(compositeEntity.getBasicOtAttendingSummaryEntity(), BasicOtAttendingSummaryForm.class,
	////				false);
	//
	//		// 基礎研修履歴フォーム
	//		BasicOtAttendingSummaryForm basicOtAttendingSummaryForm = BeanUtil.createCopyProperties(compositeEntity.getBasicOtAttendingSummaryEntity(), BasicOtAttendingSummaryForm.class,
	//				false);
	//
	//		// 基礎研修修了認定更新履歴
	//		List<BasicOtQualifyHistoryEntity> basicOtQualifyHistoryEntities = compositeEntity.getBasicOtQualifyHistoryEntities();
	//		if (!ListUtils.isEmpty(basicOtQualifyHistoryEntities)) {
	//			basicOtAttendingSummaryForm.setBasicOtQualifyHistories(basicOtQualifyHistoryEntities.stream()
	//					.map(E -> BeanUtil.createCopyProperties(E, BasicOtQualifyHistoryForm.class, false))
	//					.collect(Collectors.toList()));
	//		}
	//
	//		// 基礎研修受講履歴
	//		List<BasicOtTrainingHistoryEntity> basicOtTrainingHistoryEntities = compositeEntity.getBasicOtTrainingHistoryEntities();
	//		if (!CollectionUtils.isEmpty(basicOtTrainingHistoryEntities)) {
	//			basicOtAttendingSummaryForm.setBasicOtTrainingHistories(basicOtTrainingHistoryEntities.stream()
	//					.map(E -> BeanUtil.createCopyProperties(E, BasicOtTrainingHistoryForm.class, false))
	//					.collect(Collectors.toList()));
	//		}
	//
	//		attendingHistoryForm = compositeEntity;
	//
	//		return attendingHistoryForm;
	//
	//	}

	// TODO
}
