package jp.or.jaot.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.core.util.CipherUtil;
import jp.or.jaot.model.dto.UserDto;
import jp.or.jaot.model.dto.UserRevokedAuthorityDto;
import jp.or.jaot.model.dto.UserRoleDto;
import jp.or.jaot.model.dto.search.UserRevokedAuthoritySearchDto;
import jp.or.jaot.model.dto.search.UserRoleSearchDto;
import jp.or.jaot.model.dto.search.UserSearchDto;
import jp.or.jaot.model.entity.UserEntity;
import jp.or.jaot.model.form.UserForm;
import jp.or.jaot.model.form.UserIsAvailableForm;
import jp.or.jaot.model.form.UserListForm;
import jp.or.jaot.model.form.UserRevokedAuthorityForm;
import jp.or.jaot.model.form.UserRoleForm;
import jp.or.jaot.model.form.search.UserSearchListForm;
import jp.or.jaot.service.UserRevokedAuthorityService;
import jp.or.jaot.service.UserRoleService;
import jp.or.jaot.service.UserService;
import lombok.AllArgsConstructor;

/**
 * 事務局ユーザコントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "user", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class AdminUserController extends BaseController {

	/** 事務局ユーザサービス */
	private final UserService userSrv;

	/** 事務局ロールサービス */
	private final UserRoleService userRoleSrv;

	/** 事務局権限サービス */
	private final UserRevokedAuthorityService userRevokedAuthoritySrv;

	/**
	 * ログイン情報
	 * @return 実行結果
	 */
	@PostMapping(path = "login_user")
	public ResponseEntity<UserForm> loginUser(@AuthenticationPrincipal UserEntity userEntity) {
		return ResponseEntity.ok(BeanUtil.createCopyProperties(
				userSrv.getSearchResult(userEntity.getId()).getFirstEntity(), UserForm.class, false));
	}

	/**
	 * 利用可能状態取得
	 * @return 利用可能=true, 利用不可=false
	 */
	@PostMapping(path = "is_available")
	public ResponseEntity<Boolean> isAvailable(@AuthenticationPrincipal UserEntity userEntity,
			@RequestBody @Validated UserIsAvailableForm userIsAvailableForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		return ResponseEntity.ok(userSrv.isAvailablePathOrKey(userEntity.getId(), userIsAvailableForm.getPathOrKey()));
	}

	/**
	 * 条件検索(事務局ユーザ）
	 * @return 実行結果
	 */
	@PostMapping(path = "search_user")
	public ResponseEntity<List<UserForm>> searchUser(@RequestBody @Validated UserForm userForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		UserSearchDto searchDto = new UserSearchDto();
		BeanUtil.copyProperties(userForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<UserForm> forms = userSrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createUserForm(E)).collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 条件検索（事務局ユーザリスト）
	 * @return 実行結果
	 */
	@PostMapping(path = "search_list_user")
	public ResponseEntity<UserListForm> searchUser(@RequestBody @Validated UserSearchListForm usersearchListForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		UserSearchDto searchDto = new UserSearchDto();
		BeanUtil.copyProperties(usersearchListForm, searchDto, false); // 属性値

		// 検索結果フォーム取得
		UserListForm form = new UserListForm();
		form.setUsers(userSrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> createUserForm(E)).collect(Collectors.toList()));
		form.setResultCount(searchDto.getResultCount()); // 検索結果の件数
		form.setAllResultCount(searchDto.getAllResultCount()); // 検索結果の件数(全体)

		return ResponseEntity.ok(form);
	}

	/**
	 * 登録
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_user")
	public ResponseEntity<String> enterUser(@RequestBody @Validated UserForm userForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 追加対象DTO生成
		UserDto enterDto = UserDto.createSingle();
		UserEntity enterUserEntity = enterDto.getFirstEntity();
		BeanUtil.copyProperties(userForm, enterUserEntity, false);
		enterUserEntity.setPassword(CipherUtil.encrypt(userForm.getPassword())); // パスワード暗号化

		// 実行
		userSrv.enter(enterDto);

		return ResponseEntity.ok("enterUser ok");
	}

	/**
	 * 更新
	 * @return 実行結果
	 */
	@PostMapping(path = "update_user")
	public ResponseEntity<String> updateUser(@RequestBody @Validated UserForm userForm, Errors errors) {
		// バリデーション実行
		doValidation(errors);

		// 検索結果取得(キー)
		UserEntity userEntity = userSrv.getSearchResult(userForm.getId())
				.getFirstEntity();

		// 更新対象DTO生成
		UserDto updateDto = UserDto.createSingle();
		UserEntity updateUserEntity = updateDto.getFirstEntity();

		// 事務局ユーザの現在値をコピーする
		BeanUtil.copyProperties(userEntity, updateUserEntity, false);

		// 差分更新する。パスワード文字列が空文字であれば更新対象にしない
		if (StringUtils.isEmpty(userForm.getPassword())) {
			userForm.setPassword(null);
		}
		else {
			// ここでパスワード文字列を暗号化する。サービス層以降ではそのまま登録する想定
			userForm.setPassword(CipherUtil.encrypt(userForm.getPassword())); // パスワード暗号化
		}
		BeanUtil.copyProperties(userForm, updateUserEntity, true);

		// 実行
		userSrv.update(updateDto);

		return ResponseEntity.ok("updateUser ok");
	}

	/**
	 * 削除
	 * @return 実行結果
	 */
	@PostMapping(path = "disable_user")
	public ResponseEntity<String> disableUser(@RequestBody @Validated UserForm userForm, Errors errors) {
		// バリデーション実行
		doValidation(errors);

		// 実行
		userSrv.disable(userForm.getId());

		return ResponseEntity.ok("deleteUser ok");
	}

	/**
	 * 条件検索(事務局ロール)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_user_role")
	public ResponseEntity<List<UserRoleForm>> searchUserRole(@RequestBody @Validated UserRoleForm userRoleForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		UserRoleSearchDto searchDto = new UserRoleSearchDto();
		BeanUtil.copyProperties(userRoleForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<UserRoleForm> forms = userRoleSrv.getSearchResultByCondition(searchDto).getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, UserRoleForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(事務局ロール)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_user_role")
	public ResponseEntity<String> enterUserRole(@RequestBody @Validated UserRoleForm userRoleForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		userRoleSrv.enter(copyDto(userRoleForm, UserRoleDto.createSingle()));

		return ResponseEntity.ok("enterUserRole ok");
	}

	/**
	 * 条件検索(事務局権限)
	 * @return 実行結果
	 */
	@PostMapping(path = "search_user_revoked_authority")
	public ResponseEntity<List<UserRevokedAuthorityForm>> searchUserRevokedAuthority(
			@RequestBody @Validated UserRevokedAuthorityForm userRevokedAuthorityForm,
			Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		UserRevokedAuthoritySearchDto searchDto = new UserRevokedAuthoritySearchDto();
		BeanUtil.copyProperties(userRevokedAuthorityForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<UserRevokedAuthorityForm> forms = userRevokedAuthoritySrv.getSearchResultByCondition(searchDto)
				.getEntities()
				.stream().map(E -> BeanUtil.createCopyProperties(E, UserRevokedAuthorityForm.class, false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

	/**
	 * 登録(事務局権限)
	 * @return 実行結果
	 */
	@PostMapping(path = "enter_user_revoked_authority")
	public ResponseEntity<String> enterUserRevokedAuthority(
			@RequestBody @Validated UserRevokedAuthorityForm userRevokedAuthorityForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 実行
		userRevokedAuthoritySrv.enter(copyDto(userRevokedAuthorityForm, UserRevokedAuthorityDto.createSingle()));

		return ResponseEntity.ok("enterUserRevokedAuthority ok");
	}

	/**
	 * 事務局ユーザフォーム生成
	 * @param userEntity 事務局ユーザエンティティ
	 * @return 事務局ユーザフォーム
	 */
	private UserForm createUserForm(UserEntity userEntity) {
		// 事務局ユーザフォームに変換
		UserForm userForm = BeanUtil.createCopyProperties(userEntity, UserForm.class, false);

		return userForm;
	}
}
