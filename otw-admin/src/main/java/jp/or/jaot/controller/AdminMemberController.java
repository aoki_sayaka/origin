package jp.or.jaot.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.or.jaot.controller.common.BaseController;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.form.MemberForm;
import jp.or.jaot.service.MemberService;
import lombok.AllArgsConstructor;
/**
 * 会員コントローラ(WebAPI)
 */
@RestController
@RequestMapping(path = "member", consumes = MediaType.APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class AdminMemberController extends BaseController {

	/** 会員サービス */
	private final MemberService memberSrv;

	/**
	 * 条件検索（会員情報）
	 * @return 実行結果
	 */
	@PostMapping(path = "search_member")
	public ResponseEntity<List<MemberForm>> searchMember(@RequestBody @Validated MemberForm memberForm, Errors errors) {

		// バリデーション実行
		doValidation(errors);

		// 検索条件DTO生成
		MemberSearchDto searchDto = new MemberSearchDto();
		BeanUtil.copyProperties(memberForm, searchDto, false); // 属性値
		searchDto.setStartPosition(0); // 開始位置
		searchDto.setMaxResult(0); // 取得件数(全件)

		// 検索結果フォーム取得
		List<MemberForm> forms = memberSrv.getSearchResultByCondition(searchDto).getEntities().stream()
				.map(E -> BeanUtil.createCopyProperties(E,MemberForm.class,false))
				.collect(Collectors.toList());

		return ResponseEntity.ok(forms);
	}

}
