package jp.or.jaot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * Springアプリケーションルートクラス
 */
@SpringBootApplication
@ImportResource({ "classpath:applicationContext.xml" })
public class OtwAdminApplication {
	public static void main(String[] args) {
		SpringApplication.run(OtwAdminApplication.class, args);
	}
}
