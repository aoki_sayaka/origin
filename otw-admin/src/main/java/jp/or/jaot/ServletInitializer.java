package jp.or.jaot;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * サーブレットイニシャライザ<br>
 * (WarファイルをTomcatに展開する際に必須なクラス)
 */
public class ServletInitializer extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OtwAdminApplication.class);
	}
}
