package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the training_applications database table.
 * 
 */
@Entity
@Table(name="training_applications")
@NamedQuery(name="TrainingApplication.findAll", query="SELECT t FROM TrainingApplication t")
public class TrainingApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="advanced_win_consult_flag")
	private String advancedWinConsultFlag;

	@Column(name="applicant_category_cd")
	private String applicantCategoryCd;

	@Column(name="application_status_cd")
	private String applicationStatusCd;

	@Column(name="case_provide_cd")
	private String caseProvideCd;

	@Column(name="contact_type")
	private String contactType;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	@Temporal(TemporalType.DATE)
	@Column(name="declining_date")
	private Date decliningDate;

	private Boolean deleted;

	@Temporal(TemporalType.DATE)
	@Column(name="deposit_period_date")
	private Date depositPeriodDate;

	@Column(name="experience_year")
	private Integer experienceYear;

	private String fullname;

	@Column(name="fullname_kana")
	private String fullnameKana;

	@Column(name="job_category_cd")
	private String jobCategoryCd;

	@Column(name="member_no")
	private Integer memberNo;

	@Temporal(TemporalType.DATE)
	@Column(name="notification_date")
	private Date notificationDate;

	@Column(name="phone_number")
	private String phoneNumber;

	@Column(name="procedure_status_cd")
	private String procedureStatusCd;

	@Column(name="queue_no")
	private Integer queueNo;

	private String remarks;

	@Column(name="reminder_flag")
	private String reminderFlag;

	@Column(name="resubmission_count")
	private Integer resubmissionCount;

	@Temporal(TemporalType.DATE)
	@Column(name="resubmission_date")
	private Date resubmissionDate;

	@Column(name="target_disease")
	private String targetDisease;

	@Column(name="training_no")
	private String trainingNo;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	@Column(name="work_place_address1")
	private String workPlaceAddress1;

	@Column(name="work_place_address2")
	private String workPlaceAddress2;

	@Column(name="work_place_address3")
	private String workPlaceAddress3;

	@Column(name="work_place_nm")
	private String workPlaceNm;

	@Column(name="work_place_prefecture_cd")
	private String workPlacePrefectureCd;

	@Column(name="work_place_zipcode")
	private String workPlaceZipcode;

	public TrainingApplication() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdvancedWinConsultFlag() {
		return this.advancedWinConsultFlag;
	}

	public void setAdvancedWinConsultFlag(String advancedWinConsultFlag) {
		this.advancedWinConsultFlag = advancedWinConsultFlag;
	}

	public String getApplicantCategoryCd() {
		return this.applicantCategoryCd;
	}

	public void setApplicantCategoryCd(String applicantCategoryCd) {
		this.applicantCategoryCd = applicantCategoryCd;
	}

	public String getApplicationStatusCd() {
		return this.applicationStatusCd;
	}

	public void setApplicationStatusCd(String applicationStatusCd) {
		this.applicationStatusCd = applicationStatusCd;
	}

	public String getCaseProvideCd() {
		return this.caseProvideCd;
	}

	public void setCaseProvideCd(String caseProvideCd) {
		this.caseProvideCd = caseProvideCd;
	}

	public String getContactType() {
		return this.contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getDecliningDate() {
		return this.decliningDate;
	}

	public void setDecliningDate(Date decliningDate) {
		this.decliningDate = decliningDate;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Date getDepositPeriodDate() {
		return this.depositPeriodDate;
	}

	public void setDepositPeriodDate(Date depositPeriodDate) {
		this.depositPeriodDate = depositPeriodDate;
	}

	public Integer getExperienceYear() {
		return this.experienceYear;
	}

	public void setExperienceYear(Integer experienceYear) {
		this.experienceYear = experienceYear;
	}

	public String getFullname() {
		return this.fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getFullnameKana() {
		return this.fullnameKana;
	}

	public void setFullnameKana(String fullnameKana) {
		this.fullnameKana = fullnameKana;
	}

	public String getJobCategoryCd() {
		return this.jobCategoryCd;
	}

	public void setJobCategoryCd(String jobCategoryCd) {
		this.jobCategoryCd = jobCategoryCd;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Date getNotificationDate() {
		return this.notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getProcedureStatusCd() {
		return this.procedureStatusCd;
	}

	public void setProcedureStatusCd(String procedureStatusCd) {
		this.procedureStatusCd = procedureStatusCd;
	}

	public Integer getQueueNo() {
		return this.queueNo;
	}

	public void setQueueNo(Integer queueNo) {
		this.queueNo = queueNo;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReminderFlag() {
		return this.reminderFlag;
	}

	public void setReminderFlag(String reminderFlag) {
		this.reminderFlag = reminderFlag;
	}

	public Integer getResubmissionCount() {
		return this.resubmissionCount;
	}

	public void setResubmissionCount(Integer resubmissionCount) {
		this.resubmissionCount = resubmissionCount;
	}

	public Date getResubmissionDate() {
		return this.resubmissionDate;
	}

	public void setResubmissionDate(Date resubmissionDate) {
		this.resubmissionDate = resubmissionDate;
	}

	public String getTargetDisease() {
		return this.targetDisease;
	}

	public void setTargetDisease(String targetDisease) {
		this.targetDisease = targetDisease;
	}

	public String getTrainingNo() {
		return this.trainingNo;
	}

	public void setTrainingNo(String trainingNo) {
		this.trainingNo = trainingNo;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public String getWorkPlaceAddress1() {
		return this.workPlaceAddress1;
	}

	public void setWorkPlaceAddress1(String workPlaceAddress1) {
		this.workPlaceAddress1 = workPlaceAddress1;
	}

	public String getWorkPlaceAddress2() {
		return this.workPlaceAddress2;
	}

	public void setWorkPlaceAddress2(String workPlaceAddress2) {
		this.workPlaceAddress2 = workPlaceAddress2;
	}

	public String getWorkPlaceAddress3() {
		return this.workPlaceAddress3;
	}

	public void setWorkPlaceAddress3(String workPlaceAddress3) {
		this.workPlaceAddress3 = workPlaceAddress3;
	}

	public String getWorkPlaceNm() {
		return this.workPlaceNm;
	}

	public void setWorkPlaceNm(String workPlaceNm) {
		this.workPlaceNm = workPlaceNm;
	}

	public String getWorkPlacePrefectureCd() {
		return this.workPlacePrefectureCd;
	}

	public void setWorkPlacePrefectureCd(String workPlacePrefectureCd) {
		this.workPlacePrefectureCd = workPlacePrefectureCd;
	}

	public String getWorkPlaceZipcode() {
		return this.workPlaceZipcode;
	}

	public void setWorkPlaceZipcode(String workPlaceZipcode) {
		this.workPlaceZipcode = workPlaceZipcode;
	}

}