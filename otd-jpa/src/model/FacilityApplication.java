package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the facility_applications database table.
 * 
 */
@Entity
@Table(name="facility_applications")
@NamedQuery(name="FacilityApplication.findAll", query="SELECT f FROM FacilityApplication f")
public class FacilityApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String address1;

	private String address2;

	private String address3;

	@Column(name="corporate_nm")
	private String corporateNm;

	@Column(name="corporate_nm_kana")
	private String corporateNmKana;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	private String department;

	@Column(name="facility_category")
	private String facilityCategory;

	@Column(name="facility_nm")
	private String facilityNm;

	@Column(name="facility_nm_kana")
	private String facilityNmKana;

	@Column(name="facility_nm_kana2")
	private String facilityNmKana2;

	@Column(name="facility_nm2")
	private String facilityNm2;

	@Column(name="facility_prefecture_cd")
	private String facilityPrefectureCd;

	@Column(name="fax_number")
	private String faxNumber;

	@Column(name="member_category_cd")
	private String memberCategoryCd;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="opener_nm")
	private String openerNm;

	@Column(name="opener_nm_kana")
	private String openerNmKana;

	@Column(name="opener_type_cd")
	private String openerTypeCd;

	@Column(name="primary_phone_number")
	private String primaryPhoneNumber;

	@Column(name="responsible_email_address")
	private String responsibleEmailAddress;

	private Integer status;

	@Column(name="temporary_member_no")
	private Integer temporaryMemberNo;

	@Column(name="training_school_category_cd")
	private String trainingSchoolCategoryCd;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	private String zipcode;

	public FacilityApplication() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCorporateNm() {
		return this.corporateNm;
	}

	public void setCorporateNm(String corporateNm) {
		this.corporateNm = corporateNm;
	}

	public String getCorporateNmKana() {
		return this.corporateNmKana;
	}

	public void setCorporateNmKana(String corporateNmKana) {
		this.corporateNmKana = corporateNmKana;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getFacilityCategory() {
		return this.facilityCategory;
	}

	public void setFacilityCategory(String facilityCategory) {
		this.facilityCategory = facilityCategory;
	}

	public String getFacilityNm() {
		return this.facilityNm;
	}

	public void setFacilityNm(String facilityNm) {
		this.facilityNm = facilityNm;
	}

	public String getFacilityNmKana() {
		return this.facilityNmKana;
	}

	public void setFacilityNmKana(String facilityNmKana) {
		this.facilityNmKana = facilityNmKana;
	}

	public String getFacilityNmKana2() {
		return this.facilityNmKana2;
	}

	public void setFacilityNmKana2(String facilityNmKana2) {
		this.facilityNmKana2 = facilityNmKana2;
	}

	public String getFacilityNm2() {
		return this.facilityNm2;
	}

	public void setFacilityNm2(String facilityNm2) {
		this.facilityNm2 = facilityNm2;
	}

	public String getFacilityPrefectureCd() {
		return this.facilityPrefectureCd;
	}

	public void setFacilityPrefectureCd(String facilityPrefectureCd) {
		this.facilityPrefectureCd = facilityPrefectureCd;
	}

	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getMemberCategoryCd() {
		return this.memberCategoryCd;
	}

	public void setMemberCategoryCd(String memberCategoryCd) {
		this.memberCategoryCd = memberCategoryCd;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public String getOpenerNm() {
		return this.openerNm;
	}

	public void setOpenerNm(String openerNm) {
		this.openerNm = openerNm;
	}

	public String getOpenerNmKana() {
		return this.openerNmKana;
	}

	public void setOpenerNmKana(String openerNmKana) {
		this.openerNmKana = openerNmKana;
	}

	public String getOpenerTypeCd() {
		return this.openerTypeCd;
	}

	public void setOpenerTypeCd(String openerTypeCd) {
		this.openerTypeCd = openerTypeCd;
	}

	public String getPrimaryPhoneNumber() {
		return this.primaryPhoneNumber;
	}

	public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
		this.primaryPhoneNumber = primaryPhoneNumber;
	}

	public String getResponsibleEmailAddress() {
		return this.responsibleEmailAddress;
	}

	public void setResponsibleEmailAddress(String responsibleEmailAddress) {
		this.responsibleEmailAddress = responsibleEmailAddress;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getTemporaryMemberNo() {
		return this.temporaryMemberNo;
	}

	public void setTemporaryMemberNo(Integer temporaryMemberNo) {
		this.temporaryMemberNo = temporaryMemberNo;
	}

	public String getTrainingSchoolCategoryCd() {
		return this.trainingSchoolCategoryCd;
	}

	public void setTrainingSchoolCategoryCd(String trainingSchoolCategoryCd) {
		this.trainingSchoolCategoryCd = trainingSchoolCategoryCd;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}