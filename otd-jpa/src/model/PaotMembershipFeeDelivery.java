package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the paot_membership_fee_deliveries database table.
 * 
 */
@Entity
@Table(name="paot_membership_fee_deliveries")
@NamedQuery(name="PaotMembershipFeeDelivery.findAll", query="SELECT p FROM PaotMembershipFeeDelivery p")
public class PaotMembershipFeeDelivery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="billing_amount")
	private Integer billingAmount;

	@Column(name="billing_balance")
	private Integer billingBalance;

	@Column(name="category_cd")
	private String categoryCd;

	@Column(name="fisical_year")
	private Integer fisicalYear;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="paot_cd")
	private Integer paotCd;

	@Column(name="payment_amount")
	private Integer paymentAmount;

	@Temporal(TemporalType.DATE)
	@Column(name="payment_date")
	private Date paymentDate;

	private String remarks;

	public PaotMembershipFeeDelivery() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBillingAmount() {
		return this.billingAmount;
	}

	public void setBillingAmount(Integer billingAmount) {
		this.billingAmount = billingAmount;
	}

	public Integer getBillingBalance() {
		return this.billingBalance;
	}

	public void setBillingBalance(Integer billingBalance) {
		this.billingBalance = billingBalance;
	}

	public String getCategoryCd() {
		return this.categoryCd;
	}

	public void setCategoryCd(String categoryCd) {
		this.categoryCd = categoryCd;
	}

	public Integer getFisicalYear() {
		return this.fisicalYear;
	}

	public void setFisicalYear(Integer fisicalYear) {
		this.fisicalYear = fisicalYear;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Integer getPaotCd() {
		return this.paotCd;
	}

	public void setPaotCd(Integer paotCd) {
		this.paotCd = paotCd;
	}

	public Integer getPaymentAmount() {
		return this.paymentAmount;
	}

	public void setPaymentAmount(Integer paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}