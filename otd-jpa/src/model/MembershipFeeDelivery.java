package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the membership_fee_deliveries database table.
 * 
 */
@Entity
@Table(name="membership_fee_deliveries")
@NamedQuery(name="MembershipFeeDelivery.findAll", query="SELECT m FROM MembershipFeeDelivery m")
public class MembershipFeeDelivery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="application_content")
	private String applicationContent;

	@Temporal(TemporalType.DATE)
	@Column(name="application_date")
	private Date applicationDate;

	@Column(name="application_status")
	private String applicationStatus;

	@Column(name="billing_amount")
	private Integer billingAmount;

	@Column(name="billing_balance")
	private Integer billingBalance;

	@Column(name="category_cd")
	private String categoryCd;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	@Column(name="data_type")
	private String dataType;

	private Boolean deleted;

	@Column(name="fiscal_year")
	private Integer fiscalYear;

	@Temporal(TemporalType.DATE)
	@Column(name="import_date")
	private Date importDate;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="payment_amount")
	private Integer paymentAmount;

	@Column(name="payment_type")
	private String paymentType;

	private String remarks;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public MembershipFeeDelivery() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApplicationContent() {
		return this.applicationContent;
	}

	public void setApplicationContent(String applicationContent) {
		this.applicationContent = applicationContent;
	}

	public Date getApplicationDate() {
		return this.applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApplicationStatus() {
		return this.applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public Integer getBillingAmount() {
		return this.billingAmount;
	}

	public void setBillingAmount(Integer billingAmount) {
		this.billingAmount = billingAmount;
	}

	public Integer getBillingBalance() {
		return this.billingBalance;
	}

	public void setBillingBalance(Integer billingBalance) {
		this.billingBalance = billingBalance;
	}

	public String getCategoryCd() {
		return this.categoryCd;
	}

	public void setCategoryCd(String categoryCd) {
		this.categoryCd = categoryCd;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getFiscalYear() {
		return this.fiscalYear;
	}

	public void setFiscalYear(Integer fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public Date getImportDate() {
		return this.importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Integer getPaymentAmount() {
		return this.paymentAmount;
	}

	public void setPaymentAmount(Integer paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}