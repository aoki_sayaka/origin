package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the training_school_name_histories database table.
 * 
 */
@Entity
@Table(name="training_school_name_histories")
@NamedQuery(name="TrainingSchoolNameHistory.findAll", query="SELECT t FROM TrainingSchoolNameHistory t")
public class TrainingSchoolNameHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="facility_base_no")
	private String facilityBaseNo;

	@Column(name="facility_category_cd")
	private String facilityCategoryCd;

	@Column(name="facility_id")
	private Long facilityId;

	@Column(name="facility_no")
	private String facilityNo;

	@Column(name="facility_seq_no")
	private String facilitySeqNo;

	@Column(name="fiscal_year")
	private Integer fiscalYear;

	private String memo;

	@Temporal(TemporalType.DATE)
	@Column(name="name_create_date")
	private Date nameCreateDate;

	@Temporal(TemporalType.DATE)
	@Column(name="name_delete_date")
	private Date nameDeleteDate;

	@Column(name="name_update_date")
	private Timestamp nameUpdateDate;

	@Column(name="name_update_no")
	private Integer nameUpdateNo;

	@Column(name="name_update_user_id")
	private String nameUpdateUserId;

	@Column(name="origin_school_no")
	private Integer originSchoolNo;

	@Column(name="pref_code")
	private String prefCode;

	@Column(name="seq_no")
	private Integer seqNo;

	@Column(name="training_school_category_cd")
	private String trainingSchoolCategoryCd;

	@Column(name="training_school_nm")
	private String trainingSchoolNm;

	@Column(name="training_school_nm_kana")
	private String trainingSchoolNmKana;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public TrainingSchoolNameHistory() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getFacilityBaseNo() {
		return this.facilityBaseNo;
	}

	public void setFacilityBaseNo(String facilityBaseNo) {
		this.facilityBaseNo = facilityBaseNo;
	}

	public String getFacilityCategoryCd() {
		return this.facilityCategoryCd;
	}

	public void setFacilityCategoryCd(String facilityCategoryCd) {
		this.facilityCategoryCd = facilityCategoryCd;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public String getFacilityNo() {
		return this.facilityNo;
	}

	public void setFacilityNo(String facilityNo) {
		this.facilityNo = facilityNo;
	}

	public String getFacilitySeqNo() {
		return this.facilitySeqNo;
	}

	public void setFacilitySeqNo(String facilitySeqNo) {
		this.facilitySeqNo = facilitySeqNo;
	}

	public Integer getFiscalYear() {
		return this.fiscalYear;
	}

	public void setFiscalYear(Integer fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Date getNameCreateDate() {
		return this.nameCreateDate;
	}

	public void setNameCreateDate(Date nameCreateDate) {
		this.nameCreateDate = nameCreateDate;
	}

	public Date getNameDeleteDate() {
		return this.nameDeleteDate;
	}

	public void setNameDeleteDate(Date nameDeleteDate) {
		this.nameDeleteDate = nameDeleteDate;
	}

	public Timestamp getNameUpdateDate() {
		return this.nameUpdateDate;
	}

	public void setNameUpdateDate(Timestamp nameUpdateDate) {
		this.nameUpdateDate = nameUpdateDate;
	}

	public Integer getNameUpdateNo() {
		return this.nameUpdateNo;
	}

	public void setNameUpdateNo(Integer nameUpdateNo) {
		this.nameUpdateNo = nameUpdateNo;
	}

	public String getNameUpdateUserId() {
		return this.nameUpdateUserId;
	}

	public void setNameUpdateUserId(String nameUpdateUserId) {
		this.nameUpdateUserId = nameUpdateUserId;
	}

	public Integer getOriginSchoolNo() {
		return this.originSchoolNo;
	}

	public void setOriginSchoolNo(Integer originSchoolNo) {
		this.originSchoolNo = originSchoolNo;
	}

	public String getPrefCode() {
		return this.prefCode;
	}

	public void setPrefCode(String prefCode) {
		this.prefCode = prefCode;
	}

	public Integer getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public String getTrainingSchoolCategoryCd() {
		return this.trainingSchoolCategoryCd;
	}

	public void setTrainingSchoolCategoryCd(String trainingSchoolCategoryCd) {
		this.trainingSchoolCategoryCd = trainingSchoolCategoryCd;
	}

	public String getTrainingSchoolNm() {
		return this.trainingSchoolNm;
	}

	public void setTrainingSchoolNm(String trainingSchoolNm) {
		this.trainingSchoolNm = trainingSchoolNm;
	}

	public String getTrainingSchoolNmKana() {
		return this.trainingSchoolNmKana;
	}

	public void setTrainingSchoolNmKana(String trainingSchoolNmKana) {
		this.trainingSchoolNmKana = trainingSchoolNmKana;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}