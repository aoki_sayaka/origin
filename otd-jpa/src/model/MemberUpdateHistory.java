package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the member_update_histories database table.
 * 
 */
@Entity
@Table(name="member_update_histories")
@NamedQuery(name="MemberUpdateHistory.findAll", query="SELECT m FROM MemberUpdateHistory m")
public class MemberUpdateHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="content_after")
	private String contentAfter;

	@Column(name="content_before")
	private String contentBefore;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="field_cd")
	private Integer fieldCd;

	@Column(name="field_nm")
	private String fieldNm;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="member_update_datetime")
	private Timestamp memberUpdateDatetime;

	@Column(name="paot_cd")
	private Integer paotCd;

	@Column(name="table_cd")
	private Integer tableCd;

	@Column(name="table_nm")
	private String tableNm;

	@Column(name="update_category_cd")
	private Integer updateCategoryCd;

	@Column(name="update_date")
	private Timestamp updateDate;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_no")
	private Integer updateNo;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="update_user_id")
	private String updateUserId;

	@Column(name="version_no")
	private Integer versionNo;

	public MemberUpdateHistory() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContentAfter() {
		return this.contentAfter;
	}

	public void setContentAfter(String contentAfter) {
		this.contentAfter = contentAfter;
	}

	public String getContentBefore() {
		return this.contentBefore;
	}

	public void setContentBefore(String contentBefore) {
		this.contentBefore = contentBefore;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getFieldCd() {
		return this.fieldCd;
	}

	public void setFieldCd(Integer fieldCd) {
		this.fieldCd = fieldCd;
	}

	public String getFieldNm() {
		return this.fieldNm;
	}

	public void setFieldNm(String fieldNm) {
		this.fieldNm = fieldNm;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Timestamp getMemberUpdateDatetime() {
		return this.memberUpdateDatetime;
	}

	public void setMemberUpdateDatetime(Timestamp memberUpdateDatetime) {
		this.memberUpdateDatetime = memberUpdateDatetime;
	}

	public Integer getPaotCd() {
		return this.paotCd;
	}

	public void setPaotCd(Integer paotCd) {
		this.paotCd = paotCd;
	}

	public Integer getTableCd() {
		return this.tableCd;
	}

	public void setTableCd(Integer tableCd) {
		this.tableCd = tableCd;
	}

	public String getTableNm() {
		return this.tableNm;
	}

	public void setTableNm(String tableNm) {
		this.tableNm = tableNm;
	}

	public Integer getUpdateCategoryCd() {
		return this.updateCategoryCd;
	}

	public void setUpdateCategoryCd(Integer updateCategoryCd) {
		this.updateCategoryCd = updateCategoryCd;
	}

	public Timestamp getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(Timestamp updateDate) {
		this.updateDate = updateDate;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Integer getUpdateNo() {
		return this.updateNo;
	}

	public void setUpdateNo(Integer updateNo) {
		this.updateNo = updateNo;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateUserId() {
		return this.updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}