package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the training_attendance_results database table.
 * 
 */
@Entity
@Table(name="training_attendance_results")
@NamedQuery(name="TrainingAttendanceResult.findAll", query="SELECT t FROM TrainingAttendanceResult t")
public class TrainingAttendanceResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="attendance_status1_cd")
	private String attendanceStatus1Cd;

	@Column(name="attendance_status2_cd")
	private String attendanceStatus2Cd;

	@Column(name="attendance_status3_cd")
	private String attendanceStatus3Cd;

	@Temporal(TemporalType.DATE)
	@Column(name="completion_date")
	private Date completionDate;

	@Column(name="completion_status_cd")
	private String completionStatusCd;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="entry_fee")
	private Integer entryFee;

	private String fullname;

	@Column(name="leave_status1_cd")
	private String leaveStatus1Cd;

	@Column(name="leave_status2_cd")
	private String leaveStatus2Cd;

	@Column(name="leave_status3_cd")
	private String leaveStatus3Cd;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="pay_on_the_day_flag")
	private Boolean payOnTheDayFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="retest_date")
	private Date retestDate;

	@Temporal(TemporalType.DATE)
	@Column(name="retest_pass_date")
	private Date retestPassDate;

	@Column(name="retest_result_cd")
	private String retestResultCd;

	@Temporal(TemporalType.DATE)
	@Column(name="test_pass_date")
	private Date testPassDate;

	@Column(name="test_result_cd")
	private String testResultCd;

	@Column(name="training_no")
	private String trainingNo;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public TrainingAttendanceResult() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttendanceStatus1Cd() {
		return this.attendanceStatus1Cd;
	}

	public void setAttendanceStatus1Cd(String attendanceStatus1Cd) {
		this.attendanceStatus1Cd = attendanceStatus1Cd;
	}

	public String getAttendanceStatus2Cd() {
		return this.attendanceStatus2Cd;
	}

	public void setAttendanceStatus2Cd(String attendanceStatus2Cd) {
		this.attendanceStatus2Cd = attendanceStatus2Cd;
	}

	public String getAttendanceStatus3Cd() {
		return this.attendanceStatus3Cd;
	}

	public void setAttendanceStatus3Cd(String attendanceStatus3Cd) {
		this.attendanceStatus3Cd = attendanceStatus3Cd;
	}

	public Date getCompletionDate() {
		return this.completionDate;
	}

	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}

	public String getCompletionStatusCd() {
		return this.completionStatusCd;
	}

	public void setCompletionStatusCd(String completionStatusCd) {
		this.completionStatusCd = completionStatusCd;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getEntryFee() {
		return this.entryFee;
	}

	public void setEntryFee(Integer entryFee) {
		this.entryFee = entryFee;
	}

	public String getFullname() {
		return this.fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getLeaveStatus1Cd() {
		return this.leaveStatus1Cd;
	}

	public void setLeaveStatus1Cd(String leaveStatus1Cd) {
		this.leaveStatus1Cd = leaveStatus1Cd;
	}

	public String getLeaveStatus2Cd() {
		return this.leaveStatus2Cd;
	}

	public void setLeaveStatus2Cd(String leaveStatus2Cd) {
		this.leaveStatus2Cd = leaveStatus2Cd;
	}

	public String getLeaveStatus3Cd() {
		return this.leaveStatus3Cd;
	}

	public void setLeaveStatus3Cd(String leaveStatus3Cd) {
		this.leaveStatus3Cd = leaveStatus3Cd;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Boolean getPayOnTheDayFlag() {
		return this.payOnTheDayFlag;
	}

	public void setPayOnTheDayFlag(Boolean payOnTheDayFlag) {
		this.payOnTheDayFlag = payOnTheDayFlag;
	}

	public Date getRetestDate() {
		return this.retestDate;
	}

	public void setRetestDate(Date retestDate) {
		this.retestDate = retestDate;
	}

	public Date getRetestPassDate() {
		return this.retestPassDate;
	}

	public void setRetestPassDate(Date retestPassDate) {
		this.retestPassDate = retestPassDate;
	}

	public String getRetestResultCd() {
		return this.retestResultCd;
	}

	public void setRetestResultCd(String retestResultCd) {
		this.retestResultCd = retestResultCd;
	}

	public Date getTestPassDate() {
		return this.testPassDate;
	}

	public void setTestPassDate(Date testPassDate) {
		this.testPassDate = testPassDate;
	}

	public String getTestResultCd() {
		return this.testResultCd;
	}

	public void setTestResultCd(String testResultCd) {
		this.testResultCd = testResultCd;
	}

	public String getTrainingNo() {
		return this.trainingNo;
	}

	public void setTrainingNo(String trainingNo) {
		this.trainingNo = trainingNo;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}