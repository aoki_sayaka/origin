package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the recess_histories database table.
 * 
 */
@Entity
@Table(name="recess_histories")
@NamedQuery(name="RecessHistory.findAll", query="SELECT r FROM RecessHistory r")
public class RecessHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="certificate_received")
	private Boolean certificateReceived;

	@Temporal(TemporalType.DATE)
	@Column(name="comeback_date")
	private Date comebackDate;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Temporal(TemporalType.DATE)
	@Column(name="end_recess_date")
	private Date endRecessDate;

	@Column(name="member_no")
	private Integer memberNo;

	@Temporal(TemporalType.DATE)
	@Column(name="print_date")
	private Date printDate;

	@Column(name="process_status")
	private String processStatus;

	@Column(name="recess_reason")
	private String recessReason;

	@Column(name="reject_reason")
	private String rejectReason;

	private String remarks;

	@Temporal(TemporalType.DATE)
	@Column(name="start_recess_date")
	private Date startRecessDate;

	private Integer times;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	@Temporal(TemporalType.DATE)
	@Column(name="withdrawal_date")
	private Date withdrawalDate;

	@Column(name="withdrawal_process_status")
	private String withdrawalProcessStatus;

	@Column(name="withdrawal_reason")
	private String withdrawalReason;

	@Column(name="work_place_data_deletion")
	private Boolean workPlaceDataDeletion;

	public RecessHistory() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getCertificateReceived() {
		return this.certificateReceived;
	}

	public void setCertificateReceived(Boolean certificateReceived) {
		this.certificateReceived = certificateReceived;
	}

	public Date getComebackDate() {
		return this.comebackDate;
	}

	public void setComebackDate(Date comebackDate) {
		this.comebackDate = comebackDate;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Date getEndRecessDate() {
		return this.endRecessDate;
	}

	public void setEndRecessDate(Date endRecessDate) {
		this.endRecessDate = endRecessDate;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Date getPrintDate() {
		return this.printDate;
	}

	public void setPrintDate(Date printDate) {
		this.printDate = printDate;
	}

	public String getProcessStatus() {
		return this.processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public String getRecessReason() {
		return this.recessReason;
	}

	public void setRecessReason(String recessReason) {
		this.recessReason = recessReason;
	}

	public String getRejectReason() {
		return this.rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getStartRecessDate() {
		return this.startRecessDate;
	}

	public void setStartRecessDate(Date startRecessDate) {
		this.startRecessDate = startRecessDate;
	}

	public Integer getTimes() {
		return this.times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public Date getWithdrawalDate() {
		return this.withdrawalDate;
	}

	public void setWithdrawalDate(Date withdrawalDate) {
		this.withdrawalDate = withdrawalDate;
	}

	public String getWithdrawalProcessStatus() {
		return this.withdrawalProcessStatus;
	}

	public void setWithdrawalProcessStatus(String withdrawalProcessStatus) {
		this.withdrawalProcessStatus = withdrawalProcessStatus;
	}

	public String getWithdrawalReason() {
		return this.withdrawalReason;
	}

	public void setWithdrawalReason(String withdrawalReason) {
		this.withdrawalReason = withdrawalReason;
	}

	public Boolean getWorkPlaceDataDeletion() {
		return this.workPlaceDataDeletion;
	}

	public void setWorkPlaceDataDeletion(Boolean workPlaceDataDeletion) {
		this.workPlaceDataDeletion = workPlaceDataDeletion;
	}

}