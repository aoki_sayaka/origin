package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the questionnaire_destinations database table.
 * 
 */
@Entity
@Table(name="questionnaire_destinations")
@NamedQuery(name="QuestionnaireDestination.findAll", query="SELECT q FROM QuestionnaireDestination q")
public class QuestionnaireDestination implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="questionnaire_id")
	private Long questionnaireId;

	@Column(name="read_flg")
	private Boolean readFlg;

	@Column(name="read_time")
	private Timestamp readTime;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public QuestionnaireDestination() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Long getQuestionnaireId() {
		return this.questionnaireId;
	}

	public void setQuestionnaireId(Long questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public Boolean getReadFlg() {
		return this.readFlg;
	}

	public void setReadFlg(Boolean readFlg) {
		this.readFlg = readFlg;
	}

	public Timestamp getReadTime() {
		return this.readTime;
	}

	public void setReadTime(Timestamp readTime) {
		this.readTime = readTime;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}