package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the training_school_open_histories database table.
 * 
 */
@Entity
@Table(name="training_school_open_histories")
@NamedQuery(name="TrainingSchoolOpenHistory.findAll", query="SELECT t FROM TrainingSchoolOpenHistory t")
public class TrainingSchoolOpenHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="facility_base_no")
	private String facilityBaseNo;

	@Column(name="facility_id")
	private Long facilityId;

	@Column(name="last_fiscal_year")
	private Integer lastFiscalYear;

	@Column(name="last_recuriting_fiscal_year")
	private Integer lastRecuritingFiscalYear;

	@Column(name="open_fiscal_year")
	private Integer openFiscalYear;

	@Column(name="open_update_date")
	private Timestamp openUpdateDate;

	@Column(name="open_update_user_id")
	private String openUpdateUserId;

	@Column(name="seq_no")
	private Integer seqNo;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public TrainingSchoolOpenHistory() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getFacilityBaseNo() {
		return this.facilityBaseNo;
	}

	public void setFacilityBaseNo(String facilityBaseNo) {
		this.facilityBaseNo = facilityBaseNo;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public Integer getLastFiscalYear() {
		return this.lastFiscalYear;
	}

	public void setLastFiscalYear(Integer lastFiscalYear) {
		this.lastFiscalYear = lastFiscalYear;
	}

	public Integer getLastRecuritingFiscalYear() {
		return this.lastRecuritingFiscalYear;
	}

	public void setLastRecuritingFiscalYear(Integer lastRecuritingFiscalYear) {
		this.lastRecuritingFiscalYear = lastRecuritingFiscalYear;
	}

	public Integer getOpenFiscalYear() {
		return this.openFiscalYear;
	}

	public void setOpenFiscalYear(Integer openFiscalYear) {
		this.openFiscalYear = openFiscalYear;
	}

	public Timestamp getOpenUpdateDate() {
		return this.openUpdateDate;
	}

	public void setOpenUpdateDate(Timestamp openUpdateDate) {
		this.openUpdateDate = openUpdateDate;
	}

	public String getOpenUpdateUserId() {
		return this.openUpdateUserId;
	}

	public void setOpenUpdateUserId(String openUpdateUserId) {
		this.openUpdateUserId = openUpdateUserId;
	}

	public Integer getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}