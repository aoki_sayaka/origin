package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the application_progresses database table.
 * 
 */
@Entity
@Table(name="application_progresses")
@NamedQuery(name="ApplicationProgress.findAll", query="SELECT a FROM ApplicationProgress a")
public class ApplicationProgress implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="application_category_cd")
	private String applicationCategoryCd;

	@Column(name="application_kind_cd")
	private String applicationKindCd;

	@Column(name="application_no")
	private Long applicationNo;

	@Column(name="application_type_cd")
	private String applicationTypeCd;

	@Temporal(TemporalType.DATE)
	@Column(name="apply_date")
	private Date applyDate;

	@Temporal(TemporalType.DATE)
	@Column(name="approval_date")
	private Date approvalDate;

	@Column(name="branch_no")
	private Integer branchNo;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="progress_cd")
	private String progressCd;

	@Temporal(TemporalType.DATE)
	@Column(name="receipt_date")
	private Date receiptDate;

	@Column(name="register_category_cd")
	private String registerCategoryCd;

	@Column(name="send_category_cd")
	private String sendCategoryCd;

	@Column(name="send_count")
	private Integer sendCount;

	@Temporal(TemporalType.DATE)
	@Column(name="send_date")
	private Date sendDate;

	@Column(name="seq_no")
	private Integer seqNo;

	@Column(name="status_cd")
	private String statusCd;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public ApplicationProgress() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApplicationCategoryCd() {
		return this.applicationCategoryCd;
	}

	public void setApplicationCategoryCd(String applicationCategoryCd) {
		this.applicationCategoryCd = applicationCategoryCd;
	}

	public String getApplicationKindCd() {
		return this.applicationKindCd;
	}

	public void setApplicationKindCd(String applicationKindCd) {
		this.applicationKindCd = applicationKindCd;
	}

	public Long getApplicationNo() {
		return this.applicationNo;
	}

	public void setApplicationNo(Long applicationNo) {
		this.applicationNo = applicationNo;
	}

	public String getApplicationTypeCd() {
		return this.applicationTypeCd;
	}

	public void setApplicationTypeCd(String applicationTypeCd) {
		this.applicationTypeCd = applicationTypeCd;
	}

	public Date getApplyDate() {
		return this.applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public Date getApprovalDate() {
		return this.approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public Integer getBranchNo() {
		return this.branchNo;
	}

	public void setBranchNo(Integer branchNo) {
		this.branchNo = branchNo;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public String getProgressCd() {
		return this.progressCd;
	}

	public void setProgressCd(String progressCd) {
		this.progressCd = progressCd;
	}

	public Date getReceiptDate() {
		return this.receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getRegisterCategoryCd() {
		return this.registerCategoryCd;
	}

	public void setRegisterCategoryCd(String registerCategoryCd) {
		this.registerCategoryCd = registerCategoryCd;
	}

	public String getSendCategoryCd() {
		return this.sendCategoryCd;
	}

	public void setSendCategoryCd(String sendCategoryCd) {
		this.sendCategoryCd = sendCategoryCd;
	}

	public Integer getSendCount() {
		return this.sendCount;
	}

	public void setSendCount(Integer sendCount) {
		this.sendCount = sendCount;
	}

	public Date getSendDate() {
		return this.sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Integer getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public String getStatusCd() {
		return this.statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}