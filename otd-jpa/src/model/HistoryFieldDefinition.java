package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the history_field_definitions database table.
 * 
 */
@Entity
@Table(name="history_field_definitions")
@NamedQuery(name="HistoryFieldDefinition.findAll", query="SELECT h FROM HistoryFieldDefinition h")
public class HistoryFieldDefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="field_cd")
	private Integer fieldCd;

	@Column(name="field_nm")
	private String fieldNm;

	@Column(name="field_physical_nm")
	private String fieldPhysicalNm;

	@Column(name="table_definition_id")
	private Long tableDefinitionId;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public HistoryFieldDefinition() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getFieldCd() {
		return this.fieldCd;
	}

	public void setFieldCd(Integer fieldCd) {
		this.fieldCd = fieldCd;
	}

	public String getFieldNm() {
		return this.fieldNm;
	}

	public void setFieldNm(String fieldNm) {
		this.fieldNm = fieldNm;
	}

	public String getFieldPhysicalNm() {
		return this.fieldPhysicalNm;
	}

	public void setFieldPhysicalNm(String fieldPhysicalNm) {
		this.fieldPhysicalNm = fieldPhysicalNm;
	}

	public Long getTableDefinitionId() {
		return this.tableDefinitionId;
	}

	public void setTableDefinitionId(Long tableDefinitionId) {
		this.tableDefinitionId = tableDefinitionId;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}