package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the zipcodes database table.
 * 
 */
@Entity
@Table(name="zipcodes")
@NamedQuery(name="Zipcode.findAll", query="SELECT z FROM Zipcode z")
public class Zipcode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String address;

	@Column(name="prefecture_cd")
	private String prefectureCd;

	@Column(name="prefecture_name")
	private String prefectureName;

	private String zipcode;

	public Zipcode() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPrefectureCd() {
		return this.prefectureCd;
	}

	public void setPrefectureCd(String prefectureCd) {
		this.prefectureCd = prefectureCd;
	}

	public String getPrefectureName() {
		return this.prefectureName;
	}

	public void setPrefectureName(String prefectureName) {
		this.prefectureName = prefectureName;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}