package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the social_contribution_actual_results database table.
 * 
 */
@Entity
@Table(name="social_contribution_actual_results")
@NamedQuery(name="SocialContributionActualResult.findAll", query="SELECT s FROM SocialContributionActualResult s")
public class SocialContributionActualResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="activity_content_cd")
	private String activityContentCd;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;

	@Column(name="group_detail")
	private String groupDetail;

	@Column(name="group_name")
	private String groupName;

	@Column(name="held_times")
	private Integer heldTimes;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="register_category_cd")
	private String registerCategoryCd;

	@Temporal(TemporalType.DATE)
	@Column(name="start_date")
	private Date startDate;

	@Column(name="training_name")
	private String trainingName;

	private String type;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	private String venue;

	@Column(name="version_no")
	private Integer versionNo;

	public SocialContributionActualResult() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActivityContentCd() {
		return this.activityContentCd;
	}

	public void setActivityContentCd(String activityContentCd) {
		this.activityContentCd = activityContentCd;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getGroupDetail() {
		return this.groupDetail;
	}

	public void setGroupDetail(String groupDetail) {
		this.groupDetail = groupDetail;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getHeldTimes() {
		return this.heldTimes;
	}

	public void setHeldTimes(Integer heldTimes) {
		this.heldTimes = heldTimes;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public String getRegisterCategoryCd() {
		return this.registerCategoryCd;
	}

	public void setRegisterCategoryCd(String registerCategoryCd) {
		this.registerCategoryCd = registerCategoryCd;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getTrainingName() {
		return this.trainingName;
	}

	public void setTrainingName(String trainingName) {
		this.trainingName = trainingName;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getVenue() {
		return this.venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}