package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the members database table.
 * 
 */
@Entity
@Table(name="members")
@NamedQuery(name="Member.findAll", query="SELECT m FROM Member m")
public class Member implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="affiliated_prefecture_cd")
	private String affiliatedPrefectureCd;

	private Integer age;

	@Column(name="alma_mater_name")
	private String almaMaterName;

	@Column(name="belong_pref_association")
	private Boolean belongPrefAssociation;

	@Column(name="bill_to_japan_address")
	private Boolean billToJapanAddress;

	@Temporal(TemporalType.DATE)
	@Column(name="birth_day")
	private Date birthDay;

	@Column(name="child_welfare_facility")
	private String childWelfareFacility;

	@Column(name="child_welfare_service_category_cd")
	private String childWelfareServiceCategoryCd;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	@Column(name="degree1_award_year")
	private Integer degree1AwardYear;

	@Column(name="degree1_education_facility_name")
	private String degree1EducationFacilityName;

	@Column(name="degree1_field_name")
	private String degree1FieldName;

	@Column(name="degree1_no")
	private Integer degree1No;

	@Column(name="degree2_award_year")
	private Integer degree2AwardYear;

	@Column(name="degree2_education_facility_name")
	private String degree2EducationFacilityName;

	@Column(name="degree2_field_name")
	private String degree2FieldName;

	@Column(name="degree2_no")
	private Integer degree2No;

	@Column(name="degree3_award_year")
	private Integer degree3AwardYear;

	@Column(name="degree3_education_facility_name")
	private String degree3EducationFacilityName;

	@Column(name="degree3_field_name")
	private String degree3FieldName;

	@Column(name="degree3_no")
	private Integer degree3No;

	@Column(name="degree4_award_year")
	private Integer degree4AwardYear;

	@Column(name="degree4_education_facility_name")
	private String degree4EducationFacilityName;

	@Column(name="degree4_field_name")
	private String degree4FieldName;

	@Column(name="degree4_no")
	private Integer degree4No;

	@Column(name="degree5_award_year")
	private Integer degree5AwardYear;

	@Column(name="degree5_education_facility_name")
	private String degree5EducationFacilityName;

	@Column(name="degree5_field_name")
	private String degree5FieldName;

	@Column(name="degree5_no")
	private Integer degree5No;

	private Boolean deleted;

	@Column(name="disaster_support_volunteerable")
	private Boolean disasterSupportVolunteerable;

	private Boolean disclosed;

	@Column(name="education_credits")
	private Integer educationCredits;

	@Column(name="email_address")
	private String emailAddress;

	@Temporal(TemporalType.DATE)
	@Column(name="entry_date")
	private Date entryDate;

	@Column(name="facility_auth_category_cd")
	private String facilityAuthCategoryCd;

	@Column(name="facility_domain_cd")
	private String facilityDomainCd;

	@Column(name="facility_handicap_type")
	private String facilityHandicapType;

	@Column(name="facility_info_hide")
	private Boolean facilityInfoHide;

	@Column(name="facility_sub_domain_cd")
	private String facilitySubDomainCd;

	@Column(name="facility_sub_handicap_type")
	private String facilitySubHandicapType;

	@Column(name="facility_sub_target_disease")
	private String facilitySubTargetDisease;

	@Column(name="facility_target_disease")
	private String facilityTargetDisease;

	@Column(name="first_kana_name")
	private String firstKanaName;

	@Column(name="first_name")
	private String firstName;

	@Column(name="foreign_address")
	private String foreignAddress;

	@Column(name="foreign_license")
	private Boolean foreignLicense;

	private Integer gender;

	@Column(name="has_degree")
	private Boolean hasDegree;

	@Column(name="home_address1")
	private String homeAddress1;

	@Column(name="home_address2")
	private String homeAddress2;

	@Column(name="home_address3")
	private String homeAddress3;

	@Column(name="home_phone_number")
	private String homePhoneNumber;

	@Column(name="home_prefecture_cd")
	private String homePrefectureCd;

	@Column(name="home_zipcode")
	private String homeZipcode;

	@Column(name="honorary_membership")
	private Boolean honoraryMembership;

	@Column(name="honorary_membership_acquired_year")
	private Integer honoraryMembershipAcquiredYear;

	@Column(name="journal_send_category_cd")
	private String journalSendCategoryCd;

	@Column(name="last_kana_name")
	private String lastKanaName;

	@Column(name="last_name")
	private String lastName;

	@Column(name="license_number")
	private String licenseNumber;

	@Column(name="maiden_name")
	private String maidenName;

	@Column(name="member_cd")
	private String memberCd;

	@Column(name="member_list")
	private Boolean memberList;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="membership_certificate_issuanced")
	private Boolean membershipCertificateIssuanced;

	@Column(name="membership_fee_balance")
	private Integer membershipFeeBalance;

	@Column(name="mobile_phone_number")
	private String mobilePhoneNumber;

	@Column(name="mtdlp_rehabilitation_addition")
	private Boolean mtdlpRehabilitationAddition;

	@Column(name="ot_association_mail_receive_cd")
	private Integer otAssociationMailReceiveCd;

	@Column(name="other_facility1_domain")
	private String otherFacility1Domain;

	@Column(name="other_facility1_name")
	private String otherFacility1Name;

	@Column(name="other_facility2_domain")
	private String otherFacility2Domain;

	@Column(name="other_facility2_name")
	private String otherFacility2Name;

	@Column(name="pre_working_prefecture_cd")
	private String preWorkingPrefectureCd;

	@Column(name="pref_association_branch_cd")
	private Integer prefAssociationBranchCd;

	@Column(name="pref_association_education_info_hide")
	private Boolean prefAssociationEducationInfoHide;

	@Column(name="pref_association_fee_balance")
	private Integer prefAssociationFeeBalance;

	@Column(name="pref_association_mail_receive_cd")
	private Integer prefAssociationMailReceiveCd;

	@Column(name="pref_association_member_info_hide")
	private Boolean prefAssociationMemberInfoHide;

	@Column(name="pref_association_no")
	private Integer prefAssociationNo;

	@Column(name="pref_association_nonmember")
	private Boolean prefAssociationNonmember;

	@Column(name="pref_association_prefecture_cd")
	private String prefAssociationPrefectureCd;

	@Column(name="pref_association_shipping_category_no")
	private Integer prefAssociationShippingCategoryNo;

	@Temporal(TemporalType.DATE)
	@Column(name="pref_association_update_date")
	private Date prefAssociationUpdateDate;

	@Column(name="pref_association_value")
	private Integer prefAssociationValue;

	@Column(name="previous_member_no")
	private Integer previousMemberNo;

	@Column(name="print_enabled")
	private Boolean printEnabled;

	@Column(name="qualification_year")
	private Integer qualificationYear;

	private String remarks;

	@Column(name="security_cd")
	private String securityCd;

	@Column(name="shipping_category_no")
	private Integer shippingCategoryNo;

	@Column(name="status_no")
	private Integer statusNo;

	@Column(name="target_age")
	private Integer targetAge;

	@Column(name="target_independence_support_category_cd")
	private String targetIndependenceSupportCategoryCd;

	@Column(name="target_ot_day_count")
	private Integer targetOtDayCount;

	@Column(name="target_primary_purpose")
	private String targetPrimaryPurpose;

	@Column(name="training_school_no")
	private Integer trainingSchoolNo;

	@Column(name="transfer_confirmation")
	private Boolean transferConfirmation;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Temporal(TemporalType.DATE)
	@Column(name="update_from_portal_date")
	private Date updateFromPortalDate;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	@Column(name="web_member_id")
	private String webMemberId;

	@Column(name="web_member_name")
	private String webMemberName;

	@Column(name="web_member_password")
	private String webMemberPassword;

	@Column(name="web_published")
	private Boolean webPublished;

	@Column(name="wfot_entry_status")
	private Integer wfotEntryStatus;

	@Temporal(TemporalType.DATE)
	@Column(name="withdrawal_date")
	private Date withdrawalDate;

	@Column(name="work_phone_number")
	private String workPhoneNumber;

	@Column(name="working_condition")
	private String workingCondition;

	@Column(name="working_condition_facility_count")
	private Integer workingConditionFacilityCount;

	@Column(name="working_condition_facility_name")
	private String workingConditionFacilityName;

	@Column(name="working_facility_cd")
	private String workingFacilityCd;

	@Column(name="working_facility_unit_name")
	private String workingFacilityUnitName;

	@Column(name="working_prefecture_cd")
	private String workingPrefectureCd;

	@Column(name="working_state_cd")
	private String workingStateCd;

	public Member() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAffiliatedPrefectureCd() {
		return this.affiliatedPrefectureCd;
	}

	public void setAffiliatedPrefectureCd(String affiliatedPrefectureCd) {
		this.affiliatedPrefectureCd = affiliatedPrefectureCd;
	}

	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAlmaMaterName() {
		return this.almaMaterName;
	}

	public void setAlmaMaterName(String almaMaterName) {
		this.almaMaterName = almaMaterName;
	}

	public Boolean getBelongPrefAssociation() {
		return this.belongPrefAssociation;
	}

	public void setBelongPrefAssociation(Boolean belongPrefAssociation) {
		this.belongPrefAssociation = belongPrefAssociation;
	}

	public Boolean getBillToJapanAddress() {
		return this.billToJapanAddress;
	}

	public void setBillToJapanAddress(Boolean billToJapanAddress) {
		this.billToJapanAddress = billToJapanAddress;
	}

	public Date getBirthDay() {
		return this.birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getChildWelfareFacility() {
		return this.childWelfareFacility;
	}

	public void setChildWelfareFacility(String childWelfareFacility) {
		this.childWelfareFacility = childWelfareFacility;
	}

	public String getChildWelfareServiceCategoryCd() {
		return this.childWelfareServiceCategoryCd;
	}

	public void setChildWelfareServiceCategoryCd(String childWelfareServiceCategoryCd) {
		this.childWelfareServiceCategoryCd = childWelfareServiceCategoryCd;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Integer getDegree1AwardYear() {
		return this.degree1AwardYear;
	}

	public void setDegree1AwardYear(Integer degree1AwardYear) {
		this.degree1AwardYear = degree1AwardYear;
	}

	public String getDegree1EducationFacilityName() {
		return this.degree1EducationFacilityName;
	}

	public void setDegree1EducationFacilityName(String degree1EducationFacilityName) {
		this.degree1EducationFacilityName = degree1EducationFacilityName;
	}

	public String getDegree1FieldName() {
		return this.degree1FieldName;
	}

	public void setDegree1FieldName(String degree1FieldName) {
		this.degree1FieldName = degree1FieldName;
	}

	public Integer getDegree1No() {
		return this.degree1No;
	}

	public void setDegree1No(Integer degree1No) {
		this.degree1No = degree1No;
	}

	public Integer getDegree2AwardYear() {
		return this.degree2AwardYear;
	}

	public void setDegree2AwardYear(Integer degree2AwardYear) {
		this.degree2AwardYear = degree2AwardYear;
	}

	public String getDegree2EducationFacilityName() {
		return this.degree2EducationFacilityName;
	}

	public void setDegree2EducationFacilityName(String degree2EducationFacilityName) {
		this.degree2EducationFacilityName = degree2EducationFacilityName;
	}

	public String getDegree2FieldName() {
		return this.degree2FieldName;
	}

	public void setDegree2FieldName(String degree2FieldName) {
		this.degree2FieldName = degree2FieldName;
	}

	public Integer getDegree2No() {
		return this.degree2No;
	}

	public void setDegree2No(Integer degree2No) {
		this.degree2No = degree2No;
	}

	public Integer getDegree3AwardYear() {
		return this.degree3AwardYear;
	}

	public void setDegree3AwardYear(Integer degree3AwardYear) {
		this.degree3AwardYear = degree3AwardYear;
	}

	public String getDegree3EducationFacilityName() {
		return this.degree3EducationFacilityName;
	}

	public void setDegree3EducationFacilityName(String degree3EducationFacilityName) {
		this.degree3EducationFacilityName = degree3EducationFacilityName;
	}

	public String getDegree3FieldName() {
		return this.degree3FieldName;
	}

	public void setDegree3FieldName(String degree3FieldName) {
		this.degree3FieldName = degree3FieldName;
	}

	public Integer getDegree3No() {
		return this.degree3No;
	}

	public void setDegree3No(Integer degree3No) {
		this.degree3No = degree3No;
	}

	public Integer getDegree4AwardYear() {
		return this.degree4AwardYear;
	}

	public void setDegree4AwardYear(Integer degree4AwardYear) {
		this.degree4AwardYear = degree4AwardYear;
	}

	public String getDegree4EducationFacilityName() {
		return this.degree4EducationFacilityName;
	}

	public void setDegree4EducationFacilityName(String degree4EducationFacilityName) {
		this.degree4EducationFacilityName = degree4EducationFacilityName;
	}

	public String getDegree4FieldName() {
		return this.degree4FieldName;
	}

	public void setDegree4FieldName(String degree4FieldName) {
		this.degree4FieldName = degree4FieldName;
	}

	public Integer getDegree4No() {
		return this.degree4No;
	}

	public void setDegree4No(Integer degree4No) {
		this.degree4No = degree4No;
	}

	public Integer getDegree5AwardYear() {
		return this.degree5AwardYear;
	}

	public void setDegree5AwardYear(Integer degree5AwardYear) {
		this.degree5AwardYear = degree5AwardYear;
	}

	public String getDegree5EducationFacilityName() {
		return this.degree5EducationFacilityName;
	}

	public void setDegree5EducationFacilityName(String degree5EducationFacilityName) {
		this.degree5EducationFacilityName = degree5EducationFacilityName;
	}

	public String getDegree5FieldName() {
		return this.degree5FieldName;
	}

	public void setDegree5FieldName(String degree5FieldName) {
		this.degree5FieldName = degree5FieldName;
	}

	public Integer getDegree5No() {
		return this.degree5No;
	}

	public void setDegree5No(Integer degree5No) {
		this.degree5No = degree5No;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Boolean getDisasterSupportVolunteerable() {
		return this.disasterSupportVolunteerable;
	}

	public void setDisasterSupportVolunteerable(Boolean disasterSupportVolunteerable) {
		this.disasterSupportVolunteerable = disasterSupportVolunteerable;
	}

	public Boolean getDisclosed() {
		return this.disclosed;
	}

	public void setDisclosed(Boolean disclosed) {
		this.disclosed = disclosed;
	}

	public Integer getEducationCredits() {
		return this.educationCredits;
	}

	public void setEducationCredits(Integer educationCredits) {
		this.educationCredits = educationCredits;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Date getEntryDate() {
		return this.entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public String getFacilityAuthCategoryCd() {
		return this.facilityAuthCategoryCd;
	}

	public void setFacilityAuthCategoryCd(String facilityAuthCategoryCd) {
		this.facilityAuthCategoryCd = facilityAuthCategoryCd;
	}

	public String getFacilityDomainCd() {
		return this.facilityDomainCd;
	}

	public void setFacilityDomainCd(String facilityDomainCd) {
		this.facilityDomainCd = facilityDomainCd;
	}

	public String getFacilityHandicapType() {
		return this.facilityHandicapType;
	}

	public void setFacilityHandicapType(String facilityHandicapType) {
		this.facilityHandicapType = facilityHandicapType;
	}

	public Boolean getFacilityInfoHide() {
		return this.facilityInfoHide;
	}

	public void setFacilityInfoHide(Boolean facilityInfoHide) {
		this.facilityInfoHide = facilityInfoHide;
	}

	public String getFacilitySubDomainCd() {
		return this.facilitySubDomainCd;
	}

	public void setFacilitySubDomainCd(String facilitySubDomainCd) {
		this.facilitySubDomainCd = facilitySubDomainCd;
	}

	public String getFacilitySubHandicapType() {
		return this.facilitySubHandicapType;
	}

	public void setFacilitySubHandicapType(String facilitySubHandicapType) {
		this.facilitySubHandicapType = facilitySubHandicapType;
	}

	public String getFacilitySubTargetDisease() {
		return this.facilitySubTargetDisease;
	}

	public void setFacilitySubTargetDisease(String facilitySubTargetDisease) {
		this.facilitySubTargetDisease = facilitySubTargetDisease;
	}

	public String getFacilityTargetDisease() {
		return this.facilityTargetDisease;
	}

	public void setFacilityTargetDisease(String facilityTargetDisease) {
		this.facilityTargetDisease = facilityTargetDisease;
	}

	public String getFirstKanaName() {
		return this.firstKanaName;
	}

	public void setFirstKanaName(String firstKanaName) {
		this.firstKanaName = firstKanaName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getForeignAddress() {
		return this.foreignAddress;
	}

	public void setForeignAddress(String foreignAddress) {
		this.foreignAddress = foreignAddress;
	}

	public Boolean getForeignLicense() {
		return this.foreignLicense;
	}

	public void setForeignLicense(Boolean foreignLicense) {
		this.foreignLicense = foreignLicense;
	}

	public Integer getGender() {
		return this.gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Boolean getHasDegree() {
		return this.hasDegree;
	}

	public void setHasDegree(Boolean hasDegree) {
		this.hasDegree = hasDegree;
	}

	public String getHomeAddress1() {
		return this.homeAddress1;
	}

	public void setHomeAddress1(String homeAddress1) {
		this.homeAddress1 = homeAddress1;
	}

	public String getHomeAddress2() {
		return this.homeAddress2;
	}

	public void setHomeAddress2(String homeAddress2) {
		this.homeAddress2 = homeAddress2;
	}

	public String getHomeAddress3() {
		return this.homeAddress3;
	}

	public void setHomeAddress3(String homeAddress3) {
		this.homeAddress3 = homeAddress3;
	}

	public String getHomePhoneNumber() {
		return this.homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public String getHomePrefectureCd() {
		return this.homePrefectureCd;
	}

	public void setHomePrefectureCd(String homePrefectureCd) {
		this.homePrefectureCd = homePrefectureCd;
	}

	public String getHomeZipcode() {
		return this.homeZipcode;
	}

	public void setHomeZipcode(String homeZipcode) {
		this.homeZipcode = homeZipcode;
	}

	public Boolean getHonoraryMembership() {
		return this.honoraryMembership;
	}

	public void setHonoraryMembership(Boolean honoraryMembership) {
		this.honoraryMembership = honoraryMembership;
	}

	public Integer getHonoraryMembershipAcquiredYear() {
		return this.honoraryMembershipAcquiredYear;
	}

	public void setHonoraryMembershipAcquiredYear(Integer honoraryMembershipAcquiredYear) {
		this.honoraryMembershipAcquiredYear = honoraryMembershipAcquiredYear;
	}

	public String getJournalSendCategoryCd() {
		return this.journalSendCategoryCd;
	}

	public void setJournalSendCategoryCd(String journalSendCategoryCd) {
		this.journalSendCategoryCd = journalSendCategoryCd;
	}

	public String getLastKanaName() {
		return this.lastKanaName;
	}

	public void setLastKanaName(String lastKanaName) {
		this.lastKanaName = lastKanaName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLicenseNumber() {
		return this.licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public String getMaidenName() {
		return this.maidenName;
	}

	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}

	public String getMemberCd() {
		return this.memberCd;
	}

	public void setMemberCd(String memberCd) {
		this.memberCd = memberCd;
	}

	public Boolean getMemberList() {
		return this.memberList;
	}

	public void setMemberList(Boolean memberList) {
		this.memberList = memberList;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Boolean getMembershipCertificateIssuanced() {
		return this.membershipCertificateIssuanced;
	}

	public void setMembershipCertificateIssuanced(Boolean membershipCertificateIssuanced) {
		this.membershipCertificateIssuanced = membershipCertificateIssuanced;
	}

	public Integer getMembershipFeeBalance() {
		return this.membershipFeeBalance;
	}

	public void setMembershipFeeBalance(Integer membershipFeeBalance) {
		this.membershipFeeBalance = membershipFeeBalance;
	}

	public String getMobilePhoneNumber() {
		return this.mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public Boolean getMtdlpRehabilitationAddition() {
		return this.mtdlpRehabilitationAddition;
	}

	public void setMtdlpRehabilitationAddition(Boolean mtdlpRehabilitationAddition) {
		this.mtdlpRehabilitationAddition = mtdlpRehabilitationAddition;
	}

	public Integer getOtAssociationMailReceiveCd() {
		return this.otAssociationMailReceiveCd;
	}

	public void setOtAssociationMailReceiveCd(Integer otAssociationMailReceiveCd) {
		this.otAssociationMailReceiveCd = otAssociationMailReceiveCd;
	}

	public String getOtherFacility1Domain() {
		return this.otherFacility1Domain;
	}

	public void setOtherFacility1Domain(String otherFacility1Domain) {
		this.otherFacility1Domain = otherFacility1Domain;
	}

	public String getOtherFacility1Name() {
		return this.otherFacility1Name;
	}

	public void setOtherFacility1Name(String otherFacility1Name) {
		this.otherFacility1Name = otherFacility1Name;
	}

	public String getOtherFacility2Domain() {
		return this.otherFacility2Domain;
	}

	public void setOtherFacility2Domain(String otherFacility2Domain) {
		this.otherFacility2Domain = otherFacility2Domain;
	}

	public String getOtherFacility2Name() {
		return this.otherFacility2Name;
	}

	public void setOtherFacility2Name(String otherFacility2Name) {
		this.otherFacility2Name = otherFacility2Name;
	}

	public String getPreWorkingPrefectureCd() {
		return this.preWorkingPrefectureCd;
	}

	public void setPreWorkingPrefectureCd(String preWorkingPrefectureCd) {
		this.preWorkingPrefectureCd = preWorkingPrefectureCd;
	}

	public Integer getPrefAssociationBranchCd() {
		return this.prefAssociationBranchCd;
	}

	public void setPrefAssociationBranchCd(Integer prefAssociationBranchCd) {
		this.prefAssociationBranchCd = prefAssociationBranchCd;
	}

	public Boolean getPrefAssociationEducationInfoHide() {
		return this.prefAssociationEducationInfoHide;
	}

	public void setPrefAssociationEducationInfoHide(Boolean prefAssociationEducationInfoHide) {
		this.prefAssociationEducationInfoHide = prefAssociationEducationInfoHide;
	}

	public Integer getPrefAssociationFeeBalance() {
		return this.prefAssociationFeeBalance;
	}

	public void setPrefAssociationFeeBalance(Integer prefAssociationFeeBalance) {
		this.prefAssociationFeeBalance = prefAssociationFeeBalance;
	}

	public Integer getPrefAssociationMailReceiveCd() {
		return this.prefAssociationMailReceiveCd;
	}

	public void setPrefAssociationMailReceiveCd(Integer prefAssociationMailReceiveCd) {
		this.prefAssociationMailReceiveCd = prefAssociationMailReceiveCd;
	}

	public Boolean getPrefAssociationMemberInfoHide() {
		return this.prefAssociationMemberInfoHide;
	}

	public void setPrefAssociationMemberInfoHide(Boolean prefAssociationMemberInfoHide) {
		this.prefAssociationMemberInfoHide = prefAssociationMemberInfoHide;
	}

	public Integer getPrefAssociationNo() {
		return this.prefAssociationNo;
	}

	public void setPrefAssociationNo(Integer prefAssociationNo) {
		this.prefAssociationNo = prefAssociationNo;
	}

	public Boolean getPrefAssociationNonmember() {
		return this.prefAssociationNonmember;
	}

	public void setPrefAssociationNonmember(Boolean prefAssociationNonmember) {
		this.prefAssociationNonmember = prefAssociationNonmember;
	}

	public String getPrefAssociationPrefectureCd() {
		return this.prefAssociationPrefectureCd;
	}

	public void setPrefAssociationPrefectureCd(String prefAssociationPrefectureCd) {
		this.prefAssociationPrefectureCd = prefAssociationPrefectureCd;
	}

	public Integer getPrefAssociationShippingCategoryNo() {
		return this.prefAssociationShippingCategoryNo;
	}

	public void setPrefAssociationShippingCategoryNo(Integer prefAssociationShippingCategoryNo) {
		this.prefAssociationShippingCategoryNo = prefAssociationShippingCategoryNo;
	}

	public Date getPrefAssociationUpdateDate() {
		return this.prefAssociationUpdateDate;
	}

	public void setPrefAssociationUpdateDate(Date prefAssociationUpdateDate) {
		this.prefAssociationUpdateDate = prefAssociationUpdateDate;
	}

	public Integer getPrefAssociationValue() {
		return this.prefAssociationValue;
	}

	public void setPrefAssociationValue(Integer prefAssociationValue) {
		this.prefAssociationValue = prefAssociationValue;
	}

	public Integer getPreviousMemberNo() {
		return this.previousMemberNo;
	}

	public void setPreviousMemberNo(Integer previousMemberNo) {
		this.previousMemberNo = previousMemberNo;
	}

	public Boolean getPrintEnabled() {
		return this.printEnabled;
	}

	public void setPrintEnabled(Boolean printEnabled) {
		this.printEnabled = printEnabled;
	}

	public Integer getQualificationYear() {
		return this.qualificationYear;
	}

	public void setQualificationYear(Integer qualificationYear) {
		this.qualificationYear = qualificationYear;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSecurityCd() {
		return this.securityCd;
	}

	public void setSecurityCd(String securityCd) {
		this.securityCd = securityCd;
	}

	public Integer getShippingCategoryNo() {
		return this.shippingCategoryNo;
	}

	public void setShippingCategoryNo(Integer shippingCategoryNo) {
		this.shippingCategoryNo = shippingCategoryNo;
	}

	public Integer getStatusNo() {
		return this.statusNo;
	}

	public void setStatusNo(Integer statusNo) {
		this.statusNo = statusNo;
	}

	public Integer getTargetAge() {
		return this.targetAge;
	}

	public void setTargetAge(Integer targetAge) {
		this.targetAge = targetAge;
	}

	public String getTargetIndependenceSupportCategoryCd() {
		return this.targetIndependenceSupportCategoryCd;
	}

	public void setTargetIndependenceSupportCategoryCd(String targetIndependenceSupportCategoryCd) {
		this.targetIndependenceSupportCategoryCd = targetIndependenceSupportCategoryCd;
	}

	public Integer getTargetOtDayCount() {
		return this.targetOtDayCount;
	}

	public void setTargetOtDayCount(Integer targetOtDayCount) {
		this.targetOtDayCount = targetOtDayCount;
	}

	public String getTargetPrimaryPurpose() {
		return this.targetPrimaryPurpose;
	}

	public void setTargetPrimaryPurpose(String targetPrimaryPurpose) {
		this.targetPrimaryPurpose = targetPrimaryPurpose;
	}

	public Integer getTrainingSchoolNo() {
		return this.trainingSchoolNo;
	}

	public void setTrainingSchoolNo(Integer trainingSchoolNo) {
		this.trainingSchoolNo = trainingSchoolNo;
	}

	public Boolean getTransferConfirmation() {
		return this.transferConfirmation;
	}

	public void setTransferConfirmation(Boolean transferConfirmation) {
		this.transferConfirmation = transferConfirmation;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Date getUpdateFromPortalDate() {
		return this.updateFromPortalDate;
	}

	public void setUpdateFromPortalDate(Date updateFromPortalDate) {
		this.updateFromPortalDate = updateFromPortalDate;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public String getWebMemberId() {
		return this.webMemberId;
	}

	public void setWebMemberId(String webMemberId) {
		this.webMemberId = webMemberId;
	}

	public String getWebMemberName() {
		return this.webMemberName;
	}

	public void setWebMemberName(String webMemberName) {
		this.webMemberName = webMemberName;
	}

	public String getWebMemberPassword() {
		return this.webMemberPassword;
	}

	public void setWebMemberPassword(String webMemberPassword) {
		this.webMemberPassword = webMemberPassword;
	}

	public Boolean getWebPublished() {
		return this.webPublished;
	}

	public void setWebPublished(Boolean webPublished) {
		this.webPublished = webPublished;
	}

	public Integer getWfotEntryStatus() {
		return this.wfotEntryStatus;
	}

	public void setWfotEntryStatus(Integer wfotEntryStatus) {
		this.wfotEntryStatus = wfotEntryStatus;
	}

	public Date getWithdrawalDate() {
		return this.withdrawalDate;
	}

	public void setWithdrawalDate(Date withdrawalDate) {
		this.withdrawalDate = withdrawalDate;
	}

	public String getWorkPhoneNumber() {
		return this.workPhoneNumber;
	}

	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber;
	}

	public String getWorkingCondition() {
		return this.workingCondition;
	}

	public void setWorkingCondition(String workingCondition) {
		this.workingCondition = workingCondition;
	}

	public Integer getWorkingConditionFacilityCount() {
		return this.workingConditionFacilityCount;
	}

	public void setWorkingConditionFacilityCount(Integer workingConditionFacilityCount) {
		this.workingConditionFacilityCount = workingConditionFacilityCount;
	}

	public String getWorkingConditionFacilityName() {
		return this.workingConditionFacilityName;
	}

	public void setWorkingConditionFacilityName(String workingConditionFacilityName) {
		this.workingConditionFacilityName = workingConditionFacilityName;
	}

	public String getWorkingFacilityCd() {
		return this.workingFacilityCd;
	}

	public void setWorkingFacilityCd(String workingFacilityCd) {
		this.workingFacilityCd = workingFacilityCd;
	}

	public String getWorkingFacilityUnitName() {
		return this.workingFacilityUnitName;
	}

	public void setWorkingFacilityUnitName(String workingFacilityUnitName) {
		this.workingFacilityUnitName = workingFacilityUnitName;
	}

	public String getWorkingPrefectureCd() {
		return this.workingPrefectureCd;
	}

	public void setWorkingPrefectureCd(String workingPrefectureCd) {
		this.workingPrefectureCd = workingPrefectureCd;
	}

	public String getWorkingStateCd() {
		return this.workingStateCd;
	}

	public void setWorkingStateCd(String workingStateCd) {
		this.workingStateCd = workingStateCd;
	}

}