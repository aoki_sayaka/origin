package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the notices database table.
 * 
 */
@Entity
@Table(name="notices")
@NamedQuery(name="Notice.findAll", query="SELECT n FROM Notice n")
public class Notice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private Integer category;

	private String content;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	private Integer destination;

	@Column(name="destination_file_nm")
	private String destinationFileNm;

	@Column(name="destination_range_facility")
	private Boolean destinationRangeFacility;

	@Column(name="destination_range_training_school")
	private Boolean destinationRangeTrainingSchool;

	@Column(name="destination_type")
	private Integer destinationType;

	private Boolean priority;

	@Column(name="publish_from")
	private Timestamp publishFrom;

	@Column(name="publish_to")
	private Timestamp publishTo;

	private String subject;

	@Column(name="system_message")
	private Boolean systemMessage;

	private Integer target;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public Notice() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCategory() {
		return this.category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getDestination() {
		return this.destination;
	}

	public void setDestination(Integer destination) {
		this.destination = destination;
	}

	public String getDestinationFileNm() {
		return this.destinationFileNm;
	}

	public void setDestinationFileNm(String destinationFileNm) {
		this.destinationFileNm = destinationFileNm;
	}

	public Boolean getDestinationRangeFacility() {
		return this.destinationRangeFacility;
	}

	public void setDestinationRangeFacility(Boolean destinationRangeFacility) {
		this.destinationRangeFacility = destinationRangeFacility;
	}

	public Boolean getDestinationRangeTrainingSchool() {
		return this.destinationRangeTrainingSchool;
	}

	public void setDestinationRangeTrainingSchool(Boolean destinationRangeTrainingSchool) {
		this.destinationRangeTrainingSchool = destinationRangeTrainingSchool;
	}

	public Integer getDestinationType() {
		return this.destinationType;
	}

	public void setDestinationType(Integer destinationType) {
		this.destinationType = destinationType;
	}

	public Boolean getPriority() {
		return this.priority;
	}

	public void setPriority(Boolean priority) {
		this.priority = priority;
	}

	public Timestamp getPublishFrom() {
		return this.publishFrom;
	}

	public void setPublishFrom(Timestamp publishFrom) {
		this.publishFrom = publishFrom;
	}

	public Timestamp getPublishTo() {
		return this.publishTo;
	}

	public void setPublishTo(Timestamp publishTo) {
		this.publishTo = publishTo;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Boolean getSystemMessage() {
		return this.systemMessage;
	}

	public void setSystemMessage(Boolean systemMessage) {
		this.systemMessage = systemMessage;
	}

	public Integer getTarget() {
		return this.target;
	}

	public void setTarget(Integer target) {
		this.target = target;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}