package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the history_table_definitions database table.
 * 
 */
@Entity
@Table(name="history_table_definitions")
@NamedQuery(name="HistoryTableDefinition.findAll", query="SELECT h FROM HistoryTableDefinition h")
public class HistoryTableDefinition implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="table_cd")
	private Integer tableCd;

	@Column(name="table_nm")
	private String tableNm;

	@Column(name="table_physical_nm")
	private String tablePhysicalNm;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public HistoryTableDefinition() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getTableCd() {
		return this.tableCd;
	}

	public void setTableCd(Integer tableCd) {
		this.tableCd = tableCd;
	}

	public String getTableNm() {
		return this.tableNm;
	}

	public void setTableNm(String tableNm) {
		this.tableNm = tableNm;
	}

	public String getTablePhysicalNm() {
		return this.tablePhysicalNm;
	}

	public void setTablePhysicalNm(String tablePhysicalNm) {
		this.tablePhysicalNm = tablePhysicalNm;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}