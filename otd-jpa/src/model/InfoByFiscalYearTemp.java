package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the info_by_fiscal_year_temp database table.
 * 
 */
@Entity
@Table(name="info_by_fiscal_year_temp")
@NamedQuery(name="InfoByFiscalYearTemp.findAll", query="SELECT i FROM InfoByFiscalYearTemp i")
public class InfoByFiscalYearTemp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="admission_count")
	private Integer admissionCount;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="entrance_exam_count")
	private Integer entranceExamCount;

	@Column(name="error_message")
	private String errorMessage;

	@Column(name="facility_base_no")
	private String facilityBaseNo;

	@Column(name="fiscal_year")
	private Integer fiscalYear;

	@Column(name="pass_exam_count")
	private Integer passExamCount;

	@Column(name="process_id")
	private Integer processId;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public InfoByFiscalYearTemp() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAdmissionCount() {
		return this.admissionCount;
	}

	public void setAdmissionCount(Integer admissionCount) {
		this.admissionCount = admissionCount;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getEntranceExamCount() {
		return this.entranceExamCount;
	}

	public void setEntranceExamCount(Integer entranceExamCount) {
		this.entranceExamCount = entranceExamCount;
	}

	public String getErrorMessage() {
		return this.errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getFacilityBaseNo() {
		return this.facilityBaseNo;
	}

	public void setFacilityBaseNo(String facilityBaseNo) {
		this.facilityBaseNo = facilityBaseNo;
	}

	public Integer getFiscalYear() {
		return this.fiscalYear;
	}

	public void setFiscalYear(Integer fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public Integer getPassExamCount() {
		return this.passExamCount;
	}

	public void setPassExamCount(Integer passExamCount) {
		this.passExamCount = passExamCount;
	}

	public Integer getProcessId() {
		return this.processId;
	}

	public void setProcessId(Integer processId) {
		this.processId = processId;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}