package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the training_instructors database table.
 * 
 */
@Entity
@Table(name="training_instructors")
@NamedQuery(name="TrainingInstructor.findAll", query="SELECT t FROM TrainingInstructor t")
public class TrainingInstructor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="display_seq_no")
	private Integer displaySeqNo;

	@Column(name="doc_to_person_cd")
	private String docToPersonCd;

	@Column(name="doc_to_person_facility_nm")
	private String docToPersonFacilityNm;

	@Column(name="doc_to_person_nm")
	private String docToPersonNm;

	@Column(name="doc_to_person_print_flag")
	private Boolean docToPersonPrintFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="doc_to_person_send_date")
	private Date docToPersonSendDate;

	@Column(name="doc_to_superiors_cd")
	private String docToSuperiorsCd;

	@Column(name="doc_to_superiors_facility_nm")
	private String docToSuperiorsFacilityNm;

	@Column(name="doc_to_superiors_nm")
	private String docToSuperiorsNm;

	@Column(name="doc_to_superiors_print_flag")
	private Boolean docToSuperiorsPrintFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="doc_to_superiors_send_date")
	private Date docToSuperiorsSendDate;

	@Column(name="instructor_achievement_flag")
	private String instructorAchievementFlag;

	@Column(name="instructor_category_cd")
	private String instructorCategoryCd;

	@Column(name="instructor_nm")
	private String instructorNm;

	@Column(name="instructor_point")
	private Integer instructorPoint;

	@Column(name="instructor_type_cd")
	private String instructorTypeCd;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="reward_category_cd")
	private String rewardCategoryCd;

	@Column(name="reward_description")
	private String rewardDescription;

	@Column(name="reward_description_category_cd")
	private String rewardDescriptionCategoryCd;

	@Column(name="training_no")
	private String trainingNo;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public TrainingInstructor() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getDisplaySeqNo() {
		return this.displaySeqNo;
	}

	public void setDisplaySeqNo(Integer displaySeqNo) {
		this.displaySeqNo = displaySeqNo;
	}

	public String getDocToPersonCd() {
		return this.docToPersonCd;
	}

	public void setDocToPersonCd(String docToPersonCd) {
		this.docToPersonCd = docToPersonCd;
	}

	public String getDocToPersonFacilityNm() {
		return this.docToPersonFacilityNm;
	}

	public void setDocToPersonFacilityNm(String docToPersonFacilityNm) {
		this.docToPersonFacilityNm = docToPersonFacilityNm;
	}

	public String getDocToPersonNm() {
		return this.docToPersonNm;
	}

	public void setDocToPersonNm(String docToPersonNm) {
		this.docToPersonNm = docToPersonNm;
	}

	public Boolean getDocToPersonPrintFlag() {
		return this.docToPersonPrintFlag;
	}

	public void setDocToPersonPrintFlag(Boolean docToPersonPrintFlag) {
		this.docToPersonPrintFlag = docToPersonPrintFlag;
	}

	public Date getDocToPersonSendDate() {
		return this.docToPersonSendDate;
	}

	public void setDocToPersonSendDate(Date docToPersonSendDate) {
		this.docToPersonSendDate = docToPersonSendDate;
	}

	public String getDocToSuperiorsCd() {
		return this.docToSuperiorsCd;
	}

	public void setDocToSuperiorsCd(String docToSuperiorsCd) {
		this.docToSuperiorsCd = docToSuperiorsCd;
	}

	public String getDocToSuperiorsFacilityNm() {
		return this.docToSuperiorsFacilityNm;
	}

	public void setDocToSuperiorsFacilityNm(String docToSuperiorsFacilityNm) {
		this.docToSuperiorsFacilityNm = docToSuperiorsFacilityNm;
	}

	public String getDocToSuperiorsNm() {
		return this.docToSuperiorsNm;
	}

	public void setDocToSuperiorsNm(String docToSuperiorsNm) {
		this.docToSuperiorsNm = docToSuperiorsNm;
	}

	public Boolean getDocToSuperiorsPrintFlag() {
		return this.docToSuperiorsPrintFlag;
	}

	public void setDocToSuperiorsPrintFlag(Boolean docToSuperiorsPrintFlag) {
		this.docToSuperiorsPrintFlag = docToSuperiorsPrintFlag;
	}

	public Date getDocToSuperiorsSendDate() {
		return this.docToSuperiorsSendDate;
	}

	public void setDocToSuperiorsSendDate(Date docToSuperiorsSendDate) {
		this.docToSuperiorsSendDate = docToSuperiorsSendDate;
	}

	public String getInstructorAchievementFlag() {
		return this.instructorAchievementFlag;
	}

	public void setInstructorAchievementFlag(String instructorAchievementFlag) {
		this.instructorAchievementFlag = instructorAchievementFlag;
	}

	public String getInstructorCategoryCd() {
		return this.instructorCategoryCd;
	}

	public void setInstructorCategoryCd(String instructorCategoryCd) {
		this.instructorCategoryCd = instructorCategoryCd;
	}

	public String getInstructorNm() {
		return this.instructorNm;
	}

	public void setInstructorNm(String instructorNm) {
		this.instructorNm = instructorNm;
	}

	public Integer getInstructorPoint() {
		return this.instructorPoint;
	}

	public void setInstructorPoint(Integer instructorPoint) {
		this.instructorPoint = instructorPoint;
	}

	public String getInstructorTypeCd() {
		return this.instructorTypeCd;
	}

	public void setInstructorTypeCd(String instructorTypeCd) {
		this.instructorTypeCd = instructorTypeCd;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public String getRewardCategoryCd() {
		return this.rewardCategoryCd;
	}

	public void setRewardCategoryCd(String rewardCategoryCd) {
		this.rewardCategoryCd = rewardCategoryCd;
	}

	public String getRewardDescription() {
		return this.rewardDescription;
	}

	public void setRewardDescription(String rewardDescription) {
		this.rewardDescription = rewardDescription;
	}

	public String getRewardDescriptionCategoryCd() {
		return this.rewardDescriptionCategoryCd;
	}

	public void setRewardDescriptionCategoryCd(String rewardDescriptionCategoryCd) {
		this.rewardDescriptionCategoryCd = rewardDescriptionCategoryCd;
	}

	public String getTrainingNo() {
		return this.trainingNo;
	}

	public void setTrainingNo(String trainingNo) {
		this.trainingNo = trainingNo;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}