package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the trainings database table.
 * 
 */
@Entity
@Table(name="trainings")
@NamedQuery(name="Training.findAll", query="SELECT t FROM Training t")
public class Training implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="acceptance_cd")
	private String acceptanceCd;

	private String address1;

	private String address2;

	private String address3;

	@Temporal(TemporalType.DATE)
	@Column(name="attend_permission_deposit_period_date")
	private Date attendPermissionDepositPeriodDate;

	@Column(name="attend_permission_doc")
	private String attendPermissionDoc;

	@Temporal(TemporalType.DATE)
	@Column(name="attend_permission_doc_delivery_date")
	private Date attendPermissionDocDeliveryDate;

	@Column(name="attendance_recept_cd")
	private String attendanceReceptCd;

	@Column(name="attending_point")
	private Integer attendingPoint;

	@Temporal(TemporalType.DATE)
	@Column(name="cancel_date")
	private Date cancelDate;

	@Column(name="cancel_flag")
	private Boolean cancelFlag;

	@Column(name="capacity_count")
	private Integer capacityCount;

	@Column(name="course_category_cd")
	private String courseCategoryCd;

	@Column(name="course_nm")
	private String courseNm;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Temporal(TemporalType.DATE)
	@Column(name="delivery_end_date")
	private Date deliveryEndDate;

	@Temporal(TemporalType.DATE)
	@Column(name="delivery_start_date")
	private Date deliveryStartDate;

	@Column(name="elearning_flag")
	private Boolean elearningFlag;

	@Temporal(TemporalType.DATE)
	@Column(name="end_date")
	private Date endDate;

	@Column(name="end_flag")
	private Boolean endFlag;

	@Column(name="entry_fee")
	private Integer entryFee;

	@Column(name="event_date_free")
	private String eventDateFree;

	@Temporal(TemporalType.DATE)
	@Column(name="event_date1")
	private Date eventDate1;

	@Column(name="event_date1_start_time")
	private Time eventDate1StartTime;

	@Temporal(TemporalType.DATE)
	@Column(name="event_date2")
	private Date eventDate2;

	@Temporal(TemporalType.DATE)
	@Column(name="event_date3")
	private Date eventDate3;

	@Column(name="hold_count")
	private Integer holdCount;

	@Column(name="hold_days")
	private Integer holdDays;

	@Column(name="hold_fiscal_year")
	private Integer holdFiscalYear;

	@Temporal(TemporalType.DATE)
	@Column(name="hp_posted_date")
	private Date hpPostedDate;

	@Column(name="hp_posted_flag")
	private Boolean hpPostedFlag;

	@Column(name="hp_posted_user_id")
	private String hpPostedUserId;

	@Column(name="instructor_doc_no_to_person")
	private String instructorDocNoToPerson;

	@Column(name="instructor_doc_no_to_superiors")
	private String instructorDocNoToSuperiors;

	@Column(name="instructor_doc_print_format_cd")
	private String instructorDocPrintFormatCd;

	@Column(name="leave_confirm_cd")
	private String leaveConfirmCd;

	private String message;

	@Column(name="old_training_no")
	private String oldTrainingNo;

	@Column(name="operator_doc_no_to_person")
	private String operatorDocNoToPerson;

	@Column(name="operator_doc_no_to_superiors")
	private String operatorDocNoToSuperiors;

	@Column(name="operator_doc_print_format_cd")
	private String operatorDocPrintFormatCd;

	private String other6;

	private String overview;

	@Column(name="prefecture_cd")
	private String prefectureCd;

	@Temporal(TemporalType.DATE)
	@Column(name="reception_deadline_date")
	private Date receptionDeadlineDate;

	@Temporal(TemporalType.DATE)
	@Column(name="reception_start_date")
	private Date receptionStartDate;

	@Column(name="reception_status_cd")
	private String receptionStatusCd;

	private String remarks;

	@Column(name="student_discount_entry_fee")
	private Integer studentDiscountEntryFee;

	@Column(name="target_cd")
	private String targetCd;

	@Temporal(TemporalType.DATE)
	@Column(name="training_end_date")
	private Date trainingEndDate;

	@Column(name="training_no")
	private String trainingNo;

	@Column(name="training_program_free")
	private String trainingProgramFree;

	@Column(name="training_program_free_flag")
	private Boolean trainingProgramFreeFlag;

	@Column(name="training_type_cd")
	private String trainingTypeCd;

	@Column(name="training_update_datetime")
	private Timestamp trainingUpdateDatetime;

	@Column(name="training_update_user")
	private String trainingUpdateUser;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_status_cd")
	private String updateStatusCd;

	@Column(name="update_user")
	private String updateUser;

	private String venue;

	@Column(name="venue_free")
	private String venueFree;

	@Column(name="venue_free_flag")
	private Boolean venueFreeFlag;

	@Column(name="venue_pref_cd")
	private String venuePrefCd;

	@Column(name="version_no")
	private Integer versionNo;

	private String zipcode;

	public Training() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAcceptanceCd() {
		return this.acceptanceCd;
	}

	public void setAcceptanceCd(String acceptanceCd) {
		this.acceptanceCd = acceptanceCd;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public Date getAttendPermissionDepositPeriodDate() {
		return this.attendPermissionDepositPeriodDate;
	}

	public void setAttendPermissionDepositPeriodDate(Date attendPermissionDepositPeriodDate) {
		this.attendPermissionDepositPeriodDate = attendPermissionDepositPeriodDate;
	}

	public String getAttendPermissionDoc() {
		return this.attendPermissionDoc;
	}

	public void setAttendPermissionDoc(String attendPermissionDoc) {
		this.attendPermissionDoc = attendPermissionDoc;
	}

	public Date getAttendPermissionDocDeliveryDate() {
		return this.attendPermissionDocDeliveryDate;
	}

	public void setAttendPermissionDocDeliveryDate(Date attendPermissionDocDeliveryDate) {
		this.attendPermissionDocDeliveryDate = attendPermissionDocDeliveryDate;
	}

	public String getAttendanceReceptCd() {
		return this.attendanceReceptCd;
	}

	public void setAttendanceReceptCd(String attendanceReceptCd) {
		this.attendanceReceptCd = attendanceReceptCd;
	}

	public Integer getAttendingPoint() {
		return this.attendingPoint;
	}

	public void setAttendingPoint(Integer attendingPoint) {
		this.attendingPoint = attendingPoint;
	}

	public Date getCancelDate() {
		return this.cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public Boolean getCancelFlag() {
		return this.cancelFlag;
	}

	public void setCancelFlag(Boolean cancelFlag) {
		this.cancelFlag = cancelFlag;
	}

	public Integer getCapacityCount() {
		return this.capacityCount;
	}

	public void setCapacityCount(Integer capacityCount) {
		this.capacityCount = capacityCount;
	}

	public String getCourseCategoryCd() {
		return this.courseCategoryCd;
	}

	public void setCourseCategoryCd(String courseCategoryCd) {
		this.courseCategoryCd = courseCategoryCd;
	}

	public String getCourseNm() {
		return this.courseNm;
	}

	public void setCourseNm(String courseNm) {
		this.courseNm = courseNm;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Date getDeliveryEndDate() {
		return this.deliveryEndDate;
	}

	public void setDeliveryEndDate(Date deliveryEndDate) {
		this.deliveryEndDate = deliveryEndDate;
	}

	public Date getDeliveryStartDate() {
		return this.deliveryStartDate;
	}

	public void setDeliveryStartDate(Date deliveryStartDate) {
		this.deliveryStartDate = deliveryStartDate;
	}

	public Boolean getElearningFlag() {
		return this.elearningFlag;
	}

	public void setElearningFlag(Boolean elearningFlag) {
		this.elearningFlag = elearningFlag;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getEndFlag() {
		return this.endFlag;
	}

	public void setEndFlag(Boolean endFlag) {
		this.endFlag = endFlag;
	}

	public Integer getEntryFee() {
		return this.entryFee;
	}

	public void setEntryFee(Integer entryFee) {
		this.entryFee = entryFee;
	}

	public String getEventDateFree() {
		return this.eventDateFree;
	}

	public void setEventDateFree(String eventDateFree) {
		this.eventDateFree = eventDateFree;
	}

	public Date getEventDate1() {
		return this.eventDate1;
	}

	public void setEventDate1(Date eventDate1) {
		this.eventDate1 = eventDate1;
	}

	public Time getEventDate1StartTime() {
		return this.eventDate1StartTime;
	}

	public void setEventDate1StartTime(Time eventDate1StartTime) {
		this.eventDate1StartTime = eventDate1StartTime;
	}

	public Date getEventDate2() {
		return this.eventDate2;
	}

	public void setEventDate2(Date eventDate2) {
		this.eventDate2 = eventDate2;
	}

	public Date getEventDate3() {
		return this.eventDate3;
	}

	public void setEventDate3(Date eventDate3) {
		this.eventDate3 = eventDate3;
	}

	public Integer getHoldCount() {
		return this.holdCount;
	}

	public void setHoldCount(Integer holdCount) {
		this.holdCount = holdCount;
	}

	public Integer getHoldDays() {
		return this.holdDays;
	}

	public void setHoldDays(Integer holdDays) {
		this.holdDays = holdDays;
	}

	public Integer getHoldFiscalYear() {
		return this.holdFiscalYear;
	}

	public void setHoldFiscalYear(Integer holdFiscalYear) {
		this.holdFiscalYear = holdFiscalYear;
	}

	public Date getHpPostedDate() {
		return this.hpPostedDate;
	}

	public void setHpPostedDate(Date hpPostedDate) {
		this.hpPostedDate = hpPostedDate;
	}

	public Boolean getHpPostedFlag() {
		return this.hpPostedFlag;
	}

	public void setHpPostedFlag(Boolean hpPostedFlag) {
		this.hpPostedFlag = hpPostedFlag;
	}

	public String getHpPostedUserId() {
		return this.hpPostedUserId;
	}

	public void setHpPostedUserId(String hpPostedUserId) {
		this.hpPostedUserId = hpPostedUserId;
	}

	public String getInstructorDocNoToPerson() {
		return this.instructorDocNoToPerson;
	}

	public void setInstructorDocNoToPerson(String instructorDocNoToPerson) {
		this.instructorDocNoToPerson = instructorDocNoToPerson;
	}

	public String getInstructorDocNoToSuperiors() {
		return this.instructorDocNoToSuperiors;
	}

	public void setInstructorDocNoToSuperiors(String instructorDocNoToSuperiors) {
		this.instructorDocNoToSuperiors = instructorDocNoToSuperiors;
	}

	public String getInstructorDocPrintFormatCd() {
		return this.instructorDocPrintFormatCd;
	}

	public void setInstructorDocPrintFormatCd(String instructorDocPrintFormatCd) {
		this.instructorDocPrintFormatCd = instructorDocPrintFormatCd;
	}

	public String getLeaveConfirmCd() {
		return this.leaveConfirmCd;
	}

	public void setLeaveConfirmCd(String leaveConfirmCd) {
		this.leaveConfirmCd = leaveConfirmCd;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getOldTrainingNo() {
		return this.oldTrainingNo;
	}

	public void setOldTrainingNo(String oldTrainingNo) {
		this.oldTrainingNo = oldTrainingNo;
	}

	public String getOperatorDocNoToPerson() {
		return this.operatorDocNoToPerson;
	}

	public void setOperatorDocNoToPerson(String operatorDocNoToPerson) {
		this.operatorDocNoToPerson = operatorDocNoToPerson;
	}

	public String getOperatorDocNoToSuperiors() {
		return this.operatorDocNoToSuperiors;
	}

	public void setOperatorDocNoToSuperiors(String operatorDocNoToSuperiors) {
		this.operatorDocNoToSuperiors = operatorDocNoToSuperiors;
	}

	public String getOperatorDocPrintFormatCd() {
		return this.operatorDocPrintFormatCd;
	}

	public void setOperatorDocPrintFormatCd(String operatorDocPrintFormatCd) {
		this.operatorDocPrintFormatCd = operatorDocPrintFormatCd;
	}

	public String getOther6() {
		return this.other6;
	}

	public void setOther6(String other6) {
		this.other6 = other6;
	}

	public String getOverview() {
		return this.overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public String getPrefectureCd() {
		return this.prefectureCd;
	}

	public void setPrefectureCd(String prefectureCd) {
		this.prefectureCd = prefectureCd;
	}

	public Date getReceptionDeadlineDate() {
		return this.receptionDeadlineDate;
	}

	public void setReceptionDeadlineDate(Date receptionDeadlineDate) {
		this.receptionDeadlineDate = receptionDeadlineDate;
	}

	public Date getReceptionStartDate() {
		return this.receptionStartDate;
	}

	public void setReceptionStartDate(Date receptionStartDate) {
		this.receptionStartDate = receptionStartDate;
	}

	public String getReceptionStatusCd() {
		return this.receptionStatusCd;
	}

	public void setReceptionStatusCd(String receptionStatusCd) {
		this.receptionStatusCd = receptionStatusCd;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getStudentDiscountEntryFee() {
		return this.studentDiscountEntryFee;
	}

	public void setStudentDiscountEntryFee(Integer studentDiscountEntryFee) {
		this.studentDiscountEntryFee = studentDiscountEntryFee;
	}

	public String getTargetCd() {
		return this.targetCd;
	}

	public void setTargetCd(String targetCd) {
		this.targetCd = targetCd;
	}

	public Date getTrainingEndDate() {
		return this.trainingEndDate;
	}

	public void setTrainingEndDate(Date trainingEndDate) {
		this.trainingEndDate = trainingEndDate;
	}

	public String getTrainingNo() {
		return this.trainingNo;
	}

	public void setTrainingNo(String trainingNo) {
		this.trainingNo = trainingNo;
	}

	public String getTrainingProgramFree() {
		return this.trainingProgramFree;
	}

	public void setTrainingProgramFree(String trainingProgramFree) {
		this.trainingProgramFree = trainingProgramFree;
	}

	public Boolean getTrainingProgramFreeFlag() {
		return this.trainingProgramFreeFlag;
	}

	public void setTrainingProgramFreeFlag(Boolean trainingProgramFreeFlag) {
		this.trainingProgramFreeFlag = trainingProgramFreeFlag;
	}

	public String getTrainingTypeCd() {
		return this.trainingTypeCd;
	}

	public void setTrainingTypeCd(String trainingTypeCd) {
		this.trainingTypeCd = trainingTypeCd;
	}

	public Timestamp getTrainingUpdateDatetime() {
		return this.trainingUpdateDatetime;
	}

	public void setTrainingUpdateDatetime(Timestamp trainingUpdateDatetime) {
		this.trainingUpdateDatetime = trainingUpdateDatetime;
	}

	public String getTrainingUpdateUser() {
		return this.trainingUpdateUser;
	}

	public void setTrainingUpdateUser(String trainingUpdateUser) {
		this.trainingUpdateUser = trainingUpdateUser;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateStatusCd() {
		return this.updateStatusCd;
	}

	public void setUpdateStatusCd(String updateStatusCd) {
		this.updateStatusCd = updateStatusCd;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getVenue() {
		return this.venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getVenueFree() {
		return this.venueFree;
	}

	public void setVenueFree(String venueFree) {
		this.venueFree = venueFree;
	}

	public Boolean getVenueFreeFlag() {
		return this.venueFreeFlag;
	}

	public void setVenueFreeFlag(Boolean venueFreeFlag) {
		this.venueFreeFlag = venueFreeFlag;
	}

	public String getVenuePrefCd() {
		return this.venuePrefCd;
	}

	public void setVenuePrefCd(String venuePrefCd) {
		this.venuePrefCd = venuePrefCd;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}