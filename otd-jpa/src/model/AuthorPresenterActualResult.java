package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the author_presenter_actual_results database table.
 * 
 */
@Entity
@Table(name="author_presenter_actual_results")
@NamedQuery(name="AuthorPresenterActualResult.findAll", query="SELECT a FROM AuthorPresenterActualResult a")
public class AuthorPresenterActualResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="activity_content_cd")
	private String activityContentCd;

	@Column(name="activity_detail")
	private String activityDetail;

	@Column(name="author_presenter_category_cd")
	private String authorPresenterCategoryCd;

	@Column(name="convention_name")
	private String conventionName;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="end_page")
	private Integer endPage;

	@Column(name="group_name")
	private String groupName;

	@Column(name="held_times")
	private Integer heldTimes;

	@Column(name="isbn_issn")
	private String isbnIssn;

	private String issue;

	@Column(name="journal_name")
	private String journalName;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="other_end_page")
	private Integer otherEndPage;

	@Column(name="other_start_page")
	private Integer otherStartPage;

	@Column(name="publication_ym")
	private Integer publicationYm;

	@Column(name="publish_category_cd")
	private String publishCategoryCd;

	private String publisher;

	@Column(name="register_category_cd")
	private String registerCategoryCd;

	@Column(name="start_page")
	private Integer startPage;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	private String venue;

	@Column(name="version_no")
	private Integer versionNo;

	private String volume;

	public AuthorPresenterActualResult() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActivityContentCd() {
		return this.activityContentCd;
	}

	public void setActivityContentCd(String activityContentCd) {
		this.activityContentCd = activityContentCd;
	}

	public String getActivityDetail() {
		return this.activityDetail;
	}

	public void setActivityDetail(String activityDetail) {
		this.activityDetail = activityDetail;
	}

	public String getAuthorPresenterCategoryCd() {
		return this.authorPresenterCategoryCd;
	}

	public void setAuthorPresenterCategoryCd(String authorPresenterCategoryCd) {
		this.authorPresenterCategoryCd = authorPresenterCategoryCd;
	}

	public String getConventionName() {
		return this.conventionName;
	}

	public void setConventionName(String conventionName) {
		this.conventionName = conventionName;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getEndPage() {
		return this.endPage;
	}

	public void setEndPage(Integer endPage) {
		this.endPage = endPage;
	}

	public String getGroupName() {
		return this.groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getHeldTimes() {
		return this.heldTimes;
	}

	public void setHeldTimes(Integer heldTimes) {
		this.heldTimes = heldTimes;
	}

	public String getIsbnIssn() {
		return this.isbnIssn;
	}

	public void setIsbnIssn(String isbnIssn) {
		this.isbnIssn = isbnIssn;
	}

	public String getIssue() {
		return this.issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getJournalName() {
		return this.journalName;
	}

	public void setJournalName(String journalName) {
		this.journalName = journalName;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Integer getOtherEndPage() {
		return this.otherEndPage;
	}

	public void setOtherEndPage(Integer otherEndPage) {
		this.otherEndPage = otherEndPage;
	}

	public Integer getOtherStartPage() {
		return this.otherStartPage;
	}

	public void setOtherStartPage(Integer otherStartPage) {
		this.otherStartPage = otherStartPage;
	}

	public Integer getPublicationYm() {
		return this.publicationYm;
	}

	public void setPublicationYm(Integer publicationYm) {
		this.publicationYm = publicationYm;
	}

	public String getPublishCategoryCd() {
		return this.publishCategoryCd;
	}

	public void setPublishCategoryCd(String publishCategoryCd) {
		this.publishCategoryCd = publishCategoryCd;
	}

	public String getPublisher() {
		return this.publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getRegisterCategoryCd() {
		return this.registerCategoryCd;
	}

	public void setRegisterCategoryCd(String registerCategoryCd) {
		this.registerCategoryCd = registerCategoryCd;
	}

	public Integer getStartPage() {
		return this.startPage;
	}

	public void setStartPage(Integer startPage) {
		this.startPage = startPage;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getVenue() {
		return this.venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public String getVolume() {
		return this.volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

}