package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the training_school_relations database table.
 * 
 */
@Entity
@Table(name="training_school_relations")
@NamedQuery(name="TrainingSchoolRelation.findAll", query="SELECT t FROM TrainingSchoolRelation t")
public class TrainingSchoolRelation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="facility_base_no")
	private String facilityBaseNo;

	@Column(name="facility_id")
	private Long facilityId;

	@Column(name="origin_school_no")
	private Integer originSchoolNo;

	@Temporal(TemporalType.DATE)
	@Column(name="relation_create_date")
	private Date relationCreateDate;

	@Temporal(TemporalType.DATE)
	@Column(name="relation_delete_date")
	private Date relationDeleteDate;

	@Column(name="relation_no")
	private String relationNo;

	@Column(name="relation_reason_cd")
	private String relationReasonCd;

	@Column(name="relation_update_date")
	private Timestamp relationUpdateDate;

	@Column(name="relation_update_user_id")
	private String relationUpdateUserId;

	@Column(name="seq_no")
	private Integer seqNo;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_seq_no")
	private Integer updateSeqNo;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public TrainingSchoolRelation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getFacilityBaseNo() {
		return this.facilityBaseNo;
	}

	public void setFacilityBaseNo(String facilityBaseNo) {
		this.facilityBaseNo = facilityBaseNo;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public Integer getOriginSchoolNo() {
		return this.originSchoolNo;
	}

	public void setOriginSchoolNo(Integer originSchoolNo) {
		this.originSchoolNo = originSchoolNo;
	}

	public Date getRelationCreateDate() {
		return this.relationCreateDate;
	}

	public void setRelationCreateDate(Date relationCreateDate) {
		this.relationCreateDate = relationCreateDate;
	}

	public Date getRelationDeleteDate() {
		return this.relationDeleteDate;
	}

	public void setRelationDeleteDate(Date relationDeleteDate) {
		this.relationDeleteDate = relationDeleteDate;
	}

	public String getRelationNo() {
		return this.relationNo;
	}

	public void setRelationNo(String relationNo) {
		this.relationNo = relationNo;
	}

	public String getRelationReasonCd() {
		return this.relationReasonCd;
	}

	public void setRelationReasonCd(String relationReasonCd) {
		this.relationReasonCd = relationReasonCd;
	}

	public Timestamp getRelationUpdateDate() {
		return this.relationUpdateDate;
	}

	public void setRelationUpdateDate(Timestamp relationUpdateDate) {
		this.relationUpdateDate = relationUpdateDate;
	}

	public String getRelationUpdateUserId() {
		return this.relationUpdateUserId;
	}

	public void setRelationUpdateUserId(String relationUpdateUserId) {
		this.relationUpdateUserId = relationUpdateUserId;
	}

	public Integer getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Integer getUpdateSeqNo() {
		return this.updateSeqNo;
	}

	public void setUpdateSeqNo(Integer updateSeqNo) {
		this.updateSeqNo = updateSeqNo;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}