package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the facilities database table.
 * 
 */
@Entity
@Table(name="facilities")
@NamedQuery(name="Facility.findAll", query="SELECT f FROM Facility f")
public class Facility implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String address1;

	private String address2;

	private String address3;

	@Column(name="corporate_nm")
	private String corporateNm;

	@Column(name="corporate_nm_kana")
	private String corporateNmKana;

	@Column(name="count_license_3year")
	private Integer countLicense3year;

	@Column(name="count_license_5year")
	private Integer countLicense5year;

	@Column(name="count_member")
	private Integer countMember;

	@Column(name="count_ot")
	private Integer countOt;

	@Column(name="course_years")
	private String courseYears;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	@Column(name="day_night_flag")
	private String dayNightFlag;

	private Boolean deleted;

	private String department;

	@Column(name="doctor_course_flag")
	private String doctorCourseFlag;

	@Column(name="english_notation")
	private String englishNotation;

	private Boolean enrollment;

	@Column(name="facility_base_no")
	private String facilityBaseNo;

	@Column(name="facility_category")
	private String facilityCategory;

	@Column(name="facility_no")
	private String facilityNo;

	@Column(name="facility_seq_no")
	private Integer facilitySeqNo;

	@Column(name="fax_number")
	private String faxNumber;

	@Column(name="master_course_flag")
	private String masterCourseFlag;

	@Column(name="mtdlp_cooperator_flag")
	private Boolean mtdlpCooperatorFlag;

	private String name;

	@Column(name="name_kana")
	private String nameKana;

	@Column(name="name_kana2")
	private String nameKana2;

	private String name2;

	@Column(name="opener_nm")
	private String openerNm;

	@Column(name="opener_nm_kana")
	private String openerNmKana;

	@Column(name="opener_type_cd")
	private String openerTypeCd;

	@Column(name="origin_school_no")
	private Integer originSchoolNo;

	@Column(name="prefecture_cd")
	private String prefectureCd;

	private String remarks;

	@Column(name="responsible_email_address")
	private String responsibleEmailAddress;

	@Column(name="responsible_member_nm")
	private String responsibleMemberNm;

	@Column(name="responsible_member_nm_kana")
	private String responsibleMemberNmKana;

	@Column(name="responsible_member_no")
	private Integer responsibleMemberNo;

	@Column(name="tel_no")
	private String telNo;

	@Column(name="training_school_category_cd")
	private String trainingSchoolCategoryCd;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	@Column(name="wfot_certified_status")
	private Integer wfotCertifiedStatus;

	private String zipcode;

	public Facility() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCorporateNm() {
		return this.corporateNm;
	}

	public void setCorporateNm(String corporateNm) {
		this.corporateNm = corporateNm;
	}

	public String getCorporateNmKana() {
		return this.corporateNmKana;
	}

	public void setCorporateNmKana(String corporateNmKana) {
		this.corporateNmKana = corporateNmKana;
	}

	public Integer getCountLicense3year() {
		return this.countLicense3year;
	}

	public void setCountLicense3year(Integer countLicense3year) {
		this.countLicense3year = countLicense3year;
	}

	public Integer getCountLicense5year() {
		return this.countLicense5year;
	}

	public void setCountLicense5year(Integer countLicense5year) {
		this.countLicense5year = countLicense5year;
	}

	public Integer getCountMember() {
		return this.countMember;
	}

	public void setCountMember(Integer countMember) {
		this.countMember = countMember;
	}

	public Integer getCountOt() {
		return this.countOt;
	}

	public void setCountOt(Integer countOt) {
		this.countOt = countOt;
	}

	public String getCourseYears() {
		return this.courseYears;
	}

	public void setCourseYears(String courseYears) {
		this.courseYears = courseYears;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getDayNightFlag() {
		return this.dayNightFlag;
	}

	public void setDayNightFlag(String dayNightFlag) {
		this.dayNightFlag = dayNightFlag;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDoctorCourseFlag() {
		return this.doctorCourseFlag;
	}

	public void setDoctorCourseFlag(String doctorCourseFlag) {
		this.doctorCourseFlag = doctorCourseFlag;
	}

	public String getEnglishNotation() {
		return this.englishNotation;
	}

	public void setEnglishNotation(String englishNotation) {
		this.englishNotation = englishNotation;
	}

	public Boolean getEnrollment() {
		return this.enrollment;
	}

	public void setEnrollment(Boolean enrollment) {
		this.enrollment = enrollment;
	}

	public String getFacilityBaseNo() {
		return this.facilityBaseNo;
	}

	public void setFacilityBaseNo(String facilityBaseNo) {
		this.facilityBaseNo = facilityBaseNo;
	}

	public String getFacilityCategory() {
		return this.facilityCategory;
	}

	public void setFacilityCategory(String facilityCategory) {
		this.facilityCategory = facilityCategory;
	}

	public String getFacilityNo() {
		return this.facilityNo;
	}

	public void setFacilityNo(String facilityNo) {
		this.facilityNo = facilityNo;
	}

	public Integer getFacilitySeqNo() {
		return this.facilitySeqNo;
	}

	public void setFacilitySeqNo(Integer facilitySeqNo) {
		this.facilitySeqNo = facilitySeqNo;
	}

	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getMasterCourseFlag() {
		return this.masterCourseFlag;
	}

	public void setMasterCourseFlag(String masterCourseFlag) {
		this.masterCourseFlag = masterCourseFlag;
	}

	public Boolean getMtdlpCooperatorFlag() {
		return this.mtdlpCooperatorFlag;
	}

	public void setMtdlpCooperatorFlag(Boolean mtdlpCooperatorFlag) {
		this.mtdlpCooperatorFlag = mtdlpCooperatorFlag;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameKana() {
		return this.nameKana;
	}

	public void setNameKana(String nameKana) {
		this.nameKana = nameKana;
	}

	public String getNameKana2() {
		return this.nameKana2;
	}

	public void setNameKana2(String nameKana2) {
		this.nameKana2 = nameKana2;
	}

	public String getName2() {
		return this.name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getOpenerNm() {
		return this.openerNm;
	}

	public void setOpenerNm(String openerNm) {
		this.openerNm = openerNm;
	}

	public String getOpenerNmKana() {
		return this.openerNmKana;
	}

	public void setOpenerNmKana(String openerNmKana) {
		this.openerNmKana = openerNmKana;
	}

	public String getOpenerTypeCd() {
		return this.openerTypeCd;
	}

	public void setOpenerTypeCd(String openerTypeCd) {
		this.openerTypeCd = openerTypeCd;
	}

	public Integer getOriginSchoolNo() {
		return this.originSchoolNo;
	}

	public void setOriginSchoolNo(Integer originSchoolNo) {
		this.originSchoolNo = originSchoolNo;
	}

	public String getPrefectureCd() {
		return this.prefectureCd;
	}

	public void setPrefectureCd(String prefectureCd) {
		this.prefectureCd = prefectureCd;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getResponsibleEmailAddress() {
		return this.responsibleEmailAddress;
	}

	public void setResponsibleEmailAddress(String responsibleEmailAddress) {
		this.responsibleEmailAddress = responsibleEmailAddress;
	}

	public String getResponsibleMemberNm() {
		return this.responsibleMemberNm;
	}

	public void setResponsibleMemberNm(String responsibleMemberNm) {
		this.responsibleMemberNm = responsibleMemberNm;
	}

	public String getResponsibleMemberNmKana() {
		return this.responsibleMemberNmKana;
	}

	public void setResponsibleMemberNmKana(String responsibleMemberNmKana) {
		this.responsibleMemberNmKana = responsibleMemberNmKana;
	}

	public Integer getResponsibleMemberNo() {
		return this.responsibleMemberNo;
	}

	public void setResponsibleMemberNo(Integer responsibleMemberNo) {
		this.responsibleMemberNo = responsibleMemberNo;
	}

	public String getTelNo() {
		return this.telNo;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public String getTrainingSchoolCategoryCd() {
		return this.trainingSchoolCategoryCd;
	}

	public void setTrainingSchoolCategoryCd(String trainingSchoolCategoryCd) {
		this.trainingSchoolCategoryCd = trainingSchoolCategoryCd;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public Integer getWfotCertifiedStatus() {
		return this.wfotCertifiedStatus;
	}

	public void setWfotCertifiedStatus(Integer wfotCertifiedStatus) {
		this.wfotCertifiedStatus = wfotCertifiedStatus;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}