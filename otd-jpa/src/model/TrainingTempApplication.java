package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the training_temp_applications database table.
 * 
 */
@Entity
@Table(name="training_temp_applications")
@NamedQuery(name="TrainingTempApplication.findAll", query="SELECT t FROM TrainingTempApplication t")
public class TrainingTempApplication implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="application_deadline_datetime")
	private Timestamp applicationDeadlineDatetime;

	@Column(name="case_provide_cd")
	private String caseProvideCd;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="experience_year")
	private Integer experienceYear;

	@Column(name="mail_address")
	private String mailAddress;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="target_disease")
	private String targetDisease;

	@Column(name="training_no")
	private String trainingNo;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public TrainingTempApplication() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getApplicationDeadlineDatetime() {
		return this.applicationDeadlineDatetime;
	}

	public void setApplicationDeadlineDatetime(Timestamp applicationDeadlineDatetime) {
		this.applicationDeadlineDatetime = applicationDeadlineDatetime;
	}

	public String getCaseProvideCd() {
		return this.caseProvideCd;
	}

	public void setCaseProvideCd(String caseProvideCd) {
		this.caseProvideCd = caseProvideCd;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getExperienceYear() {
		return this.experienceYear;
	}

	public void setExperienceYear(Integer experienceYear) {
		this.experienceYear = experienceYear;
	}

	public String getMailAddress() {
		return this.mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public String getTargetDisease() {
		return this.targetDisease;
	}

	public void setTargetDisease(String targetDisease) {
		this.targetDisease = targetDisease;
	}

	public String getTrainingNo() {
		return this.trainingNo;
	}

	public void setTrainingNo(String trainingNo) {
		this.trainingNo = trainingNo;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}