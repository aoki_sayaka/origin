package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the member_password_histories database table.
 * 
 */
@Entity
@Table(name="member_password_histories")
@NamedQuery(name="MemberPasswordHistory.findAll", query="SELECT m FROM MemberPasswordHistory m")
public class MemberPasswordHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="member_no")
	private Integer memberNo;

	private String password;

	@Column(name="temp_password")
	private String tempPassword;

	@Column(name="temp_password_expiration_datetime")
	private Timestamp tempPasswordExpirationDatetime;

	@Column(name="temp_password_flag")
	private Boolean tempPasswordFlag;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public MemberPasswordHistory() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTempPassword() {
		return this.tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public Timestamp getTempPasswordExpirationDatetime() {
		return this.tempPasswordExpirationDatetime;
	}

	public void setTempPasswordExpirationDatetime(Timestamp tempPasswordExpirationDatetime) {
		this.tempPasswordExpirationDatetime = tempPasswordExpirationDatetime;
	}

	public Boolean getTempPasswordFlag() {
		return this.tempPasswordFlag;
	}

	public void setTempPasswordFlag(Boolean tempPasswordFlag) {
		this.tempPasswordFlag = tempPasswordFlag;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}