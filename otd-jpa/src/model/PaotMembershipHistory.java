package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the paot_membership_histories database table.
 * 
 */
@Entity
@Table(name="paot_membership_histories")
@NamedQuery(name="PaotMembershipHistory.findAll", query="SELECT p FROM PaotMembershipHistory p")
public class PaotMembershipHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Temporal(TemporalType.DATE)
	@Column(name="end_enroll_date")
	private Date endEnrollDate;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="paot_cd")
	private Integer paotCd;

	private String remarks;

	@Temporal(TemporalType.DATE)
	@Column(name="start_enroll_date")
	private Date startEnrollDate;

	public PaotMembershipHistory() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getEndEnrollDate() {
		return this.endEnrollDate;
	}

	public void setEndEnrollDate(Date endEnrollDate) {
		this.endEnrollDate = endEnrollDate;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Integer getPaotCd() {
		return this.paotCd;
	}

	public void setPaotCd(Integer paotCd) {
		this.paotCd = paotCd;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getStartEnrollDate() {
		return this.startEnrollDate;
	}

	public void setStartEnrollDate(Date startEnrollDate) {
		this.startEnrollDate = startEnrollDate;
	}

}