package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the training_reports database table.
 * 
 */
@Entity
@Table(name="training_reports")
@NamedQuery(name="TrainingReport.findAll", query="SELECT t FROM TrainingReport t")
public class TrainingReport implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="applicant_count")
	private Integer applicantCount;

	private Integer balance;

	private Integer budget;

	@Column(name="cooperation_evaluation_cd")
	private String cooperationEvaluationCd;

	@Column(name="cooperation_evaluation_content")
	private String cooperationEvaluationContent;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="effect_judge1_cd")
	private String effectJudge1Cd;

	@Column(name="effect_judge1_content")
	private String effectJudge1Content;

	@Column(name="effect_judge2_cd")
	private String effectJudge2Cd;

	@Column(name="effect_judge2_content")
	private String effectJudge2Content;

	private Integer expenditure;

	@Column(name="operator_fullname")
	private String operatorFullname;

	@Column(name="operator_member_no")
	private Integer operatorMemberNo;

	@Column(name="participants_evaluation_cd")
	private String participantsEvaluationCd;

	@Column(name="personal_phone_use_count")
	private Integer personalPhoneUseCount;

	@Column(name="personal_phone_use_hour")
	private Integer personalPhoneUseHour;

	private String summary;

	@Column(name="target_cd")
	private String targetCd;

	@Column(name="training_no")
	private String trainingNo;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	@Column(name="workplace_fax_receive_count")
	private Integer workplaceFaxReceiveCount;

	@Column(name="workplace_fax_send_count")
	private Integer workplaceFaxSendCount;

	@Column(name="workplace_phone_use_count")
	private Integer workplacePhoneUseCount;

	@Column(name="workplace_phone_use_hour")
	private Integer workplacePhoneUseHour;

	public TrainingReport() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getApplicantCount() {
		return this.applicantCount;
	}

	public void setApplicantCount(Integer applicantCount) {
		this.applicantCount = applicantCount;
	}

	public Integer getBalance() {
		return this.balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Integer getBudget() {
		return this.budget;
	}

	public void setBudget(Integer budget) {
		this.budget = budget;
	}

	public String getCooperationEvaluationCd() {
		return this.cooperationEvaluationCd;
	}

	public void setCooperationEvaluationCd(String cooperationEvaluationCd) {
		this.cooperationEvaluationCd = cooperationEvaluationCd;
	}

	public String getCooperationEvaluationContent() {
		return this.cooperationEvaluationContent;
	}

	public void setCooperationEvaluationContent(String cooperationEvaluationContent) {
		this.cooperationEvaluationContent = cooperationEvaluationContent;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getEffectJudge1Cd() {
		return this.effectJudge1Cd;
	}

	public void setEffectJudge1Cd(String effectJudge1Cd) {
		this.effectJudge1Cd = effectJudge1Cd;
	}

	public String getEffectJudge1Content() {
		return this.effectJudge1Content;
	}

	public void setEffectJudge1Content(String effectJudge1Content) {
		this.effectJudge1Content = effectJudge1Content;
	}

	public String getEffectJudge2Cd() {
		return this.effectJudge2Cd;
	}

	public void setEffectJudge2Cd(String effectJudge2Cd) {
		this.effectJudge2Cd = effectJudge2Cd;
	}

	public String getEffectJudge2Content() {
		return this.effectJudge2Content;
	}

	public void setEffectJudge2Content(String effectJudge2Content) {
		this.effectJudge2Content = effectJudge2Content;
	}

	public Integer getExpenditure() {
		return this.expenditure;
	}

	public void setExpenditure(Integer expenditure) {
		this.expenditure = expenditure;
	}

	public String getOperatorFullname() {
		return this.operatorFullname;
	}

	public void setOperatorFullname(String operatorFullname) {
		this.operatorFullname = operatorFullname;
	}

	public Integer getOperatorMemberNo() {
		return this.operatorMemberNo;
	}

	public void setOperatorMemberNo(Integer operatorMemberNo) {
		this.operatorMemberNo = operatorMemberNo;
	}

	public String getParticipantsEvaluationCd() {
		return this.participantsEvaluationCd;
	}

	public void setParticipantsEvaluationCd(String participantsEvaluationCd) {
		this.participantsEvaluationCd = participantsEvaluationCd;
	}

	public Integer getPersonalPhoneUseCount() {
		return this.personalPhoneUseCount;
	}

	public void setPersonalPhoneUseCount(Integer personalPhoneUseCount) {
		this.personalPhoneUseCount = personalPhoneUseCount;
	}

	public Integer getPersonalPhoneUseHour() {
		return this.personalPhoneUseHour;
	}

	public void setPersonalPhoneUseHour(Integer personalPhoneUseHour) {
		this.personalPhoneUseHour = personalPhoneUseHour;
	}

	public String getSummary() {
		return this.summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getTargetCd() {
		return this.targetCd;
	}

	public void setTargetCd(String targetCd) {
		this.targetCd = targetCd;
	}

	public String getTrainingNo() {
		return this.trainingNo;
	}

	public void setTrainingNo(String trainingNo) {
		this.trainingNo = trainingNo;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public Integer getWorkplaceFaxReceiveCount() {
		return this.workplaceFaxReceiveCount;
	}

	public void setWorkplaceFaxReceiveCount(Integer workplaceFaxReceiveCount) {
		this.workplaceFaxReceiveCount = workplaceFaxReceiveCount;
	}

	public Integer getWorkplaceFaxSendCount() {
		return this.workplaceFaxSendCount;
	}

	public void setWorkplaceFaxSendCount(Integer workplaceFaxSendCount) {
		this.workplaceFaxSendCount = workplaceFaxSendCount;
	}

	public Integer getWorkplacePhoneUseCount() {
		return this.workplacePhoneUseCount;
	}

	public void setWorkplacePhoneUseCount(Integer workplacePhoneUseCount) {
		this.workplacePhoneUseCount = workplacePhoneUseCount;
	}

	public Integer getWorkplacePhoneUseHour() {
		return this.workplacePhoneUseHour;
	}

	public void setWorkplacePhoneUseHour(Integer workplacePhoneUseHour) {
		this.workplacePhoneUseHour = workplacePhoneUseHour;
	}

}