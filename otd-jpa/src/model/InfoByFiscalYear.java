package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the info_by_fiscal_year database table.
 * 
 */
@Entity
@Table(name="info_by_fiscal_year")
@NamedQuery(name="InfoByFiscalYear.findAll", query="SELECT i FROM InfoByFiscalYear i")
public class InfoByFiscalYear implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="admission_count")
	private Integer admissionCount;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="entrance_exam_count")
	private Integer entranceExamCount;

	@Column(name="facility_base_no")
	private String facilityBaseNo;

	@Column(name="facility_id")
	private Long facilityId;

	@Column(name="fiscal_year")
	private Integer fiscalYear;

	@Temporal(TemporalType.DATE)
	@Column(name="fiscal_year_create_date")
	private Date fiscalYearCreateDate;

	@Temporal(TemporalType.DATE)
	@Column(name="fiscal_year_delete_date")
	private Date fiscalYearDeleteDate;

	@Column(name="fiscal_year_update_date")
	private Timestamp fiscalYearUpdateDate;

	@Column(name="fiscal_year_update_user_id")
	private String fiscalYearUpdateUserId;

	@Column(name="graduate_count")
	private Integer graduateCount;

	@Column(name="join_rate")
	private BigDecimal joinRate;

	@Column(name="pass_exam_count")
	private Integer passExamCount;

	@Column(name="pass_exam_rate")
	private BigDecimal passExamRate;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_no")
	private Integer updateNo;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	public InfoByFiscalYear() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getAdmissionCount() {
		return this.admissionCount;
	}

	public void setAdmissionCount(Integer admissionCount) {
		this.admissionCount = admissionCount;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getEntranceExamCount() {
		return this.entranceExamCount;
	}

	public void setEntranceExamCount(Integer entranceExamCount) {
		this.entranceExamCount = entranceExamCount;
	}

	public String getFacilityBaseNo() {
		return this.facilityBaseNo;
	}

	public void setFacilityBaseNo(String facilityBaseNo) {
		this.facilityBaseNo = facilityBaseNo;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public Integer getFiscalYear() {
		return this.fiscalYear;
	}

	public void setFiscalYear(Integer fiscalYear) {
		this.fiscalYear = fiscalYear;
	}

	public Date getFiscalYearCreateDate() {
		return this.fiscalYearCreateDate;
	}

	public void setFiscalYearCreateDate(Date fiscalYearCreateDate) {
		this.fiscalYearCreateDate = fiscalYearCreateDate;
	}

	public Date getFiscalYearDeleteDate() {
		return this.fiscalYearDeleteDate;
	}

	public void setFiscalYearDeleteDate(Date fiscalYearDeleteDate) {
		this.fiscalYearDeleteDate = fiscalYearDeleteDate;
	}

	public Timestamp getFiscalYearUpdateDate() {
		return this.fiscalYearUpdateDate;
	}

	public void setFiscalYearUpdateDate(Timestamp fiscalYearUpdateDate) {
		this.fiscalYearUpdateDate = fiscalYearUpdateDate;
	}

	public String getFiscalYearUpdateUserId() {
		return this.fiscalYearUpdateUserId;
	}

	public void setFiscalYearUpdateUserId(String fiscalYearUpdateUserId) {
		this.fiscalYearUpdateUserId = fiscalYearUpdateUserId;
	}

	public Integer getGraduateCount() {
		return this.graduateCount;
	}

	public void setGraduateCount(Integer graduateCount) {
		this.graduateCount = graduateCount;
	}

	public BigDecimal getJoinRate() {
		return this.joinRate;
	}

	public void setJoinRate(BigDecimal joinRate) {
		this.joinRate = joinRate;
	}

	public Integer getPassExamCount() {
		return this.passExamCount;
	}

	public void setPassExamCount(Integer passExamCount) {
		this.passExamCount = passExamCount;
	}

	public BigDecimal getPassExamRate() {
		return this.passExamRate;
	}

	public void setPassExamRate(BigDecimal passExamRate) {
		this.passExamRate = passExamRate;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Integer getUpdateNo() {
		return this.updateNo;
	}

	public void setUpdateNo(Integer updateNo) {
		this.updateNo = updateNo;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

}