package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the paot_officer_histories database table.
 * 
 */
@Entity
@Table(name="paot_officer_histories")
@NamedQuery(name="PaotOfficerHistory.findAll", query="SELECT p FROM PaotOfficerHistory p")
public class PaotOfficerHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="department_cd")
	private Integer departmentCd;

	@Temporal(TemporalType.DATE)
	@Column(name="end_appoint_date")
	private Date endAppointDate;

	@Column(name="member_no")
	private Integer memberNo;

	@Column(name="paot_cd")
	private Integer paotCd;

	@Column(name="position_cd")
	private Integer positionCd;

	private String remarks;

	@Temporal(TemporalType.DATE)
	@Column(name="start_appoint_date")
	private Date startAppointDate;

	public PaotOfficerHistory() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDepartmentCd() {
		return this.departmentCd;
	}

	public void setDepartmentCd(Integer departmentCd) {
		this.departmentCd = departmentCd;
	}

	public Date getEndAppointDate() {
		return this.endAppointDate;
	}

	public void setEndAppointDate(Date endAppointDate) {
		this.endAppointDate = endAppointDate;
	}

	public Integer getMemberNo() {
		return this.memberNo;
	}

	public void setMemberNo(Integer memberNo) {
		this.memberNo = memberNo;
	}

	public Integer getPaotCd() {
		return this.paotCd;
	}

	public void setPaotCd(Integer paotCd) {
		this.paotCd = paotCd;
	}

	public Integer getPositionCd() {
		return this.positionCd;
	}

	public void setPositionCd(Integer positionCd) {
		this.positionCd = positionCd;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getStartAppointDate() {
		return this.startAppointDate;
	}

	public void setStartAppointDate(Date startAppointDate) {
		this.startAppointDate = startAppointDate;
	}

}