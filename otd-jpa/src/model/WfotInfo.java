package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the wfot_info database table.
 * 
 */
@Entity
@Table(name="wfot_info")
@NamedQuery(name="WfotInfo.findAll", query="SELECT w FROM WfotInfo w")
public class WfotInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private String address1;

	private String address2;

	private String address3;

	@Column(name="create_datetime")
	private Timestamp createDatetime;

	@Column(name="create_user")
	private String createUser;

	private Boolean deleted;

	@Column(name="duration_of_course")
	private String durationOfCourse;

	@Column(name="educational_program")
	private String educationalProgram;

	@Column(name="email_address")
	private String emailAddress;

	@Column(name="facility_base_no")
	private String facilityBaseNo;

	@Column(name="facility_id")
	private Long facilityId;

	@Column(name="post_graduate_course_available")
	private Integer postGraduateCourseAvailable;

	@Column(name="prefecture_nm")
	private String prefectureNm;

	@Column(name="qualification_awarded")
	private String qualificationAwarded;

	@Column(name="school_nm")
	private String schoolNm;

	@Column(name="seq_no")
	private Integer seqNo;

	@Column(name="update_datetime")
	private Timestamp updateDatetime;

	@Column(name="update_user")
	private String updateUser;

	@Column(name="version_no")
	private Integer versionNo;

	@Column(name="website_address")
	private String websiteAddress;

	@Temporal(TemporalType.DATE)
	@Column(name="wfot_create_date")
	private Date wfotCreateDate;

	@Temporal(TemporalType.DATE)
	@Column(name="wfot_delete_date")
	private Date wfotDeleteDate;

	@Column(name="wfot_no")
	private String wfotNo;

	@Column(name="wfot_update_date")
	private Timestamp wfotUpdateDate;

	@Column(name="wfot_update_user_id")
	private String wfotUpdateUserId;

	@Column(name="year_course_accredited_by_na")
	private Integer yearCourseAccreditedByNa;

	@Column(name="year_course_commenced")
	private Integer yearCourseCommenced;

	@Column(name="year_course_discontinued")
	private Integer yearCourseDiscontinued;

	@Column(name="year_course_first_last_approved_by_wfot")
	private Integer yearCourseFirstLastApprovedByWfot;

	@Column(name="year_course_last_reviewed_by_na")
	private Integer yearCourseLastReviewedByNa;

	@Column(name="year_course_next_review_monitoring")
	private Integer yearCourseNextReviewMonitoring;

	private String zipcode;

	public WfotInfo() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return this.address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public Timestamp getCreateDatetime() {
		return this.createDatetime;
	}

	public void setCreateDatetime(Timestamp createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Boolean getDeleted() {
		return this.deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getDurationOfCourse() {
		return this.durationOfCourse;
	}

	public void setDurationOfCourse(String durationOfCourse) {
		this.durationOfCourse = durationOfCourse;
	}

	public String getEducationalProgram() {
		return this.educationalProgram;
	}

	public void setEducationalProgram(String educationalProgram) {
		this.educationalProgram = educationalProgram;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFacilityBaseNo() {
		return this.facilityBaseNo;
	}

	public void setFacilityBaseNo(String facilityBaseNo) {
		this.facilityBaseNo = facilityBaseNo;
	}

	public Long getFacilityId() {
		return this.facilityId;
	}

	public void setFacilityId(Long facilityId) {
		this.facilityId = facilityId;
	}

	public Integer getPostGraduateCourseAvailable() {
		return this.postGraduateCourseAvailable;
	}

	public void setPostGraduateCourseAvailable(Integer postGraduateCourseAvailable) {
		this.postGraduateCourseAvailable = postGraduateCourseAvailable;
	}

	public String getPrefectureNm() {
		return this.prefectureNm;
	}

	public void setPrefectureNm(String prefectureNm) {
		this.prefectureNm = prefectureNm;
	}

	public String getQualificationAwarded() {
		return this.qualificationAwarded;
	}

	public void setQualificationAwarded(String qualificationAwarded) {
		this.qualificationAwarded = qualificationAwarded;
	}

	public String getSchoolNm() {
		return this.schoolNm;
	}

	public void setSchoolNm(String schoolNm) {
		this.schoolNm = schoolNm;
	}

	public Integer getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}

	public Timestamp getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Timestamp updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String getUpdateUser() {
		return this.updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getVersionNo() {
		return this.versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public String getWebsiteAddress() {
		return this.websiteAddress;
	}

	public void setWebsiteAddress(String websiteAddress) {
		this.websiteAddress = websiteAddress;
	}

	public Date getWfotCreateDate() {
		return this.wfotCreateDate;
	}

	public void setWfotCreateDate(Date wfotCreateDate) {
		this.wfotCreateDate = wfotCreateDate;
	}

	public Date getWfotDeleteDate() {
		return this.wfotDeleteDate;
	}

	public void setWfotDeleteDate(Date wfotDeleteDate) {
		this.wfotDeleteDate = wfotDeleteDate;
	}

	public String getWfotNo() {
		return this.wfotNo;
	}

	public void setWfotNo(String wfotNo) {
		this.wfotNo = wfotNo;
	}

	public Timestamp getWfotUpdateDate() {
		return this.wfotUpdateDate;
	}

	public void setWfotUpdateDate(Timestamp wfotUpdateDate) {
		this.wfotUpdateDate = wfotUpdateDate;
	}

	public String getWfotUpdateUserId() {
		return this.wfotUpdateUserId;
	}

	public void setWfotUpdateUserId(String wfotUpdateUserId) {
		this.wfotUpdateUserId = wfotUpdateUserId;
	}

	public Integer getYearCourseAccreditedByNa() {
		return this.yearCourseAccreditedByNa;
	}

	public void setYearCourseAccreditedByNa(Integer yearCourseAccreditedByNa) {
		this.yearCourseAccreditedByNa = yearCourseAccreditedByNa;
	}

	public Integer getYearCourseCommenced() {
		return this.yearCourseCommenced;
	}

	public void setYearCourseCommenced(Integer yearCourseCommenced) {
		this.yearCourseCommenced = yearCourseCommenced;
	}

	public Integer getYearCourseDiscontinued() {
		return this.yearCourseDiscontinued;
	}

	public void setYearCourseDiscontinued(Integer yearCourseDiscontinued) {
		this.yearCourseDiscontinued = yearCourseDiscontinued;
	}

	public Integer getYearCourseFirstLastApprovedByWfot() {
		return this.yearCourseFirstLastApprovedByWfot;
	}

	public void setYearCourseFirstLastApprovedByWfot(Integer yearCourseFirstLastApprovedByWfot) {
		this.yearCourseFirstLastApprovedByWfot = yearCourseFirstLastApprovedByWfot;
	}

	public Integer getYearCourseLastReviewedByNa() {
		return this.yearCourseLastReviewedByNa;
	}

	public void setYearCourseLastReviewedByNa(Integer yearCourseLastReviewedByNa) {
		this.yearCourseLastReviewedByNa = yearCourseLastReviewedByNa;
	}

	public Integer getYearCourseNextReviewMonitoring() {
		return this.yearCourseNextReviewMonitoring;
	}

	public void setYearCourseNextReviewMonitoring(Integer yearCourseNextReviewMonitoring) {
		this.yearCourseNextReviewMonitoring = yearCourseNextReviewMonitoring;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}