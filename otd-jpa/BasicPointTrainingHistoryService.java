package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.BasicPointTrainingHistoryRepository;
import jp.or.jaot.domain.repository.LifelongEducationStatusRepository;
import jp.or.jaot.model.dto.BasicPointTrainingHistoryDto;
import jp.or.jaot.model.dto.search.BasicPointTrainingHistorySearchDto;
import jp.or.jaot.model.entity.BasicPointTrainingHistoryEntity;
import jp.or.jaot.model.entity.composite.BasicPointTrainingHistoryCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 基礎ポイント研修履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class BasicPointTrainingHistoryService extends BaseService {

	/** 基礎ポイント研修履歴リポジトリ */
	private final BasicPointTrainingHistoryRepository basicPointTrainingHistoryRep;

	/** 基礎ポイント研修履歴リポジトリ */
	@Autowired
	private LifelongEducationStatusRepository lifelongEducationStatusesRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtTrainingHistoryRep リポジトリ
	 */
	@Autowired
	public BasicPointTrainingHistoryService(BasicPointTrainingHistoryRepository basicPointTrainingHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.basicPointTrainingHistoryRep = basicPointTrainingHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public BasicPointTrainingHistoryDto getSearchResult(Long id) {
		return BasicPointTrainingHistoryDto.create()
				.setEntity(createCompositeEntity(basicPointTrainingHistoryRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public BasicPointTrainingHistoryDto getSearchResultByCondition(BasicPointTrainingHistorySearchDto searchDto) {
		return BasicPointTrainingHistoryDto.create()
				.setEntities(
						createCompositeEntities(basicPointTrainingHistoryRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(BasicPointTrainingHistoryDto dto) {
		// 基礎ポイント研修履歴エンティティ
		BasicPointTrainingHistoryCompositeEntity compositeEntity = dto.getFirstEntity();
		compositeEntity.getBasicPointTrainingHistoryEntities().stream()
				.forEach(E -> insertEntity(basicPointTrainingHistoryRep, E));
		//		insertEntity(basicPointTrainingHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(BasicPointTrainingHistoryDto dto) {
		//
		// 基礎ポイント研修履歴エンティティ
		BasicPointTrainingHistoryCompositeEntity compositeEntity = dto.getFirstEntity();
		// 基礎ポイント研修履歴
		for (BasicPointTrainingHistoryEntity entity : compositeEntity.getBasicPointTrainingHistoryEntities()) {
			logger.debug("基礎ポイント研修履歴検索1={}", basicPointTrainingHistoryRep);
			logger.debug("基礎ポイント研修履歴検索2={}", entity);
			logger.debug("基礎ポイント研修履歴検索3={}", entity.getId());
			updateEntity(basicPointTrainingHistoryRep, entity, entity.getId());
		}

		//		logger.debug("基礎ポイント研修履歴検索1={}",basicPointTrainingHistoryRep);
		//		logger.debug("基礎ポイント研修履歴検索2={}",dto.getFirstEntity());
		//		logger.debug("基礎ポイント研修履歴検索3={}",dto.getFirstEntity().getId());
		//		updateEntity(basicPointTrainingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	/**
	 * エンティティ生成
	 * @param basicPointTrainingHistoryEntity 基礎ポイント研修エンティティ
	 * @return エンティティ
	 */
	private BasicPointTrainingHistoryCompositeEntity createCompositeEntity(
			BasicPointTrainingHistoryEntity basicPointTrainingHistoryEntity) {

		// 基礎ポイント研修履歴エンティティ
		BasicPointTrainingHistoryCompositeEntity entity = new BasicPointTrainingHistoryCompositeEntity();

		// 会員番号
		Integer menberNo = basicPointTrainingHistoryEntity.getMemberNo();

		// 基礎ポイント研修履歴
		try {
			entity.setBasicPointTrainingHistoryEntities(
					basicPointTrainingHistoryRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		//  生涯教育ステータス
		try {
			entity.setLifelongEducationStatusesEntity(lifelongEducationStatusesRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		return entity;
	}

	/**
	 * エンティティリスト生成
	 * @param basicPointTrainingHistoryEntity 基礎ポイント研修履歴エンティティリスト
	 * @return エンティティリスト
	 */
	private List<BasicPointTrainingHistoryCompositeEntity> createCompositeEntities(
			List<BasicPointTrainingHistoryEntity> basicPointTrainingHistoryEntities) {
		return basicPointTrainingHistoryEntities.stream().map(E -> createCompositeEntity(E))
				.collect(Collectors.toList());
	}
	// TODO

}
