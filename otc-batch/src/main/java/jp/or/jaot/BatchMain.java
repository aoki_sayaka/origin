package jp.or.jaot;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import jp.or.jaot.core.exception.business.BatchBusinessException;
import jp.or.jaot.core.util.StringUtil;
import jp.or.jaot.job.common.BaseJob;
import lombok.AllArgsConstructor;

/**
 * バッチプログラムのメイン
 */
@SpringBootApplication
@ImportResource("classpath:applicationContext.xml")
@ComponentScan
@AllArgsConstructor
public class BatchMain implements CommandLineRunner {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(BatchMain.class);

	/** アプリケーションコンテキスト  */
	private final ApplicationContext ctx;

	/**
	 * メイン
	 * @param args 引数リスト
	 */
	public static void main(String[] args) {

		// Springアプリケーション生成
		SpringApplication springApplication = new SpringApplicationBuilder()
				.sources(BatchMain.class)
				.web(WebApplicationType.NONE)
				.build();

		// 実行
		SpringApplication.exit(springApplication.run(args));
	}

	/**
	 * バッチ処理系
	 * @param args コマンドライン引数
	 */
	@Override
	public void run(String... args) throws Exception {

		// パラメータ解析
		String commandName = "";
		if (args.length < 1) {
			System.out.println("第1引数にジョブ名を指定してください");
			return;
		} else {
			for (String arg : args) {
				if (arg.endsWith("Job")) {
					commandName = arg;
					break;
				}
			}
		}
		if (StringUtil.isEmpty(commandName)) {
			System.out.println("ジョブ名が指定されていません。");
			return;
		}

		// ジョブ取得(applicationContext.xmlに定義されているBeanを取得)
		BaseJob job = null;
		try {
			job = (BaseJob) ctx.getBean(commandName);
		} catch (Exception e) {
			String detail = String.format("Command=%s", commandName);
			String message = String.format("ジョブを生成できませんでした : %s", detail);
			throw new BatchBusinessException(detail, message, e.getCause());
		}

		// ジョブ実行
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String logMsg = null;
		long startTime = 0;
		long finishTime = 0;
		try {
			// 開始
			startTime = System.currentTimeMillis();
			logMsg = String.format("[開始]：time=%s, job=%s", sf.format(new Date()), job.getClass().getSimpleName());
			System.out.println(logMsg);
			logger.info(logMsg);

			// 実行
			job.run(args);

		} finally {
			// 終了
			finishTime = System.currentTimeMillis();
			logMsg = String.format("[終了]：time=%s, progress(msec)=%,d", sf.format(new Date()), (finishTime - startTime));
			logger.info(logMsg);
			System.out.println(logMsg);
		}

		return;
	}
}