package jp.or.jaot.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.job.common.BaseJob;
import jp.or.jaot.model.dto.search.BasicOtAttendingSummarySearchDto;
import jp.or.jaot.service.BasicOtAttendingSummaryService;
import lombok.AllArgsConstructor;

/**
 * ジョブ
 * @version 疎通確認用
 */
@Component
@Transactional
@AllArgsConstructor
public class PortalAttendingHistoryJob extends BaseJob {

	/** サービス */
	@Autowired
	private BasicOtAttendingSummaryService basicOtAttendingSummarySrv;

	/**
	 * ジョブ実行
	 * @param args コマンドライン引数
	 */
	@Override
	public void run(String... args) {
		BasicOtAttendingSummarySearchDto searchDto = new BasicOtAttendingSummarySearchDto();

		searchDto.setMemberNo(1234567);
		basicOtAttendingSummarySrv.getSearchResultByCondition(searchDto);
		return;
	}

}
