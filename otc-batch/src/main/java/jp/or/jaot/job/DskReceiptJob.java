package jp.or.jaot.job;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.supercsv.prefs.CsvPreference;

import com.github.mygreen.supercsv.io.CsvAnnotationBeanReader;

import jp.or.jaot.job.common.BaseJob;
import jp.or.jaot.model.dto.DskReceiptDto;
import jp.or.jaot.model.entity.DskReceiptEntity;
import jp.or.jaot.model.vo.DskReceiptCsvVo;
import jp.or.jaot.service.DskReceiptService;

/**
 * DskPOSTリクエストジョブ<br>
 * DSKのサーバにPOSTリクエストを行いデータ取得、DB登録をします。<br>
 * 実際に処理する/otc-batch/src/main/resources以下のアプリケーションプロパティファイルを<br>
 * 最新化後に実行してください。<br>
 * [プロパティファイル名]<br>
 * application.properties<br>
 * [テーブル名]<br>
 * dsk_receipts<br>
 * [処理]<br>
 * DSKサーバにPOSTリクエスト<br>
 * データ取得、ファイル作成<br>
 * データ加工、DB登録<br>
 */
@Component
public class DskReceiptJob extends BaseJob {

	/** DskDepositDataサービスクラス */
	@Autowired
	private DskReceiptService dskReceiptSrv;

	/** POSTインターフェースのURL */
	@Value("${dsk.post-request.url}")
	private String url;

	/** POSTリクエスト ユーザーID */
	@Value("${dsk.post-request.id}")
	private String id;

	/** POSTリクエスト パスワード */
	@Value("${dsk.post-request.password}")
	private String password;

	/** CSVファイル保存ディレクトリパス */
	@Value("${dsk.csv-file.dir}")
	private String path;

	/** SSL証明書チェック有無 0=オフ、1=オン */
	@Value("${dsk.ssl-check.enable}")
	private String sslSwitch;

	/** Proxyサーバー利用有無 0=オフ、1=オン */
	@Value("${dsk.proxy.enable}")
	private String proxySwitch;

	/** Proxyサーバーホスト名 */
	@Value("${dsk.proxy.host}")
	private String host;

	/** Proxyサーバーポート番号 */
	@Value("${dsk.proxy.port}")
	private int port;

	@Override
	public void run(String... args) {
		logger.info("-----処理開始-----");
		//引数の受け取り
		String formParam = String.format("USER_ID=%s&PW=%s", this.id, this.password);
		if (args.length >= 2) {
			formParam = formParam + String.format("&FROM_DATE=%s", args[1]);
		}
		if (args.length >= 3) {
			formParam = formParam + String.format("&TO_DATE=%s", args[2]);
		}
		logger.info("パラメータ＝" + formParam);
		String str = null;
		// https通信、postリクエスト
		try {
			str = callPost(url, "application/x-www-form-urlencoded", formParam);
			logger.debug("取得データ={}", str);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("POSTリクエストに失敗しました", e);
			return;
		}
		// csvfileの作成
		String fileName = createFileNameWithDateTime(path);
		try {
			logger.debug("CSVファイル {} を作成します", fileName);
			saveCsvFile(fileName, str);
			logger.info("CSVファイル {} を作成しました", fileName);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("csvファイル作成に失敗しました", e);
			return;
		}
		//エラーチェック
		try {
			fileErrorCheck(fileName);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("エラーを検出しました", e);
			return;
		}
		// データ読取、リストに格納
		List<DskReceiptCsvVo> list = new ArrayList<>();
		try {
			logger.info("CSVファイル {} のデータを読み取ります", fileName);
			list.addAll(this.readCsvFile(fileName));
			logger.info("読み取ったデータ件数は{}件です", list.size());
			if (0 == list.size()) {
				logger.info("-----処理終了-----");
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("CSVファイルの読み取りに失敗しました", e);
			return;
		}
		try {
			//DB登録
			//DskDepositDataEntity型のentListを作成
			List<DskReceiptEntity> entList = new ArrayList<>();
			for (DskReceiptCsvVo dsk : list) {
				entList.add(dsk.getEntity());
			}
			//DskDepositDataDtoをインスタンス化
			DskReceiptDto dto = DskReceiptDto.create();
			//BaseDtoのentitiesリストに値を格納
			dto.setEntities(entList);
			//登録
			dskReceiptSrv.enter(dto);
			logger.info("正常にDB登録を行いました");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("データの加工、DB登録に失敗しました", e);
			return;
		}
		logger.info("-----処理終了-----");
		return;
	}

	/**
	 *HTTPS通信でPOSTリクエストをしてCSV形式のデータを取るメソッド
	 * @param strPostUrl　POSTリクエストするURL
	 * @param strContentType　コンテンツタイプ
	 * @param formParam　パラメータ
	 * @return
	 * @return　カンマ区切りの文字列
	 */
	private String callPost(String strPostUrl, String strContentType, String formParam) throws Exception {
		HttpsURLConnection con = null;
		//各インスタンス化
		StringBuffer result = new StringBuffer();
		URL url = new URL(strPostUrl);
		Proxy proxy;
		int status;
		try {
			// SSL証明書検証スイッチ
			if ("1".equals(this.sslSwitch)) {
				//本番環境では１を使用
				logger.info("*** SSL証明書のチェックは有効です ***");
				//何もしない
			} else {
				//テスト環境用
				logger.info("*** SSL証明書のチェックは無効です ***");
				disableSSLCertificateChecking();
			}
			//プロキシ使用スイッチ
			if ("1".equals(this.proxySwitch)) {
				proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port));
				con = (HttpsURLConnection) url.openConnection(proxy);
			} else {
				con = (HttpsURLConnection) url.openConnection();
			}
			//POSTリクエスト
			logger.info("POSTリクエストします");
			con.setDoOutput(true);
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", strContentType);
			try (OutputStreamWriter out = new OutputStreamWriter(con.getOutputStream());) {
				out.write(formParam);
			}
			con.connect();
			// HTTPレスポンスコード
			status = con.getResponseCode();
			logger.info("statusコードは{}です", status);
			// テキストを取得する
			String encoding = con.getContentEncoding();
			//文字コードをSJISに指定
			if (null == encoding) {
				encoding = "SJIS";
			}
			try (InputStream in = con.getInputStream();
					InputStreamReader inReader = new InputStreamReader(in, encoding);
					BufferedReader bufReader = new BufferedReader(inReader);) {
				String line = null;
				// 1行ずつテキストを読み込む
				while ((line = bufReader.readLine()) != null) {
					result.append(line);
					//1行ずつ改行コードを挿入
					result.append("\r\n");
				}
			}
			logger.info("取得完了");
		} finally {
			if (con != null) {
				// コネクションを切断
				con.disconnect();
			}
		}
		if (status != HttpsURLConnection.HTTP_OK) {
			// 通信に失敗時
			throw new Exception(result.toString());
		}
		return result.toString();
	}

	/**
	 * SSL証明書チェックをスルーするメソッド
	 * @throws Exception
	 */
	private void disableSSLCertificateChecking() throws Exception {
		// ホスト名の検証を行わない
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String s, SSLSession ses) {
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		// 証明書の検証を行わない
		KeyManager[] km = null;
		TrustManager[] tm = { new X509TrustManager() {
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			}

			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		}
		};
		SSLContext sslcontext = SSLContext.getInstance("SSL");
		sslcontext.init(km, tm, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
	}

	/**
	 * ローカルにファイルを作成しデータを書き込むメソッド
	 *
	 * @param fileName　作成ファイルのフルパス
	 * @param str　カンマ区切りの文字列
	 * @throws Exception
	 */
	private void saveCsvFile(String fileName, String str) throws Exception {
		//fileの作成
		File file = new File(fileName);
		logger.info(fileName + "を作成します");
		// 文字コードを指定する
		try (
				PrintWriter writer = new PrintWriter(
						new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "Shift-JIS")));) {
			//ファイルに文字列を書き込む
			writer.println(str);
		}
	}

	/**
	 * CSVファイルを読み取るメソッド
	 *
	 * @param fileName　作成ファイルのフルパス
	 * @return　CSVデータを加工したlist
	 * @throws Exception
	 */
	private List<DskReceiptCsvVo> readCsvFile(String fileName) throws Exception {
		List<DskReceiptCsvVo> list = new ArrayList<>();
		// 全レコードを一度に読み込む
		try (
				CsvAnnotationBeanReader<DskReceiptCsvVo> csvReader = new CsvAnnotationBeanReader<>(
						DskReceiptCsvVo.class,
						Files.newBufferedReader(new File(fileName).toPath(), Charset.forName("SJIS")),
						CsvPreference.STANDARD_PREFERENCE);) {
			//listにデータを格納
			list = csvReader.readAll();
		}
		return list;
	}

	/**
	 * 作成するファイル名をdsk-csv-yyyyMMddHHmmss形式で出力するメソッド
	 *
	 * @param path　ファイルを作成するディレクトリ
	 * @return　ファイルのフルパス
	 */
	private String createFileNameWithDateTime(String path) {
		LocalDateTime d = LocalDateTime.now();
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
		String s = df.format(d);
		String fileName = String.format("%sdsk-csv-%s.csv", path, s);
		return fileName;
	}

	/**
	 * 作成されたファイルがエラーかどうかチェックするメソッド
	 *
	 * @param fileName　チェックするファイルのフルパス
	 * @throws Exception
	 */
	private void fileErrorCheck(String fileName) throws Exception {
		logger.info(fileName + "のエラーチェックを行います");
		StringBuffer result = new StringBuffer();
		String line;
		File file = new File(fileName);
		try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "SJIS"));) {
			while ((line = bufReader.readLine()) != null) {
				result.append(line);
				result.append("\r\n");
			}
		}
		if (0 == (result.indexOf("error:"))) {
			logger.info("異常");
			throw new Exception(result.toString());
		}
		logger.info("正常");
	}
}
