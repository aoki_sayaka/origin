package jp.or.jaot.job;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.job.common.BaseJob;
import jp.or.jaot.model.dto.HistoryFieldDefinitionDto;
import jp.or.jaot.model.dto.HistoryTableDefinitionDto;
import jp.or.jaot.model.dto.search.HistoryFieldDefinitionSearchDto;
import jp.or.jaot.model.dto.search.HistoryTableDefinitionSearchDto;
import jp.or.jaot.model.entity.HistoryFieldDefinitionEntity;
import jp.or.jaot.model.entity.HistoryTableDefinitionEntity;
import jp.or.jaot.service.HistoryFieldDefinitionService;
import jp.or.jaot.service.HistoryTableDefinitionService;
import jp.or.jaot.utils.DefinitionUtility;
import lombok.AllArgsConstructor;

/**
 * 定義プロパティインポートジョブ<br>
 * 該当するテーブルに対して定義データをインポートします。<br>
 * 実際に処理する/otc-service/src/main/resources以下のプロパティファイルを最新化後に実行してください。<br>
 * [プロパティファイル名]<br>
 * table.properties<br>
 * entity.properties<br>
 * [テーブル名]<br>
 * history_table_definitions<br>
 * history_field_definitions<br>
 * [処理]<br>
 * テーブルに存在しない -> 登録<br>
 * テーブルに存在する -> 更新<br>
 * プロパティファイルに存在しない -> 削除<br>
 */
@Component
@Transactional
@AllArgsConstructor
public class DefinitionPropertyJob extends BaseJob {

	/** 履歴テーブル定義サービス */
	private final HistoryTableDefinitionService historyTableDefinitionSrv;

	/** 履歴フィールド定義サービス */
	private final HistoryFieldDefinitionService historyFieldDefinitionSrv;

	/**
	 * バッチ処理系
	 * @param args コマンドライン引数
	 */
	@Override
	public void run(String... args) {

		// 履歴テーブル処理
		List<TableDto> tableDtos = execTable();

		// 履歴フィールド処理
		if (!CollectionUtils.isEmpty(tableDtos)) {
			execField(tableDtos);
		}

		return;
	}

	/**
	 * 履歴テーブル定義処理
	 * @return 履歴テーブル定義リスト
	 */
	private List<TableDto> execTable() {

		List<TableDto> tableDtos = new ArrayList<TableDto>();

		// 履歴テーブル定義情報取得(全件検索)
		HistoryTableDefinitionDto defDto = historyTableDefinitionSrv
				.getSearchResultByCondition(new HistoryTableDefinitionSearchDto());
		List<HistoryTableDefinitionEntity> defEntities = defDto.getEntities();

		// プロパティマップ取得(テーブル)
		Map<Object, Object> properties = DefinitionUtility.getTableProperties();

		// 最大テーブルコード値取得
		int maxTableCd = defEntities.stream().map(E -> E.getTableCd()).max((a, b) -> a.compareTo(b)).orElse(0);

		// エンティティ処理
		int updateCnt = 0;
		int enterCnt = 0;
		List<String> physicalNms = new ArrayList<String>();
		for (Map.Entry<Object, Object> keyValue : properties.entrySet()) {

			// 値取得
			String nm = keyValue.getValue().toString();
			String physicalNm = keyValue.getKey().toString().split("\\.")[1];
			String entityNm = keyValue.getKey().toString().split("\\.")[0];

			// 既存エンティティ取得
			HistoryTableDefinitionEntity defEntity = defEntities.stream()
					.filter(E -> E.getTablePhysicalNm().equals(physicalNm)).findFirst().orElse(null);

			// エンティティ処理
			HistoryTableDefinitionDto dto = HistoryTableDefinitionDto.createSingle();
			HistoryTableDefinitionEntity entity = dto.getFirstEntity();
			if (defEntity != null) {
				// 更新
				BeanUtil.copyProperties(defEntity, entity, false);
				if (!defEntity.getTableNm().equals(nm) || !defEntity.getTablePhysicalNm().equals(physicalNm)) {
					entity.setTableNm(nm); // テーブル名
					entity.setTablePhysicalNm(physicalNm); // テーブル名(物理)
					historyTableDefinitionSrv.update(dto);
					updateCnt++;
				}
			} else {
				// 追加
				entity.setTableCd(++maxTableCd); // テーブルコード
				entity.setTableNm(nm); // テーブル名
				entity.setTablePhysicalNm(physicalNm);
				entity = historyTableDefinitionSrv.enter(dto);
				enterCnt++;
			}
			physicalNms.add(physicalNm);
			tableDtos.add(new TableDto(entityNm, entity));
		}

		// 削除
		List<HistoryTableDefinitionEntity> deleteEntities = defEntities.stream()
				.filter(E -> !physicalNms.stream().anyMatch(V -> V.equals(E.getTablePhysicalNm())))
				.collect(Collectors.toList());
		for (HistoryTableDefinitionEntity entity : deleteEntities) {
			HistoryTableDefinitionDto dto = HistoryTableDefinitionDto.create();
			dto.setEntity(entity);
			historyTableDefinitionSrv.delete(dto);
		}

		// ログ出力
		System.out.println(String.format("履歴テーブル定義処理 : 対象=%d, 更新=%d, 追加=%d, 削除=%d",
				properties.size(), updateCnt, enterCnt, deleteEntities.size()));

		return tableDtos;
	}

	/**
	 * 履歴フィールド定義処理
	 */
	private void execField(List<TableDto> tableDtos) {

		// 履歴フィールド定義情報取得(全件検索)
		HistoryFieldDefinitionDto defDto = historyFieldDefinitionSrv
				.getSearchResultByCondition(new HistoryFieldDefinitionSearchDto());
		List<HistoryFieldDefinitionEntity> defEntities = defDto.getEntities();

		// プロパティマップ取得(エンティティ)
		Map<Object, Object> properties = DefinitionUtility.getEntityProperties();

		// 最大フィールドコード値取得
		int maxFieldCd = defEntities.stream().map(E -> E.getFieldCd()).max((a, b) -> a.compareTo(b)).orElse(0);

		// エンティティ処理
		int updateCnt = 0;
		int enterCnt = 0;
		TableDto tableDto = null;
		List<FieldDto> fieldDtos = new ArrayList<FieldDto>();
		for (Map.Entry<Object, Object> keyValue : properties.entrySet()) {

			// 値取得
			String[] keys = keyValue.getKey().toString().split("\\.");
			String nm = keyValue.getValue().toString();
			String tableNm = keys[1];
			String physicalNm = keys[2];

			// テーブルDTO取得
			if ((tableDto = tableDtos.stream().filter(D -> D.entity.getTablePhysicalNm().equals(tableNm)).findFirst()
					.orElse(null)) == null) {
				continue;
			}

			// 履歴テーブル定義ID
			Long tableDefinitionId = tableDto.entity.getId();

			// 既存エンティティ取得
			HistoryFieldDefinitionEntity defEntity = defEntities.stream()
					.filter(E -> E.getTableDefinitionId().equals(tableDefinitionId))
					.filter(E -> E.getFieldPhysicalNm().equals(physicalNm)).findFirst().orElse(null);

			// エンティティ処理
			HistoryFieldDefinitionDto dto = HistoryFieldDefinitionDto.createSingle();
			HistoryFieldDefinitionEntity entity = dto.getFirstEntity();
			if (defEntity != null) {
				// 更新
				BeanUtil.copyProperties(defEntity, entity, false);
				if (!defEntity.getFieldNm().equals(nm) || !defEntity.getFieldPhysicalNm().equals(physicalNm)) {
					entity.setFieldNm(nm); // フィールド名
					entity.setFieldPhysicalNm(physicalNm); // フィールド名(物理)
					historyFieldDefinitionSrv.update(dto);
					updateCnt++;
				}
			} else {
				// 追加
				entity.setTableDefinitionId(tableDto.entity.getId()); // 履歴テーブル定義ID
				entity.setFieldCd(++maxFieldCd); // フィールドコード
				entity.setFieldNm(nm); // フィールド名
				entity.setFieldPhysicalNm(physicalNm); // フィールド名(物理)
				historyFieldDefinitionSrv.enter(dto);
				enterCnt++;
			}
			fieldDtos.add(new FieldDto(tableDefinitionId, physicalNm));
		}

		// 削除
		List<HistoryFieldDefinitionEntity> deleteEntities = defEntities.stream()
				.filter(E -> !fieldDtos.stream().anyMatch(
						D -> D.tableId.equals(E.getTableDefinitionId()) && D.physicalNm.equals(E.getFieldPhysicalNm())))
				.collect(Collectors.toList());
		for (HistoryFieldDefinitionEntity entity : deleteEntities) {
			HistoryFieldDefinitionDto dto = HistoryFieldDefinitionDto.create();
			dto.setEntity(entity);
			historyFieldDefinitionSrv.delete(dto);
		}

		// ログ出力
		System.out.println(String.format("履歴フィールド定義処理 : 対象=%d, 更新=%d, 追加=%d, 削除=%d",
				properties.size(), updateCnt, enterCnt, deleteEntities.size()));

		return;
	}

	/**
	 * テーブルDTO
	 */
	class TableDto {
		public String name;
		public HistoryTableDefinitionEntity entity;

		public TableDto(String name, HistoryTableDefinitionEntity entity) {
			this.name = name;
			this.entity = entity;
		}
	}

	/**
	 * フィールドDTO
	 */
	class FieldDto {
		public Long tableId;
		public String physicalNm;

		public FieldDto(Long tableId, String physicalNm) {
			this.tableId = tableId;
			this.physicalNm = physicalNm;
		}
	}
}
