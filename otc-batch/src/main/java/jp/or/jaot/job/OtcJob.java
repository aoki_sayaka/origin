package jp.or.jaot.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.job.common.BaseJob;
import jp.or.jaot.service.OtcService;
import lombok.AllArgsConstructor;

/**
 * ジョブ
 * @version 疎通確認用
 */
@Component
@Transactional
@AllArgsConstructor
public class OtcJob extends BaseJob {

	/** サービス */
	@Autowired
	private OtcService otcSrv;

	/**
	 * ジョブ実行
	 * @param args コマンドライン引数
	 */
	@Override
	public void run(String... args) {
		otcSrv.getSearchResult(); // 利用者検索
		return;
	}
}
