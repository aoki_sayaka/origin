package jp.or.jaot.job.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ジョブの基底クラス
 */
public abstract class BaseJob {

	/** ロガーインスタンス */
	protected final Logger logger;

	/**
	 * コンストラクタ
	 */
	public BaseJob() {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * ジョブ実行
	 * @param args コマンドライン引数
	 */
	abstract public void run(String... args);
}
