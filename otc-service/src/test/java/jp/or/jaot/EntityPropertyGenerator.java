package jp.or.jaot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * エンティティプロパティファイルジェネレータ<br>
 * エンティティファイルから、クラス名、フィールド名、フィールドテキスト(説明)を抽出してプロパティファイルを生成します。<br>
 * [フォーマット]<br>
 * (クラス名).(テーブル名).(フィールド名)=(フィールドテキスト)
 * [実行方法]<br>
 * 「コンテキストメニュー」->「実行」->「Java アプリケーション」<br>
 */
public class EntityPropertyGenerator {

	/** プロジェクトのカレントディレクトリ */
	private static final String DEF_USER_DIR = System.getProperty("user.dir");

	/** 入力元のパス */
	private static final String DEF_INPUT_DIR = "\\src\\main\\java\\jp\\or\\jaot\\model\\entity\\";

	/** 出力先のパス */
	private static final String DEF_OUTPUT_DIR = "\\src\\main\\resources\\";

	/** 出力ファイル名 */
	private static final String DEF_OUTPUT_FILE = "entity.properties";

	/**
	 * mainメソッド
	 * @param args 引数
	 */
	public static void main(String args[]) {
		try {
			EntityPropertyGenerator generator = new EntityPropertyGenerator();
			generator.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 実行
	 * @throws Exception
	 */
	public void execute() throws Exception {

		// 入力対象ディレクトリ
		File inputFileDir = new File(DEF_USER_DIR + DEF_INPUT_DIR);

		// 出力対象ファイル
		File outputFile = new File(DEF_USER_DIR + DEF_OUTPUT_DIR + DEF_OUTPUT_FILE);

		// ファイル出力
		try (PrintWriter pw = new PrintWriter(outputFile)) {
			for (File file : inputFileDir.listFiles()) {
				if (file != null && file.isFile() && file.getName().contains("Entity")) {
					for (String formatLine : getClassDto(file).getFormatLines()) {
						pw.println(StringEscapeUtils.escapeJava(formatLine)); // エスケープ
						System.out.printf("%s\n", formatLine);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * クラスDTO取得<br>
	 * 指定されたファイルから、クラス名、フィールド名、フィールドテキスト(説明)を抽出したDTOを返却します。
	 * @param file ファイル
	 * @return クラスDTO
	 */
	private ClassDto getClassDto(File file) {

		// ファイル読込
		System.out.printf("FileName=%s\n", file.getPath());
		List<String> lines = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ClassDto(); // 空DTO
		}

		// 順序変換(逆順)
		Collections.reverse(lines);

		// クラスフィールド抽出
		ClassDto classDto = new ClassDto();
		ClassFieldDto classFieldDto = null;
		for (String line : lines) {
			String[] splits = line.trim().split(" "); // 分割(スペース区切り)
			if (splits.length >= 3 && splits[0].equals("private")) {

				// リスト追加(直前要素)
				if (classFieldDto != null) {
					classDto.fields.add(classFieldDto);
				}

				// クラスフィールド名取得 ＆ クラスフィールド生成
				classFieldDto = new ClassFieldDto(splits[2].replaceAll(";", ""), "");
			} else if (splits.length >= 3 && splits[0].contains("@Column")) {
				classFieldDto.name = splits[2].replace("\"", "").replace(")", "");// クラスフィールド名取得
			} else if (splits.length >= 2 && splits[0].equals("/**")) {
				classFieldDto.text = line.replaceAll("/\\*\\*", "").replaceAll("\\*/", "").trim(); // クラスフィールドテキスト取得
			} else if (splits.length >= 2 && splits[1].equals("class")) {
				classDto.name = splits[2]; // クラス名取得
			} else if (splits.length >= 2 && splits[0].contains("@Table")) {
				classDto.tableName = splits[2].replace("\"", "").replace(")", ""); //テーブル名(物理)
				break;
			}
		}

		// 終端処理
		if (classFieldDto != null) {
			classDto.fields.add(classFieldDto);
		}

		// 順序変換(正順)
		Collections.reverse(classDto.fields);

		return classDto;
	}

	/**
	 * クラスDTO
	 */
	class ClassDto {
		public String name;
		public String tableName;
		public List<ClassFieldDto> fields = new ArrayList<ClassFieldDto>();

		public List<String> getFormatLines() {
			return fields.stream().map(E -> String.format("%s.%s.%s=%s", name, tableName, E.name, E.text))
					.collect(Collectors.toList());
		}

		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			sb.append(String.format("Class=%s\n", name));
			for (ClassFieldDto dto : fields) {
				sb.append(dto + "\n");
			}
			return sb.toString();
		}
	}

	/**
	 * クラスフィールドDTO
	 */
	class ClassFieldDto {
		public String name;
		public String text;

		public ClassFieldDto(String name, String text) {
			this.name = name;
			this.text = text;
		}

		@Override
		public String toString() {
			return String.format("name=%s, text=%s", name, text);
		}
	}
}
