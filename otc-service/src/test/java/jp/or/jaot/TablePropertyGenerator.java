package jp.or.jaot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

/**
 * テーブルプロパティファイルジェネレータ<br>
 * エンティティファイルから、クラス名、テーブル名(エンティティ説明)を抽出してプロパティファイルを生成します。<br>
 * [フォーマット]<br>
 * (クラス名).(テーブル名)=(テーブル名)<br>
 * [実行方法]<br>
 * 「コンテキストメニュー」->「実行」->「Java アプリケーション」<br>
 */
public class TablePropertyGenerator {

	/** プロジェクトのカレントディレクトリ */
	private static final String DEF_USER_DIR = System.getProperty("user.dir");

	/** 入力元のパス */
	private static final String DEF_INPUT_DIR = "\\src\\main\\java\\jp\\or\\jaot\\model\\entity\\";

	/** 出力先のパス */
	private static final String DEF_OUTPUT_DIR = "\\src\\main\\resources\\";

	/** 出力ファイル名 */
	private static final String DEF_OUTPUT_FILE = "table.properties";

	/**
	 * mainメソッド
	 * @param args 引数
	 */
	public static void main(String args[]) {
		try {
			TablePropertyGenerator generator = new TablePropertyGenerator();
			generator.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 実行
	 * @throws Exception
	 */
	public void execute() throws Exception {

		// 入力対象ディレクトリ
		File inputFileDir = new File(DEF_USER_DIR + DEF_INPUT_DIR);

		// 出力対象ファイル
		File outputFile = new File(DEF_USER_DIR + DEF_OUTPUT_DIR + DEF_OUTPUT_FILE);

		// ファイル出力
		try (PrintWriter pw = new PrintWriter(outputFile)) {
			for (File file : inputFileDir.listFiles()) {
				if (file != null && file.isFile() && file.getName().contains("Entity")) {
					String formatLine = getClassDto(file).getFormatLine();
					pw.println(StringEscapeUtils.escapeJava(formatLine)); // エスケープ
					System.out.printf("%s\n", formatLine);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * クラスDTO取得<br>
	 * 指定されたファイルから、クラス名、テーブル名(エンティティ名)を抽出したDTOを返却します。
	 * @param file ファイル
	 * @return クラスDTO
	 */
	private ClassDto getClassDto(File file) {

		// ファイル読込
		System.out.printf("FileName=%s\n", file.getPath());
		List<String> lines = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = null;
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ClassDto(); // 空DTO
		}

		// クラス抽出
		ClassDto classDto = new ClassDto();
		for (String line : lines) {
			String[] splits = line.trim().split(" "); // 分割(スペース区切り)
			if (splits.length >= 2 && splits[1].contains("エンティティ") && StringUtils.isEmpty(classDto.text)) {
				classDto.text = splits[1].replace("エンティティ", "").replace("<br>", ""); // エンティティ説明
			} else if (splits.length >= 2 && splits[0].contains("@Table")
					&& StringUtils.isEmpty(classDto.physicalName)) {
				classDto.physicalName = splits[2].replace("\"", "").replace(")", ""); // クラス名(物理)
			} else if (splits.length >= 2 && splits[1].equals("class")) {
				classDto.name = splits[2]; // クラス名
				break;
			}
		}

		return classDto;
	}

	/**
	 * クラスDTO
	 */
	class ClassDto {
		public String name;
		public String physicalName;
		public String text;

		public ClassDto() {
			this.name = "";
			this.physicalName = "";
			this.text = "";
		}

		public String getFormatLine() {
			return String.format("%s.%s=%s", name, physicalName, text);
		}

		@Override
		public String toString() {
			return String.format("Class=%s, Text=%s\n", name, text);
		}
	}
}
