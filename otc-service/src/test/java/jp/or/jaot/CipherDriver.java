package jp.or.jaot;

import jp.or.jaot.core.util.CipherUtil;

public class CipherDriver {

	/**
	 * mainメソッド
	 * @param args 引数
	 */
	public static void main(String args[]) {
		if (args.length == 0) {
			System.out.println("[プログラムの引数]を指定してください");
			return;
		}
		try {
			CipherDriver generator = new CipherDriver();
			generator.execute(args);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 実行
	 */
	public void execute(String args[]) throws Exception {
		String input = args[0];
		String encrypt = CipherUtil.encrypt(input);
		String decrypt = CipherUtil.decrypt(input);
		System.out.println("input : " + input);
		System.out.println("encrypt(input) : " + encrypt);
		System.out.println("decrypt(input) : " + decrypt);
		System.out.println("decrypt(encrypt) : " + CipherUtil.decrypt(encrypt));
	}
}
