package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ApplicationProgressRepository;
import jp.or.jaot.model.dto.ApplicationProgressDto;
import jp.or.jaot.model.dto.search.ApplicationProgressSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 申請進捗サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ApplicationProgressService extends BaseService {

	/** 申請進捗リポジトリ */
	private final ApplicationProgressRepository applicationProgressRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param applicationProgressRep リポジトリ
	 */
	@Autowired
	public ApplicationProgressService(ApplicationProgressRepository applicationProgressRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.applicationProgressRep = applicationProgressRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public ApplicationProgressDto getSearchResult(Long id) {
		return ApplicationProgressDto.create().setEntity(applicationProgressRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(キー)
	 * @param ids ID配列
	 * @return DTO
	 */
	public ApplicationProgressDto getSearchResult(Long[] ids) {
		return ApplicationProgressDto.create().setEntities(applicationProgressRep.getSearchResult(ids));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ApplicationProgressDto getSearchResultByCondition(ApplicationProgressSearchDto searchDto) {
		return ApplicationProgressDto.create()
				.setEntities(applicationProgressRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 検索結果取得(条件(自己))
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ApplicationProgressDto getSearchResultMyselfByCondition(ApplicationProgressSearchDto searchDto) {

		// 認証済み会員番号設定
		searchDto.setMemberNo(getAuthenticationMemberNo());

		return ApplicationProgressDto.create()
				.setEntities(applicationProgressRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ApplicationProgressDto getSearchResultListByCondition(ApplicationProgressSearchDto searchDto) {
		return ApplicationProgressDto.create()
				.setEntities(applicationProgressRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(ApplicationProgressDto dto) {
		insertEntity(applicationProgressRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(ApplicationProgressDto dto) {
		dto.getEntities().stream().forEach(E -> updateEntity(applicationProgressRep, E, E.getId()));
	}

	// TODO
}
