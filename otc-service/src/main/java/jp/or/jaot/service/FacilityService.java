package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.FacilityCategoryRepository;
import jp.or.jaot.domain.repository.FacilityRepository;
import jp.or.jaot.model.dto.FacilityDto;
import jp.or.jaot.model.dto.search.FacilityAdminSearchDto;
import jp.or.jaot.model.dto.search.FacilityPortalSearchDto;
import jp.or.jaot.model.entity.FacilityCategoryEntity;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.composite.FacilityCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 施設養成校サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class FacilityService extends BaseService {

	/** 施設養成校リポジトリ */
	@Autowired
	private FacilityRepository facilityRep;

	/** 施設分類リポジトリ */
	@Autowired
	private FacilityCategoryRepository facilityCategoryRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public FacilityService() {
		super(ERROR_PLACE.LOGIC_COMMON);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public FacilityDto getSearchResult(Long id) {
		return FacilityDto.create().setEntity(createCompositeEntity(
				facilityRep.getSearchResult(id), facilityCategoryRep.getSearchResultByFacilityId(id)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityDto getSearchResultByCondition(FacilityAdminSearchDto searchDto) {
		return FacilityDto.create().setEntities(createCompositeEntities(
				facilityRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityDto getSearchResultListByCondition(FacilityAdminSearchDto searchDto) {
		return FacilityDto.create().setEntities(createCompositeEntities(
				facilityRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityDto getSearchResultByCondition(FacilityPortalSearchDto searchDto) {
		return FacilityDto.create().setEntities(createCompositeEntities(
				facilityRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityDto getSearchResultListByCondition(FacilityPortalSearchDto searchDto) {
		return FacilityDto.create().setEntities(createCompositeEntities(
				facilityRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(FacilityDto dto) {

		// 施設養成校エンティティ(複合)
		FacilityCompositeEntity compositeEntity = dto.getFirstEntity();

		// 施設養成校
		FacilityEntity facilityEntity = (FacilityEntity) insertEntity(facilityRep, compositeEntity.getFacilityEntity());

		// 施設分類
		for (FacilityCategoryEntity entity : compositeEntity.getFacilityCategoryEntities()) {
			entity.setFacilityId(facilityEntity.getId()); // 施設養成校ID
			insertEntity(facilityCategoryRep, entity);
		}
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(FacilityDto dto) {

		// 施設養成校エンティティ(複合)
		FacilityCompositeEntity compositeEntity = dto.getFirstEntity();

		// 施設養成校
		FacilityEntity facilityEntity = compositeEntity.getFacilityEntity();
		updateEntity(facilityRep, facilityEntity, facilityEntity.getId());

		// 施設分類
		for (FacilityCategoryEntity entity : compositeEntity.getFacilityCategoryEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				entity.setFacilityId(facilityEntity.getId()); // 施設養成校ID
				insertEntity(facilityCategoryRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(facilityCategoryRep, entity, id); // 削除(物理)
				} else {
					updateEntity(facilityCategoryRep, entity, id); // 更新
				}
			}
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param facilityEntity 施設養成校エンティティ
	 * @param facilityCategoryEntities 施設分類リスト
	 * @return 複合エンティティ
	 */
	private FacilityCompositeEntity createCompositeEntity(
			FacilityEntity facilityEntity, List<FacilityCategoryEntity> facilityCategoryEntities) {

		// 施設養成校エンティティ(複合)
		FacilityCompositeEntity compositeEntity = new FacilityCompositeEntity();

		// 施設養成校ID
		Long facilityId = facilityEntity.getId();

		// 施設養成校
		compositeEntity.setFacilityEntity(facilityEntity);

		// 施設分類
		compositeEntity.setFacilityCategoryEntities(facilityCategoryEntities.stream()
				.filter(E -> E.getFacilityId().equals(facilityId)).collect(Collectors.toList()));

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 施設養成校エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<FacilityCompositeEntity> createCompositeEntities(List<FacilityEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<FacilityCompositeEntity>(); // 空リスト
		}

		// 施設養成校ID配列取得
		Long[] facilityIds = entities.stream().map(E -> E.getId()).toArray(Long[]::new);
		logger.debug("施設養成校ID[{}]: {}", facilityIds.length,
				Arrays.stream(facilityIds).map(E -> E.toString()).collect(Collectors.joining(",")));

		// 施設分類取得(一括)
		List<FacilityCategoryEntity> categoryEntities = facilityCategoryRep.getSearchResultByFacilityIds(facilityIds);

		return entities.stream().map(E -> createCompositeEntity(E, categoryEntities)).collect(Collectors.toList());
	}

	// TODO
}
