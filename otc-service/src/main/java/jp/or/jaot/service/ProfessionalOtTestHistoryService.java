package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ProfessionalOtTestHistoryRepository;
import jp.or.jaot.model.dto.ProfessionalOtTestHistoryDto;
import jp.or.jaot.model.dto.search.ProfessionalOtTestHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 専門作業療法士試験結果履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ProfessionalOtTestHistoryService extends BaseService {

	/** 専門作業療法士試験結果履歴リポジトリ */
	private final ProfessionalOtTestHistoryRepository professionalOtTestHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param professionalOtTestHistorRep リポジトリ
	 */
	@Autowired
	public ProfessionalOtTestHistoryService(ProfessionalOtTestHistoryRepository professionalOtTestHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.professionalOtTestHistoryRep = professionalOtTestHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public ProfessionalOtTestHistoryDto getSearchResult(Long id) {
		return ProfessionalOtTestHistoryDto.create().setEntity(professionalOtTestHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ProfessionalOtTestHistoryDto getSearchResultByCondition(ProfessionalOtTestHistorySearchDto searchDto) {
		return ProfessionalOtTestHistoryDto.create()
				.setEntities(professionalOtTestHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(ProfessionalOtTestHistoryDto dto) {
		insertEntity(professionalOtTestHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(ProfessionalOtTestHistoryDto dto) {
		updateEntity(professionalOtTestHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO

}
