package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.SocialContributionActual2FieldRepository;
import jp.or.jaot.domain.repository.SocialContributionActualFieldRepository;
import jp.or.jaot.domain.repository.SocialContributionActualResultRepository;
import jp.or.jaot.model.dto.SocialContributionActualResultDto;
import jp.or.jaot.model.dto.search.SocialContributionActualResultSearchDto;
import jp.or.jaot.model.entity.SocialContributionActual2FieldEntity;
import jp.or.jaot.model.entity.SocialContributionActualFieldEntity;
import jp.or.jaot.model.entity.SocialContributionActualResultEntity;
import jp.or.jaot.model.entity.composite.SocialContributionActualResultCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 活動実績(後輩育成・社会貢献)サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class SocialContributionActualResultService extends BaseService {

	/** 活動実績(後輩育成・社会貢献)リポジトリ */
	@Autowired
	private SocialContributionActualResultRepository socialContributionActualResultRep;

	/** 活動実績の領域(後輩育成・社会貢献)リポジトリ */
	@Autowired
	private SocialContributionActualFieldRepository socialContributionActualFieldRep;

	/** 活動実績の領域2(後輩育成・社会貢献)リポジトリ */
	@Autowired
	private SocialContributionActual2FieldRepository socialContributionActual2FieldRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public SocialContributionActualResultService() {
		super(ERROR_PLACE.LOGIC_MEMBER);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public SocialContributionActualResultDto getSearchResult(Long id) {
		return SocialContributionActualResultDto.create()
				.setEntity(createCompositeEntity(socialContributionActualResultRep.getSearchResult(id),
						socialContributionActualFieldRep.getSearchResultByResultId(id),
						socialContributionActual2FieldRep.getSearchResultByResultId(id)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public SocialContributionActualResultDto getSearchResultByCondition(
			SocialContributionActualResultSearchDto searchDto) {
		return SocialContributionActualResultDto.create().setEntities(
				createCompositeEntities(socialContributionActualResultRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(SocialContributionActualResultDto dto) {

		// 活動実績(後輩育成・社会貢献)エンティティ(複合)
		SocialContributionActualResultCompositeEntity compositeEntity = dto.getFirstEntity();

		// 活動実績(後輩育成・社会貢献)
		SocialContributionActualResultEntity resultEntity = (SocialContributionActualResultEntity) insertEntity(
				socialContributionActualResultRep, compositeEntity.getSocialContributionActualResultEntity());

		// 活動実績の領域(後輩育成・社会貢献)
		for (SocialContributionActualFieldEntity fieldEntity : compositeEntity
				.getSocialContributionActualFieldEntities()) {
			fieldEntity.setResultId(resultEntity.getId()); // 活動実績ID
			insertEntity(socialContributionActualFieldRep, fieldEntity);
		}

		// 活動実績の領域2(後輩育成・社会貢献)
		for (SocialContributionActual2FieldEntity fieldEntity : compositeEntity
				.getSocialContributionActual2FieldEntities()) {
			fieldEntity.setResultId(resultEntity.getId()); // 活動実績ID
			insertEntity(socialContributionActual2FieldRep, fieldEntity);
		}
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(SocialContributionActualResultDto dto) {

		// 活動実績(後輩育成・社会貢献)エンティティ(複合)
		SocialContributionActualResultCompositeEntity compositeEntity = dto.getFirstEntity();

		// 活動実績(後輩育成・社会貢献)
		SocialContributionActualResultEntity updateEntity = compositeEntity.getSocialContributionActualResultEntity();
		updateEntity(socialContributionActualResultRep, updateEntity, updateEntity.getId());

		// 活動実績の領域(後輩育成・社会貢献)
		for (SocialContributionActualFieldEntity fieldEntity : compositeEntity
				.getSocialContributionActualFieldEntities()) {
			Long id = null;
			if ((id = fieldEntity.getId()) == null) {
				fieldEntity.setResultId(updateEntity.getId()); // 活動実績ID
				insertEntity(socialContributionActualFieldRep, fieldEntity); // 追加
			} else {
				if (BooleanUtils.isTrue(fieldEntity.getDeleted())) {
					deleteEntity(socialContributionActualFieldRep, fieldEntity, id); // 削除(物理)
				} else {
					updateEntity(socialContributionActualFieldRep, fieldEntity, id); // 更新
				}
			}
		}

		// 活動実績の領域2(後輩育成・社会貢献)
		for (SocialContributionActual2FieldEntity fieldEntity : compositeEntity
				.getSocialContributionActual2FieldEntities()) {
			Long id = null;
			if ((id = fieldEntity.getId()) == null) {
				fieldEntity.setResultId(updateEntity.getId()); // 活動実績ID
				insertEntity(socialContributionActual2FieldRep, fieldEntity); // 追加
			} else {
				if (BooleanUtils.isTrue(fieldEntity.getDeleted())) {
					deleteEntity(socialContributionActual2FieldRep, fieldEntity, id); // 削除(物理)
				} else {
					updateEntity(socialContributionActual2FieldRep, fieldEntity, id); // 更新
				}
			}
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param resultEntity 活動実績エンティティ
	 * @param actualFiledEntities 活動実績の領域リスト
	 * @param actual2FiledEntities 活動実績の領域2リスト
	 * @return 複合エンティティ
	 */
	private SocialContributionActualResultCompositeEntity createCompositeEntity(
			SocialContributionActualResultEntity resultEntity,
			List<SocialContributionActualFieldEntity> actualFiledEntities,
			List<SocialContributionActual2FieldEntity> actual2FiledEntities) {

		// 活動実績エンティティ(複合)
		SocialContributionActualResultCompositeEntity entity = new SocialContributionActualResultCompositeEntity();

		// 活動実績ID
		Long resultId = resultEntity.getId();

		// 活動実績
		entity.setSocialContributionActualResultEntity(resultEntity);

		// 活動実績の領域
		entity.setSocialContributionActualFieldEntities(actualFiledEntities.stream()
				.filter(E -> E.getResultId().equals(resultId)).collect(Collectors.toList()));

		// 活動実績の領域2
		entity.setSocialContributionActual2FieldEntities(actual2FiledEntities.stream()
				.filter(E -> E.getResultId().equals(resultId)).collect(Collectors.toList()));

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 活動実績エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<SocialContributionActualResultCompositeEntity> createCompositeEntities(
			List<SocialContributionActualResultEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<SocialContributionActualResultCompositeEntity>(); // 空リスト
		}

		// 活動実績ID配列取得
		Long[] resultIds = entities.stream().map(E -> E.getId()).toArray(Long[]::new);
		logger.debug("活動実績ID[{}]: {}", resultIds.length,
				Arrays.stream(resultIds).map(E -> E.toString()).collect(Collectors.joining(",")));

		// 活動実績の領域取得(一括)
		List<SocialContributionActualFieldEntity> actualFiledEntities = socialContributionActualFieldRep
				.getSearchResultByResultIds(resultIds);

		// 活動実績の領域2取得(一括)
		List<SocialContributionActual2FieldEntity> actual2FiledEntities = socialContributionActual2FieldRep
				.getSearchResultByResultIds(resultIds);

		return entities.stream().map(E -> createCompositeEntity(E, actualFiledEntities, actual2FiledEntities))
				.collect(Collectors.toList());
	}

	// TODO
}
