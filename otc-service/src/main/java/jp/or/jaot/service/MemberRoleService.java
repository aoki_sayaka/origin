package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberRoleRepository;
import jp.or.jaot.model.dto.MemberRoleDto;
import jp.or.jaot.model.dto.search.MemberRoleSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員ロールサービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberRoleService extends BaseService {

	/** 会員ロールリポジトリ */
	private final MemberRoleRepository memberRoleRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberRoleRep リポジトリ
	 */
	@Autowired
	public MemberRoleService(MemberRoleRepository memberRoleRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberRoleRep = memberRoleRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberRoleDto getSearchResultByCondition(MemberRoleSearchDto searchDto) {
		return MemberRoleDto.create().setEntities(memberRoleRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberRoleDto dto) {
		insertEntity(memberRoleRep, dto.getFirstEntity());
	}

	// TODO
}
