package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.CaseReportRepository;
import jp.or.jaot.model.dto.CaseReportDto;
import jp.or.jaot.model.dto.search.CaseReportSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 事例報告サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class CaseReportService extends BaseService {

	/** 事例報告リポジトリ */
	private final CaseReportRepository caseReportRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param caseReportRep リポジトリ
	 */
	@Autowired
	public CaseReportService(CaseReportRepository caseReportRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.caseReportRep = caseReportRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public CaseReportDto getSearchResultByCondition(CaseReportSearchDto searchDto) {
		return CaseReportDto.create().setEntities(caseReportRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
