package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.NoticeConditionPrefRepository;
import jp.or.jaot.model.dto.NoticeConditionPrefDto;
import jp.or.jaot.model.dto.search.NoticeConditionPrefSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * お知らせ(都道府県)サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class NoticeConditionPrefService extends BaseService {

	/** お知らせ(都道府県)リポジトリ */
	private final NoticeConditionPrefRepository noticeConditionPrefRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param noticeConditionPrefRep リポジトリ
	 */
	@Autowired
	public NoticeConditionPrefService(NoticeConditionPrefRepository noticeConditionPrefRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.noticeConditionPrefRep = noticeConditionPrefRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public NoticeConditionPrefDto getSearchResult(Long id) {
		return NoticeConditionPrefDto.create().setEntity(noticeConditionPrefRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public NoticeConditionPrefDto getSearchResultByCondition(NoticeConditionPrefSearchDto searchDto) {
		return NoticeConditionPrefDto.create()
				.setEntities(noticeConditionPrefRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(NoticeConditionPrefDto dto) {
		insertEntity(noticeConditionPrefRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(NoticeConditionPrefDto dto) {
		updateEntity(noticeConditionPrefRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
}
