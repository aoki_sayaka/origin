package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberPasswordHistoryRepository;
import jp.or.jaot.model.dto.MemberPasswordHistoryDto;
import jp.or.jaot.model.dto.search.MemberPasswordHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員パスワード履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberPasswordHistoryService extends BaseService {

	/** 会員パスワード履歴リポジトリ */
	private final MemberPasswordHistoryRepository memberPasswordHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberPasswordHistoryRep リポジトリ
	 */
	@Autowired
	public MemberPasswordHistoryService(MemberPasswordHistoryRepository memberPasswordHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberPasswordHistoryRep = memberPasswordHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberPasswordHistoryDto getSearchResultByCondition(MemberPasswordHistorySearchDto searchDto) {
		return MemberPasswordHistoryDto.create()
				.setEntities(memberPasswordHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberPasswordHistoryDto dto) {
		insertEntity(memberPasswordHistoryRep, dto.getFirstEntity());
	}

	// TODO
}
