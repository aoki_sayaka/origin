package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.QualifiedOtTrainingHistoryRepository;
import jp.or.jaot.model.dto.QualifiedOtTrainingHistoryDto;
import jp.or.jaot.model.dto.search.QualifiedOtTrainingHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 認定作業療法士研修修了認定更新履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class QualifiedOtTrainingHistoryService extends BaseService {

	/** 認定作業療法士研修修了認定更新履歴リポジトリ */
	private final QualifiedOtTrainingHistoryRepository qualifiedOtTrainingHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtAttendingSummaryRep リポジトリ
	 */
	@Autowired
	public QualifiedOtTrainingHistoryService(QualifiedOtTrainingHistoryRepository qualifiedOtTrainingHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.qualifiedOtTrainingHistoryRep = qualifiedOtTrainingHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public QualifiedOtTrainingHistoryDto getSearchResult(Long id) {
		return QualifiedOtTrainingHistoryDto.create().setEntity(qualifiedOtTrainingHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public QualifiedOtTrainingHistoryDto getSearchResultByCondition(QualifiedOtTrainingHistorySearchDto searchDto) {
		return QualifiedOtTrainingHistoryDto.create()
				.setEntities(qualifiedOtTrainingHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(QualifiedOtTrainingHistoryDto dto) {
		insertEntity(qualifiedOtTrainingHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(QualifiedOtTrainingHistoryDto dto) {
		updateEntity(qualifiedOtTrainingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO

}