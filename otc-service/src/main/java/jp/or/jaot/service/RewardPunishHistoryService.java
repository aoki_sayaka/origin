package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.RewardPunishHistoryRepository;
import jp.or.jaot.model.dto.RewardPunishHistoryDto;
import jp.or.jaot.model.dto.search.RewardPunishHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 賞罰履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class RewardPunishHistoryService extends BaseService {

	/** 賞罰履歴リポジトリ */
	private final RewardPunishHistoryRepository rewardPunishHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param rewardPunishHistoryRep リポジトリ
	 */
	@Autowired
	public RewardPunishHistoryService(RewardPunishHistoryRepository rewardPunishHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.rewardPunishHistoryRep = rewardPunishHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public RewardPunishHistoryDto getSearchResultByCondition(RewardPunishHistorySearchDto searchDto) {
		return RewardPunishHistoryDto.create()
				.setEntities(rewardPunishHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(RewardPunishHistoryDto dto) {
		insertEntity(rewardPunishHistoryRep, dto.getFirstEntity());
	}

	// TODO
}
