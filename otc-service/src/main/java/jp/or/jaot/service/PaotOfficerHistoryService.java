package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.PaotOfficerHistoryRepository;
import jp.or.jaot.model.dto.PaotOfficerHistoryDto;
import jp.or.jaot.model.dto.search.PaotOfficerHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 県士会役員履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class PaotOfficerHistoryService extends BaseService {

	/** 県士会役員履歴リポジトリ */
	private final PaotOfficerHistoryRepository paotOfficerHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param paotOfficerHistoryRep リポジトリ
	 */
	@Autowired
	public PaotOfficerHistoryService(PaotOfficerHistoryRepository paotOfficerHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.paotOfficerHistoryRep = paotOfficerHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public PaotOfficerHistoryDto getSearchResultByCondition(PaotOfficerHistorySearchDto searchDto) {
		return PaotOfficerHistoryDto.create().setEntities(paotOfficerHistoryRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
