package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MtdlpCaseRepository;
import jp.or.jaot.model.dto.MtdlpCaseDto;
import jp.or.jaot.model.dto.search.MtdlpCaseSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * MTDLP事例サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MtdlpCaseService extends BaseService {

	/** MTDLP事例リポジトリ */
	private final MtdlpCaseRepository mtdlpCaseRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param mtdlpCaseRep リポジトリ
	 */
	@Autowired
	public MtdlpCaseService(MtdlpCaseRepository mtdlpCaseRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.mtdlpCaseRep = mtdlpCaseRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MtdlpCaseDto getSearchResult(Long id) {
		return MtdlpCaseDto.create().setEntity(mtdlpCaseRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MtdlpCaseDto getSearchResultByCondition(MtdlpCaseSearchDto searchDto) {
		return MtdlpCaseDto.create()
				.setEntities(mtdlpCaseRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MtdlpCaseDto dto) {
		insertEntity(mtdlpCaseRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(MtdlpCaseDto dto) {
		updateEntity(mtdlpCaseRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}
}
