package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.PeriodExtensionApplicationRepository;
import jp.or.jaot.model.dto.PeriodExtensionApplicationDto;
import jp.or.jaot.model.dto.search.PeriodExtensionApplicationSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 期間延長申請サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class PeriodExtensionApplicationService extends BaseService {

	/** 事例報告リポジトリ */
	private final PeriodExtensionApplicationRepository periodExtensionApplicationRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param periodExtensionApplicationRep リポジトリ
	 */
	@Autowired
	public PeriodExtensionApplicationService(PeriodExtensionApplicationRepository periodExtensionApplicationRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.periodExtensionApplicationRep = periodExtensionApplicationRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public PeriodExtensionApplicationDto getSearchResultByCondition(PeriodExtensionApplicationSearchDto searchDto) {
		return PeriodExtensionApplicationDto.create()
				.setEntities(periodExtensionApplicationRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
