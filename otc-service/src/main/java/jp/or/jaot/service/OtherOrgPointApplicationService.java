package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberRepository;
import jp.or.jaot.domain.repository.OtherOrgPointApplicationAttachmentRepository;
import jp.or.jaot.domain.repository.OtherOrgPointApplicationRepository;
import jp.or.jaot.model.dto.OtherOrgPointApplicationDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.dto.search.OtherOrgPointApplicationSearchDto;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;
import jp.or.jaot.model.entity.composite.OtherOrgPointApplicationCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 他団体・SIGポイント申請サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class OtherOrgPointApplicationService extends BaseService {

	/** 会員リポジトリ */
	@Autowired
	private MemberRepository memberRep;

	/** 他団体・SIGポイント申請リポジトリ */
	@Autowired
	private OtherOrgPointApplicationRepository otherOrgPointApplicationRep;

	/** 他団体・SIGポイント添付ファイルリポジトリ */
	@Autowired
	private OtherOrgPointApplicationAttachmentRepository otherOrgPointApplicationAttachmentRep;


	/**
	 * コンストラクタ(インジェクション)
	 * @param otherOrgPointApplicationRep リポジトリ
	 */
	@Autowired
	public OtherOrgPointApplicationService(OtherOrgPointApplicationRepository otherOrgPointApplicationRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.otherOrgPointApplicationRep = otherOrgPointApplicationRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public OtherOrgPointApplicationDto getSearchResult(Long id) {
		return OtherOrgPointApplicationDto.create()
				.setEntity(createCompositeEntity(otherOrgPointApplicationRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	public OtherOrgPointApplicationDto getSearchResultByOtherOrgPointApplicationNo(Integer memberNo) {
		return OtherOrgPointApplicationDto.create()
				.setEntity(createCompositeEntity(otherOrgPointApplicationRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public OtherOrgPointApplicationDto getSearchResultByCondition(OtherOrgPointApplicationSearchDto searchDto) {

		return OtherOrgPointApplicationDto.create()
				.setEntities(createCompositeEntities(otherOrgPointApplicationRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(OtherOrgPointApplicationDto dto) {
		// 他団体・SIGポイント申請エンティティ(複合)
		OtherOrgPointApplicationCompositeEntity compositeEntity = dto.getFirstEntity();

		OtherOrgPointApplicationEntity otherOrgPointApplicationEntity = compositeEntity
				.getOtherOrgPointApplicationEntity();
		logger.debug("他団体・SIGポイント申請検索1={}", otherOrgPointApplicationRep);
		logger.debug("他団体・SIGポイント申請検索2={}", otherOrgPointApplicationEntity);
		logger.debug("他団体・SIGポイント申請検索3={}", otherOrgPointApplicationEntity.getId());
		// 他団体・SIGポイント申請
		insertEntity(otherOrgPointApplicationRep, compositeEntity.getOtherOrgPointApplicationEntity());

		// 基礎研修修了認定更新履歴
		compositeEntity.getOtherOrgPointApplicationAttachmentEntities().stream()
				.forEach(E -> insertEntity(otherOrgPointApplicationAttachmentRep, E));

	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(OtherOrgPointApplicationDto dto) {

		// 他団体・SIGポイント申請エンティティ(複合)
		OtherOrgPointApplicationCompositeEntity compositeEntity = dto.getFirstEntity();

		// 他団体・SIGポイント申請
		OtherOrgPointApplicationEntity otherOrgPointApplicationEntity = compositeEntity
				.getOtherOrgPointApplicationEntity();
		//		logger.debug("他団体・SIGポイント申請検索1={}",otherOrgPointApplicationRep);
		//		logger.debug("他団体・SIGポイント申請検索2={}",otherOrgPointApplicationEntity);
		//		logger.debug("他団体・SIGポイント申請検索3={}",otherOrgPointApplicationEntity.getId());
		updateEntity(otherOrgPointApplicationRep, otherOrgPointApplicationEntity, otherOrgPointApplicationEntity.getId());

		// 他団体・SIGポイント添付ファイル
		for (OtherOrgPointApplicationAttachmentEntity entity : compositeEntity.getOtherOrgPointApplicationAttachmentEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				insertEntity(otherOrgPointApplicationAttachmentRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(otherOrgPointApplicationAttachmentRep, entity, id); // 削除(物理)
				} else {
					updateEntity(otherOrgPointApplicationAttachmentRep, entity, id); // 更新
				}
			}
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param otherOrgPointApplicationEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private OtherOrgPointApplicationCompositeEntity createCompositeEntity(
			OtherOrgPointApplicationEntity otherOrgPointApplicationEntity) {

		// 他団体・SIGポイント申請エンティティ(複合)
		OtherOrgPointApplicationCompositeEntity entity = new OtherOrgPointApplicationCompositeEntity();

		// ID
		// long id = otherOrgPointApplicationEntity.getMemberNo().longValue();
		// logger.debug(id.toString());

		// 会員番号
		Integer memberNo = otherOrgPointApplicationEntity.getMemberNo();

		// 他団体・SIGポイント申請
		entity.setOtherOrgPointApplicationEntity(otherOrgPointApplicationEntity);
		
		// 会員情報
		entity.setMemberEntities(getMemberEntity(memberNo));

		// 基礎研修修了認定更新履歴
		/*
		try {
			entity.setOtherOrgPointApplicationAttachmentEntities(otherOrgPointApplicationAttachmentRep.getSearchResultById(id));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}
		*/

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param otherOrgPointApplicationEntity 他団体・SIGポイント申請エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<OtherOrgPointApplicationCompositeEntity> createCompositeEntities(
			List<OtherOrgPointApplicationEntity> otherOrgPointApplicationEntities) {
		return otherOrgPointApplicationEntities.stream().map(E -> createCompositeEntity(E)).collect(Collectors.toList());
	}

	/**
	 * 会員エンティティ取得
	 * @param memberNo 会員番号
	 * @return 会員エンティティ
	 */
	private List<MemberEntity> getMemberEntity(Integer memberNo) {

		// 会員情報検索DTO生成
		MemberSearchDto searchDto = new MemberSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号
		logger.debug("会員.会員番号={}", memberNo);

		// 会員情報取得
		List<MemberEntity> entities = memberRep.getSearchResultByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			return null; // 該当データなし
		} else if (entities.size() > 1) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("memberNo=%d, size=%d", memberNo, entities.size());
			String message = String.format("会員情報が2件以上存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		return entities;
	}
	// TODO

}
