package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ClinicalTrainingLeaderTrainingHistoryRepository;
import jp.or.jaot.model.dto.ClinicalTrainingLeaderTrainingHistoryDto;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderTrainingHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 臨床実習指導者研修受講履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ClinicalTrainingLeaderTrainingHistoryService extends BaseService {

	/** 臨床実習指導者研修受講履歴リポジトリ */
	private final ClinicalTrainingLeaderTrainingHistoryRepository clinicalTrainingLeaderTrainingHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtAttendingSummaryRep リポジトリ
	 */
	@Autowired
	public ClinicalTrainingLeaderTrainingHistoryService(
			ClinicalTrainingLeaderTrainingHistoryRepository clinicalTrainingLeaderTrainingHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.clinicalTrainingLeaderTrainingHistoryRep = clinicalTrainingLeaderTrainingHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public ClinicalTrainingLeaderTrainingHistoryDto getSearchResult(Long id) {
		return ClinicalTrainingLeaderTrainingHistoryDto.create()
				.setEntity(clinicalTrainingLeaderTrainingHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ClinicalTrainingLeaderTrainingHistoryDto getSearchResultByCondition(
			ClinicalTrainingLeaderTrainingHistorySearchDto searchDto) {
		return ClinicalTrainingLeaderTrainingHistoryDto.create()
				.setEntities(clinicalTrainingLeaderTrainingHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(ClinicalTrainingLeaderTrainingHistoryDto dto) {
		insertEntity(clinicalTrainingLeaderTrainingHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(ClinicalTrainingLeaderTrainingHistoryDto dto) {
		updateEntity(clinicalTrainingLeaderTrainingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

}
