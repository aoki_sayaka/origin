package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.RecessHistoryRepository;
import jp.or.jaot.model.dto.RecessHistoryDto;
import jp.or.jaot.model.dto.search.RecessHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 休会履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class RecessHistoryService extends BaseService {

	/** 休会履歴リポジトリ */
	private final RecessHistoryRepository recessHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param recessHistoryRep リポジトリ
	 */
	@Autowired
	public RecessHistoryService(RecessHistoryRepository recessHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.recessHistoryRep = recessHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public RecessHistoryDto getSearchResultByCondition(RecessHistorySearchDto searchDto) {
		return RecessHistoryDto.create().setEntities(recessHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(RecessHistoryDto dto) {
		insertEntity(recessHistoryRep, dto.getFirstEntity());
	}

	// TODO
}
