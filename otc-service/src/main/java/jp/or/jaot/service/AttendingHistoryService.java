package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.BasicOtAttendingSummaryRepository;
import jp.or.jaot.domain.repository.BasicOtQualifyHistoryRepository;
import jp.or.jaot.domain.repository.BasicOtTrainingHistoryRepository;
import jp.or.jaot.domain.repository.MemberLocalActivityRepository;
import jp.or.jaot.domain.repository.MemberQualificationRepository;
import jp.or.jaot.domain.repository.MemberRepository;
import jp.or.jaot.domain.repository.MemberWithdrawalHistoryRepository;
import jp.or.jaot.domain.repository.RecessHistoryRepository;
import jp.or.jaot.model.dto.AttendingHistoryDto;
import jp.or.jaot.model.dto.BasicOtAttendingSummaryDto;
import jp.or.jaot.model.dto.MemberDto;
import jp.or.jaot.model.dto.MemberWithdrawalHistoryDto;
import jp.or.jaot.model.dto.RecessHistoryDto;
import jp.or.jaot.model.dto.search.BasicOtAttendingSummarySearchDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.dto.search.MemberWithdrawalHistorySearchDto;
import jp.or.jaot.model.dto.search.RecessHistorySearchDto;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.composite.BasicOtAttendingSummaryCompositeEntity;
import jp.or.jaot.model.entity.composite.MemberCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 受講履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class AttendingHistoryService extends BaseService{

//	/** 受講履歴リポジトリ */
//	private final AttendingHistoryRepository attendingHistoryRep;

	/**会員リポジトリ */
	@Autowired
	private MemberRepository memberRep;

	/** 会員関連資格情報リポジトリ */
	@Autowired
	private MemberQualificationRepository memberQualificationRep;

	/** 会員自治体参画情報リポジトリ **/
	@Autowired
	private MemberLocalActivityRepository memberLocalActivityRep;

	/** 基礎研修履歴リポジトリ */
	@Autowired
	private BasicOtAttendingSummaryRepository basicOtAttendingSummaryRep;

	/** 基礎研修修了認定更新履歴リポジトリ */
	@Autowired
	private BasicOtQualifyHistoryRepository basicOtQualifyHistoryRep;

	/** 基礎研修受講履歴リポジトリ */
	@Autowired
	private BasicOtTrainingHistoryRepository basicOtTrainingHistoryRep;

	/** 休会履歴リポジトリ */
	@Autowired
	private RecessHistoryRepository recessHistoryRep;

	/** 退会履歴リポジトリ */
	@Autowired
	private MemberWithdrawalHistoryRepository memberWithdrawalHistoryRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public AttendingHistoryService() {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY);
	}

//	/**
//	 * 全件取得
//	 * @return DTO
//	 */
//	public MemberDto getAllResult() {
//		return MemberDto.create().setEntities(memberRep.getAllResult());
//	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MemberDto getSearchResult(Long id) {
		return MemberDto.create().setEntity(createMemberCompositeEntity(memberRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
//	public MemberDto getSearchResultByMemberNo(Integer memberNo) {
//		return MemberDto.create().setEntity(createMemberCompositeEntity(memberRep.getSearchResultByMemberNo(memberNo)));
//	}
	//会員テーブルのみ（あとで関連テーブル取得用を作成）
	public MemberDto getSearchResultByMemberNo(Integer memberNo) {
		return MemberDto.create().setEntity(createEntity(memberRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberDto getSearchResultByCondition(MemberSearchDto searchDto) {
		// [開発メモ] : 2019-07-30(rmatu)
		// 本メソッドは会員エンティティ毎に関連するテーブルを検索しており、取得件数を絞り込んだ条件下での使用を想定している
		// バッチ等で関連テーブル情報も一括で取得する場合には、パフォーマンスに影響がでる可能性が高いので別途検討すること
		return MemberDto.create().setEntities(createMenmerCompositeEntities(memberRep.getSearchResultByCondition(searchDto)));
	}

//	/**
//	 * 検索結果取得(キー)
//	 * @param id ID
//	 * @return DTO
//	 */
//	public AttendingHistoryDto getSearchResult(Long id) {
//		return AttendingHistoryDto.create().setEntity(attendingHistoryRep.getSearchResult(id));
//	}
//
//	/**
//	 * 検索結果取得(条件)
//	 * @param searchDto 検索DTO
//	 * @return DTO
//	 */
//	public AttendingHistoryDto getSearchResultByCondition(AttendingHistorySearchDto searchDto) {
//		return AttendingHistoryDto.create()
//				.setEntities(attendingHistoryRep.getSearchResultByCondition(searchDto));
//	}

	/**
	 * 基礎研修タブ
	 */

	/**
	 * 検索結果取得(キー)基礎研修タブ
	 * @param id ID
	 * @return DTO
	 */
	public BasicOtAttendingSummaryDto getSearchBasicOtResult(Long id) {
		return BasicOtAttendingSummaryDto.create().setEntity(createBasicCompositeEntity(basicOtAttendingSummaryRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(会員番号)基礎研修タブ
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	public BasicOtAttendingSummaryDto getSearchResultByBasicOtAttendingSummaryNo(Integer memberNo) {
		return BasicOtAttendingSummaryDto.create().setEntity(createBasicCompositeEntity(basicOtAttendingSummaryRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)基礎研修タブ
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public BasicOtAttendingSummaryDto getSearchResultBasicOtByCondition(BasicOtAttendingSummarySearchDto searchDto) {
		return BasicOtAttendingSummaryDto.create().setEntities(createBasicCompositeEntities(basicOtAttendingSummaryRep.getSearchResultByCondition(searchDto)));
	}

//	/**
//	 * 登録
//	 * @param dto DTO
//	 */
//	public void enter(AttendingHistoryDto dto) {
//		insertEntity(attendingHistoryRep, dto.getFirstEntity());
//	}

	/**
	 * 更新
	 * @param dto DTO
	 */
//	public void update(AttendingHistoryDto dto) {
//		updateEntity(attendingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
//	}
	public void update(AttendingHistoryDto dto , BasicOtAttendingSummaryDto basicOtDto) {

		// 受講履歴エンティティ(複合)
//		AttendingHistoryCompositeEntity AttendingHistoryEntity = dto.getFirstEntity();

		// 基礎研修履歴
		updateBasicOt(basicOtDto);

		// 基礎ポイント履歴
//		updatePoint(pointDto);

	}

	/**
	 * 更新(基礎研修タブ)
	 * @param dto DTO
	 */
	public void updateBasicOt(BasicOtAttendingSummaryDto dto) {

		// 基礎研修履歴エンティティ(複合)
		BasicOtAttendingSummaryCompositeEntity compositeEntity = dto.getFirstEntity();

		// 基礎研修履歴
		BasicOtAttendingSummaryEntity basicOtAttendingSummaryEntity = compositeEntity.getBasicOtAttendingSummaryEntity();
		updateEntity(basicOtAttendingSummaryRep, basicOtAttendingSummaryEntity, basicOtAttendingSummaryEntity.getId());

		// 基礎研修修了認定更新履歴
		for (BasicOtQualifyHistoryEntity entity : compositeEntity.getBasicOtQualifyHistoryEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				insertEntity(basicOtQualifyHistoryRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(basicOtQualifyHistoryRep, entity, id); // 削除(物理)
				} else {
					updateEntity(basicOtQualifyHistoryRep, entity, id); // 更新
				}
			}
		}

		// 基礎研修受講履歴
		for (BasicOtTrainingHistoryEntity entity : compositeEntity.getBasicOtTrainingHistoryEntities()) {
			updateEntity(basicOtTrainingHistoryRep, entity, entity.getId()); // 更新
		}
	}

//	/**
//	 * 更新(基礎ポイントタブ)
//	 * @param dto DTO
//	 */
//	public void updatePoint(BasicPointTrainingHistoryDto dto) {
//		updateEntity(basicPointTrainingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
//	}
//
	/**
	 * 複合エンティティ生成(基礎研修タブ）
	 * @param memberEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private BasicOtAttendingSummaryCompositeEntity createBasicCompositeEntity(BasicOtAttendingSummaryEntity basicOtAttendingSummaryEntity) {

		// 基礎研修履歴エンティティ(複合)
		BasicOtAttendingSummaryCompositeEntity entity = new BasicOtAttendingSummaryCompositeEntity();

		// 会員番号
		Integer memberNo = basicOtAttendingSummaryEntity.getMemberNo();

		// 基礎研修履歴
		entity.setBasicOtAttendingSummaryEntity(basicOtAttendingSummaryEntity);

		// 基礎研修修了認定更新履歴
		try {
			entity.setBasicOtQualifyHistoryEntities(basicOtQualifyHistoryRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		//  基礎研修受講履歴
		try {
			entity.setBasicOtTrainingHistoryEntities(basicOtTrainingHistoryRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		return entity;
	}

	/**
	 * 複合エンティティリスト生成(基礎研修タブ）
	 * @param basicOtAttendingSummaryEntity 基礎研修履歴エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<BasicOtAttendingSummaryCompositeEntity> createBasicCompositeEntities(List<BasicOtAttendingSummaryEntity> basicOtAttendingSummaryEntities) {
		return basicOtAttendingSummaryEntities.stream().map(E -> createBasicCompositeEntity(E)).collect(Collectors.toList());
	}

	/**
	 * エンティティ生成
	 * @param memberEntity 会員エンティティ
	 * @return エンティティ
	 */
	private MemberCompositeEntity createEntity(MemberEntity memberEntity) {

		// 会員エンティティ
		MemberCompositeEntity entity = new MemberCompositeEntity();

		// 会員
		entity.setMemberEntity(memberEntity);

		return entity;
	}

	/**
	 * 複合エンティティ生成
	 * @param memberEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private MemberCompositeEntity createMemberCompositeEntity(MemberEntity memberEntity) {

		// 会員エンティティ(複合)
		MemberCompositeEntity entity = new MemberCompositeEntity();

		// 会員番号
		Integer memberNo = memberEntity.getMemberNo();

		// 会員
		entity.setMemberEntity(memberEntity);

		// 会員関連資格情報
		try {
			entity.setMemberQualificationEntities(memberQualificationRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		// 会員自治体参画情報
		try {
			entity.setMemberLocalActivityEntities(memberLocalActivityRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param memberEntity 会員エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<MemberCompositeEntity> createMenmerCompositeEntities(List<MemberEntity> memberEntities) {
		return memberEntities.stream().map(E -> createMemberCompositeEntity(E)).collect(Collectors.toList());
	}


	/**
	 * 登録
	 * @param dto DTO
	 */
//	public void enter(AttendingHistoryDto dto) {
//		insertEntity(attendingHistoryRep, dto.getFirstEntity());
//	}

	/**
	 * 更新
	 * @param dto DTO
	 */
//	public void update(AttendingHistoryDto dto) {
//		updateEntity(attendingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
//	}

	/**
	 * 検索結果取得(条件)休会履歴
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public RecessHistoryDto getSearchResultRecessByCondition(RecessHistorySearchDto searchDto) {
		return RecessHistoryDto.create().setEntities(recessHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 検索結果取得(条件)退会履歴
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberWithdrawalHistoryDto getSearchResultMemberWithdrawalByCondition(MemberWithdrawalHistorySearchDto searchDto) {
		return MemberWithdrawalHistoryDto.create().setEntities(memberWithdrawalHistoryRep.getSearchResultByCondition(searchDto));
	}

	// TODO


}
