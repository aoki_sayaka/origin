package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.FacilityApplicationCategoryRepository;
import jp.or.jaot.model.dto.FacilityApplicationCategoryDto;
import jp.or.jaot.model.dto.search.FacilityApplicationCategorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 施設登録申請分類サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class FacilityApplicationCategoryService extends BaseService {

	/** 施設登録申請分類リポジトリ */
	private final FacilityApplicationCategoryRepository facilityApplicationCategoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param facilityApplicationCategoryRep リポジトリ
	 */
	@Autowired
	public FacilityApplicationCategoryService(FacilityApplicationCategoryRepository facilityApplicationCategoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.facilityApplicationCategoryRep = facilityApplicationCategoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityApplicationCategoryDto getSearchResultByCondition(FacilityApplicationCategorySearchDto searchDto) {
		return FacilityApplicationCategoryDto.create()
				.setEntities(facilityApplicationCategoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(FacilityApplicationCategoryDto dto) {
		insertEntity(facilityApplicationCategoryRep, dto.getFirstEntity());
	}

	// TODO
}
