package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.TrainingOperatorRepository;
import jp.or.jaot.model.dto.TrainingOperatorDto;
import jp.or.jaot.model.dto.search.TrainingOperatorSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会運営担当者サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingOperatorService extends BaseService {

	/** 研修会運営担当者リポジトリ */
	private final TrainingOperatorRepository trainingOperatorRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingOperatorRep リポジトリ
	 */
	@Autowired
	public TrainingOperatorService(TrainingOperatorRepository trainingOperatorRep) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
		this.trainingOperatorRep = trainingOperatorRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingOperatorDto getSearchResultByCondition(TrainingOperatorSearchDto searchDto) {
		return TrainingOperatorDto.create().setEntities(trainingOperatorRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(TrainingOperatorDto dto) {
		insertEntity(trainingOperatorRep, dto.getFirstEntity());
	}

	// TODO
}
