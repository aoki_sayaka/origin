package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberLoginHistoryRepository;
import jp.or.jaot.model.dto.MemberLoginHistoryDto;
import jp.or.jaot.model.dto.search.MemberLoginHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員ポータルログイン履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberLoginHistoryService extends BaseService {

	/** 会員ポータルログイン履歴リポジトリ */
	private final MemberLoginHistoryRepository memberLoginHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberLoginHistoryRep リポジトリ
	 */
	@Autowired
	public MemberLoginHistoryService(MemberLoginHistoryRepository memberLoginHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberLoginHistoryRep = memberLoginHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberLoginHistoryDto getSearchResultByCondition(MemberLoginHistorySearchDto searchDto) {
		return MemberLoginHistoryDto.create().setEntities(memberLoginHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberLoginHistoryDto dto) {
		insertEntity(memberLoginHistoryRep, dto.getFirstEntity());
	}

	// TODO
}
