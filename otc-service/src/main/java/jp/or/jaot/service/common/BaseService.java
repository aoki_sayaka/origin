package jp.or.jaot.service.common;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.model.BaseEntity;
import jp.or.jaot.core.model.LoggingUpdateEntityDto;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.core.util.LoggingUtil;
import jp.or.jaot.domain.common.LoggingRepository;
import jp.or.jaot.domain.repository.HistoryFieldDefinitionRepository;
import jp.or.jaot.domain.repository.HistoryTableDefinitionRepository;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.UserEntity;

/**
 * サービスの基底クラス
 */
public abstract class BaseService {

	/** デフォルトユーザID */
	private static String DEF_DEFAULT_USER_ID = "ot_admin";

	/** ロガーインスタンス */
	protected final Logger logger;

	/** エラー発生箇所 */
	protected final ERROR_PLACE errorPlace;

	/** ログ情報リポジトリ */
	@Autowired
	protected LoggingRepository loggingRep;

	/** 履歴テーブル定義リポジトリ */
	@Autowired
	protected HistoryTableDefinitionRepository historyTableDefinitionRep;

	/** 履歴フィールド定義リポジトリ */
	@Autowired
	protected HistoryFieldDefinitionRepository historyFieldDefinitionRep;

	/**
	 * コンストラクタ
	 * @param errorPlace エラー発生個所
	 */
	public BaseService(ERROR_PLACE errorPlace) {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
		this.errorPlace = errorPlace;
	}

	/**
	 * エンティティ追加
	 * @param entity エンティティ
	 * @param dto DTO
	 * @return 追加したエンティティ
	 */
	protected BaseEntity insertEntity(BaseRepository<?, ?> rep, BaseEntity entity) {

		// 属性設定
		entity.setCreateUser(getAuthenticationUserId()); // 作成者
		entity.setUpdateUser(getAuthenticationUserId()); // 更新者

		// 実行
		BaseEntity baseEntity = rep.insertEntity(entity);
		LoggingUpdateEntityDto loggingDto = LoggingUtil.getLoggingUpdateEntityDto(null, baseEntity);

		// エンティティログ出力(追加)
		loggingRep.loggingEntity(
				historyTableDefinitionRep.getCacheResult(), historyFieldDefinitionRep.getCacheResult(),
				LoggingUpdateEntityDto.CATEGORY.ADD, baseEntity, loggingDto);

		return baseEntity;
	}

	/**
	 * エンティティ更新
	 * @param rep リポジトリ
	 * @param entity エンティティ
	 * @param primaryKey プライマリキーの値
	 * @return 更新したエンティティ
	 */
	protected BaseEntity updateEntity(BaseRepository<?, ?> rep, BaseEntity entity, Long primaryKey) {

		// 属性設定
		entity.setUpdateUser(getAuthenticationUserId()); // 更新者

		// 実行
		LoggingUpdateEntityDto loggingDto = rep.updateEntity(entity, primaryKey);

		// エンティティログ出力(変更)
		loggingRep.loggingEntity(
				historyTableDefinitionRep.getCacheResult(), historyFieldDefinitionRep.getCacheResult(),
				LoggingUpdateEntityDto.CATEGORY.MOD, entity, loggingDto);

		return (BaseEntity) rep.find(primaryKey, false); // 最新エンティティ取得(排他ロックなし)
	}

	/**
	 * エンティティ削除(物理)
	 * @param rep リポジトリ
	 * @param entity エンティティ
	 * @param primaryKey プライマリキーの値
	 */
	protected void deleteEntity(BaseRepository<?, ?> rep, BaseEntity entity, Long primaryKey) {

		// 実行
		rep.deleteEntity(entity, primaryKey);
		LoggingUpdateEntityDto loggingDto = LoggingUtil.getLoggingUpdateEntityDto(entity, null);

		// エンティティログ出力(削除)
		loggingRep.loggingEntity(
				historyTableDefinitionRep.getCacheResult(), historyFieldDefinitionRep.getCacheResult(),
				LoggingUpdateEntityDto.CATEGORY.DEL, entity, loggingDto);
	}

	/**
	 * 追加
	 * @param rep リポジトリ
	 * @param entity エンティティ
	 */
	protected void insert(BaseRepository<?, ?> rep, BaseEntity entity) {

		// 属性設定
		entity.setCreateUser(getAuthenticationUserId()); // 作成者
		entity.setUpdateUser(getAuthenticationUserId()); // 更新者

		// 実行
		rep.insert(entity);
	}

	/**
	 * 更新
	 * @param rep リポジトリ
	 * @param entity エンティティ
	 */
	protected void update(BaseRepository<?, ?> rep, BaseEntity entity) {

		// 属性設定
		entity.setUpdateUser(getAuthenticationUserId()); // 更新者

		// 実行
		rep.update(entity);
	}

	/**
	 * 削除(物理)
	 * @param rep リポジトリ
	 * @param entity エンティティ
	 */
	protected void delete(BaseRepository<?, ?> rep, BaseEntity entity) {

		// 実行
		rep.delete(entity);
	}

	/**
	 * 認証済みユーザ詳細情報取得
	 * @return ユーザ詳細情報
	 */
	protected UserDetails getAuthenticationUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null) {
			Object principal = authentication.getPrincipal();
			if (principal instanceof UserDetails) {
				return (UserDetails) principal;
			} else {
				return null;
			}
		}
		return null;
	}

	/**
	 * 認証済みユーザID取得
	 * @return ユーザID
	 */
	protected String getAuthenticationUserId() {
		UserDetails userDetails = null;
		if ((userDetails = getAuthenticationUser()) != null) {
			if (userDetails instanceof MemberEntity) {
				// 会員
				return String.valueOf(((MemberEntity) userDetails).getMemberNo()); // 会員番号
			} else if (userDetails instanceof UserEntity) {
				// 事務局ユーザ
				return ((UserEntity) userDetails).getLoginId(); // ログインID
			}
		}
		return DEF_DEFAULT_USER_ID;
	}

	/**
	 * 認証済み会員番号取得
	 * @return 会員番号
	 */
	protected Integer getAuthenticationMemberNo() {
		UserDetails userDetails = null;
		if ((userDetails = getAuthenticationUser()) != null) {
			if (userDetails instanceof MemberEntity) {
				return ((MemberEntity) userDetails).getMemberNo(); // 会員番号
			}
		}
		return null;
	}

	/**
	 * 認証済みログインID取得
	 * @return ログインID
	 */
	protected String getAuthenticationLoginId() {
		UserDetails userDetails = null;
		if ((userDetails = getAuthenticationUser()) != null) {
			if (userDetails instanceof UserEntity) {
				return ((UserEntity) userDetails).getLoginId(); // ログインID
			}
		}
		return null;
	}

	/**
	 * 配列生成(Long)
	 * @param value 値
	 * @return Long配列(値がnullの場合はnullを返却)
	 */
	protected Long[] createArray(Long value) {
		return (value == null) ? (null) : (new Long[] { value });
	}

	/**
	 * 配列生成(Integer)
	 * @param value 値
	 * @return Integer配列(値がnullの場合はnullを返却)
	 */
	protected Integer[] createArray(Integer value) {
		return (value == null) ? (null) : (new Integer[] { value });
	}

	/**
	 * 配列生成(String)
	 * @param value 値
	 * @return String配列(値がnullの場合はnullを返却)
	 */
	protected String[] createArray(String value) {
		return (value == null) ? (null) : (new String[] { value });
	}

	/**
	 * ログフォーマット文字列取得(Long)
	 * @param values 値配列
	 * @return ログ出力文字列(カンマ区切り)
	 */
	protected String getLogFormatText(Long[] values) {
		return Arrays.stream(values).map(E -> E.toString()).collect(Collectors.joining(","));
	}

	/**
	 * ログフォーマット文字列取得(Integer)
	 * @param values 値配列
	 * @return ログ出力文字列(カンマ区切り)
	 */
	protected String getLogFormatText(Integer[] values) {
		return Arrays.stream(values).map(E -> E.toString()).collect(Collectors.joining(","));
	}

	/**
	 * ログフォーマット文字列取得(String)
	 * @param values 値配列
	 * @return ログ出力文字列(カンマ区切り)
	 */
	protected String getLogFormatText(String[] values) {
		return Arrays.stream(values).map(E -> E.toString()).collect(Collectors.joining(","));
	}

}
