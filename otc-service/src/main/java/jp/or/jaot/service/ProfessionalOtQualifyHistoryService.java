package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ProfessionalOtQualifyHistoryRepository;
import jp.or.jaot.model.dto.ProfessionalOtQualifyHistoryDto;
import jp.or.jaot.model.dto.search.ProfessionalOtQualifyHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 専門作業療法士認定更新履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ProfessionalOtQualifyHistoryService extends BaseService {

	/** 専門作業療法士認定更新履歴リポジトリ */
	private final ProfessionalOtQualifyHistoryRepository professionalOtQualifyHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param professionalOtQualifyHistoryRep リポジトリ
	 */
	@Autowired
	public ProfessionalOtQualifyHistoryService(ProfessionalOtQualifyHistoryRepository professionalOtQualifyHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.professionalOtQualifyHistoryRep = professionalOtQualifyHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public ProfessionalOtQualifyHistoryDto getSearchResult(Long id) {
		return ProfessionalOtQualifyHistoryDto.create().setEntity(professionalOtQualifyHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ProfessionalOtQualifyHistoryDto getSearchResultByCondition(ProfessionalOtQualifyHistorySearchDto searchDto) {
		return ProfessionalOtQualifyHistoryDto.create()
				.setEntities(professionalOtQualifyHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(ProfessionalOtQualifyHistoryDto dto) {
		insertEntity(professionalOtQualifyHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(ProfessionalOtQualifyHistoryDto dto) {
		updateEntity(professionalOtQualifyHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO

}
