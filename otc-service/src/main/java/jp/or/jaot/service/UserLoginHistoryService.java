package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.UserLoginHistoryRepository;
import jp.or.jaot.model.dto.UserLoginHistoryDto;
import jp.or.jaot.model.dto.search.UserLoginHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 事務局サイトログイン履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class UserLoginHistoryService extends BaseService {

	/** 事務局サイトログイン履歴リポジトリ */
	private final UserLoginHistoryRepository userLoginHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param userLoginHistoryRep リポジトリ
	 */
	@Autowired
	public UserLoginHistoryService(UserLoginHistoryRepository userLoginHistoryRep) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
		this.userLoginHistoryRep = userLoginHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public UserLoginHistoryDto getSearchResultByCondition(UserLoginHistorySearchDto searchDto) {
		return UserLoginHistoryDto.create().setEntities(userLoginHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(UserLoginHistoryDto dto) {
		insertEntity(userLoginHistoryRep, dto.getFirstEntity());
	}

}
