package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.model.BaseEntity.ORDER_ASC;
import jp.or.jaot.core.model.BaseSearchDto.SearchOrder;
import jp.or.jaot.core.util.BeanUtil;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberRepository;
import jp.or.jaot.model.dto.TrainingMemberDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.dto.search.TrainingMemberSearchDto;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.composite.TrainingMemberCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会会員サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingMemberService extends BaseService {

	/** 会員リポジトリ */
	@Autowired
	private MemberRepository memberRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public TrainingMemberService() {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingMemberDto getSearchResultListByCondition(TrainingMemberSearchDto searchDto) {

		// 会員検索条件生成
		MemberSearchDto memberSearchDto = new MemberSearchDto();
		BeanUtil.copyProperties(searchDto, memberSearchDto, false);
		memberSearchDto.getOrders().add(new SearchOrder(ORDER_ASC.DESC, "memberNo")); // 降順:会員番号

		// 検索
		List<TrainingMemberCompositeEntity> entities = createCompositeEntities(
				memberRep.getSearchResultByCondition(memberSearchDto));

		// 検索結果再設定
		searchDto.setResultCount(memberSearchDto.getResultCount()); // 検索結果の件数
		searchDto.setAllResultCount(memberSearchDto.getAllResultCount()); // 検索結果の件数(全体)

		return TrainingMemberDto.create().setEntities(entities);
	}

	/**
	 * 複合エンティティ生成
	 * @param memberEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private TrainingMemberCompositeEntity createCompositeEntity(MemberEntity memberEntity) {

		// 研修会会員エンティティ(複合)
		TrainingMemberCompositeEntity compositeEntity = new TrainingMemberCompositeEntity();

		// 会員
		compositeEntity.setMemberEntity(memberEntity);

		// TODO : 関連テーブルの処理

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 会員エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<TrainingMemberCompositeEntity> createCompositeEntities(List<MemberEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<TrainingMemberCompositeEntity>(); // 空リスト
		}

		// TODO : 関連テーブルの処理

		return entities.stream().map(E -> createCompositeEntity(E)).collect(Collectors.toList());
	}

	// TODO
}
