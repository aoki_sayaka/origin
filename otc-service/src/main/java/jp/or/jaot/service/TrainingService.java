package jp.or.jaot.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.model.BaseEntity.ORDER_ASC;
import jp.or.jaot.core.model.BaseSearchDto.MATCH_CONDITION;
import jp.or.jaot.core.model.BaseSearchDto.SearchOrder;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.TrainingApplicationRepository;
import jp.or.jaot.domain.repository.TrainingInstructorRepository;
import jp.or.jaot.domain.repository.TrainingOperatorRepository;
import jp.or.jaot.domain.repository.TrainingOrganizerRepository;
import jp.or.jaot.domain.repository.TrainingRepository;
import jp.or.jaot.model.dto.TrainingDto;
import jp.or.jaot.model.dto.search.TrainingSearchDto;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import jp.or.jaot.model.entity.TrainingInstructorEntity;
import jp.or.jaot.model.entity.TrainingOperatorEntity;
import jp.or.jaot.model.entity.TrainingOrganizerEntity;
import jp.or.jaot.model.entity.composite.TrainingCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingService extends BaseService {

	/** 研修会番号枝番最大値 */
	private int DEF_MAX_TRAINING_NO_REV = 99;

	/** 主催者コード初期値 */
	private String DEF_ORGANIZER_CD = "00"; // OT協会

	/** 研修会リポジトリ */
	@Autowired
	private TrainingRepository trainingRep;

	/** 研修会申込リポジトリ[子] */
	@Autowired
	private TrainingApplicationRepository trainingApplicationRep;

	/** 研修会主催者リポジトリ[子] */
	@Autowired
	private TrainingOrganizerRepository trainingOrganizerRep;

	/** 研修会講師実績リポジトリ[子] */
	@Autowired
	private TrainingInstructorRepository trainingInstructorRep;

	/** 研修会運営担当者リポジトリ[子] */
	@Autowired
	private TrainingOperatorRepository trainingOperatorRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public TrainingService() {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public TrainingDto getSearchResult(Long id) {
		return TrainingDto.create().setEntity(createCompositeEntity(trainingRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingDto getSearchResultByCondition(TrainingSearchDto searchDto) {
		if (!ArrayUtils.isEmpty(searchDto.getIds())) {
			// ID指定あり
			return TrainingDto.create()
					.setEntities(createCompositeEntities(trainingRep.getSearchResult(searchDto.getIds())));
		} else {
			// ID指定なし
			return TrainingDto.create()
					.setEntities(createCompositeEntities(trainingRep.getSearchResultByCondition(searchDto)));
		}
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingDto getSearchResultListByCondition(TrainingSearchDto searchDto) {
		return TrainingDto.create()
				.setEntities(createCompositeEntities(trainingRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(TrainingDto dto) {

		TrainingCompositeEntity compositeEntity = dto.getFirstEntity();

		// 研修会
		TrainingEntity trainingEntity = compositeEntity.getTrainingEntity();

		// 研修会主催者
		List<TrainingOrganizerEntity> trainingOrganizerEntities = compositeEntity.getTrainingOrganizerEntities();
		if (CollectionUtils.isEmpty(trainingOrganizerEntities)) {
			TrainingOrganizerEntity entity = new TrainingOrganizerEntity();
			entity.setOrganizerCd(DEF_ORGANIZER_CD); // 初期値
			trainingOrganizerEntities.add(entity);
		}

		// 研修会番号設定
		String trainingNo = trainingEntity.getTrainingNo();
		String createTrainingNo = trainingNo;
		if (StringUtils.isEmpty(trainingNo)) {

			// 研修会番号生成(検索用)
			String fiscalYear = (trainingEntity.getHoldFiscalYear() == null) ? ""
					: String.valueOf(trainingEntity.getHoldFiscalYear()); // 年度
			String organizerCd = trainingOrganizerEntities.stream()
					.map(E -> E.getOrganizerCd()).findFirst().orElse(DEF_ORGANIZER_CD); // 主催者コード(先頭)
			String courseCategoryCd = StringUtils.defaultString(trainingEntity.getCourseCategoryCd()); // 講座分類
			String searchTrainingNo = String.format("%s%s%s", fiscalYear, organizerCd, courseCategoryCd);

			// 最大枝番取得
			int rev = getMaxTrainingNoRev(searchTrainingNo) + 1; // 最大値 + 1
			if (rev > DEF_MAX_TRAINING_NO_REV) {
				ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
				String detail = String.format("searchTrainingNo=%s", searchTrainingNo);
				String message = String.format("研修会番号の枝番が制限値を超えています : 最大=%d", DEF_MAX_TRAINING_NO_REV);
				throw new SearchBusinessException(errorCause, errorPlace, detail, message);
			}

			// 研修会番号生成(登録用)
			String revStr = String.format("%0" + String.valueOf(DEF_MAX_TRAINING_NO_REV).length() + "d", rev); // 枝番
			createTrainingNo = String.format("%s%s", searchTrainingNo, revStr);
			trainingEntity.setTrainingNo(createTrainingNo);
			logger.debug("研修会番号(年度={}, 主催者コード={}, 講座分類={}, 枝番={}) : {}",
					fiscalYear, organizerCd, courseCategoryCd, revStr, createTrainingNo);
		}

		// 更新情報
		trainingEntity.setTrainingUpdateUser(getAuthenticationLoginId()); // 研修会更新者名
		trainingEntity.setTrainingUpdateDatetime(new Timestamp(System.currentTimeMillis())); // 研修会更新日時(システム現在日時)

		// 研修会登録
		insertEntity(trainingRep, trainingEntity);

		// 研修会主催者登録
		for (int seqNo = 1; seqNo <= trainingOrganizerEntities.size(); seqNo++) {
			TrainingOrganizerEntity entity = trainingOrganizerEntities.get(seqNo - 1);
			entity.setSeqNo(seqNo); // 連番
			entity.setTrainingNo(createTrainingNo); // 研修会番号
			insertEntity(trainingOrganizerRep, entity);
		}

		// 研修会講師実績登録
		for (TrainingInstructorEntity entity : compositeEntity.getTrainingInstructorEntities()) {
			entity.setId(null); // ID初期化
			entity.setTrainingNo(createTrainingNo); // 研修会番号
			insertEntity(trainingInstructorRep, entity);
		}

		// 研修会運営担当者登録
		for (TrainingOperatorEntity entity : compositeEntity.getTrainingOperatorEntities()) {
			entity.setId(null); // ID初期化
			entity.setTrainingNo(createTrainingNo); // 研修会番号
			insertEntity(trainingOperatorRep, entity);
		}
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(TrainingDto dto) {

		// 研修会エンティティ(複合)
		TrainingCompositeEntity compositeEntity = dto.getFirstEntity();

		// 研修会
		TrainingEntity trainingEntity = compositeEntity.getTrainingEntity();
		trainingEntity.setTrainingUpdateUser(getAuthenticationLoginId()); // 研修会更新者名
		trainingEntity.setTrainingUpdateDatetime(new Timestamp(System.currentTimeMillis())); // 研修会更新日時(システム現在日時)
		updateEntity(trainingRep, trainingEntity, trainingEntity.getId());

		// 研修会講師実績
		for (TrainingInstructorEntity entity : compositeEntity.getTrainingInstructorEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				if (BooleanUtils.isFalse(entity.getDeleted())) {
					entity.setTrainingNo(trainingEntity.getTrainingNo()); // 研修会番号
					insertEntity(trainingInstructorRep, entity); // 追加(削除時は対象外)
				}
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(trainingInstructorRep, entity, id); // 削除(物理)
				} else {
					updateEntity(trainingInstructorRep, entity, id); // 更新
				}
			}
		}

		// 研修会運営担当者
		for (TrainingOperatorEntity entity : compositeEntity.getTrainingOperatorEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				if (BooleanUtils.isFalse(entity.getDeleted())) {
					entity.setTrainingNo(trainingEntity.getTrainingNo()); // 研修会番号
					insertEntity(trainingOperatorRep, entity); // 追加(削除時は対象外)
				}
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(trainingOperatorRep, entity, id); // 削除(物理)
				} else {
					updateEntity(trainingOperatorRep, entity, id); // 更新
				}
			}
		}
	}

	/**
	 * 状態設定
	 * @param dto DTO
	 * @param enabled 状態(true=使用可能, false=使用不可)
	 */
	public void setEnable(TrainingDto dto, boolean enabled) {

		// 研修会エンティティ(複合)
		TrainingCompositeEntity compositeEntity = dto.getFirstEntity();

		// 研修会
		trainingRep.setEnable(compositeEntity.getTrainingEntity().getId(), getAuthenticationUserId(), enabled);

		// 研修会講師実績
		compositeEntity.getTrainingInstructorEntities().stream()
				.forEach(E -> trainingInstructorRep.setEnable(E.getId(), getAuthenticationUserId(), enabled));

		// 研修会運営担当者
		compositeEntity.getTrainingOperatorEntities().stream()
				.forEach(E -> trainingOperatorRep.setEnable(E.getId(), getAuthenticationUserId(), enabled));
	}

	/**
	 * 研修会番号最大枝番取得
	 * @param searchTrainingNo 検索研修会番号
	 * @return 最大枝番(末尾n桁)
	 */
	private int getMaxTrainingNoRev(String searchTrainingNo) {

		// 検索条件
		TrainingSearchDto searchDto = new TrainingSearchDto();
		searchDto.setMaxResult(1); // 取得件数(1件)
		searchDto.getOrders().add(new SearchOrder(ORDER_ASC.DESC, "trainingNo")); // 降順:研修会番号
		searchDto.setUseDeleted(false); // 削除フラグ不使用(論理削除されたデータも対象)
		searchDto.setTrainingNo(searchTrainingNo); // 研修会番号
		searchDto.setMatchCondTrainingNo(MATCH_CONDITION.FWD); // 研修会番号(前方一致)

		// 検索結果取得
		List<TrainingEntity> entities = trainingRep.getSearchResultByCondition(searchDto);
		if (!CollectionUtils.isEmpty(entities)) {
			String no = entities.get(0).getTrainingNo(); // 最大要素(1件)
			String rev = no.substring(no.length() - String.valueOf(DEF_MAX_TRAINING_NO_REV).length()); // 末尾n桁
			return NumberUtils.isNumber(rev) ? Integer.parseInt(rev) : (0);
		}

		return 0;
	}

	/**
	 * 複合エンティティ生成
	 * @param trainingEntity 研修会エンティティ
	 * @return 複合エンティティ
	 */
	private TrainingCompositeEntity createCompositeEntity(TrainingEntity trainingEntity) {
		String trainingNos[] = createArray(trainingEntity.getTrainingNo());
		return createCompositeEntity(trainingEntity,
				trainingApplicationRep.getSearchResultByTrainingNos(trainingNos),
				trainingOrganizerRep.getSearchResultByTrainingNos(trainingNos),
				trainingInstructorRep.getSearchResultByTrainingNos(trainingNos),
				trainingOperatorRep.getSearchResultByTrainingNos(trainingNos));
	}

	/**
	 * 複合エンティティ生成
	 * @param trainingEntity 研修会エンティティ
	 * @param trainingApplicationEntities 研修会申込リスト
	 * @param trainingOrganizerEntities 研修会主催者リスト
	 * @param trainingInstructorEntities 研修会講師実績リスト
	 * @param trainingOperatorEntities 研修会運営担当者リスト
	 * @return 複合エンティティ
	 */
	private TrainingCompositeEntity createCompositeEntity(TrainingEntity trainingEntity,
			List<TrainingApplicationEntity> trainingApplicationEntities,
			List<TrainingOrganizerEntity> trainingOrganizerEntities,
			List<TrainingInstructorEntity> trainingInstructorEntities,
			List<TrainingOperatorEntity> trainingOperatorEntities) {

		// 研修会エンティティ(複合)
		TrainingCompositeEntity compositeEntity = new TrainingCompositeEntity();

		// 研修会番号
		String trainingNo = trainingEntity.getTrainingNo();

		// 研修会
		compositeEntity.setTrainingEntity(trainingEntity);

		// 研修会申込[子]
		compositeEntity.setTrainingApplicationEntities(trainingApplicationEntities.stream()
				.filter(E -> E.getTrainingNo().equals(trainingNo)).collect(Collectors.toList()));

		// 研修会主催者[子]
		compositeEntity.setTrainingOrganizerEntities(trainingOrganizerEntities.stream()
				.filter(E -> E.getTrainingNo().equals(trainingNo)).collect(Collectors.toList()));

		// 研修会講師実績[子]
		compositeEntity.setTrainingInstructorEntities(trainingInstructorEntities.stream()
				.filter(E -> E.getTrainingNo().equals(trainingNo)).collect(Collectors.toList()));

		// 研修会運営担当者[子]
		compositeEntity.setTrainingOperatorEntities(trainingOperatorEntities.stream()
				.filter(E -> E.getTrainingNo().equals(trainingNo)).collect(Collectors.toList()));

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 研修会エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<TrainingCompositeEntity> createCompositeEntities(List<TrainingEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<TrainingCompositeEntity>(); // 空リスト
		}

		// 研修会番号配列取得
		String[] trainingNos = entities.stream().map(E -> E.getTrainingNo()).toArray(String[]::new);
		logger.debug("研修会番号[{}]: {}", trainingNos.length, getLogFormatText(trainingNos));

		// 研修会申込取得(一括)
		List<TrainingApplicationEntity> applicationEntities = trainingApplicationRep
				.getSearchResultByTrainingNos(trainingNos);

		// 研修会主催者取得(一括)
		List<TrainingOrganizerEntity> organizerEntities = trainingOrganizerRep
				.getSearchResultByTrainingNos(trainingNos);

		// 研修会講師実績取得(一括)
		List<TrainingInstructorEntity> instructorEntities = trainingInstructorRep
				.getSearchResultByTrainingNos(trainingNos);

		// 研修会運営担当者取得(一括)
		List<TrainingOperatorEntity> operatorEntities = trainingOperatorRep
				.getSearchResultByTrainingNos(trainingNos);

		return entities.stream()
				.map(E -> createCompositeEntity(E,
						applicationEntities, organizerEntities, instructorEntities, operatorEntities))
				.collect(Collectors.toList());
	}

	// TODO
}
