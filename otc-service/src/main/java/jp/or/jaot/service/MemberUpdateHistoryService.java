package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberUpdateHistoryRepository;
import jp.or.jaot.model.dto.MemberUpdateHistoryDto;
import jp.or.jaot.model.dto.search.MemberUpdateHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員更新履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberUpdateHistoryService extends BaseService {

	/** 会員更新履歴リポジトリ */
	private final MemberUpdateHistoryRepository memberUpdateHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberUpdateHistoryRep リポジトリ
	 */
	@Autowired
	public MemberUpdateHistoryService(MemberUpdateHistoryRepository memberUpdateHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberUpdateHistoryRep = memberUpdateHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberUpdateHistoryDto getSearchResultByCondition(MemberUpdateHistorySearchDto searchDto) {
		return MemberUpdateHistoryDto.create()
				.setEntities(memberUpdateHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberUpdateHistoryDto dto) {
		insertEntity(memberUpdateHistoryRep, dto.getFirstEntity());
	}

	// TODO
}
