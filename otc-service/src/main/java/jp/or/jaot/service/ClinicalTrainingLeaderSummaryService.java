package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ClinicalTrainingLeaderQualifyHistoryRepository;
import jp.or.jaot.domain.repository.ClinicalTrainingLeaderSummaryRepository;
import jp.or.jaot.domain.repository.ClinicalTrainingLeaderTrainingHistoryRepository;
import jp.or.jaot.model.dto.ClinicalTrainingLeaderSummaryDto;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderSummarySearchDto;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderQualifyHistoryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;
import jp.or.jaot.model.entity.composite.ClinicalTrainingLeaderSummaryCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 臨床実習指導者履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ClinicalTrainingLeaderSummaryService extends BaseService {

	/** 臨床実習指導者履歴リポジトリ*/
	@Autowired
	private ClinicalTrainingLeaderSummaryRepository clinicalTrainingLeaderSummaryRep;

	/** 臨床実習指導者認定履歴リポジトリ */
	@Autowired
	private ClinicalTrainingLeaderQualifyHistoryRepository clinicalTrainingLeaderQualifyHistoryRep;

	/** 臨床実習指導者研修受講履歴リポジトリ **/
	@Autowired
	private ClinicalTrainingLeaderTrainingHistoryRepository clinicalTrainingLeaderTrainingHistoryRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public ClinicalTrainingLeaderSummaryService(
			ClinicalTrainingLeaderSummaryRepository clinicalTrainingLeaderSummaryRepository) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.clinicalTrainingLeaderSummaryRep = clinicalTrainingLeaderSummaryRepository;
	}

	/**
	 *
	 * @param id
	 * @return DTO
	 */
	public ClinicalTrainingLeaderSummaryDto getSearchResult(Long id) {
		return ClinicalTrainingLeaderSummaryDto.create()
				.setEntity(createCompositeEntity(clinicalTrainingLeaderSummaryRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	public ClinicalTrainingLeaderSummaryDto getSearchResultByMemberNo(Integer memberNo) {
		return ClinicalTrainingLeaderSummaryDto.create().setEntity(
				createCompositeEntity(clinicalTrainingLeaderSummaryRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ClinicalTrainingLeaderSummaryDto getSearchResultByCondition(
			ClinicalTrainingLeaderSummarySearchDto searchDto) {
		return ClinicalTrainingLeaderSummaryDto.create().setEntities(
				createCompositeEntities(clinicalTrainingLeaderSummaryRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(ClinicalTrainingLeaderSummaryDto dto) {

		//  臨床実習指導者履歴エンティティ(複合)
		ClinicalTrainingLeaderSummaryCompositeEntity compositeEntity = dto.getFirstEntity();

		// 会員
		ClinicalTrainingLeaderSummaryEntity memberEntity = compositeEntity.getClinicalTrainingLeaderSummaryEntity();
		updateEntity(clinicalTrainingLeaderSummaryRep, memberEntity, memberEntity.getId());

		//  臨床実習指導者履歴
		for (ClinicalTrainingLeaderQualifyHistoryEntity entity : compositeEntity
				.getClinicalTrainingLeaderQualifyHistoryEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				insertEntity(clinicalTrainingLeaderQualifyHistoryRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(clinicalTrainingLeaderQualifyHistoryRep, entity, id); // 削除(物理)
				} else {
					updateEntity(clinicalTrainingLeaderQualifyHistoryRep, entity, id); // 更新
				}
			}
		}

		// 臨床実習指導者認定履歴
		for (ClinicalTrainingLeaderTrainingHistoryEntity entity : compositeEntity
				.getClinicalTrainingLeaderTrainingHistoryEntities()) {
			updateEntity(clinicalTrainingLeaderTrainingHistoryRep, entity, entity.getId()); // 更新
		}

	}

	/**
	 * 複合エンティティ生成
	 * @param memberEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private ClinicalTrainingLeaderSummaryCompositeEntity createCompositeEntity(
			ClinicalTrainingLeaderSummaryEntity clinicalTrainingLeaderSummaryEntity) {

		// 臨床実習指導者履歴エンティティ(複合)
		ClinicalTrainingLeaderSummaryCompositeEntity entity = new ClinicalTrainingLeaderSummaryCompositeEntity();

		// 会員番号
		Integer memberNo = clinicalTrainingLeaderSummaryEntity.getMemberNo();

		// 臨床実習指導者履歴
		entity.setClinicalTrainingLeaderSummaryEntity(clinicalTrainingLeaderSummaryEntity);

		// 会員関連資格情報
		try {
			entity.setClinicalTrainingLeaderQualifyHistoryEntities(
					clinicalTrainingLeaderQualifyHistoryRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		// 臨床実習指導者研修受講履歴
		try {
			entity.setClinicalTrainingLeaderTrainingHistoryEntities(
					clinicalTrainingLeaderTrainingHistoryRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param memberEntity 臨床実習指導者履歴エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<ClinicalTrainingLeaderSummaryCompositeEntity> createCompositeEntities(
			List<ClinicalTrainingLeaderSummaryEntity> clinicalTrainingLeaderSummaryEntities) {
		return clinicalTrainingLeaderSummaryEntities.stream().map(E -> createCompositeEntity(E))
				.collect(Collectors.toList());
	}

	// TODO
}
