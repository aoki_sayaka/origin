package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.UserRepository;
import jp.or.jaot.model.dto.UserDto;
import jp.or.jaot.model.dto.search.UserSearchDto;
import jp.or.jaot.model.entity.UserEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 事務局ユーザサービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class UserService extends BaseService {

	/** 事務局ユーザリポジトリ */
	@Autowired
	private UserRepository userRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public UserService() {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public UserDto getSearchResult(Long id) {
		return UserDto.create().setEntity(userRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(ログインID)
	 * @param loginId ログインID
	 * @return DTO
	 */
	public UserDto getSearchResultByLoginId(String loginId) {
		return UserDto.create().setEntity(userRep.getSearchResultByLoginId(loginId));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public UserDto getSearchResultByCondition(UserSearchDto searchDto) {
		return UserDto.create().setEntities(userRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 利用可能状態取得(パス/キー値)
	 * @param id ID
	 * @param pathOrKey パス/キー値
	 * @return 利用可能=true, 利用不可=false
	 */
	public boolean isAvailablePathOrKey(Long id, String pathOrKey) {
		return userRep.isAvailablePathOrKey(id, pathOrKey);
	}

	/**
	 * 入会
	 * @param dto DTO
	 */
	public void enter(UserDto dto) {

		// 属性再設定
		UserEntity userEntity = dto.getFirstEntity();

		// 追加
		insertEntity(userRep, userEntity);
	}

	/**
	 * 更新
	 * @param dto 事務局ユーザDTO
	 */
	public void update(UserDto dto) {
		// 事務局ユーザエンティティ
		UserEntity userEntity = dto.getFirstEntity();
		updateEntity(userRep, userEntity, userEntity.getId());
	}

	/**
	 * 事務局ユーザの削除処理
	 * 論理削除を行う
	 * @param id
	 */
	public void disable(Long id) {
		userRep.setEnable(id, getAuthenticationUserId(), false);
	}
}
