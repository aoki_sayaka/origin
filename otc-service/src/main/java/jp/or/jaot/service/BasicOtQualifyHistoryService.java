package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.BasicOtQualifyHistoryRepository;
import jp.or.jaot.model.dto.BasicOtQualifyHistoryDto;
import jp.or.jaot.model.dto.search.BasicOtQualifyHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 基礎研修修了認定更新履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class BasicOtQualifyHistoryService extends BaseService {

	/** 基礎研修修了認定更新履歴リポジトリ */
	private final BasicOtQualifyHistoryRepository basicOtQualifyHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtQualifyHistoryRep リポジトリ
	 */
	@Autowired
	public BasicOtQualifyHistoryService(BasicOtQualifyHistoryRepository basicOtQualifyHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.basicOtQualifyHistoryRep = basicOtQualifyHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public BasicOtQualifyHistoryDto getSearchResult(Long id) {
		return BasicOtQualifyHistoryDto.create().setEntity(basicOtQualifyHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public BasicOtQualifyHistoryDto getSearchResultByCondition(BasicOtQualifyHistorySearchDto searchDto) {
		return BasicOtQualifyHistoryDto.create()
				.setEntities(basicOtQualifyHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(BasicOtQualifyHistoryDto dto) {
		insertEntity(basicOtQualifyHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(BasicOtQualifyHistoryDto dto) {
		updateEntity(basicOtQualifyHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO

}
