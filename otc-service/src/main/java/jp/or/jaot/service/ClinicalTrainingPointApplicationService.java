package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ClinicalTrainingPointApplicationRepository;
import jp.or.jaot.domain.repository.FacilityRepository;
import jp.or.jaot.domain.repository.MemberRepository;
import jp.or.jaot.model.dto.ClinicalTrainingPointApplicationDto;
import jp.or.jaot.model.dto.search.ClinicalTrainingPointApplicationSearchDto;
import jp.or.jaot.model.dto.search.FacilityPortalSearchDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.entity.ClinicalTrainingPointApplicationEntity;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.composite.ClinicalTrainingPointApplicationCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 臨床実習ポイント申請サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ClinicalTrainingPointApplicationService extends BaseService {

	/** 臨床実習ポイント申請リポジトリ */
	@Autowired
	private ClinicalTrainingPointApplicationRepository clinicalTrainingPointApplicationRep;

	/** 会員リポジトリ */
	@Autowired
	private MemberRepository memberRep;
	
	/** 養成校リポジトリ */
	@Autowired
	private FacilityRepository facilityRep;
	
	/**
	 * コンストラクタ
	 */
	//TODO ERROR_PLACEが未定義
	@Autowired
	public ClinicalTrainingPointApplicationService(ClinicalTrainingPointApplicationRepository clinicalTrainingPointApplicationRep) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
		this.clinicalTrainingPointApplicationRep = clinicalTrainingPointApplicationRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ClinicalTrainingPointApplicationDto getSearchResultByCondition(ClinicalTrainingPointApplicationSearchDto searchDto) {
		return ClinicalTrainingPointApplicationDto.create()
				.setEntities(createCompositeEntities(clinicalTrainingPointApplicationRep.getSearchResultByCondition(searchDto)));
	}
	
	/**
	 * 複合エンティティリスト生成
	 * @param clinicalTrainingPointApplicationEntity 他団体・SIGポイント申請エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<ClinicalTrainingPointApplicationCompositeEntity> createCompositeEntities(
			List<ClinicalTrainingPointApplicationEntity> clinicalTrainingPointApplicationEntities) {
		return clinicalTrainingPointApplicationEntities.stream().map(E -> createCompositeEntity(E)).collect(Collectors.toList());
	}

	/**
	 * 複合エンティティ生成
	 * @param clinicalTrainingPointApplicationEntity 他団体・SIGポイント申請エンティティ
	 * @return 複合エンティティ
	 */
	private ClinicalTrainingPointApplicationCompositeEntity createCompositeEntity(
			ClinicalTrainingPointApplicationEntity clinicalTrainingPointApplicationEntity) {

		// 他団体・SIGポイント申請エンティティ(複合)
		ClinicalTrainingPointApplicationCompositeEntity entity = new ClinicalTrainingPointApplicationCompositeEntity();

		// 会員番号
		Integer memberNo = clinicalTrainingPointApplicationEntity.getApplicationMemberNo();

		// 養成校番号
		String facilityNo = clinicalTrainingPointApplicationEntity.getFacilityNo();
				
		// 他団体・SIGポイント申請
		entity.setClinicalTrainingPointApplicationEntity(clinicalTrainingPointApplicationEntity);
		
		// 会員情報
		entity.setMemberEntities(getMemberEntity(memberNo));
		
		// 養成校情報
		entity.setFacilityEntities(getFacilityEntity(facilityNo));

		return entity;
	}
	
	/**
	 * 会員エンティティ取得
	 * @param memberNo 会員番号
	 * @return 会員エンティティ
	 */
	private List<MemberEntity> getMemberEntity(Integer memberNo) {

		// 会員情報検索DTO生成
		MemberSearchDto searchDto = new MemberSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号
		logger.debug("会員.会員番号={}", memberNo);

		// 会員情報取得
		List<MemberEntity> entities = memberRep.getSearchResultByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			return null; // 該当データなし
		} else if (entities.size() > 1) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("memberNo=%d, size=%d", memberNo, entities.size());
			String message = String.format("会員情報が2件以上存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		return entities;
	}
	
	/**
	 * 養成校エンティティ取得
	 * @param facilityNo 養成校番号
	 * @return 養成校エンティティ
	 */
	private List<FacilityEntity> getFacilityEntity(String facilityNo) {

		// 養成校情報検索DTO生成
		FacilityPortalSearchDto searchDto = new FacilityPortalSearchDto();
		searchDto.setFacilityNo(facilityNo); // 会員番号
		logger.debug("養成校.養成校番号={}", facilityNo);

		// 養成校情報取得
		List<FacilityEntity> entities = facilityRep.getSearchResultByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			return null; // 該当データなし
		} else if (entities.size() > 1) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("facilityNo=%d, size=%d", facilityNo, entities.size());
			String message = String.format("会員情報が2件以上存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		return entities;
	}
}
