package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.AuthorPresenterActualFieldRepository;
import jp.or.jaot.model.dto.AuthorPresenterActualFieldDto;
import jp.or.jaot.model.dto.search.AuthorPresenterActualFieldSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 活動実績の領域(発表・著作等)サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class AuthorPresenterActualFieldService extends BaseService {

	/** 活動実績の領域(発表・著作等)リポジトリ */
	private final AuthorPresenterActualFieldRepository authorPresenterActualFieldRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param authorPresenterActualFieldRep リポジトリ
	 */
	@Autowired
	public AuthorPresenterActualFieldService(AuthorPresenterActualFieldRepository authorPresenterActualFieldRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.authorPresenterActualFieldRep = authorPresenterActualFieldRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public AuthorPresenterActualFieldDto getSearchResult(Long id) {
		return AuthorPresenterActualFieldDto.create()
				.setEntity(authorPresenterActualFieldRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public AuthorPresenterActualFieldDto getSearchResultByCondition(AuthorPresenterActualFieldSearchDto searchDto) {
		return AuthorPresenterActualFieldDto.create()
				.setEntities(authorPresenterActualFieldRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(AuthorPresenterActualFieldDto dto) {
		insertEntity(authorPresenterActualFieldRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(AuthorPresenterActualFieldDto dto) {
		updateEntity(authorPresenterActualFieldRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
}
