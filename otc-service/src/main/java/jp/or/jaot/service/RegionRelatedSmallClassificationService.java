package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.RegionRelatedSmallClassificationRepository;
import jp.or.jaot.model.dto.RegionRelatedSmallClassificationDto;
import jp.or.jaot.model.dto.search.RegionRelatedSmallClassificationSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 領域関連小分類サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class RegionRelatedSmallClassificationService extends BaseService {

	/** 領域関連小分類リポジトリ */
	private final RegionRelatedSmallClassificationRepository regionRelatedSmallClassificationRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param regionRelatedSmallClassificationRep リポジトリ
	 */
	@Autowired
	public RegionRelatedSmallClassificationService(
			RegionRelatedSmallClassificationRepository regionRelatedSmallClassificationRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.regionRelatedSmallClassificationRep = regionRelatedSmallClassificationRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public RegionRelatedSmallClassificationDto getSearchResultByCondition(
			RegionRelatedSmallClassificationSearchDto searchDto) {
		return RegionRelatedSmallClassificationDto.create()
				.setEntities(regionRelatedSmallClassificationRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
