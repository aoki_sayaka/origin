package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.TrainingApplicationRepository;
import jp.or.jaot.domain.repository.TrainingAttendanceResultRepository;
import jp.or.jaot.domain.repository.TrainingReportRepository;
import jp.or.jaot.domain.repository.TrainingRepository;
import jp.or.jaot.model.dto.TrainingReportDto;
import jp.or.jaot.model.dto.search.TrainingReportSearchDto;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import jp.or.jaot.model.entity.TrainingReportEntity;
import jp.or.jaot.model.entity.composite.TrainingReportCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会報告書サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingReportService extends BaseService {

	/** 研修会報告書リポジトリ */
	@Autowired
	private TrainingReportRepository trainingReportRep;

	/** 研修会リポジトリ[親] */
	@Autowired
	private TrainingRepository trainingRep;

	/** 研修会申込リポジトリ[関連] */
	@Autowired
	private TrainingApplicationRepository trainingApplicationRep;

	/** 研修会出欠合否リポジトリ[関連] */
	@Autowired
	private TrainingAttendanceResultRepository trainingAttendanceResultRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public TrainingReportService() {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public TrainingReportDto getSearchResult(Long id) {
		return TrainingReportDto.create().setEntity(createCompositeEntity(trainingReportRep.getSearchResult(id),
				new ArrayList<TrainingEntity>(),
				new ArrayList<TrainingApplicationEntity>(),
				new ArrayList<TrainingAttendanceResultEntity>()));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingReportDto getSearchResultByCondition(TrainingReportSearchDto searchDto) {
		return TrainingReportDto.create()
				.setEntities(createCompositeEntities(trainingReportRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 検索結果単一項目取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingReportDto getSearchResultUnitItemByCondition(TrainingReportSearchDto searchDto) {

		// 研修会番号チェック
		if (searchDto.getTrainingNos().length == 0) {
			// 0件(指定なし)
			return TrainingReportDto.create();
		} else if (searchDto.getTrainingNos().length > 1) {
			// 2件以上
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("trainingNos=%s", StringUtils.join(searchDto.getTrainingNos(), ","));
			String message = String.format("複数の研修会番号が指定されています");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		// 研修会報告書取得
		List<TrainingReportEntity> entities = trainingReportRep.getSearchResultByCondition(searchDto);

		// 研修会番号設定(未登録時)
		if (CollectionUtils.isEmpty(entities)) {
			TrainingReportDto dto = TrainingReportDto.createSingle();
			TrainingReportCompositeEntity compositeEntity = dto.getFirstEntity();
			TrainingReportEntity entity = compositeEntity.getTrainingReportEntity();
			entity.setTrainingNo(Arrays.stream(searchDto.getTrainingNos()).findFirst().orElse(null)); // 先頭固定
			entities.add(entity);
		}

		return TrainingReportDto.create().setEntities(createCompositeEntities(entities));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(TrainingReportDto dto) {
		insertEntity(trainingReportRep, dto.getFirstEntity().getTrainingReportEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(TrainingReportDto dto) {

		// 研修会報告書エンティティ(複合)
		TrainingReportCompositeEntity compositeEntity = dto.getFirstEntity();

		// 研修会報告書
		TrainingReportEntity entity = compositeEntity.getTrainingReportEntity();
		TrainingReportEntity updateEntity = null;
		Long id = null;
		if ((id = entity.getId()) == null) {
			updateEntity = (TrainingReportEntity) insertEntity(trainingReportRep, entity); // 追加
		} else {
			updateEntity = (TrainingReportEntity) updateEntity(trainingReportRep, entity, id); // 更新
		}

		// 更新エンティティ設定
		compositeEntity.setTrainingReportEntity(updateEntity);
	}

	/**
	 * 複合エンティティ生成
	 * @param trainingReportEntity 研修会報告書エンティティ
	 * @param trainingEntities 研修会リスト
	 * @param trainingApplicationEntities 研修会申込リスト
	 * @param trainingAttendanceResultEntities 研修会出欠合否リスト
	 * @return 複合エンティティ
	 */
	private TrainingReportCompositeEntity createCompositeEntity(TrainingReportEntity trainingReportEntity,
			List<TrainingEntity> trainingEntities,
			List<TrainingApplicationEntity> trainingApplicationEntities,
			List<TrainingAttendanceResultEntity> trainingAttendanceResultEntities) {

		// 研修会エンティティ(複合)
		TrainingReportCompositeEntity compositeEntity = new TrainingReportCompositeEntity();

		// 研修会番号
		String trainingNo = trainingReportEntity.getTrainingNo();

		// 研修会報告書
		compositeEntity.setTrainingReportEntity(trainingReportEntity);

		// 研修会[親]
		TrainingEntity trainingEntity = trainingEntities.stream().filter(E -> E.getTrainingNo().equals(trainingNo))
				.findFirst().orElse(null);
		if (trainingEntity != null) {
			compositeEntity.setTrainingEntity(trainingEntity);
		}

		// 研修会申込[関連]
		compositeEntity.setTrainingApplicationEntities(trainingApplicationEntities.stream()
				.filter(E -> E.getTrainingNo().equals(trainingNo)).collect(Collectors.toList()));

		// 研修会出欠合否[関連]
		compositeEntity.setTrainingAttendanceResultEntities(trainingAttendanceResultEntities.stream()
				.filter(E -> E.getTrainingNo().equals(trainingNo)).collect(Collectors.toList()));

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 研修会報告書エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<TrainingReportCompositeEntity> createCompositeEntities(List<TrainingReportEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<TrainingReportCompositeEntity>(); // 空リスト
		}

		// 研修会番号配列取得
		String[] trainingNos = entities.stream().map(E -> E.getTrainingNo()).distinct().sorted().toArray(String[]::new);
		logger.debug("研修会番号[{}]: {}", trainingNos.length, getLogFormatText(trainingNos));

		// 研修会取得(一括)
		List<TrainingEntity> trainingEntities = trainingRep.getSearchResultByTrainingNos(trainingNos);

		// 研修会申込取得(一括)
		List<TrainingApplicationEntity> trainingApplicationEntities = trainingApplicationRep
				.getSearchResultByTrainingNos(trainingNos);

		// 研修会出欠合否取得(一括)
		List<TrainingAttendanceResultEntity> trainingAttendanceResultEntities = trainingAttendanceResultRep
				.getSearchResultByTrainingNos(trainingNos);

		return entities.stream().map(E -> createCompositeEntity(E,
				trainingEntities, trainingApplicationEntities, trainingAttendanceResultEntities))
				.collect(Collectors.toList());
	}

	// TODO
}
