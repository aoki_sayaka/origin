package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.model.dto.HistoryTableDefinitionDto;
import jp.or.jaot.model.dto.search.HistoryTableDefinitionSearchDto;
import jp.or.jaot.model.entity.HistoryTableDefinitionEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 履歴テーブル定義サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class HistoryTableDefinitionService extends BaseService {

	/**
	 * コンストラクタ
	 */
	@Autowired
	public HistoryTableDefinitionService() {
		super(ERROR_PLACE.LOGIC_COMMON);
	}

	/**
	 * キャッシュ結果取得
	 * @return エンティティリスト
	 */
	public List<HistoryTableDefinitionEntity> getCacheResult() {
		return historyTableDefinitionRep.getCacheResult();
	}

	/**
	 * キャッシュ結果取得
	 * @param isMember 会員名簿(true=含む, false=含まない)
	 * @param isAttend 受講履歴(true=含む, false=含まない)
	 * @return エンティティリスト
	 */
	public List<HistoryTableDefinitionEntity> getCacheResult(Boolean isMember, Boolean isAttend) {
		List<HistoryTableDefinitionEntity> cacheResult = historyTableDefinitionRep.getCacheResult();
		if (BooleanUtils.isTrue(isMember) && BooleanUtils.isFalse(isAttend)) {
			List<String> tables = loggingRep.getMembersUpdateTables();
			return cacheResult.stream().filter(E -> tables.contains(E.getTablePhysicalNm()))
					.collect(Collectors.toList()); // 会員名簿
		} else if (BooleanUtils.isFalse(isMember) && BooleanUtils.isTrue(isAttend)) {
			List<String> tables = loggingRep.getAttendUpdateTables();
			return cacheResult.stream().filter(E -> tables.contains(E.getTablePhysicalNm()))
					.collect(Collectors.toList()); // 受講履歴
		} else {
			return cacheResult; // 全て
		}
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public HistoryTableDefinitionDto getSearchResultByCondition(HistoryTableDefinitionSearchDto searchDto) {
		return HistoryTableDefinitionDto.create()
				.setEntities(historyTableDefinitionRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 * @return 追加したエンティティ
	 */
	public HistoryTableDefinitionEntity enter(HistoryTableDefinitionDto dto) {
		return (HistoryTableDefinitionEntity) insertEntity(historyTableDefinitionRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(HistoryTableDefinitionDto dto) {
		updateEntity(historyTableDefinitionRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	/**
	 * 削除
	 * @param dto DTO
	 */
	public void delete(HistoryTableDefinitionDto dto) {
		deleteEntity(historyTableDefinitionRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
}
