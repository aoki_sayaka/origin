package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.QualificationOtAttendingHistoryRepository;
import jp.or.jaot.model.dto.QualificationOtAttendingHistoryDto;
import jp.or.jaot.model.dto.search.QualificationOtAttendingHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 認定作業療法士研修受講履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class QualificationOtAttendingHistoryService extends BaseService{

	/** 受講履歴リポジトリ */
	private final QualificationOtAttendingHistoryRepository qualificationOtAttendingHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param qualificationOtAttendingHistoryRep リポジトリ
	 */
	@Autowired
	public QualificationOtAttendingHistoryService(QualificationOtAttendingHistoryRepository qualificationOtAttendingHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.qualificationOtAttendingHistoryRep = qualificationOtAttendingHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public QualificationOtAttendingHistoryDto getSearchResult(Long id) {
		return QualificationOtAttendingHistoryDto.create().setEntity(qualificationOtAttendingHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public QualificationOtAttendingHistoryDto getSearchResultByCondition(QualificationOtAttendingHistorySearchDto searchDto) {
		return QualificationOtAttendingHistoryDto.create()
				.setEntities(qualificationOtAttendingHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(QualificationOtAttendingHistoryDto dto) {
		updateEntity(qualificationOtAttendingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
	

}
