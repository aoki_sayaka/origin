package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MtdlpTrainingHistoryRepository;
import jp.or.jaot.model.dto.MtdlpTrainingHistoryDto;
import jp.or.jaot.model.dto.search.MtdlpTrainingHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * MTDLP研修受講履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MtdlpTrainingHistoryService extends BaseService {

	/** MTDLP研修受講履歴リポジトリ */
	private final MtdlpTrainingHistoryRepository mtdlpTrainingHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param mtdlpTrainingHistoryRep リポジトリ
	 */
	@Autowired
	public MtdlpTrainingHistoryService(MtdlpTrainingHistoryRepository mtdlpTrainingHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.mtdlpTrainingHistoryRep = mtdlpTrainingHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MtdlpTrainingHistoryDto getSearchResult(Long id) {
		return MtdlpTrainingHistoryDto.create().setEntity(mtdlpTrainingHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MtdlpTrainingHistoryDto getSearchResultByCondition(MtdlpTrainingHistorySearchDto searchDto) {
		return MtdlpTrainingHistoryDto.create()
				.setEntities(mtdlpTrainingHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MtdlpTrainingHistoryDto dto) {
		insertEntity(mtdlpTrainingHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(MtdlpTrainingHistoryDto dto) {
		updateEntity(mtdlpTrainingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

}
