package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.BasicOtAttendingSummaryRepository;
import jp.or.jaot.domain.repository.MemberRepository;
import jp.or.jaot.domain.repository.TrainingTempApplicationRepository;
import jp.or.jaot.model.dto.TrainingTempApplicationDto;
import jp.or.jaot.model.dto.search.TrainingTempApplicationSearchDto;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.TrainingTempApplicationEntity;
import jp.or.jaot.model.entity.composite.TrainingTempApplicationCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会仮申込サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingTempApplicationService extends BaseService {

	/** 研修会仮申込リポジトリ */
	@Autowired
	private TrainingTempApplicationRepository trainingTempApplicationRep;

	/** 会員リポジトリ[関連] */
	@Autowired
	private MemberRepository memberRep;

	/** 基礎研修履歴リポジトリ[関連] */
	@Autowired
	private BasicOtAttendingSummaryRepository basicOtAttendingSummaryRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public TrainingTempApplicationService() {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingTempApplicationDto getSearchResultByCondition(TrainingTempApplicationSearchDto searchDto) {
		return TrainingTempApplicationDto.create()
				.setEntities(createCompositeEntities(trainingTempApplicationRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingTempApplicationDto getSearchResultListByCondition(TrainingTempApplicationSearchDto searchDto) {
		return TrainingTempApplicationDto.create()
				.setEntities(createCompositeEntities(trainingTempApplicationRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(TrainingTempApplicationDto dto) {
		insertEntity(trainingTempApplicationRep, dto.getFirstEntity().getTrainingTempApplicationEntity());
	}

	/**
	 * 削除(物理)
	 * @param searchDto 検索DTO
	 */
	public void delete(TrainingTempApplicationSearchDto searchDto) {
		trainingTempApplicationRep.getSearchResult(searchDto.getIds()).stream()
				.forEach(E -> deleteEntity(trainingTempApplicationRep, E, E.getId()));
	}

	/**
	 * 複合エンティティ生成
	 * @param trainingTempApplicationEntity 研修会仮申込エンティティ
	 * @param memberEntities 会員リスト
	 * @param summaryEntities 基礎研修履歴リスト
	 * @return 複合エンティティ
	 */
	private TrainingTempApplicationCompositeEntity createCompositeEntity(
			TrainingTempApplicationEntity trainingTempApplicationEntity,
			List<MemberEntity> memberEntities, List<BasicOtAttendingSummaryEntity> summaryEntities) {

		// 研修会仮申込エンティティ(複合)
		TrainingTempApplicationCompositeEntity compositeEntity = new TrainingTempApplicationCompositeEntity();

		// 会員番号
		Integer memberNo = trainingTempApplicationEntity.getMemberNo();

		// 研修会仮申込
		compositeEntity.setTrainingTempApplicationEntity(trainingTempApplicationEntity);

		// 会員[関連]
		MemberEntity memberEntity = memberEntities.stream()
				.filter(E -> E.getMemberNo().equals(memberNo)).findFirst().orElse(null);
		if (memberEntity != null) {
			compositeEntity.setMemberEntity(memberEntity);
		}

		// 基礎研修履歴[関連]
		BasicOtAttendingSummaryEntity summaryEntity = summaryEntities.stream()
				.filter(E -> E.getMemberNo().equals(memberNo)).findFirst().orElse(null);
		if (summaryEntity != null) {
			compositeEntity.setBasicOtAttendingSummaryEntity(summaryEntity);
		}

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 研修会仮申込エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<TrainingTempApplicationCompositeEntity> createCompositeEntities(
			List<TrainingTempApplicationEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<TrainingTempApplicationCompositeEntity>(); // 空リスト
		}

		// 会員番号配列取得
		Integer[] memberNos = entities.stream().map(E -> E.getMemberNo()).distinct().sorted().toArray(Integer[]::new);
		logger.debug("会員番号[{}]: {}", memberNos.length, getLogFormatText(memberNos));

		// 会員取得(一括)
		List<MemberEntity> memberEntities = memberRep.getSearchResultByMemberNos(memberNos);

		// 基礎研修履歴(一括)
		List<BasicOtAttendingSummaryEntity> summaryEntities = basicOtAttendingSummaryRep
				.getSearchResultByMemberNos(memberNos);

		return entities.stream().map(E -> createCompositeEntity(E, memberEntities, summaryEntities))
				.collect(Collectors.toList());
	}

	// TODO
}
