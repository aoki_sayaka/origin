package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.UserRevokedAuthorityRepository;
import jp.or.jaot.model.dto.UserRevokedAuthorityDto;
import jp.or.jaot.model.dto.search.UserRevokedAuthoritySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 事務局権限サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class UserRevokedAuthorityService extends BaseService {

	/** 事務局権限リポジトリ */
	private final UserRevokedAuthorityRepository userRevokedAuthorityRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param userRevokedAuthorityRep リポジトリ
	 */
	@Autowired
	public UserRevokedAuthorityService(UserRevokedAuthorityRepository userRevokedAuthorityRep) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
		this.userRevokedAuthorityRep = userRevokedAuthorityRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public UserRevokedAuthorityDto getSearchResultByCondition(UserRevokedAuthoritySearchDto searchDto) {
		return UserRevokedAuthorityDto.create()
				.setEntities(userRevokedAuthorityRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(UserRevokedAuthorityDto dto) {
		insertEntity(userRevokedAuthorityRep, dto.getFirstEntity());
	}

	// TODO
}
