package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.FacilityApplicationCategoryDomain;
import jp.or.jaot.domain.FacilityApplicationDomain;
import jp.or.jaot.domain.FacilityCategoryDomain;
import jp.or.jaot.domain.FacilityCategoryDomain.CATEGORY_DIVISION;
import jp.or.jaot.domain.MemberDomain;
import jp.or.jaot.domain.MemberFacilityCategoryDomain.MEMBER_FACILITY_CATEGORY_DIVISION;
import jp.or.jaot.domain.repository.BasicOtAttendingSummaryRepository;
import jp.or.jaot.domain.repository.DskReceiptRepository;
import jp.or.jaot.domain.repository.FacilityApplicationCategoryRepository;
import jp.or.jaot.domain.repository.FacilityApplicationRepository;
import jp.or.jaot.domain.repository.FacilityCategoryRepository;
import jp.or.jaot.domain.repository.FacilityRepository;
import jp.or.jaot.domain.repository.LifelongEducationStatusRepository;
import jp.or.jaot.domain.repository.MemberFacilityCategoryRepository;
import jp.or.jaot.domain.repository.MemberLocalActivityRepository;
import jp.or.jaot.domain.repository.MemberLoginHistoryRepository;
import jp.or.jaot.domain.repository.MemberMailRepository;
import jp.or.jaot.domain.repository.MemberQualificationRepository;
import jp.or.jaot.domain.repository.MemberRepository;
import jp.or.jaot.domain.repository.MembershipFeeDeliveryRepository;
import jp.or.jaot.domain.repository.ProfessionalOtAttendingSummaryRepository;
import jp.or.jaot.domain.repository.QualifiedOtAttendingSummaryRepository;
import jp.or.jaot.domain.repository.RecessHistoryRepository;
import jp.or.jaot.model.dto.MemberDto;
import jp.or.jaot.model.dto.search.FacilityApplicationSearchDto;
import jp.or.jaot.model.dto.search.FacilityPortalSearchDto;
import jp.or.jaot.model.dto.search.LifelongEducationStatusSearchDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.dto.search.RecessHistorySearchDto;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.DskReceiptEntity;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;
import jp.or.jaot.model.entity.FacilityApplicationEntity;
import jp.or.jaot.model.entity.FacilityCategoryEntity;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.LifelongEducationStatusEntity;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberFacilityCategoryEntity;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;
import jp.or.jaot.model.entity.MemberLoginHistoryEntity;
import jp.or.jaot.model.entity.MemberPasswordHistoryEntity;
import jp.or.jaot.model.entity.MemberQualificationEntity;
import jp.or.jaot.model.entity.MembershipFeeDeliveryEntity;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.RecessHistoryEntity;
import jp.or.jaot.model.entity.composite.FacilityApplicationCompositeEntity;
import jp.or.jaot.model.entity.composite.FacilityCompositeEntity;
import jp.or.jaot.model.entity.composite.MemberCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberService extends BaseService {

	/** 会員リポジトリ */
	@Autowired
	private MemberRepository memberRep;

	/** 会員関連資格情報リポジトリ */
	@Autowired
	private MemberQualificationRepository memberQualificationRep;

	/** 会員ポータルログイン履歴リポジトリ */
	@Autowired
	private MemberLoginHistoryRepository memberLoginHistoryRep;

	/** 会員自治体参画情報リポジトリ **/
	@Autowired
	private MemberLocalActivityRepository memberLocalActivityRep;

	/** 施設登録申請リポジトリ */
	@Autowired
	private FacilityApplicationRepository facilityApplicationRep;

	/** 施設登録申請分類リポジトリ */
	@Autowired
	private FacilityApplicationCategoryRepository facilityApplicationCategoryRep;

	/** 施設養成校リポジトリ */
	@Autowired
	private FacilityRepository facilityRep;

	/** 施設分類リポジトリ */
	@Autowired
	private FacilityCategoryRepository facilityCategoryRep;

	/** 休会履歴リポジトリ */
	@Autowired
	private RecessHistoryRepository recessHistoryRep;

	/** 会員勤務先業務分類リポジトリ */
	@Autowired
	private MemberFacilityCategoryRepository memberFacilityCategoryRep;

	/** 生涯教育ステータスリポジトリ */
	@Autowired
	private LifelongEducationStatusRepository lifelongEducationStatusRep;

	/** パスワード再発行メール送信リポジトリ */
	@Autowired
	private MemberMailRepository memberMailRep;

	/** 基礎研修履歴リポジトリ */
	@Autowired
	private BasicOtAttendingSummaryRepository basicOtAttendingSummaryRep;

	/** 専門作業療法士受講履歴リポジトリ */
	@Autowired
	private ProfessionalOtAttendingSummaryRepository professionalOtAttendingSummaryRep;

	/** 認定作業療法士研修履歴リポジトリ */
	@Autowired
	private QualifiedOtAttendingSummaryRepository qualifiedOtAttendingSummaryRep;

	/** DSK領収リポジトリ */
	@Autowired
	private DskReceiptRepository DskReceiptRep;

	/** 会費納入リポジトリ */
	@Autowired
	private MembershipFeeDeliveryRepository MembershipFeeDeliveryRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public MemberService() {
		super(ERROR_PLACE.LOGIC_MEMBER);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MemberDto getSearchResult(Long id) {
		return MemberDto.create().setEntity(createCompositeEntity(memberRep.getSearchResult(id), true));
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	public MemberDto getSearchResultByMemberNo(Integer memberNo) {
		return MemberDto.create().setEntity(createCompositeEntity(memberRep.getSearchResultByMemberNo(memberNo), true));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberDto getSearchResultByCondition(MemberSearchDto searchDto) {
		// [開発メモ] : 2019-07-30(rmatu)
		// 本メソッドは会員エンティティ毎に関連するテーブルを検索しており、取得件数を絞り込んだ条件下での使用を想定している
		// バッチ等で関連テーブル情報も一括で取得する場合には、パフォーマンスに影響がでる可能性が高いので別途検討すること
		return MemberDto.create()
				.setEntities(createCompositeEntities(memberRep.getSearchResultByCondition(searchDto), true));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberDto getSearchResultListByCondition(MemberSearchDto searchDto) {
		return MemberDto.create()
				.setEntities(createCompositeEntities(memberRep.getSearchResultByCondition(searchDto), false));
	}

	/**
	 * 状態設定
	 * @param id ID
	 * @param enabled 状態(true=使用可能, false=使用不可)
	 */
	public void setEnable(Long id, boolean enabled) {
		memberRep.setEnable(id, getAuthenticationUserId(), enabled);
	}

	/**
	 * 利用可能状態取得(パス/キー値)
	 * @param memberNo 会員番号
	 * @param pathOrKey パス/キー値
	 * @return 利用可能=true, 利用不可=false
	 */
	public boolean isAvailablePathOrKey(Integer memberNo, String pathOrKey) {
		return memberRep.isAvailablePathOrKey(memberNo, pathOrKey);
	}

	/**
	 * 入会
	 * @param dto DTO
	 */
	public void enter(MemberDto dto) {

		// 会員エンティティ(複合)
		MemberCompositeEntity compositeEntity = dto.getFirstEntity();

		// 会員
		insertEntity(memberRep, compositeEntity.getMemberEntity());

		// 会員関連資格情報
		compositeEntity.getMemberQualificationEntities().stream().forEach(E -> insertEntity(memberQualificationRep, E));

		// 会員自治体参画情報
		compositeEntity.getMemberLocalActivityEntities().stream().forEach(E -> insertEntity(memberLocalActivityRep, E));

		// 施設登録申請(複合)
		FacilityApplicationCompositeEntity facilityCompositeEntity = null;
		FacilityApplicationEntity facilityEntity = null;
		if (((facilityCompositeEntity = compositeEntity.getFacilityApplicationEntity()) != null)
				&& ((facilityEntity = facilityCompositeEntity.getFacilityApplicationEntity()) != null)) {

			// 施設登録申請
			facilityEntity.setMemberCategoryCd(FacilityApplicationDomain.MEMBER_CATEGORY_CD.REGULAR.getValue()); // 申請者区分コード(正会員)
			facilityEntity.setMemberNo(compositeEntity.getMemberEntity().getMemberNo()); // 会員番号
			facilityEntity.setStatus(FacilityApplicationDomain.APPLICATION_STATUS.APPLYING.getValue()); // 申請ステータス(申請中)
			insertEntity(facilityApplicationRep, facilityEntity);

			// 施設登録申請分類
			for (FacilityApplicationCategoryEntity entity : facilityCompositeEntity
					.getFacilityApplicationCategoryEntities()) {
				entity.setApplicantId(facilityEntity.getId()); // 施設登録申請ID
				insertEntity(facilityApplicationCategoryRep, entity);
			}
		}
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(MemberDto dto) {

		// 会員エンティティ(複合)
		MemberCompositeEntity compositeEntity = dto.getFirstEntity();

		// 会員
		MemberEntity memberEntity = compositeEntity.getMemberEntity();
		updateEntity(memberRep, memberEntity, memberEntity.getId());

		// 会員関連資格情報
		for (MemberQualificationEntity entity : compositeEntity.getMemberQualificationEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				insertEntity(memberQualificationRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(memberQualificationRep, entity, id); // 削除(物理)
				} else {
					updateEntity(memberQualificationRep, entity, id); // 更新
				}
			}
		}

		// 会員自治体参画情報
		for (MemberLocalActivityEntity entity : compositeEntity.getMemberLocalActivityEntities()) {
			updateEntity(memberLocalActivityRep, entity, entity.getId()); // 更新
		}

		// 施設登録申請(複合)
		FacilityApplicationCompositeEntity facilityCompositeEntity = null;
		FacilityApplicationEntity facilityApplicationEntity = null;
		if (((facilityCompositeEntity = compositeEntity.getFacilityApplicationEntity()) != null)
				&& ((facilityApplicationEntity = facilityCompositeEntity.getFacilityApplicationEntity()) != null)) {

			// 施設登録申請
			Long id = null;
			if ((id = facilityApplicationEntity.getId()) == null) {
				facilityApplicationEntity
						.setMemberCategoryCd(FacilityApplicationDomain.MEMBER_CATEGORY_CD.REGULAR.getValue()); // 申請者区分コード(正会員)
				facilityApplicationEntity.setMemberNo(compositeEntity.getMemberEntity().getMemberNo()); // 会員番号
				facilityApplicationEntity.setStatus(FacilityApplicationDomain.APPLICATION_STATUS.APPLYING.getValue()); // 申請ステータス(申請中)
				insertEntity(facilityApplicationRep, facilityApplicationEntity); // 追加
			} else {
				updateEntity(facilityApplicationRep, facilityApplicationEntity, id); // 更新
			}

			// 施設登録申請分類
			for (FacilityApplicationCategoryEntity entity : facilityCompositeEntity
					.getFacilityApplicationCategoryEntities()) {
				if ((id = entity.getId()) == null) {
					entity.setApplicantId(facilityApplicationEntity.getId()); // 施設登録申請ID
					insertEntity(facilityApplicationCategoryRep, entity); // 追加
				} else {
					if (BooleanUtils.isTrue(entity.getDeleted())) {
						deleteEntity(facilityApplicationCategoryRep, entity, id); // 削除(物理)
					} else {
						updateEntity(facilityApplicationCategoryRep, entity, id); // 更新
					}
				}
			}
		}

		// 会員勤務先業務分類
		for (MemberFacilityCategoryEntity entity : compositeEntity.getMemberFacilityCategoryEntities()) {
			Long id = entity.getId();
			if (id == null) {
				insertEntity(memberFacilityCategoryRep, entity); // 追加
				continue;
			}

			if (BooleanUtils.isTrue(entity.getDeleted())) {
				deleteEntity(memberFacilityCategoryRep, entity, id); // 削除(物理)
			} else {
				updateEntity(memberFacilityCategoryRep, entity, id); // 更新
			}
		}

		// 申請状態ならば、登録しない会員勤務先業務分類を追加する
		MemberDomain memberDomain = memberRep.findDomain(memberEntity.getId(), false);
		memberDomain.copyValue(memberEntity);

		// 申請状態かどうか
		if (memberDomain.isWorkingFacilityApplicationSelected()) {
			List<FacilityApplicationCategoryEntity> facilityApplicationCategoryEntityList = facilityApplicationCategoryRep
					.getSearchResultByApplicantId(facilityApplicationEntity.getId());

			// 会員勤務先業務分類を施設登録申請分類に変換して登録する
			for (MemberFacilityCategoryEntity memberFacilityCategoryEntity : compositeEntity
					.getMemberFacilityCategoryEntities()) {

				// ループしている会員勤務先分類が施設登録申請分類に存在するかどうか？
				FacilityApplicationCategoryEntity registered = facilityApplicationCategoryEntityList.stream()
						.filter(facilityApplicationCategoryEntity -> {
							// ドメインロジックを必要とするのでドメインを作成
							FacilityApplicationCategoryDomain facilityApplicationCategoryDomain = facilityApplicationCategoryRep
									.findDomain(facilityApplicationCategoryEntity.getId(), false);

							//一致しているかどうか確認
							return facilityApplicationCategoryDomain.isSameFacilityCategory(
									memberDomain
											.findFacilityDomainCdByMemberFacilityCategory(memberFacilityCategoryEntity),
									facilityApplicationCategoryDomain.getEntity().getCategoryDivision(),
									facilityApplicationCategoryDomain.getEntity().getParentCode(),
									facilityApplicationCategoryDomain.getEntity().getParentCode());
						}).findFirst().orElse(null);

				// 見つからないならば登録する
				if (registered == null) {
					FacilityApplicationCategoryEntity enter = new FacilityApplicationCategoryEntity();
					enter.setApplicantId(facilityApplicationEntity.getId());
					enter.setCategory(
							memberDomain.findFacilityDomainCdByMemberFacilityCategory(memberFacilityCategoryEntity));
					enter.setCategoryDivision(CATEGORY_DIVISION.convertFromMemberFacilityCategoryDivision(
							MEMBER_FACILITY_CATEGORY_DIVISION
									.valueOf(memberFacilityCategoryEntity.getCategoryDivision()))
							.getValue());
					enter.setParentCode(memberFacilityCategoryEntity.getParentCode());
					enter.setCategoryCode(memberFacilityCategoryEntity.getCategoryCode());
					logger.debug("付帯情報追加:申請からの追加 " + enter);
					insertEntity(facilityApplicationCategoryRep, enter);
				}
			}
		} else {
			// 申請済みの施設を選択したならば、会員勤務先業務分類を施設分類に変換して登録する

			// 施設分類を取得する
			Long facilityId = Long.parseLong(memberEntity.getWorkingFacilityCd());
			List<FacilityCategoryDomain> facilityCategoryDomainList = facilityCategoryRep
					.getSearchResultDomainByFacilityId(facilityId);

			// 入力の会員勤務先業務分類をループさせる
			for (MemberFacilityCategoryEntity memberFacilityCategoryEntity : compositeEntity
					.getMemberFacilityCategoryEntities()) {
				// ループしている会員勤務先分類が施設分類に存在するかどうか？
				FacilityCategoryDomain registered = facilityCategoryDomainList.stream()
						.filter(facilityCategoryDomain -> {
							// 一致しているか確認
							return facilityCategoryDomain.isSameFacilityCategory(
									memberDomain
											.findFacilityDomainCdByMemberFacilityCategory(memberFacilityCategoryEntity),
									facilityCategoryDomain.getEntity().getCategoryDivision(),
									facilityCategoryDomain.getEntity().getParentCode(),
									facilityCategoryDomain.getEntity().getParentCode());
						}).findFirst().orElse(null);

				// 見つからないならば登録する
				if (registered == null) {
					FacilityCategoryEntity enter = new FacilityCategoryEntity();
					enter.setFacilityId(facilityId);
					enter.setCategory(
							memberDomain.findFacilityDomainCdByMemberFacilityCategory(memberFacilityCategoryEntity));
					enter.setCategoryDivision(CATEGORY_DIVISION.convertFromMemberFacilityCategoryDivision(
							MEMBER_FACILITY_CATEGORY_DIVISION
									.valueOf(memberFacilityCategoryEntity.getCategoryDivision()))
							.getValue());
					enter.setParentCode(memberFacilityCategoryEntity.getParentCode());
					enter.setCategoryCode(memberFacilityCategoryEntity.getCategoryCode());
					logger.debug("付帯情報追加:申請済みからの追加 " + enter);
					insertEntity(facilityCategoryRep, enter);
				}
			}
		}
	}

	/**
	 * パスワード再発行処理をする。本人であるかどうか確認し、本人であるならば、仮パスワード再発行処理をしてメールを送る
	 * @param searchDto 本人確認するための情報を入れてるDTO
	 * @return Integer 本人確認できた場合、会員番号を返す。そうでない場合はnullを返す
	 */
	public Integer passwordReissue(MemberSearchDto searchDto) {

		// 会員番号を検索
		MemberEntity memberEntity = getSearchResultByMemberCd(searchDto);

		// 本人でなければ、nullで返して終了
		if (memberEntity == null) {
			// 本人ではないとき
			return null;
		}

		// メール送信
		memberMailRep.sendPasswordReissueMail(memberEntity, createTempPassword(memberEntity));

		// 会員番号を返す(これをコントローラ側に返す) ※値が無かったらnull
		return memberEntity.getMemberNo();
	}

	/**
	 * 会員検索
	 * @param searchDto 検索DTO
	 * @return MemberEntity 会員エンティティ
	 */
	public MemberEntity getSearchResultByMemberCd(MemberSearchDto searchDto) {
		MemberDto memberResponse = MemberDto.create()
				.setEntity(createCompositeEntity(memberRep.getSearchResultByMemberCd(searchDto), false));

		// 検索結果DTOから会員番号を抽出する処理①
		List<MemberCompositeEntity> entities = memberResponse.getEntities();

		// 検索結果DTOから会員番号を抽出する処理②
		MemberCompositeEntity firstEntities = entities.get(0);

		// 確認用
		logger.debug("memberResponseの値: " + memberResponse);
		logger.debug("Entitiesの値: " + memberResponse.getEntities());

		// 初期化
		MemberEntity personConfirm = null;

		// 本人確認
		if (firstEntities == null) {
			// nullを返す
			personConfirm = null;
		} else {
			// 会員エンティティを返す
			personConfirm = firstEntities.getMemberEntity();
		}

		// 確認用
		logger.debug("personConfirmの値: " + personConfirm);

		// 会員エンティティを返す ("会員エンティティ" or "null")
		return personConfirm;
	}

	/**
	 * 仮パスワード生成して登録する処理
	 * @param Integer 会員番号
	 * @return MemberPasswordHistoryEntity 登録したパスワード履歴エンティティを返す
	 */
	public MemberPasswordHistoryEntity createTempPassword(MemberEntity memberEntity) {
		// 仮パスワード生成処理
		String provisionalPassword = RandomStringUtils.randomAlphanumeric(32);

		// 値抽出
		Integer memberNo = memberEntity.getMemberNo();
		String password = memberEntity.getPassword();

		MemberPasswordHistoryEntity resultEntity = new MemberPasswordHistoryEntity();
		resultEntity.setMemberNo(memberNo); // 会員番号
		resultEntity.setPassword(password); // パスワード
		resultEntity.setTempPassword(provisionalPassword); // 仮パスワード
		resultEntity.setTempPasswordFlag(false); // 仮パスワードフラグ

		// 有効期限
		Date date = new Date(); // 現在時刻を確認
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date); // 「calendar」に現在日時をセット
		calendar.add(Calendar.DATE, 7); // 1週間の有効期限
		Date expireDate = calendar.getTime(); // Date型に戻す

		resultEntity.setTempPasswordExpirationDatetime(expireDate);

		// 生成したパスワードを引数に仮パスワード登録
		this.insertEntity(memberRep, resultEntity);

		// パスワード履歴エンティティを返す
		return resultEntity;
	}

	/**
	 * 複合エンティティ生成
	 * @param memberEntity 会員エンティティ
	 * @param isComposite 複合要素設定(true=設定する, false=設定しない)
	 * @return 複合エンティティ
	 */
	private MemberCompositeEntity createCompositeEntity(MemberEntity memberEntity, boolean isComposite) {

		// nullExceptionの回避
		if (memberEntity == null) {
			return null;
		}

		// 会員エンティティ(複合)
		MemberCompositeEntity entity = new MemberCompositeEntity();

		// 会員番号
		Integer memberNo = memberEntity.getMemberNo();

		// 会員
		entity.setMemberEntity(memberEntity);

		// 勤務先コード
		String workingFacilityCd = memberEntity.getWorkingFacilityCd();

		// 複合要素設定
		if (isComposite) {

			// 会員ポータルログイン履歴
			List<MemberLoginHistoryEntity> memberLoginHistoryEntities = memberLoginHistoryRep
					.getSearchResultByMemberNo(memberNo);
			if (CollectionUtils.isNotEmpty(memberLoginHistoryEntities) && memberLoginHistoryEntities.size() > 1) {

				// 前回ログイン(降順)
				List<MemberLoginHistoryEntity> lastLoginEntities = memberLoginHistoryEntities.stream().sorted(
						(new MemberLoginHistoryEntity.LoginDatetimeComparator()).reversed())
						.collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(lastLoginEntities) && lastLoginEntities.size() > 1) {
					entity.setLastLoginMemberLoginHistoryEntity(lastLoginEntities.get(1)); // 先頭は最新ログイン日時
				}

				// 前回ログアウト(降順)
				List<MemberLoginHistoryEntity> lastLogoutEntities = memberLoginHistoryEntities.stream().sorted(
						(new MemberLoginHistoryEntity.LogoutDatetimeComparator()).reversed())
						.collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(lastLogoutEntities)) {
					entity.setLastLogoutMemberLoginHistoryEntity(lastLogoutEntities.get(0)); // 先頭は前回ログアウト日時
				}
			}

			// 会員関連資格情報
			entity.setMemberQualificationEntities(memberQualificationRep.getSearchResultByMemberNo(memberNo));

			// 会員自治体参画情報
			entity.setMemberLocalActivityEntities(memberLocalActivityRep.getSearchResultByMemberNo(memberNo));

			// 施設登録申請
			entity.setFacilityApplicationEntity(createFacilityApplicationCompositeEntity(memberNo));

			// 施設養成校
			entity.setFacilityEntity(createFacilityCompositeEntity(workingFacilityCd));

			// 休会履歴
			entity.setRecessHistoryEntity(getRecessHistoryEntity(memberNo));

			// 会員勤務先業務分類
			entity.setMemberFacilityCategoryEntities(memberFacilityCategoryRep.getSearchResultByMemberNo(memberNo));

			// 生涯教育ステータス
			entity.setLifelongEducationStatusEntity(getLifelongEducationStatusEntity(memberNo));
		}

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param memberEntities 会員エンティティリスト
	 * @param isComposite 複合要素設定(true=設定する, false=設定しない)
	 * @return 複合エンティティリスト
	 */
	private List<MemberCompositeEntity> createCompositeEntities(List<MemberEntity> memberEntities,
			boolean isComposite) {
		return memberEntities.stream().map(E -> createCompositeEntity(E, isComposite)).collect(Collectors.toList());
	}

	/**
	 * 施設登録申請エンティティ(複合)生成
	 * @param memberNo 会員番号
	 * @return 施設登録申請エンティティ(複合)
	 */
	private FacilityApplicationCompositeEntity createFacilityApplicationCompositeEntity(Integer memberNo) {

		// 施設登録申請検索DTO生成
		FacilityApplicationSearchDto searchDto = new FacilityApplicationSearchDto();
		searchDto.setMemberNos(createArray(memberNo)); // 会員番号
		searchDto.setStatuses(new Integer[] {
				FacilityApplicationDomain.APPLICATION_STATUS.APPLYING.getValue(), // 申請中
				FacilityApplicationDomain.APPLICATION_STATUS.APPROVED.getValue() // 承認済み
		});
		logger.debug("施設登録申請.会員番号={}, 申請ステータス={}", memberNo, getLogFormatText(searchDto.getStatuses()));

		// 施設登録申請取得
		List<FacilityApplicationEntity> entities = facilityApplicationRep.getSearchResultByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			return null; // 該当データなし
		} else if (entities.size() > 1) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("memberNo=%d, size=%d", memberNo, entities.size());
			String message = String.format("施設登録申請情報が2件以上存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		// 施設登録申請ID配列取得
		Long[] applicantIds = entities.stream().map(E -> E.getId()).toArray(Long[]::new);
		logger.debug("施設登録申請ID[{}] : {}", applicantIds.length, getLogFormatText(applicantIds));

		// 施設登録申請分類取得(一括)
		List<FacilityApplicationCategoryEntity> categoryEntities = facilityApplicationCategoryRep
				.getSearchResultByApplicantIds(applicantIds);

		// 施設登録申請エンティティ(複合)生成
		FacilityApplicationCompositeEntity compositeEntity = new FacilityApplicationCompositeEntity();
		compositeEntity.setFacilityApplicationEntity(entities.get(0)); // 先頭要素固定
		compositeEntity.setFacilityApplicationCategoryEntities(categoryEntities);

		return compositeEntity;
	}

	/**
	 * 施設養成校エンティティ(複合)生成
	 * @param facilityNo 施設番号
	 * @return 施設養成校エンティティ(複合)
	 */
	private FacilityCompositeEntity createFacilityCompositeEntity(String facilityNo) {

		// 引数チェック
		logger.debug("施設養成校.施設番号={}", facilityNo);
		if (StringUtils.isEmpty(facilityNo)) {
			return null; // 該当データなし
		}

		// 施設養成校検索DTO生成
		FacilityPortalSearchDto facilityPortalSearchDto = new FacilityPortalSearchDto();
		facilityPortalSearchDto.setFacilityNo(facilityNo);

		// 施設養成校取得
		List<FacilityEntity> entities = facilityRep.getSearchResultByCondition(facilityPortalSearchDto);
		if (CollectionUtils.isEmpty(entities)) {
			return null; // 該当データなし
		} else if (entities.size() > 1) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("facilityNo=%s, size=%d", facilityNo, entities.size());
			String message = String.format("施設養成校情報が2件以上存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		// 施設登録ID配列取得
		Long id = entities.get(0).getId();
		logger.debug("施設登録ID={}", id);

		// 施設登録分類取得(一括)
		List<FacilityCategoryEntity> categoryEntities = facilityCategoryRep
				.getSearchResultByFacilityId(id);

		// 施設養成校エンティティ(複合)生成
		FacilityCompositeEntity compositeEntity = new FacilityCompositeEntity();
		compositeEntity.setFacilityEntity(entities.get(0)); // 先頭要素固定
		compositeEntity.setFacilityCategoryEntities(categoryEntities);

		return compositeEntity;
	}

	/**
	 * 休会履歴エンティティ取得
	 * @param memberNo 会員番号
	 * @return 休会履歴エンティティ
	 */
	private RecessHistoryEntity getRecessHistoryEntity(Integer memberNo) {

		// 休会履歴検索DTO生成
		RecessHistorySearchDto searchDto = new RecessHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号
		logger.debug("休会履歴.会員番号={}", memberNo);

		// 休会履歴取得
		List<RecessHistoryEntity> entities = recessHistoryRep.getSearchResultByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			return null; // 該当データなし
		} else if (entities.size() > 1) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("memberNo=%d, size=%d", memberNo, entities.size());
			String message = String.format("休会履歴情報が2件以上存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		return entities.get(0); // 先頭要素固定
	}

	/**
	 * 生涯教育エンティティ取得
	 * @param memberNo 会員番号
	 * @return 生涯教育エンティティ
	 */
	private LifelongEducationStatusEntity getLifelongEducationStatusEntity(Integer memberNo) {

		// 生涯教育検索DTO生成
		LifelongEducationStatusSearchDto searchDto = new LifelongEducationStatusSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号
		logger.debug("生涯教育.会員番号={}", memberNo);

		// 生涯教育取得
		List<LifelongEducationStatusEntity> entities = lifelongEducationStatusRep.getSearchResultByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			return null; // 該当データなし
		} else if (entities.size() > 1) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("memberNo=%d, size=%d", memberNo, entities.size());
			String message = String.format("生涯教育情報が2件以上存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		return entities.get(0); // 先頭要素固定
	}

	/**
	 * パスワード再設定メール送信
	 * @param memberCompositeEntity 会員エンティティ(複合)
	 */
	public void sendPasswordResettingMail(MemberCompositeEntity memberCompositeEntity) {
		// メール送信
		memberMailRep.sendPasswordResettingMail(memberCompositeEntity.getMemberEntity());
	}

	/**
	* メールアドレス変更メール送信
	* @param memberCompositeEntity 会員エンティティ(複合)
	*/
	public void sendEmailConfirmMail(MemberCompositeEntity memberCompositeEntity,
			MemberEmailHistoryEntity memberEmailHistoryEntity) {
		// メール送信
		memberMailRep.sendEmailConfirmMail(memberCompositeEntity.getMemberEntity(), memberEmailHistoryEntity);
	}

	/**
	 * 会員番号、資格名、専門分野で資格を持っている(true)/持っていない(false)を返す
	 * 資格名は01(基礎OT)、02(認定OT)、03(専門OT)
	 * @param memberNo 会員番号
	 * @param professionalField 専門分野
	 * @param qualificationName 資格名(basicOt、professionalOt、qualifiedOt)
	 * @return boolean
	 */
	public boolean isQualificationStatus(Integer memberNo, String qualificationCode) {
		MemberDomain memberDomain;
		List<ProfessionalOtAttendingSummaryEntity> professionalOtList = new ArrayList<>();
		//資格により分岐
		//基礎OTの資格を確認
		if ("01".equals(qualificationCode)) {
			BasicOtAttendingSummaryEntity basicEntity = basicOtAttendingSummaryRep.getSearchResultByMemberNo(memberNo);
			memberDomain = memberRep.findDomain(basicEntity.getId(), false);
			if (memberDomain.isQualifiedBasicTraining(basicEntity)) {
				return true;
			}
			//認定OTの資格を確認
			QualifiedOtAttendingSummaryEntity otEntity = qualifiedOtAttendingSummaryRep
					.getSearchResultByMemberNo(memberNo);
			memberDomain = memberRep.findDomain(otEntity.getId(), false);
			if (memberDomain.isQualifiedOt(otEntity)) {
				return true;
			}
			//専門OTの資格を確認
			professionalOtList.addAll(professionalOtAttendingSummaryRep.getSearchResultByProfessionalField(memberNo));
			for (ProfessionalOtAttendingSummaryEntity professionalOtEntity : professionalOtList) {
				memberDomain = memberRep.findDomain(professionalOtEntity.getId(), false);
				if (memberDomain.isQualifiedProfessionalOt(professionalOtEntity)) {
					return true;
				}
			}
		}

		//認定OTの資格を確認
		if ("02".equals(qualificationCode)) {
			QualifiedOtAttendingSummaryEntity otEntity = qualifiedOtAttendingSummaryRep
					.getSearchResultByMemberNo(memberNo);
			memberDomain = memberRep.findDomain(otEntity.getId(), false);
			if (memberDomain.isQualifiedOt(otEntity)) {
				return true;
			}
			//専門OTの資格を確認
			professionalOtList.addAll(professionalOtAttendingSummaryRep.getSearchResultByProfessionalField(memberNo));
			for (ProfessionalOtAttendingSummaryEntity professionalOtEntity : professionalOtList) {
				memberDomain = memberRep.findDomain(professionalOtEntity.getId(), false);
				if (memberDomain.isQualifiedProfessionalOt(professionalOtEntity)) {
					return true;
				}
			}
		}

		//専門OTの資格を確認
		if ("03".equals(qualificationCode)) {
			professionalOtList.addAll(professionalOtAttendingSummaryRep.getSearchResultByProfessionalField(memberNo));
			for (ProfessionalOtAttendingSummaryEntity professionalOtEntity : professionalOtList) {
				memberDomain = memberRep.findDomain(professionalOtEntity.getId(), false);
				if (memberDomain.isQualifiedProfessionalOt(professionalOtEntity)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 会員番号、年度で会費が完納状態または、速報値確報値を持っている(true)/持っていない(false)
	 * @param memberNo 会員番号
	 * @param fiscalYear 年度
	 * @return boolean
	 */
	public boolean isMembershipFeeCompleteStatus(Integer memberNo, Integer fiscalYear) throws Exception {
		MemberDomain memberDomain;
		List<MembershipFeeDeliveryEntity> membershipFeeDeliveryList = new ArrayList<>();

		//会費が納入済みか確認
		membershipFeeDeliveryList.addAll(MembershipFeeDeliveryRep.getSearchResultByMemberNo(memberNo));
		for (MembershipFeeDeliveryEntity membershipFeeDeliveryEntity : membershipFeeDeliveryList) {
			memberDomain = memberRep.findDomain(membershipFeeDeliveryEntity.getId(), false);
			if (memberDomain.isMembershipFeeComplete(membershipFeeDeliveryEntity)) {
				return true;
			}
		}

		List<DskReceiptEntity> receiptList = new ArrayList<>();
		//会員番号を0埋め
		String strMemberNo = String.format("%06d", memberNo);
		//検索の為、請求年月日を作成
		String strFiscalYear = String.valueOf(fiscalYear);
		Date paymentDueDate = java.sql.Date.valueOf(strFiscalYear + "-12-25");//本来は"-12-25"
		//速報値があるか確認
		receiptList.addAll(DskReceiptRep.getSearchResultByPaymentDueDate(strMemberNo, paymentDueDate));
		for (DskReceiptEntity receiptEntity : receiptList) {
			memberDomain = memberRep.findDomain(receiptEntity.getId(), false);
			if (memberDomain.hasQuickAnnouncement(receiptEntity)) {
				return true;
			}
		}
		return false;
	}
	// TODO
}
