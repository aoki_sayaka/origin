package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.BasicOtTrainingHistoryRepository;
import jp.or.jaot.domain.repository.HandbookMigrationApplicationRepository;
import jp.or.jaot.domain.repository.MemberRepository;
import jp.or.jaot.domain.repository.MemberWithdrawalHistoryRepository;
import jp.or.jaot.domain.repository.RecessHistoryRepository;
import jp.or.jaot.model.dto.BasicOtTrainingHistoryDto;
import jp.or.jaot.model.dto.HandbookMigrationApplicationDto;
import jp.or.jaot.model.dto.MemberDto;
import jp.or.jaot.model.dto.MemberWithdrawalHistoryDto;
import jp.or.jaot.model.dto.RecessHistoryDto;
import jp.or.jaot.model.dto.search.HandbookMigrationApplicationSearchDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.dto.search.MemberWithdrawalHistorySearchDto;
import jp.or.jaot.model.dto.search.RecessHistorySearchDto;
import jp.or.jaot.model.entity.HandbookMigrationApplicationEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.composite.HandbookMigrationApplicationCompositeEntity;
import jp.or.jaot.model.entity.composite.MemberCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 手帳移行申請サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class HandbookMigrationApplicationService extends BaseService {

	/**会員リポジトリ */
	@Autowired
	private MemberRepository memberRep;

	/** 手帳移行申請リポジトリ */
	@Autowired
	private HandbookMigrationApplicationRepository handbookMigrationApplicationRep;

	/** 基礎研修受講履歴リポジトリ */
	@Autowired
	private BasicOtTrainingHistoryRepository basicOtTrainingHistoryRep;

	/** 休会履歴リポジトリ */
	@Autowired
	private RecessHistoryRepository recessHistoryRep;

	/** 退会履歴リポジトリ */
	@Autowired
	private MemberWithdrawalHistoryRepository memberWithdrawalHistoryRep;

	/**
	 * コンストラクタ
	 */
	//TODO ERROR_PLACEが未定義
	@Autowired
	public HandbookMigrationApplicationService() {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY);
	}
	
	public HandbookMigrationApplicationService(HandbookMigrationApplicationRepository handbookMigrationApplicationRep) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
		this.handbookMigrationApplicationRep = handbookMigrationApplicationRep;
	}
	

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	/*
	public HandbookMigrationApplicationDto getSearchResult(Long id) {
		return HandbookMigrationApplicationDto.create()
				.setEntity(handbookMigrationApplicationRep.getSearchResult(id));
	}
	*/

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	//会員テーブルのみ（あとで関連テーブル取得用を作成）
	public MemberDto getSearchResultByMemberNo(Integer memberNo) {
		return MemberDto.create().setEntity(createEntity(memberRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public HandbookMigrationApplicationDto getSearchResultByCondition(HandbookMigrationApplicationSearchDto searchDto) {
		return HandbookMigrationApplicationDto.create()
				.setEntities(createCompositeEntities(handbookMigrationApplicationRep.getSearchResultByCondition(searchDto)));
	}
	
	/**
	 * 複合エンティティリスト生成
	 * @param handbookMigrationApplicationEntity 手帳移行申請エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<HandbookMigrationApplicationCompositeEntity> createCompositeEntities(
			List<HandbookMigrationApplicationEntity> handbookMigrationApplicationEntities) {
		return handbookMigrationApplicationEntities.stream().map(E -> createCompositeEntity(E)).collect(Collectors.toList());
	}

	/**
	 * 複合エンティティ生成
	 * @param handbookMigrationApplicationEntity 手帳移行申請申請エンティティ
	 * @return 複合エンティティ
	 */
	private HandbookMigrationApplicationCompositeEntity createCompositeEntity(
			HandbookMigrationApplicationEntity handbookMigrationApplicationEntity) {

		// 手帳移行申請エンティティ(複合)
		HandbookMigrationApplicationCompositeEntity entity = new HandbookMigrationApplicationCompositeEntity();

		// 会員番号
		Integer memberNo = handbookMigrationApplicationEntity.getMemberNo();

		// 手帳移行申請
		entity.setHandbookMigrationApplicationEntity(handbookMigrationApplicationEntity);
		
		// 会員情報
		entity.setMemberEntities(getMemberEntity(memberNo));

		return entity;
	}
	
	/**
	 * 会員エンティティ取得
	 * @param memberNo 会員番号
	 * @return 会員エンティティ
	 */
	private List<MemberEntity> getMemberEntity(Integer memberNo) {

		// 会員情報検索DTO生成
		MemberSearchDto searchDto = new MemberSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号
		logger.debug("会員.会員番号={}", memberNo);

		// 会員情報取得
		List<MemberEntity> entities = memberRep.getSearchResultByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			return null; // 該当データなし
		} else if (entities.size() > 1) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("memberNo=%d, size=%d", memberNo, entities.size());
			String message = String.format("会員情報が2件以上存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		return entities;
	}
	
	/**
	 * 検索結果取得(受講履歴)
	 * @param memberNo 
	 * @return DTO
	 */
	public BasicOtTrainingHistoryDto getSearchResultByCondition(Integer memberNo) {
		return BasicOtTrainingHistoryDto.create()
				.setEntities(basicOtTrainingHistoryRep.getSearchResultByMemberNo(memberNo));
	}

	/**
	 * エンティティ生成
	 * @param memberEntity 会員エンティティ
	 * @return エンティティ
	 */
	private MemberCompositeEntity createEntity(MemberEntity memberEntity) {

		// 会員エンティティ
		MemberCompositeEntity entity = new MemberCompositeEntity();

		// 会員
		entity.setMemberEntity(memberEntity);

		return entity;
	}

	/**
	 * 検索結果取得(条件)休会履歴
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public RecessHistoryDto getSearchResultRecessByCondition(RecessHistorySearchDto searchDto) {
		return RecessHistoryDto.create().setEntities(recessHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 検索結果取得(条件)退会履歴
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberWithdrawalHistoryDto getSearchResultMemberWithdrawalByCondition(
			MemberWithdrawalHistorySearchDto searchDto) {
		return MemberWithdrawalHistoryDto.create()
				.setEntities(memberWithdrawalHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(HandbookMigrationApplicationDto dto) {
		HandbookMigrationApplicationCompositeEntity compositeEntity = dto.getFirstEntity();
		insertEntity(handbookMigrationApplicationRep, compositeEntity.getHandbookMigrationApplicationEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	/*
	public void update(HandbookMigrationApplicationDto dto) {
		updateEntity(handbookMigrationApplicationRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}
	*/

}
