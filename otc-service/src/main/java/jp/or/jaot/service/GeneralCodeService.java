package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.GeneralCodeRepository;
import jp.or.jaot.model.dto.GeneralCodeDto;
import jp.or.jaot.model.dto.search.GeneralCodeSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 汎用コードサービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class GeneralCodeService extends BaseService {

	/** 汎用コードリポジトリ */
	private final GeneralCodeRepository generalCodeRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param generalCodeRep リポジトリ
	 */
	@Autowired
	public GeneralCodeService(GeneralCodeRepository generalCodeRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.generalCodeRep = generalCodeRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public GeneralCodeDto getSearchResultByCondition(GeneralCodeSearchDto searchDto) {
		return GeneralCodeDto.create().setEntities(generalCodeRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
