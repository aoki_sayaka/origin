package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.TrainingInstructorRepository;
import jp.or.jaot.model.dto.TrainingInstructorDto;
import jp.or.jaot.model.dto.search.TrainingInstructorSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会講師実績サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingInstructorService extends BaseService {

	/** 研修会講師実績リポジトリ */
	private final TrainingInstructorRepository trainingInstructorRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingInstructorRep リポジトリ
	 */
	@Autowired
	public TrainingInstructorService(TrainingInstructorRepository trainingInstructorRep) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
		this.trainingInstructorRep = trainingInstructorRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingInstructorDto getSearchResultByCondition(TrainingInstructorSearchDto searchDto) {
		return TrainingInstructorDto.create().setEntities(trainingInstructorRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(TrainingInstructorDto dto) {
		insertEntity(trainingInstructorRep, dto.getFirstEntity());
	}

	// TODO
}
