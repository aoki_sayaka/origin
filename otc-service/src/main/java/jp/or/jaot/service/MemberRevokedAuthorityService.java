package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberRevokedAuthorityRepository;
import jp.or.jaot.model.dto.MemberRevokedAuthorityDto;
import jp.or.jaot.model.dto.search.MemberRevokedAuthoritySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員権限サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberRevokedAuthorityService extends BaseService {

	/** 会員権限リポジトリ */
	private final MemberRevokedAuthorityRepository memberRevokedAuthorityRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberRevokedAuthorityRep リポジトリ
	 */
	@Autowired
	public MemberRevokedAuthorityService(MemberRevokedAuthorityRepository memberRevokedAuthorityRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberRevokedAuthorityRep = memberRevokedAuthorityRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberRevokedAuthorityDto getSearchResultByCondition(MemberRevokedAuthoritySearchDto searchDto) {
		return MemberRevokedAuthorityDto.create()
				.setEntities(memberRevokedAuthorityRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberRevokedAuthorityDto dto) {
		insertEntity(memberRevokedAuthorityRep, dto.getFirstEntity());
	}

	// TODO
}
