package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.PaotMembershipFeeDeliveryRepository;
import jp.or.jaot.model.dto.PaotMembershipFeeDeliveryDto;
import jp.or.jaot.model.dto.search.PaotMembershipFeeDeliverySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 県士会会費納入サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class PaotMembershipFeeDeliveryService extends BaseService {

	/** 県士会会費納入リポジトリ */
	private final PaotMembershipFeeDeliveryRepository paotMembershipFeeDeliveryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param paotMembershipFeeDeliveryRep リポジトリ
	 */
	@Autowired
	public PaotMembershipFeeDeliveryService(PaotMembershipFeeDeliveryRepository paotMembershipFeeDeliveryRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.paotMembershipFeeDeliveryRep = paotMembershipFeeDeliveryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public PaotMembershipFeeDeliveryDto getSearchResultByCondition(PaotMembershipFeeDeliverySearchDto searchDto) {
		return PaotMembershipFeeDeliveryDto.create()
				.setEntities(paotMembershipFeeDeliveryRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
