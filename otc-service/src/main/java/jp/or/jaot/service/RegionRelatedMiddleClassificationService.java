package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.RegionRelatedMiddleClassificationRepository;
import jp.or.jaot.model.dto.RegionRelatedMiddleClassificationDto;
import jp.or.jaot.model.dto.search.RegionRelatedMiddleClassificationSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 領域関連中分類サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class RegionRelatedMiddleClassificationService extends BaseService {

	/** 領域関連中分類リポジトリ */
	private final RegionRelatedMiddleClassificationRepository regionRelatedMiddleClassificationRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param regionRelatedMiddleClassificationRep リポジトリ
	 */
	@Autowired
	public RegionRelatedMiddleClassificationService(
			RegionRelatedMiddleClassificationRepository regionRelatedMiddleClassificationRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.regionRelatedMiddleClassificationRep = regionRelatedMiddleClassificationRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public RegionRelatedMiddleClassificationDto getSearchResultByCondition(
			RegionRelatedMiddleClassificationSearchDto searchDto) {
		return RegionRelatedMiddleClassificationDto.create()
				.setEntities(regionRelatedMiddleClassificationRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
