package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.SocialContributionActualFieldRepository;
import jp.or.jaot.model.dto.SocialContributionActualFieldDto;
import jp.or.jaot.model.dto.search.SocialContributionActualFieldSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 活動実績の領域(後輩育成・社会貢献)サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class SocialContributionActualFieldService extends BaseService {

	/** 活動実績の領域(後輩育成・社会貢献)リポジトリ */
	private final SocialContributionActualFieldRepository socialContributionActualFieldRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param socialContributionActualFieldRep リポジトリ
	 */
	@Autowired
	public SocialContributionActualFieldService(
			SocialContributionActualFieldRepository socialContributionActualFieldRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.socialContributionActualFieldRep = socialContributionActualFieldRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public SocialContributionActualFieldDto getSearchResult(Long id) {
		return SocialContributionActualFieldDto.create()
				.setEntity(socialContributionActualFieldRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public SocialContributionActualFieldDto getSearchResultByCondition(
			SocialContributionActualFieldSearchDto searchDto) {
		return SocialContributionActualFieldDto.create()
				.setEntities(socialContributionActualFieldRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(SocialContributionActualFieldDto dto) {
		insertEntity(socialContributionActualFieldRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(SocialContributionActualFieldDto dto) {
		updateEntity(socialContributionActualFieldRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
}
