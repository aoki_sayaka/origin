package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ClinicalTrainingLeaderQualifyHistoryRepository;
import jp.or.jaot.model.dto.ClinicalTrainingLeaderQualifyHistoryDto;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderQualifyHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 臨床実習指導者認定履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ClinicalTrainingLeaderQualifyHistoryService extends BaseService {

	/** 臨床実習指導者認定履歴リポジトリ */
	private final ClinicalTrainingLeaderQualifyHistoryRepository clinicalTrainingLeaderQualifyHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtAttendingSummaryRep リポジトリ
	 */
	@Autowired
	public ClinicalTrainingLeaderQualifyHistoryService(
			ClinicalTrainingLeaderQualifyHistoryRepository clinicalTrainingLeaderQualifyHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.clinicalTrainingLeaderQualifyHistoryRep = clinicalTrainingLeaderQualifyHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public ClinicalTrainingLeaderQualifyHistoryDto getSearchResult(Long id) {
		return ClinicalTrainingLeaderQualifyHistoryDto.create()
				.setEntity(clinicalTrainingLeaderQualifyHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ClinicalTrainingLeaderQualifyHistoryDto getSearchResultByCondition(
			ClinicalTrainingLeaderQualifyHistorySearchDto searchDto) {
		return ClinicalTrainingLeaderQualifyHistoryDto.create()
				.setEntities(clinicalTrainingLeaderQualifyHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(ClinicalTrainingLeaderQualifyHistoryDto dto) {
		insertEntity(clinicalTrainingLeaderQualifyHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(ClinicalTrainingLeaderQualifyHistoryDto dto) {
		updateEntity(clinicalTrainingLeaderQualifyHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

}
