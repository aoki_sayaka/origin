package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.BasicOtAttendingSummaryRepository;
import jp.or.jaot.domain.repository.BasicOtQualifyHistoryRepository;
import jp.or.jaot.domain.repository.BasicOtTrainingHistoryRepository;
import jp.or.jaot.domain.repository.PeriodExtensionApplicationRepository;
import jp.or.jaot.model.dto.BasicOtAttendingSummaryDto;
import jp.or.jaot.model.dto.search.BasicOtAttendingSummarySearchDto;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.composite.BasicOtAttendingSummaryCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 基礎研修履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class BasicOtAttendingSummaryService extends BaseService {

	/** 基礎研修履歴リポジトリ */
	@Autowired
	private BasicOtAttendingSummaryRepository basicOtAttendingSummaryRep;

	/** 基礎研修受講履歴リポジトリ */
	@Autowired
	private BasicOtQualifyHistoryRepository basicOtQualifyHistoryRep;

	/** 基礎研修受講履歴リポジトリ */
	@Autowired
	private BasicOtTrainingHistoryRepository basicOtTrainingHistoryRep;

	/** 期間延長申請リポジトリ */
	@Autowired
	private PeriodExtensionApplicationRepository periodExtensionApplicationRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtAttendingSummaryRep リポジトリ
	 */
	@Autowired
	public BasicOtAttendingSummaryService(BasicOtAttendingSummaryRepository basicOtAttendingSummaryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.basicOtAttendingSummaryRep = basicOtAttendingSummaryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public BasicOtAttendingSummaryDto getSearchResult(Long id) {
		return BasicOtAttendingSummaryDto.create()
				.setEntity(createCompositeEntity(basicOtAttendingSummaryRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	public BasicOtAttendingSummaryDto getSearchResultByBasicOtAttendingSummaryNo(Integer memberNo) {
		return BasicOtAttendingSummaryDto.create()
				.setEntity(createCompositeEntity(basicOtAttendingSummaryRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public BasicOtAttendingSummaryDto getSearchResultByCondition(BasicOtAttendingSummarySearchDto searchDto) {

		return BasicOtAttendingSummaryDto.create()
				.setEntities(createCompositeEntities(basicOtAttendingSummaryRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(BasicOtAttendingSummaryDto dto) {
		// 基礎研修履歴エンティティ(複合)
		BasicOtAttendingSummaryCompositeEntity compositeEntity = dto.getFirstEntity();

		BasicOtAttendingSummaryEntity basicOtAttendingSummaryEntity = compositeEntity
				.getBasicOtAttendingSummaryEntity();
		logger.debug("基礎研修履歴検索1={}", basicOtAttendingSummaryRep);
		logger.debug("基礎研修履歴検索2={}", basicOtAttendingSummaryEntity);
		logger.debug("基礎研修履歴検索3={}", basicOtAttendingSummaryEntity.getId());
		// 基礎研修履歴
		insertEntity(basicOtAttendingSummaryRep, compositeEntity.getBasicOtAttendingSummaryEntity());

		// 基礎研修修了認定更新履歴
		compositeEntity.getBasicOtQualifyHistoryEntities().stream()
				.forEach(E -> insertEntity(basicOtQualifyHistoryRep, E));

		// 基礎研修受講履歴
		compositeEntity.getBasicOtTrainingHistoryEntities().stream()
				.forEach(E -> insertEntity(basicOtTrainingHistoryRep, E));
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(BasicOtAttendingSummaryDto dto) {

		// 基礎研修履歴エンティティ(複合)
		BasicOtAttendingSummaryCompositeEntity compositeEntity = dto.getFirstEntity();

		// 基礎研修履歴
		BasicOtAttendingSummaryEntity basicOtAttendingSummaryEntity = compositeEntity
				.getBasicOtAttendingSummaryEntity();
		//		logger.debug("基礎研修履歴検索1={}",basicOtAttendingSummaryRep);
		//		logger.debug("基礎研修履歴検索2={}",basicOtAttendingSummaryEntity);
		//		logger.debug("基礎研修履歴検索3={}",basicOtAttendingSummaryEntity.getId());
		updateEntity(basicOtAttendingSummaryRep, basicOtAttendingSummaryEntity, basicOtAttendingSummaryEntity.getId());

		// 基礎研修修了認定更新履歴
		for (BasicOtQualifyHistoryEntity entity : compositeEntity.getBasicOtQualifyHistoryEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				insertEntity(basicOtQualifyHistoryRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(basicOtQualifyHistoryRep, entity, id); // 削除(物理)
				} else {
					updateEntity(basicOtQualifyHistoryRep, entity, id); // 更新
				}
			}
		}

		// 基礎研修受講履歴
		for (BasicOtTrainingHistoryEntity entity : compositeEntity.getBasicOtTrainingHistoryEntities()) {
			//			logger.debug("基礎研修受講履歴検索1={}",basicOtTrainingHistoryRep);
			//			logger.debug("基礎研修受講履歴検索2={}",entity);
			//			logger.debug("基礎研修受講履歴検索3={}",entity.getId());
			updateEntity(basicOtTrainingHistoryRep, entity, entity.getId()); // 更新
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param basicOtAttendingSummaryEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private BasicOtAttendingSummaryCompositeEntity createCompositeEntity(
			BasicOtAttendingSummaryEntity basicOtAttendingSummaryEntity) {

		// 基礎研修履歴エンティティ(複合)
		BasicOtAttendingSummaryCompositeEntity entity = new BasicOtAttendingSummaryCompositeEntity();

		// 会員番号
		Integer menberNo = basicOtAttendingSummaryEntity.getMemberNo();

		// 基礎研修履歴
		entity.setBasicOtAttendingSummaryEntity(basicOtAttendingSummaryEntity);

		// 基礎研修修了認定更新履歴
		try {
			entity.setBasicOtQualifyHistoryEntities(basicOtQualifyHistoryRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		//  基礎研修受講履歴
		try {
			entity.setBasicOtTrainingHistoryEntities(basicOtTrainingHistoryRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		//  期間延長申請
		try {
			entity.setPeriodExtensionApplicationEntities(
					periodExtensionApplicationRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param basicOtAttendingSummaryEntity 基礎研修履歴エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<BasicOtAttendingSummaryCompositeEntity> createCompositeEntities(
			List<BasicOtAttendingSummaryEntity> basicOtAttendingSummaryEntities) {
		return basicOtAttendingSummaryEntities.stream().map(E -> createCompositeEntity(E)).collect(Collectors.toList());
	}

	/**
	 * 会員番号で検索
	 * @param memberNo 会員番号
	 * @return エンティティ
	 */
	public BasicOtAttendingSummaryEntity getSearchResultByBasicOtAttendingSummaryMemberNo(Integer memberNo) {
		//エンティティを取得
		BasicOtAttendingSummaryEntity entity = basicOtAttendingSummaryRep
				.getSearchResultByMemberNo(memberNo);

		return entity;
	}
	// TODO

}
