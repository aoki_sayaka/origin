package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberLocalActivityRepository;
import jp.or.jaot.model.dto.MemberLocalActivityDto;
import jp.or.jaot.model.dto.search.MemberLocalActivitySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員自治体参画情報サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberLocalActivityService extends BaseService {

	/** 会員自治体参画情報リポジトリ */
	private final MemberLocalActivityRepository memberLocalActivityRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberLocalActivityRep リポジトリ
	 */
	@Autowired
	public MemberLocalActivityService(MemberLocalActivityRepository memberLocalActivityRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberLocalActivityRep = memberLocalActivityRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MemberLocalActivityDto getSearchResult(Long id) {
		return MemberLocalActivityDto.create().setEntity(memberLocalActivityRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberLocalActivityDto getSearchResultByCondition(MemberLocalActivitySearchDto searchDto) {
		return MemberLocalActivityDto.create()
				.setEntities(memberLocalActivityRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberLocalActivityDto dto) {
		insertEntity(memberLocalActivityRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(MemberLocalActivityDto dto) {
		updateEntity(memberLocalActivityRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
}
