package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberEmailHistoryRepository;
import jp.or.jaot.model.dto.MemberEmailHistoryDto;
import jp.or.jaot.model.dto.search.MemberEmailHistorySearchDto;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員パスワード履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberEmailHistoryService extends BaseService {

	/** 会員メールアドレス履歴リポジトリ */
	private final MemberEmailHistoryRepository memberEmailHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberEmailHistoryRep リポジトリ
	 */
	@Autowired
	public MemberEmailHistoryService(MemberEmailHistoryRepository memberEmailHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberEmailHistoryRep = memberEmailHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberEmailHistoryDto getSearchResultByCondition(MemberEmailHistorySearchDto searchDto) {
		return MemberEmailHistoryDto.create()
				.setEntities(memberEmailHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberEmailHistoryDto dto) {
		insertEntity(memberEmailHistoryRep, dto.getFirstEntity());
	}


	/**
	 * 更新
	 * @param dto MemberEmailHistoryDto
	 */
	public void update(final MemberEmailHistoryDto dto) {
		for (MemberEmailHistoryEntity e : dto.getEntities()) {
			updateEntity(historyTableDefinitionRep, e, e.getId());
		}
	}
}
