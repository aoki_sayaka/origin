package jp.or.jaot.service.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ドメインサービスの基底クラス</br>
 * サービス/リポジトリ/ドメインに実装された各処理を横断的に内包した内部連携処理を提供するための基底クラス
 */
public abstract class BaseDomainService {

	/** ロガーインスタンス */
	protected final Logger logger;

	/**
	 * コンストラクタ
	 */
	public BaseDomainService() {
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	// TODO
}
