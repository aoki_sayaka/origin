package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.CaseReportRepository;
import jp.or.jaot.domain.repository.PeriodExtensionApplicationRepository;
import jp.or.jaot.domain.repository.QualificationOtAppHistoryRepository;
import jp.or.jaot.domain.repository.QualifiedOtAttendingSummaryRepository;
import jp.or.jaot.domain.repository.QualifiedOtCompletionHistoryRepository;
import jp.or.jaot.domain.repository.QualifiedOtTrainingHistoryRepository;
import jp.or.jaot.model.dto.QualifiedOtAttendingSummaryDto;
import jp.or.jaot.model.dto.search.QualifiedOtAttendingSummarySearchDto;
import jp.or.jaot.model.entity.CaseReportEntity;
import jp.or.jaot.model.entity.QualificationOtAppHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.composite.QualifiedOtAttendingSummaryCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 認定作業療法士研修履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class QualifiedOtAttendingSummaryService extends BaseService {

	/** 認定作業療法士研修履歴リポジトリ */
	@Autowired
	private QualifiedOtAttendingSummaryRepository qualifiedOtAttendingSummaryRep;

	/** 認定作業療法士研修修了認定更新履歴リポジトリ */
	@Autowired
	private QualifiedOtCompletionHistoryRepository qualifiedOtCompletionHistoryRep;

	/** 認定作業療法士研修受講履歴リポジトリ **/
	@Autowired
	private QualifiedOtTrainingHistoryRepository qualifiedOtTrainingHistoryRep;

	/** 事例報告リポジトリ **/
	@Autowired
	private CaseReportRepository caseReportRep;

	/** 認定作業療法士申請履歴リポジトリ **/
	@Autowired
	private QualificationOtAppHistoryRepository qualificationOtAppHistoryRep;

	/** 期間延長申請リポジトリ */
	@Autowired
	private PeriodExtensionApplicationRepository periodExtensionApplicationRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public QualifiedOtAttendingSummaryService() {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public QualifiedOtAttendingSummaryDto getSearchResult(Long id) {
		return QualifiedOtAttendingSummaryDto.create()
				.setEntity(createCompositeEntity(qualifiedOtAttendingSummaryRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	public QualifiedOtAttendingSummaryDto getSearchResultByMemberNo(Integer memberNo) {
		return QualifiedOtAttendingSummaryDto.create()
				.setEntity(createCompositeEntity(qualifiedOtAttendingSummaryRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public QualifiedOtAttendingSummaryDto getSearchResultByCondition(QualifiedOtAttendingSummarySearchDto searchDto) {

		return QualifiedOtAttendingSummaryDto.create().setEntities(
				createCompositeEntities(qualifiedOtAttendingSummaryRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 入会
	 * @param dto DTO
	 */
	public void enter(QualifiedOtAttendingSummaryDto dto) {

		// 認定作業療法士研修履歴エンティティ(複合)
		QualifiedOtAttendingSummaryCompositeEntity compositeEntity = dto.getFirstEntity();

		// 認定作業療法士研修履歴
		insertEntity(qualifiedOtAttendingSummaryRep, compositeEntity.getQualifiedOtAttendingSummaryEntity());

		// 認定作業療法士研修修了認定更新履歴
		compositeEntity.getQualifiedOtCompletionHistoryEntities().stream()
				.forEach(E -> insertEntity(qualifiedOtCompletionHistoryRep, E));

		// 認定作業療法士研修受講履歴
		compositeEntity.getQualifiedOtTrainingHistoryEntities().stream()
				.forEach(E -> insertEntity(qualifiedOtTrainingHistoryRep, E));

		// 事例報告
		compositeEntity.getCaseReportEntities().stream()
				.forEach(E -> insertEntity(caseReportRep, E));

		// 認定作業療法士申請履歴
		compositeEntity.getQualificationOtAppHistoryEntities().stream()
				.forEach(E -> insertEntity(qualificationOtAppHistoryRep, E));
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(QualifiedOtAttendingSummaryDto dto) {

		// 認定作業療法士研修履歴エンティティ(複合)
		QualifiedOtAttendingSummaryCompositeEntity compositeEntity = dto.getFirstEntity();

		// 会員
		QualifiedOtAttendingSummaryEntity memberEntity = compositeEntity.getQualifiedOtAttendingSummaryEntity();
		updateEntity(qualifiedOtAttendingSummaryRep, memberEntity, memberEntity.getId());

		// 認定作業療法士研修修了認定更新履歴
		for (QualifiedOtCompletionHistoryEntity entity : compositeEntity.getQualifiedOtCompletionHistoryEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				insertEntity(qualifiedOtCompletionHistoryRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(qualifiedOtCompletionHistoryRep, entity, id); // 削除(物理)
				} else {
					updateEntity(qualifiedOtCompletionHistoryRep, entity, id); // 更新
				}
			}
		}

		// 認定作業療法士研修受講履歴
		for (QualifiedOtTrainingHistoryEntity entity : compositeEntity.getQualifiedOtTrainingHistoryEntities()) {
			updateEntity(qualifiedOtTrainingHistoryRep, entity, entity.getId()); // 更新
		}

		// 事例報告
		for (CaseReportEntity entity : compositeEntity.getCaseReportEntities()) {
			updateEntity(caseReportRep, entity, entity.getId()); // 更新
		}

		// 認定作業療法士申請履歴
		for (QualificationOtAppHistoryEntity entity : compositeEntity.getQualificationOtAppHistoryEntities()) {
			updateEntity(qualificationOtAppHistoryRep, entity, entity.getId()); // 更新
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param memberEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private QualifiedOtAttendingSummaryCompositeEntity createCompositeEntity(
			QualifiedOtAttendingSummaryEntity qualifiedOtAttendingSummaryEntity) {

		// 認定作業療法士研修履歴エンティティ(複合)
		QualifiedOtAttendingSummaryCompositeEntity entity = new QualifiedOtAttendingSummaryCompositeEntity();

		// 会員番号
		Integer memberNo = qualifiedOtAttendingSummaryEntity.getMemberNo();

		// 認定作業療法士研修履歴
		entity.setQualifiedOtAttendingSummaryEntity(qualifiedOtAttendingSummaryEntity);

		// 会員関連資格情報
		try {
			entity.setQualifiedOtCompletionHistoryEntities(
					qualifiedOtCompletionHistoryRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		// 認定作業療法士研修受講履歴
		try {
			entity.setQualifiedOtTrainingHistoryEntities(
					qualifiedOtTrainingHistoryRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		// 事例報告
		try {
			entity.setCaseReportEntities(
					caseReportRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		// 認定作業療法士申請履歴
		try {
			entity.setQualificationOtAppHistoryEntities(
					qualificationOtAppHistoryRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		// 期間延長申請
		try {
			entity.setPeriodExtensionApplicationEntities(
					periodExtensionApplicationRep.getSearchResultByMemberNo(memberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}
		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param memberEntity 認定作業療法士研修履歴エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<QualifiedOtAttendingSummaryCompositeEntity> createCompositeEntities(
			List<QualifiedOtAttendingSummaryEntity> qualifiedOtAttendingSummaryEntities) {
		return qualifiedOtAttendingSummaryEntities.stream().map(E -> createCompositeEntity(E))
				.collect(Collectors.toList());
	}

	/**
	 * 会員番号で検索
	 * @param memberNo 会員番号
	 * @return エンティティ
	 */
	public QualifiedOtAttendingSummaryEntity getSearchResultByQualifiedOtAttendingSummaryMemberNo(Integer memberNo) {
		//エンティティを取得
		QualifiedOtAttendingSummaryEntity entity = qualifiedOtAttendingSummaryRep
				.getSearchResultByMemberNo(memberNo);

		return entity;
	}
	// TODO
}
