package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.TrainingApplicationRepository;
import jp.or.jaot.domain.repository.TrainingAttendanceResultRepository;
import jp.or.jaot.domain.repository.TrainingRepository;
import jp.or.jaot.model.dto.TrainingAttendanceResultDto;
import jp.or.jaot.model.dto.search.TrainingApplicationSearchDto;
import jp.or.jaot.model.dto.search.TrainingAttendanceResultSearchDto;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import jp.or.jaot.model.entity.composite.TrainingAttendanceResultCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会出欠合否サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingAttendanceResultService extends BaseService {

	/** 研修会出欠合否リポジトリ */
	@Autowired
	private TrainingAttendanceResultRepository trainingAttendanceResultRep;

	/** 研修会リポジトリ[親] */
	@Autowired
	private TrainingRepository trainingRep;

	/** 研修会申込リポジトリ[関連] */
	@Autowired
	private TrainingApplicationRepository trainingApplicationRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public TrainingAttendanceResultService() {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public TrainingAttendanceResultDto getSearchResult(Long id) {
		return TrainingAttendanceResultDto.create().setEntity(createCompositeEntity(
				trainingAttendanceResultRep.getSearchResult(id), new ArrayList<TrainingEntity>()));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingAttendanceResultDto getSearchResultByCondition(TrainingAttendanceResultSearchDto searchDto) {
		return TrainingAttendanceResultDto.create().setEntities(createOuterJoinCompositeEntities(searchDto));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingAttendanceResultDto getSearchResultListByCondition(TrainingAttendanceResultSearchDto searchDto) {
		return TrainingAttendanceResultDto.create().setEntities(createOuterJoinCompositeEntities(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(TrainingAttendanceResultDto dto) {
		insertEntity(trainingAttendanceResultRep, dto.getFirstEntity().getTrainingAttendanceResultEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(TrainingAttendanceResultDto dto) {

		// 研修会出欠合否エンティティ(複合)
		TrainingAttendanceResultCompositeEntity compositeEntity = dto.getFirstEntity();

		// 研修会出欠合否
		TrainingAttendanceResultEntity entity = compositeEntity.getTrainingAttendanceResultEntity();
		TrainingAttendanceResultEntity updateEntity = null;
		Long id = null;
		if ((id = entity.getId()) == null) {
			updateEntity = (TrainingAttendanceResultEntity) insertEntity(trainingAttendanceResultRep, entity); // 追加
		} else {
			updateEntity = (TrainingAttendanceResultEntity) updateEntity(trainingAttendanceResultRep, entity, id); // 更新
		}

		// 更新エンティティ設定
		compositeEntity.setTrainingAttendanceResultEntity(updateEntity);
	}

	/**
	 * 複合エンティティ生成
	 * @param trainingAttendanceResultEntity 研修会出欠合否エンティティ
	 * @param trainingEntities 研修会リスト
	 * @return 複合エンティティ
	 */
	private TrainingAttendanceResultCompositeEntity createCompositeEntity(
			TrainingAttendanceResultEntity trainingAttendanceResultEntity, List<TrainingEntity> trainingEntities) {

		// 研修会出欠合否エンティティ(複合)
		TrainingAttendanceResultCompositeEntity compositeEntity = new TrainingAttendanceResultCompositeEntity();

		// 研修会番号
		String trainingNo = trainingAttendanceResultEntity.getTrainingNo();

		// 研修会出欠合否
		compositeEntity.setTrainingAttendanceResultEntity(trainingAttendanceResultEntity);

		// 研修会[親]
		TrainingEntity trainingEntity = trainingEntities.stream().filter(E -> E.getTrainingNo().equals(trainingNo))
				.findFirst().orElse(null);
		if (trainingEntity != null) {
			compositeEntity.setTrainingEntity(trainingEntity);
		}

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 研修会出欠合否エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<TrainingAttendanceResultCompositeEntity> createCompositeEntities(
			List<TrainingAttendanceResultEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<TrainingAttendanceResultCompositeEntity>(); // 空リスト
		}

		// 研修会番号配列取得
		String[] trainingNos = entities.stream().map(E -> E.getTrainingNo()).distinct().sorted().toArray(String[]::new);
		logger.debug("研修会番号[{}]: {}", trainingNos.length, getLogFormatText(trainingNos));

		// 研修会取得(一括)
		List<TrainingEntity> trainingEntities = trainingRep.getSearchResultByTrainingNos(trainingNos);

		return entities.stream().map(E -> createCompositeEntity(E, trainingEntities)).collect(Collectors.toList());
	}

	/**
	 * 複合エンティティリスト生成(外部結合)
	 * 研修会申込時に研修会出欠合否のレコードが生成されない場合を考慮して、研修会申込の内容を外部結合した結果を返却する
	 * @param searchDto 検索DTO
	 * @return 複合エンティティリスト
	 */
	private List<TrainingAttendanceResultCompositeEntity> createOuterJoinCompositeEntities(
			TrainingAttendanceResultSearchDto searchDto) {

		// 研修会出欠合否取得
		List<TrainingAttendanceResultEntity> entities = trainingAttendanceResultRep
				.getSearchResultByCondition(searchDto);

		// 研修会申込結合チェック
		if (BooleanUtils.isFalse(searchDto.getJoinApplicationFlag())) {
			return createCompositeEntities(entities); // 外部結合なし
		}

		// 研修会申込取得
		TrainingApplicationSearchDto applicationSearchDto = new TrainingApplicationSearchDto() {
			{
				setTrainingNos(searchDto.getTrainingNos()); // 研修会番号
				setMemberNo(searchDto.getMemberNo()); // 申込者番号
				setFullname(searchDto.getFullname()); // 氏名
			}
		};
		List<TrainingApplicationEntity> applicationEntities = trainingApplicationRep
				.getSearchResultByCondition(applicationSearchDto);
		if (CollectionUtils.isEmpty(applicationEntities)) {
			return createCompositeEntities(null); // 研修会申込なし
		}

		// 研修会申込重複チェック
		List<String> applicationKeys = applicationEntities.stream()
				.map(E -> String.format("%s_%d", E.getTrainingNo(), E.getMemberNo())).collect(Collectors.toList());
		if (applicationKeys.size() != new HashSet<>(applicationKeys).size()) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_DATA;
			String detail = String.format("applicationKeys=%s", applicationKeys);
			String message = String.format("研修会申込[関連]の研修会番号と申込者番号の組み合わせが重複しています");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		// 研修会申込外部結合
		TrainingAttendanceResultEntity entity = null;
		List<TrainingAttendanceResultEntity> outerJoinEntities = new ArrayList<TrainingAttendanceResultEntity>();
		for (TrainingApplicationEntity applicationEntity : applicationEntities) {

			// 研修会合否取得
			if ((entity = entities.stream()
					.filter(E -> E.getTrainingNo().equals(applicationEntity.getTrainingNo()))
					.filter(E -> E.getMemberNo().equals(applicationEntity.getMemberNo()))
					.findFirst().orElse(null)) == null) {

				// 研修会合否生成(未指定：id, versionNo)
				entity = TrainingAttendanceResultDto.createSingle().getFirstEntity()
						.getTrainingAttendanceResultEntity();
				entity.setTrainingNo(applicationEntity.getTrainingNo()); // 研修会番号
				entity.setMemberNo(applicationEntity.getMemberNo()); // 申込者番号
				entity.setFullname(applicationEntity.getFullname()); // 氏名
			}

			// 研修会合否追加
			outerJoinEntities.add(entity);
		}

		return createCompositeEntities(outerJoinEntities); // 外部結合あり

	}
	// TODO
}
