package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.FacilityCategoryRepository;
import jp.or.jaot.model.dto.FacilityCategoryDto;
import jp.or.jaot.model.dto.search.FacilityCategorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 施設分類サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class FacilityCategoryService extends BaseService {

	/** 施設分類リポジトリ */
	private final FacilityCategoryRepository facilityCategoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param facilityCategoryRep リポジトリ
	 */
	@Autowired
	public FacilityCategoryService(FacilityCategoryRepository facilityCategoryRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.facilityCategoryRep = facilityCategoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityCategoryDto getSearchResultByCondition(FacilityCategorySearchDto searchDto) {
		return FacilityCategoryDto.create().setEntities(facilityCategoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(FacilityCategoryDto dto) {
		insertEntity(facilityCategoryRep, dto.getFirstEntity());
	}

	// TODO
}
