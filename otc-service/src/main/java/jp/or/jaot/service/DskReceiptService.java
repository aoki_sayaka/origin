package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.DskReceiptRepository;
import jp.or.jaot.model.dto.DskReceiptDto;
import jp.or.jaot.model.entity.DskReceiptEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * DskPOSTリクエストサービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class DskReceiptService extends BaseService {

	/** DskPOSTリクエストリポジトリ */
	private DskReceiptRepository dskReceiptRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param DskReceiptRepository dskReceiptRep リポジトリ
	 */
	@Autowired
	public DskReceiptService(DskReceiptRepository dskReceiptRep) {
		super(ERROR_PLACE.BATCH_DSK);
		this.dskReceiptRep = dskReceiptRep;
	}

	/**
	 * エンティティをテーブルに登録する
	 * @param dto DskReceiptDto dto
	 */
	public void enter(DskReceiptDto dto) {

		for (DskReceiptEntity dre : dto.getEntities()) {
			insertEntity(dskReceiptRep, dre);
		}
	}

	/**
	 * 年度と会員番号を引数に速報値あり(true)／なし(false)を返す（速報取消がある場合は、「なし(false)」を返す）
	 * 年度と会員番号で検索し、IDが大きい(新しい)データが速報、速報取消かで判定
	 * @param memberNo 会員番号
	 * @param fiscalYear 年度 yyyy
	 * @return boolean
	 * @throws Exception
	 */
	public List<DskReceiptEntity> hasQuickAnnouncement(Integer memberNo, Integer fiscalYear) throws Exception {
		//会員番号を0埋め
		String strMemberNo = String.format("%06d", memberNo);
		//検索の為、請求年月日を作成
		String strFiscalYear = String.valueOf(fiscalYear);
		Date paymentDueDate = java.sql.Date.valueOf(strFiscalYear + "-12-31");

		//種別01、03のデータをリストに格納
		List<DskReceiptEntity> list = new ArrayList<>();
		//データを取得
		list.addAll(dskReceiptRep.getSearchResultByPaymentDueDate(strMemberNo, paymentDueDate));
		//id順でソート
		list.sort((i, j) -> (int) (j.getId() - i.getId()));

		return list;
	}

	// TODO

}
