package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.TrainingApplicationRepository;
import jp.or.jaot.domain.repository.TrainingRepository;
import jp.or.jaot.model.dto.TrainingApplicationDto;
import jp.or.jaot.model.dto.search.TrainingApplicationSearchDto;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import jp.or.jaot.model.entity.composite.TrainingApplicationCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会申込サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingApplicationService extends BaseService {

	/** 研修会申込リポジトリ */
	@Autowired
	private TrainingApplicationRepository trainingApplicationRep;

	/** 研修会リポジトリ[親] */
	@Autowired
	private TrainingRepository trainingRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public TrainingApplicationService() {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingApplicationDto getSearchResultByCondition(TrainingApplicationSearchDto searchDto) {
		return TrainingApplicationDto.create()
				.setEntities(createCompositeEntities(trainingApplicationRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingApplicationDto getSearchResultListByCondition(TrainingApplicationSearchDto searchDto) {
		return TrainingApplicationDto.create()
				.setEntities(createCompositeEntities(trainingApplicationRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(TrainingApplicationDto dto) {
		insertEntity(trainingApplicationRep, dto.getFirstEntity().getTrainingApplicationEntity());
	}

	/**
	 * 複合エンティティ生成
	 * @param trainingApplicationEntity 研修会申込エンティティ
	 * @param trainingEntities 研修会リスト
	 * @return 複合エンティティ
	 */
	private TrainingApplicationCompositeEntity createCompositeEntity(
			TrainingApplicationEntity trainingApplicationEntity, List<TrainingEntity> trainingEntities) {

		// 研修会エンティティ(複合)
		TrainingApplicationCompositeEntity compositeEntity = new TrainingApplicationCompositeEntity();

		// 研修会番号
		String trainingNo = trainingApplicationEntity.getTrainingNo();

		// 研修会申込
		compositeEntity.setTrainingApplicationEntity(trainingApplicationEntity);

		// 研修会[親]
		TrainingEntity trainingEntity = trainingEntities.stream().filter(E -> E.getTrainingNo().equals(trainingNo))
				.findFirst().orElse(null);
		if (trainingEntity != null) {
			compositeEntity.setTrainingEntity(trainingEntity);
		}

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 研修会申込エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<TrainingApplicationCompositeEntity> createCompositeEntities(List<TrainingApplicationEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<TrainingApplicationCompositeEntity>(); // 空リスト
		}

		// 研修会番号配列取得
		String[] trainingNos = entities.stream().map(E -> E.getTrainingNo()).distinct().sorted().toArray(String[]::new);
		logger.debug("研修会番号[{}]: {}", trainingNos.length, getLogFormatText(trainingNos));

		// 研修会取得(一括)
		List<TrainingEntity> trainingEntities = trainingRep.getSearchResultByTrainingNos(trainingNos);

		return entities.stream().map(E -> createCompositeEntity(E, trainingEntities)).collect(Collectors.toList());
	}

	// TODO
}
