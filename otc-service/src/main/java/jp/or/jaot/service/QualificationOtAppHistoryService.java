package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.QualificationOtAppHistoryRepository;
import jp.or.jaot.model.dto.QualificationOtAppHistoryDto;
import jp.or.jaot.model.dto.search.QualificationOtAppHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 認定作業療法士申請履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class QualificationOtAppHistoryService extends BaseService {

	/** 認定作業療法士申請履歴リポジトリ */
	private final QualificationOtAppHistoryRepository qualificationOtAppHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param attendingHistoryRep リポジトリ
	 */
	@Autowired
	public QualificationOtAppHistoryService(QualificationOtAppHistoryRepository qualificationOtAppHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.qualificationOtAppHistoryRep = qualificationOtAppHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public QualificationOtAppHistoryDto getSearchResult(Long id) {
		return QualificationOtAppHistoryDto.create().setEntity(qualificationOtAppHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public QualificationOtAppHistoryDto getSearchResultByCondition(QualificationOtAppHistorySearchDto searchDto) {
		return QualificationOtAppHistoryDto.create()
				.setEntities(qualificationOtAppHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(QualificationOtAppHistoryDto dto) {
		updateEntity(qualificationOtAppHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO


}
