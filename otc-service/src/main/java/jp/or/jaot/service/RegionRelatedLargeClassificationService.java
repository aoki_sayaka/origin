package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.RegionRelatedLargeClassificationRepository;
import jp.or.jaot.model.dto.RegionRelatedLargeClassificationDto;
import jp.or.jaot.model.dto.search.RegionRelatedLargeClassificationSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 領域関連大分類サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class RegionRelatedLargeClassificationService extends BaseService {

	/** 領域関連大分類リポジトリ */
	private final RegionRelatedLargeClassificationRepository regionRelatedLargeClassificationRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param regionRelatedLargeClassificationRep リポジトリ
	 */
	@Autowired
	public RegionRelatedLargeClassificationService(
			RegionRelatedLargeClassificationRepository regionRelatedLargeClassificationRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.regionRelatedLargeClassificationRep = regionRelatedLargeClassificationRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public RegionRelatedLargeClassificationDto getSearchResultByCondition(
			RegionRelatedLargeClassificationSearchDto searchDto) {
		return RegionRelatedLargeClassificationDto.create()
				.setEntities(regionRelatedLargeClassificationRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
