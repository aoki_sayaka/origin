package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.DepartmentHistoryRepository;
import jp.or.jaot.model.dto.DepartmentHistoryDto;
import jp.or.jaot.model.dto.search.DepartmentHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 部署履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class DepartmentHistoryService extends BaseService {

	/** 部署履歴リポジトリ */
	private final DepartmentHistoryRepository departmentHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param departmentHistoryRep リポジトリ
	 */
	@Autowired
	public DepartmentHistoryService(DepartmentHistoryRepository departmentHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.departmentHistoryRep = departmentHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public DepartmentHistoryDto getSearchResultByCondition(DepartmentHistorySearchDto searchDto) {
		return DepartmentHistoryDto.create().setEntities(departmentHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(DepartmentHistoryDto dto) {
		insertEntity(departmentHistoryRep, dto.getFirstEntity());
	}

	// TODO
}
