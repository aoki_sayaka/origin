package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.FacilityApplicationDomain;
import jp.or.jaot.domain.repository.FacilityApplicationCategoryRepository;
import jp.or.jaot.domain.repository.FacilityApplicationRepository;
import jp.or.jaot.model.dto.FacilityApplicationDto;
import jp.or.jaot.model.dto.search.FacilityApplicationSearchDto;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;
import jp.or.jaot.model.entity.FacilityApplicationEntity;
import jp.or.jaot.model.entity.composite.FacilityApplicationCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 施設登録申請サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class FacilityApplicationService extends BaseService {

	/** 施設登録申請リポジトリ */
	@Autowired
	private FacilityApplicationRepository facilityApplicationRep;

	/** 施設登録申請分類リポジトリ */
	@Autowired
	private FacilityApplicationCategoryRepository facilityApplicationCategoryRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public FacilityApplicationService() {
		super(ERROR_PLACE.LOGIC_MEMBER);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public FacilityApplicationDto getSearchResult(Long id) {
		return FacilityApplicationDto.create().setEntity(createCompositeEntity(
				facilityApplicationRep.getSearchResult(id),
				facilityApplicationCategoryRep.getSearchResultByApplicantId(id)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityApplicationDto getSearchResultByCondition(FacilityApplicationSearchDto searchDto) {
		return FacilityApplicationDto.create()
				.setEntities(createCompositeEntities(facilityApplicationRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityApplicationDto getSearchResultListByCondition(FacilityApplicationSearchDto searchDto) {
		return FacilityApplicationDto.create()
				.setEntities(createCompositeEntities(facilityApplicationRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録(正会員)
	 * @param dto DTO
	 */
	public void enterRegular(FacilityApplicationDto dto) {

		// 施設登録申請
		FacilityApplicationEntity entity = dto.getFirstEntity().getFacilityApplicationEntity();

		// 申請者区分コード(正会員)
		entity.setMemberCategoryCd(FacilityApplicationDomain.MEMBER_CATEGORY_CD.REGULAR.getValue());

		// 会員番号
		entity.setMemberNo(getAuthenticationMemberNo());

		// 申請ステータス(申請中)
		entity.setStatus(FacilityApplicationDomain.APPLICATION_STATUS.APPLYING.getValue());

		// 登録
		enter(dto);
	}

	/**
	 * 登録(仮会員)
	 * @param dto DTO
	 */
	public void enterTemporary(FacilityApplicationDto dto) {

		// 施設登録申請
		FacilityApplicationEntity entity = dto.getFirstEntity().getFacilityApplicationEntity();

		// 申請者区分コード(仮会員)
		entity.setMemberCategoryCd(FacilityApplicationDomain.MEMBER_CATEGORY_CD.TEMPORARY.getValue());

		// 仮会員番号
		// TODO

		// 申請ステータス
		// TODO

		// 登録
		enter(dto);
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(FacilityApplicationDto dto) {

		// 施設登録申請エンティティ(複合)
		FacilityApplicationCompositeEntity compositeEntity = dto.getFirstEntity();

		// 施設登録申請
		FacilityApplicationEntity facilityApplicationEntity = (FacilityApplicationEntity) insertEntity(
				facilityApplicationRep, compositeEntity.getFacilityApplicationEntity());

		// 施設登録申請分類
		for (FacilityApplicationCategoryEntity entity : compositeEntity.getFacilityApplicationCategoryEntities()) {
			entity.setApplicantId(facilityApplicationEntity.getId()); // 施設登録申請ID
			insertEntity(facilityApplicationCategoryRep, entity);
		}
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(FacilityApplicationDto dto) {

		// 施設登録申請エンティティ(複合)
		FacilityApplicationCompositeEntity compositeEntity = dto.getFirstEntity();

		// 施設登録申請
		FacilityApplicationEntity facilityApplicationEntity = compositeEntity.getFacilityApplicationEntity();
		updateEntity(facilityApplicationRep, facilityApplicationEntity, facilityApplicationEntity.getId());

		// 施設登録申請分類
		for (FacilityApplicationCategoryEntity entity : compositeEntity.getFacilityApplicationCategoryEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				entity.setApplicantId(facilityApplicationEntity.getId()); // 施設登録申請ID
				insertEntity(facilityApplicationCategoryRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(facilityApplicationCategoryRep, entity, id); // 削除(物理)
				} else {
					updateEntity(facilityApplicationCategoryRep, entity, id); // 更新
				}
			}
		}
	}

	/**
	 * 削除(物理)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public void delete(FacilityApplicationSearchDto searchDto) {

		// 対象検索
		for (FacilityApplicationEntity entity : facilityApplicationRep.getSearchResultByCondition(searchDto)) {

			// 施設登録申請分類
			for (FacilityApplicationCategoryEntity categoryEntity : facilityApplicationCategoryRep
					.getSearchResultByApplicantId(entity.getId())) {
				deleteEntity(facilityApplicationCategoryRep, categoryEntity, categoryEntity.getId());
			}

			// 施設登録申請
			deleteEntity(facilityApplicationRep, entity, entity.getId());
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param facilityApplicationEntity 施設登録申請エンティティ
	 * @param facilityApplicationCategoryEntities 施設登録申請分類リスト
	 * @return 複合エンティティ
	 */
	private FacilityApplicationCompositeEntity createCompositeEntity(
			FacilityApplicationEntity facilityApplicationEntity,
			List<FacilityApplicationCategoryEntity> facilityApplicationCategoryEntities) {

		// 施設登録申請エンティティ(複合)
		FacilityApplicationCompositeEntity compositeEntity = new FacilityApplicationCompositeEntity();

		// 施設登録申請ID
		Long applicantId = facilityApplicationEntity.getId();

		// 施設登録申請
		compositeEntity.setFacilityApplicationEntity(facilityApplicationEntity);

		// 施設登録申請分類
		compositeEntity.setFacilityApplicationCategoryEntities(facilityApplicationCategoryEntities.stream()
				.filter(E -> E.getApplicantId().equals(applicantId)).collect(Collectors.toList()));

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 施設登録申請エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<FacilityApplicationCompositeEntity> createCompositeEntities(List<FacilityApplicationEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<FacilityApplicationCompositeEntity>(); // 空リスト
		}

		// 施設登録申請ID配列取得
		Long[] applicantIds = entities.stream().map(E -> E.getId()).toArray(Long[]::new);
		logger.debug("施設登録申請ID[{}]: {}", applicantIds.length,
				Arrays.stream(applicantIds).map(E -> E.toString()).collect(Collectors.joining(",")));

		// 施設登録申請分類取得(一括)
		List<FacilityApplicationCategoryEntity> categoryEntities = facilityApplicationCategoryRep
				.getSearchResultByApplicantIds(applicantIds);

		return entities.stream().map(E -> createCompositeEntity(E, categoryEntities)).collect(Collectors.toList());
	}

	// TODO
}
