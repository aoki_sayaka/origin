package jp.or.jaot.service;

import java.sql.Timestamp;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.NoticeDestinationRepository;
import jp.or.jaot.model.dto.NoticeDestinationDto;
import jp.or.jaot.model.dto.search.NoticeDestinationSearchDto;
import jp.or.jaot.model.entity.NoticeDestinationEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * お知らせ(宛先)サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class NoticeDestinationService extends BaseService {

	/** お知らせ(宛先)リポジトリ */
	private final NoticeDestinationRepository noticeDestinationRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param noticeDestinationRep リポジトリ
	 */
	@Autowired
	public NoticeDestinationService(NoticeDestinationRepository noticeDestinationRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.noticeDestinationRep = noticeDestinationRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public NoticeDestinationDto getSearchResult(Long id) {
		return NoticeDestinationDto.create().setEntity(noticeDestinationRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public NoticeDestinationDto getSearchResultByCondition(NoticeDestinationSearchDto searchDto) {
		return NoticeDestinationDto.create().setEntities(noticeDestinationRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(NoticeDestinationDto dto) {
		insertEntity(noticeDestinationRep, dto.getFirstEntity());
	}

	/**
	 * 登録(自己)
	 * @param dto DTO
	 */
	public void enterMyself(NoticeDestinationDto dto) {

		// お知らせ(宛先)エンティティ
		NoticeDestinationEntity entity = dto.getFirstEntity();

		// 会員番号
		entity.setMemberNo(getAuthenticationMemberNo()); // 認証済み会員番号

		// 開封日時
		if (BooleanUtils.isTrue(entity.getReadFlg())) {
			// 既読
			entity.setReadTime(new Timestamp(System.currentTimeMillis())); // システム現在日時
		}

		// 登録
		insertEntity(noticeDestinationRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(NoticeDestinationDto dto) {
		updateEntity(noticeDestinationRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	/**
	 * 更新(自己)
	 * @param dto DTO
	 */
	public void updateMyself(NoticeDestinationDto dto) {

		// お知らせ(宛先)エンティティ
		NoticeDestinationEntity entity = dto.getFirstEntity();

		// 会員番号
		entity.setMemberNo(getAuthenticationMemberNo()); // 認証済み会員番号

		// 開封日時
		if (BooleanUtils.isTrue(entity.getReadFlg())) {
			// 既読
			entity.setReadTime(new Timestamp(System.currentTimeMillis())); // システム現在日時
		} else {
			// 未読(初期化)
			entity.setReadTime(null);
		}

		// 更新
		updateEntity(noticeDestinationRep, entity, entity.getId());
	}

	// TODO
}
