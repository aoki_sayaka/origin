package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ProfessionalOtTrainingHistoryRepository;
import jp.or.jaot.model.dto.ProfessionalOtTrainingHistoryDto;
import jp.or.jaot.model.dto.search.ProfessionalOtTrainingHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 専門作業療法士研修受講履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ProfessionalOtTrainingHistoryService extends BaseService {

	/** 専門作業療法士研修受講履歴リポジトリ */
	private final ProfessionalOtTrainingHistoryRepository professionalOtTrainingHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtTrainingHistoryRep リポジトリ
	 */
	@Autowired
	public ProfessionalOtTrainingHistoryService(
			ProfessionalOtTrainingHistoryRepository professionalOtTrainingHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.professionalOtTrainingHistoryRep = professionalOtTrainingHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public ProfessionalOtTrainingHistoryDto getSearchResult(Long id) {
		return ProfessionalOtTrainingHistoryDto.create()
				.setEntity(professionalOtTrainingHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ProfessionalOtTrainingHistoryDto getSearchResultByCondition(
			ProfessionalOtTrainingHistorySearchDto searchDto) {
		return ProfessionalOtTrainingHistoryDto.create()
				.setEntities(professionalOtTrainingHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(ProfessionalOtTrainingHistoryDto dto) {
		insertEntity(professionalOtTrainingHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(ProfessionalOtTrainingHistoryDto dto) {
		updateEntity(professionalOtTrainingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO

}
