package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MtdlpQualifyHistoryRepository;
import jp.or.jaot.model.dto.MtdlpQualifyHistoryDto;
import jp.or.jaot.model.dto.search.MtdlpQualifyHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * MTDLP認定履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MtdlpQualifyHistoryService extends BaseService {

	/** MTDLP認定履歴リポジトリ */
	private final MtdlpQualifyHistoryRepository mtdlpQualifyHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param mtdlpQualifyHistoryRep リポジトリ
	 */
	@Autowired
	public MtdlpQualifyHistoryService(MtdlpQualifyHistoryRepository mtdlpQualifyHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.mtdlpQualifyHistoryRep = mtdlpQualifyHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MtdlpQualifyHistoryDto getSearchResult(Long id) {
		return MtdlpQualifyHistoryDto.create().setEntity(mtdlpQualifyHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MtdlpQualifyHistoryDto getSearchResultByCondition(MtdlpQualifyHistorySearchDto searchDto) {
		return MtdlpQualifyHistoryDto.create()
				.setEntities(mtdlpQualifyHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MtdlpQualifyHistoryDto dto) {
		insertEntity(mtdlpQualifyHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(MtdlpQualifyHistoryDto dto) {
		updateEntity(mtdlpQualifyHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}
}
