package jp.or.jaot.service.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.or.jaot.domain.common.FileRepository;
import jp.or.jaot.model.dto.common.FileDto;

/**
 * ファイルサービス
 */
@Service
public class FileService {

	/** ファイルリポジトリ */
	private final FileRepository fileRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param otUserDao 利用者DAO
	 */
	@Autowired
	public FileService(FileRepository fileRep) {
		this.fileRep = fileRep;
	}

	/**
	 * アップロード
	 * @param dto ファイルDTO
	 */
	public void upload(FileDto dto) {
		fileRep.upload(dto);
	}
}
