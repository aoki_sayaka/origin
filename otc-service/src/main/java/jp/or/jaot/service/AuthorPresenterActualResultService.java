package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.AuthorPresenterActualFieldRepository;
import jp.or.jaot.domain.repository.AuthorPresenterActualResultRepository;
import jp.or.jaot.model.dto.AuthorPresenterActualResultDto;
import jp.or.jaot.model.dto.search.AuthorPresenterActualResultSearchDto;
import jp.or.jaot.model.entity.AuthorPresenterActualFieldEntity;
import jp.or.jaot.model.entity.AuthorPresenterActualResultEntity;
import jp.or.jaot.model.entity.composite.AuthorPresenterActualResultCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 活動実績(発表・著作等)サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class AuthorPresenterActualResultService extends BaseService {

	/** 活動実績(発表・著作等)リポジトリ */
	@Autowired
	private AuthorPresenterActualResultRepository authorPresenterActualResultRep;

	/** 活動実績の領域(発表・著作等)リポジトリ */
	@Autowired
	private AuthorPresenterActualFieldRepository authorPresenterActualFieldRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public AuthorPresenterActualResultService() {
		super(ERROR_PLACE.LOGIC_MEMBER);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public AuthorPresenterActualResultDto getSearchResult(Long id) {
		return AuthorPresenterActualResultDto.create()
				.setEntity(createCompositeEntity(authorPresenterActualResultRep.getSearchResult(id),
						authorPresenterActualFieldRep.getSearchResultByResultId(id)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public AuthorPresenterActualResultDto getSearchResultByCondition(AuthorPresenterActualResultSearchDto searchDto) {
		return AuthorPresenterActualResultDto.create().setEntities(
				createCompositeEntities(authorPresenterActualResultRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(AuthorPresenterActualResultDto dto) {

		// 活動実績(発表・著作等)エンティティ(複合)
		AuthorPresenterActualResultCompositeEntity compositeEntity = dto.getFirstEntity();

		// 活動実績(発表・著作等)
		AuthorPresenterActualResultEntity resultEntity = (AuthorPresenterActualResultEntity) insertEntity(
				authorPresenterActualResultRep, compositeEntity.getAuthorPresenterActualResultEntity());

		// 活動実績の領域(発表・著作等)
		for (AuthorPresenterActualFieldEntity fieldEntity : compositeEntity.getAuthorPresenterActualFieldEntities()) {
			fieldEntity.setResultId(resultEntity.getId()); // 活動実績ID
			insertEntity(authorPresenterActualFieldRep, fieldEntity);
		}
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(AuthorPresenterActualResultDto dto) {

		// 活動実績(発表・著作等)エンティティ(複合)
		AuthorPresenterActualResultCompositeEntity compositeEntity = dto.getFirstEntity();

		// 活動実績(発表・著作等)
		AuthorPresenterActualResultEntity updateEntity = compositeEntity.getAuthorPresenterActualResultEntity();
		updateEntity(authorPresenterActualResultRep, updateEntity, updateEntity.getId());

		// 活動実績の領域(発表・著作等)
		for (AuthorPresenterActualFieldEntity fieldEntity : compositeEntity.getAuthorPresenterActualFieldEntities()) {
			Long id = null;
			if ((id = fieldEntity.getId()) == null) {
				fieldEntity.setResultId(updateEntity.getId()); // 活動実績ID
				insertEntity(authorPresenterActualFieldRep, fieldEntity); // 追加
			} else {
				if (BooleanUtils.isTrue(fieldEntity.getDeleted())) {
					deleteEntity(authorPresenterActualFieldRep, fieldEntity, id); // 削除(物理)
				} else {
					updateEntity(authorPresenterActualFieldRep, fieldEntity, id); // 更新
				}
			}
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param resultEntity 活動実績エンティティ
	 * @param actualFiledEntities 活動実績の領域リスト
	 * @return 複合エンティティ
	 */
	private AuthorPresenterActualResultCompositeEntity createCompositeEntity(
			AuthorPresenterActualResultEntity resultEntity,
			List<AuthorPresenterActualFieldEntity> actualFiledEntities) {

		// 活動実績エンティティ(複合)
		AuthorPresenterActualResultCompositeEntity entity = new AuthorPresenterActualResultCompositeEntity();

		// 活動実績ID
		Long resultId = resultEntity.getId();

		// 活動実績
		entity.setAuthorPresenterActualResultEntity(resultEntity);

		// 活動実績の領域
		entity.setAuthorPresenterActualFieldEntities(actualFiledEntities.stream()
				.filter(E -> E.getResultId().equals(resultId)).collect(Collectors.toList()));

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities 活動実績エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<AuthorPresenterActualResultCompositeEntity> createCompositeEntities(
			List<AuthorPresenterActualResultEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<AuthorPresenterActualResultCompositeEntity>(); // 空リスト
		}

		// 活動実績ID配列取得
		Long[] resultIds = entities.stream().map(E -> E.getId()).toArray(Long[]::new);
		logger.debug("活動実績ID[{}]: {}", resultIds.length,
				Arrays.stream(resultIds).map(E -> E.toString()).collect(Collectors.joining(",")));

		// 活動実績の領域取得(一括)
		List<AuthorPresenterActualFieldEntity> actualFiledEntities = authorPresenterActualFieldRep
				.getSearchResultByResultIds(resultIds);

		return entities.stream().map(E -> createCompositeEntity(E, actualFiledEntities)).collect(Collectors.toList());
	}

	// TODO
}
