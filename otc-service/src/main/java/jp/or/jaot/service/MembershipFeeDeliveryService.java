package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MembershipFeeDeliveryRepository;
import jp.or.jaot.model.dto.MembershipFeeDeliveryDto;
import jp.or.jaot.model.dto.search.MembershipFeeDeliverySearchDto;
import jp.or.jaot.model.entity.MembershipFeeDeliveryEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 会費納入サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MembershipFeeDeliveryService extends BaseService {

	/** 会費納入リポジトリ */
	private final MembershipFeeDeliveryRepository membershipFeeDeliveryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param membershipFeeDeliveryRep リポジトリ
	 */
	@Autowired
	public MembershipFeeDeliveryService(MembershipFeeDeliveryRepository membershipFeeDeliveryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.membershipFeeDeliveryRep = membershipFeeDeliveryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MembershipFeeDeliveryDto getSearchResultByCondition(MembershipFeeDeliverySearchDto searchDto) {
		return MembershipFeeDeliveryDto.create()
				.setEntities(membershipFeeDeliveryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MembershipFeeDeliveryDto dto) {
		insertEntity(membershipFeeDeliveryRep, dto.getFirstEntity());
	}

	/**
	 * 会員番号を引数に入金あり(true)／なし(false)を返す
	 * 請求が0、かつ入金日がNULL、かつ支払残高が0、または
	 * 入金日がNULLではない、かつ支払残高が0で完納状態と判定
	 * @param memberNo 会員番号
	 * @return boolean
	 */
	public boolean isMembershipFeeComplete(Integer memberNo) {
		//会費納入テーブルから会員番号、年度で検索、データをリストに格納
		List<MembershipFeeDeliveryEntity> list = new ArrayList<>();
		list.addAll(membershipFeeDeliveryRep.getSearchResultByMemberNo(memberNo));
		int ans = 0;

		//listが空ではない
		if (list.size() != 0) {
			//完納状態か確認
			for (MembershipFeeDeliveryEntity ent : list) {
				//請求が0、かつ入金日がNULL、かつ支払残高が0
				if (ent.getBillingAmount() == 0 && ent.getPaymentDate() == null &&
						ent.getBillingBalance() == 0) {
					ans++;
				}
				//入金日がNULLではない、かつ支払残高が0
				if (ent.getPaymentDate() != null && ent.getBillingBalance() == 0) {
					ans++;				}
			}
			//全年度支払済みならtrue
			if (list.size() == ans) {
				return true;
			}
		}
		return false;
	}

	// TODO

}
