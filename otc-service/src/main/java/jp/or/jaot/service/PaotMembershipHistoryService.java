package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.PaotMembershipHistoryRepository;
import jp.or.jaot.model.dto.PaotMembershipHistoryDto;
import jp.or.jaot.model.dto.search.PaotMembershipHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 県士会在籍履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class PaotMembershipHistoryService extends BaseService {

	/** 県士会在籍履歴リポジトリ */
	private final PaotMembershipHistoryRepository paotMembershipHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param paotMembershipHistoryRep リポジトリ
	 */
	@Autowired
	public PaotMembershipHistoryService(PaotMembershipHistoryRepository paotMembershipHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.paotMembershipHistoryRep = paotMembershipHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public PaotMembershipHistoryDto getSearchResultByCondition(PaotMembershipHistorySearchDto searchDto) {
		return PaotMembershipHistoryDto.create()
				.setEntities(paotMembershipHistoryRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
