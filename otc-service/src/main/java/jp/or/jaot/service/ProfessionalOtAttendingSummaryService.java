package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ProfessionalOtAttendingSummaryRepository;
import jp.or.jaot.domain.repository.ProfessionalOtQualifyHistoryRepository;
import jp.or.jaot.domain.repository.ProfessionalOtTestHistoryRepository;
import jp.or.jaot.domain.repository.ProfessionalOtTrainingHistoryRepository;
import jp.or.jaot.model.dto.ProfessionalOtAttendingSummaryDto;
import jp.or.jaot.model.dto.search.ProfessionalOtAttendingSummarySearchDto;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.composite.ProfessionalOtAttendingSummaryCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 専門作業療法士受講履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ProfessionalOtAttendingSummaryService extends BaseService {

	/** 専門作業療法士受講履歴リポジトリ */
	@Autowired
	private ProfessionalOtAttendingSummaryRepository professionalOtAttendingSummaryRep;

	/** 専門作業療法士認定更新履歴リポジトリ */
	@Autowired
	private ProfessionalOtQualifyHistoryRepository professionalOtQualifyHistoryRep;

	/** 専門作業療法士試験結果履歴リポジトリ */
	@Autowired
	private ProfessionalOtTestHistoryRepository professionalOtTestHistoryRep;

	/** 専門作業療法士研修受講履歴リポジトリ */
	@Autowired
	private ProfessionalOtTrainingHistoryRepository professionalOtTrainingHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param professionalOtAttendingSummaryRep リポジトリ
	 */
	@Autowired
	public ProfessionalOtAttendingSummaryService(
			ProfessionalOtAttendingSummaryRepository professionalOtAttendingSummaryRep) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY);
		this.professionalOtAttendingSummaryRep = professionalOtAttendingSummaryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public ProfessionalOtAttendingSummaryDto getSearchResult(Long id) {
		return ProfessionalOtAttendingSummaryDto.create()
				.setEntity(createCompositeEntity(professionalOtAttendingSummaryRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	public ProfessionalOtAttendingSummaryDto getSearchResultByProfessionalOtAttendingSummaryNo(Integer memberNo) {
		return ProfessionalOtAttendingSummaryDto.create().setEntity(
				createCompositeEntity(professionalOtAttendingSummaryRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ProfessionalOtAttendingSummaryDto getSearchResultByCondition(
			ProfessionalOtAttendingSummarySearchDto searchDto) {

		return ProfessionalOtAttendingSummaryDto.create().setEntities(
				createCompositeEntities(professionalOtAttendingSummaryRep.getSearchResultByCondition(searchDto)));
	}

	/**
	* 検索結果取得(ログイン者)
	* @param searchDto 検索DTO
	* @return DTO
	*/
	public ProfessionalOtAttendingSummaryDto getSearchMyselfResultByCondition(
			ProfessionalOtAttendingSummarySearchDto searchDto) {
		searchDto.setMemberNo(getAuthenticationMemberNo()); // 会員番号

		return ProfessionalOtAttendingSummaryDto.create().setEntities(
				createCompositeEntities(professionalOtAttendingSummaryRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(ProfessionalOtAttendingSummaryDto dto) {
		// 専門作業療法士受講履歴エンティティ(複合)
		ProfessionalOtAttendingSummaryCompositeEntity compositeEntity = dto.getFirstEntity();

		// 会員
		insertEntity(professionalOtAttendingSummaryRep, compositeEntity.getProfessionalOtAttendingSummaryEntity());

		// 専門作業療法士認定更新履歴
		compositeEntity.getProfessionalOtQualifyHistoryEntities().stream()
				.forEach(E -> insertEntity(professionalOtQualifyHistoryRep, E));

		// 専門作業療法士試験結果履歴
		compositeEntity.getProfessionalOtTestHistoryEntities().stream()
				.forEach(E -> insertEntity(professionalOtTestHistoryRep, E));

		// 専門作業療法士研修受講履歴
		compositeEntity.getProfessionalOtTrainingHistoryEntities().stream()
				.forEach(E -> insertEntity(professionalOtTrainingHistoryRep, E));
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(ProfessionalOtAttendingSummaryDto dto) {

		// 専門作業療法士受講履歴エンティティ(複合)
		ProfessionalOtAttendingSummaryCompositeEntity compositeEntity = dto.getFirstEntity();

		// 会員
		ProfessionalOtAttendingSummaryEntity memberEntity = compositeEntity.getProfessionalOtAttendingSummaryEntity();
		updateEntity(professionalOtAttendingSummaryRep, memberEntity, memberEntity.getId());

		// 専門作業療法士認定更新履歴
		for (ProfessionalOtQualifyHistoryEntity entity : compositeEntity.getProfessionalOtQualifyHistoryEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				insertEntity(professionalOtQualifyHistoryRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(professionalOtQualifyHistoryRep, entity, id); // 削除(物理)
				} else {
					updateEntity(professionalOtQualifyHistoryRep, entity, id); // 更新
				}
			}
		}

		// 専門作業療法士試験結果履歴
		for (ProfessionalOtTestHistoryEntity entity : compositeEntity.getProfessionalOtTestHistoryEntities()) {
			updateEntity(professionalOtTestHistoryRep, entity, entity.getId()); // 更新
		}

		// 専門作業療法士研修受講履歴
		for (ProfessionalOtTrainingHistoryEntity entity : compositeEntity.getProfessionalOtTrainingHistoryEntities()) {
			updateEntity(professionalOtTrainingHistoryRep, entity, entity.getId()); // 更新
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param professionalOtAttendingSummaryEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private ProfessionalOtAttendingSummaryCompositeEntity createCompositeEntity(
			ProfessionalOtAttendingSummaryEntity professionalOtAttendingSummaryEntity) {

		// 専門作業療法士受講履歴エンティティ(複合)
		ProfessionalOtAttendingSummaryCompositeEntity entity = new ProfessionalOtAttendingSummaryCompositeEntity();

		// 会員番号
		Integer menberNo = professionalOtAttendingSummaryEntity.getMemberNo();

		// 専門作業療法士受講履歴
		entity.setProfessionalOtAttendingSummaryEntity(professionalOtAttendingSummaryEntity);

		// 専門作業療法士認定更新履歴
		try {
			entity.setProfessionalOtQualifyHistoryEntities(
					professionalOtQualifyHistoryRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		// 専門作業療法士試験結果履歴
		try {
			entity.setProfessionalOtTestHistoryEntities(
					professionalOtTestHistoryRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		//  専門作業療法士研修受講履歴
		try {
			entity.setProfessionalOtTrainingHistoryEntities(
					professionalOtTrainingHistoryRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param professionalOtAttendingSummaryEntity 専門作業療法士受講履歴エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<ProfessionalOtAttendingSummaryCompositeEntity> createCompositeEntities(
			List<ProfessionalOtAttendingSummaryEntity> professionalOtAttendingSummaryEntities) {
		return professionalOtAttendingSummaryEntities.stream().map(E -> createCompositeEntity(E))
				.collect(Collectors.toList());
	}

	/**
	 * 会員番号で検索、リストに格納
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtAttendingSummaryEntity> getSearchResultByProfessionalOtAttendingSummaryMemberNo(
			Integer memberNo) {
		//エンティティリストを取得
		List<ProfessionalOtAttendingSummaryEntity> list = new ArrayList<>();
		list.addAll(professionalOtAttendingSummaryRep.getSearchResultByProfessionalField(memberNo));

		return list;
	}
	// TODO

}
