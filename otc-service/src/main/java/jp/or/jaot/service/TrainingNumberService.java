package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.TrainingNumberRepository;
import jp.or.jaot.model.dto.TrainingNumberDto;
import jp.or.jaot.model.dto.search.TrainingNumberSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修番号サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingNumberService extends BaseService {

	/** 研修番号リポジトリ */
	private final TrainingNumberRepository trainingNumberRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingNumberRep リポジトリ
	 */
	@Autowired
	public TrainingNumberService(TrainingNumberRepository trainingNumberRep) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY);
		this.trainingNumberRep = trainingNumberRep;
	}

	/**
	 * 検索結果リスト取得(年度)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingNumberDto getSearchResultListByFiscalYear(Integer fiscalYear) {
		return TrainingNumberDto.create().setEntities(trainingNumberRep.getSearchResultByFiscalYear(fiscalYear));
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingNumberDto getSearchResultListByCondition(TrainingNumberSearchDto searchDto) {
		return TrainingNumberDto.create().setEntities(trainingNumberRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
