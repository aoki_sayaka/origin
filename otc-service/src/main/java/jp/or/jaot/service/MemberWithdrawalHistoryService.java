package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberWithdrawalHistoryRepository;
import jp.or.jaot.model.dto.MemberWithdrawalHistoryDto;
import jp.or.jaot.model.dto.search.MemberWithdrawalHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 退会履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberWithdrawalHistoryService extends BaseService {

	/** 退会履歴リポジトリ */
	private final MemberWithdrawalHistoryRepository memberWithrawalHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param recessHistoryRep リポジトリ
	 */
	@Autowired
	public MemberWithdrawalHistoryService(MemberWithdrawalHistoryRepository memberWithrawalHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberWithrawalHistoryRep = memberWithrawalHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberWithdrawalHistoryDto getSearchResultByCondition(MemberWithdrawalHistorySearchDto searchDto) {
		return MemberWithdrawalHistoryDto.create().setEntities(memberWithrawalHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberWithdrawalHistoryDto dto) {
		insertEntity(memberWithrawalHistoryRep, dto.getFirstEntity());
	}

	// TODO
}
