package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.UserRoleRepository;
import jp.or.jaot.model.dto.UserRoleDto;
import jp.or.jaot.model.dto.search.UserRoleSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 事務局ロールサービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class UserRoleService extends BaseService {

	/** 事務局ロールリポジトリ */
	private final UserRoleRepository userRoleRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param userRoleRep リポジトリ
	 */
	@Autowired
	public UserRoleService(UserRoleRepository userRoleRep) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
		this.userRoleRep = userRoleRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public UserRoleDto getSearchResultByCondition(UserRoleSearchDto searchDto) {
		return UserRoleDto.create().setEntities(userRoleRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(UserRoleDto dto) {
		insertEntity(userRoleRep, dto.getFirstEntity());
	}

	// TODO
}
