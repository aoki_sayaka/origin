package jp.or.jaot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.model.dto.HistoryFieldDefinitionDto;
import jp.or.jaot.model.dto.search.HistoryFieldDefinitionSearchDto;
import jp.or.jaot.model.entity.HistoryFieldDefinitionEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * 履歴フィールド定義サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class HistoryFieldDefinitionService extends BaseService {

	/**
	 * コンストラクタ
	 */
	@Autowired
	public HistoryFieldDefinitionService() {
		super(ERROR_PLACE.LOGIC_COMMON);
	}

	/**
	 * キャッシュ結果取得
	 * @return エンティティリスト
	 */
	public List<HistoryFieldDefinitionEntity> getCacheResult() {
		return historyFieldDefinitionRep.getCacheResult();
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public HistoryFieldDefinitionDto getSearchResultByCondition(HistoryFieldDefinitionSearchDto searchDto) {
		return HistoryFieldDefinitionDto.create()
				.setEntities(historyFieldDefinitionRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 * @return 追加したエンティティ
	 */
	public HistoryFieldDefinitionEntity enter(HistoryFieldDefinitionDto dto) {
		return (HistoryFieldDefinitionEntity) insertEntity(historyFieldDefinitionRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(HistoryFieldDefinitionDto dto) {
		updateEntity(historyFieldDefinitionRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	/**
	 * 削除
	 * @param dto DTO
	 */
	public void delete(HistoryFieldDefinitionDto dto) {
		deleteEntity(historyTableDefinitionRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
}
