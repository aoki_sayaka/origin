
package jp.or.jaot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MtdlpCaseReportRepository;
import jp.or.jaot.domain.repository.MtdlpQualifyHistoryRepository;
import jp.or.jaot.domain.repository.MtdlpSummaryRepository;
import jp.or.jaot.domain.repository.MtdlpTrainingHistoryRepository;
import jp.or.jaot.model.dto.MtdlpSummaryDto;
import jp.or.jaot.model.dto.search.MtdlpSummarySearchDto;
import jp.or.jaot.model.entity.MtdlpCaseReportEntity;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;
import jp.or.jaot.model.entity.MtdlpSummaryEntity;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;
import jp.or.jaot.model.entity.composite.MtdlpCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * MTDLP履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MtdlpSummaryService extends BaseService {

	/** MTDLP履歴リポジトリ */
	@Autowired
	private MtdlpSummaryRepository mtdlpSummaryRep;

	/** MTDLP認定履歴リポジトリ */
	@Autowired
	private MtdlpQualifyHistoryRepository mtdlpQualifyHistoryRep;

	/** MTDLP研修受講履歴リポジトリ */
	@Autowired
	private MtdlpTrainingHistoryRepository mtdlpTrainingHistoryRep;

	/** MTDLP事例リポジトリ */
//	@Autowired
//	private MtdlpCaseRepository mtdlpCaseRep;

	/** MTDLP事例報告リポジトリ */
	@Autowired
	private MtdlpCaseReportRepository mtdlpCaseReportRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtAttendingSummaryRep リポジトリ
	 */
	@Autowired
	public MtdlpSummaryService(MtdlpSummaryRepository mtdlpSummaryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.mtdlpSummaryRep = mtdlpSummaryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MtdlpSummaryDto getSearchResult(Long id) {
		return MtdlpSummaryDto.create().setEntity(createCompositeEntity(mtdlpSummaryRep.getSearchResult(id)));
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return DTO
	 */
	public MtdlpSummaryDto getSearchResultByMtdlpSummaryNo(Integer memberNo) {
		return MtdlpSummaryDto.create()
				.setEntity(createCompositeEntity(mtdlpSummaryRep.getSearchResultByMemberNo(memberNo)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MtdlpSummaryDto getSearchResultByCondition(MtdlpSummarySearchDto searchDto) {

		return MtdlpSummaryDto.create()
				.setEntities(createCompositeEntities(mtdlpSummaryRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MtdlpSummaryDto dto) {
		// MTDLP履歴エンティティ(複合)
		MtdlpCompositeEntity compositeEntity = dto.getFirstEntity();

		// MTDLP履歴
		insertEntity(mtdlpSummaryRep, compositeEntity.getMtdlpSummaryEntity());

		// MTDLP認定履歴
		compositeEntity.getMtdlpQualifyHistoryEntities().stream().forEach(E -> insertEntity(mtdlpQualifyHistoryRep, E));

		// MTDLP研修受講履歴
		compositeEntity.getMtdlpTrainingHistoryEntities().stream()
				.forEach(E -> insertEntity(mtdlpTrainingHistoryRep, E));

		// MTDLP事例
//		compositeEntity.getMtdlpCaseEntities().stream().forEach(E -> insertEntity(mtdlpCaseRep, E));

		// MTDLP事例報告
		compositeEntity.getMtdlpCaseReportEntities().stream().forEach(E -> insertEntity(mtdlpCaseReportRep, E));
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(MtdlpSummaryDto dto) {

		// MTDLP履歴エンティティ(複合)
		MtdlpCompositeEntity compositeEntity = dto.getFirstEntity();

		// MTDLP履歴
		MtdlpSummaryEntity mtdlpSummaryEntity = compositeEntity.getMtdlpSummaryEntity();
		updateEntity(mtdlpSummaryRep, mtdlpSummaryEntity, mtdlpSummaryEntity.getId());

		// MTDLP認定履歴
		for (MtdlpQualifyHistoryEntity entity : compositeEntity.getMtdlpQualifyHistoryEntities()) {
			Long id = null;
			if ((id = entity.getId()) == null) {
				insertEntity(mtdlpQualifyHistoryRep, entity); // 追加
			} else {
				if (BooleanUtils.isTrue(entity.getDeleted())) {
					deleteEntity(mtdlpQualifyHistoryRep, entity, id); // 削除(物理)
				} else {
					updateEntity(mtdlpQualifyHistoryRep, entity, id); // 更新
				}
			}
		}

		// MTDLP研修受講履歴
		for (MtdlpTrainingHistoryEntity entity : compositeEntity.getMtdlpTrainingHistoryEntities()) {
			updateEntity(mtdlpTrainingHistoryRep, entity, entity.getId()); // 更新
		}

		// MTDLP事例
//		for (MtdlpCaseEntity entity : compositeEntity.getMtdlpCaseEntities()) {
//			updateEntity(mtdlpCaseRep, entity, entity.getId()); // 更新
//		}

		// MTDLP事例報告
		for (MtdlpCaseReportEntity entity : compositeEntity.getMtdlpCaseReportEntities()) {
			updateEntity(mtdlpCaseReportRep, entity, entity.getId()); // 更新
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param mtdlpSummaryEntity 会員エンティティ
	 * @return 複合エンティティ
	 */
	private MtdlpCompositeEntity createCompositeEntity(MtdlpSummaryEntity mtdlpSummaryEntity) {

		// MTDLP履歴エンティティ(複合)
		MtdlpCompositeEntity entity = new MtdlpCompositeEntity();

		// 会員番号
		Integer menberNo = mtdlpSummaryEntity.getMemberNo();

		// MTDLP履歴
		entity.setMtdlpSummaryEntity(mtdlpSummaryEntity);

		// MTDLP認定履歴
		try {
			entity.setMtdlpQualifyHistoryEntities(mtdlpQualifyHistoryRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		//  MTDLP研修受講履歴
		try {
			entity.setMtdlpTrainingHistoryEntities(mtdlpTrainingHistoryRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		//  MTDLP事例
//		try {
//			entity.setMtdlpCaseEntities(mtdlpCaseRep.getSearchResultByMemberNo(menberNo));
//		} catch (SearchBusinessException e) {
//			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
//		}

		//  MTDLP事例報告
		try {
			entity.setMtdlpCaseReportEntities(mtdlpCaseReportRep.getSearchResultByMemberNo(menberNo));
		} catch (SearchBusinessException e) {
			logger.info("{} : {}", e.getMessage(), e.getDetail()); // ログを出力して継続
		}

		return entity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param mtdlpSummaryEntity MTDLP履歴エンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<MtdlpCompositeEntity> createCompositeEntities(List<MtdlpSummaryEntity> mtdlpSummaryEntities) {
		return mtdlpSummaryEntities.stream().map(E -> createCompositeEntity(E)).collect(Collectors.toList());
	}
	// TODO

}
