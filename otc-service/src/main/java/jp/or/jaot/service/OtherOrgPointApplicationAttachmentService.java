package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.OtherOrgPointApplicationAttachmentRepository;
import jp.or.jaot.model.dto.OtherOrgPointApplicationAttachmentDto;
import jp.or.jaot.model.dto.search.OtherOrgPointApplicationAttachmentSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 他団体・SIGポイント添付ファイルサービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class OtherOrgPointApplicationAttachmentService extends BaseService {

	/** 他団体・SIGポイント添付ファイルリポジトリ */
	private final OtherOrgPointApplicationAttachmentRepository otherOrgPointApplicationAttachmentRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param otherOrgPointApplicationAttachmentRep リポジトリ
	 */
	@Autowired
	public OtherOrgPointApplicationAttachmentService(OtherOrgPointApplicationAttachmentRepository otherOrgPointApplicationAttachmentRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.otherOrgPointApplicationAttachmentRep = otherOrgPointApplicationAttachmentRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public OtherOrgPointApplicationAttachmentDto getSearchResult(Long id) {
		return OtherOrgPointApplicationAttachmentDto.create().setEntity(otherOrgPointApplicationAttachmentRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public OtherOrgPointApplicationAttachmentDto getSearchResultByCondition(OtherOrgPointApplicationAttachmentSearchDto searchDto) {
		return OtherOrgPointApplicationAttachmentDto.create()
				.setEntities(otherOrgPointApplicationAttachmentRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(OtherOrgPointApplicationAttachmentDto dto) {
		insertEntity(otherOrgPointApplicationAttachmentRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(OtherOrgPointApplicationAttachmentDto dto) {
		updateEntity(otherOrgPointApplicationAttachmentRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO

}
