package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.SocialContributionActual2FieldRepository;
import jp.or.jaot.model.dto.SocialContributionActual2FieldDto;
import jp.or.jaot.model.dto.search.SocialContributionActual2FieldSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 活動実績の領域2(後輩育成・社会貢献)サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class SocialContributionActual2FieldService extends BaseService {

	/** 活動実績の領域2(後輩育成・社会貢献)リポジトリ */
	private final SocialContributionActual2FieldRepository socialContributionActual2FieldRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param socialContributionActual2FieldRep リポジトリ
	 */
	@Autowired
	public SocialContributionActual2FieldService(
			SocialContributionActual2FieldRepository socialContributionActual2FieldRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.socialContributionActual2FieldRep = socialContributionActual2FieldRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public SocialContributionActual2FieldDto getSearchResult(Long id) {
		return SocialContributionActual2FieldDto.create()
				.setEntity(socialContributionActual2FieldRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public SocialContributionActual2FieldDto getSearchResultByCondition(
			SocialContributionActual2FieldSearchDto searchDto) {
		return SocialContributionActual2FieldDto.create()
				.setEntities(socialContributionActual2FieldRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(SocialContributionActual2FieldDto dto) {
		insertEntity(socialContributionActual2FieldRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(SocialContributionActual2FieldDto dto) {
		updateEntity(socialContributionActual2FieldRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
}
