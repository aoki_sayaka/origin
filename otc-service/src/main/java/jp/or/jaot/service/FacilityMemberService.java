package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.FacilityMemberRepository;
import jp.or.jaot.model.dto.FacilityMemberDto;
import jp.or.jaot.model.dto.search.FacilityMemberSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 施設養成校勤務者サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class FacilityMemberService extends BaseService {

	/** 施設養成校勤務者リポジトリ */
	private final FacilityMemberRepository facilityMemberRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param facilityMemberRep リポジトリ
	 */
	@Autowired
	public FacilityMemberService(FacilityMemberRepository facilityMemberRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.facilityMemberRep = facilityMemberRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public FacilityMemberDto getSearchResultByCondition(FacilityMemberSearchDto searchDto) {
		return FacilityMemberDto.create().setEntities(facilityMemberRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(FacilityMemberDto dto) {
		insertEntity(facilityMemberRep, dto.getFirstEntity());
	}

	// TODO
}
