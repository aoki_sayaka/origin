package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.OfficerHistoryRepository;
import jp.or.jaot.model.dto.OfficerHistoryDto;
import jp.or.jaot.model.dto.search.OfficerHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 役員履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class OfficerHistoryService extends BaseService {

	/** 役員履歴リポジトリ */
	private final OfficerHistoryRepository officerHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param officerHistoryRep リポジトリ
	 */
	@Autowired
	public OfficerHistoryService(OfficerHistoryRepository officerHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.officerHistoryRep = officerHistoryRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public OfficerHistoryDto getSearchResultByCondition(OfficerHistorySearchDto searchDto) {
		return OfficerHistoryDto.create().setEntities(officerHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(OfficerHistoryDto dto) {
		insertEntity(officerHistoryRep, dto.getFirstEntity());
	}

	// TODO
}
