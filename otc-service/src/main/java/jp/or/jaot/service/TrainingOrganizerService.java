package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.TrainingOrganizerRepository;
import jp.or.jaot.model.dto.TrainingOrganizerDto;
import jp.or.jaot.model.dto.search.TrainingOrganizerSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 研修会主催者サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class TrainingOrganizerService extends BaseService {

	/** 研修会主催者リポジトリ */
	private final TrainingOrganizerRepository trainingOrganizerRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingOrganizerRep リポジトリ
	 */
	@Autowired
	public TrainingOrganizerService(TrainingOrganizerRepository trainingOrganizerRep) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT);
		this.trainingOrganizerRep = trainingOrganizerRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public TrainingOrganizerDto getSearchResultByCondition(TrainingOrganizerSearchDto searchDto) {
		return TrainingOrganizerDto.create().setEntities(trainingOrganizerRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(TrainingOrganizerDto dto) {
		insertEntity(trainingOrganizerRep, dto.getFirstEntity());
	}

	// TODO
}
