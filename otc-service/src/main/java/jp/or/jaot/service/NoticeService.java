package jp.or.jaot.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.NoticeConditionPrefRepository;
import jp.or.jaot.domain.repository.NoticeDestinationRepository;
import jp.or.jaot.domain.repository.NoticeRepository;
import jp.or.jaot.model.dto.NoticeDto;
import jp.or.jaot.model.dto.search.NoticeDestinationSearchDto;
import jp.or.jaot.model.dto.search.NoticeSearchDto;
import jp.or.jaot.model.entity.NoticeConditionPrefEntity;
import jp.or.jaot.model.entity.NoticeDestinationEntity;
import jp.or.jaot.model.entity.NoticeEntity;
import jp.or.jaot.model.entity.composite.NoticeCompositeEntity;
import jp.or.jaot.service.common.BaseService;

/**
 * お知らせサービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class NoticeService extends BaseService {

	/** お知らせリポジトリ */
	@Autowired
	private NoticeRepository noticeRep;

	/** お知らせ(都道府県)リポジトリ */
	@Autowired
	private NoticeConditionPrefRepository noticeConditionPrefRep;

	/** お知らせ(宛先)リポジトリ */
	@Autowired
	private NoticeDestinationRepository noticeDestinationRep;

	/**
	 * コンストラクタ
	 */
	@Autowired
	public NoticeService() {
		super(ERROR_PLACE.LOGIC_MEMBER);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public NoticeDto getSearchResult(Long id) {
		return NoticeDto.create()
				.setEntity(createCompositeEntity(noticeRep.getSearchResult(id),
						noticeConditionPrefRep.getSearchResultByResultId(id),
						noticeDestinationRep.getSearchResultByResultId(id)));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public NoticeDto getSearchResultByCondition(NoticeSearchDto searchDto) {
		return NoticeDto.create()
				.setEntities(createCompositeEntities(noticeRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 検索結果取得(条件(自己))
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public NoticeDto getSearchResultMyselfByCondition(NoticeSearchDto searchDto) {
		return NoticeDto.create()
				.setEntities(createCompositeMyselfEntities(noticeRep.getSearchResultByCondition(searchDto)));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(NoticeDto dto) {

		// お知らせエンティティ(複合)
		NoticeCompositeEntity compositeEntity = dto.getFirstEntity();

		// お知らせ
		NoticeEntity noticeEntity = (NoticeEntity) insertEntity(noticeRep, compositeEntity.getNoticeEntity());

		// お知らせ(都道府県)
		for (NoticeConditionPrefEntity entity : compositeEntity.getNoticeConditionPrefEntities()) {
			entity.setNoticeId(noticeEntity.getId()); // お知らせID
			insertEntity(noticeConditionPrefRep, entity);
		}

		// お知らせ(宛先)
		for (NoticeDestinationEntity entity : compositeEntity.getNoticeDestinationEntities()) {
			entity.setNoticeId(noticeEntity.getId()); // お知らせID
			insertEntity(noticeConditionPrefRep, entity);
		}
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(NoticeDto dto) {

		// お知らせエンティティ(複合)
		NoticeCompositeEntity compositeEntity = dto.getFirstEntity();

		// お知らせ
		NoticeEntity noticeEntity = compositeEntity.getNoticeEntity();
		updateEntity(noticeRep, noticeEntity, noticeEntity.getId());

		// お知らせ(都道府県)
		for (NoticeConditionPrefEntity entity : compositeEntity.getNoticeConditionPrefEntities()) {
			updateEntity(noticeConditionPrefRep, entity, entity.getId());
		}

		// お知らせ(宛先)
		for (NoticeDestinationEntity entity : compositeEntity.getNoticeDestinationEntities()) {
			updateEntity(noticeDestinationRep, entity, entity.getId());
		}
	}

	/**
	 * 複合エンティティ生成
	 * @param noticeEntity お知らせエンティティ
	 * @param prefEntities お知らせ(都道府県)リスト
	 * @param destinationEntities お知らせ(宛先)リスト
	 * @return 複合エンティティ
	 */
	private NoticeCompositeEntity createCompositeEntity(NoticeEntity noticeEntity,
			List<NoticeConditionPrefEntity> prefEntities,
			List<NoticeDestinationEntity> destinationEntities) {

		// お知らせエンティティ(複合)
		NoticeCompositeEntity compositeEntity = new NoticeCompositeEntity();

		// お知らせID
		Long noticeId = noticeEntity.getId();

		// お知らせ
		compositeEntity.setNoticeEntity(noticeEntity);

		// お知らせ(都道府県)
		compositeEntity.setNoticeConditionPrefEntities(prefEntities.stream()
				.filter(E -> E.getNoticeId().equals(noticeId)).collect(Collectors.toList()));

		// お知らせ(宛先)
		compositeEntity.setNoticeDestinationEntities(destinationEntities.stream()
				.filter(E -> E.getNoticeId().equals(noticeId)).collect(Collectors.toList()));

		return compositeEntity;
	}

	/**
	 * 複合エンティティリスト生成
	 * @param entities お知らせエンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<NoticeCompositeEntity> createCompositeEntities(List<NoticeEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<NoticeCompositeEntity>(); // 空リスト
		}

		// お知らせID配列取得
		Long[] resultIds = entities.stream().map(E -> E.getId()).toArray(Long[]::new);
		logger.debug("お知らせID[{}]: {}", resultIds.length,
				Arrays.stream(resultIds).map(E -> E.toString()).collect(Collectors.joining(",")));

		// お知らせ(都道府県)取得(一括)
		List<NoticeConditionPrefEntity> prefEntities = noticeConditionPrefRep.getSearchResultByResultIds(resultIds);

		// お知らせ(宛先)取得(一括)
		List<NoticeDestinationEntity> destinationEntities = noticeDestinationRep.getSearchResultByResultIds(resultIds);

		return entities.stream().map(E -> createCompositeEntity(E, prefEntities, destinationEntities))
				.collect(Collectors.toList());
	}

	/**
	 * 複合エンティティリスト生成(自己)
	 * @param entities お知らせエンティティリスト
	 * @return 複合エンティティリスト
	 */
	private List<NoticeCompositeEntity> createCompositeMyselfEntities(List<NoticeEntity> entities) {

		// エンティティ数チェック
		if (CollectionUtils.isEmpty(entities)) {
			return new ArrayList<NoticeCompositeEntity>(); // 空リスト
		}

		// お知らせ(都道府県)取得(空リスト)
		List<NoticeConditionPrefEntity> prefEntities = new ArrayList<NoticeConditionPrefEntity>();

		// お知らせ(宛先)取得(会員番号)
		NoticeDestinationSearchDto destinationSearchDto = new NoticeDestinationSearchDto();
		destinationSearchDto.setMemberNo(getAuthenticationMemberNo()); // 認証済み会員番号
		List<NoticeDestinationEntity> destinationEntities = noticeDestinationRep
				.getSearchResultByCondition(destinationSearchDto);

		return entities.stream().map(E -> createCompositeEntity(E, prefEntities, destinationEntities))
				.collect(Collectors.toList());
	}

	// TODO
}
