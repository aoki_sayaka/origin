package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MtdlpCaseReportRepository;
import jp.or.jaot.model.dto.MtdlpCaseReportDto;
import jp.or.jaot.model.dto.search.MtdlpCaseReportSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * MTDLP事例報告サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MtdlpCaseReportService extends BaseService {

	/** MTDLP事例報告リポジトリ */
	private final MtdlpCaseReportRepository mtdlpCaseReportRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param mtdlpCaseRep リポジトリ
	 */
	@Autowired
	public MtdlpCaseReportService(MtdlpCaseReportRepository mtdlpCaseReportRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.mtdlpCaseReportRep = mtdlpCaseReportRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MtdlpCaseReportDto getSearchResult(Long id) {
		return MtdlpCaseReportDto.create().setEntity(mtdlpCaseReportRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MtdlpCaseReportDto getSearchResultByCondition(MtdlpCaseReportSearchDto searchDto) {
		return MtdlpCaseReportDto.create()
				.setEntities(mtdlpCaseReportRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MtdlpCaseReportDto dto) {
		insertEntity(mtdlpCaseReportRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(MtdlpCaseReportDto dto) {
		updateEntity(mtdlpCaseReportRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}
}
