package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.QualificationOtHistoryRepository;
import jp.or.jaot.model.dto.QualificationOtHistoryDto;
import jp.or.jaot.model.dto.search.QualificationOtHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 認定作業療法士履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class QualificationOtHistoryService extends BaseService{

	/** 認定作業療法士履歴リポジトリ */
	private final QualificationOtHistoryRepository qualificationOtHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param attendingHistoryRep リポジトリ
	 */
	@Autowired
	public QualificationOtHistoryService(QualificationOtHistoryRepository qualificationOtHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.qualificationOtHistoryRep = qualificationOtHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public QualificationOtHistoryDto getSearchResult(Long id) {
		return QualificationOtHistoryDto.create().setEntity(qualificationOtHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public QualificationOtHistoryDto getSearchResultByCondition(QualificationOtHistorySearchDto searchDto) {
		return QualificationOtHistoryDto.create()
				.setEntities(qualificationOtHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(QualificationOtHistoryDto dto) {
		updateEntity(qualificationOtHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
	

}
