package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.BasicOtTrainingHistoryRepository;
import jp.or.jaot.model.dto.BasicOtTrainingHistoryDto;
import jp.or.jaot.model.dto.search.BasicOtTrainingHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 基礎研修受講履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class BasicOtTrainingHistoryService extends BaseService {

	/** 基礎研修受講履歴リポジトリ */
	private final BasicOtTrainingHistoryRepository basicOtTrainingHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtTrainingHistoryRep リポジトリ
	 */
	@Autowired
	public BasicOtTrainingHistoryService(BasicOtTrainingHistoryRepository basicOtTrainingHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.basicOtTrainingHistoryRep = basicOtTrainingHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public BasicOtTrainingHistoryDto getSearchResult(Long id) {
		return BasicOtTrainingHistoryDto.create().setEntity(basicOtTrainingHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public BasicOtTrainingHistoryDto getSearchResultByCondition(BasicOtTrainingHistorySearchDto searchDto) {
		return BasicOtTrainingHistoryDto.create()
				.setEntities(basicOtTrainingHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(BasicOtTrainingHistoryDto dto) {
		insertEntity(basicOtTrainingHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(BasicOtTrainingHistoryDto dto) {
		updateEntity(basicOtTrainingHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO

}
