package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.ZipcodeRepository;
import jp.or.jaot.model.dto.ZipcodeDto;
import jp.or.jaot.model.dto.search.ZipcodeSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 郵便番号サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ZipcodeService extends BaseService {

	/** 郵便番号リポジトリ */
	private final ZipcodeRepository zipcodeRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param zipcodeRep リポジトリ
	 */
	@Autowired
	public ZipcodeService(ZipcodeRepository zipcodeRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.zipcodeRep = zipcodeRep;
	}

	/**
	 * 検索結果リスト取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public ZipcodeDto getSearchResultListByCondition(ZipcodeSearchDto searchDto) {
		return ZipcodeDto.create().setEntities(zipcodeRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
