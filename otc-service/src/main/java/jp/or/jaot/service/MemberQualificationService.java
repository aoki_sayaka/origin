package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.MemberQualificationRepository;
import jp.or.jaot.model.dto.MemberQualificationDto;
import jp.or.jaot.model.dto.search.MemberQualificationSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 会員関連資格情報サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class MemberQualificationService extends BaseService {

	/** 会員関連資格情報リポジトリ */
	private final MemberQualificationRepository memberQualificationRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberQualificationRep リポジトリ
	 */
	@Autowired
	public MemberQualificationService(MemberQualificationRepository memberQualificationRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.memberQualificationRep = memberQualificationRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public MemberQualificationDto getSearchResult(Long id) {
		return MemberQualificationDto.create().setEntity(memberQualificationRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public MemberQualificationDto getSearchResultByCondition(MemberQualificationSearchDto searchDto) {
		return MemberQualificationDto.create()
				.setEntities(memberQualificationRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(MemberQualificationDto dto) {
		insertEntity(memberQualificationRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(MemberQualificationDto dto) {
		updateEntity(memberQualificationRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO
}
