package jp.or.jaot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.domain.repository.UserRepository;
import jp.or.jaot.model.entity.UserEntity;
import lombok.AllArgsConstructor;

/**
 *  事務局ユーザサービス(詳細)<br>
 *  UserDetailsServiceを実装した認証時に使用するクラス
 */
@Service
@AllArgsConstructor
public class SecretariatUserDetailsService implements UserDetailsService {

	/** ロガーインスタンス */
	private final Logger logger = LoggerFactory.getLogger(SecretariatUserDetailsService.class);

	/** 事務局ユーザリポジトリ */
	private final UserRepository userRep;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException {

		// 検索結果取得(ログインID)
		UserEntity userEntity = null;
		try {
			userEntity = userRep.getSearchResultByLoginId(accountName);
		} catch (Exception e) {
			if (e instanceof SearchBusinessException) {
				SearchBusinessException sbe = (SearchBusinessException) e;
				logger.error("{} : {}", sbe.getMessage(), sbe.getDetail());
			} else {
				logger.error(e.getMessage());
			}
			throw new UsernameNotFoundException("");
		}

		return userEntity;
	}

	/**
	 * 利用可能状態取得(パス/キー値)
	 * @param id ID
	 * @param pathOrKey パス/キー値
	 * @return 利用可能=true, 利用不可=false
	 */
	public boolean isAvailablePathOrKey(Long id, String pathOrKey) {
		return userRep.isAvailablePathOrKey(id, pathOrKey);
	}
}
