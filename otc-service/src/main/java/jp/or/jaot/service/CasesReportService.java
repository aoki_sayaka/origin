package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.CasesReportRepository;
import jp.or.jaot.model.dto.CasesReportDto;
import jp.or.jaot.model.dto.search.CasesReportSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 事例報告サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class CasesReportService extends BaseService {

	/** 事例報告リポジトリ */
	private final CasesReportRepository casesReportRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param casesReportRep リポジトリ
	 */
	@Autowired
	public CasesReportService(CasesReportRepository casesReportRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.casesReportRep = casesReportRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public CasesReportDto getSearchResultByCondition(CasesReportSearchDto searchDto) {
		return CasesReportDto.create().setEntities(casesReportRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
