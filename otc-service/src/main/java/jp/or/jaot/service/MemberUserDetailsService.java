package jp.or.jaot.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import jp.or.jaot.domain.repository.MemberRepository;
import jp.or.jaot.model.entity.MemberEntity;
import lombok.AllArgsConstructor;

/**
 *  会員サービス(詳細)<br>
 *  UserDetailsServiceを実装した認証時に使用するクラス
 */
@Service
@AllArgsConstructor
public class MemberUserDetailsService implements UserDetailsService {

	/** 会員リポジトリ */
	private final MemberRepository memberRep;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserDetails loadUserByUsername(String accountName) throws UsernameNotFoundException {

		// 変換(アカウント名 -> 会員番号)
		Integer memberNo = null;
		try {
			memberNo = Integer.parseInt(accountName);
		} catch (NumberFormatException e) {
			throw new UsernameNotFoundException("");
		}

		// 検索結果取得(会員番号)
		MemberEntity memberEntity = null;
		try {
			memberEntity = memberRep.getSearchResultByMemberNo(memberNo);
		} catch (Exception e) {
			throw new UsernameNotFoundException("");
		}

		return memberEntity;
	}

	/**
	 * 利用可能状態取得(パス/キー値)
	 * @param memberNo 会員番号
	 * @param pathOrKey パス/キー値
	 * @return 利用可能=true, 利用不可=false
	 */
	public boolean isAvailablePathOrKey(Integer memberNo, String pathOrKey) {
		return memberRep.isAvailablePathOrKey(memberNo, pathOrKey);
	}
}
