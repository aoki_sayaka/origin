package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.QualifiedOtCompletionHistoryRepository;
import jp.or.jaot.model.dto.QualifiedOtCompletionHistoryDto;
import jp.or.jaot.model.dto.search.QualifiedOtCompletionHistorySearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 認定作業療法士研修修了認定更新履歴サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class QualifiedOtCompletionHistoryService extends BaseService {

	/** 認定作業療法士研修修了認定更新履歴リポジトリ */
	private final QualifiedOtCompletionHistoryRepository qualifiedOtCompletionHistoryRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtAttendingSummaryRep リポジトリ
	 */
	@Autowired
	public QualifiedOtCompletionHistoryService(QualifiedOtCompletionHistoryRepository qualifiedOtCompletionHistoryRep) {
		super(ERROR_PLACE.LOGIC_MEMBER);
		this.qualifiedOtCompletionHistoryRep = qualifiedOtCompletionHistoryRep;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return DTO
	 */
	public QualifiedOtCompletionHistoryDto getSearchResult(Long id) {
		return QualifiedOtCompletionHistoryDto.create().setEntity(qualifiedOtCompletionHistoryRep.getSearchResult(id));
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public QualifiedOtCompletionHistoryDto getSearchResultByCondition(QualifiedOtCompletionHistorySearchDto searchDto) {
		return QualifiedOtCompletionHistoryDto.create()
				.setEntities(qualifiedOtCompletionHistoryRep.getSearchResultByCondition(searchDto));
	}

	/**
	 * 登録
	 * @param dto DTO
	 */
	public void enter(QualifiedOtCompletionHistoryDto dto) {
		insertEntity(qualifiedOtCompletionHistoryRep, dto.getFirstEntity());
	}

	/**
	 * 更新
	 * @param dto DTO
	 */
	public void update(QualifiedOtCompletionHistoryDto dto) {
		updateEntity(qualifiedOtCompletionHistoryRep, dto.getFirstEntity(), dto.getFirstEntity().getId());
	}

	// TODO

}