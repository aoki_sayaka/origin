package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.domain.repository.OtcRepository;

/**
 * サービス
 * @version 疎通確認用
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class OtcService {

	/** モックリポジトリ */
	@Autowired
	private OtcRepository otcRep;

	/**
	 * 利用者検索
	 */
	public void getSearchResult() {
		otcRep.getSearchResult();
	}

	/**
	 * メール送信
	 */
	public void sendMail() {
		otcRep.sendMail();
	}
}
