package jp.or.jaot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.domain.repository.QualificationRepository;
import jp.or.jaot.model.dto.QualificationDto;
import jp.or.jaot.model.dto.search.QualificationSearchDto;
import jp.or.jaot.service.common.BaseService;

/**
 * 関連資格サービス
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class QualificationService extends BaseService {

	/** 関連資格リポジトリ */
	private final QualificationRepository qualificationRep;

	/**
	 * コンストラクタ(インジェクション)
	 * @param qualificationRep リポジトリ
	 */
	@Autowired
	public QualificationService(QualificationRepository qualificationRep) {
		super(ERROR_PLACE.LOGIC_COMMON);
		this.qualificationRep = qualificationRep;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return DTO
	 */
	public QualificationDto getSearchResultByCondition(QualificationSearchDto searchDto) {
		return QualificationDto.create().setEntities(qualificationRep.getSearchResultByCondition(searchDto));
	}

	// TODO
}
