package jp.or.jaot.utils;

import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jp.or.jaot.core.util.PropertyUtility;

/**
 * VelocityAPI に関するユーティリティクラスです.<br>
 * シングルトンクラスです.
 */
public final class VelocityUtility {

	/** ロギングクラス */
	private static final Logger logger = LoggerFactory.getLogger(VelocityUtility.class);

	/** テンプレートディレクトリ */
	private static final String TEMPLATE_DIR = "template/vm/";

	/** プロパティファイル名 */
	private static final String PROPERTY_NAME = "velocity.properties";

	/** 唯一のインスタンス */
	private static final VelocityUtility INSTANCE = new VelocityUtility();

	/** プロパティファイルの内容 */
	private static final Properties properties = PropertyUtility.read(PROPERTY_NAME);

	/**
	 * デフォルトコンストラクタ
	 */
	private VelocityUtility() {
	}

	/**
	 * インスタンスを取得します.
	 * @return アプリ内で唯一のインスタンスを返します.
	 */
	public static VelocityUtility getInstance() {
		return INSTANCE;
	}

	/**
	 * 指定したテンプレートと変数Mapから、文面を作成します.<br>
	 * 引数の templateFileNameに null または 空文字列を指定すると、nullを返します.<br>
	 * 引数の variableMapに nullを指定すると、nullを返します.<br>
	 * @param templateFileName velocityテンプレートファイル名(ファイルは、velocity.properties のキー：file.resource.loader.path からの相対パスであること)
	 * @param variableMap コンテキスト変数Map(キー：テンプレートで参照する際に使用する変数名、値：対応するオブジェクト)
	 * @return テンプレートに変数情報を当てはめた文面を返します.作成に失敗した時は、 nullを返します.
	 */
	public String createPostText(String templateFileName, Map<String, Object> variableMap) {

		// 引数チェック
		if (StringUtils.isEmpty(templateFileName)) {
			logger.error("velocityテンプレートファイル名が null または 空文字列が設定されています");
			return null;
		}
		if (variableMap == null) {
			logger.error("コンテキスト変数Mapに nullが設定されています");
			return null;
		}

		// テンプレートファイルのパス
		String templatePath = TEMPLATE_DIR + templateFileName;
		logger.debug("テンプレートファイル : templatePath={}", templatePath);
		String postText = null;
		try {
			// 初期化
			Velocity.init(properties);

			// Velocityコンテキストに値を設定
			VelocityContext context = new VelocityContext();
			for (Map.Entry<String, Object> e : variableMap.entrySet()) {
				context.put(e.getKey(), e.getValue());
			}

			// テンプレートを作成・マージ
			StringWriter writer = new StringWriter();
			Template tmplate = Velocity.getTemplate(templatePath);
			tmplate.merge(context, writer);

			// 戻り値に文字列を出力する
			postText = writer.toString();
		} catch (Exception e) {
			// 実行時例外が発生したら、ログを出力し処理は続行する(連続処理のケースを考慮)
			logger.error(e.getMessage(), e);
		}

		return postText;
	}
}
