package jp.or.jaot.utils;

import java.util.Properties;
import java.util.TreeMap;

import jp.or.jaot.core.util.PropertyUtility;

/**
 * 定義プロパティファイルに関するユーティリティクラスです.<br>
 * シングルトンクラスです.
 */
public final class DefinitionUtility {

	/** 唯一のインスタンス */
	private static final DefinitionUtility INSTANCE = new DefinitionUtility();

	/** プロパティファイルの内容(テーブル) */
	private static final Properties tableProperties = PropertyUtility.read("table.properties");

	/** プロパティファイルの内容(テーブル) */
	private static final Properties entityProperties = PropertyUtility.read("entity.properties");

	/**
	 * デフォルトコンストラクタ
	 */
	private DefinitionUtility() {
	}

	/**
	 * インスタンスを取得します.
	 * @return アプリ内で唯一のインスタンスを返します.
	 */
	public static DefinitionUtility getInstance() {
		return INSTANCE;
	}

	/**
	 * プロパティ取得(テーブル)
	 * @return プロパティ
	 */
	public static Properties getTableProperties() {
		return tableProperties;
	}

	/**
	 * プロパティ取得(エンティティ)
	 * @return プロパティ
	 */
	public static Properties getEntityProperties() {
		return entityProperties;
	}

	/**
	 * プロパティマップ取得(テーブル)
	 * @return プロパティマップ
	 */
	public TreeMap<Object, Object> getTablePropertyMap() {
		return new TreeMap<Object, Object>(tableProperties);
	}

	/**
	 * プロパティマップ取得(エンティティ)
	 * @return プロパティマップ
	 */
	public TreeMap<Object, Object> getEntityPropertyMap() {
		return new TreeMap<Object, Object>(entityProperties);
	}
}
