package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberRevokedAuthorityDao;
import jp.or.jaot.domain.MemberRevokedAuthorityDomain;
import jp.or.jaot.model.dto.search.MemberRevokedAuthoritySearchDto;
import jp.or.jaot.model.entity.MemberRevokedAuthorityEntity;

/**
 * 会員権限リポジトリ
 */
@Repository
public class MemberRevokedAuthorityRepository
		extends BaseRepository<MemberRevokedAuthorityDomain, MemberRevokedAuthorityEntity> {

	/** 会員権限DAO */
	private final MemberRevokedAuthorityDao memberRevokedAuthorityDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberRevokedAuthorityDao DAO
	 */
	@Autowired
	public MemberRevokedAuthorityRepository(MemberRevokedAuthorityDao memberRevokedAuthorityDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberRevokedAuthorityDao);
		this.memberRevokedAuthorityDao = memberRevokedAuthorityDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberRevokedAuthorityEntity getSearchResult(Long id) {
		MemberRevokedAuthorityEntity entity = null;
		if ((id == null) || ((entity = memberRevokedAuthorityDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員権限が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberRevokedAuthorityEntity> getSearchResultByCondition(MemberRevokedAuthoritySearchDto searchDto) {

		// 検索
		List<MemberRevokedAuthorityEntity> entities = memberRevokedAuthorityDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberRevokedAuthorityEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員権限検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
