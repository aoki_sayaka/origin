package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MtdlpCaseReportEntity;

/**
 * MTDLP事例報告ドメイン
 */
@Component
@Scope("prototype")
public class MtdlpCaseReportDomain extends BaseDomain<MtdlpCaseReportEntity> {

	/**
	 * コンストラクタ
	 */
	public MtdlpCaseReportDomain() {
		setEntity(new MtdlpCaseReportEntity()); // 初期エンティティ
	}

	// TODO
}
