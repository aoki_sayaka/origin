package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.DepartmentHistoryEntity;

/**
 * 部署履歴ドメイン
 */
@Component
@Scope("prototype")
public class DepartmentHistoryDomain extends BaseDomain<DepartmentHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public DepartmentHistoryDomain() {
		setEntity(new DepartmentHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
