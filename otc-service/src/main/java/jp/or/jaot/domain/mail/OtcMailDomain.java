package jp.or.jaot.domain.mail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseMailDomain;
import jp.or.jaot.core.model.MailContentDto;
import jp.or.jaot.utils.VelocityUtility;

/**
 * メールドメイン
 * @version 疎通確認用
 */
@Component
@Scope("prototype")
public class OtcMailDomain extends BaseMailDomain {

	/** velocityテンプレートファイル名 */
	private static String DEF_TEMPLATE_FILE_NAME = "OtcMail.vm";

	/**
	 * メール送信
	 */
	public void sendMail() {

		// メール情報DTO生成
		MailContentDto mailDto = new MailContentDto();

		// 送信先設定
		List<String> toAddressList = new ArrayList<String>() {
			{
				add("otc@ark-info-sys.co.jp");
			}
		};
		mailDto.setToAddressList(toAddressList);

		// 本文設定
		Map<String, Object> variableMap = new HashMap<String, Object>() {
			{
				put("accountName", "otc");
				put("lastName", "山田");
				put("firstName", "太郎");
			}
		};
		mailDto.setBody(VelocityUtility.getInstance().createPostText(DEF_TEMPLATE_FILE_NAME, variableMap));

		// メール送信
		sendMail(mailDto, null);
	}
}
