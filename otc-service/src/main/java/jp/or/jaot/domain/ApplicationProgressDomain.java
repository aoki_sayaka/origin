package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ApplicationProgressEntity;

/**
 * 申請進捗ドメイン
 */
@Component
@Scope("prototype")
public class ApplicationProgressDomain extends BaseDomain<ApplicationProgressEntity> {

	/**
	 * コンストラクタ
	 */
	public ApplicationProgressDomain() {
		setEntity(new ApplicationProgressEntity()); // 初期エンティティ
	}

	// TODO
}
