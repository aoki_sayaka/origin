package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.SocialContributionActualResultEntity;

/**
 * 活動実績(後輩育成・社会貢献)ドメイン
 */
@Component
@Scope("prototype")
public class SocialContributionActualResultDomain extends BaseDomain<SocialContributionActualResultEntity> {

	/**
	 * コンストラクタ
	 */
	public SocialContributionActualResultDomain() {
		setEntity(new SocialContributionActualResultEntity()); // 初期エンティティ
	}

	// TODO
}
