package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.UserRoleDao;
import jp.or.jaot.domain.UserRoleDomain;
import jp.or.jaot.model.dto.search.UserRoleSearchDto;
import jp.or.jaot.model.entity.UserRoleEntity;

/**
 * 事務局ロールリポジトリ
 */
@Repository
public class UserRoleRepository extends BaseRepository<UserRoleDomain, UserRoleEntity> {

	/** 事務局ロールDAO */
	private final UserRoleDao userRoleDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param userRoleDao DAO
	 */
	@Autowired
	public UserRoleRepository(UserRoleDao userRoleDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, userRoleDao);
		this.userRoleDao = userRoleDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public UserRoleEntity getSearchResult(Long id) {
		UserRoleEntity entity = null;
		if ((id == null) || ((entity = userRoleDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する事務局ロールが存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<UserRoleEntity> getSearchResultByCondition(UserRoleSearchDto searchDto) {

		// 検索
		List<UserRoleEntity> entities = userRoleDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<UserRoleEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("事務局ロール検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
