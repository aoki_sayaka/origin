package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.NoticeConditionPrefEntity;

/**
 * お知らせ(都道府県)ドメイン
 */
@Component
@Scope("prototype")
public class NoticeConditionPrefDomain extends BaseDomain<NoticeConditionPrefEntity> {

	/**
	 * コンストラクタ
	 */
	public NoticeConditionPrefDomain() {
		setEntity(new NoticeConditionPrefEntity()); // 初期エンティティ
	}

	// TODO
}
