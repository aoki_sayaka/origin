package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.BasicOtAttendingSummaryDao;
import jp.or.jaot.domain.BasicOtAttendingSummaryDomain;
import jp.or.jaot.model.dto.search.BasicOtAttendingSummarySearchDto;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;

/**
 * 基礎研修履歴リポジトリ
 */
@Repository
public class BasicOtAttendingSummaryRepository
		extends BaseRepository<BasicOtAttendingSummaryDomain, BasicOtAttendingSummaryEntity> {

	/** 基礎研修履歴DAO */
	private final BasicOtAttendingSummaryDao basicOtAttendingSummaryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtAttendingSummaryDao DAO
	 */
	@Autowired
	public BasicOtAttendingSummaryRepository(BasicOtAttendingSummaryDao basicOtAttendingSummaryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, basicOtAttendingSummaryDao);
		this.basicOtAttendingSummaryDao = basicOtAttendingSummaryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public BasicOtAttendingSummaryEntity getSearchResult(Long id) {
		BasicOtAttendingSummaryEntity entity = null;
		if ((id == null) || ((entity = basicOtAttendingSummaryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する基礎研修受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public BasicOtAttendingSummaryEntity getSearchResultByMemberNo(Integer memberNo) {
		BasicOtAttendingSummaryEntity entity = null;
		if ((memberNo == null) || ((entity = basicOtAttendingSummaryDao.findByMemberNo(memberNo)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("memberNo=%s", memberNo);
			String message = String.format("検索条件に指定された会員番号に該当する基礎研修受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNos 会員番号配列
	 * @return エンティティリスト
	 */
	public List<BasicOtAttendingSummaryEntity> getSearchResultByMemberNos(Integer[] memberNos) {

		// 検索条件生成
		BasicOtAttendingSummarySearchDto searchDto = new BasicOtAttendingSummarySearchDto();
		searchDto.setMemberNos(memberNos); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<BasicOtAttendingSummaryEntity> getSearchResultByCondition(BasicOtAttendingSummarySearchDto searchDto) {

		// 検索
		List<BasicOtAttendingSummaryEntity> entities = basicOtAttendingSummaryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<BasicOtAttendingSummaryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("基礎研修履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
