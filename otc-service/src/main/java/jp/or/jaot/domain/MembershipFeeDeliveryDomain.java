package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MembershipFeeDeliveryEntity;

/**
 * 会費納入ドメイン
 */
@Component
@Scope("prototype")
public class MembershipFeeDeliveryDomain extends BaseDomain<MembershipFeeDeliveryEntity> {

	/**
	 * コンストラクタ
	 */
	public MembershipFeeDeliveryDomain() {
		setEntity(new MembershipFeeDeliveryEntity()); // 初期エンティティ
	}

	// TODO
}
