package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.HistoryTableDefinitionEntity;

/**
 * 履歴テーブル定義ドメイン
 */
@Component
@Scope("prototype")
public class HistoryTableDefinitionDomain extends BaseDomain<HistoryTableDefinitionEntity> {

	/**
	 * コンストラクタ
	 */
	public HistoryTableDefinitionDomain() {
		setEntity(new HistoryTableDefinitionEntity()); // 初期エンティティ
	}

	// TODO
}
