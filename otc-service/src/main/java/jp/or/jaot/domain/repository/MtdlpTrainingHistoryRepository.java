package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MtdlpTrainingHistoryDao;
import jp.or.jaot.domain.MtdlpTrainingHistoryDomain;
import jp.or.jaot.model.dto.search.MtdlpTrainingHistorySearchDto;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;

/**
 * MTDLP研修受講履歴リポジトリ
 */
@Repository
public class MtdlpTrainingHistoryRepository
		extends BaseRepository<MtdlpTrainingHistoryDomain, MtdlpTrainingHistoryEntity> {

	/** MTDLP研修受講履歴DAO */
	private final MtdlpTrainingHistoryDao mtdlpTrainingHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param mtdlpTrainingHistoryDao DAO
	 */
	@Autowired
	public MtdlpTrainingHistoryRepository(MtdlpTrainingHistoryDao mtdlpTrainingHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, mtdlpTrainingHistoryDao);
		this.mtdlpTrainingHistoryDao = mtdlpTrainingHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MtdlpTrainingHistoryEntity getSearchResult(Long id) {
		MtdlpTrainingHistoryEntity entity = null;
		if ((id == null) || ((entity = mtdlpTrainingHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定された会員番号に該当するMTDLP研修受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MtdlpTrainingHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MtdlpTrainingHistorySearchDto searchDto = new MtdlpTrainingHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MtdlpTrainingHistoryEntity> getSearchResultByCondition(
			MtdlpTrainingHistorySearchDto searchDto) {

		// 検索
		List<MtdlpTrainingHistoryEntity> entities = mtdlpTrainingHistoryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MtdlpTrainingHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("MTDLP研修受講履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
