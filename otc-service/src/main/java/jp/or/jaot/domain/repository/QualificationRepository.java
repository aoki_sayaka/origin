package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.QualificationDao;
import jp.or.jaot.domain.QualificationDomain;
import jp.or.jaot.model.dto.search.QualificationSearchDto;
import jp.or.jaot.model.entity.QualificationEntity;

/**
 * 関連資格リポジトリ
 */
@Repository
public class QualificationRepository extends BaseRepository<QualificationDomain, QualificationEntity> {

	/** 関連資格コードDAO */
	private final QualificationDao qualificationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param qualificationDao DAO
	 */
	@Autowired
	public QualificationRepository(QualificationDao qualificationDao) {
		super(ERROR_PLACE.LOGIC_COMMON, qualificationDao);
		this.qualificationDao = qualificationDao;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<QualificationEntity> getSearchResultByCondition(QualificationSearchDto searchDto) {

		// 検索
		List<QualificationEntity> entities = qualificationDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<QualificationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("関連資格検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
