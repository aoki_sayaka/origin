package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.QualificationOtAttendingHistoryDao;
import jp.or.jaot.domain.QualificationOtAttendingHistoryDomain;
import jp.or.jaot.model.dto.search.QualificationOtAttendingHistorySearchDto;
import jp.or.jaot.model.entity.QualificationOtAttendingHistoryEntity;

/**
 * 認定作業療法士研修受講履歴リポジトリ
 */
@Repository
public class QualificationOtAttendingHistoryRepository
		extends BaseRepository<QualificationOtAttendingHistoryDomain, QualificationOtAttendingHistoryEntity> {

	/** 認定作業療法士研修受講履歴DAO */
	private final QualificationOtAttendingHistoryDao qualificationOtAttendingHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param qualificationOtAttendingHistoryDao DAO
	 */
	@Autowired
	public QualificationOtAttendingHistoryRepository(
			QualificationOtAttendingHistoryDao qualificationOtAttendingHistoryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, qualificationOtAttendingHistoryDao);
		this.qualificationOtAttendingHistoryDao = qualificationOtAttendingHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public QualificationOtAttendingHistoryEntity getSearchResult(Long id) {
		QualificationOtAttendingHistoryEntity entity = null;
		if ((id == null) || ((entity = qualificationOtAttendingHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する認定作業療法士研修受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<QualificationOtAttendingHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		QualificationOtAttendingHistorySearchDto searchDto = new QualificationOtAttendingHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<QualificationOtAttendingHistoryEntity> getSearchResultByCondition(
			QualificationOtAttendingHistorySearchDto searchDto) {

		// 検索
		List<QualificationOtAttendingHistoryEntity> entities = qualificationOtAttendingHistoryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<QualificationOtAttendingHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("認定作業療法士研修受講履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
