package jp.or.jaot.domain;

import java.util.Arrays;
import java.util.Objects;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.domain.MemberFacilityCategoryDomain.MEMBER_FACILITY_CATEGORY_DIVISION;
import jp.or.jaot.model.entity.FacilityCategoryEntity;

/**
 * 施設分類ドメイン
 */
@Component
@Scope("prototype")
public class FacilityCategoryDomain extends BaseDomain<FacilityCategoryEntity> {

	/**
	 * 分類区分の列挙体
	 */
	public enum CATEGORY_DIVISION {

		/** 大分類 */
		LARGE(1),
		/** 中分類 */
		MIDDLE(2),
		/** 小分類 */
		SMALL(3);

		/** 値 */
		private final Integer value;

		/**
		 * コンストラクタ
		 * @param value 値
		 */
		private CATEGORY_DIVISION(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static CATEGORY_DIVISION valueOf(Integer value) {
			return Arrays.stream(CATEGORY_DIVISION.values()).filter(V -> V.getValue().equals(value)).findFirst()
					.orElse(null);
		}

		/**
		 * 会員勤務先業務分類の分類区分から変換する
		 * @param value
		 * @return
		 */
		public static CATEGORY_DIVISION convertFromMemberFacilityCategoryDivision(
				MEMBER_FACILITY_CATEGORY_DIVISION value) {
			switch (value) {
			case LARGE:
				return CATEGORY_DIVISION.LARGE;
			case MIDDLE:
				return CATEGORY_DIVISION.MIDDLE;
			case MIDDLE_SUB:
				return CATEGORY_DIVISION.MIDDLE;
			case SMALL:
				return CATEGORY_DIVISION.SMALL;
			}
			return null;
		}
	}

	/**
	 * コンストラクタ
	 */
	public FacilityCategoryDomain() {
		setEntity(new FacilityCategoryEntity()); // 初期エンティティ
	}

	/**
	 * 施設分類と会員勤務先業務分類が同じかどうか
	 * @param category 分類種別
	 * @param categoryDivision 分類区分
	 * @param parentCode 親分類コード
	 * @param categoryCode 分類コード
	 * @return 同じであるならばtrueそうでないならばfalseを返す
	 */
	public boolean isSameFacilityCategory(int category, int categoryDivision, String parentCode, String categoryCode) {
		// 分類種別が同じかどうか
		if (this.getEntity().getCategory() != category) {
			return false;
		}
		// 分類区分が同じかどうか
		if (this.getEntity().getCategoryDivision() != categoryDivision) {
			return false;
		}
		// 親分類コードが同じかどうか。nullableなプロパティなのでnull同士でもtrueになるようにした
		if (!Objects.equals(this.getEntity().getParentCode(), parentCode)) {
			return false;
		}
		// 分類コードが同じかどうか。nullableなプロパティなのでnull同士でもtrueになるようにした
		if (!Objects.equals(this.getEntity().getCategoryCode(), categoryCode)) {
			return false;
		}
		// すべて一致してるならば、同じ
		return true;
	}
}
