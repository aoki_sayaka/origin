package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.QualificationOtAppHistoryEntity;

/**
 * 認定作業療法士申請履歴ドメイン
 */
@Component
@Scope("prototype")
public class QualificationOtAppHistoryDomain extends BaseDomain<QualificationOtAppHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public QualificationOtAppHistoryDomain() {
		setEntity(new QualificationOtAppHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
