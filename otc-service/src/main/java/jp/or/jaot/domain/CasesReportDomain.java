package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.CasesReportEntity;

/**
 * 事例報告ドメイン
 */
@Component
@Scope("prototype")
public class CasesReportDomain extends BaseDomain<CasesReportEntity> {

	/**
	 * コンストラクタ
	 */
	public CasesReportDomain() {
		setEntity(new CasesReportEntity()); // 初期エンティティ
	}

	// TODO
}
