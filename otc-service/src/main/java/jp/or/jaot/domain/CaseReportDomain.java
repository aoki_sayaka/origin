package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.CaseReportEntity;

/**
 * 事例報告ドメイン
 */
@Component
@Scope("prototype")
public class CaseReportDomain extends BaseDomain<CaseReportEntity> {

	/**
	 * コンストラクタ
	 */
	public CaseReportDomain() {
		setEntity(new CaseReportEntity()); // 初期エンティティ
	}

	// TODO
}
