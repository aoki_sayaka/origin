package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingOrganizerEntity;

/**
 * 研修会主催者ドメイン
 */
@Component
@Scope("prototype")
public class TrainingOrganizerDomain extends BaseDomain<TrainingOrganizerEntity> {

	/**
	 * コンストラクタ
	 */
	public TrainingOrganizerDomain() {
		setEntity(new TrainingOrganizerEntity()); // 初期エンティティ
	}

	// TODO
}
