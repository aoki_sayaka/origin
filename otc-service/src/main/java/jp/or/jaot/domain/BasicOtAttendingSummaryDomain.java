package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;

/**
 * 基礎研修履歴ドメイン
 */
@Component
@Scope("prototype")
public class BasicOtAttendingSummaryDomain extends BaseDomain<BasicOtAttendingSummaryEntity> {

	/**
	 * コンストラクタ
	 */
	public BasicOtAttendingSummaryDomain() {
		setEntity(new BasicOtAttendingSummaryEntity()); // 初期エンティティ
	}

	// TODO
}
