package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.FacilityMemberDao;
import jp.or.jaot.domain.FacilityMemberDomain;
import jp.or.jaot.model.dto.search.FacilityMemberSearchDto;
import jp.or.jaot.model.entity.FacilityMemberEntity;

/**
 * 施設養成校勤務者リポジトリ
 */
@Repository
public class FacilityMemberRepository extends BaseRepository<FacilityMemberDomain, FacilityMemberEntity> {

	/** 施設養成校勤務者DAO */
	private final FacilityMemberDao facilityMemberDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param facilityMemberDao DAO
	 */
	@Autowired
	public FacilityMemberRepository(FacilityMemberDao facilityMemberDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, facilityMemberDao);
		this.facilityMemberDao = facilityMemberDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public FacilityMemberEntity getSearchResult(Long id) {
		FacilityMemberEntity entity = null;
		if ((id == null) || ((entity = facilityMemberDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する施設養成校勤務者が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(施設養成校ID)
	 * @param facilityId 施設養成校ID
	 * @return エンティティリスト
	 */
	public List<FacilityMemberEntity> getSearchResultByFacilityId(Long facilityId) {

		// 検索条件生成
		FacilityMemberSearchDto searchDto = new FacilityMemberSearchDto();
		searchDto.setFacilityIds(new Long[] { facilityId }); // 施設養成校ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(施設養成校ID)
	 * @param facilityIds 施設養成校ID配列
	 * @return エンティティリスト
	 */
	public List<FacilityMemberEntity> getSearchResultByFacilityIds(Long[] facilityIds) {

		// 検索条件生成
		FacilityMemberSearchDto searchDto = new FacilityMemberSearchDto();
		searchDto.setFacilityIds(facilityIds); // 施設養成校ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<FacilityMemberEntity> getSearchResultByCondition(FacilityMemberSearchDto searchDto) {

		// 検索
		List<FacilityMemberEntity> entities = facilityMemberDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<FacilityMemberEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("施設養成校勤務者検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
