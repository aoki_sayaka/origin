package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.AuthorPresenterActualFieldDao;
import jp.or.jaot.domain.AuthorPresenterActualFieldDomain;
import jp.or.jaot.model.dto.search.AuthorPresenterActualFieldSearchDto;
import jp.or.jaot.model.entity.AuthorPresenterActualFieldEntity;

/**
 * 活動実績の領域(発表・著作等)リポジトリ
 */
@Repository
public class AuthorPresenterActualFieldRepository
		extends BaseRepository<AuthorPresenterActualFieldDomain, AuthorPresenterActualFieldEntity> {

	/** 活動実績の領域(発表・著作等)DAO */
	private final AuthorPresenterActualFieldDao authorPresenterActualFieldDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param authorPresenterActualFieldDao DAO
	 */
	@Autowired
	public AuthorPresenterActualFieldRepository(AuthorPresenterActualFieldDao authorPresenterActualFieldDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, authorPresenterActualFieldDao);
		this.authorPresenterActualFieldDao = authorPresenterActualFieldDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public AuthorPresenterActualFieldEntity getSearchResult(Long id) {
		AuthorPresenterActualFieldEntity entity = null;
		if ((id == null) || ((entity = authorPresenterActualFieldDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する活動実績の領域(発表・著作等)が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(活動実績ID)
	 * @param resultId 活動実績ID
	 * @return エンティティリスト
	 */
	public List<AuthorPresenterActualFieldEntity> getSearchResultByResultId(Long resultId) {

		// 検索条件生成
		AuthorPresenterActualFieldSearchDto searchDto = new AuthorPresenterActualFieldSearchDto();
		searchDto.setResultIds(new Long[] { resultId }); // 活動実績ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(活動実績ID)
	 * @param resultIds 活動実績ID配列
	 * @return エンティティリスト
	 */
	public List<AuthorPresenterActualFieldEntity> getSearchResultByResultIds(Long[] resultIds) {

		// 検索条件生成
		AuthorPresenterActualFieldSearchDto searchDto = new AuthorPresenterActualFieldSearchDto();
		searchDto.setResultIds(resultIds); // 活動実績ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<AuthorPresenterActualFieldEntity> getSearchResultByCondition(
			AuthorPresenterActualFieldSearchDto searchDto) {

		// 検索
		List<AuthorPresenterActualFieldEntity> entities = authorPresenterActualFieldDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<AuthorPresenterActualFieldEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("活動実績の領域(発表・著作等)検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
