package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberRoleEntity;

/**
 * 会員ロールドメイン
 */
@Component
@Scope("prototype")
public class MemberRoleDomain extends BaseDomain<MemberRoleEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberRoleDomain() {
		setEntity(new MemberRoleEntity()); // 初期エンティティ
	}

	// TODO
}
