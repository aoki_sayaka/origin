package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.UserRevokedAuthorityEntity;

/**
 * 事務局権限ドメイン
 */
@Component
@Scope("prototype")
public class UserRevokedAuthorityDomain extends BaseDomain<UserRevokedAuthorityEntity> {

	/**
	 * コンストラクタ
	 */
	public UserRevokedAuthorityDomain() {
		setEntity(new UserRevokedAuthorityEntity()); // 初期エンティティ
	}

	// TODO
}
