package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.HandbookMigrationApplicationDao;
import jp.or.jaot.domain.HandbookMigrationApplicationDomain;
import jp.or.jaot.model.dto.search.HandbookMigrationApplicationSearchDto;
import jp.or.jaot.model.entity.HandbookMigrationApplicationEntity;

/**
 * 手帳移行申請リポジトリ
 */
@Repository
public class HandbookMigrationApplicationRepository
		extends BaseRepository<HandbookMigrationApplicationDomain, HandbookMigrationApplicationEntity> {

	/** 手帳移行申請DAO */
	private final HandbookMigrationApplicationDao handbookMigrationApplicationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param handbookMigrationApplicationDao DAO
	 */
	//TODO ERROR_PLACEが未定義
	@Autowired
	public HandbookMigrationApplicationRepository(HandbookMigrationApplicationDao handbookMigrationApplicationDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, handbookMigrationApplicationDao);
		this.handbookMigrationApplicationDao = handbookMigrationApplicationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public HandbookMigrationApplicationEntity getSearchResult(Long id) {
		HandbookMigrationApplicationEntity entity = null;
		if ((id == null) || ((entity = handbookMigrationApplicationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する手帳移行申請が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public HandbookMigrationApplicationEntity getSearchResultByMemberNo(Integer memberNo) {
		HandbookMigrationApplicationEntity entity = null;
		if ((memberNo == null) || ((entity = handbookMigrationApplicationDao.findByMemberNo(memberNo)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("memberNo=%s", memberNo);
			String message = String.format("検索条件に指定された会員番号に該当する手帳移行申請が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<HandbookMigrationApplicationEntity> getSearchResultByCondition(
			HandbookMigrationApplicationSearchDto searchDto) {

		// 検索
		List<HandbookMigrationApplicationEntity> entities = handbookMigrationApplicationDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<HandbookMigrationApplicationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("手帳移行申請検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
