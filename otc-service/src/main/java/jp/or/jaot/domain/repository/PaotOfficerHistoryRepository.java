package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.PaotOfficerHistoryDao;
import jp.or.jaot.domain.PaotOfficerHistoryDomain;
import jp.or.jaot.model.dto.search.PaotOfficerHistorySearchDto;
import jp.or.jaot.model.entity.PaotOfficerHistoryEntity;

/**
 * 県士会役員履歴リポジトリ
 */
@Repository
public class PaotOfficerHistoryRepository extends BaseRepository<PaotOfficerHistoryDomain, PaotOfficerHistoryEntity> {

	/** 県士会役員履歴DAO */
	private final PaotOfficerHistoryDao paotOfficerHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param paotOfficerHistoryDao DAO
	 */
	@Autowired
	public PaotOfficerHistoryRepository(PaotOfficerHistoryDao paotOfficerHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, paotOfficerHistoryDao);
		this.paotOfficerHistoryDao = paotOfficerHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public PaotOfficerHistoryEntity getSearchResult(Long id) {
		PaotOfficerHistoryEntity entity = null;
		if ((id == null) || ((entity = paotOfficerHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する県士会役員履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<PaotOfficerHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		PaotOfficerHistorySearchDto searchDto = new PaotOfficerHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<PaotOfficerHistoryEntity> getSearchResultByCondition(PaotOfficerHistorySearchDto searchDto) {

		// 検索
		List<PaotOfficerHistoryEntity> entities = paotOfficerHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<PaotOfficerHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("県士会役員履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
