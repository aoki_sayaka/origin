package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderQualifyHistoryEntity;

/**
 * 臨床実習指導者認定履歴ドメイン
 */
@Component
@Scope("prototype")
public class ClinicalTrainingLeaderQualifyHistoryDomain extends BaseDomain<ClinicalTrainingLeaderQualifyHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public ClinicalTrainingLeaderQualifyHistoryDomain() {
		setEntity(new ClinicalTrainingLeaderQualifyHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
