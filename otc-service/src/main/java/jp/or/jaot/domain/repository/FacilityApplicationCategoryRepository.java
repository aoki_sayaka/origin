package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.FacilityApplicationCategoryDao;
import jp.or.jaot.domain.FacilityApplicationCategoryDomain;
import jp.or.jaot.model.dto.search.FacilityApplicationCategorySearchDto;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;

/**
 * 施設登録申請分類リポジトリ
 */
@Repository
public class FacilityApplicationCategoryRepository
		extends BaseRepository<FacilityApplicationCategoryDomain, FacilityApplicationCategoryEntity> {

	/** 施設登録申請分類DAO */
	private final FacilityApplicationCategoryDao facilityCategoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param facilityCategoryDao DAO
	 */
	@Autowired
	public FacilityApplicationCategoryRepository(FacilityApplicationCategoryDao facilityCategoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, facilityCategoryDao);
		this.facilityCategoryDao = facilityCategoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public FacilityApplicationCategoryEntity getSearchResult(Long id) {
		FacilityApplicationCategoryEntity entity = null;
		if ((id == null) || ((entity = facilityCategoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する施設登録申請分類が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(施設登録申請ID)
	 * @param applicantId 施設登録申請ID
	 * @return エンティティリスト
	 */
	public List<FacilityApplicationCategoryEntity> getSearchResultByApplicantId(Long applicantId) {

		// 検索条件生成
		FacilityApplicationCategorySearchDto searchDto = new FacilityApplicationCategorySearchDto();
		searchDto.setApplicantIds(new Long[] { applicantId }); // 施設登録申請ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(施設登録申請ID)
	 * @param applicantIds 施設登録申請ID配列
	 * @return エンティティリスト
	 */
	public List<FacilityApplicationCategoryEntity> getSearchResultByApplicantIds(Long[] applicantIds) {

		// 検索条件生成
		FacilityApplicationCategorySearchDto searchDto = new FacilityApplicationCategorySearchDto();
		searchDto.setApplicantIds(applicantIds); // 施設登録申請ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<FacilityApplicationCategoryEntity> getSearchResultByCondition(
			FacilityApplicationCategorySearchDto searchDto) {

		// 検索
		List<FacilityApplicationCategoryEntity> entities = facilityCategoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<FacilityApplicationCategoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("施設登録申請分類検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
