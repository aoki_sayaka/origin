package jp.or.jaot.domain.mail;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseMailDomain;
import jp.or.jaot.core.exception.system.MailSystemException;
import jp.or.jaot.core.model.MailContentDto;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberPasswordHistoryEntity;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;
import jp.or.jaot.utils.VelocityUtility;

/**
 * 会員メールドメイン
 */
@Component
@Scope("prototype")
public class MemberMailDomain extends BaseMailDomain {

	/** メール送信インターフェースのURL */
	@Value("${pass.common.mail}")
	private String sendCommonMailUrl;

	/**
	 * パスワード再発行メール送信
	 * @param memberEntity 会員エンティティ
	 * @param memberPasswordHistoryEntity 会員パスワード履歴エンティティ
	 */
	public void sendPasswordReissueMail(MemberEntity memberEntity,
			MemberPasswordHistoryEntity memberPasswordHistoryEntity) {

		// メール情報DTO生成
		MailContentDto mailDto = new MailContentDto();
		mailDto.setToAddressList(Arrays.asList(memberEntity.getEmailAddress())); // 送信先
		mailDto.setSubject("日本作業療法士協会パスワード再発行URLの通知"); // 件名

		// 本文設定
		String lastNameMessage = memberEntity.getLastName() + "様";
		String urlMessage = sendCommonMailUrl + "/#/portal_password_resetting/?temp_password="
				+ memberPasswordHistoryEntity.getTempPassword();
		Map<String, Object> variableMap = new HashMap<String, Object>() {
			{
				put("lastName", lastNameMessage); // 姓
				put("url", urlMessage); // URL
			}
		};
		mailDto.setBody(VelocityUtility.getInstance().createPostText("MemberPasswordMail.vm", variableMap));

		// メール送信
		try {
			sendMail(mailDto, null);
		} catch (Exception e) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.MAIL_SEND;
			String message = String.format("メール送信に失敗しました");
			String detail = "";
			throw new MailSystemException(errorCause, detail, message, e);
		}
	}

	/**
	 * パスワード再設定メール送信
	 * @param memberEntity 会員エンティティ
	 */
	public void sendPasswordResettingMail(MemberEntity memberEntity) {

		// メール情報DTO生成
		MailContentDto mailDto = new MailContentDto();
		mailDto.setToAddressList(Arrays.asList(memberEntity.getEmailAddress())); // 送信先
		mailDto.setSubject("日本作業療法士協会パスワード再設定の通知"); // 件名

		// 本文設定
		Map<String, Object> variableMap = new HashMap<String, Object>() {
			{
				put("lastName", memberEntity.getLastName()); // 姓
			}
		};
		mailDto.setBody(VelocityUtility.getInstance().createPostText("MemberPasswordResettingMail.vm", variableMap));

		// メール送信
		try {
			sendMail(mailDto, null);
		} catch (Exception e) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.MAIL_SEND;
			String message = String.format("メール送信に失敗しました");
			String detail = "";
			throw new MailSystemException(errorCause, detail, message, e);
		}
	}

	/**
	 * メールアドレス変更メール送信
	 * @param memberEntity 会員エンティティ
	 * @param memberEmailHistoryEntity 会員メールアドレス履歴エンティティ
	 */
	public void sendEmailConfirmMail(MemberEntity memberEntity,
										MemberEmailHistoryEntity memberEmailHistoryEntity) {

		// メール情報DTO生成
		MailContentDto mailDto = new MailContentDto();
		mailDto.setToAddressList(Arrays.asList(memberEmailHistoryEntity.getEmailAddress())); // 送信先
		mailDto.setSubject("日本作業療法士協会  事務局です。"); // 件名

		// 本文設定
		String fullNameMessage = memberEntity.getFullName();
		String urlMessage = sendCommonMailUrl + "/#/portal_confirm_email/"
                + memberEmailHistoryEntity.getTempKey();
		Map<String, Object> variableMap = new HashMap<String, Object>() {
			{
				put("fullName", fullNameMessage); // 氏名
				put("url", urlMessage); // URL
			}
		};
		mailDto.setBody(VelocityUtility.getInstance().createPostText("MemberEmailUpdate.vm", variableMap));

		// メール送信
		try {
			sendMail(mailDto, null);
		} catch (Exception e) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.MAIL_SEND;
			String message = String.format("メール送信に失敗しました");
			String detail = "";
			throw new MailSystemException(errorCause, detail, message, e);
		}
	}
}
