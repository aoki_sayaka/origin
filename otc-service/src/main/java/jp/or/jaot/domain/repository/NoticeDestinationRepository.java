package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.NoticeDestinationDao;
import jp.or.jaot.domain.NoticeDestinationDomain;
import jp.or.jaot.model.dto.search.NoticeDestinationSearchDto;
import jp.or.jaot.model.entity.NoticeDestinationEntity;

/**
 * お知らせ(宛先)リポジトリ
 */
@Repository
public class NoticeDestinationRepository extends BaseRepository<NoticeDestinationDomain, NoticeDestinationEntity> {

	/** お知らせ(宛先)DAO */
	private final NoticeDestinationDao noticeDestinationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param noticeDestinationDao DAO
	 */
	@Autowired
	public NoticeDestinationRepository(NoticeDestinationDao noticeDestinationDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, noticeDestinationDao);
		this.noticeDestinationDao = noticeDestinationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public NoticeDestinationEntity getSearchResult(Long id) {
		NoticeDestinationEntity entity = null;
		if ((id == null) || ((entity = noticeDestinationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当するお知らせ(宛先)が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(お知らせID)
	 * @param noticeIds お知らせID
	 * @return エンティティリスト
	 */
	public List<NoticeDestinationEntity> getSearchResultByResultId(Long noticeId) {

		// 検索条件生成
		NoticeDestinationSearchDto searchDto = new NoticeDestinationSearchDto();
		searchDto.setNoticeIds(new Long[] { noticeId }); // お知らせID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(お知らせID)
	 * @param noticeIds お知らせID配列
	 * @return エンティティリスト
	 */
	public List<NoticeDestinationEntity> getSearchResultByResultIds(Long[] noticeIds) {

		// 検索条件生成
		NoticeDestinationSearchDto searchDto = new NoticeDestinationSearchDto();
		searchDto.setNoticeIds(noticeIds); // お知らせID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<NoticeDestinationEntity> getSearchResultByCondition(
			NoticeDestinationSearchDto searchDto) {

		// 検索
		List<NoticeDestinationEntity> entities = noticeDestinationDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<NoticeDestinationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("お知らせ(宛先)検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
