package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;

/**
 * 基礎研修修了認定更新履歴ドメイン
 */
@Component
@Scope("prototype")
public class BasicOtQualifyHistoryDomain extends BaseDomain<BasicOtQualifyHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public BasicOtQualifyHistoryDomain() {
		setEntity(new BasicOtQualifyHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
