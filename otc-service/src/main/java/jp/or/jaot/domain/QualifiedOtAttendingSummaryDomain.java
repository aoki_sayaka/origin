package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;

/**
 * 認定作業療法士研修履歴ドメイン
 */
@Component
@Scope("prototype")
public class QualifiedOtAttendingSummaryDomain extends BaseDomain<QualifiedOtAttendingSummaryEntity> {

	/**
	 * コンストラクタ
	 */
	public QualifiedOtAttendingSummaryDomain() {
		setEntity(new QualifiedOtAttendingSummaryEntity()); // 初期エンティティ
	}

	// TODO
}
