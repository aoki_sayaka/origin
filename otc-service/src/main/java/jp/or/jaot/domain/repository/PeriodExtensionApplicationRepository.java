package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.PeriodExtensionApplicationDao;
import jp.or.jaot.domain.PeriodExtensionApplicationDomain;
import jp.or.jaot.model.dto.search.PeriodExtensionApplicationSearchDto;
import jp.or.jaot.model.entity.PeriodExtensionApplicationEntity;

/**
 *期間延長申請リポジトリ
 */
@Repository
public class PeriodExtensionApplicationRepository
		extends BaseRepository<PeriodExtensionApplicationDomain, PeriodExtensionApplicationEntity> {

	/** 期間延長申請DAO */
	private final PeriodExtensionApplicationDao periodExtensionApplicationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param periodExtensionApplicationDao DAO
	 */
	@Autowired
	public PeriodExtensionApplicationRepository(PeriodExtensionApplicationDao periodExtensionApplicationDao) {
		super(ERROR_PLACE.LOGIC_COMMON, periodExtensionApplicationDao);
		this.periodExtensionApplicationDao = periodExtensionApplicationDao;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<PeriodExtensionApplicationEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		PeriodExtensionApplicationSearchDto searchDto = new PeriodExtensionApplicationSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号
		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<PeriodExtensionApplicationEntity> getSearchResultByCondition(
			PeriodExtensionApplicationSearchDto searchDto) {

		// 検索
		List<PeriodExtensionApplicationEntity> entities = periodExtensionApplicationDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<PeriodExtensionApplicationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("期間延長申請検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
