package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;

/**
 * 会員自治体参画情報ドメイン
 */
@Component
@Scope("prototype")
public class MemberLocalActivityDomain extends BaseDomain<MemberLocalActivityEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberLocalActivityDomain() {
		setEntity(new MemberLocalActivityEntity()); // 初期エンティティ
	}

	// TODO
}
