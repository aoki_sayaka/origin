package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.PaotMembershipFeeDeliveryEntity;

/**
 * 県士会会費納入ドメイン
 */
@Component
@Scope("prototype")
public class PaotMembershipFeeDeliveryDomain extends BaseDomain<PaotMembershipFeeDeliveryEntity> {

	/**
	 * コンストラクタ
	 */
	public PaotMembershipFeeDeliveryDomain() {
		setEntity(new PaotMembershipFeeDeliveryEntity()); // 初期エンティティ
	}

	// TODO
}
