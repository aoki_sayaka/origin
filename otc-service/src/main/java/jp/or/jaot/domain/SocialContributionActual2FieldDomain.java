package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.SocialContributionActual2FieldEntity;

/**
 * 活動実績の領域2(後輩育成・社会貢献)ドメイン
 */
@Component
@Scope("prototype")
public class SocialContributionActual2FieldDomain extends BaseDomain<SocialContributionActual2FieldEntity> {

	/**
	 * コンストラクタ
	 */
	public SocialContributionActual2FieldDomain() {
		setEntity(new SocialContributionActual2FieldEntity()); // 初期エンティティ
	}

	// TODO
}
