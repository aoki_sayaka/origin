package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.AttendingHistoryDao;
import jp.or.jaot.domain.AttendingHistoryDomain;
import jp.or.jaot.model.dto.search.AttendingHistorySearchDto;
import jp.or.jaot.model.entity.AttendingHistoryEntity;

/**
 * 受講履歴リポジトリ
 */
@Repository
public class AttendingHistoryRepository extends BaseRepository<AttendingHistoryDomain, AttendingHistoryEntity> {

	/** 受講履歴DAO */
	private final AttendingHistoryDao attendingHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param attendingHistoryDao DAO
	 */
	@Autowired
	public AttendingHistoryRepository(AttendingHistoryDao attendingHistoryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, attendingHistoryDao);
		this.attendingHistoryDao = attendingHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public AttendingHistoryEntity getSearchResult(Long id) {
		AttendingHistoryEntity entity = null;
		if ((id == null) || ((entity = attendingHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<AttendingHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		AttendingHistorySearchDto searchDto = new AttendingHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<AttendingHistoryEntity> getSearchResultByCondition(AttendingHistorySearchDto searchDto) {

		// 検索
		List<AttendingHistoryEntity> entities = attendingHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<AttendingHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("受講履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
