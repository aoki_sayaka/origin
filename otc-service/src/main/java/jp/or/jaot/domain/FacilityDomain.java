package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.FacilityEntity;

/**
 * 施設養成校ドメイン
 */
@Component
@Scope("prototype")
public class FacilityDomain extends BaseDomain<FacilityEntity> {

	/**
	 * コンストラクタ
	 */
	public FacilityDomain() {
		setEntity(new FacilityEntity()); // 初期エンティティ
	}

	// TODO
}
