package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.NoticeDao;
import jp.or.jaot.domain.NoticeDomain;
import jp.or.jaot.model.dto.search.NoticeSearchDto;
import jp.or.jaot.model.entity.NoticeEntity;

/**
 * お知らせリポジトリ
 */
@Repository
public class NoticeRepository extends BaseRepository<NoticeDomain, NoticeEntity> {

	/** お知らせDAO */
	private final NoticeDao noticeDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param noticeDao DAO
	 */
	@Autowired
	public NoticeRepository(NoticeDao noticeDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, noticeDao);
		this.noticeDao = noticeDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public NoticeEntity getSearchResult(Long id) {
		NoticeEntity entity = null;
		if ((id == null) || ((entity = noticeDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当するお知らせが存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<NoticeEntity> getSearchResultByCondition(NoticeSearchDto searchDto) {

		// 検索
		List<NoticeEntity> entities = noticeDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<NoticeEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("お知らせ検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
