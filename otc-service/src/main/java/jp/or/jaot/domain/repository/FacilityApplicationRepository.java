package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.FacilityApplicationDao;
import jp.or.jaot.domain.FacilityApplicationDomain;
import jp.or.jaot.model.dto.search.FacilityApplicationSearchDto;
import jp.or.jaot.model.entity.FacilityApplicationEntity;

/**
 * 施設登録申請リポジトリ
 */
@Repository
public class FacilityApplicationRepository
		extends BaseRepository<FacilityApplicationDomain, FacilityApplicationEntity> {

	/** 施設登録申請DAO */
	private final FacilityApplicationDao facilityApplicationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param facilityApplicationDao DAO
	 */
	@Autowired
	public FacilityApplicationRepository(FacilityApplicationDao facilityApplicationDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, facilityApplicationDao);
		this.facilityApplicationDao = facilityApplicationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public FacilityApplicationEntity getSearchResult(Long id) {
		FacilityApplicationEntity entity = null;
		if ((id == null) || ((entity = facilityApplicationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する施設登録申請が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<FacilityApplicationEntity> getSearchResultByCondition(FacilityApplicationSearchDto searchDto) {

		// 検索
		List<FacilityApplicationEntity> entities = facilityApplicationDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<FacilityApplicationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("施設登録申請検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
