package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MtdlpSummaryEntity;

/**
 * MTDLP履歴ドメイン
 */
@Component
@Scope("prototype")
public class MtdlpSummaryDomain extends BaseDomain<MtdlpSummaryEntity> {

	/**
	 * コンストラクタ
	 */
	public MtdlpSummaryDomain() {
		setEntity(new MtdlpSummaryEntity()); // 初期エンティティ
	}
}
