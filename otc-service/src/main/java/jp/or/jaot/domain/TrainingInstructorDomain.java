package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingInstructorEntity;

/**
 * 研修会講師実績ドメイン
 */
@Component
@Scope("prototype")
public class TrainingInstructorDomain extends BaseDomain<TrainingInstructorEntity> {

	/**
	 * コンストラクタ
	 */
	public TrainingInstructorDomain() {
		setEntity(new TrainingInstructorEntity()); // 初期エンティティ
	}

	// TODO
}
