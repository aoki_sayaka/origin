package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.QualifiedOtTrainingHistoryDao;
import jp.or.jaot.domain.QualifiedOtTrainingHistoryDomain;
import jp.or.jaot.model.dto.search.QualifiedOtTrainingHistorySearchDto;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;

/**
 * 認定作業療法士研修受講履歴リポジトリ
 */
@Repository
public class QualifiedOtTrainingHistoryRepository
		extends BaseRepository<QualifiedOtTrainingHistoryDomain, QualifiedOtTrainingHistoryEntity> {

	/** 認定作業療法士研修受講履歴DAO */
	private final QualifiedOtTrainingHistoryDao qualifiedOtTrainingHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param qualifiedOtTrainingHistoryDao DAO
	 */
	@Autowired
	public QualifiedOtTrainingHistoryRepository(QualifiedOtTrainingHistoryDao qualifiedOtTrainingHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, qualifiedOtTrainingHistoryDao);
		this.qualifiedOtTrainingHistoryDao = qualifiedOtTrainingHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public QualifiedOtTrainingHistoryEntity getSearchResult(Long id) {
		QualifiedOtTrainingHistoryEntity entity = null;
		if ((id == null) || ((entity = qualifiedOtTrainingHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定された会員番号に該当する認定作業療法士研修受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<QualifiedOtTrainingHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		QualifiedOtTrainingHistorySearchDto searchDto = new QualifiedOtTrainingHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<QualifiedOtTrainingHistoryEntity> getSearchResultByCondition(
			QualifiedOtTrainingHistorySearchDto searchDto) {

		// 検索
		List<QualifiedOtTrainingHistoryEntity> entities = qualifiedOtTrainingHistoryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<QualifiedOtTrainingHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("認定作業療法士研修受講履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
