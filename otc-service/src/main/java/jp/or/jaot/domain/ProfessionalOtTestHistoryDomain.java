package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;

/**
 * 専門作業療法士試験結果履歴ドメイン
 */
@Component
@Scope("prototype")
public class ProfessionalOtTestHistoryDomain extends BaseDomain<ProfessionalOtTestHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public ProfessionalOtTestHistoryDomain() {
		setEntity(new ProfessionalOtTestHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
