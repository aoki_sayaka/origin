package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.QualificationOtAppHistoryDao;
import jp.or.jaot.domain.QualificationOtAppHistoryDomain;
import jp.or.jaot.model.dto.search.QualificationOtAppHistorySearchDto;
import jp.or.jaot.model.entity.QualificationOtAppHistoryEntity;

/**
 * 認定作業療法士申請履歴リポジトリ
 */
@Repository
public class QualificationOtAppHistoryRepository extends BaseRepository<QualificationOtAppHistoryDomain, QualificationOtAppHistoryEntity> {

	/** 認定作業療法士申請履歴DAO */
	private final QualificationOtAppHistoryDao qualificationOtAppHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param qualificationOtAppHistoryDao DAO
	 */
	@Autowired
	public QualificationOtAppHistoryRepository(QualificationOtAppHistoryDao qualificationOtAppHistoryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, qualificationOtAppHistoryDao);
		this.qualificationOtAppHistoryDao = qualificationOtAppHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public QualificationOtAppHistoryEntity getSearchResult(Long id) {
		QualificationOtAppHistoryEntity entity = null;
		if ((id == null) || ((entity = qualificationOtAppHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する認定作業療法士申請履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<QualificationOtAppHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		QualificationOtAppHistorySearchDto searchDto = new QualificationOtAppHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<QualificationOtAppHistoryEntity> getSearchResultByCondition(QualificationOtAppHistorySearchDto searchDto) {

		// 検索
		List<QualificationOtAppHistoryEntity> entities = qualificationOtAppHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<QualificationOtAppHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("認定作業療法士申請履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
