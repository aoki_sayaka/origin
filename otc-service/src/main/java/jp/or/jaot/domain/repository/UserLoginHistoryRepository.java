package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.UserLoginHistoryDao;
import jp.or.jaot.domain.UserLoginHistoryDomain;
import jp.or.jaot.model.dto.search.UserLoginHistorySearchDto;
import jp.or.jaot.model.entity.UserLoginHistoryEntity;

/**
 * 事務局サイトログイン履歴リポジトリ
 */
@Repository
public class UserLoginHistoryRepository extends BaseRepository<UserLoginHistoryDomain, UserLoginHistoryEntity> {

	/** 事務局サイトログイン履歴DAO */
	private final UserLoginHistoryDao userLoginHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param userLoginHistoryDao DAO
	 */
	@Autowired
	public UserLoginHistoryRepository(UserLoginHistoryDao userLoginHistoryDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, userLoginHistoryDao);
		this.userLoginHistoryDao = userLoginHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public UserLoginHistoryEntity getSearchResult(Long id) {
		UserLoginHistoryEntity entity = null;
		if ((id == null) || ((entity = userLoginHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する事務局サイトログイン履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<UserLoginHistoryEntity> getSearchResultByCondition(UserLoginHistorySearchDto searchDto) {

		// 検索
		List<UserLoginHistoryEntity> entities = userLoginHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<UserLoginHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("事務局サイトログイン履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

}
