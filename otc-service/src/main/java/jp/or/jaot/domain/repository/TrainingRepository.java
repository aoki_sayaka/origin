package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingDao;
import jp.or.jaot.domain.TrainingDomain;
import jp.or.jaot.model.dto.search.TrainingSearchDto;
import jp.or.jaot.model.entity.TrainingEntity;

/**
 * 研修会リポジトリ
 */
@Repository
public class TrainingRepository extends BaseRepository<TrainingDomain, TrainingEntity> {

	/** 研修会DAO */
	private final TrainingDao trainingDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingDao DAO
	 */
	@Autowired
	public TrainingRepository(TrainingDao trainingDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, trainingDao);
		this.trainingDao = trainingDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingEntity getSearchResult(Long id) {
		TrainingEntity entity = null;
		if ((id == null) || ((entity = trainingDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修会が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(キー)
	 * @param ids ID配列
	 * @return エンティティリスト
	 * @throws SearchBusinessException 検索業務例外
	 */
	public List<TrainingEntity> getSearchResult(Long[] ids) {
		List<TrainingEntity> entities = trainingDao.find(ids);
		if (ids.length != entities.stream().filter(E -> E != null).count()) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("ids=%s", StringUtils.join(ids, ","));
			String message = String.format("検索条件に指定されたID配列に該当する研修会が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entities;
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNos 研修会番号配列
	 * @return エンティティリスト
	 */
	public List<TrainingEntity> getSearchResultByTrainingNos(String[] trainingNos) {

		// 検索条件生成
		TrainingSearchDto searchDto = new TrainingSearchDto();
		searchDto.setTrainingNos(trainingNos); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingEntity> getSearchResultByCondition(TrainingSearchDto searchDto) {

		// 検索
		List<TrainingEntity> entities = trainingDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修会検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
