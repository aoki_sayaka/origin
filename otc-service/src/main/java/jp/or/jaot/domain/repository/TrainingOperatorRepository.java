package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingOperatorDao;
import jp.or.jaot.domain.TrainingOperatorDomain;
import jp.or.jaot.model.dto.search.TrainingOperatorSearchDto;
import jp.or.jaot.model.entity.TrainingOperatorEntity;

/**
 * 研修会運営担当者リポジトリ
 */
@Repository
public class TrainingOperatorRepository extends BaseRepository<TrainingOperatorDomain, TrainingOperatorEntity> {

	/** 研修会運営担当者DAO */
	private final TrainingOperatorDao trainingOperatorDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingOperatorDao DAO
	 */
	@Autowired
	public TrainingOperatorRepository(TrainingOperatorDao trainingOperatorDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, trainingOperatorDao);
		this.trainingOperatorDao = trainingOperatorDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingOperatorEntity getSearchResult(Long id) {
		TrainingOperatorEntity entity = null;
		if ((id == null) || ((entity = trainingOperatorDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修会運営担当者が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNo 研修会番号
	 * @return エンティティリスト
	 */
	public List<TrainingOperatorEntity> getSearchResultByTrainingNo(String trainingNo) {

		// 検索条件生成
		TrainingOperatorSearchDto searchDto = new TrainingOperatorSearchDto();
		searchDto.setTrainingNos(new String[] { trainingNo }); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNos 研修会番号配列
	 * @return エンティティリスト
	 */
	public List<TrainingOperatorEntity> getSearchResultByTrainingNos(String[] trainingNos) {

		// 検索条件生成
		TrainingOperatorSearchDto searchDto = new TrainingOperatorSearchDto();
		searchDto.setTrainingNos(trainingNos); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingOperatorEntity> getSearchResultByCondition(TrainingOperatorSearchDto searchDto) {

		// 検索
		List<TrainingOperatorEntity> entities = trainingOperatorDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingOperatorEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修会運営担当者検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	/**
	 * 状態設定
	 * @param id ID
	 * @param updateUser 更新者
	 * @param enabled 状態(true=使用可能, false=使用不可)
	 */
	public void setEnable(Long id, String updateUser, boolean enabled) {
		trainingOperatorDao.setEnableEntity(id, updateUser, enabled);
	}

	// TODO
}
