package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.OfficerHistoryEntity;

/**
 * 役員履歴ドメイン
 */
@Component
@Scope("prototype")
public class OfficerHistoryDomain extends BaseDomain<OfficerHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public OfficerHistoryDomain() {
		setEntity(new OfficerHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
