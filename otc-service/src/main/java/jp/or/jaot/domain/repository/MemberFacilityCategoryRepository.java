package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberFacilityCategoryDao;
import jp.or.jaot.domain.MemberFacilityCategoryDomain;
import jp.or.jaot.model.dto.search.MemberFacilityCategorySearchDto;
import jp.or.jaot.model.entity.MemberFacilityCategoryEntity;

/**
 * 会員勤務先業務分類リポジトリ
 */
@Repository
public class MemberFacilityCategoryRepository
		extends BaseRepository<MemberFacilityCategoryDomain, MemberFacilityCategoryEntity> {

	/** 会員勤務先業務分類DAO */
	private final MemberFacilityCategoryDao memberFacilityCategoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberFacilityCategoryDao DAO
	 */
	@Autowired
	public MemberFacilityCategoryRepository(MemberFacilityCategoryDao memberFacilityCategoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberFacilityCategoryDao);
		this.memberFacilityCategoryDao = memberFacilityCategoryDao;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MemberFacilityCategoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MemberFacilityCategorySearchDto searchDto = new MemberFacilityCategorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberFacilityCategoryEntity> getSearchResultByCondition(MemberFacilityCategorySearchDto searchDto) {

		// 検索
		List<MemberFacilityCategoryEntity> entities = memberFacilityCategoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberFacilityCategoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員勤務先業務分類検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

}
