package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MembershipFeeDeliveryDao;
import jp.or.jaot.domain.MembershipFeeDeliveryDomain;
import jp.or.jaot.model.dto.search.MembershipFeeDeliverySearchDto;
import jp.or.jaot.model.entity.MembershipFeeDeliveryEntity;

/**
 * 会費納入リポジトリ
 */
@Repository
public class MembershipFeeDeliveryRepository
		extends BaseRepository<MembershipFeeDeliveryDomain, MembershipFeeDeliveryEntity> {

	/** 会費納入DAO */
	private final MembershipFeeDeliveryDao membershipFeeDeliveryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param membershipFeeDeliveryDao DAO
	 */
	@Autowired
	public MembershipFeeDeliveryRepository(MembershipFeeDeliveryDao membershipFeeDeliveryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, membershipFeeDeliveryDao);
		this.membershipFeeDeliveryDao = membershipFeeDeliveryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MembershipFeeDeliveryEntity getSearchResult(Long id) {
		MembershipFeeDeliveryEntity entity = null;
		if ((id == null) || ((entity = membershipFeeDeliveryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会費納入が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MembershipFeeDeliveryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MembershipFeeDeliverySearchDto searchDto = new MembershipFeeDeliverySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(会員番号、年度)
	 * @param memberNo 会員番号
	 * @param fiscalYear 年度
	 * @return エンティティリスト
	 */
	public List<MembershipFeeDeliveryEntity> getSearchResultByFiscalYear(Integer memberNo, Integer fiscalYear) {

		// 検索条件生成
		MembershipFeeDeliverySearchDto searchDto = new MembershipFeeDeliverySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号
		searchDto.setFiscalYear(fiscalYear);//請求年度

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MembershipFeeDeliveryEntity> getSearchResultByCondition(MembershipFeeDeliverySearchDto searchDto) {

		// 検索
		List<MembershipFeeDeliveryEntity> entities = membershipFeeDeliveryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MembershipFeeDeliveryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会費納入検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
