package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.RegionRelatedLargeClassificationDao;
import jp.or.jaot.domain.RegionRelatedLargeClassificationDomain;
import jp.or.jaot.model.dto.search.RegionRelatedLargeClassificationSearchDto;
import jp.or.jaot.model.entity.RegionRelatedLargeClassificationEntity;

/**
 * 領域関連大分類リポジトリ
 */
@Repository
public class RegionRelatedLargeClassificationRepository
		extends BaseRepository<RegionRelatedLargeClassificationDomain, RegionRelatedLargeClassificationEntity> {

	/** 領域関連大分類DAO */
	private final RegionRelatedLargeClassificationDao regionRelatedLargeClassificationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param regionRelatedLargeClassificationDao DAO
	 */
	@Autowired
	public RegionRelatedLargeClassificationRepository(
			RegionRelatedLargeClassificationDao regionRelatedLargeClassificationDao) {
		super(ERROR_PLACE.LOGIC_COMMON, regionRelatedLargeClassificationDao);
		this.regionRelatedLargeClassificationDao = regionRelatedLargeClassificationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public RegionRelatedLargeClassificationEntity getSearchResult(Long id) {
		RegionRelatedLargeClassificationEntity entity = null;
		if ((id == null) || ((entity = regionRelatedLargeClassificationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する領域関連大分類が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<RegionRelatedLargeClassificationEntity> getSearchResultByCondition(
			RegionRelatedLargeClassificationSearchDto searchDto) {

		// 検索
		List<RegionRelatedLargeClassificationEntity> entities = regionRelatedLargeClassificationDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<RegionRelatedLargeClassificationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("領域関連大分類検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
