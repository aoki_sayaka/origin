package jp.or.jaot.domain;

import java.time.LocalDate;
import java.util.Date;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.domain.MemberFacilityCategoryDomain.MEMBER_FACILITY_CATEGORY_PRIMARY_DIVISION;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.DskReceiptEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberFacilityCategoryEntity;
import jp.or.jaot.model.entity.MembershipFeeDeliveryEntity;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;

/**
 * 会員ドメイン
 */
@Component
@Scope("prototype")
public class MemberDomain extends BaseDomain<MemberEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberDomain() {
		setEntity(new MemberEntity()); // 初期エンティティ
	}

	/**
	 * 勤務施設選択で、申請を選択しているか
	 * @return
	 */
	public boolean isWorkingFacilityApplicationSelected() {
		// 勤務施設コードが空の場合
		return this.getEntity().getWorkingFacilityCd() == null;
	}

	/**
	 * 会員勤務先業務分類から対応する業務領域コードを返す
	 * @param memberFacilityCategoryEntity 会員勤務先業務分類
	 * @return 主業務領域コードもしくは従業務領域コードを返す
	 */
	public int findFacilityDomainCdByMemberFacilityCategory(MemberFacilityCategoryEntity memberFacilityCategoryEntity) {
		if (memberFacilityCategoryEntity.getPrimaryDivision() == MEMBER_FACILITY_CATEGORY_PRIMARY_DIVISION.MAIN
				.getValue()) {
			return Integer.parseInt(this.getEntity().getFacilityDomainCd());
		}
		return Integer.parseInt(this.getEntity().getFacilitySubDomainCd());

	}

	/**
	 * 基礎研修の資格が期間内(true)/期間外または未取得(false)を返す
	 * @param summary 基礎研修エンティティ
	 * @return boolean
	 */
	public boolean isQualifiedBasicTraining(BasicOtAttendingSummaryEntity summary) {
		//資格満了日と現在の日付を取得
		Date date = summary.getQualificationPeriodDate();
		//当日日付＜＝資格満了日
		if (date != null) {
			LocalDate now = LocalDate.now();
			LocalDate periodDate = new java.sql.Date(date.getTime()).toLocalDate();
			if (now.isBefore(periodDate) || now.isEqual(periodDate)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 専門OTの資格が期間内(true)/期間外または未取得(false)を返す
	 * @param summary 専門OTエンティティ
	 * @return boolean
	 */
	public boolean isQualifiedProfessionalOt(ProfessionalOtAttendingSummaryEntity summary) {
		//資格満了日と現在の日付を取得
		Date date = summary.getQualificationPeriodDate();
		//認定番号があるかつ、当日日付＜＝資格満了日
		if (date != null) {
			LocalDate now = LocalDate.now();
			LocalDate periodDate = new java.sql.Date(date.getTime()).toLocalDate();
			if (summary.getQualificationNumber() != null
					&& (now.isBefore(periodDate) || now.isEqual(periodDate))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 認定OTの資格が期間内(true)/期間外または未取得(false)を返す
	 * @param summary 認定OTエンティティ
	 * @return boolean
	 */
	public boolean isQualifiedOt(QualifiedOtAttendingSummaryEntity summary) {
		//資格満了日と現在の日付を取得
		Date date = summary.getQualificationPeriodDate();
		//認定番号があるかつ、当日日付＜＝資格満了日
		if (date != null) {
			LocalDate now = LocalDate.now();
			LocalDate periodDate = new java.sql.Date(date.getTime()).toLocalDate();
			if (summary.getQualificationNumber() != null
					&& (now.isBefore(periodDate) || now.isEqual(periodDate))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 速報値確報値あり(true)／なし(false)を返す（速報取消がある場合は、「なし(false)」を返す）
	 * 年度と会員番号で検索し、IDが大きい(新しい)データが速報、速報取消かで判定
	 * @param memberNo 会員番号
	 * @param fiscalYear 年度 yyyy
	 * @return boolean
	 * @throws Exception
	 */
	public boolean hasQuickAnnouncement(DskReceiptEntity receipt) throws Exception {
		//typeCdが01(速報)または、02(確報)ならtrue
		if ("01".equals(receipt.getTypeCd()) || "02".equals(receipt.getTypeCd())) {
			return true;
		}
		return false;
	}

	/**
	 * 会費納入テーブルに入金あり(true)／なし(false)を返す
	 * 請求が0、かつ入金日がNULL、かつ支払残高が0、または
	 * 入金日がNULLではない、かつ支払残高が0で完納状態と判定
	 * @param memberNo 会員番号
	 * @param fiscalYear 年度
	 * @return boolean
	 */
	public boolean isMembershipFeeComplete(MembershipFeeDeliveryEntity membershipFeeDelivery) {
		//完納状態か確認
		//請求が0、かつ入金日がNULL、かつ支払残高が0
		if (membershipFeeDelivery.getBillingAmount() == 0 && membershipFeeDelivery.getPaymentDate() == null &&
				membershipFeeDelivery.getBillingBalance() == 0) {
			return true;
		}
		//入金日がNULLではない、かつ支払残高が0
		if (membershipFeeDelivery.getPaymentDate() != null && membershipFeeDelivery.getBillingBalance() == 0) {
			return true;
		}
		return false;
	}
}
