package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberWithdrawalHistoryEntity;

/**
 * 退会履歴ドメイン
 */
@Component
@Scope("prototype")
public class MemberWithdrawalHistoryDomain extends BaseDomain<MemberWithdrawalHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberWithdrawalHistoryDomain() {
		setEntity(new MemberWithdrawalHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
