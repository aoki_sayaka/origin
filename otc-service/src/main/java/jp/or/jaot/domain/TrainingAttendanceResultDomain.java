package jp.or.jaot.domain;

import java.util.Arrays;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;

/**
 * 研修会出欠合否ドメイン
 */
@Component
@Scope("prototype")
public class TrainingAttendanceResultDomain extends BaseDomain<TrainingAttendanceResultEntity> {

	/**
	 * 研修会出席状況の列挙体(コード設計:[30260])
	 */
	public enum ATTENDANCE_STATUS_CD {

		/** 欠席 */
		ABSENCE("00"),
		/** 出席 */
		ATTENDANCE("10"),
		/** 免除 */
		EXEMPTION("20");

		/** 値 */
		private final String value;

		/**
		 * コンストラクタ
		 * @param value 値
		 */
		private ATTENDANCE_STATUS_CD(final String value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public String getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static ATTENDANCE_STATUS_CD valueOfText(String value) {
			return Arrays.stream(ATTENDANCE_STATUS_CD.values()).filter(V -> V.getValue().equals(value)).findFirst()
					.orElse(null);
		}
	}

	/**
	 * コンストラクタ
	 */
	public TrainingAttendanceResultDomain() {
		setEntity(new TrainingAttendanceResultEntity()); // 初期エンティティ
	}

	// TODO
}
