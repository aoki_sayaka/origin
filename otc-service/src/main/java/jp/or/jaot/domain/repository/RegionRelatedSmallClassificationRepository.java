package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.RegionRelatedSmallClassificationDao;
import jp.or.jaot.domain.RegionRelatedSmallClassificationDomain;
import jp.or.jaot.model.dto.search.RegionRelatedSmallClassificationSearchDto;
import jp.or.jaot.model.entity.RegionRelatedSmallClassificationEntity;

/**
 * 領域関連小分類リポジトリ
 */
@Repository
public class RegionRelatedSmallClassificationRepository
		extends BaseRepository<RegionRelatedSmallClassificationDomain, RegionRelatedSmallClassificationEntity> {

	/** 領域関連小分類DAO */
	private final RegionRelatedSmallClassificationDao regionRelatedSmallClassificationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param regionRelatedSmallClassificationDao DAO
	 */
	@Autowired
	public RegionRelatedSmallClassificationRepository(
			RegionRelatedSmallClassificationDao regionRelatedSmallClassificationDao) {
		super(ERROR_PLACE.LOGIC_COMMON, regionRelatedSmallClassificationDao);
		this.regionRelatedSmallClassificationDao = regionRelatedSmallClassificationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public RegionRelatedSmallClassificationEntity getSearchResult(Long id) {
		RegionRelatedSmallClassificationEntity entity = null;
		if ((id == null) || ((entity = regionRelatedSmallClassificationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する領域関連小分類が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<RegionRelatedSmallClassificationEntity> getSearchResultByCondition(
			RegionRelatedSmallClassificationSearchDto searchDto) {

		// 検索
		List<RegionRelatedSmallClassificationEntity> entities = regionRelatedSmallClassificationDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<RegionRelatedSmallClassificationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("領域関連小分類検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
