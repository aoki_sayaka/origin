package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ProfessionalOtTrainingHistoryEntity;

/**
 * 専門作業療法士研修受講履歴ドメイン
 */
@Component
@Scope("prototype")
public class ProfessionalOtTrainingHistoryDomain extends BaseDomain<ProfessionalOtTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public ProfessionalOtTrainingHistoryDomain() {
		setEntity(new ProfessionalOtTrainingHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
