package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ProfessionalOtTrainingHistoryDao;
import jp.or.jaot.domain.ProfessionalOtTrainingHistoryDomain;
import jp.or.jaot.model.dto.search.ProfessionalOtTrainingHistorySearchDto;
import jp.or.jaot.model.entity.ProfessionalOtTrainingHistoryEntity;

/**
 * 専門作業療法士研修受講履歴リポジトリ
 */
@Repository
public class ProfessionalOtTrainingHistoryRepository
		extends BaseRepository<ProfessionalOtTrainingHistoryDomain, ProfessionalOtTrainingHistoryEntity> {

	/** 専門作業療法士研修受講履歴DAO */
	private final ProfessionalOtTrainingHistoryDao professionalOtTrainingHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param professionalOtTrainingHistoryDao DAO
	 */
	@Autowired
	public ProfessionalOtTrainingHistoryRepository(ProfessionalOtTrainingHistoryDao professionalOtTrainingHistoryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, professionalOtTrainingHistoryDao);
		this.professionalOtTrainingHistoryDao = professionalOtTrainingHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ProfessionalOtTrainingHistoryEntity getSearchResult(Long id) {
		ProfessionalOtTrainingHistoryEntity entity = null;
		if ((id == null) || ((entity = professionalOtTrainingHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する専門作業療法士研修受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtTrainingHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		ProfessionalOtTrainingHistorySearchDto searchDto = new ProfessionalOtTrainingHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtTrainingHistoryEntity> getSearchResultByCondition(
			ProfessionalOtTrainingHistorySearchDto searchDto) {

		// 検索
		List<ProfessionalOtTrainingHistoryEntity> entities = professionalOtTrainingHistoryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ProfessionalOtTrainingHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("専門作業療法士研修受講履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
