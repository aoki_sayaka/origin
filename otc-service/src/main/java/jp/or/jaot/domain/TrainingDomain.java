package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingEntity;

/**
 * 研修会ドメイン
 */
@Component
@Scope("prototype")
public class TrainingDomain extends BaseDomain<TrainingEntity> {

	/**
	 * コンストラクタ
	 */
	public TrainingDomain() {
		setEntity(new TrainingEntity()); // 初期エンティティ
	}

	// TODO
}
