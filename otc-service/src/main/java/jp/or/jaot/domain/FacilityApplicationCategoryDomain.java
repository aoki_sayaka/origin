package jp.or.jaot.domain;

import java.util.Objects;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;

/**
 * 施設登録申請分類ドメイン
 */
@Component
@Scope("prototype")
public class FacilityApplicationCategoryDomain extends BaseDomain<FacilityApplicationCategoryEntity> {

	/**
	 * コンストラクタ
	 */
	public FacilityApplicationCategoryDomain() {
		setEntity(new FacilityApplicationCategoryEntity()); // 初期エンティティ
	}

	/**
	 * 施設分類と会員勤務先業務分類が同じかどうか
	 * @param category 分類種別
	 * @param categoryDivision 分類区分
	 * @param parentCode 親分類コード
	 * @param categoryCode 分類コード
	 * @return 同じであるならばtrueそうでないならばfalseを返す
	 */
	public boolean isSameFacilityCategory(int category, int categoryDivision, String parentCode, String categoryCode) {
		// 分類種別が同じかどうか
		if (this.getEntity().getCategory() != category) {
			return false;
		}
		// 分類区分が同じかどうか
		if (this.getEntity().getCategoryDivision() != categoryDivision) {
			return false;
		}
		// 親分類コードが同じかどうか。nullableなプロパティなのでnull同士でもtrueになるようにした
		if (!Objects.equals(this.getEntity().getParentCode(), parentCode)) {
			return false;
		}
		// 分類コードが同じかどうか。nullableなプロパティなのでnull同士でもtrueになるようにした
		if (!Objects.equals(this.getEntity().getCategoryCode(), categoryCode)) {
			return false;
		}
		// すべて一致してるならば、同じ
		return true;
	}
}
