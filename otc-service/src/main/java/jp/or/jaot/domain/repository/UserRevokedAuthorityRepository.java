package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.UserRevokedAuthorityDao;
import jp.or.jaot.domain.UserRevokedAuthorityDomain;
import jp.or.jaot.model.dto.search.UserRevokedAuthoritySearchDto;
import jp.or.jaot.model.entity.UserRevokedAuthorityEntity;

/**
 * 事務局権限リポジトリ
 */
@Repository
public class UserRevokedAuthorityRepository
		extends BaseRepository<UserRevokedAuthorityDomain, UserRevokedAuthorityEntity> {

	/** 事務局権限DAO */
	private final UserRevokedAuthorityDao userRevokedAuthorityDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param userRevokedAuthorityDao DAO
	 */
	@Autowired
	public UserRevokedAuthorityRepository(UserRevokedAuthorityDao userRevokedAuthorityDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, userRevokedAuthorityDao);
		this.userRevokedAuthorityDao = userRevokedAuthorityDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public UserRevokedAuthorityEntity getSearchResult(Long id) {
		UserRevokedAuthorityEntity entity = null;
		if ((id == null) || ((entity = userRevokedAuthorityDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する事務局権限が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<UserRevokedAuthorityEntity> getSearchResultByCondition(UserRevokedAuthoritySearchDto searchDto) {

		// 検索
		List<UserRevokedAuthorityEntity> entities = userRevokedAuthorityDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<UserRevokedAuthorityEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("事務局権限検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
