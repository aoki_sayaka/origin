package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.RecessHistoryDao;
import jp.or.jaot.domain.RecessHistoryDomain;
import jp.or.jaot.model.dto.search.RecessHistorySearchDto;
import jp.or.jaot.model.entity.RecessHistoryEntity;

/**
 * 休会履歴リポジトリ
 */
@Repository
public class RecessHistoryRepository extends BaseRepository<RecessHistoryDomain, RecessHistoryEntity> {

	/** 休会履歴DAO */
	private final RecessHistoryDao recessHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param recessHistoryDao DAO
	 */
	@Autowired
	public RecessHistoryRepository(RecessHistoryDao recessHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, recessHistoryDao);
		this.recessHistoryDao = recessHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public RecessHistoryEntity getSearchResult(Long id) {
		RecessHistoryEntity entity = null;
		if ((id == null) || ((entity = recessHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する休会履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<RecessHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		RecessHistorySearchDto searchDto = new RecessHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<RecessHistoryEntity> getSearchResultByCondition(RecessHistorySearchDto searchDto) {

		// 検索
		List<RecessHistoryEntity> entities = recessHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<RecessHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("休会履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
