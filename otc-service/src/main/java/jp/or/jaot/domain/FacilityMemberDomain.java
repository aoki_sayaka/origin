package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.FacilityMemberEntity;

/**
 * 施設養成校勤務者ドメイン
 */
@Component
@Scope("prototype")
public class FacilityMemberDomain extends BaseDomain<FacilityMemberEntity> {

	/**
	 * コンストラクタ
	 */
	public FacilityMemberDomain() {
		setEntity(new FacilityMemberEntity()); // 初期エンティティ
	}

	// TODO
}
