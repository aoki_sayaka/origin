package jp.or.jaot.domain.common;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.common.FileEntity;

/**
 * ファイルドメイン
 */
@Component
@Scope("prototype")
public class FileDomain extends BaseDomain<FileEntity> {

	/**
	 * コンストラクタ
	 */
	public FileDomain() {
		this.setEntity(new FileEntity()); // 初期エンティティ
	}

	/**
	 * パス設定
	 * @param path ディレクトリパス
	 */
	public void setPath(String path) {
		this.getEntity().setPath(path);
	}
}
