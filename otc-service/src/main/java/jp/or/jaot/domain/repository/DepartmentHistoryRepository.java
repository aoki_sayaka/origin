package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.DepartmentHistoryDao;
import jp.or.jaot.domain.DepartmentHistoryDomain;
import jp.or.jaot.model.dto.search.DepartmentHistorySearchDto;
import jp.or.jaot.model.entity.DepartmentHistoryEntity;

/**
 * 部署履歴リポジトリ
 */
@Repository
public class DepartmentHistoryRepository extends BaseRepository<DepartmentHistoryDomain, DepartmentHistoryEntity> {

	/** 部署履歴DAO */
	private final DepartmentHistoryDao departmentHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param departmentHistoryDao DAO
	 */
	@Autowired
	public DepartmentHistoryRepository(DepartmentHistoryDao departmentHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, departmentHistoryDao);
		this.departmentHistoryDao = departmentHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public DepartmentHistoryEntity getSearchResult(Long id) {
		DepartmentHistoryEntity entity = null;
		if ((id == null) || ((entity = departmentHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する部署履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<DepartmentHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		DepartmentHistorySearchDto searchDto = new DepartmentHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<DepartmentHistoryEntity> getSearchResultByCondition(DepartmentHistorySearchDto searchDto) {

		// 検索
		List<DepartmentHistoryEntity> entities = departmentHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<DepartmentHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("部署履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
