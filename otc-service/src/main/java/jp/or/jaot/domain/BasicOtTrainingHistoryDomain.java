package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;

/**
 * 基礎研修受講履歴ドメイン
 */
@Component
@Scope("prototype")
public class BasicOtTrainingHistoryDomain extends BaseDomain<BasicOtTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public BasicOtTrainingHistoryDomain() {
		setEntity(new BasicOtTrainingHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
