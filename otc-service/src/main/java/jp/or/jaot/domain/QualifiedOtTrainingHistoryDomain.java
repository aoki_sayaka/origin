package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;

/**
 * 認定作業療法士研修受講履歴ドメイン
 */
@Component
@Scope("prototype")
public class QualifiedOtTrainingHistoryDomain extends BaseDomain<QualifiedOtTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public QualifiedOtTrainingHistoryDomain() {
		setEntity(new QualifiedOtTrainingHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
