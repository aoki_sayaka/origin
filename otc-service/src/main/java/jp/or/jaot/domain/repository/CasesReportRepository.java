package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.CasesReportDao;
import jp.or.jaot.domain.CasesReportDomain;
import jp.or.jaot.model.dto.search.CasesReportSearchDto;
import jp.or.jaot.model.entity.CasesReportEntity;

/**
 * 事例報告リポジトリ
 */
@Repository
public class CasesReportRepository extends BaseRepository<CasesReportDomain, CasesReportEntity> {

	/** 事例報告コードDAO */
	private final CasesReportDao casesReportDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param casesReportDao DAO
	 */
	@Autowired
	public CasesReportRepository(CasesReportDao casesReportDao) {
		super(ERROR_PLACE.LOGIC_COMMON, casesReportDao);
		this.casesReportDao = casesReportDao;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<CasesReportEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		CasesReportSearchDto searchDto = new CasesReportSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<CasesReportEntity> getSearchResultByCondition(CasesReportSearchDto searchDto) {

		// 検索
		List<CasesReportEntity> entities = casesReportDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<CasesReportEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("事例報告検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
