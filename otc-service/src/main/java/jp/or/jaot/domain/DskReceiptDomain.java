package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.DskReceiptEntity;

/**
 * DskPOSTリクエストドメイン
 */
@Component
@Scope("prototype")
public class DskReceiptDomain extends BaseDomain<DskReceiptEntity> {
	/**
	 * コンストラクタ
	 */
	public DskReceiptDomain() {
		setEntity(new DskReceiptEntity()); // 初期エンティティ
	}

}
