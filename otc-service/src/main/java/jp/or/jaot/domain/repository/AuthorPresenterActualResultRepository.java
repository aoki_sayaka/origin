package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.AuthorPresenterActualResultDao;
import jp.or.jaot.domain.AuthorPresenterActualResultDomain;
import jp.or.jaot.model.dto.search.AuthorPresenterActualResultSearchDto;
import jp.or.jaot.model.entity.AuthorPresenterActualResultEntity;

/**
 * 活動実績(発表・著作等)リポジトリ
 */
@Repository
public class AuthorPresenterActualResultRepository
		extends BaseRepository<AuthorPresenterActualResultDomain, AuthorPresenterActualResultEntity> {

	/** 活動実績(発表・著作等)DAO */
	private final AuthorPresenterActualResultDao authorPresenterActualResultDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param authorPresenterActualResultDao DAO
	 */
	@Autowired
	public AuthorPresenterActualResultRepository(
			AuthorPresenterActualResultDao authorPresenterActualResultDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, authorPresenterActualResultDao);
		this.authorPresenterActualResultDao = authorPresenterActualResultDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public AuthorPresenterActualResultEntity getSearchResult(Long id) {
		AuthorPresenterActualResultEntity entity = null;
		if ((id == null) || ((entity = authorPresenterActualResultDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する活動実績(発表・著作等)が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<AuthorPresenterActualResultEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		AuthorPresenterActualResultSearchDto searchDto = new AuthorPresenterActualResultSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<AuthorPresenterActualResultEntity> getSearchResultByCondition(
			AuthorPresenterActualResultSearchDto searchDto) {

		// 検索
		List<AuthorPresenterActualResultEntity> entities = authorPresenterActualResultDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<AuthorPresenterActualResultEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("活動実績(発表・著作等)検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
