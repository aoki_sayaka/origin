package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;

/**
 * 専門作業療法士認定更新履歴ドメイン
 */
@Component
@Scope("prototype")
public class ProfessionalOtQualifyHistoryDomain extends BaseDomain<ProfessionalOtQualifyHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public ProfessionalOtQualifyHistoryDomain() {
		setEntity(new ProfessionalOtQualifyHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
