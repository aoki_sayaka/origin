package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.AttendingHistoryEntity;

/**
 * 受講履歴ドメイン
 */
@Component
@Scope("prototype")
public class AttendingHistoryDomain extends BaseDomain<AttendingHistoryEntity>{

	/**
	 * コンストラクタ
	 */
	public AttendingHistoryDomain() {
		setEntity(new AttendingHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
