package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.HistoryTableDefinitionDao;
import jp.or.jaot.domain.HistoryTableDefinitionDomain;
import jp.or.jaot.model.dto.search.HistoryTableDefinitionSearchDto;
import jp.or.jaot.model.entity.HistoryTableDefinitionEntity;

/**
 * 履歴テーブル定義リポジトリ<br>
 * インスタンス生成時にDBを読み込んでキャッシュ用リストを設定します。
 */
@Repository
public class HistoryTableDefinitionRepository
		extends BaseRepository<HistoryTableDefinitionDomain, HistoryTableDefinitionEntity> {

	/** 履歴テーブル定義DAO */
	private final HistoryTableDefinitionDao historyTableDefinitionDao;

	/** 履歴テーブル定義エンティティリスト(キャッシュ用) */
	private final List<HistoryTableDefinitionEntity> cacheEntities;

	/**
	 * コンストラクタ(インジェクション)
	 * @param historyTableDefinitionDao DAO
	 */
	@Autowired
	public HistoryTableDefinitionRepository(HistoryTableDefinitionDao historyTableDefinitionDao) {
		super(ERROR_PLACE.LOGIC_COMMON, historyTableDefinitionDao);
		this.historyTableDefinitionDao = historyTableDefinitionDao;
		this.cacheEntities = historyTableDefinitionDao.findByCondition(new HistoryTableDefinitionSearchDto());
		logger.info("履歴テーブル定義読込 : 件数={}", cacheEntities.size());
		return;
	}

	/**
	 * キャッシュ結果取得
	 * @return エンティティリスト
	 */
	public List<HistoryTableDefinitionEntity> getCacheResult() {
		return cacheEntities;
	}

	/**
	 * キャッシュ結果取得(テーブル名(物理))
	 * @param physicalNm テーブル名(物理)
	 * @return エンティティ
	 */
	public HistoryTableDefinitionEntity getCacheResultByPhysicalNm(String physicalNm) {
		return cacheEntities.stream().filter(E -> E.getTablePhysicalNm().equals(physicalNm)).findFirst().orElse(null);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public HistoryTableDefinitionEntity getSearchResult(Long id) {
		HistoryTableDefinitionEntity entity = null;
		if ((id == null) || ((entity = historyTableDefinitionDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する履歴テーブル定義が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(テーブル名(物理))
	 * @param physicalNm テーブル名(物理)
	 * @return エンティティリスト
	 */
	public List<HistoryTableDefinitionEntity> getSearchResultByPhysicalNm(String physicalNm) {

		// 検索条件生成
		HistoryTableDefinitionSearchDto searchDto = new HistoryTableDefinitionSearchDto();
		searchDto.setTablePhysicalNm(physicalNm); // テーブル名(物理)

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<HistoryTableDefinitionEntity> getSearchResultByCondition(HistoryTableDefinitionSearchDto searchDto) {

		// 検索
		List<HistoryTableDefinitionEntity> entities = historyTableDefinitionDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<HistoryTableDefinitionEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("履歴テーブル定義検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
