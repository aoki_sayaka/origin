package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.FacilityCategoryDao;
import jp.or.jaot.domain.FacilityCategoryDomain;
import jp.or.jaot.model.dto.search.FacilityCategorySearchDto;
import jp.or.jaot.model.entity.FacilityCategoryEntity;

/**
 * 施設分類リポジトリ
 */
@Repository
public class FacilityCategoryRepository extends BaseRepository<FacilityCategoryDomain, FacilityCategoryEntity> {

	/** 施設分類DAO */
	private final FacilityCategoryDao facilityCategoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param facilityCategoryDao DAO
	 */
	@Autowired
	public FacilityCategoryRepository(FacilityCategoryDao facilityCategoryDao) {
		super(ERROR_PLACE.LOGIC_COMMON, facilityCategoryDao);
		this.facilityCategoryDao = facilityCategoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public FacilityCategoryEntity getSearchResult(Long id) {
		FacilityCategoryEntity entity = null;
		if ((id == null) || ((entity = facilityCategoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する施設分類が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(施設養成校ID)
	 * @param facilityId 施設養成校ID
	 * @return エンティティリスト
	 */
	public List<FacilityCategoryEntity> getSearchResultByFacilityId(Long facilityId) {

		// 検索条件生成
		FacilityCategorySearchDto searchDto = new FacilityCategorySearchDto();
		searchDto.setFacilityIds(new Long[] { facilityId }); // 施設養成校ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(施設養成校ID)
	 * @param facilityId 施設養成校ID
	 * @return ドメインリスト
	 */
	public List<FacilityCategoryDomain> getSearchResultDomainByFacilityId(Long facilityId) {
		return this.getSearchResultByFacilityId(facilityId).stream()
				.map(entity -> this.createDomain(entity)).collect(Collectors.toList());
	}

	/**
	 * 検索結果取得(施設養成校ID)
	 * @param facilityIds 施設養成校ID配列
	 * @return エンティティリスト
	 */
	public List<FacilityCategoryEntity> getSearchResultByFacilityIds(Long[] facilityIds) {

		// 検索条件生成
		FacilityCategorySearchDto searchDto = new FacilityCategorySearchDto();
		searchDto.setFacilityIds(facilityIds); // 施設養成校ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<FacilityCategoryEntity> getSearchResultByCondition(FacilityCategorySearchDto searchDto) {

		// 検索
		List<FacilityCategoryEntity> entities = facilityCategoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<FacilityCategoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("施設分類検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
