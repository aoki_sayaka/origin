package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.QualificationEntity;

/**
 * 関連資格ドメイン
 */
@Component
@Scope("prototype")
public class QualificationDomain extends BaseDomain<QualificationEntity> {

	/**
	 * コンストラクタ
	 */
	public QualificationDomain() {
		setEntity(new QualificationEntity()); // 初期エンティティ
	}

	// TODO
}
