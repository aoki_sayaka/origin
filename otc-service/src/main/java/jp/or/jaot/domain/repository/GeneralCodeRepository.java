package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.GeneralCodeDao;
import jp.or.jaot.domain.GeneralCodeDomain;
import jp.or.jaot.model.dto.search.GeneralCodeSearchDto;
import jp.or.jaot.model.entity.GeneralCodeEntity;

/**
 * 汎用コードリポジトリ
 */
@Repository
public class GeneralCodeRepository extends BaseRepository<GeneralCodeDomain, GeneralCodeEntity> {

	/** 汎用コードDAO */
	private final GeneralCodeDao generalCodeDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param generalCodeDao DAO
	 */
	@Autowired
	public GeneralCodeRepository(GeneralCodeDao generalCodeDao) {
		super(ERROR_PLACE.LOGIC_COMMON, generalCodeDao);
		this.generalCodeDao = generalCodeDao;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<GeneralCodeEntity> getSearchResultByCondition(GeneralCodeSearchDto searchDto) {

		// 検索
		List<GeneralCodeEntity> entities = generalCodeDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<GeneralCodeEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("汎用コード検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
