package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberUpdateHistoryDao;
import jp.or.jaot.domain.MemberUpdateHistoryDomain;
import jp.or.jaot.model.dto.search.MemberUpdateHistorySearchDto;
import jp.or.jaot.model.entity.MemberUpdateHistoryEntity;

/**
 * 会員更新履歴リポジトリ
 */
@Repository
public class MemberUpdateHistoryRepository
		extends BaseRepository<MemberUpdateHistoryDomain, MemberUpdateHistoryEntity> {

	/** 会員更新履歴DAO */
	private final MemberUpdateHistoryDao memberUpdateHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberUpdateHistoryDao DAO
	 */
	@Autowired
	public MemberUpdateHistoryRepository(MemberUpdateHistoryDao memberUpdateHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberUpdateHistoryDao);
		this.memberUpdateHistoryDao = memberUpdateHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberUpdateHistoryEntity getSearchResult(Long id) {
		MemberUpdateHistoryEntity entity = null;
		if ((id == null) || ((entity = memberUpdateHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員更新履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberUpdateHistoryEntity> getSearchResultByCondition(MemberUpdateHistorySearchDto searchDto) {

		// 検索
		List<MemberUpdateHistoryEntity> entities = memberUpdateHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberUpdateHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員更新履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
