package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.DskReceiptDao;
import jp.or.jaot.domain.DskReceiptDomain;
import jp.or.jaot.model.dto.search.DskReceiptSearchDto;
import jp.or.jaot.model.entity.DskReceiptEntity;

/**
 *DskPOSTリクエストリポジトリ
 */
@Repository
public class DskReceiptRepository extends BaseRepository<DskReceiptDomain, DskReceiptEntity> {

	/** DskPOSTリクエストDAO */
	private final DskReceiptDao dskReceiptDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param dskReceiptDao DskReceiptDao
	 */
	@Autowired
	public DskReceiptRepository(DskReceiptDao dskReceiptDao) {
		super(ERROR_PLACE.BATCH_DSK, dskReceiptDao);
		this.dskReceiptDao = dskReceiptDao;
	}

	/**
	 * 検索結果取得(種別)
	 * @param typeCd1 種別1
	 * @return エンティティリスト
	 */
	public List<DskReceiptEntity> getSearchResultByTypeCd(String typeCd) {

		// 検索条件生成
		DskReceiptSearchDto searchDto = new DskReceiptSearchDto();
		searchDto.setTypeCd(typeCd); // 種別

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(会員番号、支払期限)
	 * @param memberNo 会員番号
	 * @param minFiscalYear 年度
	 * @return エンティティリスト
	 */
	public List<DskReceiptEntity> getSearchResultByPaymentDueDate(String memberNo, Date paymentDueDate) {

		// 検索条件生成
		DskReceiptSearchDto searchDto = new DskReceiptSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号
		searchDto.setPaymentDueDate(paymentDueDate); // 請求年度

		List<DskReceiptEntity> list = getSearchResultByCondition(searchDto);
		list.sort((i, j) -> (int) (j.getId() - i.getId()));

		return list;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<DskReceiptEntity> getSearchResultByCondition(DskReceiptSearchDto searchDto) {

		// 検索
		List<DskReceiptEntity> entities = dskReceiptDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<DskReceiptEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("DskReceipt検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

}
