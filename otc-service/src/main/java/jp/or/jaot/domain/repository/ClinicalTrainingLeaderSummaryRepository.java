package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ClinicalTrainingLeaderSummaryDao;
import jp.or.jaot.domain.ClinicalTrainingLeaderSummaryDomain;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderSummarySearchDto;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;

/**
 * 臨床実習指導者履歴リポジトリ
 */
@Repository
public class ClinicalTrainingLeaderSummaryRepository
		extends BaseRepository<ClinicalTrainingLeaderSummaryDomain, ClinicalTrainingLeaderSummaryEntity> {

	/** 臨床実習指導者履歴DAO */
	private final ClinicalTrainingLeaderSummaryDao clinicalTrainingLeaderSummaryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param clinicalTrainingLeaderSummaryDao DAO
	 */
	@Autowired
	public ClinicalTrainingLeaderSummaryRepository(ClinicalTrainingLeaderSummaryDao clinicalTrainingLeaderSummaryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, clinicalTrainingLeaderSummaryDao);
		this.clinicalTrainingLeaderSummaryDao = clinicalTrainingLeaderSummaryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ClinicalTrainingLeaderSummaryEntity getSearchResult(Long id) {
		ClinicalTrainingLeaderSummaryEntity entity = null;
		if ((id == null) || ((entity = clinicalTrainingLeaderSummaryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する臨床実習指導者履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ClinicalTrainingLeaderSummaryEntity getSearchResultByMemberNo(Integer memberNo) {
		ClinicalTrainingLeaderSummaryEntity entity = null;
		if ((memberNo == null) || ((entity = clinicalTrainingLeaderSummaryDao.findByMemberNo(memberNo)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("memberNo=%s", memberNo);
			String message = String.format("検索条件に指定された会員番号に該当する会員が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ClinicalTrainingLeaderSummaryEntity> getSearchResultByCondition(
			ClinicalTrainingLeaderSummarySearchDto searchDto) {

		// 検索
		List<ClinicalTrainingLeaderSummaryEntity> entities = clinicalTrainingLeaderSummaryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ClinicalTrainingLeaderSummaryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("臨床実習指導者履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
