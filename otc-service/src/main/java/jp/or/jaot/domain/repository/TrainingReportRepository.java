package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingReportDao;
import jp.or.jaot.domain.TrainingReportDomain;
import jp.or.jaot.model.dto.search.TrainingReportSearchDto;
import jp.or.jaot.model.entity.TrainingReportEntity;

/**
 * 研修会報告書リポジトリ
 */
@Repository
public class TrainingReportRepository
		extends BaseRepository<TrainingReportDomain, TrainingReportEntity> {

	/** 研修会報告書DAO */
	private final TrainingReportDao trainingReportDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingReportDao DAO
	 */
	@Autowired
	public TrainingReportRepository(TrainingReportDao trainingReportDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, trainingReportDao);
		this.trainingReportDao = trainingReportDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingReportEntity getSearchResult(Long id) {
		TrainingReportEntity entity = null;
		if ((id == null) || ((entity = trainingReportDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修会報告書が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(キー)
	 * @param ids ID配列
	 * @return エンティティリスト
	 * @throws SearchBusinessException 検索業務例外
	 */
	public List<TrainingReportEntity> getSearchResult(Long[] ids) {
		List<TrainingReportEntity> entities = trainingReportDao.find(ids);
		if (ids.length != entities.stream().filter(E -> E != null).count()) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("ids=%s", StringUtils.join(ids, ","));
			String message = String.format("検索条件に指定されたID配列に該当する研修会報告書が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entities;
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNo 研修会番号
	 * @return エンティティリスト
	 */
	public List<TrainingReportEntity> getSearchResultByTrainingNo(String trainingNo) {

		// 検索条件生成
		TrainingReportSearchDto searchDto = new TrainingReportSearchDto();
		searchDto.setTrainingNos(new String[] { trainingNo }); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNos 研修会番号配列
	 * @return エンティティリスト
	 */
	public List<TrainingReportEntity> getSearchResultByTrainingNos(String[] trainingNos) {

		// 検索条件生成
		TrainingReportSearchDto searchDto = new TrainingReportSearchDto();
		searchDto.setTrainingNos(trainingNos); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingReportEntity> getSearchResultByCondition(
			TrainingReportSearchDto searchDto) {

		// 検索
		List<TrainingReportEntity> entities = trainingReportDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingReportEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修会報告書検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
