package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ClinicalTrainingPointApplicationEntity;

/**
 * 臨床実習ポイント申請ドメイン
 */
@Component
@Scope("prototype")
public class ClinicalTrainingPointApplicationDomain extends BaseDomain<ClinicalTrainingPointApplicationEntity> {

	/**
	 * コンストラクタ
	 */
	public ClinicalTrainingPointApplicationDomain() {
		setEntity(new ClinicalTrainingPointApplicationEntity()); // 初期エンティティ
	}

	// TODO
}
