package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.RecessHistoryEntity;

/**
 * 休会履歴ドメイン
 */
@Component
@Scope("prototype")
public class RecessHistoryDomain extends BaseDomain<RecessHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public RecessHistoryDomain() {
		setEntity(new RecessHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
