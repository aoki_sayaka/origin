package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ZipcodeDao;
import jp.or.jaot.domain.ZipcodeDomain;
import jp.or.jaot.model.dto.search.ZipcodeSearchDto;
import jp.or.jaot.model.entity.ZipcodeEntity;

/**
 * 郵便番号リポジトリ
 */
@Repository
public class ZipcodeRepository extends BaseRepository<ZipcodeDomain, ZipcodeEntity> {

	/** 郵便番号DAO */
	private final ZipcodeDao zipcodeDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param zipcodeDao DAO
	 */
	@Autowired
	public ZipcodeRepository(ZipcodeDao zipcodeDao) {
		super(ERROR_PLACE.LOGIC_COMMON, zipcodeDao);
		this.zipcodeDao = zipcodeDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ZipcodeEntity getSearchResult(Long id) {
		ZipcodeEntity entity = null;
		if ((id == null) || ((entity = zipcodeDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する郵便番号が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ZipcodeEntity> getSearchResultByCondition(ZipcodeSearchDto searchDto) {

		// 検索
		List<ZipcodeEntity> entities = zipcodeDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ZipcodeEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("郵便番号検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
