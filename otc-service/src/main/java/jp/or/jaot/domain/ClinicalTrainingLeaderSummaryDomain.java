package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;

/**
 * 臨床実習指導者履歴ドメイン
 */
@Component
@Scope("prototype")
public class ClinicalTrainingLeaderSummaryDomain extends BaseDomain<ClinicalTrainingLeaderSummaryEntity> {

	/**
	 * コンストラクタ
	 */
	public ClinicalTrainingLeaderSummaryDomain() {
		setEntity(new ClinicalTrainingLeaderSummaryEntity()); // 初期エンティティ
	}
}
