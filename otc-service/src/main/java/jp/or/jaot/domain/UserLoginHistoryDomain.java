package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.UserLoginHistoryEntity;

/**
 * 事務局サイトログイン履歴ドメイン
 */
@Component
@Scope("prototype")
public class UserLoginHistoryDomain extends BaseDomain<UserLoginHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public UserLoginHistoryDomain() {
		setEntity(new UserLoginHistoryEntity()); // 初期エンティティ
	}

}
