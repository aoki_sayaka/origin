package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.LifelongEducationStatusEntity;

/**
 * 生涯教育ステータスドメイン
 */
@Component
@Scope("prototype")
public class LifelongEducationStatusDomain extends BaseDomain<LifelongEducationStatusEntity> {
	/**
	 * コンストラクタ
	 */
	public LifelongEducationStatusDomain() {
		setEntity(new LifelongEducationStatusEntity()); // 初期エンティティ
	}

}
