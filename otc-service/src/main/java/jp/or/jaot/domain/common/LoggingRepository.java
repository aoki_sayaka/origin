package jp.or.jaot.domain.common;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.model.BaseEntity;
import jp.or.jaot.core.model.LoggingElementDto;
import jp.or.jaot.core.model.LoggingUpdateEntityDto;
import jp.or.jaot.core.model.LoggingUpdateEntityDto.CATEGORY;
import jp.or.jaot.core.util.LoggingUtil;
import jp.or.jaot.core.util.ReflectionUtil;
import jp.or.jaot.core.util.StringUtil;
import jp.or.jaot.datasource.MemberDao;
import jp.or.jaot.datasource.MemberUpdateHistoryDao;
import jp.or.jaot.model.entity.HistoryFieldDefinitionEntity;
import jp.or.jaot.model.entity.HistoryTableDefinitionEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberUpdateHistoryEntity;
import lombok.AllArgsConstructor;

/**
 * ログ情報リポジトリ
 */
@Repository
@AllArgsConstructor
public class LoggingRepository {

	/** ロガーインスタンス */
	private static final Logger logger = LoggerFactory.getLogger(LoggingRepository.class);

	/** 更新履歴対象(会員名簿) */
	private static final String[] DEF_MEMBER_UPDATES = { "MemberEntity", "MemberRevokedAuthorityEntity",
			"MemberQualificationEntity", "MemberLocalActivityEntity", "PaotMembershipFeeDeliveryEntity",
			"MembershipFeeDeliveryEntity", "SocialContributionActualFieldEntity",
			"SocialContributionActual2FieldEntity", "SocialContributionActualResultEntity",
			"AuthorPresenterActualFieldEntity", "AuthorPresenterActualResultEntity", "ActivityHistoryEntity",
			"OfficerHistoryEntity", "RewardPunishHistoryEntity", "DepartmentHistoryEntity",
			"PaotMembershipHistoryEntity", "PaotOfficerHistoryEntity", "PublicationSendHistoryEntity",
			"RecessHistoryEntity", "MemberUpdateHistoryEntity" }; // TODO

	/** 更新履歴対象(受講履歴) */
	private static final String[] DEF_ATTEND_UPDATES = { "AttendingHistoryEntity" };// TODO 

	/** 会員DAO */
	private final MemberDao memberDao;

	/** 会員更新履歴DAO */
	private final MemberUpdateHistoryDao memberUpdateHistoryDao;

	/**
	 * 更新履歴対象テーブル名取得(会員名簿)
	 * @return テーブル名(物理)リスト
	 */
	public List<String> getMembersUpdateTables() {
		return Arrays.asList(DEF_MEMBER_UPDATES).stream().map(E -> LoggingUtil.cnvNameEntityToTable(E))
				.collect(Collectors.toList());
	}

	/**
	 * 更新履歴対象テーブル名取得(受講履歴)
	 * @return テーブル名(物理)リスト
	 */
	public List<String> getAttendUpdateTables() {
		return Arrays.asList(DEF_ATTEND_UPDATES).stream().map(E -> LoggingUtil.cnvNameEntityToTable(E))
				.collect(Collectors.toList());
	}

	/**
	 * エンティティログ出力
	 * @param tableEntities 履歴テーブル定義エンティティリスト
	 * @param fieldEntities 履歴フィールド定義エンティティリスト
	 * @param category 更新区分
	 * @param entity 対象エンティティ
	 * @param dto 更新エンティティログ情報DTO
	 */
	public void loggingEntity(
			List<HistoryTableDefinitionEntity> tableEntities, List<HistoryFieldDefinitionEntity> fieldEntities,
			CATEGORY category, BaseEntity entity, LoggingUpdateEntityDto dto) {

		// ログ出力
		int elementSize = dto.getLoggingElements().size();
		String entityName = entity.getClass().getSimpleName();
		logger.info("ログ情報 : entityName={}, dto={}", entityName, dto);

		// 件数チェック
		if (elementSize == 0) {
			return;
		}

		// ログ出力
		if (Arrays.asList(DEF_MEMBER_UPDATES).contains(entityName)) { // 会員名簿
			insertLoggingEntity(tableEntities, fieldEntities, category, entity, dto, memberUpdateHistoryDao);
		} else if (Arrays.asList(DEF_ATTEND_UPDATES).contains(entityName)) { // 受講履歴
			// TODO
		}
	}

	/**
	 * ログ登録
	 * @param tableEntities 履歴テーブル定義エンティティリスト
	 * @param fieldEntities 履歴フィールド定義エンティティリスト
	 * @param category 更新区分
	 * @param entity 対象エンティティ
	 * @param dto 更新エンティティログ情報DTO
	 * @param baseDao 対象DAO
	 */
	private void insertLoggingEntity(
			List<HistoryTableDefinitionEntity> tableEntities, List<HistoryFieldDefinitionEntity> fieldEntities,
			CATEGORY category, BaseEntity entity, LoggingUpdateEntityDto dto, BaseDao<?> baseDao) {

		// 履歴テーブル定義取得
		HistoryTableDefinitionEntity tableEntity = tableEntities.stream()
				.filter(E -> E.getTablePhysicalNm().equals(dto.getPhysicalName())).findFirst().orElse(null);
		if (tableEntity == null) {
			logger.error("該当する履歴テーブル定義が存在しません : tablePhysicalNm={}", dto.getPhysicalName());
			return;
		}

		// 会員エンティティ取得
		MemberEntity memberEntity = getMemberEntity(entity);

		// ログ情報(要素)
		int updateNo = 1; // 更新連番
		Long tableDefinitionId = tableEntity.getId();// 履歴テーブル定義ID
		String updateUser = entity.getUpdateUser(); // 更新者
		Timestamp updateDatetime = entity.getUpdateDatetime(); // 更新日時
		for (LoggingElementDto element : dto.getLoggingElements()) {

			// 項目名取得(変換)
			String fieldPhysicalNm = StringUtil.camelToSnake(element.getFieldName());

			// 履歴フィールド定義取得
			HistoryFieldDefinitionEntity fieldEntity = fieldEntities.stream()
					.filter(E -> E.getTableDefinitionId().equals(tableDefinitionId))
					.filter(E -> E.getFieldPhysicalNm().equals(fieldPhysicalNm)).findFirst().orElse(null);
			if (fieldEntity == null) {
				logger.error("該当する履歴フィールド定義が存在しません : fieldPhysicalNm={}", fieldPhysicalNm);
				continue;
			}

			// 属性設定
			MemberUpdateHistoryEntity insertEntity = new MemberUpdateHistoryEntity();
			insertEntity.setUpdateNo(updateNo++); // 更新連番
			insertEntity.setMemberUpdateDatetime(updateDatetime); // 更新日時
			insertEntity.setUpdateUserId(updateUser); // 更新ユーザID
			insertEntity.setMemberNo(memberEntity.getMemberNo()); // 会員番号
			insertEntity.setPaotCd(memberEntity.getPrefAssociationNo()); // 県士会番号
			insertEntity.setUpdateDate(updateDatetime); // 更新日
			insertEntity.setCreateUser(updateUser); // 作成者
			insertEntity.setUpdateUser(updateUser); // 更新者
			insertEntity.setUpdateCategoryCd(category.getValue()); // 更新区分コード
			insertEntity.setTableCd(tableEntity.getTableCd()); // テーブルコード
			insertEntity.setTableNm(tableEntity.getTableNm()); // テーブル名
			insertEntity.setFieldCd(fieldEntity.getFieldCd()); // 項目コード
			insertEntity.setFieldNm(fieldEntity.getFieldNm()); // 項目名
			insertEntity.setContentBefore(element.getBefore()); // 変更前
			insertEntity.setContentAfter(element.getAfter()); // 変更後

			// 追加
			baseDao.insert(insertEntity);

			logger.info("履歴追加 : {}", insertEntity);
		}
	}

	/**
	 * 会員エンティティ取得
	 * @param entity 対象エンティティ
	 * @return 会員エンティティ
	 */
	private MemberEntity getMemberEntity(BaseEntity entity) {
		try {
			return memberDao.findByMemberNo((Integer) ReflectionUtil.getValue(entity, "memberNo"));
		} catch (Exception e) {
			return new MemberEntity(); // 空エンティティ
		}
	}

	// TODO
}
