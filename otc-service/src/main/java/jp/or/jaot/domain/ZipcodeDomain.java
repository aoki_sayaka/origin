package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ZipcodeEntity;

/**
 * 郵便番号ドメイン
 */
@Component
@Scope("prototype")
public class ZipcodeDomain extends BaseDomain<ZipcodeEntity> {

	/**
	 * コンストラクタ
	 */
	public ZipcodeDomain() {
		setEntity(new ZipcodeEntity()); // 初期エンティティ
	}

	// TODO
}
