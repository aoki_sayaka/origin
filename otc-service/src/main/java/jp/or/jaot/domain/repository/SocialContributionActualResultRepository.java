package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.SocialContributionActualResultDao;
import jp.or.jaot.domain.SocialContributionActualResultDomain;
import jp.or.jaot.model.dto.search.SocialContributionActualResultSearchDto;
import jp.or.jaot.model.entity.SocialContributionActualResultEntity;

/**
 * 活動実績(後輩育成・社会貢献)リポジトリ
 */
@Repository
public class SocialContributionActualResultRepository
		extends BaseRepository<SocialContributionActualResultDomain, SocialContributionActualResultEntity> {

	/** 活動実績(後輩育成・社会貢献)DAO */
	private final SocialContributionActualResultDao socialContributionActualResultDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param socialContributionActualResultDao DAO
	 */
	@Autowired
	public SocialContributionActualResultRepository(
			SocialContributionActualResultDao socialContributionActualResultDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, socialContributionActualResultDao);
		this.socialContributionActualResultDao = socialContributionActualResultDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public SocialContributionActualResultEntity getSearchResult(Long id) {
		SocialContributionActualResultEntity entity = null;
		if ((id == null) || ((entity = socialContributionActualResultDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する活動実績(後輩育成・社会貢献)が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<SocialContributionActualResultEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		SocialContributionActualResultSearchDto searchDto = new SocialContributionActualResultSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<SocialContributionActualResultEntity> getSearchResultByCondition(
			SocialContributionActualResultSearchDto searchDto) {

		// 検索
		List<SocialContributionActualResultEntity> entities = socialContributionActualResultDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<SocialContributionActualResultEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("活動実績(後輩育成・社会貢献)検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
