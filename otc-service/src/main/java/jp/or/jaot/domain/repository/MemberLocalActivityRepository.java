package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberLocalActivityDao;
import jp.or.jaot.domain.MemberLocalActivityDomain;
import jp.or.jaot.model.dto.search.MemberLocalActivitySearchDto;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;

/**
 * 会員自治体参画情報リポジトリ
 */
@Repository
public class MemberLocalActivityRepository
		extends BaseRepository<MemberLocalActivityDomain, MemberLocalActivityEntity> {

	/** 会員自治体参画情報DAO */
	private final MemberLocalActivityDao memberLocalActivityDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberLocalActivityDao DAO
	 */
	@Autowired
	public MemberLocalActivityRepository(MemberLocalActivityDao memberLocalActivityDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberLocalActivityDao);
		this.memberLocalActivityDao = memberLocalActivityDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberLocalActivityEntity getSearchResult(Long id) {
		MemberLocalActivityEntity entity = null;
		if ((id == null) || ((entity = memberLocalActivityDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員自治体参画情報が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MemberLocalActivityEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MemberLocalActivitySearchDto searchDto = new MemberLocalActivitySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberLocalActivityEntity> getSearchResultByCondition(MemberLocalActivitySearchDto searchDto) {

		// 検索
		List<MemberLocalActivityEntity> entities = memberLocalActivityDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberLocalActivityEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員自治体参画情報検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
