package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ClinicalTrainingPointApplicationDao;
import jp.or.jaot.domain.ClinicalTrainingPointApplicationDomain;
import jp.or.jaot.model.dto.search.ClinicalTrainingPointApplicationSearchDto;
import jp.or.jaot.model.entity.ClinicalTrainingPointApplicationEntity;

/**
 * 臨床実習ポイント申請リポジトリ
 */
@Repository
public class ClinicalTrainingPointApplicationRepository
		extends BaseRepository<ClinicalTrainingPointApplicationDomain, ClinicalTrainingPointApplicationEntity> {

	/** 臨床実習ポイント申請DAO */
	private final ClinicalTrainingPointApplicationDao clinicalTrainingPointApplicationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param clinicalTrainingPointApplicationDao DAO
	 */
	//TODO ERROR_PLACEが未定義
	@Autowired
	public ClinicalTrainingPointApplicationRepository(ClinicalTrainingPointApplicationDao clinicalTrainingPointApplicationDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, clinicalTrainingPointApplicationDao);
		this.clinicalTrainingPointApplicationDao = clinicalTrainingPointApplicationDao;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ClinicalTrainingPointApplicationEntity> getSearchResultByCondition(
			ClinicalTrainingPointApplicationSearchDto searchDto) {

		// 検索
		List<ClinicalTrainingPointApplicationEntity> entities = clinicalTrainingPointApplicationDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ClinicalTrainingPointApplicationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("臨床実習ポイント申請検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
