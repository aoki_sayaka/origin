package jp.or.jaot.domain.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.model.Entity;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.common.FileDao;
import jp.or.jaot.model.dto.common.FileDto;
import jp.or.jaot.model.entity.common.FileEntity;

/**
 * ファイルリポジトリ
 */
@Repository
public class FileRepository extends BaseRepository<FileDomain, FileEntity> {

	/** ファイルDAO */
	private final FileDao fileDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param fileDao ファイルDAO
	 */
	@Autowired
	public FileRepository(FileDao fileDao) {
		super(ERROR_PLACE.FILE, null);
		this.fileDao = fileDao;
	}

	/**
	 * アップロード
	 * @param dto ファイルDTO
	 */
	public void upload(FileDto dto) {

		// ドメイン生成
		FileDomain domain = createDomain(dto.getFirstEntity());

		// ディレクトリパス設定
		domain.setPath(getDirectoryPath());

		// アップロード
		fileDao.upload(domain.getEntity());
	}

	/**
	 * ディレクトリパスを取得
	 * @return ディレクトリパス
	 */
	private String getDirectoryPath() {

		// TODO(本実装時はパスの取得先を変更(環境変数 or プロパティファイル or DB))
		String path = "E:\\Temp";

		return path;
	}

	/**
	 * 追加
	 * @param entity エンティティ
	 */
	@Override
	@Deprecated
	public void insert(Entity entity) {
		// 使用不可
	}

	/**
	 * 更新
	 * @param entity エンティティ
	 */
	@Override
	@Deprecated
	public void update(Entity entity) {
		// 使用不可
	}

	/** 削除(物理)
	 * @param entity エンティティ
	 */
	@Override
	@Deprecated
	public void delete(Entity entity) {
		// 使用不可
	}
}
