package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MtdlpCaseDao;
import jp.or.jaot.domain.MtdlpCaseDomain;
import jp.or.jaot.model.dto.search.MtdlpCaseSearchDto;
import jp.or.jaot.model.entity.MtdlpCaseEntity;

/**
 * MTDLP事例リポジトリ
 */
@Repository
public class MtdlpCaseRepository extends BaseRepository<MtdlpCaseDomain, MtdlpCaseEntity> {

	/** MTDLP事例DAO */
	private final MtdlpCaseDao mtdlpCaseDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param mtdlpCaseDao DAO
	 */
	@Autowired
	public MtdlpCaseRepository(MtdlpCaseDao mtdlpCaseDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, mtdlpCaseDao);
		this.mtdlpCaseDao = mtdlpCaseDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MtdlpCaseEntity getSearchResult(Long id) {
		MtdlpCaseEntity entity = null;
		if ((id == null) || ((entity = mtdlpCaseDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定された会員番号に該当するMTDLP事例が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MtdlpCaseEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MtdlpCaseSearchDto searchDto = new MtdlpCaseSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MtdlpCaseEntity> getSearchResultByCondition(
			MtdlpCaseSearchDto searchDto) {

		// 検索
		List<MtdlpCaseEntity> entities = mtdlpCaseDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MtdlpCaseEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("MTDLP事例検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
