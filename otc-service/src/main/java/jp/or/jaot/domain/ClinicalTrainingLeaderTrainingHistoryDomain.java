package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;

/**
 * 臨床実習指導者研修受講履歴ドメイン
 */
@Component
@Scope("prototype")
public class ClinicalTrainingLeaderTrainingHistoryDomain
		extends BaseDomain<ClinicalTrainingLeaderTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public ClinicalTrainingLeaderTrainingHistoryDomain() {
		setEntity(new ClinicalTrainingLeaderTrainingHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
