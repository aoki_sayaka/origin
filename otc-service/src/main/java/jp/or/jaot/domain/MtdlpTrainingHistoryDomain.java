package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;

/**
 * MTDLP研修受講履歴ドメイン
 */
@Component
@Scope("prototype")
public class MtdlpTrainingHistoryDomain extends BaseDomain<MtdlpTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public MtdlpTrainingHistoryDomain() {
		setEntity(new MtdlpTrainingHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
