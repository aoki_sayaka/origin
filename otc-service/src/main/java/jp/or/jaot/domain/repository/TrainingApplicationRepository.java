package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingApplicationDao;
import jp.or.jaot.domain.TrainingApplicationDomain;
import jp.or.jaot.model.dto.search.TrainingApplicationSearchDto;
import jp.or.jaot.model.entity.TrainingApplicationEntity;

/**
 * 研修会申込リポジトリ
 */
@Repository
public class TrainingApplicationRepository
		extends BaseRepository<TrainingApplicationDomain, TrainingApplicationEntity> {

	/** 研修会申込DAO */
	private final TrainingApplicationDao trainingApplicationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingApplicationDao DAO
	 */
	@Autowired
	public TrainingApplicationRepository(TrainingApplicationDao trainingApplicationDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, trainingApplicationDao);
		this.trainingApplicationDao = trainingApplicationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingApplicationEntity getSearchResult(Long id) {
		TrainingApplicationEntity entity = null;
		if ((id == null) || ((entity = trainingApplicationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修会申込が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNo 研修会番号
	 * @return エンティティリスト
	 */
	public List<TrainingApplicationEntity> getSearchResultByTrainingNo(String trainingNo) {

		// 検索条件生成
		TrainingApplicationSearchDto searchDto = new TrainingApplicationSearchDto();
		searchDto.setTrainingNos(new String[] { trainingNo }); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNos 研修会番号配列
	 * @return エンティティリスト
	 */
	public List<TrainingApplicationEntity> getSearchResultByTrainingNos(String[] trainingNos) {

		// 検索条件生成
		TrainingApplicationSearchDto searchDto = new TrainingApplicationSearchDto();
		searchDto.setTrainingNos(trainingNos); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingApplicationEntity> getSearchResultByCondition(TrainingApplicationSearchDto searchDto) {

		// 検索
		List<TrainingApplicationEntity> entities = trainingApplicationDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingApplicationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修会申込検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
