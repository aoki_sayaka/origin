package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingTempApplicationEntity;

/**
 * 研修会仮申込ドメイン
 */
@Component
@Scope("prototype")
public class TrainingTempApplicationDomain extends BaseDomain<TrainingTempApplicationEntity> {

	/**
	 * コンストラクタ
	 */
	public TrainingTempApplicationDomain() {
		setEntity(new TrainingTempApplicationEntity()); // 初期エンティティ
	}

	// TODO
}
