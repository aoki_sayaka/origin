package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberRevokedAuthorityEntity;

/**
 * 会員権限ドメイン
 */
@Component
@Scope("prototype")
public class MemberRevokedAuthorityDomain extends BaseDomain<MemberRevokedAuthorityEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberRevokedAuthorityDomain() {
		setEntity(new MemberRevokedAuthorityEntity()); // 初期エンティティ
	}

	// TODO
}
