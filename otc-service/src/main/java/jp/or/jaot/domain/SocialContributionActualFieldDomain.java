package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.SocialContributionActualFieldEntity;

/**
 * 活動実績の領域(後輩育成・社会貢献)ドメイン
 */
@Component
@Scope("prototype")
public class SocialContributionActualFieldDomain extends BaseDomain<SocialContributionActualFieldEntity> {

	/**
	 * コンストラクタ
	 */
	public SocialContributionActualFieldDomain() {
		setEntity(new SocialContributionActualFieldEntity()); // 初期エンティティ
	}

	// TODO
}
