package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.CaseReportDao;
import jp.or.jaot.domain.CaseReportDomain;
import jp.or.jaot.model.dto.search.CaseReportSearchDto;
import jp.or.jaot.model.entity.CaseReportEntity;

/**
 * 事例報告リポジトリ
 */
@Repository
public class CaseReportRepository extends BaseRepository<CaseReportDomain, CaseReportEntity> {

	/** 事例報告コードDAO */
	private final CaseReportDao caseReportDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param caseReportDao DAO
	 */
	@Autowired
	public CaseReportRepository(CaseReportDao caseReportDao) {
		super(ERROR_PLACE.LOGIC_COMMON, caseReportDao);
		this.caseReportDao = caseReportDao;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<CaseReportEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		CaseReportSearchDto searchDto = new CaseReportSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<CaseReportEntity> getSearchResultByCondition(CaseReportSearchDto searchDto) {

		// 検索
		List<CaseReportEntity> entities = caseReportDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<CaseReportEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("事例報告検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
