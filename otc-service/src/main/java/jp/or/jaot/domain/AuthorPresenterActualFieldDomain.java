package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.AuthorPresenterActualFieldEntity;

/**
 * 活動実績の領域(発表・著作等)ドメイン
 */
@Component
@Scope("prototype")
public class AuthorPresenterActualFieldDomain extends BaseDomain<AuthorPresenterActualFieldEntity> {

	/**
	 * コンストラクタ
	 */
	public AuthorPresenterActualFieldDomain() {
		setEntity(new AuthorPresenterActualFieldEntity()); // 初期エンティティ
	}

	// TODO
}
