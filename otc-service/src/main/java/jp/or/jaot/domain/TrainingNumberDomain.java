package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingNumberEntity;

/**
 * 研修番号マスタドメイン
 */
@Component
@Scope("prototype")
public class TrainingNumberDomain extends BaseDomain<TrainingNumberEntity> {

	/**
	 * コンストラクタ
	 */
	public TrainingNumberDomain() {
		setEntity(new TrainingNumberEntity()); // 初期エンティティ
	}
}
