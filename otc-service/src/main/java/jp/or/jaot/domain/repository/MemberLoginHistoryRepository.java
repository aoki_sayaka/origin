package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberLoginHistoryDao;
import jp.or.jaot.domain.MemberLoginHistoryDomain;
import jp.or.jaot.model.dto.search.MemberLoginHistorySearchDto;
import jp.or.jaot.model.entity.MemberLoginHistoryEntity;

/**
 * 会員ポータルログイン履歴リポジトリ
 */
@Repository
public class MemberLoginHistoryRepository extends BaseRepository<MemberLoginHistoryDomain, MemberLoginHistoryEntity> {

	/** 会員ポータルログイン履歴DAO */
	private final MemberLoginHistoryDao memberLoginHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberLoginHistoryDao DAO
	 */
	@Autowired
	public MemberLoginHistoryRepository(MemberLoginHistoryDao memberLoginHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberLoginHistoryDao);
		this.memberLoginHistoryDao = memberLoginHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberLoginHistoryEntity getSearchResult(Long id) {
		MemberLoginHistoryEntity entity = null;
		if ((id == null) || ((entity = memberLoginHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員ポータルログイン履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MemberLoginHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MemberLoginHistorySearchDto searchDto = new MemberLoginHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberLoginHistoryEntity> getSearchResultByCondition(MemberLoginHistorySearchDto searchDto) {

		// 検索
		List<MemberLoginHistoryEntity> entities = memberLoginHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberLoginHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員ポータルログイン履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
