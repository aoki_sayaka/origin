package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.QualificationOtHistoryEntity;

/**
 * 認定作業療法士履歴ドメイン
 */
@Component
@Scope("prototype")
public class QualificationOtHistoryDomain extends BaseDomain<QualificationOtHistoryEntity>{

	/**
	 * コンストラクタ
	 */
	public QualificationOtHistoryDomain() {
		setEntity(new QualificationOtHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
