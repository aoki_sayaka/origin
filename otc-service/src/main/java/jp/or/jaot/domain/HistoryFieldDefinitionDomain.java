package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.HistoryFieldDefinitionEntity;

/**
 * 履歴フィールド定義ドメイン
 */
@Component
@Scope("prototype")
public class HistoryFieldDefinitionDomain extends BaseDomain<HistoryFieldDefinitionEntity> {

	/**
	 * コンストラクタ
	 */
	public HistoryFieldDefinitionDomain() {
		setEntity(new HistoryFieldDefinitionEntity()); // 初期エンティティ
	}

	// TODO
}
