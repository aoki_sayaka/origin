package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.NoticeConditionPrefDao;
import jp.or.jaot.domain.NoticeConditionPrefDomain;
import jp.or.jaot.model.dto.search.NoticeConditionPrefSearchDto;
import jp.or.jaot.model.entity.NoticeConditionPrefEntity;

/**
 * お知らせ(都道府県)リポジトリ
 */
@Repository
public class NoticeConditionPrefRepository
		extends BaseRepository<NoticeConditionPrefDomain, NoticeConditionPrefEntity> {

	/** お知らせ(都道府県)DAO */
	private final NoticeConditionPrefDao noticeConditionPrefDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param noticeConditionPrefDao DAO
	 */
	@Autowired
	public NoticeConditionPrefRepository(NoticeConditionPrefDao noticeConditionPrefDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, noticeConditionPrefDao);
		this.noticeConditionPrefDao = noticeConditionPrefDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public NoticeConditionPrefEntity getSearchResult(Long id) {
		NoticeConditionPrefEntity entity = null;
		if ((id == null) || ((entity = noticeConditionPrefDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当するお知らせ(都道府県)が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(お知らせID)
	 * @param noticeIds お知らせID
	 * @return エンティティリスト
	 */
	public List<NoticeConditionPrefEntity> getSearchResultByResultId(Long noticeId) {

		// 検索条件生成
		NoticeConditionPrefSearchDto searchDto = new NoticeConditionPrefSearchDto();
		searchDto.setNoticeIds(new Long[] { noticeId }); // お知らせID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(お知らせID)
	 * @param noticeIds お知らせID配列
	 * @return エンティティリスト
	 */
	public List<NoticeConditionPrefEntity> getSearchResultByResultIds(Long[] noticeIds) {

		// 検索条件生成
		NoticeConditionPrefSearchDto searchDto = new NoticeConditionPrefSearchDto();
		searchDto.setNoticeIds(noticeIds); // お知らせID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<NoticeConditionPrefEntity> getSearchResultByCondition(
			NoticeConditionPrefSearchDto searchDto) {

		// 検索
		List<NoticeConditionPrefEntity> entities = noticeConditionPrefDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<NoticeConditionPrefEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("お知らせ(都道府県)検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
