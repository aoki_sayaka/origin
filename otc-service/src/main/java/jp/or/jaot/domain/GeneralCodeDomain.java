package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.GeneralCodeEntity;

/**
 * 汎用コードドメイン
 */
@Component
@Scope("prototype")
public class GeneralCodeDomain extends BaseDomain<GeneralCodeEntity> {

	/**
	 * コンストラクタ
	 */
	public GeneralCodeDomain() {
		setEntity(new GeneralCodeEntity()); // 初期エンティティ
	}

	// TODO
}
