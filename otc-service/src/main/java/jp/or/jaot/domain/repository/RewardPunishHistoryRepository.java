package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.RewardPunishHistoryDao;
import jp.or.jaot.domain.RewardPunishHistoryDomain;
import jp.or.jaot.model.dto.search.RewardPunishHistorySearchDto;
import jp.or.jaot.model.entity.RewardPunishHistoryEntity;

/**
 * 賞罰履歴リポジトリ
 */
@Repository
public class RewardPunishHistoryRepository
		extends BaseRepository<RewardPunishHistoryDomain, RewardPunishHistoryEntity> {

	/** 賞罰履歴DAO */
	private final RewardPunishHistoryDao rewardPunishHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param rewardPunishHistoryDao DAO
	 */
	@Autowired
	public RewardPunishHistoryRepository(RewardPunishHistoryDao rewardPunishHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, rewardPunishHistoryDao);
		this.rewardPunishHistoryDao = rewardPunishHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public RewardPunishHistoryEntity getSearchResult(Long id) {
		RewardPunishHistoryEntity entity = null;
		if ((id == null) || ((entity = rewardPunishHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する賞罰履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<RewardPunishHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		RewardPunishHistorySearchDto searchDto = new RewardPunishHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<RewardPunishHistoryEntity> getSearchResultByCondition(RewardPunishHistorySearchDto searchDto) {

		// 検索
		List<RewardPunishHistoryEntity> entities = rewardPunishHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<RewardPunishHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("賞罰履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
