package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MtdlpCaseReportDao;
import jp.or.jaot.domain.MtdlpCaseReportDomain;
import jp.or.jaot.model.dto.search.MtdlpCaseReportSearchDto;
import jp.or.jaot.model.entity.MtdlpCaseReportEntity;

/**
 * MTDLP事例報告リポジトリ
 */
@Repository
public class MtdlpCaseReportRepository extends BaseRepository<MtdlpCaseReportDomain, MtdlpCaseReportEntity> {

	/** MTDLP事例報告DAO */
	private final MtdlpCaseReportDao mtdlpCaseReportDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param mtdlpCaseDao DAO
	 */
	@Autowired
	public MtdlpCaseReportRepository(MtdlpCaseReportDao mtdlpCaseReportDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, mtdlpCaseReportDao);
		this.mtdlpCaseReportDao = mtdlpCaseReportDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MtdlpCaseReportEntity getSearchResult(Long id) {
		MtdlpCaseReportEntity entity = null;
		if ((id == null) || ((entity = mtdlpCaseReportDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定された会員番号に該当するMTDLP事例報告が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MtdlpCaseReportEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MtdlpCaseReportSearchDto searchDto = new MtdlpCaseReportSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MtdlpCaseReportEntity> getSearchResultByCondition(
			MtdlpCaseReportSearchDto searchDto) {

		// 検索
		List<MtdlpCaseReportEntity> entities = mtdlpCaseReportDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MtdlpCaseReportEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("MTDLP事例報告検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
