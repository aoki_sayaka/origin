package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.RewardPunishHistoryEntity;

/**
 * 賞罰履歴ドメイン
 */
@Component
@Scope("prototype")
public class RewardPunishHistoryDomain extends BaseDomain<RewardPunishHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public RewardPunishHistoryDomain() {
		setEntity(new RewardPunishHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
