package jp.or.jaot.domain;

import java.util.Arrays;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingApplicationEntity;

/**
 * 研修会申込ドメイン
 */
@Component
@Scope("prototype")
public class TrainingApplicationDomain extends BaseDomain<TrainingApplicationEntity> {

	/**
	 * 研修会申込状況の列挙体(コード設計:[30180])
	 */
	public enum STATUS_CD {

		/** 申込前 */
		BEFORE_APPLICATION("00"),
		/** 仮申込 */
		TEMPORARY_APPLICATION("10"),
		/** 本申込 */
		DEFINITIVE_APPLICATION("20"),
		/** 受講許可 */
		PERMISSION_ATTENDANCE("30"),
		/** 受講不許可 */
		DISAPPROVAL_ATTENDANCE("40"),
		/** 受講決定 */
		DECISION_ATTENDANCE("50");

		/** 値 */
		private final String value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private STATUS_CD(final String value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public String getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static STATUS_CD valueOfText(String value) {
			return Arrays.stream(STATUS_CD.values()).filter(V -> V.getValue().equals(value)).findFirst().orElse(null);
		}
	}

	/**
	 * コンストラクタ
	 */
	public TrainingApplicationDomain() {
		setEntity(new TrainingApplicationEntity()); // 初期エンティティ
	}

	// TODO
}
