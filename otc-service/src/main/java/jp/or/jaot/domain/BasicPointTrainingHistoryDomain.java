package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.BasicPointTrainingHistoryEntity;

/**
 * 基礎ポイント研修履歴ドメイン
 */
@Component
@Scope("prototype")
public class BasicPointTrainingHistoryDomain extends BaseDomain<BasicPointTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public BasicPointTrainingHistoryDomain() {
		setEntity(new BasicPointTrainingHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
