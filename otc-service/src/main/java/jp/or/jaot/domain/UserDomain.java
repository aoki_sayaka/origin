package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.UserEntity;

/**
 * 事務局ユーザドメイン
 */
@Component
@Scope("prototype")
public class UserDomain extends BaseDomain<UserEntity> {

	/**
	 * コンストラクタ
	 */
	public UserDomain() {
		setEntity(new UserEntity()); // 初期エンティティ
	}

	// TODO
}
