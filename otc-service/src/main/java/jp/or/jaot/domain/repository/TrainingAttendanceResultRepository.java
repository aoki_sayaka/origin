package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingAttendanceResultDao;
import jp.or.jaot.domain.TrainingAttendanceResultDomain;
import jp.or.jaot.model.dto.search.TrainingAttendanceResultSearchDto;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;

/**
 * 研修会出欠合否リポジトリ
 */
@Repository
public class TrainingAttendanceResultRepository
		extends BaseRepository<TrainingAttendanceResultDomain, TrainingAttendanceResultEntity> {

	/** 研修会出欠合否DAO */
	private final TrainingAttendanceResultDao trainingAttendanceResultDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingAttendanceResultDao DAO
	 */
	@Autowired
	public TrainingAttendanceResultRepository(TrainingAttendanceResultDao trainingAttendanceResultDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, trainingAttendanceResultDao);
		this.trainingAttendanceResultDao = trainingAttendanceResultDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingAttendanceResultEntity getSearchResult(Long id) {
		TrainingAttendanceResultEntity entity = null;
		if ((id == null) || ((entity = trainingAttendanceResultDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修会出欠合否が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNo 研修会番号
	 * @return エンティティリスト
	 */
	public List<TrainingAttendanceResultEntity> getSearchResultByTrainingNo(String trainingNo) {

		// 検索条件生成
		TrainingAttendanceResultSearchDto searchDto = new TrainingAttendanceResultSearchDto();
		searchDto.setTrainingNos(new String[] { trainingNo }); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNos 研修会番号配列
	 * @return エンティティリスト
	 */
	public List<TrainingAttendanceResultEntity> getSearchResultByTrainingNos(String[] trainingNos) {

		// 検索条件生成
		TrainingAttendanceResultSearchDto searchDto = new TrainingAttendanceResultSearchDto();
		searchDto.setTrainingNos(trainingNos); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingAttendanceResultEntity> getSearchResultByCondition(
			TrainingAttendanceResultSearchDto searchDto) {

		// 検索
		List<TrainingAttendanceResultEntity> entities = trainingAttendanceResultDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingAttendanceResultEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修会出欠合否検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
