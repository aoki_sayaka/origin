package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ApplicationProgressDao;
import jp.or.jaot.domain.ApplicationProgressDomain;
import jp.or.jaot.model.dto.search.ApplicationProgressSearchDto;
import jp.or.jaot.model.entity.ApplicationProgressEntity;

/**
 * 申請進捗リポジトリ
 */
@Repository
public class ApplicationProgressRepository
		extends BaseRepository<ApplicationProgressDomain, ApplicationProgressEntity> {

	/** 申請進捗DAO */
	private final ApplicationProgressDao applicationProgressDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param applicationProgressDao DAO
	 */
	@Autowired
	public ApplicationProgressRepository(ApplicationProgressDao applicationProgressDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, applicationProgressDao);
		this.applicationProgressDao = applicationProgressDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ApplicationProgressEntity getSearchResult(Long id) {
		ApplicationProgressEntity entity = null;
		if ((id == null) || ((entity = applicationProgressDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する申請進捗が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(キー)
	 * @param ids ID配列
	 * @return エンティティリスト
	 * @throws SearchBusinessException 検索業務例外
	 */
	public List<ApplicationProgressEntity> getSearchResult(Long[] ids) {
		List<ApplicationProgressEntity> entities = applicationProgressDao.find(ids);
		if (ids.length != entities.stream().filter(E -> E != null).count()) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("ids=%s", StringUtils.join(ids, ","));
			String message = String.format("検索条件に指定されたID配列に該当する申請進捗が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entities;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<ApplicationProgressEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		ApplicationProgressSearchDto searchDto = new ApplicationProgressSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ApplicationProgressEntity> getSearchResultByCondition(ApplicationProgressSearchDto searchDto) {

		// 検索
		List<ApplicationProgressEntity> entities = applicationProgressDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ApplicationProgressEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("申請進捗検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
