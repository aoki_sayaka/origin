package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingOperatorEntity;

/**
 * 研修会運営担当者ドメイン
 */
@Component
@Scope("prototype")
public class TrainingOperatorDomain extends BaseDomain<TrainingOperatorEntity> {

	/**
	 * コンストラクタ
	 */
	public TrainingOperatorDomain() {
		setEntity(new TrainingOperatorEntity()); // 初期エンティティ
	}

	// TODO
}
