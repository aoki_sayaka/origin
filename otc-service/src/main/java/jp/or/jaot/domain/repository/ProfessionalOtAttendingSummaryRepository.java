package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ProfessionalOtAttendingSummaryDao;
import jp.or.jaot.domain.ProfessionalOtAttendingSummaryDomain;
import jp.or.jaot.model.dto.search.ProfessionalOtAttendingSummarySearchDto;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;

/**
 * 専門作業療法士受講履歴リポジトリ
 */
@Repository
public class ProfessionalOtAttendingSummaryRepository
		extends BaseRepository<ProfessionalOtAttendingSummaryDomain, ProfessionalOtAttendingSummaryEntity> {

	/** 専門作業療法士受講履歴DAO */
	private final ProfessionalOtAttendingSummaryDao professionalOtAttendingSummaryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param professionalOtAttendingHistoryDao DAO
	 */
	@Autowired
	public ProfessionalOtAttendingSummaryRepository(
			ProfessionalOtAttendingSummaryDao professionalOtAttendingSummaryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, professionalOtAttendingSummaryDao);
		this.professionalOtAttendingSummaryDao = professionalOtAttendingSummaryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ProfessionalOtAttendingSummaryEntity getSearchResult(Long id) {
		ProfessionalOtAttendingSummaryEntity entity = null;
		if ((id == null) || ((entity = professionalOtAttendingSummaryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する専門作業療法士受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ProfessionalOtAttendingSummaryEntity getSearchResultByMemberNo(Integer memberNo) {
		ProfessionalOtAttendingSummaryEntity entity = null;
		if ((memberNo == null) || ((entity = professionalOtAttendingSummaryDao.findByMemberNo(memberNo)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("memberNo=%s", memberNo);
			String message = String.format("検索条件に指定された会員番号に該当する基礎研修受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtAttendingSummaryEntity> getSearchResultByProfessionalField(Integer memberNo) {

		// 検索条件生成
		ProfessionalOtAttendingSummarySearchDto searchDto = new ProfessionalOtAttendingSummarySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtAttendingSummaryEntity> getSearchResultByCondition(
			ProfessionalOtAttendingSummarySearchDto searchDto) {

		// 検索
		List<ProfessionalOtAttendingSummaryEntity> entities = professionalOtAttendingSummaryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ProfessionalOtAttendingSummaryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("専門作業療法士受講履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
