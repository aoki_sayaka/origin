package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;

/**
 * MTDLP認定履歴ドメイン
 */
@Component
@Scope("prototype")
public class MtdlpQualifyHistoryDomain extends BaseDomain<MtdlpQualifyHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public MtdlpQualifyHistoryDomain() {
		setEntity(new MtdlpQualifyHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
