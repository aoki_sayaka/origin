package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.SocialContributionActualFieldDao;
import jp.or.jaot.domain.SocialContributionActualFieldDomain;
import jp.or.jaot.model.dto.search.SocialContributionActualFieldSearchDto;
import jp.or.jaot.model.entity.SocialContributionActualFieldEntity;

/**
 * 活動実績の領域(後輩育成・社会貢献)リポジトリ
 */
@Repository
public class SocialContributionActualFieldRepository
		extends BaseRepository<SocialContributionActualFieldDomain, SocialContributionActualFieldEntity> {

	/** 活動実績の領域(後輩育成・社会貢献)DAO */
	private final SocialContributionActualFieldDao socialContributionActualFieldDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param socialContributionActualFieldDao DAO
	 */
	@Autowired
	public SocialContributionActualFieldRepository(SocialContributionActualFieldDao socialContributionActualFieldDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, socialContributionActualFieldDao);
		this.socialContributionActualFieldDao = socialContributionActualFieldDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public SocialContributionActualFieldEntity getSearchResult(Long id) {
		SocialContributionActualFieldEntity entity = null;
		if ((id == null) || ((entity = socialContributionActualFieldDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する活動実績の領域(後輩育成・社会貢献)が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(活動実績ID)
	 * @param resultId 活動実績ID
	 * @return エンティティリスト
	 */
	public List<SocialContributionActualFieldEntity> getSearchResultByResultId(Long resultId) {

		// 検索条件生成
		SocialContributionActualFieldSearchDto searchDto = new SocialContributionActualFieldSearchDto();
		searchDto.setResultIds(new Long[] { resultId }); // 活動実績ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(活動実績ID)
	 * @param resultIds 活動実績ID配列
	 * @return エンティティリスト
	 */
	public List<SocialContributionActualFieldEntity> getSearchResultByResultIds(Long[] resultIds) {

		// 検索条件生成
		SocialContributionActualFieldSearchDto searchDto = new SocialContributionActualFieldSearchDto();
		searchDto.setResultIds(resultIds); // 活動実績ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<SocialContributionActualFieldEntity> getSearchResultByCondition(
			SocialContributionActualFieldSearchDto searchDto) {

		// 検索
		List<SocialContributionActualFieldEntity> entities = socialContributionActualFieldDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<SocialContributionActualFieldEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("活動実績の領域(後輩育成・社会貢献)検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
