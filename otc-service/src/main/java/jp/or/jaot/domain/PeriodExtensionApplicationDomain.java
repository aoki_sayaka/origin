package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.PeriodExtensionApplicationEntity;

/**
 * 期間延長申請ドメイン
 */
@Component
@Scope("prototype")
public class PeriodExtensionApplicationDomain extends BaseDomain<PeriodExtensionApplicationEntity> {

	/**
	 * コンストラクタ
	 */
	public PeriodExtensionApplicationDomain() {
		setEntity(new PeriodExtensionApplicationEntity()); // 初期エンティティ
	}
}
