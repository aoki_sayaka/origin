package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.LifelongEducationStatusDao;
import jp.or.jaot.domain.LifelongEducationStatusDomain;
import jp.or.jaot.model.dto.search.LifelongEducationStatusSearchDto;
import jp.or.jaot.model.entity.LifelongEducationStatusEntity;

/**
 * 生涯教育ステータスリポジトリ
 */
@Repository
public class LifelongEducationStatusRepository
		extends BaseRepository<LifelongEducationStatusDomain, LifelongEducationStatusEntity> {

	private final LifelongEducationStatusDao lifelongEducationStatusDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberDao DAO
	 */
	@Autowired
	public LifelongEducationStatusRepository(LifelongEducationStatusDao lifelongEducationStatusDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, lifelongEducationStatusDao);
		this.lifelongEducationStatusDao = lifelongEducationStatusDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public LifelongEducationStatusEntity getSearchResult(Long id) {
		LifelongEducationStatusEntity entity = null;
		if ((id == null) || ((entity = lifelongEducationStatusDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public LifelongEducationStatusEntity getSearchResultByMemberNo(Integer memberNo) {
		LifelongEducationStatusEntity entity = null;
		if ((memberNo == null) || ((entity = lifelongEducationStatusDao.findByMemberNo(memberNo)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("memberNo=%s", memberNo);
			String message = String.format("検索条件に指定された会員番号に該当する会員が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<LifelongEducationStatusEntity> getSearchResultByCondition(LifelongEducationStatusSearchDto searchDto) {

		// 検索
		List<LifelongEducationStatusEntity> entities = lifelongEducationStatusDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<LifelongEducationStatusEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("生涯教育ステータス検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
