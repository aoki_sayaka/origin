package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;

/**
 * 他団体・SIGポイント添付ファイル履歴ドメイン
 */
@Component
@Scope("prototype")
public class OtherOrgPointApplicationAttachmentDomain extends BaseDomain<OtherOrgPointApplicationAttachmentEntity> {

	/**
	 * コンストラクタ
	 */
	public OtherOrgPointApplicationAttachmentDomain() {
		setEntity(new OtherOrgPointApplicationAttachmentEntity()); // 初期エンティティ
	}

	// TODO
}
