package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MtdlpSummaryDao;
import jp.or.jaot.domain.MtdlpSummaryDomain;
import jp.or.jaot.model.dto.search.MtdlpSummarySearchDto;
import jp.or.jaot.model.entity.MtdlpSummaryEntity;

/**
 * MTDLP履歴リポジトリ
 */
@Repository
public class MtdlpSummaryRepository extends BaseRepository<MtdlpSummaryDomain, MtdlpSummaryEntity> {

	/** MTDLP履歴DAO */
	private final MtdlpSummaryDao mtdlpSummaryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param mtdlpSummaryDao DAO
	 */
	@Autowired
	public MtdlpSummaryRepository(MtdlpSummaryDao mtdlpSummaryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, mtdlpSummaryDao);
		this.mtdlpSummaryDao = mtdlpSummaryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MtdlpSummaryEntity getSearchResult(Long id) {
		MtdlpSummaryEntity entity = null;
		if ((id == null) || ((entity = mtdlpSummaryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当するMTDLP履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MtdlpSummaryEntity getSearchResultByMemberNo(Integer memberNo) {
		MtdlpSummaryEntity entity = null;
		if ((memberNo == null) || ((entity = mtdlpSummaryDao.findByMemberNo(memberNo)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("memberNo=%s", memberNo);
			String message = String.format("検索条件に指定された会員番号に該当する会員が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MtdlpSummaryEntity> getSearchResultByCondition(MtdlpSummarySearchDto searchDto) {

		// 検索
		List<MtdlpSummaryEntity> entities = mtdlpSummaryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MtdlpSummaryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("MTDLP履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
