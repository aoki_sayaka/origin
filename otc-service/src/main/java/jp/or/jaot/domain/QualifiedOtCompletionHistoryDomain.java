package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;

/**
 * 認定作業療法士研修修了認定更新履歴ドメイン
 */
@Component
@Scope("prototype")
public class QualifiedOtCompletionHistoryDomain extends BaseDomain<QualifiedOtCompletionHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public QualifiedOtCompletionHistoryDomain() {
		setEntity(new QualifiedOtCompletionHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
