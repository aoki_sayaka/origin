package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.PaotMembershipHistoryEntity;

/**
 * 県士会在籍履歴ドメイン
 */
@Component
@Scope("prototype")
public class PaotMembershipHistoryDomain extends BaseDomain<PaotMembershipHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public PaotMembershipHistoryDomain() {
		setEntity(new PaotMembershipHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
