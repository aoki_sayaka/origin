package jp.or.jaot.domain.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.domain.mail.MemberMailDomain;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberPasswordHistoryEntity;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;

/**
 * 会員メールリポジトリ
 */
@Repository
public class MemberMailRepository {

	/** 会員メールドメイン */
	@Autowired
	private MemberMailDomain memberMailDomain;

	/**
	 * パスワード再発行メール送信
	 * @param memberEntity 会員エンティティ
	 * @param memberPasswordHistoryEntity 会員パスワード履歴エンティティ
	 */
	public void sendPasswordReissueMail(MemberEntity memberEntity,
			MemberPasswordHistoryEntity memberPasswordHistoryEntity) {
		memberMailDomain.sendPasswordReissueMail(memberEntity, memberPasswordHistoryEntity);
	}

	/**
	 * パスワード再設定メール送信
	 * @param memberEntity 会員エンティティ
	 */
	public void sendPasswordResettingMail(MemberEntity memberEntity) {
		memberMailDomain.sendPasswordResettingMail(memberEntity);
	}

	/**
	 * メールアドレス変更メール送信
	 * @param memberEntity 会員エンティティ
	 * @param memberEmailHistoryEntity 会員メールアドレス履歴エンティティ
	 */
	public void sendEmailConfirmMail(MemberEntity memberEntity,
										MemberEmailHistoryEntity memberEmailHistoryEntity) {
		memberMailDomain.sendEmailConfirmMail(memberEntity, memberEmailHistoryEntity);
	}
}
