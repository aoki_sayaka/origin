package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.RegionRelatedMiddleClassificationEntity;

/**
 * 領域関連中分類ドメイン
 */
@Component
@Scope("prototype")
public class RegionRelatedMiddleClassificationDomain extends BaseDomain<RegionRelatedMiddleClassificationEntity> {

	/**
	 * コンストラクタ
	 */
	public RegionRelatedMiddleClassificationDomain() {
		setEntity(new RegionRelatedMiddleClassificationEntity()); // 初期エンティティ
	}

	// TODO
}
