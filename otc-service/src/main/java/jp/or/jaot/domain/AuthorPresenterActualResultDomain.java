package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.AuthorPresenterActualResultEntity;

/**
 * 活動実績(発表・著作等)ドメイン
 */
@Component
@Scope("prototype")
public class AuthorPresenterActualResultDomain extends BaseDomain<AuthorPresenterActualResultEntity> {

	/**
	 * コンストラクタ
	 */
	public AuthorPresenterActualResultDomain() {
		setEntity(new AuthorPresenterActualResultEntity()); // 初期エンティティ
	}

	// TODO
}
