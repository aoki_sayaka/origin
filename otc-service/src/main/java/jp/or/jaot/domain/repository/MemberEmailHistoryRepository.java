package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberEmailHistoryDao;
import jp.or.jaot.domain.MemberEmailHistoryDomain;
import jp.or.jaot.model.dto.search.MemberEmailHistorySearchDto;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;

/**
 * 会員メールアドレス履歴リポジトリ
 */
@Repository
public class MemberEmailHistoryRepository
		extends BaseRepository<MemberEmailHistoryDomain, MemberEmailHistoryEntity> {

	/** 会員メールアドレス履歴DAO */
	private final MemberEmailHistoryDao memberEmailHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberEmailHistoryDao DAO
	 */
	@Autowired
	public MemberEmailHistoryRepository(MemberEmailHistoryDao memberEmailHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberEmailHistoryDao);
		this.memberEmailHistoryDao = memberEmailHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberEmailHistoryEntity getSearchResult(Long id) {
		MemberEmailHistoryEntity entity = null;
		if ((id == null) || ((entity = memberEmailHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員メールアドレス履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MemberEmailHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MemberEmailHistorySearchDto searchDto = new MemberEmailHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberEmailHistoryEntity> getSearchResultByCondition(MemberEmailHistorySearchDto searchDto) {

		// 検索
		List<MemberEmailHistoryEntity> entities = memberEmailHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberEmailHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員メールアドレス履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
