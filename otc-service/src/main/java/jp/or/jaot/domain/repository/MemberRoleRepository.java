package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberRoleDao;
import jp.or.jaot.domain.MemberRoleDomain;
import jp.or.jaot.model.dto.search.MemberRoleSearchDto;
import jp.or.jaot.model.entity.MemberRoleEntity;

/**
 * 会員ロールリポジトリ
 */
@Repository
public class MemberRoleRepository extends BaseRepository<MemberRoleDomain, MemberRoleEntity> {

	/** 会員ロールDAO */
	private final MemberRoleDao memberRoleDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberRoleDao DAO
	 */
	@Autowired
	public MemberRoleRepository(MemberRoleDao memberRoleDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberRoleDao);
		this.memberRoleDao = memberRoleDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberRoleEntity getSearchResult(Long id) {
		MemberRoleEntity entity = null;
		if ((id == null) || ((entity = memberRoleDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員ロールが存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MemberRoleEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MemberRoleSearchDto searchDto = new MemberRoleSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberRoleEntity> getSearchResultByCondition(MemberRoleSearchDto searchDto) {

		// 検索
		List<MemberRoleEntity> entities = memberRoleDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberRoleEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員ロール検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
