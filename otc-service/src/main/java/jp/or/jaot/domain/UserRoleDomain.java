package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.UserRoleEntity;

/**
 * 事務局ロールドメイン
 */
@Component
@Scope("prototype")
public class UserRoleDomain extends BaseDomain<UserRoleEntity> {

	/**
	 * コンストラクタ
	 */
	public UserRoleDomain() {
		setEntity(new UserRoleEntity()); // 初期エンティティ
	}

	// TODO
}
