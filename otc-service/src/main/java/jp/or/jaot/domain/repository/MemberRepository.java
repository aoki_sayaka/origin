package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.model.BaseEntity.ORDER_ASC;
import jp.or.jaot.core.model.BaseSearchDto.SearchOrder;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberDao;
import jp.or.jaot.datasource.MemberRevokedAuthorityDao;
import jp.or.jaot.datasource.MemberRoleDao;
import jp.or.jaot.domain.MemberDomain;
import jp.or.jaot.model.dto.search.MemberRevokedAuthoritySearchDto;
import jp.or.jaot.model.dto.search.MemberRoleSearchDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.entity.GeneralCodeId;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberRevokedAuthorityEntity;

/**
 * 会員リポジトリ
 */
@Repository
public class MemberRepository extends BaseRepository<MemberDomain, MemberEntity> {

	/** 会員DAO */
	private final MemberDao memberDao;

	/** 会員ロールDAO */
	private final MemberRoleDao memberRoleDao;

	/** 会員権限DAO */
	private final MemberRevokedAuthorityDao memberRevokedAuthorityDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberDao DAO
	 * @param memberRoleDao 会員ロールDAO
	 * @param memberRevokedAuthorityDao 会員権限DAO
	 */
	@Autowired
	public MemberRepository(MemberDao memberDao, MemberRoleDao memberRoleDao,
			MemberRevokedAuthorityDao memberRevokedAuthorityDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberDao);
		this.memberDao = memberDao;
		this.memberRoleDao = memberRoleDao;
		this.memberRevokedAuthorityDao = memberRevokedAuthorityDao;
	}

	/**
	 * 全件取得
	 * @return エンティティリスト
	 */
	public List<MemberEntity> getAllResult() {
		return memberDao.findAll();
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberEntity getSearchResult(Long id) {
		MemberEntity entity = null;
		if ((id == null) || ((entity = memberDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberEntity getSearchResultByMemberNo(Integer memberNo) {
		MemberEntity entity = null;
		if ((memberNo == null) || ((entity = memberDao.findByMemberNo(memberNo)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("memberNo=%s", memberNo);
			String message = String.format("検索条件に指定された会員番号に該当する会員が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNos 会員番号配列
	 * @return エンティティリスト
	 */
	public List<MemberEntity> getSearchResultByMemberNos(Integer[] memberNos) {

		// 検索条件生成
		MemberSearchDto searchDto = new MemberSearchDto();
		searchDto.setMemberNos(memberNos); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberEntity> getSearchResultByCondition(MemberSearchDto searchDto) {

		// ソート順序
		searchDto.getOrders().add(new SearchOrder(ORDER_ASC.DESC, "createDatetime")); // 降順:作成日時
		searchDto.getOrders().add(new SearchOrder(ORDER_ASC.DESC, "updateDatetime")); // 降順:更新日時

		// 検索
		List<MemberEntity> entities = memberDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	/**
	 * 状態設定
	 * @param id ID
	 * @param updateUser 更新者
	 * @param enabled 状態(true=使用可能, false=使用不可)
	 */
	public void setEnable(Long id, String updateUser, boolean enabled) {
		memberDao.setEnableEntity(id, updateUser, enabled);
	}

	/**
	 * 利用可能状態取得(パス/キー値)
	 * @param memberNo 会員番号
	 * @param pathOrKey パス/キー値
	 * @return 利用可能=true, 利用不可=false
	 */
	public boolean isAvailablePathOrKey(Integer memberNo, String pathOrKey) {

		// ログ出力
		logger.debug("利用可能状態取得 : 会員番号={}, パス/キー値={}", memberNo, pathOrKey);

		try {
			// 汎用コードID配列取得
			MemberRoleSearchDto roleSearchDto = new MemberRoleSearchDto();
			roleSearchDto.setMemberNo(memberNo); // 会員番号
			GeneralCodeId[] generalCodeIds = memberRoleDao.findByCondition(roleSearchDto).stream()
					.map(E -> new GeneralCodeId(E.getRoleCodeId(), E.getRoleCode())).toArray(GeneralCodeId[]::new);
			if (ArrayUtils.isEmpty(generalCodeIds)) {
				logger.debug("ロール指定 : なし");
				return true;
			}

			// 権限取得
			MemberRevokedAuthoritySearchDto authoritySearchDto = new MemberRevokedAuthoritySearchDto();
			authoritySearchDto.setGeneralCodeIds(generalCodeIds); // 汎用コードID配列
			List<MemberRevokedAuthorityEntity> authorityEntities = memberRevokedAuthorityDao
					.findByCondition(authoritySearchDto);
			if (CollectionUtils.isEmpty(authorityEntities)) {
				logger.debug("権限指定 : なし");
				return true;
			}

			// 権限判定(ブラックリスト形式(正規表現))
			for (MemberRevokedAuthorityEntity authorityEntity : authorityEntities) {
				if (pathOrKey.matches(authorityEntity.getPathOrKey())) {
					logger.debug("利用不可 : ID={}, コード識別ID={}, コード値={}, パス/キー値={}", authorityEntity.getId(),
							authorityEntity.getCodeId(), authorityEntity.getCode(), authorityEntity.getPathOrKey());
					return false; // 利用不可
				}
			}
		} catch (Exception e) {
			logger.debug(e.getMessage()); // ログを出力して処理継続
		}

		return true;
	}

	/**
	 * 検索結果取得(会員コード)
	 * @param searchDto 検索DTO
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberEntity getSearchResultByMemberCd(MemberSearchDto searchDto) {
		MemberEntity entity = null;
		if ((entity = memberDao.findByMemberCd(searchDto)) == null) {
			entity = null;
		}

		// 検索結果
		logger.debug("会員検索 : 件数={}, 条件={}", searchDto);

		return entity;
	}

	// TODO
}
