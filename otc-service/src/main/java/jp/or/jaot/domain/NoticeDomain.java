package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.NoticeEntity;

/**
 * お知らせドメイン
 */
@Component
@Scope("prototype")
public class NoticeDomain extends BaseDomain<NoticeEntity> {

	/**
	 * コンストラクタ
	 */
	public NoticeDomain() {
		setEntity(new NoticeEntity()); // 初期エンティティ
	}

	// TODO
}
