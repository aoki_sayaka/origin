package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.FacilityAdminDao;
import jp.or.jaot.datasource.FacilityPortalDao;
import jp.or.jaot.domain.FacilityDomain;
import jp.or.jaot.model.dto.search.FacilityAdminSearchDto;
import jp.or.jaot.model.dto.search.FacilityPortalSearchDto;
import jp.or.jaot.model.entity.FacilityEntity;

/**
 * 施設養成校リポジトリ
 */
@Repository
public class FacilityRepository extends BaseRepository<FacilityDomain, FacilityEntity> {

	/** 施設養成校DAO(事務局サイト) */
	private final FacilityAdminDao facilityAdminDao;

	/** 施設養成校DAO(会員ポータル) */
	private final FacilityPortalDao facilityPortalDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param facilityAdminDao DAO(事務局サイト)
	 * @param facilityPortalDao DAO(会員ポータル)
	 */
	@Autowired
	public FacilityRepository(FacilityAdminDao facilityAdminDao, FacilityPortalDao facilityPortalDao) {
		super(ERROR_PLACE.LOGIC_COMMON, facilityPortalDao);
		this.facilityAdminDao = facilityAdminDao;
		this.facilityPortalDao = facilityPortalDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public FacilityEntity getSearchResult(Long id) {
		FacilityEntity entity = null;
		if ((id == null) || ((entity = facilityAdminDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する施設養成校が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<FacilityEntity> getSearchResultByCondition(FacilityAdminSearchDto searchDto) {

		// 検索
		List<FacilityEntity> entities = facilityAdminDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<FacilityEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("施設養成校検索(事務局サイト) : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<FacilityEntity> getSearchResultByCondition(FacilityPortalSearchDto searchDto) {

		// 検索
		List<FacilityEntity> entities = facilityPortalDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<FacilityEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("施設養成校検索(会員ポータル) : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
