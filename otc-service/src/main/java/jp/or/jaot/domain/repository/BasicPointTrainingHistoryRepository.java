package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.BasicPointTrainingHistoryDao;
import jp.or.jaot.domain.BasicPointTrainingHistoryDomain;
import jp.or.jaot.model.dto.search.BasicPointTrainingHistorySearchDto;
import jp.or.jaot.model.entity.BasicPointTrainingHistoryEntity;

/**
 * 基礎ポイント研修履歴リポジトリ
 */
@Repository
public class BasicPointTrainingHistoryRepository
		extends BaseRepository<BasicPointTrainingHistoryDomain, BasicPointTrainingHistoryEntity> {

	/** 基礎ポイント研修履歴DAO */
	private final BasicPointTrainingHistoryDao basicPointTrainingHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicPointTrainingHistoryDao DAO
	 */
	@Autowired
	public BasicPointTrainingHistoryRepository(BasicPointTrainingHistoryDao basicPointTrainingHistoryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, basicPointTrainingHistoryDao);
		this.basicPointTrainingHistoryDao = basicPointTrainingHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public BasicPointTrainingHistoryEntity getSearchResult(Long id) {
		BasicPointTrainingHistoryEntity entity = null;
		if ((id == null) || ((entity = basicPointTrainingHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する基礎ポイント研修履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<BasicPointTrainingHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		BasicPointTrainingHistorySearchDto searchDto = new BasicPointTrainingHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<BasicPointTrainingHistoryEntity> getSearchResultByCondition(
			BasicPointTrainingHistorySearchDto searchDto) {

		// 検索
		List<BasicPointTrainingHistoryEntity> entities = basicPointTrainingHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<BasicPointTrainingHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("基礎ポイント研修履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
