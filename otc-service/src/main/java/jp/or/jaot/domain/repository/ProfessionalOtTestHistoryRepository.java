package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ProfessionalOtTestHistoryDao;
import jp.or.jaot.domain.ProfessionalOtTestHistoryDomain;
import jp.or.jaot.model.dto.search.ProfessionalOtTestHistorySearchDto;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;

/**
 * 専門作業療法士試験結果履歴リポジトリ
 */
@Repository
public class ProfessionalOtTestHistoryRepository
		extends BaseRepository<ProfessionalOtTestHistoryDomain, ProfessionalOtTestHistoryEntity> {

	/** 専門作業療法士試験結果履歴DAO */
	private final ProfessionalOtTestHistoryDao professionalOtTestHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtQualifyHistoryDao DAO
	 */
	@Autowired
	public ProfessionalOtTestHistoryRepository(ProfessionalOtTestHistoryDao professionalOtTestHistoryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, professionalOtTestHistoryDao);
		this.professionalOtTestHistoryDao = professionalOtTestHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ProfessionalOtTestHistoryEntity getSearchResult(Long id) {
		ProfessionalOtTestHistoryEntity entity = null;
		if ((id == null) || ((entity = professionalOtTestHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する専門作業療法士試験結果履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtTestHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		ProfessionalOtTestHistorySearchDto searchDto = new ProfessionalOtTestHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtTestHistoryEntity> getSearchResultByCondition(
			ProfessionalOtTestHistorySearchDto searchDto) {

		// 検索
		List<ProfessionalOtTestHistoryEntity> entities = professionalOtTestHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ProfessionalOtTestHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("専門作業療法士試験結果履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
