package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberQualificationDao;
import jp.or.jaot.domain.MemberQualificationDomain;
import jp.or.jaot.model.dto.search.MemberQualificationSearchDto;
import jp.or.jaot.model.entity.MemberQualificationEntity;

/**
 * 会員関連資格情報リポジトリ
 */
@Repository
public class MemberQualificationRepository
		extends BaseRepository<MemberQualificationDomain, MemberQualificationEntity> {

	/** 会員関連資格情報DAO */
	private final MemberQualificationDao memberQualificationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberQualificationDao DAO
	 */
	@Autowired
	public MemberQualificationRepository(MemberQualificationDao memberQualificationDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberQualificationDao);
		this.memberQualificationDao = memberQualificationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberQualificationEntity getSearchResult(Long id) {
		MemberQualificationEntity entity = null;
		if ((id == null) || ((entity = memberQualificationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する会員関連資格情報が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MemberQualificationEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MemberQualificationSearchDto searchDto = new MemberQualificationSearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberQualificationEntity> getSearchResultByCondition(MemberQualificationSearchDto searchDto) {

		// 検索
		List<MemberQualificationEntity> entities = memberQualificationDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberQualificationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("会員関連資格情報検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
