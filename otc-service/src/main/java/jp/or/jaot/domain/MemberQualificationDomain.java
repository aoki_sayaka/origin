package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberQualificationEntity;

/**
 * 会員関連資格情報ドメイン
 */
@Component
@Scope("prototype")
public class MemberQualificationDomain extends BaseDomain<MemberQualificationEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberQualificationDomain() {
		setEntity(new MemberQualificationEntity()); // 初期エンティティ
	}

	// TODO
}
