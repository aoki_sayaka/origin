package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.QualifiedOtAttendingSummaryDao;
import jp.or.jaot.domain.QualifiedOtAttendingSummaryDomain;
import jp.or.jaot.model.dto.search.QualifiedOtAttendingSummarySearchDto;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;

/**
 * 認定作業療法士研修履歴リポジトリ
 */
@Repository
public class QualifiedOtAttendingSummaryRepository
		extends BaseRepository<QualifiedOtAttendingSummaryDomain, QualifiedOtAttendingSummaryEntity> {

	/** 認定作業療法士研修履歴DAO */
	private final QualifiedOtAttendingSummaryDao qualifiedOtAttendingSummaryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param qualifiedOtAttendingSummaryDao DAO
	 */
	@Autowired
	public QualifiedOtAttendingSummaryRepository(QualifiedOtAttendingSummaryDao qualifiedOtAttendingSummaryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, qualifiedOtAttendingSummaryDao);
		this.qualifiedOtAttendingSummaryDao = qualifiedOtAttendingSummaryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public QualifiedOtAttendingSummaryEntity getSearchResult(Long id) {
		QualifiedOtAttendingSummaryEntity entity = null;
		if ((id == null) || ((entity = qualifiedOtAttendingSummaryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する認定作業療法士研修履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public QualifiedOtAttendingSummaryEntity getSearchResultByMemberNo(Integer memberNo) {
		QualifiedOtAttendingSummaryEntity entity = null;
		if ((memberNo == null) || ((entity = qualifiedOtAttendingSummaryDao.findByMemberNo(memberNo)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("memberNo=%s", memberNo);
			String message = String.format("検索条件に指定された会員番号に該当する会員が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<QualifiedOtAttendingSummaryEntity> getSearchResultByCondition(
			QualifiedOtAttendingSummarySearchDto searchDto) {

		// 検索
		List<QualifiedOtAttendingSummaryEntity> entities = qualifiedOtAttendingSummaryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<QualifiedOtAttendingSummaryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("認定作業療法士研修履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
