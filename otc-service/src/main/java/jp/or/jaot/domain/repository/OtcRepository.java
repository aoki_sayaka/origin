package jp.or.jaot.domain.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.domain.mail.OtcMailDomain;

/**
 * リポジトリ
 * @version 疎通確認用
 */
@Repository
public class OtcRepository {

	/** ロギングクラス */
	private static final Logger logger = LoggerFactory.getLogger(OtcRepository.class);

	/** メールドメイン */
	@Autowired
	private OtcMailDomain otcMailDomain;

	/**
	 * 利用者検索
	 */
	public void getSearchResult() {
		logger.debug("getSearchResult()");
		return;
	}

	/**
	 * メール送信
	 */
	public void sendMail() {
		otcMailDomain.sendMail();
	}
}
