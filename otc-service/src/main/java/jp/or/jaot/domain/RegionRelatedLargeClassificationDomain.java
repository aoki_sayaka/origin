package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.RegionRelatedLargeClassificationEntity;

/**
 * 領域関連大分類ドメイン
 */
@Component
@Scope("prototype")
public class RegionRelatedLargeClassificationDomain extends BaseDomain<RegionRelatedLargeClassificationEntity> {

	/**
	 * コンストラクタ
	 */
	public RegionRelatedLargeClassificationDomain() {
		setEntity(new RegionRelatedLargeClassificationEntity()); // 初期エンティティ
	}

	// TODO
}
