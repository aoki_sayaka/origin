package jp.or.jaot.domain;

import java.util.Arrays;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberFacilityCategoryEntity;

/**
 * 会員勤務先業務分類ドメイン
 */
@Component
@Scope("prototype")
public class MemberFacilityCategoryDomain extends BaseDomain<MemberFacilityCategoryEntity> {

	/**
	 * 会員勤務先業務分類の分類区分の列挙体
	 */
	public enum MEMBER_FACILITY_CATEGORY_DIVISION {

		/** 大分類 */
		LARGE(1),
		/** 中分類（主） */
		MIDDLE(2),
		/** 中分類（従） */
		MIDDLE_SUB(3),
		/** 小分類 */
		SMALL(4);

		/** 値 */
		private final Integer value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private MEMBER_FACILITY_CATEGORY_DIVISION(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static MEMBER_FACILITY_CATEGORY_DIVISION valueOf(Integer value) {
			return Arrays.stream(MEMBER_FACILITY_CATEGORY_DIVISION.values()).filter(V -> V.getValue().equals(value))
					.findFirst()
					.orElse(null);
		}

	}

	/**
	 * 主従区分の列挙体
	 */
	public enum MEMBER_FACILITY_CATEGORY_PRIMARY_DIVISION {

		/** 主 */
		MAIN(1),
		/** 従 */
		SUB(2);

		/** 値 */
		private final Integer value;

		/**
		 * コンストラクタ
		 * @param value 値
		 */
		private MEMBER_FACILITY_CATEGORY_PRIMARY_DIVISION(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}
	}

	/**
	 * コンストラクタ
	 */
	public MemberFacilityCategoryDomain() {
		setEntity(new MemberFacilityCategoryEntity()); // 初期エンティティ
	}


}
