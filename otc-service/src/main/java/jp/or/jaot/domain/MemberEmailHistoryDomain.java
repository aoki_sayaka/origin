package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;

/**
 * 会員メールアドレス履歴ドメイン
 */
@Component
@Scope("prototype")
public class MemberEmailHistoryDomain extends BaseDomain<MemberEmailHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberEmailHistoryDomain() {
		setEntity(new MemberEmailHistoryEntity()); // 初期エンティティ
	}
}
