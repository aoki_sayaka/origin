package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberPasswordHistoryEntity;

/**
 * 会員パスワード履歴ドメイン
 */
@Component
@Scope("prototype")
public class MemberPasswordHistoryDomain extends BaseDomain<MemberPasswordHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberPasswordHistoryDomain() {
		setEntity(new MemberPasswordHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
