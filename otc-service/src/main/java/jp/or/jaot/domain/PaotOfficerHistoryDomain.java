package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.PaotOfficerHistoryEntity;

/**
 * 県士会役員履歴ドメイン
 */
@Component
@Scope("prototype")
public class PaotOfficerHistoryDomain extends BaseDomain<PaotOfficerHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public PaotOfficerHistoryDomain() {
		setEntity(new PaotOfficerHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
