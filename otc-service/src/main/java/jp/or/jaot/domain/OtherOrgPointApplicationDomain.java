package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;

/**
 * 他団体・SIGポイント申請履歴ドメイン
 */
@Component
@Scope("prototype")
public class OtherOrgPointApplicationDomain extends BaseDomain<OtherOrgPointApplicationEntity> {

	/**
	 * コンストラクタ
	 */
	public OtherOrgPointApplicationDomain() {
		setEntity(new OtherOrgPointApplicationEntity()); // 初期エンティティ
	}

	// TODO
}
