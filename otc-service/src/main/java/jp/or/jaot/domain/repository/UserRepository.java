package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.UserDao;
import jp.or.jaot.datasource.UserRevokedAuthorityDao;
import jp.or.jaot.datasource.UserRoleDao;
import jp.or.jaot.domain.UserDomain;
import jp.or.jaot.model.dto.search.UserRevokedAuthoritySearchDto;
import jp.or.jaot.model.dto.search.UserRoleSearchDto;
import jp.or.jaot.model.dto.search.UserSearchDto;
import jp.or.jaot.model.entity.GeneralCodeId;
import jp.or.jaot.model.entity.UserEntity;
import jp.or.jaot.model.entity.UserRevokedAuthorityEntity;

/**
 * 事務局ユーザリポジトリ
 */
@Repository
public class UserRepository extends BaseRepository<UserDomain, UserEntity> {

	/** 事務局ユーザDAO */
	private final UserDao userDao;

	/** 事務局ロールDAO */
	private final UserRoleDao userRoleDao;

	/** 事務局権限DAO */
	private final UserRevokedAuthorityDao userRevokedAuthorityDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param userDao 事務局ユーザDAO
	 * @param userRoleDao 事務局ロールDAO
	 * @param userRevokedAuthorityDao 事務局権限DAO
	 */
	@Autowired
	public UserRepository(UserDao userDao, UserRoleDao userRoleDao, UserRevokedAuthorityDao userRevokedAuthorityDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, userDao);
		this.userDao = userDao;
		this.userRoleDao = userRoleDao;
		this.userRevokedAuthorityDao = userRevokedAuthorityDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public UserEntity getSearchResult(Long id) {
		UserEntity entity = null;
		if ((id == null) || ((entity = userDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する事務局ユーザが存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(ログインID)
	 * @param loginId ログインID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public UserEntity getSearchResultByLoginId(String loginId) {

		// 検索条件
		UserSearchDto searchDto = new UserSearchDto();
		searchDto.setLoginId(loginId); // ログインID

		// 検索
		List<UserEntity> entities = userDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("loginId=%s", loginId);
			String message = String.format("検索条件に指定されたログインIDに該当する事務局ユーザが存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		} else if (entities.size() >= 2) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("loginId=%s, size=%d", loginId, entities.size());
			String message = String.format("検索条件に指定されたログインIDに該当する事務局ユーザが複数存在します");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}

		return entities.get(0); // 常に先頭要素
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<UserEntity> getSearchResultByCondition(UserSearchDto searchDto) {

		// 検索
		List<UserEntity> entities = userDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<UserEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("事務局ユーザ検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	/**
	 * 利用可能状態取得(パス/キー値)
	 * @param id ID
	 * @param pathOrKey パス/キー値
	 * @return 利用可能=true, 利用不可=false
	 */
	public boolean isAvailablePathOrKey(Long id, String pathOrKey) {

		// ログ出力
		logger.debug("利用可能状態取得 : 事務局ユーザID={}, パス/キー値={}", id, pathOrKey);

		try {
			// 汎用コードID配列取得
			UserRoleSearchDto roleSearchDto = new UserRoleSearchDto();
			roleSearchDto.setUserId(id); // 事務局ユーザID
			GeneralCodeId[] generalCodeIds = userRoleDao.findByCondition(roleSearchDto).stream()
					.map(E -> new GeneralCodeId(E.getRoleCodeId(), E.getRoleCode())).toArray(GeneralCodeId[]::new);
			if (ArrayUtils.isEmpty(generalCodeIds)) {
				logger.debug("ロール指定 : なし");
				return true;
			}

			// 権限取得
			UserRevokedAuthoritySearchDto authoritySearchDto = new UserRevokedAuthoritySearchDto();
			authoritySearchDto.setGeneralCodeIds(generalCodeIds); // 汎用コードID配列
			List<UserRevokedAuthorityEntity> authorityEntities = userRevokedAuthorityDao
					.findByCondition(authoritySearchDto);
			if (CollectionUtils.isEmpty(authorityEntities)) {
				logger.debug("権限指定 : なし");
				return true;
			}

			// 権限判定(ブラックリスト形式(正規表現))
			for (UserRevokedAuthorityEntity authorityEntity : authorityEntities) {
				if (pathOrKey.matches(authorityEntity.getPathOrKey())) {
					logger.debug("利用不可 : ID={}, コード識別ID={}, コード値={}, パス/キー値={}", authorityEntity.getId(),
							authorityEntity.getCodeId(), authorityEntity.getCode(), authorityEntity.getPathOrKey());
					return false; // 利用不可
				}
			}
		} catch (Exception e) {
			logger.debug(e.getMessage()); // ログを出力して処理継続
		}

		return true;
	}

	// TODO
}
