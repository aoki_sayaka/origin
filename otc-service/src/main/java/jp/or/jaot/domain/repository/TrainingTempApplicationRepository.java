package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingTempApplicationDao;
import jp.or.jaot.domain.TrainingTempApplicationDomain;
import jp.or.jaot.model.dto.search.TrainingTempApplicationSearchDto;
import jp.or.jaot.model.entity.TrainingTempApplicationEntity;

/**
 * 研修会仮申込リポジトリ
 */
@Repository
public class TrainingTempApplicationRepository
		extends BaseRepository<TrainingTempApplicationDomain, TrainingTempApplicationEntity> {

	/** 研修会仮申込DAO */
	private final TrainingTempApplicationDao trainingTempApplicationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingTempApplicationDao DAO
	 */
	@Autowired
	public TrainingTempApplicationRepository(TrainingTempApplicationDao trainingTempApplicationDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, trainingTempApplicationDao);
		this.trainingTempApplicationDao = trainingTempApplicationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingTempApplicationEntity getSearchResult(Long id) {
		TrainingTempApplicationEntity entity = null;
		if ((id == null) || ((entity = trainingTempApplicationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修会仮申込が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(キー)
	 * @param ids ID配列
	 * @return エンティティリスト
	 * @throws SearchBusinessException 検索業務例外
	 */
	public List<TrainingTempApplicationEntity> getSearchResult(Long[] ids) {
		List<TrainingTempApplicationEntity> entities = trainingTempApplicationDao.find(ids);
		if (ids.length != entities.stream().filter(E -> E != null).count()) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("ids=%s", StringUtils.join(ids, ","));
			String message = String.format("検索条件に指定されたID配列に該当する研修会仮申込が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entities;
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNo 研修会番号
	 * @return エンティティリスト
	 */
	public List<TrainingTempApplicationEntity> getSearchResultByTrainingNo(String trainingNo) {

		// 検索条件生成
		TrainingTempApplicationSearchDto searchDto = new TrainingTempApplicationSearchDto();
		searchDto.setTrainingNos(new String[] { trainingNo }); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNos 研修会番号配列
	 * @return エンティティリスト
	 */
	public List<TrainingTempApplicationEntity> getSearchResultByTrainingNos(String[] trainingNos) {

		// 検索条件生成
		TrainingTempApplicationSearchDto searchDto = new TrainingTempApplicationSearchDto();
		searchDto.setTrainingNos(trainingNos); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingTempApplicationEntity> getSearchResultByCondition(
			TrainingTempApplicationSearchDto searchDto) {

		// 検索
		List<TrainingTempApplicationEntity> entities = trainingTempApplicationDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingTempApplicationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修会仮申込検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
