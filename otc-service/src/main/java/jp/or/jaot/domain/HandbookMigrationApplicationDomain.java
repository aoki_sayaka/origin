package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.HandbookMigrationApplicationEntity;

/**
 * 基礎研修履歴ドメイン
 */
@Component
@Scope("prototype")
public class HandbookMigrationApplicationDomain extends BaseDomain<HandbookMigrationApplicationEntity> {

	/**
	 * コンストラクタ
	 */
	public HandbookMigrationApplicationDomain() {
		setEntity(new HandbookMigrationApplicationEntity()); // 初期エンティティ
	}

	// TODO
}
