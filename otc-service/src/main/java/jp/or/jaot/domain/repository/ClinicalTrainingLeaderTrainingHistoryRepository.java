package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ClinicalTrainingLeaderTrainingHistoryDao;
import jp.or.jaot.domain.ClinicalTrainingLeaderTrainingHistoryDomain;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderTrainingHistorySearchDto;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;

/**
 * 臨床実習指導者研修受講履歴リポジトリ
 */
@Repository
public class ClinicalTrainingLeaderTrainingHistoryRepository extends
		BaseRepository<ClinicalTrainingLeaderTrainingHistoryDomain, ClinicalTrainingLeaderTrainingHistoryEntity> {

	/** 臨床実習指導者研修受講履歴DAO */
	private final ClinicalTrainingLeaderTrainingHistoryDao clinicalTrainingLeaderTrainingHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param clinicalTrainingLeaderTrainingHistoryDao DAO
	 */
	@Autowired
	public ClinicalTrainingLeaderTrainingHistoryRepository(
			ClinicalTrainingLeaderTrainingHistoryDao clinicalTrainingLeaderTrainingHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, clinicalTrainingLeaderTrainingHistoryDao);
		this.clinicalTrainingLeaderTrainingHistoryDao = clinicalTrainingLeaderTrainingHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ClinicalTrainingLeaderTrainingHistoryEntity getSearchResult(Long id) {
		ClinicalTrainingLeaderTrainingHistoryEntity entity = null;
		if ((id == null) || ((entity = clinicalTrainingLeaderTrainingHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定された会員番号に該当する臨床実習指導者研修受講履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<ClinicalTrainingLeaderTrainingHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		ClinicalTrainingLeaderTrainingHistorySearchDto searchDto = new ClinicalTrainingLeaderTrainingHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ClinicalTrainingLeaderTrainingHistoryEntity> getSearchResultByCondition(
			ClinicalTrainingLeaderTrainingHistorySearchDto searchDto) {

		// 検索
		List<ClinicalTrainingLeaderTrainingHistoryEntity> entities = clinicalTrainingLeaderTrainingHistoryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ClinicalTrainingLeaderTrainingHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("臨床実習指導者研修受講履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
