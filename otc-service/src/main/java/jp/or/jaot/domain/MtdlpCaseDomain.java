package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MtdlpCaseEntity;

/**
 * MTDLP事例ドメイン
 */
@Component
@Scope("prototype")
public class MtdlpCaseDomain extends BaseDomain<MtdlpCaseEntity> {

	/**
	 * コンストラクタ
	 */
	public MtdlpCaseDomain() {
		setEntity(new MtdlpCaseEntity()); // 初期エンティティ
	}

	// TODO
}
