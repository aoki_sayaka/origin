package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingOrganizerDao;
import jp.or.jaot.domain.TrainingOrganizerDomain;
import jp.or.jaot.model.dto.search.TrainingOrganizerSearchDto;
import jp.or.jaot.model.entity.TrainingOrganizerEntity;

/**
 * 研修会主催者リポジトリ
 */
@Repository
public class TrainingOrganizerRepository extends BaseRepository<TrainingOrganizerDomain, TrainingOrganizerEntity> {

	/** 研修会主催者DAO */
	private final TrainingOrganizerDao trainingOrganizerDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingOrganizerDao DAO
	 */
	@Autowired
	public TrainingOrganizerRepository(TrainingOrganizerDao trainingOrganizerDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, trainingOrganizerDao);
		this.trainingOrganizerDao = trainingOrganizerDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingOrganizerEntity getSearchResult(Long id) {
		TrainingOrganizerEntity entity = null;
		if ((id == null) || ((entity = trainingOrganizerDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修会主催者が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNo 研修会番号
	 * @return エンティティリスト
	 */
	public List<TrainingOrganizerEntity> getSearchResultByTrainingNo(String trainingNo) {

		// 検索条件生成
		TrainingOrganizerSearchDto searchDto = new TrainingOrganizerSearchDto();
		searchDto.setTrainingNos(new String[] { trainingNo }); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNos 研修会番号配列
	 * @return エンティティリスト
	 */
	public List<TrainingOrganizerEntity> getSearchResultByTrainingNos(String[] trainingNos) {

		// 検索条件生成
		TrainingOrganizerSearchDto searchDto = new TrainingOrganizerSearchDto();
		searchDto.setTrainingNos(trainingNos); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingOrganizerEntity> getSearchResultByCondition(TrainingOrganizerSearchDto searchDto) {

		// 検索
		List<TrainingOrganizerEntity> entities = trainingOrganizerDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingOrganizerEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修会主催者検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
