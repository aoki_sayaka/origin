package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.OtherOrgPointApplicationDao;
import jp.or.jaot.domain.OtherOrgPointApplicationDomain;
import jp.or.jaot.model.dto.search.OtherOrgPointApplicationSearchDto;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;

/**
 * 他団体・SIGポイント申請リポジトリ
 */
@Repository
public class OtherOrgPointApplicationRepository
		extends BaseRepository<OtherOrgPointApplicationDomain, OtherOrgPointApplicationEntity> {

	/** 他団体・SIGポイント申請DAO */
	private final OtherOrgPointApplicationDao otherOrgPointApplicationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param otherOrgPointApplicationDao DAO
	 */
	@Autowired
	public OtherOrgPointApplicationRepository(OtherOrgPointApplicationDao otherOrgPointApplicationDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, otherOrgPointApplicationDao);
		this.otherOrgPointApplicationDao = otherOrgPointApplicationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public OtherOrgPointApplicationEntity getSearchResult(Long id) {
		OtherOrgPointApplicationEntity entity = null;
		if ((id == null) || ((entity = otherOrgPointApplicationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する他団体・SIGポイント申請が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(ID)
	 * @param id
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public OtherOrgPointApplicationEntity getSearchResultByMemberNo(Integer id) {
		OtherOrgPointApplicationEntity entity = null;
		if ((id == null) || ((entity = otherOrgPointApplicationDao.findById(id)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する他団体・SIGポイント申請が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<OtherOrgPointApplicationEntity> getSearchResultByCondition(OtherOrgPointApplicationSearchDto searchDto) {

		// 検索
		List<OtherOrgPointApplicationEntity> entities = otherOrgPointApplicationDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<OtherOrgPointApplicationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("他団体・SIGポイント申請検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
