package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ClinicalTrainingLeaderQualifyHistoryDao;
import jp.or.jaot.domain.ClinicalTrainingLeaderQualifyHistoryDomain;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderQualifyHistorySearchDto;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderQualifyHistoryEntity;

/**
 * 臨床実習指導者認定履歴リポジトリ
 */
@Repository
public class ClinicalTrainingLeaderQualifyHistoryRepository
		extends BaseRepository<ClinicalTrainingLeaderQualifyHistoryDomain, ClinicalTrainingLeaderQualifyHistoryEntity> {

	/** 臨床実習指導者認定履歴DAO */
	private final ClinicalTrainingLeaderQualifyHistoryDao clinicalTrainingLeaderQualifyHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param clinicalTrainingLeaderQualifyHistoryDao DAO
	 */
	@Autowired
	public ClinicalTrainingLeaderQualifyHistoryRepository(
			ClinicalTrainingLeaderQualifyHistoryDao clinicalTrainingLeaderQualifyHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, clinicalTrainingLeaderQualifyHistoryDao);
		this.clinicalTrainingLeaderQualifyHistoryDao = clinicalTrainingLeaderQualifyHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ClinicalTrainingLeaderQualifyHistoryEntity getSearchResult(Long id) {
		ClinicalTrainingLeaderQualifyHistoryEntity entity = null;
		if ((id == null) || ((entity = clinicalTrainingLeaderQualifyHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定された会員番号に該当する臨床実習指導者認定履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<ClinicalTrainingLeaderQualifyHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		ClinicalTrainingLeaderQualifyHistorySearchDto searchDto = new ClinicalTrainingLeaderQualifyHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ClinicalTrainingLeaderQualifyHistoryEntity> getSearchResultByCondition(
			ClinicalTrainingLeaderQualifyHistorySearchDto searchDto) {

		// 検索
		List<ClinicalTrainingLeaderQualifyHistoryEntity> entities = clinicalTrainingLeaderQualifyHistoryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ClinicalTrainingLeaderQualifyHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("臨床実習指導者認定履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
