package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.NoticeDestinationEntity;

/**
 * お知らせ(宛先)ドメイン
 */
@Component
@Scope("prototype")
public class NoticeDestinationDomain extends BaseDomain<NoticeDestinationEntity> {

	/**
	 * コンストラクタ
	 */
	public NoticeDestinationDomain() {
		setEntity(new NoticeDestinationEntity()); // 初期エンティティ
	}

	// TODO
}
