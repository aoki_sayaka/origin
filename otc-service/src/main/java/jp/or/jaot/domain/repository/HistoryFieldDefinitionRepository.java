package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.HistoryFieldDefinitionDao;
import jp.or.jaot.domain.HistoryFieldDefinitionDomain;
import jp.or.jaot.model.dto.search.HistoryFieldDefinitionSearchDto;
import jp.or.jaot.model.entity.HistoryFieldDefinitionEntity;

/**
 * 履歴フィールド定義リポジトリ<br>
 * インスタンス生成時にDBを読み込んでキャッシュ用リストを設定します。
 */
@Repository
public class HistoryFieldDefinitionRepository
		extends BaseRepository<HistoryFieldDefinitionDomain, HistoryFieldDefinitionEntity> {

	/** 履歴フィールド定義DAO */
	private final HistoryFieldDefinitionDao historyFieldDefinitionDao;

	/** 履歴フィールド定義エンティティリスト(キャッシュ用) */
	private final List<HistoryFieldDefinitionEntity> cacheEntities;

	/**
	 * コンストラクタ(インジェクション)
	 * @param historyFieldDefinitionDao DAO
	 */
	@Autowired
	public HistoryFieldDefinitionRepository(HistoryFieldDefinitionDao historyFieldDefinitionDao) {
		super(ERROR_PLACE.LOGIC_COMMON, historyFieldDefinitionDao);
		this.historyFieldDefinitionDao = historyFieldDefinitionDao;
		this.cacheEntities = historyFieldDefinitionDao.findByCondition(new HistoryFieldDefinitionSearchDto());
		logger.info("履歴フィールド定義読込 : 件数={}", cacheEntities.size());
	}

	/**
	 * キャッシュ結果取得
	 * @return エンティティリスト
	 */
	public List<HistoryFieldDefinitionEntity> getCacheResult() {
		return cacheEntities;
	}

	/**
	 * キャッシュ結果取得(フィールド名(物理))
	 * @param physicalNm フィールド名(物理)
	 * @return エンティティ
	 */
	public HistoryFieldDefinitionEntity getCacheResultByPhysicalNm(String physicalNm) {
		return cacheEntities.stream().filter(E -> E.getFieldPhysicalNm().equals(physicalNm)).findFirst().orElse(null);
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public HistoryFieldDefinitionEntity getSearchResult(Long id) {
		HistoryFieldDefinitionEntity entity = null;
		if ((id == null) || ((entity = historyFieldDefinitionDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する履歴フィールド定義が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(フィールド名(物理))
	 * @param physicalNm フィールド名(物理)
	 * @return エンティティリスト
	 */
	public List<HistoryFieldDefinitionEntity> getSearchResultByPhysicalNm(String physicalNm) {

		// 検索条件生成
		HistoryFieldDefinitionSearchDto searchDto = new HistoryFieldDefinitionSearchDto();
		searchDto.setFieldPhysicalNm(physicalNm); // フィールド名(物理)

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<HistoryFieldDefinitionEntity> getSearchResultByCondition(HistoryFieldDefinitionSearchDto searchDto) {

		// 検索
		List<HistoryFieldDefinitionEntity> entities = historyFieldDefinitionDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<HistoryFieldDefinitionEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("履歴フィールド定義検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
