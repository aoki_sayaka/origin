package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.RegionRelatedSmallClassificationEntity;

/**
 * 領域関連小分類ドメイン
 */
@Component
@Scope("prototype")
public class RegionRelatedSmallClassificationDomain extends BaseDomain<RegionRelatedSmallClassificationEntity> {

	/**
	 * コンストラクタ
	 */
	public RegionRelatedSmallClassificationDomain() {
		setEntity(new RegionRelatedSmallClassificationEntity()); // 初期エンティティ
	}

	// TODO
}
