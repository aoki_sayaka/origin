package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.TrainingReportEntity;

/**
 * 研修会報告書ドメイン
 */
@Component
@Scope("prototype")
public class TrainingReportDomain extends BaseDomain<TrainingReportEntity> {

	/**
	 * コンストラクタ
	 */
	public TrainingReportDomain() {
		setEntity(new TrainingReportEntity()); // 初期エンティティ
	}

	// TODO
}
