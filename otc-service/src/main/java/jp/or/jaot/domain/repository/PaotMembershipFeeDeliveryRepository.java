package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.PaotMembershipFeeDeliveryDao;
import jp.or.jaot.domain.PaotMembershipFeeDeliveryDomain;
import jp.or.jaot.model.dto.search.PaotMembershipFeeDeliverySearchDto;
import jp.or.jaot.model.entity.PaotMembershipFeeDeliveryEntity;

/**
 * 県士会会費納入リポジトリ
 */
@Repository
public class PaotMembershipFeeDeliveryRepository
		extends BaseRepository<PaotMembershipFeeDeliveryDomain, PaotMembershipFeeDeliveryEntity> {

	/** 県士会会費納入DAO */
	private final PaotMembershipFeeDeliveryDao paotMembershipFeeDeliveryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param paotMembershipFeeDeliveryDao DAO
	 */
	@Autowired
	public PaotMembershipFeeDeliveryRepository(PaotMembershipFeeDeliveryDao paotMembershipFeeDeliveryDao) {
		super(ERROR_PLACE.LOGIC_COMMON, paotMembershipFeeDeliveryDao);
		this.paotMembershipFeeDeliveryDao = paotMembershipFeeDeliveryDao;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<PaotMembershipFeeDeliveryEntity> getSearchResultByCondition(
			PaotMembershipFeeDeliverySearchDto searchDto) {

		// 検索
		List<PaotMembershipFeeDeliveryEntity> entities = paotMembershipFeeDeliveryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<PaotMembershipFeeDeliveryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug(" 県士会会費納入検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
