package jp.or.jaot.domain;

import java.util.Arrays;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.FacilityApplicationEntity;

/**
 * 施設登録申請ドメイン
 */
@Component
@Scope("prototype")
public class FacilityApplicationDomain extends BaseDomain<FacilityApplicationEntity> {

	/**
	 * 申請者区分コードの列挙体
	 */
	public enum MEMBER_CATEGORY_CD {

		/** 正会員 */
		REGULAR("00"),
		/** 仮会員 */
		TEMPORARY("01");

		/** 値 */
		private final String value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private MEMBER_CATEGORY_CD(final String value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public String getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static MEMBER_CATEGORY_CD valueOfText(String value) {
			return Arrays.stream(MEMBER_CATEGORY_CD.values()).filter(V -> V.getValue().equals(value)).findFirst()
					.orElse(null);
		}
	}

	/**
	 * 申請ステータスの列挙体
	 */
	public enum APPLICATION_STATUS {

		/** 申請中 */
		APPLYING(1),
		/** 承認済 */
		APPROVED(2),
		/** 廃止 */
		ABOLISHED(3);

		/** 値 */
		private final Integer value;

		/**  
		 * コンストラクタ
		 * @param value 値
		 */
		private APPLICATION_STATUS(final Integer value) {
			this.value = value;
		}

		/**
		 * 値取得
		 * @return 値
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * 列挙体取得
		 * @param value 値
		 * @return 列挙体(存在しない : null)
		 */
		public static APPLICATION_STATUS valueOf(Integer value) {
			return Arrays.stream(APPLICATION_STATUS.values()).filter(V -> V.getValue().equals(value)).findFirst()
					.orElse(null);
		}
	}

	/**
	 * コンストラクタ
	 */
	public FacilityApplicationDomain() {
		setEntity(new FacilityApplicationEntity()); // 初期エンティティ
	}

	// TODO
}
