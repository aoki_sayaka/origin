package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.MemberWithdrawalHistoryDao;
import jp.or.jaot.domain.MemberWithdrawalHistoryDomain;
import jp.or.jaot.model.dto.search.MemberWithdrawalHistorySearchDto;
import jp.or.jaot.model.entity.MemberWithdrawalHistoryEntity;

/**
 * 退会履歴リポジトリ
 */
@Repository
public class MemberWithdrawalHistoryRepository extends BaseRepository<MemberWithdrawalHistoryDomain, MemberWithdrawalHistoryEntity> {

	/** 退会履歴DAO */
	private final MemberWithdrawalHistoryDao memberWithdrawalHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param memberWithdrawalHistoryDao DAO
	 */
	@Autowired
	public MemberWithdrawalHistoryRepository(MemberWithdrawalHistoryDao memberWithdrawalHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, memberWithdrawalHistoryDao);
		this.memberWithdrawalHistoryDao = memberWithdrawalHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public MemberWithdrawalHistoryEntity getSearchResult(Long id) {
		MemberWithdrawalHistoryEntity entity = null;
		if ((id == null) || ((entity = memberWithdrawalHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する退会履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<MemberWithdrawalHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		MemberWithdrawalHistorySearchDto searchDto = new MemberWithdrawalHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<MemberWithdrawalHistoryEntity> getSearchResultByCondition(MemberWithdrawalHistorySearchDto searchDto) {

		// 検索
		List<MemberWithdrawalHistoryEntity> entities = memberWithdrawalHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<MemberWithdrawalHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("退会履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
