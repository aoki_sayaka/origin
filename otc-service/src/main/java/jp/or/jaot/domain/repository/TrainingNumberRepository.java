package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingNumberDao;
import jp.or.jaot.domain.TrainingNumberDomain;
import jp.or.jaot.model.dto.search.TrainingNumberSearchDto;
import jp.or.jaot.model.entity.TrainingNumberEntity;

@Repository
public class TrainingNumberRepository extends BaseRepository<TrainingNumberDomain, TrainingNumberEntity> {

	/** 基礎研修履歴DAO */
	private final TrainingNumberDao trainingNumberDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingNumberDao DAO
	 */
	@Autowired
	public TrainingNumberRepository(TrainingNumberDao trainingNumberDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, trainingNumberDao);
		this.trainingNumberDao = trainingNumberDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingNumberEntity getSearchResult(Long id) {
		TrainingNumberEntity entity = null;
		if ((id == null) || ((entity = trainingNumberDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修番号が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(年度)
	 * @param fiscalYear 年度
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public List<TrainingNumberEntity> getSearchResultByFiscalYear(Integer fiscalYear) {
		// 検索条件生成
		TrainingNumberSearchDto searchDto = new TrainingNumberSearchDto();
		searchDto.setFiscalYear(fiscalYear); // 会員番号
		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingNumberEntity> getSearchResultByCondition(TrainingNumberSearchDto searchDto) {

		// 検索
		List<TrainingNumberEntity> entities = trainingNumberDao.findByByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingNumberEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修番号検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}
}
