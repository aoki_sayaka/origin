package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberLoginHistoryEntity;

/**
 * 会員ポータルログイン履歴ドメイン
 */
@Component
@Scope("prototype")
public class MemberLoginHistoryDomain extends BaseDomain<MemberLoginHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberLoginHistoryDomain() {
		setEntity(new MemberLoginHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
