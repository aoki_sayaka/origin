package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.SocialContributionActual2FieldDao;
import jp.or.jaot.domain.SocialContributionActual2FieldDomain;
import jp.or.jaot.model.dto.search.SocialContributionActual2FieldSearchDto;
import jp.or.jaot.model.entity.SocialContributionActual2FieldEntity;

/**
 * 活動実績の領域2(後輩育成・社会貢献)リポジトリ
 */
@Repository
public class SocialContributionActual2FieldRepository
		extends BaseRepository<SocialContributionActual2FieldDomain, SocialContributionActual2FieldEntity> {

	/** 活動実績の領域2(後輩育成・社会貢献)DAO */
	private final SocialContributionActual2FieldDao socialContributionActual2FieldDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param socialContributionActual2FieldDao DAO
	 */
	@Autowired
	public SocialContributionActual2FieldRepository(
			SocialContributionActual2FieldDao socialContributionActual2FieldDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, socialContributionActual2FieldDao);
		this.socialContributionActual2FieldDao = socialContributionActual2FieldDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public SocialContributionActual2FieldEntity getSearchResult(Long id) {
		SocialContributionActual2FieldEntity entity = null;
		if ((id == null) || ((entity = socialContributionActual2FieldDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する活動実績の領域2(後輩育成・社会貢献)が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(活動実績ID)
	 * @param resultId 活動実績ID
	 * @return エンティティリスト
	 */
	public List<SocialContributionActual2FieldEntity> getSearchResultByResultId(Long resultId) {

		// 検索条件生成
		SocialContributionActual2FieldSearchDto searchDto = new SocialContributionActual2FieldSearchDto();
		searchDto.setResultIds(new Long[] { resultId }); // 活動実績ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(活動実績ID)
	 * @param resultIds 活動実績ID配列
	 * @return エンティティリスト
	 */
	public List<SocialContributionActual2FieldEntity> getSearchResultByResultIds(Long[] resultIds) {

		// 検索条件生成
		SocialContributionActual2FieldSearchDto searchDto = new SocialContributionActual2FieldSearchDto();
		searchDto.setResultIds(resultIds); // 活動実績ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<SocialContributionActual2FieldEntity> getSearchResultByCondition(
			SocialContributionActual2FieldSearchDto searchDto) {

		// 検索
		List<SocialContributionActual2FieldEntity> entities = socialContributionActual2FieldDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<SocialContributionActual2FieldEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("活動実績の領域2(後輩育成・社会貢献)検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
