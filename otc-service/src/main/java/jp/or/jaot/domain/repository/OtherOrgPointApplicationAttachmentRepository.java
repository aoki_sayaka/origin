package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.OtherOrgPointApplicationAttachmentDao;
import jp.or.jaot.domain.OtherOrgPointApplicationAttachmentDomain;
import jp.or.jaot.model.dto.search.OtherOrgPointApplicationAttachmentSearchDto;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;

/**
 * 他団体・SIGポイント添付ファイルリポジトリ
 */
@Repository
public class OtherOrgPointApplicationAttachmentRepository
		extends BaseRepository<OtherOrgPointApplicationAttachmentDomain, OtherOrgPointApplicationAttachmentEntity> {

	/** 他団体・SIGポイント添付ファイルDAO */
	private final OtherOrgPointApplicationAttachmentDao otherOrgPointApplicationAttachmentDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param otherOrgPointApplicationAttachmentDao DAO
	 */
	@Autowired
	public OtherOrgPointApplicationAttachmentRepository(OtherOrgPointApplicationAttachmentDao otherOrgPointApplicationAttachmentDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, otherOrgPointApplicationAttachmentDao);
		this.otherOrgPointApplicationAttachmentDao = otherOrgPointApplicationAttachmentDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public OtherOrgPointApplicationAttachmentEntity getSearchResult(Long id) {
		OtherOrgPointApplicationAttachmentEntity entity = null;
		if ((id == null) || ((entity = otherOrgPointApplicationAttachmentDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する他団体・SIGポイント添付ファイルが存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(ID)
	 * @param id 
	 * @return エンティティリスト
	 */
	public List<OtherOrgPointApplicationAttachmentEntity> getSearchResultById(Integer id) {

		// 検索条件生成
		OtherOrgPointApplicationAttachmentSearchDto searchDto = new OtherOrgPointApplicationAttachmentSearchDto();
		searchDto.setId(id); // ID

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<OtherOrgPointApplicationAttachmentEntity> getSearchResultByCondition(OtherOrgPointApplicationAttachmentSearchDto searchDto) {

		// 検索
		List<OtherOrgPointApplicationAttachmentEntity> entities = otherOrgPointApplicationAttachmentDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<OtherOrgPointApplicationAttachmentEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("他団体・SIGポイント添付ファイル検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
