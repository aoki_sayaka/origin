package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;

/**
 * 専門作業療法士受講履歴ドメイン
 */
@Component
@Scope("prototype")
public class ProfessionalOtAttendingSummaryDomain extends BaseDomain<ProfessionalOtAttendingSummaryEntity> {

	/**
	 * コンストラクタ
	 */
	public ProfessionalOtAttendingSummaryDomain() {
		setEntity(new ProfessionalOtAttendingSummaryEntity()); // 初期エンティティ
	}

	// TODO
}
