package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.TrainingInstructorDao;
import jp.or.jaot.domain.TrainingInstructorDomain;
import jp.or.jaot.model.dto.search.TrainingInstructorSearchDto;
import jp.or.jaot.model.entity.TrainingInstructorEntity;

/**
 * 研修会講師実績リポジトリ
 */
@Repository
public class TrainingInstructorRepository extends BaseRepository<TrainingInstructorDomain, TrainingInstructorEntity> {

	/** 研修会講師実績DAO */
	private final TrainingInstructorDao trainingInstructorDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param trainingInstructorDao DAO
	 */
	@Autowired
	public TrainingInstructorRepository(TrainingInstructorDao trainingInstructorDao) {
		super(ERROR_PLACE.LOGIC_SECRETARIAT, trainingInstructorDao);
		this.trainingInstructorDao = trainingInstructorDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public TrainingInstructorEntity getSearchResult(Long id) {
		TrainingInstructorEntity entity = null;
		if ((id == null) || ((entity = trainingInstructorDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する研修会講師実績が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNo 研修会番号
	 * @return エンティティリスト
	 */
	public List<TrainingInstructorEntity> getSearchResultByTrainingNo(String trainingNo) {

		// 検索条件生成
		TrainingInstructorSearchDto searchDto = new TrainingInstructorSearchDto();
		searchDto.setTrainingNos(new String[] { trainingNo }); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(研修会番号)
	 * @param trainingNos 研修会番号配列
	 * @return エンティティリスト
	 */
	public List<TrainingInstructorEntity> getSearchResultByTrainingNos(String[] trainingNos) {

		// 検索条件生成
		TrainingInstructorSearchDto searchDto = new TrainingInstructorSearchDto();
		searchDto.setTrainingNos(trainingNos); // 研修会番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<TrainingInstructorEntity> getSearchResultByCondition(TrainingInstructorSearchDto searchDto) {

		// 検索
		List<TrainingInstructorEntity> entities = trainingInstructorDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<TrainingInstructorEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("研修会講師実績検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	/**
	 * 状態設定
	 * @param id ID
	 * @param updateUser 更新者
	 * @param enabled 状態(true=使用可能, false=使用不可)
	 */
	public void setEnable(Long id, String updateUser, boolean enabled) {
		trainingInstructorDao.setEnableEntity(id, updateUser, enabled);
	}

	// TODO
}
