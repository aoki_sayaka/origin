package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.RegionRelatedMiddleClassificationDao;
import jp.or.jaot.domain.RegionRelatedMiddleClassificationDomain;
import jp.or.jaot.model.dto.search.RegionRelatedMiddleClassificationSearchDto;
import jp.or.jaot.model.entity.RegionRelatedMiddleClassificationEntity;

/**
 * 領域関連中分類リポジトリ
 */
@Repository
public class RegionRelatedMiddleClassificationRepository
		extends BaseRepository<RegionRelatedMiddleClassificationDomain, RegionRelatedMiddleClassificationEntity> {

	/** 領域関連中分類DAO */
	private final RegionRelatedMiddleClassificationDao regionRelatedMiddleClassificationDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param regionRelatedMiddleClassificationDao DAO
	 */
	@Autowired
	public RegionRelatedMiddleClassificationRepository(
			RegionRelatedMiddleClassificationDao regionRelatedMiddleClassificationDao) {
		super(ERROR_PLACE.LOGIC_COMMON, regionRelatedMiddleClassificationDao);
		this.regionRelatedMiddleClassificationDao = regionRelatedMiddleClassificationDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public RegionRelatedMiddleClassificationEntity getSearchResult(Long id) {
		RegionRelatedMiddleClassificationEntity entity = null;
		if ((id == null) || ((entity = regionRelatedMiddleClassificationDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する領域関連中分類が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<RegionRelatedMiddleClassificationEntity> getSearchResultByCondition(
			RegionRelatedMiddleClassificationSearchDto searchDto) {

		// 検索
		List<RegionRelatedMiddleClassificationEntity> entities = regionRelatedMiddleClassificationDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<RegionRelatedMiddleClassificationEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("領域関連中分類検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
