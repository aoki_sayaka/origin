package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.PaotMembershipHistoryDao;
import jp.or.jaot.domain.PaotMembershipHistoryDomain;
import jp.or.jaot.model.dto.search.PaotMembershipHistorySearchDto;
import jp.or.jaot.model.entity.PaotMembershipHistoryEntity;

/**
 * 県士会在籍履歴リポジトリ
 */
@Repository
public class PaotMembershipHistoryRepository
		extends BaseRepository<PaotMembershipHistoryDomain, PaotMembershipHistoryEntity> {

	/** 県士会在籍履歴DAO */
	private final PaotMembershipHistoryDao paotMembershipHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param paotMembershipHistoryDao DAO
	 */
	@Autowired
	public PaotMembershipHistoryRepository(PaotMembershipHistoryDao paotMembershipHistoryDao) {
		super(ERROR_PLACE.LOGIC_MEMBER, paotMembershipHistoryDao);
		this.paotMembershipHistoryDao = paotMembershipHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public PaotMembershipHistoryEntity getSearchResult(Long id) {
		PaotMembershipHistoryEntity entity = null;
		if ((id == null)
				|| ((entity = paotMembershipHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する県士会在籍履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<PaotMembershipHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		PaotMembershipHistorySearchDto searchDto = new PaotMembershipHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<PaotMembershipHistoryEntity> getSearchResultByCondition(PaotMembershipHistorySearchDto searchDto) {

		// 検索
		List<PaotMembershipHistoryEntity> entities = paotMembershipHistoryDao.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<PaotMembershipHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("県士会在籍履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO
}
