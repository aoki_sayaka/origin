package jp.or.jaot.domain.repository;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import jp.or.jaot.core.domain.BaseRepository;
import jp.or.jaot.core.exception.business.SearchBusinessException;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_CAUSE;
import jp.or.jaot.core.util.ExceptionHelper.ERROR_PLACE;
import jp.or.jaot.datasource.ProfessionalOtQualifyHistoryDao;
import jp.or.jaot.domain.ProfessionalOtQualifyHistoryDomain;
import jp.or.jaot.model.dto.search.ProfessionalOtQualifyHistorySearchDto;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;

/**
 * 専門作業療法士認定更新履歴リポジトリ
 */
@Repository
public class ProfessionalOtQualifyHistoryRepository
		extends BaseRepository<ProfessionalOtQualifyHistoryDomain, ProfessionalOtQualifyHistoryEntity> {

	/** 専門作業療法士認定更新履歴DAO */
	private final ProfessionalOtQualifyHistoryDao professionalOtQualifyHistoryDao;

	/**
	 * コンストラクタ(インジェクション)
	 * @param basicOtQualifyHistoryDao DAO
	 */
	@Autowired
	public ProfessionalOtQualifyHistoryRepository(ProfessionalOtQualifyHistoryDao professionalOtQualifyHistoryDao) {
		super(ERROR_PLACE.LOGIC_ATTENDING_HISTORY, professionalOtQualifyHistoryDao);
		this.professionalOtQualifyHistoryDao = professionalOtQualifyHistoryDao;
	}

	/**
	 * 検索結果取得(キー)
	 * @param id ID
	 * @return エンティティ
	 * @throws SearchBusinessException 検索業務例外
	 */
	public ProfessionalOtQualifyHistoryEntity getSearchResult(Long id) {
		ProfessionalOtQualifyHistoryEntity entity = null;
		if ((id == null) || ((entity = professionalOtQualifyHistoryDao.find(id, false)) == null)) {
			ERROR_CAUSE errorCause = ERROR_CAUSE.CODE_INVALID_PARAM;
			String detail = String.format("id=%s", id);
			String message = String.format("検索条件に指定されたIDに該当する専門作業療法士認定更新履歴が存在しません");
			throw new SearchBusinessException(errorCause, errorPlace, detail, message);
		}
		return entity;
	}

	/**
	 * 検索結果取得(会員番号)
	 * @param memberNo 会員番号
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtQualifyHistoryEntity> getSearchResultByMemberNo(Integer memberNo) {

		// 検索条件生成
		ProfessionalOtQualifyHistorySearchDto searchDto = new ProfessionalOtQualifyHistorySearchDto();
		searchDto.setMemberNo(memberNo); // 会員番号

		return getSearchResultByCondition(searchDto);
	}

	/**
	 * 検索結果取得(条件)
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 */
	public List<ProfessionalOtQualifyHistoryEntity> getSearchResultByCondition(
			ProfessionalOtQualifyHistorySearchDto searchDto) {

		// 検索
		List<ProfessionalOtQualifyHistoryEntity> entities = professionalOtQualifyHistoryDao
				.findByCondition(searchDto);
		if (CollectionUtils.isEmpty(entities)) {
			entities = new ArrayList<ProfessionalOtQualifyHistoryEntity>(); // 空リスト
		}

		// 検索結果リスト数
		logger.debug("専門作業療法士認定更新履歴検索 : 件数={}, 条件={}", entities.size(), searchDto);

		return entities;
	}

	// TODO

}
