package jp.or.jaot.domain;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.domain.BaseDomain;
import jp.or.jaot.model.entity.MemberUpdateHistoryEntity;

/**
 * 会員更新履歴ドメイン
 */
@Component
@Scope("prototype")
public class MemberUpdateHistoryDomain extends BaseDomain<MemberUpdateHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	public MemberUpdateHistoryDomain() {
		setEntity(new MemberUpdateHistoryEntity()); // 初期エンティティ
	}

	// TODO
}
