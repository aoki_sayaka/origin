package jp.or.jaot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * パスワードエンコーダ設定クラス
 */
@Configuration
public class PasswordEncoderConfig {

	/**
	 *  パスワードエンコーダ(Bean定義)
	 */
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
