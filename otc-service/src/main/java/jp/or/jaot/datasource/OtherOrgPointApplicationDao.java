package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.dto.search.OtherOrgPointApplicationSearchDto;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;

/**
 * 他団体・SIGポイント申請DAO
 */
@Component
public class OtherOrgPointApplicationDao extends BaseDao<OtherOrgPointApplicationEntity> {

	/**
	 * ID検索
	 * @param id
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public OtherOrgPointApplicationEntity findById(Integer id) {
		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<OtherOrgPointApplicationEntity> query = builder
					.createQuery(OtherOrgPointApplicationEntity.class);

			// FROM句
			Root<OtherOrgPointApplicationEntity> root = query.from(OtherOrgPointApplicationEntity.class);

			// WHERE句
			Predicate where = builder.equal(root.get("id"), id);

			// 実行
			CriteriaQuery<OtherOrgPointApplicationEntity> resultQuery = query.select(root).where(where);
			OtherOrgPointApplicationEntity entity = getEntityManager().createQuery(resultQuery).getSingleResult();

			return entity;
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<OtherOrgPointApplicationEntity> findByCondition(OtherOrgPointApplicationSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<OtherOrgPointApplicationEntity> query = builder
					.createQuery(OtherOrgPointApplicationEntity.class);

			// FROM句
			Root<OtherOrgPointApplicationEntity> root = query.from(OtherOrgPointApplicationEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// 実行
			return getResultList(query.select(root).where(where), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<OtherOrgPointApplicationEntity> root,
			OtherOrgPointApplicationSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// ID
				if (searchDto.getId() != null) {
					add(builder.equal(root.get("id"), searchDto.getId()));
				}

				// TODO : 検索条件追加
				
				// 申請日
				Date fromApplicationDate = DateUtil.getFromDate(searchDto.getApplicationDateFrom());
				Date toApplicationDate = DateUtil.getToDate(searchDto.getApplicationDateTo());
				if (fromApplicationDate != null && toApplicationDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.between(root.get("applicationDate"), fromApplicationDate, toApplicationDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (fromApplicationDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.greaterThanOrEqualTo(root.get("applicationDate"), fromApplicationDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (toApplicationDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.lessThanOrEqualTo(root.get("applicationDate"), toApplicationDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				}
				
				// 申請状態
				if (StringUtils.hasText(searchDto.getApplicationStatusCd())) {
					add(builder.equal(root.get("statusCd"), searchDto.getApplicationStatusCd()));
				}
				
				// 受講日
				Date fromAttendingDate = DateUtil.getFromDate(searchDto.getAttendingDateFrom());
				Date toAttendingDate = DateUtil.getToDate(searchDto.getAttendingDateTo());
				if (fromAttendingDate != null && toAttendingDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.between(root.get("attendingDateFrom"), fromAttendingDate, toAttendingDate));
							add(builder.between(root.get("attendingDateTo"), fromAttendingDate, toAttendingDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (fromAttendingDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.greaterThanOrEqualTo(root.get("attendingDateFrom"), fromAttendingDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (toAttendingDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.lessThanOrEqualTo(root.get("attendingDateTo"), toAttendingDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				}
				
				// 主催者
				if (StringUtils.hasText(searchDto.getOrganizerCd())){
					add(builder.equal(root.get("organizerCd"), searchDto.getOrganizerCd()));
				}
				
				// 他団体SIG名
				if (searchDto.getSigNameCd() != null) {
					add(builder.equal(root.get("otherOrganizationSigCd"), searchDto.getSigNameCd()));
				}
				
				// 受講テーマ
				if (StringUtils.hasText(searchDto.getAttendingTheme())) {
					add(builder.like(root.get("thema"), parMatch(searchDto.getAttendingTheme())));
				}
				
				// ポイント種別
				if (StringUtils.hasText(searchDto.getPointTypeCd())) {
					add(builder.equal(root.get("pointCategoryCd"), searchDto.getPointTypeCd()));
				}
				
				// 申請者 会員番号
				Integer fromMemberNo = searchDto.getMemberNoFrom();
				Integer toMemberNo = searchDto.getMemberNoTo();
				if (fromMemberNo != null && toMemberNo != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.between(root.get("memberNo"), fromMemberNo, toMemberNo));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (fromMemberNo != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.equal(root.get("memberNo"), fromMemberNo));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (toMemberNo != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.equal(root.get("memberNo"), toMemberNo));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				}
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
