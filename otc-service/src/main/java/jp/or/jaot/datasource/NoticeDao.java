package jp.or.jaot.datasource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.NoticeSearchDto;
import jp.or.jaot.model.entity.NoticeEntity;

/**
 * お知らせDAO
 */
@Component
public class NoticeDao extends BaseDao<NoticeEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<NoticeEntity> findByCondition(NoticeSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<NoticeEntity> query = builder.createQuery(NoticeEntity.class);

			// FROM句
			Root<NoticeEntity> root = query.from(NoticeEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.desc(root.get("publishFrom"))); // 降順:掲載開始日
					add(builder.desc(root.get("publishTo"))); // 降順:掲載終了日
					add(builder.desc(root.get("id"))); // 降順:ID
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<NoticeEntity> root,
			NoticeSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 掲載日範囲
				Timestamp rangeFromDate = searchDto.getPublishRangeFrom();
				Timestamp rangeToDate = searchDto.getPublishRangeTo();
				if (rangeFromDate != null && rangeToDate != null) {
					List<Predicate> rangePredicates = new ArrayList<Predicate>() {
						{
							// 掲載開始日が掲載日範囲内に存在
							add(builder.between(root.get("publishFrom"), rangeFromDate, rangeToDate));

							// 掲載終了日が掲載日範囲内に存在							
							add(builder.between(root.get("publishTo"), rangeFromDate, rangeToDate));

							// 掲載開始日から掲載終了日の間に掲載日範囲が存在
							List<Predicate> rangeSubPredicates = new ArrayList<Predicate>() {
								{
									add(builder.lessThan(root.get("publishFrom"), rangeFromDate));
									add(builder.greaterThan(root.get("publishTo"), rangeToDate));
								}
							};
							add(builder.and(rangeSubPredicates.toArray(new Predicate[rangeSubPredicates.size()])));
						}
					};
					add(builder.or(rangePredicates.toArray(new Predicate[rangePredicates.size()])));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
