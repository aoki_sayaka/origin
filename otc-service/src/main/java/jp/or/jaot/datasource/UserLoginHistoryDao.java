package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.UserLoginHistorySearchDto;
import jp.or.jaot.model.entity.UserLoginHistoryEntity;

/**
 * 事務局サイトログイン履歴DAO
 */
@Component
public class UserLoginHistoryDao extends BaseDao<UserLoginHistoryEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<UserLoginHistoryEntity> findByCondition(UserLoginHistorySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<UserLoginHistoryEntity> query = builder.createQuery(UserLoginHistoryEntity.class);

			// FROM句
			Root<UserLoginHistoryEntity> root = query.from(UserLoginHistoryEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<UserLoginHistoryEntity> root,
			BaseSearchDto baseSearchDto) {

		// 事務局サイトログイン履歴DTO
		UserLoginHistorySearchDto searchDto = (UserLoginHistorySearchDto) baseSearchDto;

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 事務局ユーザID
				if (searchDto.getUserId() != null) {
					add(builder.equal(root.get("userId"), searchDto.getUserId()));
				}

			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
