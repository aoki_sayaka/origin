package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.MemberRevokedAuthoritySearchDto;
import jp.or.jaot.model.entity.GeneralCodeId;
import jp.or.jaot.model.entity.MemberRevokedAuthorityEntity;

/**
 * 会員権限DAO
 */
@Component
public class MemberRevokedAuthorityDao extends BaseDao<MemberRevokedAuthorityEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<MemberRevokedAuthorityEntity> findByCondition(MemberRevokedAuthoritySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<MemberRevokedAuthorityEntity> query = builder.createQuery(MemberRevokedAuthorityEntity.class);

			// FROM句
			Root<MemberRevokedAuthorityEntity> root = query.from(MemberRevokedAuthorityEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("codeId"))); // 昇順:コード識別ID
					add(builder.asc(root.get("code"))); // 昇順:コード値
					add(builder.asc(root.get("pathOrKey"))); // 昇順:パス/キー値
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<MemberRevokedAuthorityEntity> root,
			MemberRevokedAuthoritySearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 汎用コードID(複数指定)
				if (!ArrayUtils.isEmpty(searchDto.getGeneralCodeIds())) {
					List<Predicate> codePredicates = new ArrayList<Predicate>() {
						{
							for (GeneralCodeId generalCodeId : searchDto.getGeneralCodeIds()) {
								Predicate pCodeId = builder.equal(root.get("codeId"), generalCodeId.getId());
								Predicate pCode = builder.equal(root.get("code"), generalCodeId.getCode());
								add(builder.and(pCodeId, pCode));
							}
						}
					};
					add(builder.or(codePredicates.toArray(new Predicate[codePredicates.size()])));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
