package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.SocialContributionActual2FieldSearchDto;
import jp.or.jaot.model.entity.SocialContributionActual2FieldEntity;

/**
 * 活動実績の領域2(後輩育成・社会貢献)DAO
 */
@Component
public class SocialContributionActual2FieldDao extends BaseDao<SocialContributionActual2FieldEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<SocialContributionActual2FieldEntity> findByCondition(
			SocialContributionActual2FieldSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<SocialContributionActual2FieldEntity> query = builder
					.createQuery(SocialContributionActual2FieldEntity.class);

			// FROM句
			Root<SocialContributionActual2FieldEntity> root = query.from(SocialContributionActual2FieldEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("resultId"))); // 昇順:活動実績ID
					add(builder.asc(root.get("fieldCd"))); // 昇順:領域コード
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<SocialContributionActual2FieldEntity> root,
			SocialContributionActual2FieldSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 活動実績ID
				if (!ArrayUtils.isEmpty(searchDto.getResultIds())) {
					add(createInPhrase(builder, root, "resultId", searchDto.getResultIds()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
