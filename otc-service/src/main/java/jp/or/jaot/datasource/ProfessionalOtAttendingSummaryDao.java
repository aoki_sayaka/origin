package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.ProfessionalOtAttendingSummarySearchDto;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;

/**
 * 専門作業療法士受講履歴DAO
 */
@Component
public class ProfessionalOtAttendingSummaryDao extends BaseDao<ProfessionalOtAttendingSummaryEntity> {

	/**
	 * 会員番号検索
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public ProfessionalOtAttendingSummaryEntity findByMemberNo(Integer memberNo) {
		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<ProfessionalOtAttendingSummaryEntity> query = builder
					.createQuery(ProfessionalOtAttendingSummaryEntity.class);

			// FROM句
			Root<ProfessionalOtAttendingSummaryEntity> root = query.from(ProfessionalOtAttendingSummaryEntity.class);

			// WHERE句
			Predicate where = builder.equal(root.get("memberNo"), memberNo);

			// 実行
			CriteriaQuery<ProfessionalOtAttendingSummaryEntity> resultQuery = query.select(root).where(where);
			ProfessionalOtAttendingSummaryEntity entity = getEntityManager().createQuery(resultQuery).getSingleResult();

			return entity;
		} catch (NoResultException e) {
			return null; // 該当データなし
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<ProfessionalOtAttendingSummaryEntity> findByCondition(
			ProfessionalOtAttendingSummarySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<ProfessionalOtAttendingSummaryEntity> query = builder
					.createQuery(ProfessionalOtAttendingSummaryEntity.class);

			// FROM句
			Root<ProfessionalOtAttendingSummaryEntity> root = query.from(ProfessionalOtAttendingSummaryEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// 実行
			return getResultList(query.select(root).where(where), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<ProfessionalOtAttendingSummaryEntity> root,
			ProfessionalOtAttendingSummarySearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}
				// 専門分野
				if (searchDto.getProfessionalField() != null) {
					add(builder.equal(root.get("professionalField"), searchDto.getProfessionalField()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
