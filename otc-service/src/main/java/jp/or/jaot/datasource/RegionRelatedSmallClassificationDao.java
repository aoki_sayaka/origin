package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.RegionRelatedSmallClassificationSearchDto;
import jp.or.jaot.model.entity.RegionRelatedSmallClassificationEntity;

/**
 * 領域関連小分類DAO
 */
@Component
public class RegionRelatedSmallClassificationDao extends BaseDao<RegionRelatedSmallClassificationEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<RegionRelatedSmallClassificationEntity> findByCondition(
			RegionRelatedSmallClassificationSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<RegionRelatedSmallClassificationEntity> query = builder
					.createQuery(RegionRelatedSmallClassificationEntity.class);

			// FROM句
			Root<RegionRelatedSmallClassificationEntity> root = query
					.from(RegionRelatedSmallClassificationEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("codeId"))); // 昇順:コード識別ID
					add(builder.asc(root.get("sortOrder"))); // 昇順:表示順序
					add(builder.asc(root.get("code"))); // 昇順:コード値
					add(builder.asc(root.get("parentCode"))); // 昇順:親コード値
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<RegionRelatedSmallClassificationEntity> root,
			RegionRelatedSmallClassificationSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// コード識別ID
				if (StringUtils.hasText(searchDto.getCodeId())) {
					add(builder.equal(root.get("codeId"), searchDto.getCodeId()));
				}

				// コード値
				if (StringUtils.hasText(searchDto.getCode())) {
					add(builder.equal(root.get("code"), searchDto.getCode()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
