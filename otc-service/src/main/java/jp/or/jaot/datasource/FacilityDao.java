package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.model.BaseSearchDto.CLASS_CONDITION;
import jp.or.jaot.model.dto.common.ClassificationSearchDto;
import jp.or.jaot.model.dto.common.RegionSearchDto;
import jp.or.jaot.model.entity.FacilityCategoryEntity;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.FacilityMemberEntity;
import jp.or.jaot.model.entity.TrainingSchoolOpenHistoryEntity;

/**
 * 施設養成校DAO(基底クラス)
 */
@Component
public abstract class FacilityDao<E> extends BaseDao<E> {

	/**
	 * 領域条件生成
	 * @param builder ビルダー
	 * @param root ルート
	 * @param region 領域検索DTO
	 * @return 領域条件
	 */
	protected Predicate createtRegionPredicate(CriteriaBuilder builder, Root<FacilityEntity> root,
			RegionSearchDto region) {

		// 条件
		List<Predicate> subPredicates = new ArrayList<Predicate>() {
			{
				for (ClassificationSearchDto classification : region.getClassifications()) {
					if (CLASS_CONDITION.OR.equals(classification.getCondition())) {
						add(builder.in(root.get("id")).value(createtClassificationSubquery(builder,
								region, classification, classification.getCodes()))); // 分類コード全体
					} else if (CLASS_CONDITION.NOT.equals(classification.getCondition())) {
						add(builder.not(builder.in(root.get("id")).value(createtClassificationSubquery(builder,
								region, classification, classification.getCodes())))); // 分類コード全体
					} else if (CLASS_CONDITION.AND.equals(classification.getCondition())) {
						for (String code : classification.getCodes()) {
							add(builder.and(builder.in(root.get("id")).value(createtClassificationSubquery(builder,
									region, classification, new String[] { code })))); // 分類コード毎
						}
					}
				}
			}
		};

		return builder.and(subPredicates.toArray(new Predicate[subPredicates.size()]));
	}

	/**
	 * 施設分類サブクエリ生成
	 * @param builder ビルダー
	 * @param region 領域検索DTO
	 * @param classification 分類検索DTO
	 * @param codes 分類コード配列
	 * @return 施設分類サブクエリ
	 */
	protected Subquery<FacilityCategoryEntity> createtClassificationSubquery(CriteriaBuilder builder,
			RegionSearchDto region, ClassificationSearchDto classification, String[] codes) {

		Class<FacilityCategoryEntity> entityClazz = FacilityCategoryEntity.class;
		Subquery<FacilityCategoryEntity> subQuery = builder.createQuery(entityClazz).subquery(entityClazz);
		Root<FacilityCategoryEntity> subRoot = subQuery.from(entityClazz);

		// 条件
		List<Predicate> codePredicates = createCodePredicates(builder, subRoot, region.getCategory(),
				classification.getDivision().getValue(), codes);

		return subQuery.select(subRoot.get("facilityId")).distinct(true)
				.where(builder.or(codePredicates.toArray(new Predicate[codePredicates.size()])));
	}

	/**
	 * 分類コード条件生成
	 * @param builder ビルダー
	 * @param subRoot サブルート
	 * @param category 分類種別
	 * @param categoryDivision 分類区分
	 * @param codes 分類コード配列
	 * @return 分類コード条件
	 */
	protected List<Predicate> createCodePredicates(CriteriaBuilder builder, Root<FacilityCategoryEntity> subRoot,
			Integer category, Integer categoryDivision, String[] codes) {

		// 条件
		List<Predicate> codePredicates = new ArrayList<Predicate>() {
			{
				for (String code : codes) {
					List<Predicate> predicates = new ArrayList<Predicate>() {
						{
							add(builder.equal(subRoot.get("category"), category)); // 分類種別
							add(builder.equal(subRoot.get("categoryDivision"), categoryDivision)); // 分類区分
							add(builder.equal(subRoot.get("categoryCode"), code)); // 分類コード
						}
					};
					add(builder.and(predicates.toArray(new Predicate[predicates.size()])));
				}
			}
		};

		return codePredicates;
	}

	/**
	 * 開設年度条件生成
	 * @param builder ビルダー
	 * @param root ルート
	 * @param from 開設年度(From)
	 * @param to 開設年度(To)
	 * @return 開設年度条件
	 */
	protected Predicate createtFiscalYearPredicate(CriteriaBuilder builder, Root<FacilityEntity> root,
			Integer from, Integer to) {

		Class<TrainingSchoolOpenHistoryEntity> entityClazz = TrainingSchoolOpenHistoryEntity.class;
		Subquery<TrainingSchoolOpenHistoryEntity> subQuery = builder.createQuery(entityClazz).subquery(entityClazz);
		Root<TrainingSchoolOpenHistoryEntity> subRoot = subQuery.from(entityClazz);

		// 条件
		Predicate subPredicate = null;
		if (from != null && to != null) {
			subPredicate = builder.between(subRoot.get("openFiscalYear"), from, to);
		} else if (from != null) {
			subPredicate = builder.greaterThanOrEqualTo(subRoot.get("openFiscalYear"), from);
		} else if (to != null) {
			subPredicate = builder.lessThanOrEqualTo(subRoot.get("openFiscalYear"), to);
		}

		return builder.in(root.get("id"))
				.value(subQuery.select(subRoot.get("facilityId")).distinct(true).where(subPredicate));
	}

	/**
	 * 施設養成校勤務者条件生成
	 * @param builder ビルダー
	 * @param root ルート
	 * @param selectSubquery 検索サブクエリ
	 * @return 施設養成校勤務者条件
	 */
	protected Predicate createtFacilityMemberPredicate(CriteriaBuilder builder, Root<FacilityEntity> root,
			Subquery<?> selectSubquery) {

		Class<FacilityMemberEntity> entityClazz = FacilityMemberEntity.class;
		Subquery<FacilityMemberEntity> subQuery = builder.createQuery(entityClazz).subquery(entityClazz);
		Root<FacilityMemberEntity> subRoot = subQuery.from(entityClazz);

		return builder.in(root.get("id"))
				.value(subQuery.select(subRoot.get("facilityId")).distinct(true)
						.where(builder.in(subRoot.get("memberNo")).value(selectSubquery)));
	}
}
