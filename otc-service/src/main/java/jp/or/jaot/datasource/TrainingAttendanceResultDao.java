package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.TrainingAttendanceResultSearchDto;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;

/**
 * 研修会出欠合否DAO
 */
@Component
public class TrainingAttendanceResultDao extends BaseDao<TrainingAttendanceResultEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<TrainingAttendanceResultEntity> findByCondition(TrainingAttendanceResultSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TrainingAttendanceResultEntity> query = builder
					.createQuery(TrainingAttendanceResultEntity.class);

			// FROM句
			Root<TrainingAttendanceResultEntity> root = query.from(TrainingAttendanceResultEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<TrainingAttendanceResultEntity> root,
			BaseSearchDto baseSearchDto) {

		// 研修会出欠合否検索DTO
		TrainingAttendanceResultSearchDto searchDto = (TrainingAttendanceResultSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 研修会番号
				if (!ArrayUtils.isEmpty(searchDto.getTrainingNos())) {
					add(createInPhrase(builder, root, "trainingNo", searchDto.getTrainingNos()));
				}

				// 申込者番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}

				// 氏名
				if (StringUtils.hasText(searchDto.getFullname())) {
					add(builder.like(root.get("fullname"), parMatch(searchDto.getFullname())));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
