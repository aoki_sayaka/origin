package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.OtherOrgPointApplicationAttachmentSearchDto;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;

/**
 * 他団体・SIGポイント添付ファイルDAO
 */
@Component
public class OtherOrgPointApplicationAttachmentDao extends BaseDao<OtherOrgPointApplicationAttachmentEntity> {

	/**
	 * ID検索
	 * @param id
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public OtherOrgPointApplicationAttachmentEntity findByID(Integer id) {
		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<OtherOrgPointApplicationAttachmentEntity> query = builder
					.createQuery(OtherOrgPointApplicationAttachmentEntity.class);

			// FROM句
			Root<OtherOrgPointApplicationAttachmentEntity> root = query.from(OtherOrgPointApplicationAttachmentEntity.class);

			// WHERE句
			Predicate where = builder.equal(root.get("id"), id);

			// 実行
			CriteriaQuery<OtherOrgPointApplicationAttachmentEntity> resultQuery = query.select(root).where(where);
			OtherOrgPointApplicationAttachmentEntity entity = getEntityManager().createQuery(resultQuery).getSingleResult();

			return entity;
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<OtherOrgPointApplicationAttachmentEntity> findByCondition(OtherOrgPointApplicationAttachmentSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<OtherOrgPointApplicationAttachmentEntity> query = builder
					.createQuery(OtherOrgPointApplicationAttachmentEntity.class);

			// FROM句
			Root<OtherOrgPointApplicationAttachmentEntity> root = query.from(OtherOrgPointApplicationAttachmentEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// 実行
			return getResultList(query.select(root).where(where), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<OtherOrgPointApplicationAttachmentEntity> root,
			OtherOrgPointApplicationAttachmentSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (searchDto.getId() != null) {
					add(builder.equal(root.get("id"), searchDto.getId()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
