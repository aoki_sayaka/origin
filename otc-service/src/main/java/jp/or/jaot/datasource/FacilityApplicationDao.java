package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.FacilityApplicationSearchDto;
import jp.or.jaot.model.entity.FacilityApplicationEntity;

/**
 * 施設登録申請DAO
 */
@Component
public class FacilityApplicationDao extends BaseDao<FacilityApplicationEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<FacilityApplicationEntity> findByCondition(FacilityApplicationSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FacilityApplicationEntity> query = builder.createQuery(FacilityApplicationEntity.class);

			// FROM句
			Root<FacilityApplicationEntity> root = query.from(FacilityApplicationEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<FacilityApplicationEntity> root,
			BaseSearchDto baseSearchDto) {

		// 施設登録申請検索DTO
		FacilityApplicationSearchDto searchDto = (FacilityApplicationSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (!ArrayUtils.isEmpty(searchDto.getMemberNos())) {
					add(createInPhrase(builder, root, "memberNo", searchDto.getMemberNos()));
				}

				// 仮会員番号
				if (!ArrayUtils.isEmpty(searchDto.getTemporaryMemberNos())) {
					add(createInPhrase(builder, root, "temporaryMemberNo", searchDto.getTemporaryMemberNos()));
				}

				// 申請ステータス
				if (!ArrayUtils.isEmpty(searchDto.getStatuses())) {
					add(createInPhrase(builder, root, "status", searchDto.getStatuses()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
