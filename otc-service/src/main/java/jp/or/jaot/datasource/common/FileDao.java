package jp.or.jaot.datasource.common;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.util.FileUtil;
import jp.or.jaot.model.entity.common.FileEntity;

/**
 * ファイルDAO
 */
@Component
public class FileDao {

	/**
	 * アップロード
	 * @param entity ファイルエンティティ
	 */
	public void upload(FileEntity entity) {
		FileUtil.uploadFile(entity.getPath(), entity.getName(), entity.getMultipartFile());
	}
}
