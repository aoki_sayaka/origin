package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.FacilityAdminSearchDto;
import jp.or.jaot.model.entity.FacilityEntity;

/**
 * 施設養成校(事務局サイト)
 */
@Component
public class FacilityAdminDao extends FacilityDao<FacilityEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<FacilityEntity> findByCondition(FacilityAdminSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FacilityEntity> query = builder.createQuery(FacilityEntity.class);

			// FROM句
			Root<FacilityEntity> root = query.from(FacilityEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<FacilityEntity> root, BaseSearchDto baseSearchDto) {

		// 施設養成校検索DTO
		FacilityAdminSearchDto searchDto = (FacilityAdminSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 施設養成校名(漢字)
				if (StringUtils.hasText(searchDto.getName())) {
					add(builder.like(root.get("name"), parMatch(searchDto.getName())));
				}

				// 施設養成校名(カナ)
				if (StringUtils.hasText(searchDto.getNameKana())) {
					add(builder.like(root.get("nameKana"), parMatch(searchDto.getNameKana())));
				}

				// 都道府県コード
				if (!ArrayUtils.isEmpty(searchDto.getPrefectureCds())) {
					add(createInPhrase(builder, root, "prefectureCd", searchDto.getPrefectureCds()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
