package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.dto.search.HandbookMigrationApplicationSearchDto;
import jp.or.jaot.model.entity.HandbookMigrationApplicationEntity;

@Component
public class HandbookMigrationApplicationDao extends BaseDao<HandbookMigrationApplicationEntity> {

	/**
	 * 手帳移行申請検索
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public HandbookMigrationApplicationEntity findByMemberNo(Integer memberNo) {
		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<HandbookMigrationApplicationEntity> query = builder
					.createQuery(HandbookMigrationApplicationEntity.class);

			// FROM句
			Root<HandbookMigrationApplicationEntity> root = query.from(HandbookMigrationApplicationEntity.class);

			// WHERE句
			Predicate where = builder.equal(root.get("memberNo"), memberNo);

			// 実行
			CriteriaQuery<HandbookMigrationApplicationEntity> resultQuery = query.select(root).where(where);
			HandbookMigrationApplicationEntity entity = getEntityManager().createQuery(resultQuery).getSingleResult();

			return entity;
		} catch (NoResultException e) {
			return null; // 該当データなし
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<HandbookMigrationApplicationEntity> findByCondition(HandbookMigrationApplicationSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<HandbookMigrationApplicationEntity> query = builder
					.createQuery(HandbookMigrationApplicationEntity.class);

			// FROM句
			Root<HandbookMigrationApplicationEntity> root = query.from(HandbookMigrationApplicationEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// 実行
			return getResultList(query.select(root).where(where), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<HandbookMigrationApplicationEntity> root,
			HandbookMigrationApplicationSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				/*
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}
				*/

				// TODO : 検索条件追加
				// 申請日
				Date fromApplicationDate = DateUtil.getFromDate(searchDto.getApplicationDateFrom());
				Date toApplicationDate = DateUtil.getToDate(searchDto.getApplicationDateTo());
				if (fromApplicationDate != null && toApplicationDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.between(root.get("applicationDate"), fromApplicationDate, toApplicationDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (fromApplicationDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.greaterThanOrEqualTo(root.get("applicationDate"), fromApplicationDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (toApplicationDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.lessThanOrEqualTo(root.get("applicationDate"), toApplicationDate));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				}
				
				// 申請状態
				if (StringUtils.hasText(searchDto.getApplicationStatusCd())) {
					add(builder.equal(root.get("statusCd"), searchDto.getApplicationStatusCd()));
				}
				
				// 申請者 会員番号
				Integer fromMemberNo = searchDto.getMemberNoFrom();
				Integer toMemberNo = searchDto.getMemberNoTo();
				if (fromMemberNo != null && toMemberNo != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.between(root.get("memberNo"), fromMemberNo, toMemberNo));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (fromMemberNo != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.equal(root.get("memberNo"), fromMemberNo));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (toMemberNo != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.equal(root.get("memberNo"), toMemberNo));
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				}
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
