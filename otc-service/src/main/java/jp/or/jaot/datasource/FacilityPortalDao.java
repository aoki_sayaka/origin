package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.common.RegionSearchDto;
import jp.or.jaot.model.dto.search.FacilityPortalSearchDto;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;

/**
 * 施設養成校DAO(会員ポータル)
 */
@Component
public class FacilityPortalDao extends FacilityDao<FacilityEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<FacilityEntity> findByCondition(FacilityPortalSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FacilityEntity> query = builder.createQuery(FacilityEntity.class);

			// FROM句
			Root<FacilityEntity> root = query.from(FacilityEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<FacilityEntity> root, BaseSearchDto baseSearchDto) {

		// 施設養成校検索DTO
		FacilityPortalSearchDto searchDto = (FacilityPortalSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 施設番号
				if (StringUtils.hasText(searchDto.getFacilityNo())) {
					add(builder.equal(root.get("facilityNo"), searchDto.getFacilityNo()));
				}

				// 施設養成校名(漢字)
				if (StringUtils.hasText(searchDto.getName())) {
					add(builder.like(root.get("name"), parMatch(searchDto.getName())));
				}

				// 施設養成校名(カナ)
				if (StringUtils.hasText(searchDto.getNameKana())) {
					add(builder.like(root.get("nameKana"), parMatch(searchDto.getNameKana())));
				}

				// 都道府県コード
				if (!ArrayUtils.isEmpty(searchDto.getPrefectureCds())) {
					add(createInPhrase(builder, root, "prefectureCd", searchDto.getPrefectureCds()));
				}

				// 施設区分
				if (!ArrayUtils.isEmpty(searchDto.getFacilityCategories())) {
					add(createInPhrase(builder, root, "facilityCategory", searchDto.getFacilityCategories()));
				}

				// 養成校種別
				if (!ArrayUtils.isEmpty(searchDto.getTrainingSchoolCategoryCds())) {
					add(createInPhrase(builder, root, "trainingSchoolCategoryCd",
							searchDto.getTrainingSchoolCategoryCds()));
				}

				// 開設者種別
				if (!ArrayUtils.isEmpty(searchDto.getOpenerTypeCds())) {
					add(createInPhrase(builder, root, "openerTypeCd", searchDto.getOpenerTypeCds()));
				}

				// 住所
				if (StringUtils.hasText(searchDto.getAddress())) {
					List<Predicate> addressPredicates = new ArrayList<Predicate>() {
						{
							add(builder.like(root.get("address1"), parMatch(searchDto.getAddress())));
							add(builder.like(root.get("address2"), parMatch(searchDto.getAddress())));
							add(builder.like(root.get("address3"), parMatch(searchDto.getAddress())));
						}
					};
					add(builder.or(addressPredicates.toArray(new Predicate[addressPredicates.size()])));
				}

				// 領域
				if (!ArrayUtils.isEmpty(searchDto.getRegions())) {
					for (RegionSearchDto region : searchDto.getRegions()) {
						if (region.getCategory() != null // 分類種別あり
								&& BooleanUtils.isTrue(region.getIsApplicable()) // 該当あり 
								&& !ArrayUtils.isEmpty(region.getClassifications())) { // 分類コードあり
							add(createtRegionPredicate(builder, root, region));
						}
					}
				}

				// 英文表記
				if (StringUtils.hasText(searchDto.getEnglishNotation())) {
					add(builder.like(root.get("englishNotation"), parMatch(searchDto.getEnglishNotation())));
				}

				// 課程年限
				if (StringUtils.hasText(searchDto.getCourseYears())) {
					add(builder.equal(root.get("courseYears"), searchDto.getCourseYears()));
				}

				// 修士課程有無
				if (StringUtils.hasText(searchDto.getMasterCourseFlag())) {
					add(builder.equal(root.get("masterCourseFlag"), searchDto.getMasterCourseFlag()));
				}

				// 博士課程有無
				if (StringUtils.hasText(searchDto.getDoctorCourseFlag())) {
					add(builder.equal(root.get("doctorCourseFlag"), searchDto.getDoctorCourseFlag()));
				}

				// 昼間・夜間フラグ
				if (StringUtils.hasText(searchDto.getDayNightFlag())) {
					add(builder.equal(root.get("dayNightFlag"), searchDto.getDayNightFlag()));
				}

				// 開設年度
				Integer openFiscalYearFrom = searchDto.getOpenFiscalYearFrom();
				Integer openFiscalYearTo = searchDto.getOpenFiscalYearTo();
				if (openFiscalYearFrom != null || openFiscalYearTo != null) {
					add(createtFiscalYearPredicate(builder, root, openFiscalYearFrom, openFiscalYearTo));
				}

				// 会員在籍
				if (searchDto.getCountMember() != null) {
					add(createtRangePredicate(builder, root, "countMember", searchDto.getCountMember(),
							searchDto.getCountMemberRC()));
				}

				// 免許取得３年以上
				if (searchDto.getCountLicense3year() != null) {
					add(createtRangePredicate(builder, root, "countLicense3year", searchDto.getCountLicense3year(),
							searchDto.getCountLicense3yearRC()));
				}

				// 免許取得５年以上
				if (searchDto.getCountLicense5year() != null) {
					add(createtRangePredicate(builder, root, "countLicense5year", searchDto.getCountLicense5year(),
							searchDto.getCountLicense5yearRC()));
				}

				// 臨床実習指導施設認定状況
				if (!ArrayUtils.isEmpty(searchDto.getClinicalTrainingFacilityCertifiedStatuses())) {
					// TODO(該当するエンティティが存在しない(臨床実習指導施設認定) : 2019/9/24(rmatu))
				}

				// WFOT認定状況
				if (!ArrayUtils.isEmpty(searchDto.getWfotCertifiedStatuses())) {
					add(createInPhrase(builder, root, "wfotCertifiedStatus", searchDto.getWfotCertifiedStatuses()));
				}

				// MTDLP推進協力校
				if (searchDto.getMtdlpCooperatorFlag() != null) {
					add(builder.equal(root.get("mtdlpCooperatorFlag"), searchDto.getMtdlpCooperatorFlag()));
				}

				// 基礎研修修了者在籍有無
				if (BooleanUtils.isTrue(searchDto.getIsRegistBasicOtQualify())) {
					add(createtFacilityMemberPredicate(builder, root,
							createtSelectSingleSubquery(builder, BasicOtQualifyHistoryEntity.class, "memberNo")));
				}

				// 認定作業療法士在籍有無
				if (BooleanUtils.isTrue(searchDto.getIsRegistQualificationOtAttending())) {
					add(createtFacilityMemberPredicate(builder, root,
							createtSelectSingleSubquery(builder, QualifiedOtCompletionHistoryEntity.class,
									"memberNo")));
				}

				// 専門作業療法士在籍分野
				if (!ArrayUtils.isEmpty(searchDto.getProfessionalOtAttendingFields())) {

					// WHERE句(専門分野)
					Subquery<?> subQuery = createtSelectSingleSubquery(
							builder, ProfessionalOtQualifyHistoryEntity.class, "memberNo");
					Predicate subWhere = createInPhrase(builder, subQuery.getRoots().iterator().next(),
							"professionalField", searchDto.getProfessionalOtAttendingFields());

					// 追加
					add(createtFacilityMemberPredicate(builder, root, subQuery.where(subWhere)));
				}

				// 臨床実習指導者研修修了者在籍有無
				if (BooleanUtils.isTrue(searchDto.getIsRegistClinicalTrainingLeaderTraining())) {
					add(createtFacilityMemberPredicate(builder, root,
							createtSelectSingleSubquery(builder, ClinicalTrainingLeaderSummaryEntity.class,
									"memberNo")));
				}

				// 臨床実習指導者講習受講者在籍有無
				if (BooleanUtils.isTrue(searchDto.getIsRegistSessionAttendQualification())) {
					add(createtFacilityMemberPredicate(builder, root,
							createtSelectSingleSubquery(builder, ClinicalTrainingLeaderTrainingHistoryEntity.class,
									"memberNo")));
				}

				// MTDLP基礎研修修了者在籍有無
				if (BooleanUtils.isTrue(searchDto.getIsRegistMtdlpBasicAttending())) {
					// TODO(基礎研修の判定方法が不明 : 2019/9/24(rmatu))
					add(createtFacilityMemberPredicate(builder, root,
							createtSelectSingleSubquery(builder, MtdlpTrainingHistoryEntity.class, "memberNo")));
				}

				// MTDLP研修修了者在籍有無
				if (BooleanUtils.isTrue(searchDto.getIsRegistMtdlpTraining())) {
					add(createtFacilityMemberPredicate(builder, root,
							createtSelectSingleSubquery(builder, MtdlpTrainingHistoryEntity.class, "memberNo")));
				}

				// MTDLP認定者在籍有無
				if (BooleanUtils.isTrue(searchDto.getIsRegistMtdlpTrainingLeader())) {
					add(createtFacilityMemberPredicate(builder, root,
							createtSelectSingleSubquery(builder, MtdlpQualifyHistoryEntity.class, "memberNo")));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
