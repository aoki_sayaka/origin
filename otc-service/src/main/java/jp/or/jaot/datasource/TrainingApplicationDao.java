package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.TrainingApplicationSearchDto;
import jp.or.jaot.model.entity.TrainingApplicationEntity;

/**
 * 研修会申込DAO
 */
@Component
public class TrainingApplicationDao extends BaseDao<TrainingApplicationEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<TrainingApplicationEntity> findByCondition(TrainingApplicationSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TrainingApplicationEntity> query = builder.createQuery(TrainingApplicationEntity.class);

			// FROM句
			Root<TrainingApplicationEntity> root = query.from(TrainingApplicationEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<TrainingApplicationEntity> root,
			BaseSearchDto baseSearchDto) {

		// 研修会申込検索DTO
		TrainingApplicationSearchDto searchDto = (TrainingApplicationSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 研修会番号
				if (!ArrayUtils.isEmpty(searchDto.getTrainingNos())) {
					add(createInPhrase(builder, root, "trainingNo", searchDto.getTrainingNos()));
				}

				// 申込者番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}

				// 氏名
				if (StringUtils.hasText(searchDto.getFullname())) {
					add(builder.like(root.get("fullname"), parMatch(searchDto.getFullname())));
				}

				// 勤務先
				if (StringUtils.hasText(searchDto.getWorkPlaceNm())) {
					add(builder.like(root.get("workPlaceNm"), parMatch(searchDto.getWorkPlaceNm())));
				}

				// 勤務先都道府県
				if (!ArrayUtils.isEmpty(searchDto.getWorkPlacePrefectureCds())) {
					add(createInPhrase(builder, root, "workPlacePrefectureCd", searchDto.getWorkPlacePrefectureCds()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
