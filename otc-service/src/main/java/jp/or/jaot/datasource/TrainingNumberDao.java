package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.TrainingNumberSearchDto;
import jp.or.jaot.model.entity.TrainingNumberEntity;

@Component
public class TrainingNumberDao extends BaseDao<TrainingNumberEntity> {

	/**
	 * 会員番号検索
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public TrainingNumberEntity findByFiscalYear(Integer fiscalYear) {
		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TrainingNumberEntity> query = builder
					.createQuery(TrainingNumberEntity.class);

			// FROM句
			Root<TrainingNumberEntity> root = query.from(TrainingNumberEntity.class);

			// WHERE句
			Predicate where = builder.equal(root.get("fiscalYear"), fiscalYear);

			// 実行
			CriteriaQuery<TrainingNumberEntity> resultQuery = query.select(root).where(where);
			TrainingNumberEntity entity = getEntityManager().createQuery(resultQuery).getSingleResult();

			return entity;
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<TrainingNumberEntity> findByByCondition(TrainingNumberSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TrainingNumberEntity> query = builder
					.createQuery(TrainingNumberEntity.class);

			// FROM句
			Root<TrainingNumberEntity> root = query.from(TrainingNumberEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// 実行
			return getResultList(query.select(root).where(where), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<TrainingNumberEntity> root,
			TrainingNumberSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (searchDto.getFiscalYear() != null) {
					add(builder.equal(root.get("fiscalYear"), searchDto.getFiscalYear()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
