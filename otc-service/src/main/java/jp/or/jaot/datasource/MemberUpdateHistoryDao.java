package jp.or.jaot.datasource;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.MemberUpdateHistorySearchDto;
import jp.or.jaot.model.entity.MemberUpdateHistoryEntity;

/**
 * 会員更新履歴DAO
 */
@Component
public class MemberUpdateHistoryDao extends BaseDao<MemberUpdateHistoryEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<MemberUpdateHistoryEntity> findByCondition(MemberUpdateHistorySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<MemberUpdateHistoryEntity> query = builder.createQuery(MemberUpdateHistoryEntity.class);

			// FROM句
			Root<MemberUpdateHistoryEntity> root = query.from(MemberUpdateHistoryEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<MemberUpdateHistoryEntity> root,
			MemberUpdateHistorySearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}

				// 更新区分コード
				if (searchDto.getUpdateCategoryCd() != null) {
					add(builder.equal(root.get("updateCategoryCd"), searchDto.getUpdateCategoryCd()));
				}

				// テーブルコード
				if (searchDto.getTableCd() != null) {
					add(builder.equal(root.get("tableCd"), searchDto.getTableCd()));
				}

				// 項目コード(複数)
				if (!ArrayUtils.isEmpty(searchDto.getFieldCds())) {
					add(root.get("fieldCd").in(Arrays.asList(searchDto.getFieldCds())));
				}

				// 更新日
				Timestamp fromDate = getFromTimestamp(searchDto.getUpdateFromDate());
				Timestamp toDate = getToTimestamp(searchDto.getUpdateToDate());
				if (fromDate != null && toDate != null) {
					add(builder.between(root.get("updateDate"), fromDate, toDate));
				} else if (fromDate != null) {
					add(builder.greaterThanOrEqualTo(root.get("updateDate"), fromDate));
				} else if (toDate != null) {
					add(builder.lessThanOrEqualTo(root.get("updateDate"), toDate));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
