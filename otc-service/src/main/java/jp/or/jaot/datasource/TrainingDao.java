package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.dto.search.TrainingSearchDto;
import jp.or.jaot.model.entity.TrainingEntity;
import jp.or.jaot.model.entity.TrainingOrganizerEntity;

/**
 * 研修会DAO
 */
@Component
public class TrainingDao extends BaseDao<TrainingEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<TrainingEntity> findByCondition(TrainingSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<TrainingEntity> query = builder.createQuery(TrainingEntity.class);

			// FROM句
			Root<TrainingEntity> root = query.from(TrainingEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					addAll(getOrderList(builder, root, searchDto)); // 指定:ソート順序
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<TrainingEntity> root, BaseSearchDto baseSearchDto) {

		// 研修会検索DTO
		TrainingSearchDto searchDto = (TrainingSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				if (searchDto.isUseDeleted()) {
					add(builder.equal(root.get("deleted"), false));
				}

				// 研修会番号
				if (!ArrayUtils.isEmpty(searchDto.getTrainingNos())) {
					// 複数
					add(createInPhrase(builder, root, "trainingNo", searchDto.getTrainingNos()));
				} else if (StringUtils.hasText(searchDto.getTrainingNo())) {
					// 単独
					if (BaseSearchDto.MATCH_CONDITION.FWD.equals(searchDto.getMatchCondTrainingNo())) {
						add(builder.like(root.get("trainingNo"), fwdMatch(searchDto.getTrainingNo()))); // 前方一致
					} else {
						add(builder.equal(root.get("trainingNo"), searchDto.getTrainingNo())); // 完全一致
					}
				}

				// 開催年度
				if (searchDto.getHoldFiscalYear() != null) {
					add(builder.equal(root.get("holdFiscalYear"), searchDto.getHoldFiscalYear()));
				}

				// 主催者
				if (!ArrayUtils.isEmpty(searchDto.getOrganizerCds())) {

					// サブクエリ生成(主催者コードに該当する研修会主催者を検索)
					Class<TrainingOrganizerEntity> entityClazz = TrainingOrganizerEntity.class;
					Subquery<TrainingOrganizerEntity> subQuery = builder.createQuery(entityClazz).subquery(entityClazz);
					Root<TrainingOrganizerEntity> subRoot = subQuery.from(entityClazz);
					Predicate subWhere = createInPhrase(builder, subRoot, "organizerCd", searchDto.getOrganizerCds());

					// 条件追加
					add(builder.in(root.get("trainingNo"))
							.value(subQuery.where(subWhere).select(subRoot.get("trainingNo")).distinct(true)));
				}

				// 研修会種別
				if (!ArrayUtils.isEmpty(searchDto.getTrainingTypeCds())) {
					add(createInPhrase(builder, root, "trainingTypeCd", searchDto.getTrainingTypeCds()));
				}

				// 講座分類
				if (!ArrayUtils.isEmpty(searchDto.getCourseCategoryCds())) {
					add(createInPhrase(builder, root, "courseCategoryCd", searchDto.getCourseCategoryCds()));
				}

				// 講座名
				if (StringUtils.hasText(searchDto.getCourseNm())) {
					add(builder.like(root.get("courseNm"), parMatch(searchDto.getCourseNm())));
				}

				// 受付状況
				if (!ArrayUtils.isEmpty(searchDto.getReceptionStatusCds())) {
					add(createInPhrase(builder, root, "receptionStatusCd", searchDto.getReceptionStatusCds()));
				}

				// 更新状況
				if (!ArrayUtils.isEmpty(searchDto.getUpdateStatusCds())) {
					add(createInPhrase(builder, root, "updateStatusCd", searchDto.getUpdateStatusCds()));
				}

				// 開催地
				if (!ArrayUtils.isEmpty(searchDto.getVenuePrefCds())) {
					add(createInPhrase(builder, root, "venuePrefCd", searchDto.getVenuePrefCds()));
				}

				// 開催日
				Date fromDate = DateUtil.getFromDate(searchDto.getEventDateFrom());
				Date toDate = DateUtil.getToDate(searchDto.getEventDateTo());
				if (fromDate != null && toDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.between(root.get("eventDate1"), fromDate, toDate)); // 開催日1
							add(builder.between(root.get("eventDate2"), fromDate, toDate)); // 開催日2
							add(builder.between(root.get("eventDate3"), fromDate, toDate)); // 開催日3
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (fromDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.greaterThanOrEqualTo(root.get("eventDate1"), fromDate)); // 開催日1
							add(builder.greaterThanOrEqualTo(root.get("eventDate2"), fromDate)); // 開催日2
							add(builder.greaterThanOrEqualTo(root.get("eventDate3"), fromDate)); // 開催日3
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				} else if (toDate != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							add(builder.lessThanOrEqualTo(root.get("eventDate1"), toDate)); // 開催日1
							add(builder.lessThanOrEqualTo(root.get("eventDate2"), toDate)); // 開催日2
							add(builder.lessThanOrEqualTo(root.get("eventDate3"), toDate)); // 開催日3
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				}

				// e-learningフラグ(チェックされている場合のみ対象)
				if (searchDto.getElearningFlag() != null && searchDto.getElearningFlag()) {
					add(builder.equal(root.get("elearningFlag"), searchDto.getElearningFlag()));
				}

				// 終了フラグ(チェックされている場合のみ対象)
				if (searchDto.getEndFlag() != null && searchDto.getEndFlag()) {
					add(builder.equal(root.get("endFlag"), searchDto.getEndFlag()));
				}

				// キャンセルフラグ(チェックされている場合のみ対象)
				if (searchDto.getCancelFlag() != null && searchDto.getCancelFlag() != null) {
					add(builder.equal(root.get("cancelFlag"), searchDto.getCancelFlag()));
				}

				// 通知
				// TODO

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
