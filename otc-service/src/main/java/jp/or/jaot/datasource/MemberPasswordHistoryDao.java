package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.MemberPasswordHistorySearchDto;
import jp.or.jaot.model.entity.MemberPasswordHistoryEntity;

/**
 * 会員パスワード履歴DAO
 */
@Component
public class MemberPasswordHistoryDao extends BaseDao<MemberPasswordHistoryEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<MemberPasswordHistoryEntity> findByCondition(MemberPasswordHistorySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<MemberPasswordHistoryEntity> query = builder.createQuery(MemberPasswordHistoryEntity.class);

			// FROM句
			Root<MemberPasswordHistoryEntity> root = query.from(MemberPasswordHistoryEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = getOrder(builder, root, searchDto);

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}
	
	private List<Order> getOrder(CriteriaBuilder builder, Root<MemberPasswordHistoryEntity> root,
			MemberPasswordHistorySearchDto searchDto) {
		Order defaultOrder = builder.asc(root.get("createDatetime")); // 昇順:作成日時
		return searchDto.getOrders().size() == 0 ? new ArrayList<Order>() {
			{
				add(defaultOrder);
			}
		} : searchDto.getOrders().stream().map((searchOrder) -> {
			switch (searchOrder.getAsc()) {
			case ASC:
				return builder.asc(root.get(searchOrder.getColumn()));
			case DESC:
				return builder.desc(root.get(searchOrder.getColumn()));
			default:
				return defaultOrder;
			}
		}).collect(Collectors.toList());
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<MemberPasswordHistoryEntity> root,
			MemberPasswordHistorySearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}

				// 仮パスワード
				if (searchDto.getTempPassword() != null) {
					add(builder.equal(root.get("tempPassword"), searchDto.getTempPassword()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}

}
