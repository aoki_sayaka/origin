package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.HistoryTableDefinitionSearchDto;
import jp.or.jaot.model.entity.HistoryTableDefinitionEntity;

/**
 * 履歴テーブル定義DAO
 */
@Component
public class HistoryTableDefinitionDao extends BaseDao<HistoryTableDefinitionEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<HistoryTableDefinitionEntity> findByCondition(HistoryTableDefinitionSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<HistoryTableDefinitionEntity> query = builder.createQuery(HistoryTableDefinitionEntity.class);

			// FROM句
			Root<HistoryTableDefinitionEntity> root = query.from(HistoryTableDefinitionEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("tableCd"))); // 昇順:テーブルコード
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<HistoryTableDefinitionEntity> root,
			HistoryTableDefinitionSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// テーブル名(物理)
				if (!StringUtils.isEmpty(searchDto.getTablePhysicalNm())) {
					add(builder.equal(root.get("tablePhysicalNm"), searchDto.getTablePhysicalNm()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
