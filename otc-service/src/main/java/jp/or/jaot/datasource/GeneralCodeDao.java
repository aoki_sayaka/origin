package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.GeneralCodeSearchDto;
import jp.or.jaot.model.entity.GeneralCodeEntity;
import jp.or.jaot.model.entity.GeneralCodeId;

/**
 * 汎用コードDAO
 */
@Component
public class GeneralCodeDao extends BaseDao<GeneralCodeEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<GeneralCodeEntity> findByCondition(GeneralCodeSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<GeneralCodeEntity> query = builder.createQuery(GeneralCodeEntity.class);

			// FROM句
			Root<GeneralCodeEntity> root = query.from(GeneralCodeEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("generalCodeId").get("id"))); // 昇順:コードID
					add(builder.asc(root.get("sortOrder"))); // 昇順:表示順序
					add(builder.asc(root.get("generalCodeId").get("code"))); // 昇順:コード値
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<GeneralCodeEntity> root, GeneralCodeSearchDto searchDto) {

		GeneralCodeId[] generalCodeIds = searchDto.getGeneralCodeIds();
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 汎用コードID
				if (!ArrayUtils.isEmpty(generalCodeIds)) {
					List<Predicate> codePredicates = new ArrayList<Predicate>() {
						{
							for (GeneralCodeId generalCodeId : generalCodeIds) {
								Predicate pCodeId = builder.equal(root.get("generalCodeId").get("id"),
										generalCodeId.getId()); // コードID
								Predicate pCode = builder.equal(root.get("generalCodeId").get("code"),
										generalCodeId.getCode()); // コード値
								if (StringUtils.hasText(generalCodeId.getId())
										&& StringUtils.hasText(generalCodeId.getCode())) {
									add(builder.and(pCodeId, pCode));
								} else if (StringUtils.hasText(generalCodeId.getId())) {
									add(pCodeId);
								} else if (StringUtils.hasText(generalCodeId.getCode())) {
									add(pCode);
								}
							}
						}
					};
					if (!CollectionUtils.isEmpty(codePredicates)) {
						add(builder.or(codePredicates.toArray(new Predicate[codePredicates.size()])));
					}
				}
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}

	// TODO
}
