package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.ClinicalTrainingLeaderSummarySearchDto;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;

/**
 * 臨床実習指導者履歴DAO
 */
@Component
public class ClinicalTrainingLeaderSummaryDao extends BaseDao<ClinicalTrainingLeaderSummaryEntity> {

	/**
	 * 会員番号検索
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public ClinicalTrainingLeaderSummaryEntity findByMemberNo(Integer memberNo) {
		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<ClinicalTrainingLeaderSummaryEntity> query = builder
					.createQuery(ClinicalTrainingLeaderSummaryEntity.class);

			// FROM句
			Root<ClinicalTrainingLeaderSummaryEntity> root = query.from(ClinicalTrainingLeaderSummaryEntity.class);

			// WHERE句
			Predicate where = builder.equal(root.get("memberNo"), memberNo);

			// 実行
			CriteriaQuery<ClinicalTrainingLeaderSummaryEntity> resultQuery = query.select(root).where(where);
			ClinicalTrainingLeaderSummaryEntity entity = getEntityManager().createQuery(resultQuery).getSingleResult();

			return entity;
		} catch (NoResultException e) {
			return null; // 該当データなし
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<ClinicalTrainingLeaderSummaryEntity> findByCondition(
			ClinicalTrainingLeaderSummarySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<ClinicalTrainingLeaderSummaryEntity> query = builder
					.createQuery(ClinicalTrainingLeaderSummaryEntity.class);

			// FROM句
			Root<ClinicalTrainingLeaderSummaryEntity> root = query.from(ClinicalTrainingLeaderSummaryEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<ClinicalTrainingLeaderSummaryEntity> root,
			ClinicalTrainingLeaderSummarySearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
