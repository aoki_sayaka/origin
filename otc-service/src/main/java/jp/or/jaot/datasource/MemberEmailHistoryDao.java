package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.MemberEmailHistorySearchDto;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;


/**
 * 会員メールアドレス履歴DAO
 */
@Component
public class MemberEmailHistoryDao  extends BaseDao<MemberEmailHistoryEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<MemberEmailHistoryEntity> findByCondition(MemberEmailHistorySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<MemberEmailHistoryEntity> query = builder.createQuery(MemberEmailHistoryEntity.class);

			// FROM句
			Root<MemberEmailHistoryEntity> root = query.from(MemberEmailHistoryEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = getOrder(builder, root, searchDto);

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	private List<Order> getOrder(CriteriaBuilder builder, Root<MemberEmailHistoryEntity> root,
			MemberEmailHistorySearchDto searchDto) {
		Order defaultOrder = builder.asc(root.get("createDatetime")); // 昇順:作成日時
		return searchDto.getOrders().size() == 0 ? new ArrayList<Order>() {
			{
				add(defaultOrder);
			}
		} : searchDto.getOrders().stream().map((searchOrder) -> {
			switch (searchOrder.getAsc()) {
			case ASC:
				return builder.asc(root.get(searchOrder.getColumn()));
			case DESC:
				return builder.desc(root.get(searchOrder.getColumn()));
			default:
				return defaultOrder;
			}
		}).collect(Collectors.toList());
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<MemberEmailHistoryEntity> root,
			MemberEmailHistorySearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}

				// 仮メールアドレスキー値
				if (searchDto.getTempKey() != null) {
					add(builder.equal(root.get("tempKey"), searchDto.getTempKey()));
				}

				// キー値
				if (searchDto.getTempFlag() != null) {
					add(builder.equal(root.get("tempFlag"), searchDto.getTempFlag()));
				}

				// 有効期限
				if (searchDto.getTempExpirationDatetime() != null) {
					add(builder.greaterThan(root.get("tempExpirationDatetime"), searchDto.getTempExpirationDatetime()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
