package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.FacilityCategorySearchDto;
import jp.or.jaot.model.entity.FacilityCategoryEntity;

/**
 * 施設分類DAO
 */
@Component
public class FacilityCategoryDao extends BaseDao<FacilityCategoryEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<FacilityCategoryEntity> findByCondition(FacilityCategorySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<FacilityCategoryEntity> query = builder.createQuery(FacilityCategoryEntity.class);

			// FROM句
			Root<FacilityCategoryEntity> root = query.from(FacilityCategoryEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<FacilityCategoryEntity> root,
			FacilityCategorySearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 施設養成校ID
				if (!ArrayUtils.isEmpty(searchDto.getFacilityIds())) {
					add(createInPhrase(builder, root, "facilityId", searchDto.getFacilityIds()));
				}

				// 分類種別
				if (searchDto.getCategory() != null) {
					add(builder.equal(root.get("category"), searchDto.getCategory()));
				}

				// 分類区分
				if (searchDto.getCategoryDivision() != null) {
					add(builder.equal(root.get("categoryDivision"), searchDto.getCategoryDivision()));
				}

				// 分類コード
				if (!StringUtils.isEmpty(searchDto.getCategoryCode())) {
					add(builder.equal(root.get("categoryCode"), searchDto.getCategoryCode()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
