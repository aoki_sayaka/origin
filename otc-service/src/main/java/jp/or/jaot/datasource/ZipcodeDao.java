package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.ZipcodeSearchDto;
import jp.or.jaot.model.entity.ZipcodeEntity;

/**
 * 郵便番号DAO
 */
@Component
public class ZipcodeDao extends BaseDao<ZipcodeEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<ZipcodeEntity> findByCondition(ZipcodeSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<ZipcodeEntity> query = builder.createQuery(ZipcodeEntity.class);

			// FROM句
			Root<ZipcodeEntity> root = query.from(ZipcodeEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("zipcode"))); // 昇順:郵便番号
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<ZipcodeEntity> root, BaseSearchDto baseSearchDto) {

		// 郵便番号検索DTO
		ZipcodeSearchDto searchDto = (ZipcodeSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 郵便番号
				if (StringUtils.hasText(searchDto.getZipcode())) {
					add(builder.like(root.get("zipcode"), fwdMatch(searchDto.getZipcode())));
				}

				// 住所
				if (StringUtils.hasText(searchDto.getAddress())) {
					add(builder.like(root.get("address"), parMatch(searchDto.getAddress())));
				}

				// 都道府県番号
				if (StringUtils.hasText(searchDto.getPrefectureCd())) {
					add(builder.equal(root.get("prefectureCd"), searchDto.getPrefectureCd()));
				}

				// 都道府県名
				if (StringUtils.hasText(searchDto.getPrefectureName())) {
					add(builder.equal(root.get("prefectureName"), searchDto.getPrefectureName()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
