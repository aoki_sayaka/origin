package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.MemberSearchDto;
import jp.or.jaot.model.entity.MemberEntity;

/**
 * 会員DAO
 */
@Component
public class MemberDao extends BaseDao<MemberEntity> {

	/**
	 * 会員番号検索
	 * @param memberNo 会員番号
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public MemberEntity findByMemberNo(Integer memberNo) {
		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<MemberEntity> query = builder.createQuery(MemberEntity.class);

			// FROM句
			Root<MemberEntity> root = query.from(MemberEntity.class);

			// WHERE句
			Predicate where = builder.equal(root.get("memberNo"), memberNo);

			// 実行
			CriteriaQuery<MemberEntity> resultQuery = query.select(root).where(where);
			MemberEntity entity = getEntityManager().createQuery(resultQuery).getSingleResult();

			return entity;
		} catch (NoResultException e) {
			return null; // 該当データなし
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<MemberEntity> findByCondition(MemberSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<MemberEntity> query = builder.createQuery(MemberEntity.class);

			// FROM句
			Root<MemberEntity> root = query.from(MemberEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = getOrderList(builder, root, searchDto);

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * 条件検索(パスワード再発行画面)
	 * @param searchDto 検索DTO
	 * @return エンティティ
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public MemberEntity findByMemberCd(MemberSearchDto searchDto) {

		try {
			// クエリー作成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<MemberEntity> query = builder.createQuery(MemberEntity.class);

			// 確認用
			System.out.println(searchDto);

			// FROM句
			Root<MemberEntity> root = query.from(MemberEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// 実行
			CriteriaQuery<MemberEntity> resultQuery = query.select(root).where(where);
			MemberEntity entity = null;
			try {
				entity = getEntityManager().createQuery(resultQuery).getSingleResult();
			} catch (NoResultException e) {
				// 結果が１件もなかった場合はnullとする
				entity = null;
			}
			// 確認用
			logger.debug("Daoでのエンティティの値" + entity);

			return entity;
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<MemberEntity> root, BaseSearchDto baseSearchDto) {

		// 会員検索DTO
		MemberSearchDto searchDto = (MemberSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (!ArrayUtils.isEmpty(searchDto.getMemberNos())) {
					add(createInPhrase(builder, root, "memberNo", searchDto.getMemberNos())); // 複数
				} else if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo())); // 単独
				}

				// 性
				if (StringUtils.hasText(searchDto.getLastName())) {
					add(builder.like(root.get("lastName"), parMatch(searchDto.getLastName())));
				}

				// 名
				if (StringUtils.hasText(searchDto.getFirstName())) {
					add(builder.like(root.get("firstName"), parMatch(searchDto.getFirstName())));
				}

				// 会員コード
				if (searchDto.getMemberCd() != null) {
					add(builder.equal(root.get("memberCd"), searchDto.getMemberCd()));
				}

				// セキュリティコード
				if (searchDto.getSecurityCd() != null) {
					add(builder.equal(root.get("securityCd"), searchDto.getSecurityCd()));
				}

				// 生年月日
				if (searchDto.getBirthDay() != null) {
					add(builder.equal(root.get("birthDay"), searchDto.getBirthDay()));
				}

				// メールアドレス
				if (searchDto.getEmailAddress() != null) {
					add(builder.equal(root.get("emailAddress"), searchDto.getEmailAddress()));
				}

				// 所属都道府県コード
				if (searchDto.getAffiliatedPrefectureCd() != null) {
					add(builder.equal(root.get("affiliatedPrefectureCd"), searchDto.getAffiliatedPrefectureCd()));
				}

				// ステータス番号
				if (searchDto.getStatusNo() != null) {
					add(builder.equal(root.get("statusNo"), searchDto.getStatusNo()));
				}

				// 勤務先都道府県コード
				if (searchDto.getWorkingPrefectureCd() != null) {
					add(builder.equal(root.get("workingPrefectureCd"), searchDto.getWorkingPrefectureCd()));
				}

				//会員番号(範囲)
				Integer memberFrom = searchDto.getMemberFrom();
				Integer memberTo = searchDto.getMemberTo();
				if (memberFrom != null && memberTo != null) {
					List<Predicate> subPredicates = new ArrayList<Predicate>() {
						{
							if (memberFrom <= memberTo) {
								add(builder.between(root.get("memberNo"), memberFrom, memberTo));
							} else {
								add(builder.between(root.get("memberNo"), memberTo, memberFrom));
							}
						}
					};
					add(builder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
				}

				// 氏名
				if (StringUtils.hasText(searchDto.getFullName())) {
					add(builder.like(root.get("fullName"), parMatch(searchDto.getFullName())));
				}
				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}

	// TODO
}
