package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.QualificationOtAppHistorySearchDto;
import jp.or.jaot.model.entity.QualificationOtAppHistoryEntity;

/**
 * 認定作業療法士申請履歴DAO
 */
@Component
public class QualificationOtAppHistoryDao extends BaseDao<QualificationOtAppHistoryEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<QualificationOtAppHistoryEntity> findByCondition(QualificationOtAppHistorySearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<QualificationOtAppHistoryEntity> query = builder
					.createQuery(QualificationOtAppHistoryEntity.class);

			// FROM句
			Root<QualificationOtAppHistoryEntity> root = query.from(QualificationOtAppHistoryEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			//			List<Order> orders = new ArrayList<Order>();
			//			orders.add(builder.asc(root.get("seq"))); // 昇順:回数

			// 実行
			//			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
			return getResultList(query.select(root).where(where), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<QualificationOtAppHistoryEntity> root,
			QualificationOtAppHistorySearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{

				// 会員番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}

}
