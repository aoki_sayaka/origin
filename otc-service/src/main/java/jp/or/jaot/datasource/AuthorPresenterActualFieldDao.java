package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.model.dto.search.AuthorPresenterActualFieldSearchDto;
import jp.or.jaot.model.entity.AuthorPresenterActualFieldEntity;

/**
 * 活動実績の領域(発表・著作等)DAO
 */
@Component
public class AuthorPresenterActualFieldDao extends BaseDao<AuthorPresenterActualFieldEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<AuthorPresenterActualFieldEntity> findByCondition(
			AuthorPresenterActualFieldSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<AuthorPresenterActualFieldEntity> query = builder
					.createQuery(AuthorPresenterActualFieldEntity.class);

			// FROM句
			Root<AuthorPresenterActualFieldEntity> root = query.from(AuthorPresenterActualFieldEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("resultId"))); // 昇順:活動実績ID
					add(builder.asc(root.get("fieldCd"))); // 昇順:領域コード
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * WHERE句取得
	 * @param builder ビルダー
	 * @param root ルート
	 * @param searchDto 検索DTO
	 * @return WHERE句
	 */
	private Predicate getWhere(CriteriaBuilder builder, Root<AuthorPresenterActualFieldEntity> root,
			AuthorPresenterActualFieldSearchDto searchDto) {

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 活動実績ID
				if (!ArrayUtils.isEmpty(searchDto.getResultIds())) {
					add(createInPhrase(builder, root, "resultId", searchDto.getResultIds()));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
