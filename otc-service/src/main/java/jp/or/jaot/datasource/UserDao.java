package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.UserSearchDto;
import jp.or.jaot.model.entity.UserEntity;

/**
 * 事務局ユーザDAO
 */
@Component
public class UserDao extends BaseDao<UserEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<UserEntity> findByCondition(UserSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<UserEntity> query = builder.createQuery(UserEntity.class);

			// FROM句
			Root<UserEntity> root = query.from(UserEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.asc(root.get("createDatetime"))); // 昇順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<UserEntity> root, BaseSearchDto baseSearchDto) {

		// 事務局ユーザDTO
		UserSearchDto searchDto = (UserSearchDto) baseSearchDto;

		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// ID
				if (searchDto.getId() != null) {
					add(builder.equal(root.get("id"), searchDto.getId()));
				}

				// ログインID
				if (StringUtils.hasText(searchDto.getLoginId())) {
					add(builder.equal(root.get("loginId"), searchDto.getLoginId()));
				}

				// ユーザ名。部分一致検索
				if (StringUtils.hasText(searchDto.getName())) {
					add(builder.like(root.get("name"), parMatch(searchDto.getName())));
				}

			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
