package jp.or.jaot.datasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import jp.or.jaot.core.datasource.BaseDao;
import jp.or.jaot.core.exception.system.HibernateSystemException;
import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.search.ApplicationProgressSearchDto;
import jp.or.jaot.model.entity.ApplicationProgressEntity;

/**
 * 申請進捗DAO
 */
@Component
public class ApplicationProgressDao extends BaseDao<ApplicationProgressEntity> {

	/**
	 * 条件検索
	 * @param searchDto 検索DTO
	 * @return エンティティリスト
	 * @throws HibernateSystemException Hibernateシステム例外
	 */
	public List<ApplicationProgressEntity> findByCondition(ApplicationProgressSearchDto searchDto) {

		try {
			// クエリー生成
			CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
			CriteriaQuery<ApplicationProgressEntity> query = builder.createQuery(ApplicationProgressEntity.class);

			// FROM句
			Root<ApplicationProgressEntity> root = query.from(ApplicationProgressEntity.class);

			// WHERE句
			Predicate where = getWhere(builder, root, searchDto);

			// ORDER BY句
			List<Order> orders = new ArrayList<Order>() {
				{
					add(builder.desc(root.get("applyDate"))); // 降順:受付日
					add(builder.desc(root.get("createDatetime"))); // 降順:作成日時
				}
			};

			// 実行
			return getResultList(query.select(root).where(where).orderBy(orders), searchDto);
		} catch (Exception e) {
			throw new HibernateSystemException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Predicate getWhere(CriteriaBuilder builder, Root<ApplicationProgressEntity> root,
			BaseSearchDto baseSearchDto) {

		// 申請進捗検索DTO
		ApplicationProgressSearchDto searchDto = (ApplicationProgressSearchDto) baseSearchDto;

		// WHERE句生成
		List<Predicate> predicates = new ArrayList<Predicate>() {
			{
				// 削除フラグ
				add(builder.equal(root.get("deleted"), false));

				// 会員番号
				if (searchDto.getMemberNo() != null) {
					add(builder.equal(root.get("memberNo"), searchDto.getMemberNo()));
				}

				// 受付日範囲
				Date rangeFromApplyDate = searchDto.getApplyDateRangeFrom();
				Date rangeToApplyDate = searchDto.getApplyDateRangeTo();
				if (rangeFromApplyDate != null && rangeToApplyDate != null) {
					add(builder.between(root.get("applyDate"), rangeFromApplyDate, rangeToApplyDate));
				}

				// TODO : 検索条件追加
			}
		};
		Predicate where = builder.and(predicates.toArray(new Predicate[predicates.size()]));

		return where;
	}
}
