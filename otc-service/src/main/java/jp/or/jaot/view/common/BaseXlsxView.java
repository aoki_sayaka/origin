package jp.or.jaot.view.common;

import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractXlsxView;
import org.springframework.web.util.UriUtils;

/**
 * Excelビューの基底クラス
 */
public abstract class BaseXlsxView extends AbstractXlsxView {

	/** ロガーインスタンス */
	protected final Logger logger;

	/**
	 * コンストラクタ
	 */
	public BaseXlsxView() {
		super();
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * ファイル名設定
	 * @param response Httpレスポンス
	 * @param fileName ファイル名
	 */
	protected void setFileName(HttpServletResponse response, String fileName) {
		String name = "Content-Disposition";
		String fileNameEncode = UriUtils.encode(fileName, StandardCharsets.UTF_8.name());
		String value = String.format("attachment; filename=\"%s\"; filename*=UTF-8''%s", fileName, fileNameEncode);
		response.setHeader(name, value);
	}
}
