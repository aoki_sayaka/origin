package jp.or.jaot.view.common;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.AbstractView;
import org.springframework.web.util.UriUtils;

/**
 * Csvビューの基底クラス
 */
public abstract class BaseCsvView extends AbstractView {

	/** ロガーインスタンス */
	protected final Logger logger;

	/**
	 * コンストラクタ
	 */
	public BaseCsvView() {
		super();
		this.setContentType("text/csv");
		this.logger = LoggerFactory.getLogger(this.getClass().getName());
	}

	/**
	 * ファイル名設定
	 * @param response current HTTP response
	 * @param fileName file name
	 */
	protected void setFileName(HttpServletResponse response, String fileName) {
		String name = "Content-Disposition";
		String fileNameEncode = UriUtils.encode(fileName, StandardCharsets.UTF_8.name());
		String value = String.format("attachment; filename=\"%s\"; filename*=UTF-8''%s", fileName, fileNameEncode);
		response.setHeader(name, value);
	}

	/**
	 * CSVドキュメント生成
	 * @param model combined output Map
	 * @param request current HTTP request
	 * @param response  current HTTP response
	 * @throws Exception if rendering failed
	 */
	protected abstract void buildCsvDocument(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception;

	@Override
	protected boolean generatesDownloadContent() {
		return true;
	}

	@Override
	protected final void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setContentType(this.getContentType());
		buildCsvDocument(model, request, response);
	}
}
