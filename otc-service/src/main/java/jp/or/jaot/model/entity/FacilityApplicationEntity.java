package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請エンティティ<br>
 */
@Entity
@Table(name = "facility_applications")
@Getter
@Setter
public class FacilityApplicationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 施設区分 */
	@Column(name = "facility_category")
	private String facilityCategory;

	/** 申請者区分コード */
	@Column(name = "member_category_cd")
	private String memberCategoryCd;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 仮会員番号 */
	@Column(name = "temporary_member_no")
	private Integer temporaryMemberNo;

	/** 申請ステータス */
	private Integer status;

	/** 開設者種別コード */
	@Column(name = "opener_type_cd")
	private String openerTypeCd;

	/** 開設者名 */
	@Column(name = "opener_nm")
	private String openerNm;

	/** 開設者名(カナ) */
	@Column(name = "opener_nm_kana")
	private String openerNmKana;

	/** 責任者メールアドレス */
	@Column(name = "responsible_email_address")
	private String responsibleEmailAddress;

	/** 施設名 */
	@Column(name = "facility_nm")
	private String facilityNm;

	/** 施設名２ */
	@Column(name = "facility_nm2")
	private String facilityNm2;

	/** 施設名(カナ) */
	@Column(name = "facility_nm_kana")
	private String facilityNmKana;

	/** 施設名(カナ)２ */
	@Column(name = "facility_nm_kana2")
	private String facilityNmKana2;

	/** 郵便番号 */
	private String zipcode;

	/** 施設都道府県コード */
	@Column(name = "facility_prefecture_cd")
	private String facilityPrefectureCd;

	/** 住所１ */
	private String address1;

	/** 住所２ */
	private String address2;

	/** 住所３ */
	private String address3;

	/** 代表電話番号 */
	@Column(name = "primary_phone_number")
	private String primaryPhoneNumber;

	/** FAX番号 */
	@Column(name = "fax_number")
	private String faxNumber;

	/** 団体・法人名 */
	@Column(name = "corporate_nm")
	private String corporateNm;

	/** 団体・法人名(カナ) */
	@Column(name = "corporate_nm_kana")
	private String corporateNmKana;

	/** 学部・学科・専攻 */
	private String department;

	/** 養成校種別 */
	@Column(name = "training_school_category_cd")
	private String trainingSchoolCategoryCd;
}
