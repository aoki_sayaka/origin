package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(後輩育成・社会貢献)エンティティ<br>
 */
@Entity
@Table(name = "social_contribution_actual_results")
@Getter
@Setter
public class SocialContributionActualResultEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 登録者区分 */
	@Column(name = "register_category_cd")
	private String registerCategoryCd;

	/** 審査等内容コード */
	@Column(name = "activity_content_cd")
	private String activityContentCd;

	/** 開始年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;

	/** 終了年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	/** 種別 */
	private String type;

	/** 団体名 */
	@Column(name = "group_name")
	private String groupName;

	/** 他団体詳細 */
	@Column(name = "group_detail")
	private String groupDetail;

	/** 研修会名 */
	@Column(name = "training_name")
	private String trainingName;

	/** 開催回 */
	@Column(name = "held_times")
	private Integer heldTimes;

	/** 開催地 */
	private String venue;
}
