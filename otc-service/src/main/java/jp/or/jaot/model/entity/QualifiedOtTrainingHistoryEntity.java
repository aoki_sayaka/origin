package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴エンティティ<br>
 */
@Entity
@Table(name = "qualified_ot_training_histories")
@Getter
@Setter
public class QualifiedOtTrainingHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 研修会コード */
	@Column(name = "training_cd")
	private String trainingCd;

	/** 年度 */
	@Column(name = "fiscal_year")
	private String fiscalYear;

	/** 主催者コード */
	@Column(name = "promoter_cd")
	private String promoterCd;

	/** カテゴリーコード */
	@Column(name = "category_cd")
	private String categoryCd;

	/** 専門分野コード */
	@Column(name = "professional_field_cd")
	private String professionalFieldCd;

	/** グループコード */
	@Column(name = "group_cd")
	private String groupCd;

	/** クラスコード */
	@Column(name = "class_cd")
	private String classCd;

	/** 枝番 */
	@Column(name = "branch_cd")
	private String branchCd;

	/** 回数 */
	@Column(name = "seq")
	private Integer seq;

	/** 研修名 */
	@Column(name = "training_name")
	private String trainingName;

	/** 研修受講年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "attend_date")
	private Date attendDate;
}
