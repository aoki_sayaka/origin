package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.QualificationOtAppHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士申請履歴DTO
 */
@Getter
@Setter
public class QualificationOtAppHistoryDto extends BaseDto<QualificationOtAppHistoryDto, QualificationOtAppHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private QualificationOtAppHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private QualificationOtAppHistoryDto(QualificationOtAppHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static QualificationOtAppHistoryDto create() {
		return new QualificationOtAppHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static QualificationOtAppHistoryDto createSingle() {
		return new QualificationOtAppHistoryDto(new QualificationOtAppHistoryEntity());
	}

}
