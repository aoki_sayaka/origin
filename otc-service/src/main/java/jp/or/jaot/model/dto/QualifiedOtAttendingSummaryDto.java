package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.QualifiedOtAttendingSummaryCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修履歴DTO
 */
@Getter
@Setter
public class QualifiedOtAttendingSummaryDto
		extends BaseDto<QualifiedOtAttendingSummaryDto, QualifiedOtAttendingSummaryCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private QualifiedOtAttendingSummaryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private QualifiedOtAttendingSummaryDto(QualifiedOtAttendingSummaryCompositeEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static QualifiedOtAttendingSummaryDto create() {
		return new QualifiedOtAttendingSummaryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static QualifiedOtAttendingSummaryDto createSingle() {
		return new QualifiedOtAttendingSummaryDto(new QualifiedOtAttendingSummaryCompositeEntity());
		//CompositeEntityに変更（複合）
	}

}
