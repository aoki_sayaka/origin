package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請検索DTO
 */
@Getter
@Setter
public class FacilityApplicationSearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer[] memberNos;

	/** 仮会員番号 */
	private Integer[] temporaryMemberNos;

	/** 申請ステータス */
	private Integer[] statuses;

	// TODO : 検索条件追加
}
