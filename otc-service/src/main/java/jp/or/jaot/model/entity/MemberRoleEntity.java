package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員ロールエンティティ<br>
 */
@Entity
@Table(name = "member_roles")
@Getter
@Setter
public class MemberRoleEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 権限コード識別ID */
	@Column(name = "role_code_id")
	private String roleCodeId;

	/** 権限コード値 */
	@Column(name = "role_code")
	private String roleCode;
}
