package jp.or.jaot.model.entity;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会エンティティ<br>
 */
@Entity
@Table(name = "trainings")
@Getter
@Setter
public class TrainingEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 合否判定 */
	@Column(name = "acceptance_cd")
	private String acceptanceCd;

	/** 住所1 */
	private String address1;

	/** 住所2 */
	private String address2;

	/** 住所3 */
	private String address3;

	/** 受講許可振込期限日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "attend_permission_deposit_period_date")
	private Date attendPermissionDepositPeriodDate;

	/** 受講許可公文書 */
	@Column(name = "attend_permission_doc")
	private String attendPermissionDoc;

	/** 受講許可公文書発送日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "attend_permission_doc_delivery_date")
	private Date attendPermissionDocDeliveryDate;

	/** 出席受付 */
	@Column(name = "attendance_recept_cd")
	private String attendanceReceptCd;

	/** 受講ポイント */
	@Column(name = "attending_point")
	private Integer attendingPoint;

	/** 中止日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "cancel_date")
	private Date cancelDate;

	/** キャンセルフラグ */
	@Column(name = "cancel_flag")
	private Boolean cancelFlag;

	/** 定員 */
	@Column(name = "capacity_count")
	private Integer capacityCount;

	/** 講座分類 */
	@Column(name = "course_category_cd")
	private String courseCategoryCd;

	/** 講座名 */
	@Column(name = "course_nm")
	private String courseNm;

	/** 配信終了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_end_date")
	private Date deliveryEndDate;

	/** 配信開始日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_start_date")
	private Date deliveryStartDate;

	/** e-learningフラグ */
	@Column(name = "elearning_flag")
	private Boolean elearningFlag;

	/** 終了日時 */
	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;

	/** 終了フラグ */
	@Column(name = "end_flag")
	private Boolean endFlag;

	/** 参加費 */
	@Column(name = "entry_fee")
	private Integer entryFee;

	/** 開催日フリー */
	@Column(name = "event_date_free")
	private String eventDateFree;

	/** 開催日1 */
	@Temporal(TemporalType.DATE)
	@Column(name = "event_date1")
	private Date eventDate1;

	/** 開催日1開始時間 */
	@Column(name = "event_date1_start_time")
	private Time eventDate1StartTime;

	/** 開催日2 */
	@Temporal(TemporalType.DATE)
	@Column(name = "event_date2")
	private Date eventDate2;

	/** 開催日3 */
	@Temporal(TemporalType.DATE)
	@Column(name = "event_date3")
	private Date eventDate3;

	/** 開催回数 */
	@Column(name = "hold_count")
	private Integer holdCount;

	/** 開催日数 */
	@Column(name = "hold_days")
	private Integer holdDays;

	/** 開催年度 */
	@Column(name = "hold_fiscal_year")
	private Integer holdFiscalYear;

	/** 協会HP掲載日時 */
	@Temporal(TemporalType.DATE)
	@Column(name = "hp_posted_date")
	private Date hpPostedDate;

	/** 協会HP掲載フラグ */
	@Column(name = "hp_posted_flag")
	private Boolean hpPostedFlag;

	/** 協会HP掲載ユーザID */
	@Column(name = "hp_posted_user_id")
	private String hpPostedUserId;

	/** 講師本人宛公文書番号 */
	@Column(name = "instructor_doc_no_to_person")
	private String instructorDocNoToPerson;

	/** 講師上司宛公文書番号 */
	@Column(name = "instructor_doc_no_to_superiors")
	private String instructorDocNoToSuperiors;

	/** 講師公文書出力形式 */
	@Column(name = "instructor_doc_print_format_cd")
	private String instructorDocPrintFormatCd;

	/** 退席確認 */
	@Column(name = "leave_confirm_cd")
	private String leaveConfirmCd;

	/** 連絡欄 */
	private String message;

	/** 旧研修番号 */
	@Column(name = "old_training_no")
	private String oldTrainingNo;

	/** 運営担当者本人宛公文書番号 */
	@Column(name = "operator_doc_no_to_person")
	private String operatorDocNoToPerson;

	/** 運営担当者上長宛公文書番号 */
	@Column(name = "operator_doc_no_to_superiors")
	private String operatorDocNoToSuperiors;

	/** 運営担当者公文書出力形式 */
	@Column(name = "operator_doc_print_format_cd")
	private String operatorDocPrintFormatCd;

	/** 本人宛その他6 */
	private String other6;

	/** 概要および目的 */
	private String overview;

	/** 都道府県コード */
	@Column(name = "prefecture_cd")
	private String prefectureCd;

	/** 受付締切日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "reception_deadline_date")
	private Date receptionDeadlineDate;

	/** 受付開始日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "reception_start_date")
	private Date receptionStartDate;

	/** 受付状況 */
	@Column(name = "reception_status_cd")
	private String receptionStatusCd;

	/** 備考 */
	private String remarks;

	/** 学割参加費 */
	@Column(name = "student_discount_entry_fee")
	private Integer studentDiscountEntryFee;

	/** 参加対象 */
	@Column(name = "target_cd")
	private String targetCd;

	/** 研修会終了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "training_end_date")
	private Date trainingEndDate;

	/** 研修会番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 研修会プログラムフリー */
	@Column(name = "training_program_free")
	private String trainingProgramFree;

	/** 研修会プログラムフリーフラグ */
	@Column(name = "training_program_free_flag")
	private Boolean trainingProgramFreeFlag;

	/** 研修会種別 */
	@Column(name = "training_type_cd")
	private String trainingTypeCd;

	/** 研修会更新日時 */
	@Column(name = "training_update_datetime")
	private Timestamp trainingUpdateDatetime;

	/** 研修会更新者名 */
	@Column(name = "training_update_user")
	private String trainingUpdateUser;

	/** 更新状況 */
	@Column(name = "update_status_cd")
	private String updateStatusCd;

	/** 開催場所 */
	private String venue;

	/** 開催場所フリー */
	@Column(name = "venue_free")
	private String venueFree;

	/** 開催場所フリーフラグ */
	@Column(name = "venue_free_flag")
	private Boolean venueFreeFlag;

	/** 開催地 */
	@Column(name = "venue_pref_cd")
	private String venuePrefCd;

	/** 郵便番号 */
	private String zipcode;
}
