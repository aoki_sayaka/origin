package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MtdlpCaseReportEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP事例報告DTO
 */
@Getter
@Setter
public class MtdlpCaseReportDto extends BaseDto<MtdlpCaseReportDto, MtdlpCaseReportEntity> {

	/**
	 * コンストラクタ
	 */
	private MtdlpCaseReportDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MtdlpCaseReportDto(MtdlpCaseReportEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MtdlpCaseReportDto create() {
		return new MtdlpCaseReportDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MtdlpCaseReportDto createSingle() {
		return new MtdlpCaseReportDto(new MtdlpCaseReportEntity());
	}
}
