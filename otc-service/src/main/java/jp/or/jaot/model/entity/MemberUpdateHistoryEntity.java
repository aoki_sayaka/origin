package jp.or.jaot.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員更新履歴エンティティ<br>
 */
@Entity
@Table(name = "member_update_histories")
@Getter
@Setter
public class MemberUpdateHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 更新連番 */
	@Column(name = "update_no")
	private Integer updateNo;

	/** 更新日時 */
	@Column(name = "member_update_datetime")
	private Timestamp memberUpdateDatetime;

	/** 更新ユーザID */
	@Column(name = "update_user_id")
	private String updateUserId;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 県士会番号 */
	@Column(name = "paot_cd")
	private Integer paotCd;

	/** 更新区分コード */
	@Column(name = "update_category_cd")
	private Integer updateCategoryCd;

	/** 更新日 */
	@Column(name = "update_date")
	private Timestamp updateDate;

	/** テーブルコード */
	@Column(name = "table_cd")
	private Integer tableCd;

	/** テーブル名 */
	@Column(name = "table_nm")
	private String tableNm;

	/** 項目コード */
	@Column(name = "field_cd")
	private Integer fieldCd;

	/** 項目名 */
	@Column(name = "field_nm")
	private String fieldNm;

	/** 変更前 */
	@Column(name = "content_before")
	private String contentBefore;

	/** 変更後 */
	@Column(name = "content_after")
	private String contentAfter;
}
