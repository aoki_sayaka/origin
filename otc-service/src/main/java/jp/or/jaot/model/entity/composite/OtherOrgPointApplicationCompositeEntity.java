package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;
import jp.or.jaot.model.entity.OtherOrgPointApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント申請エンティティ(複合)<br>
 * ・他団体・SIGポイント申請と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class OtherOrgPointApplicationCompositeEntity implements Entity {

	// 他団体・SIGポイント申請
	private OtherOrgPointApplicationEntity otherOrgPointApplicationEntity = new OtherOrgPointApplicationEntity();

	// 他団体・SIGポイント添付ファイル
	private List<OtherOrgPointApplicationAttachmentEntity> otherOrgPointApplicationAttachmentEntities = new ArrayList<OtherOrgPointApplicationAttachmentEntity>();

	// 会員情報
	private List<MemberEntity> memberEntities = new ArrayList<MemberEntity>();

}
