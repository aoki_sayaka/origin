package jp.or.jaot.model.entity;

import java.sql.Timestamp;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局サイトログイン履歴エンティティ<br>
 */
@Entity
@Table(name = "user_login_histories")
@Getter
@Setter
public class UserLoginHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 事務局ユーザID */
	@Column(name = "user_id")
	private Long userId;

	/** ログイン日時 */
	@Column(name = "login_datetime")
	private Timestamp loginDatetime;

	/** ログアウト日時 */
	@Column(name = "logout_datetime")
	private Timestamp logoutDatetime;

	/**
	 * ログイン日時比較
	 */
	public static class LoginDatetimeComparator implements Comparator<UserLoginHistoryEntity> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int compare(UserLoginHistoryEntity e1, UserLoginHistoryEntity e2) {
			Timestamp t1 = e1.getLoginDatetime();
			Timestamp t2 = e2.getLoginDatetime();
			if (t1 != null && t2 != null) {
				return t1.compareTo(t2);
			} else if (t1 != null && t2 == null) {
				return (1);
			} else if (t1 == null && t2 != null) {
				return (-1);
			} else {
				return (0);
			}
		}
	}

	/**
	 * ログアウト日時比較
	 */
	public static class LogoutDatetimeComparator implements Comparator<UserLoginHistoryEntity> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int compare(UserLoginHistoryEntity e1, UserLoginHistoryEntity e2) {
			Timestamp t1 = e1.getLogoutDatetime();
			Timestamp t2 = e2.getLogoutDatetime();
			if (t1 != null && t2 != null) {
				return t1.compareTo(t2);
			} else if (t1 != null && t2 == null) {
				return (1);
			} else if (t1 == null && t2 != null) {
				return (-1);
			} else {
				return (0);
			}
		}
	}
}
