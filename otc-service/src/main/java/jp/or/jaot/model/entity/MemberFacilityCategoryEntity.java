package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員勤務先業務分類エンティティ<br>
 * ・BaseEntityに定義済みの共通カラムは本エンティティから削除する<br>
 * ・getter/setterもlombokを使用するため本エンティティから削除する<br>
 */
@Entity
@Table(name = "member_facility_categories")
@Getter
@Setter
public class MemberFacilityCategoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 分類区分 */
	@Column(name = "category_division")
	private Integer categoryDivision;

	/** 主従区分 */
	@Column(name = "primary_division")
	private Integer primaryDivision;

	/** 親分類コード */
	@Column(name = "parent_code")
	private String parentCode;

	/** 分類コード */
	@Column(name = "category_code")
	private String categoryCode;
}