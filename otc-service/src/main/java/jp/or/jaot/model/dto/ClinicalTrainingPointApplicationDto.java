package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.ClinicalTrainingPointApplicationCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習ポイント申請DTO
 */
@Getter
@Setter
public class ClinicalTrainingPointApplicationDto
		extends BaseDto<ClinicalTrainingPointApplicationDto, ClinicalTrainingPointApplicationCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private ClinicalTrainingPointApplicationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ClinicalTrainingPointApplicationDto(ClinicalTrainingPointApplicationCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ClinicalTrainingPointApplicationDto create() {
		return new ClinicalTrainingPointApplicationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ClinicalTrainingPointApplicationDto createSingle() {
		return new ClinicalTrainingPointApplicationDto(new ClinicalTrainingPointApplicationCompositeEntity());
	}
}
