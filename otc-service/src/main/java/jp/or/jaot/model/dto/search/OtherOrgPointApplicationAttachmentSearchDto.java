package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント添付ファイル検索DTO
 */
@Getter
@Setter
public class OtherOrgPointApplicationAttachmentSearchDto extends BaseSearchDto {
	
	/** id */
	private Integer id;

	/** 会員番号 */
	private Integer memberNo;

}
