package jp.or.jaot.model.entity;

import java.sql.Timestamp;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員ポータルログイン履歴エンティティ<br>
 */
@Entity
@Table(name = "member_login_histories")
@Getter
@Setter
public class MemberLoginHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** ログイン日時 */
	@Column(name = "login_datetime")
	private Timestamp loginDatetime;

	/** ログアウト日時 */
	@Column(name = "logout_datetime")
	private Timestamp logoutDatetime;

	/**
	 * ログイン日時比較
	 */
	public static class LoginDatetimeComparator implements Comparator<MemberLoginHistoryEntity> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int compare(MemberLoginHistoryEntity e1, MemberLoginHistoryEntity e2) {
			Timestamp t1 = e1.getLoginDatetime();
			Timestamp t2 = e2.getLoginDatetime();
			if (t1 != null && t2 != null) {
				return t1.compareTo(t2);
			} else if (t1 != null && t2 == null) {
				return (1);
			} else if (t1 == null && t2 != null) {
				return (-1);
			} else {
				return (0);
			}
		}
	}

	/**
	 * ログアウト日時比較
	 */
	public static class LogoutDatetimeComparator implements Comparator<MemberLoginHistoryEntity> {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int compare(MemberLoginHistoryEntity e1, MemberLoginHistoryEntity e2) {
			Timestamp t1 = e1.getLogoutDatetime();
			Timestamp t2 = e2.getLogoutDatetime();
			if (t1 != null && t2 != null) {
				return t1.compareTo(t2);
			} else if (t1 != null && t2 == null) {
				return (1);
			} else if (t1 == null && t2 != null) {
				return (-1);
			} else {
				return (0);
			}
		}
	}
}
