package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberQualificationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員関連資格情報DTO
 */
@Getter
@Setter
public class MemberQualificationDto extends BaseDto<MemberQualificationDto, MemberQualificationEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberQualificationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberQualificationDto(MemberQualificationEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberQualificationDto create() {
		return new MemberQualificationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberQualificationDto createSingle() {
		return new MemberQualificationDto(new MemberQualificationEntity());
	}
}
