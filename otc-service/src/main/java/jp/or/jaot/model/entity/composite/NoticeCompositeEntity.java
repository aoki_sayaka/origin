package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.NoticeConditionPrefEntity;
import jp.or.jaot.model.entity.NoticeDestinationEntity;
import jp.or.jaot.model.entity.NoticeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせエンティティ(複合)<br>
 * ・お知らせと関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class NoticeCompositeEntity implements Entity {

	/** お知らせ */
	private NoticeEntity noticeEntity = new NoticeEntity();

	/** お知らせ(都道府県) */
	private List<NoticeConditionPrefEntity> noticeConditionPrefEntities = new ArrayList<NoticeConditionPrefEntity>();

	/** お知らせ(宛先) */
	private List<NoticeDestinationEntity> noticeDestinationEntities = new ArrayList<NoticeDestinationEntity>();
}
