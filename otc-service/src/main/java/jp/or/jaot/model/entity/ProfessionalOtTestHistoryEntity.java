package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士試験結果履歴エンティティ<br>
 */
@Entity
@Table(name = "professional_ot_test_histories")
@Getter
@Setter
public class ProfessionalOtTestHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 専門分野 */
	@Column(name = "professional_field")
	private String professionalField;

	/** 試験日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "test_date")
	private Date testDate;

	/** 合否判定コード */
	@Column(name = "acceptance_cd")
	private String acceptanceCd;
}
