package jp.or.jaot.model.dto.search;

import java.util.Date;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員検索DTO
 */
@Getter
@Setter
public class MemberSearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 会員番号(複数) */
	private Integer[] memberNos;

	/** 会員番号(範囲from) */
	private Integer memberFrom;

	/** 会員番号(範囲to) */
	private Integer memberTo;

	/** 姓 */
	private String lastName;

	/** 名 */
	private String firstName;

	/** 氏名(フルネーム) */
	private String fullName;

	/** 会員コード */
	private String memberCd;

	/** セキュリティコード */
	private String securityCd;

	/** 生年月日 */
	private Date birthDay;

	/** メールアドレス */
	private String emailAddress;

	/** ステータス番号 */
	private Integer statusNo;

	/** 所属都道府県コード */
	private String affiliatedPrefectureCd;

	/** 勤務先都道府県コード */
	private String workingPrefectureCd;

	// TODO
}
