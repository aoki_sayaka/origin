package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習ポイント申請エンティティ<br>
 */

@Entity
@Table(name = "clinical_training_point_applications")
@Getter
@Setter
public class ClinicalTrainingPointApplicationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 申請日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "application_date")
	private Date applicationDate;
	
	/** 申請者会員番号 */
	@Column(name = "application_member_no")
	private Integer applicationMemberNo;
	
	/** 養成校番号 */
	@Column(name = "facility_no")
	private String facilityNo;
	
	/** 状態 */
	@Column(name = "status_cd")
	private String statusCd;
}
