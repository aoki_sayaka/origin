package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者研修受講履歴DTO
 */
@Getter
@Setter
public class ClinicalTrainingLeaderTrainingHistoryDto
		extends BaseDto<ClinicalTrainingLeaderTrainingHistoryDto, ClinicalTrainingLeaderTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private ClinicalTrainingLeaderTrainingHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ClinicalTrainingLeaderTrainingHistoryDto(ClinicalTrainingLeaderTrainingHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ClinicalTrainingLeaderTrainingHistoryDto create() {
		return new ClinicalTrainingLeaderTrainingHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ClinicalTrainingLeaderTrainingHistoryDto createSingle() {
		return new ClinicalTrainingLeaderTrainingHistoryDto(new ClinicalTrainingLeaderTrainingHistoryEntity());
	}
}
