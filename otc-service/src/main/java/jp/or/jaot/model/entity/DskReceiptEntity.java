package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * DskPOSTリクエストエンティティ
 */
@Entity
@Table(name = "dsk_receipts")
@Getter
@Setter
@ToString
public class DskReceiptEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 種別 */
	@Column(name = "type_cd")
	private String typeCd;

	/** 収納日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "receipted_date")
	private Date receiptedDate;

	/** 収納時刻 */
	@Temporal(TemporalType.TIME)
	@Column(name = "receipted_time")
	private Date receiptedTime;

	/** バーコード */
	@Column(name = "barcode")
	private String barcode;

	/** ユーザー使用欄１ */
	@Column(name = "user_data1")
	private String userData1;

	/** ユーザー使用欄２ */
	@Column(name = "user_data2")
	private String userData2;

	/** 印紙フラグ */
	@Column(name = "stamp")
	private Boolean stamp;

	/** 金額 */
	@Column(name = "amount_of_money")
	private Integer amountOfMoney;

	/** コンビニ本部コード */
	@Column(name = "convenience_head_cd")
	private String convenienceHeadCd;

	/** コンビニ名 */
	@Column(name = "convenience_name")
	private String convenienceName;

	/** コンビニ店舗コード */
	@Column(name = "convenience_cd")
	private String convenienceCd;

	/** 振込日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "transfer_date")
	private Date transferDate;

	/** データ作成日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "creation_date")
	private Date creationDate;

	/** 請求ID */
	@Column(name = "billing_id")
	private String billingId;

	/** 会員番号 */
	@Column(name = "member_no")
	private String memberNo;

	/** 再発行区分 */
	@Column(name = "number_of_reissues")
	private Integer numberOfReissues;

	/** 支払期限 */
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_due_date")
	private Date paymentDueDate;

}