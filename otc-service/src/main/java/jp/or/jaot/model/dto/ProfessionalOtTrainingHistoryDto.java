package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.ProfessionalOtTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士研修受講履歴DTO
 */
@Getter
@Setter
public class ProfessionalOtTrainingHistoryDto
		extends BaseDto<ProfessionalOtTrainingHistoryDto, ProfessionalOtTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private ProfessionalOtTrainingHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ProfessionalOtTrainingHistoryDto(ProfessionalOtTrainingHistoryEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ProfessionalOtTrainingHistoryDto create() {
		return new ProfessionalOtTrainingHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ProfessionalOtTrainingHistoryDto createSingle() {
		return new ProfessionalOtTrainingHistoryDto(new ProfessionalOtTrainingHistoryEntity());
		//CompositeEntityに変更（複合）
	}

}
