package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 申請進捗エンティティ<br>
 */
@Entity
@Table(name = "application_progresses")
@Getter
@Setter
public class ApplicationProgressEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 申請番号 */
	@Column(name = "application_no")
	private Long applicationNo;

	/** 申請枝番 */
	@Column(name = "branch_no")
	private Integer branchNo;

	/** 連番 */
	@Column(name = "seq_no")
	private Integer seqNo;

	/** 登録者区分 */
	@Column(name = "register_category_cd")
	private String registerCategoryCd;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 申請種別 */
	@Column(name = "application_type_cd")
	private String applicationTypeCd;

	/** 申請種類 */
	@Column(name = "application_kind_cd")
	private String applicationKindCd;

	/** 申請区分 */
	@Column(name = "application_category_cd")
	private String applicationCategoryCd;

	/** 受付日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "apply_date")
	private Date applyDate;

	/** 送付日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "send_date")
	private Date sendDate;

	/** 受領日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "receipt_date")
	private Date receiptDate;

	/** 承認日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "approval_date")
	private Date approvalDate;

	/** 状況 */
	@Column(name = "progress_cd")
	private String progressCd;

	/** 状態 */
	@Column(name = "status_cd")
	private String statusCd;

	/** 送付区分 */
	@Column(name = "send_category_cd")
	private String sendCategoryCd;

	/** 送付回数 */
	@Column(name = "send_count")
	private Integer sendCount;
}
