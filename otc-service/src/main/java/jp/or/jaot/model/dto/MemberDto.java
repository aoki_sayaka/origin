package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.MemberCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員DTO
 */
@Getter
@Setter
public class MemberDto extends BaseDto<MemberDto, MemberCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberDto(MemberCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberDto create() {
		return new MemberDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberDto createSingle() {
		return new MemberDto(new MemberCompositeEntity());
	}
}
