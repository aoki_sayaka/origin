package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員関連資格情報エンティティ<br>
 */
@Entity
@Table(name = "member_qualifications")
@Getter
@Setter
public class MemberQualificationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 関連資格コード */
	@Column(name = "qualification_cd")
	private String qualificationCd;

	/** 区分コード */
	@Column(name = "qualification_category_cd")
	private String qualificationCategoryCd;
}
