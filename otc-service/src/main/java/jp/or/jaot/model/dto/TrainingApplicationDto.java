package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.TrainingApplicationCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会申込DTO
 */
@Getter
@Setter
public class TrainingApplicationDto extends BaseDto<TrainingApplicationDto, TrainingApplicationCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingApplicationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingApplicationDto(TrainingApplicationCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingApplicationDto create() {
		return new TrainingApplicationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingApplicationDto createSingle() {
		return new TrainingApplicationDto(new TrainingApplicationCompositeEntity());
	}
}
