package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.TrainingOperatorEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会運営担当者DTO
 */
@Getter
@Setter
public class TrainingOperatorDto extends BaseDto<TrainingOperatorDto, TrainingOperatorEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingOperatorDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingOperatorDto(TrainingOperatorEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingOperatorDto create() {
		return new TrainingOperatorDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingOperatorDto createSingle() {
		return new TrainingOperatorDto(new TrainingOperatorEntity());
	}
}
