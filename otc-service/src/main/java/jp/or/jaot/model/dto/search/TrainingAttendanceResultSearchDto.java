package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会出欠合否検索DTO
 */
@Getter
@Setter
public class TrainingAttendanceResultSearchDto extends BaseSearchDto {

	/** 研修会申込結合フラグ(true=外部結合する, false=外部結合しない) */
	private Boolean joinApplicationFlag = false;

	/** 研修会番号 */
	private String[] trainingNos;

	/** 申込者番号 */
	private Integer memberNo;

	/** 氏名 */
	private String fullname;

	// TODO : 検索条件追加
}
