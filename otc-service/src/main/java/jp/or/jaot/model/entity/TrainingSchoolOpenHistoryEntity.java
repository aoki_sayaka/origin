package jp.or.jaot.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 養成校開設履歴エンティティ<br>
 */
@Entity
@Table(name = "training_school_open_histories")
@Getter
@Setter
public class TrainingSchoolOpenHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 施設養成校ID */
	@Column(name = "facility_id")
	private Long facilityId;

	/** 施設基本番号 */
	@Column(name = "facility_base_no")
	private String facilityBaseNo;

	/** 連番 */
	@Column(name = "seq_no")
	private Integer seqNo;

	/** 更新ユーザID */
	@Column(name = "open_update_user_id")
	private String openUpdateUserId;

	/** 更新日時 */
	@Column(name = "open_update_date")
	private Timestamp openUpdateDate;

	/** 開設年度 */
	@Column(name = "open_fiscal_year")
	private Integer openFiscalYear;

	/** 最終募集年度 */
	@Column(name = "last_recuriting_fiscal_year")
	private Integer lastRecuritingFiscalYear;

	/** 最終年度 */
	@Column(name = "last_fiscal_year")
	private Integer lastFiscalYear;
}
