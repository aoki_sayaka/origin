package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会報告書エンティティ<br>
 */
@Entity
@Table(name = "training_reports")
@Getter
@Setter
public class TrainingReportEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 研修会番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 対象 */
	@Column(name = "target_cd")
	private String targetCd;

	/** 予算 */
	private Integer budget;

	/** 収支 */
	private Integer balance;

	/** 支出 */
	private Integer expenditure;

	/** 参加者の評価 */
	@Column(name = "participants_evaluation_cd")
	private String participantsEvaluationCd;

	/** 運営担当者会員番号 */
	@Column(name = "operator_member_no")
	private Integer operatorMemberNo;

	/** 運営担当者氏名 */
	@Column(name = "operator_fullname")
	private String operatorFullname;

	/** 申込者数 */
	@Column(name = "applicant_count")
	private Integer applicantCount;

	/** 効果判定1評価 */
	@Column(name = "effect_judge1_cd")
	private String effectJudge1Cd;

	/** 効果判定1 */
	@Column(name = "effect_judge1_content")
	private String effectJudge1Content;

	/** 効果判定2評価 */
	@Column(name = "effect_judge2_cd")
	private String effectJudge2Cd;

	/** 効果判定2 */
	@Column(name = "effect_judge2_content")
	private String effectJudge2Content;

	/** 各連絡・協力評価 */
	@Column(name = "cooperation_evaluation_cd")
	private String cooperationEvaluationCd;

	/** 各連絡・協力 */
	@Column(name = "cooperation_evaluation_content")
	private String cooperationEvaluationContent;

	/** 職場電話話回数 */
	@Column(name = "workplace_phone_use_count")
	private Integer workplacePhoneUseCount;

	/** 職場電話通話総時間 */
	@Column(name = "workplace_phone_use_hour")
	private Integer workplacePhoneUseHour;

	/** 職場FAX信件数 */
	@Column(name = "workplace_fax_receive_count")
	private Integer workplaceFaxReceiveCount;

	/** 職場FAX受信件数 */
	@Column(name = "workplace_fax_send_count")
	private Integer workplaceFaxSendCount;

	/** 個人電話話回数 */
	@Column(name = "personal_phone_use_count")
	private Integer personalPhoneUseCount;

	/** 個人電話通話総時間 */
	@Column(name = "personal_phone_use_hour")
	private Integer personalPhoneUseHour;

	/** 総括 */
	private String summary;
}
