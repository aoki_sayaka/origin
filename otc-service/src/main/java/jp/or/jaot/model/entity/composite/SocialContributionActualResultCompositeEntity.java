package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.SocialContributionActual2FieldEntity;
import jp.or.jaot.model.entity.SocialContributionActualFieldEntity;
import jp.or.jaot.model.entity.SocialContributionActualResultEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(後輩育成・社会貢献)エンティティ(複合)<br>
 * ・活動実績(後輩育成・社会貢献)と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class SocialContributionActualResultCompositeEntity implements Entity {

	/** 活動実績(後輩育成・社会貢献) */
	private SocialContributionActualResultEntity socialContributionActualResultEntity = new SocialContributionActualResultEntity();

	/** 活動実績の領域(後輩育成・社会貢献) */
	private List<SocialContributionActualFieldEntity> socialContributionActualFieldEntities = new ArrayList<SocialContributionActualFieldEntity>();

	/** 活動実績の領域2(後輩育成・社会貢献) */
	private List<SocialContributionActual2FieldEntity> socialContributionActual2FieldEntities = new ArrayList<SocialContributionActual2FieldEntity>();
}
