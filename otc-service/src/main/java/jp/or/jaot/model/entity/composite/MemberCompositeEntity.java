package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.LifelongEducationStatusEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberFacilityCategoryEntity;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;
import jp.or.jaot.model.entity.MemberLoginHistoryEntity;
import jp.or.jaot.model.entity.MemberQualificationEntity;
import jp.or.jaot.model.entity.MemberWithdrawalHistoryEntity;
import jp.or.jaot.model.entity.RecessHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員エンティティ(複合)<br>
 * ・会員情報と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class MemberCompositeEntity implements Entity {

	/** 会員 */
	private MemberEntity memberEntity = new MemberEntity();

	/** 会員ポータルログイン履歴(前回ログイン) */
	private MemberLoginHistoryEntity lastLoginMemberLoginHistoryEntity;

	/** 会員ポータルログイン履歴(前回ログアウト) */
	private MemberLoginHistoryEntity lastLogoutMemberLoginHistoryEntity;

	/** 会員関連資格情報 */
	private List<MemberQualificationEntity> memberQualificationEntities = new ArrayList<MemberQualificationEntity>();

	/** 会員自治体参画情報 */
	private List<MemberLocalActivityEntity> memberLocalActivityEntities = new ArrayList<MemberLocalActivityEntity>();

	/** 施設登録申請 */
	private FacilityApplicationCompositeEntity facilityApplicationEntity;

	/** 施設養成校 */
	private FacilityCompositeEntity facilityEntity;

	/** 休会履歴 */
	private RecessHistoryEntity recessHistoryEntity;

	/** 退会履歴 */
	private MemberWithdrawalHistoryEntity memberWithdrawalHistoryEntity;

	/** 会員勤務先業務分類 */
	private List<MemberFacilityCategoryEntity> memberFacilityCategoryEntities = new ArrayList<>();

	/** 生涯教育ステータス */
	private LifelongEducationStatusEntity lifelongEducationStatusEntity;
}
