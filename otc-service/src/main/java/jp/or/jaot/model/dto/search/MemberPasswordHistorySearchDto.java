package jp.or.jaot.model.dto.search;

import java.util.Date;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員パスワード履歴検索DTO
 */
@Getter
@Setter
public class MemberPasswordHistorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 仮パスワード */
	private String tempPassword;

	/** 有効期限 */
	private Date tempPasswordExpirationDatetime;

	// TODO : 検索条件追加
}
