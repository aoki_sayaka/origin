package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設分類検索DTO
 */
@Getter
@Setter
public class FacilityCategorySearchDto extends BaseSearchDto {

	/** 施設養成校ID */
	private Long[] facilityIds;

	/** 分類種別 */
	private Integer category;

	/** 分類区分 */
	private Integer categoryDivision;

	/** 分類コード */
	private String categoryCode;
}
