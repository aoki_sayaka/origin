package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校エンティティ<br>
 */
@Entity
@Table(name = "facilities")
@Getter
@Setter
public class FacilityEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 住所１ */
	private String address1;

	/** 住所２ */
	private String address2;

	/** 住所３ */
	private String address3;

	/** 団体・法人名 */
	@Column(name = "corporate_nm")
	private String corporateNm;

	/** 団体・法人名(カナ) */
	@Column(name = "corporate_nm_kana")
	private String corporateNmKana;

	/** 免許取得３年以上 */
	@Column(name = "count_license_3year")
	private Integer countLicense3year;

	/** 免許取得５年以上 */
	@Column(name = "count_license_5year")
	private Integer countLicense5year;

	/** 会員在籍者 */
	@Column(name = "count_member")
	private Integer countMember;

	/** OT在籍者数 */
	@Column(name = "count_ot")
	private Integer countOt;

	/** 課程年限 */
	@Column(name = "course_years")
	private String courseYears;

	/** 昼間・夜間フラグ */
	@Column(name = "day_night_flag")
	private String dayNightFlag;

	/** 学部・学科・専攻 */
	private String department;

	/** 博士課程有無 */
	@Column(name = "doctor_course_flag")
	private String doctorCourseFlag;

	/** 英文表記 */
	@Column(name = "english_notation")
	private String englishNotation;

	/** 在学中フラグ */
	private Boolean enrollment;

	/** 開設者種別 */
	@Column(name = "opener_type_cd")
	private String openerTypeCd;

	/** 開設者名 */
	@Column(name = "opener_nm")
	private String openerNm;

	/** 開設者名(カナ) */
	@Column(name = "opener_nm_kana")
	private String openerNmKana;

	/** 施設基本番号 */
	@Column(name = "facility_base_no")
	private String facilityBaseNo;

	/** 施設区分 */
	@Column(name = "facility_category")
	private String facilityCategory;

	/** 施設番号 */
	@Column(name = "facility_no")
	private String facilityNo;

	/** 施設連番 */
	@Column(name = "facility_seq_no")
	private Integer facilitySeqNo;

	/** FAX番号 */
	@Column(name = "fax_number")
	private String faxNumber;

	/** 修士課程有無 */
	@Column(name = "master_course_flag")
	private String masterCourseFlag;

	/** MTDLP推進協力校フラグ */
	@Column(name = "mtdlp_cooperator_flag")
	private Boolean mtdlpCooperatorFlag;

	/** 施設養成校名(漢字) */
	private String name;

	/** 施設養成校名(カナ) */
	@Column(name = "name_kana")
	private String nameKana;

	/** 施設養成校名２(カナ) */
	@Column(name = "name_kana2")
	private String nameKana2;

	/** 施設養成校名２(漢字) */
	private String name2;

	/** 出身養成校番号 */
	@Column(name = "origin_school_no")
	private Integer originSchoolNo;

	/** 都道府県コード */
	@Column(name = "prefecture_cd")
	private String prefectureCd;

	/** 備考 */
	private String remarks;

	/** 責任者メールアドレス */
	@Column(name = "responsible_email_address")
	private String responsibleEmailAddress;

	/** 施設情報責任者名 */
	@Column(name = "responsible_member_nm")
	private String responsibleMemberNm;

	/** 施設情報責任者名(カナ) */
	@Column(name = "responsible_member_nm_kana")
	private String responsibleMemberNmKana;

	/** 施設情報責任者会員番号 */
	@Column(name = "responsible_member_no")
	private Integer responsibleMemberNo;

	/** 代表電話番号 */
	@Column(name = "tel_no")
	private String telNo;

	/** 養成校種別 */
	@Column(name = "training_school_category_cd")
	private String trainingSchoolCategoryCd;

	/** WFOT認定状況 */
	@Column(name = "wfot_certified_status")
	private Integer wfotCertifiedStatus;

	/** 郵便番号 */
	private String zipcode;
}
