package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.SocialContributionActualResultCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(後輩育成・社会貢献)DTO
 */
@Getter
@Setter
public class SocialContributionActualResultDto
		extends BaseDto<SocialContributionActualResultDto, SocialContributionActualResultCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private SocialContributionActualResultDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private SocialContributionActualResultDto(SocialContributionActualResultCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static SocialContributionActualResultDto create() {
		return new SocialContributionActualResultDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static SocialContributionActualResultDto createSingle() {
		return new SocialContributionActualResultDto(new SocialContributionActualResultCompositeEntity());
	}
}
