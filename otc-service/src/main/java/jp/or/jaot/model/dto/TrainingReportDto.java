package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.TrainingReportCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会報告書DTO
 */
@Getter
@Setter
public class TrainingReportDto extends BaseDto<TrainingReportDto, TrainingReportCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingReportDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingReportDto(TrainingReportCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingReportDto create() {
		return new TrainingReportDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingReportDto createSingle() {
		return new TrainingReportDto(new TrainingReportCompositeEntity());
	}
}
