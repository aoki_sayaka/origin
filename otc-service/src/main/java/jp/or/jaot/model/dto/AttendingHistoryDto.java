package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.AttendingHistoryCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 受講履歴DTO
 */
@Getter
@Setter
//CompositeEntityに変更（複合）
public class AttendingHistoryDto extends BaseDto<AttendingHistoryDto, AttendingHistoryCompositeEntity>{
	
	/**
	 * コンストラクタ
	 */
	private AttendingHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private AttendingHistoryDto(AttendingHistoryCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static AttendingHistoryDto create() {
		return new AttendingHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static AttendingHistoryDto createSingle() {
		return new AttendingHistoryDto(new AttendingHistoryCompositeEntity());
	}

}
