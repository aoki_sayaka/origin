package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.HistoryTableDefinitionEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 履歴テーブル定義DTO
 */
@Getter
@Setter
public class HistoryTableDefinitionDto extends BaseDto<HistoryTableDefinitionDto, HistoryTableDefinitionEntity> {

	/**
	 * コンストラクタ
	 */
	private HistoryTableDefinitionDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private HistoryTableDefinitionDto(HistoryTableDefinitionEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static HistoryTableDefinitionDto create() {
		return new HistoryTableDefinitionDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static HistoryTableDefinitionDto createSingle() {
		return new HistoryTableDefinitionDto(new HistoryTableDefinitionEntity());
	}
}
