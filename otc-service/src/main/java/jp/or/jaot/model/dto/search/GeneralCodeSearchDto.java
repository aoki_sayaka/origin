package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.entity.GeneralCodeId;
import lombok.Getter;
import lombok.Setter;

/**
 * 汎用コード検索DTO
 */
@Getter
@Setter
public class GeneralCodeSearchDto extends BaseSearchDto {

	/** 汎用コードID */
	private GeneralCodeId[] generalCodeIds;
}
