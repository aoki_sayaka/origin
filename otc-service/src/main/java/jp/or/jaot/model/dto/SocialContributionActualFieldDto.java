package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.SocialContributionActualFieldEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績の領域(後輩育成・社会貢献)DTO
 */
@Getter
@Setter
public class SocialContributionActualFieldDto
		extends BaseDto<SocialContributionActualFieldDto, SocialContributionActualFieldEntity> {

	/**
	 * コンストラクタ
	 */
	private SocialContributionActualFieldDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private SocialContributionActualFieldDto(SocialContributionActualFieldEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static SocialContributionActualFieldDto create() {
		return new SocialContributionActualFieldDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static SocialContributionActualFieldDto createSingle() {
		return new SocialContributionActualFieldDto(new SocialContributionActualFieldEntity());
	}
}
