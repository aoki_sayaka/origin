package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修受講履歴DTO
 */
@Getter
@Setter
public class BasicOtTrainingHistoryDto extends BaseDto<BasicOtTrainingHistoryDto, BasicOtTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private BasicOtTrainingHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private BasicOtTrainingHistoryDto(BasicOtTrainingHistoryEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static BasicOtTrainingHistoryDto create() {
		return new BasicOtTrainingHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static BasicOtTrainingHistoryDto createSingle() {
		return new BasicOtTrainingHistoryDto(new BasicOtTrainingHistoryEntity());
		//CompositeEntityに変更（複合）
	}

}
