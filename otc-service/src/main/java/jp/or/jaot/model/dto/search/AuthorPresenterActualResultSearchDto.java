package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(発表・著作等)検索DTO
 */
@Getter
@Setter
public class AuthorPresenterActualResultSearchDto extends BaseSearchDto {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	// TODO : 検索条件追加
}
