package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 領域関連中分類検索DTO
 */
@Getter
@Setter
public class RegionRelatedMiddleClassificationSearchDto extends BaseSearchDto {

	/** コード識別ID */
	private String codeId;

	/** コード値 */
	private String code;

	// TODO : 検索条件追加
}
