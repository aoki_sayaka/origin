package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会申込エンティティ<br>
 */
@Entity
@Table(name = "training_applications")
@Getter
@Setter
public class TrainingApplicationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 繰上打診回答フラグ */
	@Column(name = "advanced_win_consult_flag")
	private String advancedWinConsultFlag;

	/** 申込者分類 */
	@Column(name = "applicant_category_cd")
	private String applicantCategoryCd;

	/** 申込状況 */
	@Column(name = "application_status_cd")
	private String applicationStatusCd;

	/** 症例提供 */
	@Column(name = "case_provide_cd")
	private String caseProvideCd;

	/** 連絡方法 */
	@Column(name = "contact_type")
	private String contactType;

	/** 取消辞退日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "declining_date")
	private Date decliningDate;

	/** 入金期限日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "deposit_period_date")
	private Date depositPeriodDate;

	/** 経験年数 */
	@Column(name = "experience_year")
	private Integer experienceYear;

	/** 氏名 */
	private String fullname;

	/** 氏名(カナ) */
	@Column(name = "fullname_kana")
	private String fullnameKana;

	/** 職種 */
	@Column(name = "job_category_cd")
	private String jobCategoryCd;

	/** 申込者番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 通知日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "notification_date")
	private Date notificationDate;

	/** 電話番号 */
	@Column(name = "phone_number")
	private String phoneNumber;

	/** 手続状況 */
	@Column(name = "procedure_status_cd")
	private String procedureStatusCd;

	/** 整理番号 */
	@Column(name = "queue_no")
	private Integer queueNo;

	/** 備考 */
	private String remarks;

	/** 督促フラグ */
	@Column(name = "reminder_flag")
	private String reminderFlag;

	/** 再提出回数 */
	@Column(name = "resubmission_count")
	private Integer resubmissionCount;

	/** 再提出日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "resubmission_date")
	private Date resubmissionDate;

	/** 対象疾患 */
	@Column(name = "target_disease")
	private String targetDisease;

	/** 研修会番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 勤務先住所1 */
	@Column(name = "work_place_address1")
	private String workPlaceAddress1;

	/** 勤務先住所2 */
	@Column(name = "work_place_address2")
	private String workPlaceAddress2;

	/** 勤務先住所3 */
	@Column(name = "work_place_address3")
	private String workPlaceAddress3;

	/** 勤務先名 */
	@Column(name = "work_place_nm")
	private String workPlaceNm;

	/** 勤務先都道府県 */
	@Column(name = "work_place_prefecture_cd")
	private String workPlacePrefectureCd;

	/** 郵便番号 */
	@Column(name = "work_place_zipcode")
	private String workPlaceZipcode;
}
