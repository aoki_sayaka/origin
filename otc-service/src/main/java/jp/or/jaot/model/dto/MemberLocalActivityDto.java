package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員自治体参画情報DTO
 */
@Getter
@Setter
public class MemberLocalActivityDto extends BaseDto<MemberLocalActivityDto, MemberLocalActivityEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberLocalActivityDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberLocalActivityDto(MemberLocalActivityEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberLocalActivityDto create() {
		return new MemberLocalActivityDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberLocalActivityDto createSingle() {
		return new MemberLocalActivityDto(new MemberLocalActivityEntity());
	}
}
