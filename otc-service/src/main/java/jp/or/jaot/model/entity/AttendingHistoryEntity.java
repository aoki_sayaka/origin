package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 受講履歴エンティティ<br>
 * ・自動生成されたクラスのメンバ変数定義を最大限流用するため、各メンバ変数へのコメントは記述しない<br>
 * ・BaseEntityに定義済みの共通カラムは本エンティティから削除する<br>
 * ・getter/setterもlombokを使用するため本エンティティから削除する<br>
 */
@Entity
@Table(name = "attending_histories")
@Getter
@Setter
public class AttendingHistoryEntity  extends BaseEntity{
	
	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/** 会員番号 */
	@Column(name = "member_no")
	private String memberNo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "attend_date")
	private Date attendDate;

	@Column(name = "category")
	private String category;
	
	@Column(name = "professional_field")
	private String professionalField;
	
	@Column(name = "attend_group")
	private String attendGroup;
	
	@Column(name = "attend_class")
	private String attendClass;
	
	@Column(name = "seq")
	private Integer seq;
	
	@Column(name = "remission_cause_cd")
	private String remissionCause_cd;
	
	@Column(name = "remission_qualification_cd")
	private String remission_QualificationCd;
	
	@Column(name = "acceptance_cd")
	private String acceptanceCd;
	
	@Column(name = "promoter_cd")
	private String promoterCd;
	
	@Column(name = "others_sig_cd")
	private String othersSigCd;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "point_type_cd")
	private String pointTypeCd;
	
	@Column(name = "days")
	private Integer days;
	
	@Column(name = "point")
	private Integer point;
	
	@Column(name = "facility_no")
	private String facilityNo;
	
	@Column(name = "use_point")
	private boolean usePoint;
	
	@Column(name = "collation_member_name")
	private String collationMemberName;
	
	@Column(name = "collation_rate")
	private String collationRate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;
	
	@Column(name = "check_marked")
	private boolean checkMarked;
	
	@Column(name = "remarks")
	private String remarks;

}
