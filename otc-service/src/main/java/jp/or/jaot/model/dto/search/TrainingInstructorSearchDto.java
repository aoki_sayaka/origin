package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会講師実績検索DTO
 */
@Getter
@Setter
public class TrainingInstructorSearchDto extends BaseSearchDto {

	/** 研修会番号 */
	private String[] trainingNos;

	// TODO : 検索条件追加
}
