package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.ClinicalTrainingLeaderSummaryCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者履歴DTO
 */
@Getter
@Setter
public class ClinicalTrainingLeaderSummaryDto
		extends BaseDto<ClinicalTrainingLeaderSummaryDto, ClinicalTrainingLeaderSummaryCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private ClinicalTrainingLeaderSummaryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ClinicalTrainingLeaderSummaryDto(ClinicalTrainingLeaderSummaryCompositeEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ClinicalTrainingLeaderSummaryDto create() {
		return new ClinicalTrainingLeaderSummaryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ClinicalTrainingLeaderSummaryDto createSingle() {
		return new ClinicalTrainingLeaderSummaryDto(new ClinicalTrainingLeaderSummaryCompositeEntity());
		//CompositeEntityに変更（複合）
	}
}
