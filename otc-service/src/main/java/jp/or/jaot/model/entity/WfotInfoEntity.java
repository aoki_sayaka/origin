package jp.or.jaot.model.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 養成校WFOT情報エンティティ<br>
 */
@Entity
@Table(name = "wfot_info")
@Getter
@Setter
public class WfotInfoEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 施設養成校ID */
	@Column(name = "facility_id")
	private Long facilityId;

	/** 施設基本番号 */
	@Column(name = "facility_base_no")
	private String facilityBaseNo;

	/** address1 */
	private String address1;

	/** address2 */
	private String address2;

	/** address3 */
	private String address3;

	/** Duration of course */
	@Column(name = "duration_of_course")
	private String durationOfCourse;

	/** Head of Educational Program */
	@Column(name = "educational_program")
	private String educationalProgram;

	/** Email address */
	@Column(name = "email_address")
	private String emailAddress;

	/** Post Graduate Course Available */
	@Column(name = "post_graduate_course_available")
	private Integer postGraduateCourseAvailable;

	/** Prefecture */
	@Column(name = "prefecture_nm")
	private String prefectureNm;

	/** qualification_awarded */
	@Column(name = "qualification_awarded")
	private String qualificationAwarded;

	/** Name of School */
	@Column(name = "school_nm")
	private String schoolNm;

	/** 連番 */
	@Column(name = "seq_no")
	private Integer seqNo;

	/** Website address */
	@Column(name = "website_address")
	private String websiteAddress;

	/** 登録日時 */
	@Temporal(TemporalType.DATE)
	@Column(name = "wfot_create_date")
	private Date wfotCreateDate;

	/** 削除日付 */
	@Temporal(TemporalType.DATE)
	@Column(name = "wfot_delete_date")
	private Date wfotDeleteDate;

	/** WFOTナンバー */
	@Column(name = "wfot_no")
	private String wfotNo;

	/** 更新日時 */
	@Column(name = "wfot_update_date")
	private Timestamp wfotUpdateDate;

	/** 更新ユーザID */
	@Column(name = "wfot_update_user_id")
	private String wfotUpdateUserId;

	/** Year course discontinued */
	@Column(name = "year_course_discontinued")
	private Integer yearCourseDiscontinued;

	/** Year course accredited by National Association */
	@Column(name = "year_course_accredited_by_na")
	private Integer yearCourseAccreditedByNa;

	/** Year course Commenced */
	@Column(name = "year_course_commenced")
	private Integer yearCourseCommenced;

	/** Year course first/last approved by WFOT */
	@Column(name = "year_course_first_last_approved_by_wfot")
	private Integer yearCourseFirstLastApprovedByWfot;

	/** Year course last reviewed by National Association */
	@Column(name = "year_course_last_reviewed_by_na")
	private Integer yearCourseLastReviewedByNa;

	/** Year course next review/monitoring */
	@Column(name = "year_course_next_review_monitoring")
	private Integer yearCourseNextReviewMonitoring;

	/** Postal Zip Code */
	private String zipcode;
}
