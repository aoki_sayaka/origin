package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント申請エンティティ<br>
 */

@Entity
@Table(name = "other_org_point_applications")
@Getter
@Setter
public class OtherOrgPointApplicationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;
	
	/** 申請日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "application_date")
	private Date applicationDate;
	
	/** 申請者コード */
	@Column(name = "applicant_cd")
	private String applicantCd;

	/** 状態 */
	@Column(name = "status_cd")
	private String statusCd;

	/** 受講日（自） */
	@Temporal(TemporalType.DATE)
	@Column(name = "attending_date_from")
	private Date attendingDateFrom;
	
	/** 受講日（至） */
	@Temporal(TemporalType.DATE)
	@Column(name = "attending_date_to")
	private Date attendingDateTo;	
	
	/** 受講日チェック */
	@Column(name = "attending_date_check")
	private boolean attendingDateCheck;

	/** 研修日数 */
	@Column(name = "attending_days")
	private Integer attendingDays;
	
	/** 研修日数チェック */
	@Column(name = "attending_days_check")	
	private boolean attendingDaysCheck;
	
	/** 主催者コード */
	@Column(name = "organizer_cd")	
	private String organizerCd;
	
	/** 主催者チェック */
	@Column(name = "organizer_check")
	private boolean organizerCheck;
	
	/** 主催者連絡先 */
	@Column(name = "organizer_tel_no")
	private String organizerTelNo;
	
	/** 他団体・SIGコード */
	@Column(name = "other_organization_sig_cd")
	private Integer otherOrganizationSigCd;
	
	/** 他団体・SIGチェック */
	@Column(name = "other_organization_sig_check")
	private boolean otherOrganizationSigCheck;
	
	/** 受講テーマ */
	@Column(name = "thema")
	private String thema;
	
	/** ポイント種別 */
	@Column(name = "point_category_cd")
	private String pointCategoryCd;
	
	/** ポイント種別チェック */
	@Column(name = "point_category_check")
	private boolean pointCategoryCheck;
	
	/** ポイント */
	@Column(name = "point")
	private Integer point;
	
	/** ポイントチェック */
	@Column(name = "point_check")
	private boolean pointCheck;
	
	/** 添付ファイルチェック */
	@Column(name = "attachment_file_check")
	private boolean attachmentFileCheck;
	
	/** 承認フラグ */
	@Column(name = "approval_flag")
	private String approvalFlag;
	
	/** 事務局確認日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "secretariat_confirm_date")
	private Date secretariatConfirmDate;
	
	/** 仮登録フラグ */
	@Column(name = "temp_flag")
	private Integer tempFlag;
	
	

}
