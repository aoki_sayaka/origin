package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

/**
 * 県士会会費納入エンティティ<br>
 */
@Entity
@Table(name = "paot_membership_fee_deliveries")
@Getter
@Setter
public class PaotMembershipFeeDeliveryEntity implements jp.or.jaot.core.model.Entity {

	/** ID */
	@Id
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 県士会番号 */
	@Column(name = "paot_cd")
	private Integer paotCd;

	/** 年度 */
	@Column(name = "fisical_year")
	private Integer fisicalYear;

	/** 会費区分コード */
	@Column(name = "category_cd")
	private String categoryCd;

	/** 請求額 */
	@Column(name = "billing_amount")
	private Integer billingAmount;

	/** 入金額 */
	@Column(name = "payment_amount")
	private Integer paymentAmount;

	/** 請求残額 */
	@Column(name = "billing_balance")
	private Integer billingBalance;

	/** 入金日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;

	/** 備考 */
	private String remarks;
}