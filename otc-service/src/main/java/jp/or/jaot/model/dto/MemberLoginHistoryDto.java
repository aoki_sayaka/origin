package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberLoginHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員ポータルログイン履歴DTO
 */
@Getter
@Setter
public class MemberLoginHistoryDto extends BaseDto<MemberLoginHistoryDto, MemberLoginHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberLoginHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberLoginHistoryDto(MemberLoginHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberLoginHistoryDto create() {
		return new MemberLoginHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberLoginHistoryDto createSingle() {
		return new MemberLoginHistoryDto(new MemberLoginHistoryEntity());
	}
}
