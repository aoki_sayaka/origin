package jp.or.jaot.model.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 年度別情報エンティティ<br>
 */
@Entity
@Table(name = "info_by_fiscal_year")
@Getter
@Setter
public class InfoByFiscalYearEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 施設養成校ID */
	@Column(name = "facility_id")
	private Long facilityId;

	/** 施設基本番号 */
	@Column(name = "facility_base_no")
	private String facilityBaseNo;

	/** 年度 */
	@Column(name = "fiscal_year")
	private Integer fiscalYear;

	/** 更新ユーザID */
	@Column(name = "fiscal_year_update_user_id")
	private String fiscalYearUpdateUserId;

	/** 更新日時 */
	@Column(name = "fiscal_year_update_date")
	private Timestamp fiscalYearUpdateDate;

	/** 更新連番 */
	@Column(name = "update_no")
	private Integer updateNo;

	/** 登録日付 */
	@Temporal(TemporalType.DATE)
	@Column(name = "fiscal_year_create_date")
	private Date fiscalYearCreateDate;

	/** 削除日付 */
	@Temporal(TemporalType.DATE)
	@Column(name = "fiscal_year_delete_date")
	private Date fiscalYearDeleteDate;

	/** 入学定員 */
	@Column(name = "admission_count")
	private Integer admissionCount;

	/** 卒業定員 */
	@Column(name = "graduate_count")
	private Integer graduateCount;

	/** 国家試験受験者数 */
	@Column(name = "entrance_exam_count")
	private Integer entranceExamCount;

	/** 国家試験合格者数 */
	@Column(name = "pass_exam_count")
	private Integer passExamCount;

	/** 国家試験合格率 */
	@Column(name = "pass_exam_rate")
	private BigDecimal passExamRate;

	/** 入会率 */
	@Column(name = "join_rate")
	private BigDecimal joinRate;
}
