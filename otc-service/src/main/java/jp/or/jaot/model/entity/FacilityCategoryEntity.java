package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設分類エンティティ<br>
 */
@Entity
@Table(name = "facility_categories")
@Getter
@Setter
public class FacilityCategoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 施設養成校ID */
	@Column(name = "facility_id")
	private Long facilityId;

	/** 分類種別 */
	private Integer category;

	/** 分類区分 */
	@Column(name = "category_division")
	private Integer categoryDivision;

	/** 親分類コード */
	@Column(name = "parent_code")
	private String parentCode;

	/** 分類コード */
	@Column(name = "category_code")
	private String categoryCode;

	/** その他 */
	private String other;
}
