package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.FacilityCategoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設分類DTO
 */
@Getter
@Setter
public class FacilityCategoryDto extends BaseDto<FacilityCategoryDto, FacilityCategoryEntity> {

	/**
	 * コンストラクタ
	 */
	private FacilityCategoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private FacilityCategoryDto(FacilityCategoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static FacilityCategoryDto create() {
		return new FacilityCategoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static FacilityCategoryDto createSingle() {
		return new FacilityCategoryDto(new FacilityCategoryEntity());
	}
}
