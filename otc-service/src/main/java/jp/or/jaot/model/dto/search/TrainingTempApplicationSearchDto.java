package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込検索DTO
 */
@Getter
@Setter
public class TrainingTempApplicationSearchDto extends BaseSearchDto {

	/** ID */
	private Long[] ids;

	/** 研修会番号 */
	private String[] trainingNos;

	// TODO : 検索条件追加
}
