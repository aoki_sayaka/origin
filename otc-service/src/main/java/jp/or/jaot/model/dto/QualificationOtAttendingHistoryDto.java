package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.QualificationOtAttendingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴DTO
 */
@Getter
@Setter
public class QualificationOtAttendingHistoryDto extends BaseDto<QualificationOtAttendingHistoryDto, QualificationOtAttendingHistoryEntity>{
	
	/**
	 * コンストラクタ
	 */
	private QualificationOtAttendingHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private QualificationOtAttendingHistoryDto(QualificationOtAttendingHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static QualificationOtAttendingHistoryDto create() {
		return new QualificationOtAttendingHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static QualificationOtAttendingHistoryDto createSingle() {
		return new QualificationOtAttendingHistoryDto(new QualificationOtAttendingHistoryEntity());
	}

}
