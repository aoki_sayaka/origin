package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 汎用コードエンティティ<br>
 */
@Entity
@Table(name = "general_codes")
@Getter
@Setter
public class GeneralCodeEntity extends BaseEntity {

	/** 汎用コードID(複合キー) */
	@EmbeddedId
	private GeneralCodeId generalCodeId;

	/** 内容 */
	private String content;

	/** 省略内容 */
	@Column(name = "simple_content")
	private String simpleContent;

	/** 親コード値 */
	@Column(name = "parent_code")
	private String parentCode;

	/** 表示順序 */
	@Column(name = "sort_order")
	private Integer sortOrder;
}