package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 生涯教育ステータスエンティティ<br>
 */
@Entity
@Table(name = "lifelong_education_statuses")
@Getter
@Setter
public class LifelongEducationStatusEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 現在のポイント */
	@Column(name = "point")
	private Integer point;

	/** 使用済みポイント */
	@Column(name = "used_point")
	private Integer usedPoint;
}
