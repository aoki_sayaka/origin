package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.RegionRelatedSmallClassificationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 領域関連小分類DTO
 */
@Getter
@Setter
public class RegionRelatedSmallClassificationDto
		extends BaseDto<RegionRelatedSmallClassificationDto, RegionRelatedSmallClassificationEntity> {

	/**
	 * コンストラクタ
	 */
	private RegionRelatedSmallClassificationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private RegionRelatedSmallClassificationDto(RegionRelatedSmallClassificationEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static RegionRelatedSmallClassificationDto create() {
		return new RegionRelatedSmallClassificationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static RegionRelatedSmallClassificationDto createSingle() {
		return new RegionRelatedSmallClassificationDto(new RegionRelatedSmallClassificationEntity());
	}
}
