package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberRevokedAuthorityEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員権限DTO
 */
@Getter
@Setter
public class MemberRevokedAuthorityDto extends BaseDto<MemberRevokedAuthorityDto, MemberRevokedAuthorityEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberRevokedAuthorityDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberRevokedAuthorityDto(MemberRevokedAuthorityEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberRevokedAuthorityDto create() {
		return new MemberRevokedAuthorityDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberRevokedAuthorityDto createSingle() {
		return new MemberRevokedAuthorityDto(new MemberRevokedAuthorityEntity());
	}
}
