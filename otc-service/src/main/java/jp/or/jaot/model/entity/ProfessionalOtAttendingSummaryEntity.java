package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士受講履歴エンティティ<br>
 */
@Entity
@Table(name = "professional_ot_attending_summaries")
@Getter
@Setter
public class ProfessionalOtAttendingSummaryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 専門分野 */
	@Column(name = "professional_field")
	private String professionalField;

	/** 認定年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "qualification_date")
	private Date qualificationDate;

	/** 認定期間満了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "qualification_period_date")
	private Date qualificationPeriodDate;

	/** 認定番号 */
	@Column(name = "qualification_number")
	private Integer qualificationNumber;

	/** 更新回数 */
	@Column(name = "renewal_count")
	private Integer renewalCount;

	/** 認知症ケア専門士 */
	@Column(name = "dementia_care_specialist")
	private boolean dementiaCareSpecialist;

	/** 認知症ケア実務者研修修了 */
	@Column(name = "dementia_care_trained")
	private boolean dementiaCareTrained;

	/** 教員免許取得 */
	@Column(name = "teacher_license")
	private boolean teacherLicense;

	/** 認定訪問療法士 */
	@Column(name = "qualified_visiting_ot")
	private boolean qualifiedVisitingOt;

	/** 訪問リハビリテーション:管理研修①②③ */
	@Column(name = "visit_rehabilitation_training123")
	private boolean visitRehabilitationTraining123;

	/** リンパ浮腫セラピスト */
	@Column(name = "lymphedema_therapist")
	private boolean lymphedemaTherapist;
}
