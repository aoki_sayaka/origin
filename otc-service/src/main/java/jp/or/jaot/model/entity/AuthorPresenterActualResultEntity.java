package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(発表・著作等)エンティティ<br>
 */
@Entity
@Table(name = "author_presenter_actual_results")
@Getter
@Setter
public class AuthorPresenterActualResultEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 登録者区分 */
	@Column(name = "register_category_cd")
	private String registerCategoryCd;

	/** 発表著作内容コード */
	@Column(name = "activity_content_cd")
	private String activityContentCd;

	/** 活動詳細 */
	@Column(name = "activity_detail")
	private String activityDetail;

	/** 著者/発表者区分コード */
	@Column(name = "author_presenter_category_cd")
	private String authorPresenterCategoryCd;

	/** 発行発表年月日 */
	@Column(name = "publication_ym")
	private Integer publicationYm;

	/** 巻 */
	private String volume;

	/** 号 */
	private String issue;

	/** 掲載開始ページ */
	@Column(name = "start_page")
	private Integer startPage;

	/** 掲載終了ページ */
	@Column(name = "end_page")
	private Integer endPage;

	/** 発行者出版社 */
	private String publisher;

	/** 雑誌学術誌名 */
	@Column(name = "journal_name")
	private String journalName;

	/** 大会名 */
	@Column(name = "convention_name")
	private String conventionName;

	/** 開催回 */
	@Column(name = "held_times")
	private Integer heldTimes;

	/** 開催地 */
	private String venue;

	/** 団体名 */
	@Column(name = "group_name")
	private String groupName;

	/** 他学会発表抄録掲載開始ページ */
	@Column(name = "other_start_page")
	private Integer otherStartPage;

	/** 他学会発表抄録掲載終了ページ */
	@Column(name = "other_end_page")
	private Integer otherEndPage;

	/** ISBN/ISSN */
	@Column(name = "isbn_issn")
	private String isbnIssn;

	/** 発表区分コード */
	@Column(name = "publish_category_cd")
	private String publishCategoryCd;
}
