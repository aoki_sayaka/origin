package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 郵便番号検索DTO
 */
@Getter
@Setter
public class ZipcodeSearchDto extends BaseSearchDto {

	/** 郵便番号 */
	private String zipcode;

	/** 住所 */
	private String address;

	/** 都道府県番号 */
	private String prefectureCd;

	/** 都道府県名 */
	private String prefectureName;
}
