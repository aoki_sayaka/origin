package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員権限エンティティ<br>
 */
@Entity
@Table(name = "member_revoked_authorities")
@Getter
@Setter
public class MemberRevokedAuthorityEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** コード識別ID */
	@Column(name = "code_id")
	private String codeId;

	/** コード値 */
	private String code;

	/** パス/キー値 */
	@Column(name = "path_or_key")
	private String pathOrKey;
}
