package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderQualifyHistoryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.MemberLocalActivityEntity;
import jp.or.jaot.model.entity.MemberQualificationEntity;
import jp.or.jaot.model.entity.MtdlpCaseEntity;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;
import jp.or.jaot.model.entity.MtdlpSummaryEntity;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.RecessHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 受講履歴エンティティ(複合)<br>
 * ・受講履歴詳細画面の各タブと関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class AttendingHistoryCompositeEntity implements Entity {
	
	// 基礎研修履歴
	private BasicOtAttendingSummaryEntity basicOtAttendingSummaryEntity = new BasicOtAttendingSummaryEntity();

	// 基礎研修修了認定更新履歴
	private List<BasicOtQualifyHistoryEntity> basicOtQualifyHistoryEntities = new ArrayList<BasicOtQualifyHistoryEntity>();

	// 基礎研修修了認定更新履歴
	private List<BasicOtTrainingHistoryEntity> basicOtTrainingHistoryEntities = new ArrayList<BasicOtTrainingHistoryEntity>();

	// 認定作業療法士研修履歴
	private QualifiedOtAttendingSummaryEntity qualifiedOtAttendingSummaryEntity = new QualifiedOtAttendingSummaryEntity();

	// 認定作業療法士研修修了認定更新履歴
	private List<QualifiedOtCompletionHistoryEntity> qualifiedOtCompletionHistoryEntities = new ArrayList<QualifiedOtCompletionHistoryEntity>();

	// 認定作業療法士研修受講履歴
	private List<QualifiedOtTrainingHistoryEntity> qualifiedOtTrainingHistoryEntities = new ArrayList<QualifiedOtTrainingHistoryEntity>();

	// 専門作業療法士受講履歴
	private ProfessionalOtAttendingSummaryEntity professionalOtAttendingSummaryEntity = new ProfessionalOtAttendingSummaryEntity();

	// 専門作業療法士認定更新履歴
	private List<ProfessionalOtQualifyHistoryEntity> professionalOtQualifyHistoryEntities = new ArrayList<ProfessionalOtQualifyHistoryEntity>();

	// 専門作業療法士試験結果履歴
	private List<ProfessionalOtTestHistoryEntity> professionalOtTestHistoryEntities = new ArrayList<ProfessionalOtTestHistoryEntity>();
	
	// 専門作業療法士研修受講履歴
	private List<ProfessionalOtTrainingHistoryEntity> professionalOtTrainingHistoryEntities = new ArrayList<ProfessionalOtTrainingHistoryEntity>();
	
	// 臨床実習指導者履歴
	private ClinicalTrainingLeaderSummaryEntity clinicalTrainingLeaderSummaryEntity = new ClinicalTrainingLeaderSummaryEntity();

	// 臨床実習指導者認定履歴
	private List<ClinicalTrainingLeaderQualifyHistoryEntity> clinicalTrainingLeaderQualifyHistoryEntities = new ArrayList<ClinicalTrainingLeaderQualifyHistoryEntity>();

	// 臨床実習指導者研修受講履歴
	private List<ClinicalTrainingLeaderTrainingHistoryEntity> clinicalTrainingLeaderTrainingHistoryEntities = new ArrayList<ClinicalTrainingLeaderTrainingHistoryEntity>();

	// MTDLP履歴
	private MtdlpSummaryEntity mtdlpSummaryEntity = new MtdlpSummaryEntity();

	// MTDLP認定履歴
	private List<MtdlpQualifyHistoryEntity> mtdlpQualifyHistoryEntities = new ArrayList<MtdlpQualifyHistoryEntity>();

	// MTDLP研修受講履歴
	private List<MtdlpTrainingHistoryEntity> mtdlpTrainingHistoryEntities = new ArrayList<MtdlpTrainingHistoryEntity>();

	// MTDLP事例
	private List<MtdlpCaseEntity> mtdlpCaseEntity = new ArrayList<MtdlpCaseEntity>();
	
	// 会員
	private List<MemberEntity> memberEntity = new ArrayList<MemberEntity>();

	// 会員（仮）
	private MemberEntity memberEntity2 = new MemberEntity();
	
	// 会員関連資格情報
	private List<MemberQualificationEntity> memberQualificationEntities = new ArrayList<MemberQualificationEntity>();

	// 会員自治体参画情報
	private List<MemberLocalActivityEntity> memberLocalActivityEntities = new ArrayList<MemberLocalActivityEntity>();
	
	// 休会履歴
	private List<RecessHistoryEntity> recessHistory = new ArrayList<RecessHistoryEntity>();

	}


