package jp.or.jaot.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせエンティティ<br>
 */
@Entity
@Table(name = "notices")
@Getter
@Setter
public class NoticeEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 対象 */
	private Integer target;

	/** カテゴリ */
	private Integer category;

	/** 重要 */
	private Boolean priority;

	/** 掲載開始日 */
	@Column(name = "publish_from")
	private Timestamp publishFrom;

	/** 掲載終了日 */
	@Column(name = "publish_to")
	private Timestamp publishTo;

	/** システムメッセージ */
	@Column(name = "system_message")
	private Boolean systemMessage;

	/** 件名 */
	private String subject;

	/** 内容 */
	private String content;

	/** 宛先 */
	private Integer destination;

	/** 宛先区分 */
	@Column(name = "destination_type")
	private Integer destinationType;

	/** 宛先ファイル名 */
	@Column(name = "destination_file_nm")
	private String destinationFileNm;

	/** 宛先範囲施設 */
	@Column(name = "destination_range_facility")
	private Boolean destinationRangeFacility;

	/** 宛先範囲養成校 */
	@Column(name = "destination_range_training_school")
	private Boolean destinationRangeTrainingSchool;
}
