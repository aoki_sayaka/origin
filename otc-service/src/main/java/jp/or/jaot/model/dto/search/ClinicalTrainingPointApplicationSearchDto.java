package jp.or.jaot.model.dto.search;

import java.util.Date;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習ポイント申請検索DTO
 */
@Getter
@Setter
public class ClinicalTrainingPointApplicationSearchDto extends BaseSearchDto {

	/** 申請日(From) */
	private Date applicationDateFrom;

	/** 申請日(To) */
	private Date applicationDateTo;

	/** 申請状態 */
	private String applicationStatusCd;

	/** 申請者 会員番号(From) */
	private Integer memberNoFrom;

	/** 申請者 会員番号(To) */
	private Integer memberNoTo;
}
