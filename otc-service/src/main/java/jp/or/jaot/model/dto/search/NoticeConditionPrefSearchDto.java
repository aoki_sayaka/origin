package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(都道府県)検索DTO
 */
@Getter
@Setter
public class NoticeConditionPrefSearchDto extends BaseSearchDto {

	/** お知らせID */
	private Long[] noticeIds;

	// TODO : 検索条件追加
}
