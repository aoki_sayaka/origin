package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.HandbookMigrationApplicationEntity;
import jp.or.jaot.model.entity.MemberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 手帳移行申請エンティティ(複合)<br>
 * ・手帳移行申請と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class HandbookMigrationApplicationCompositeEntity implements Entity {

	// 手帳移行申請
	private HandbookMigrationApplicationEntity handbookMigrationApplicationEntity = new HandbookMigrationApplicationEntity();

	// 会員情報
	private List<MemberEntity> memberEntities = new ArrayList<MemberEntity>();
	
	// 養成校情報
	private List<FacilityEntity> facilityEntities = new ArrayList<FacilityEntity>();

}
