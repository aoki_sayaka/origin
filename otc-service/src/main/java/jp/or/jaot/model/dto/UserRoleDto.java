package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.UserRoleEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ロールDTO
 */
@Getter
@Setter
public class UserRoleDto extends BaseDto<UserRoleDto, UserRoleEntity> {

	/**
	 * コンストラクタ
	 */
	private UserRoleDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private UserRoleDto(UserRoleEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static UserRoleDto create() {
		return new UserRoleDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static UserRoleDto createSingle() {
		return new UserRoleDto(new UserRoleEntity());
	}
}
