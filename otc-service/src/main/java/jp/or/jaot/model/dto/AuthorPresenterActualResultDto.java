package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.AuthorPresenterActualResultCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(発表・著作等)DTO
 */
@Getter
@Setter
public class AuthorPresenterActualResultDto
		extends BaseDto<AuthorPresenterActualResultDto, AuthorPresenterActualResultCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private AuthorPresenterActualResultDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private AuthorPresenterActualResultDto(AuthorPresenterActualResultCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static AuthorPresenterActualResultDto create() {
		return new AuthorPresenterActualResultDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static AuthorPresenterActualResultDto createSingle() {
		return new AuthorPresenterActualResultDto(new AuthorPresenterActualResultCompositeEntity());
	}
}
