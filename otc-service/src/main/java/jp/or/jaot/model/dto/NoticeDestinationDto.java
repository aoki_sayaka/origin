package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.NoticeDestinationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(宛先)DTO
 */
@Getter
@Setter
public class NoticeDestinationDto extends BaseDto<NoticeDestinationDto, NoticeDestinationEntity> {

	/**
	 * コンストラクタ
	 */
	private NoticeDestinationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private NoticeDestinationDto(NoticeDestinationEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static NoticeDestinationDto create() {
		return new NoticeDestinationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static NoticeDestinationDto createSingle() {
		return new NoticeDestinationDto(new NoticeDestinationEntity());
	}
}
