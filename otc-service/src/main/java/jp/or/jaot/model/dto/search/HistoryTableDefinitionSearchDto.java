package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 履歴テーブル定義検索DTO
 */
@Getter
@Setter
public class HistoryTableDefinitionSearchDto extends BaseSearchDto {

	/** テーブル名(物理) */
	private String tablePhysicalNm;

	// TODO : 検索条件追加
}
