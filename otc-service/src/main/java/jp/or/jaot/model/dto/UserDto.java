package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.UserEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ユーザDTO
 */
@Getter
@Setter
public class UserDto extends BaseDto<UserDto, UserEntity> {

	/**
	 * コンストラクタ
	 */
	private UserDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private UserDto(UserEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static UserDto create() {
		return new UserDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static UserDto createSingle() {
		return new UserDto(new UserEntity());
	}
}
