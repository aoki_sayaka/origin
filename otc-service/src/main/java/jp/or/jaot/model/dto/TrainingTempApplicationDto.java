package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.TrainingTempApplicationCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込DTO
 */
@Getter
@Setter
public class TrainingTempApplicationDto
		extends BaseDto<TrainingTempApplicationDto, TrainingTempApplicationCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingTempApplicationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingTempApplicationDto(TrainingTempApplicationCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingTempApplicationDto create() {
		return new TrainingTempApplicationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingTempApplicationDto createSingle() {
		return new TrainingTempApplicationDto(new TrainingTempApplicationCompositeEntity());
	}
}
