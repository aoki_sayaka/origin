package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.UserLoginHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局サイトログイン履歴DTO
 */
@Getter
@Setter
public class UserLoginHistoryDto extends BaseDto<UserLoginHistoryDto, UserLoginHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private UserLoginHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private UserLoginHistoryDto(UserLoginHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static UserLoginHistoryDto create() {
		return new UserLoginHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static UserLoginHistoryDto createSingle() {
		return new UserLoginHistoryDto(new UserLoginHistoryEntity());
	}
}
