package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberRoleEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員ロールDTO
 */
@Getter
@Setter
public class MemberRoleDto extends BaseDto<MemberRoleDto, MemberRoleEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberRoleDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberRoleDto(MemberRoleEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberRoleDto create() {
		return new MemberRoleDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberRoleDto createSingle() {
		return new MemberRoleDto(new MemberRoleEntity());
	}
}
