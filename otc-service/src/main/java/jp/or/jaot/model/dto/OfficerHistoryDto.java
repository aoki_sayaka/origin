package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.OfficerHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 役員履歴DTO
 */
@Getter
@Setter
public class OfficerHistoryDto extends BaseDto<OfficerHistoryDto, OfficerHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private OfficerHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private OfficerHistoryDto(OfficerHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static OfficerHistoryDto create() {
		return new OfficerHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static OfficerHistoryDto createSingle() {
		return new OfficerHistoryDto(new OfficerHistoryEntity());
	}
}
