package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.DepartmentHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 部署履歴DTO
 */
@Getter
@Setter
public class DepartmentHistoryDto extends BaseDto<DepartmentHistoryDto, DepartmentHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private DepartmentHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private DepartmentHistoryDto(DepartmentHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static DepartmentHistoryDto create() {
		return new DepartmentHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static DepartmentHistoryDto createSingle() {
		return new DepartmentHistoryDto(new DepartmentHistoryEntity());
	}
}
