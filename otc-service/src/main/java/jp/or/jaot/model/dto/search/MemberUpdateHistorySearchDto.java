package jp.or.jaot.model.dto.search;

import java.sql.Timestamp;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員更新履歴検索DTO
 */
@Getter
@Setter
public class MemberUpdateHistorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 更新区分コード */
	private Integer updateCategoryCd;

	/** テーブルコード */
	private Integer tableCd;

	/** 項目コード */
	private Integer[] fieldCds;

	/** 更新日(From) */
	private Timestamp updateFromDate;

	/** 更新日(To) */
	private Timestamp updateToDate;
}
