package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberPasswordHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員パスワード履歴DTO
 */
@Getter
@Setter
public class MemberPasswordHistoryDto extends BaseDto<MemberPasswordHistoryDto, MemberPasswordHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberPasswordHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberPasswordHistoryDto(MemberPasswordHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberPasswordHistoryDto create() {
		return new MemberPasswordHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberPasswordHistoryDto createSingle() {
		return new MemberPasswordHistoryDto(new MemberPasswordHistoryEntity());
	}
}
