package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.TrainingNumberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修履歴DTO
 */
@Getter
@Setter
public class TrainingNumberDto
		extends BaseDto<TrainingNumberDto, TrainingNumberEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingNumberDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingNumberDto(TrainingNumberEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingNumberDto create() {
		return new TrainingNumberDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingNumberDto createSingle() {
		return new TrainingNumberDto(new TrainingNumberEntity());
		//Entityに変更（複合）
	}

}
