package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.PaotMembershipFeeDeliveryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 県士会会費納入DTO
 */
@Getter
@Setter
public class PaotMembershipFeeDeliveryDto
		extends BaseDto<PaotMembershipFeeDeliveryDto, PaotMembershipFeeDeliveryEntity> {

	/**
	 * コンストラクタ
	 */
	private PaotMembershipFeeDeliveryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private PaotMembershipFeeDeliveryDto(PaotMembershipFeeDeliveryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static PaotMembershipFeeDeliveryDto create() {
		return new PaotMembershipFeeDeliveryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static PaotMembershipFeeDeliveryDto createSingle() {
		return new PaotMembershipFeeDeliveryDto(new PaotMembershipFeeDeliveryEntity());
	}
}
