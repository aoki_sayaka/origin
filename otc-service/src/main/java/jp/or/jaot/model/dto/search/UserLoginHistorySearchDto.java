package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局サイトログイン履歴検索DTO
 */
@Getter
@Setter
public class UserLoginHistorySearchDto extends BaseSearchDto {

	/** 事務局ユーザID */
	private Long userId;
}
