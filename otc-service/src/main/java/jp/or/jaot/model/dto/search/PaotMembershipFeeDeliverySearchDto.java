package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 県士会会費納入検索DTO
 */
@Getter
@Setter
public class PaotMembershipFeeDeliverySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	// TODO : 検索条件追加

}
