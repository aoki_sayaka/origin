package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員勤務先業務分類検索DTO
 */
@Getter
@Setter
public class MemberFacilityCategorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

}
