package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 会費納入検索DTO
 */
@Getter
@Setter
public class MembershipFeeDeliverySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 年度 */
	private Integer fiscalYear;

	// TODO : 検索条件追加
}
