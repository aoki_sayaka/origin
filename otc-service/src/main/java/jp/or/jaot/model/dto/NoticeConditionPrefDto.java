package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.NoticeConditionPrefEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(都道府県)DTO
 */
@Getter
@Setter
public class NoticeConditionPrefDto extends BaseDto<NoticeConditionPrefDto, NoticeConditionPrefEntity> {

	/**
	 * コンストラクタ
	 */
	private NoticeConditionPrefDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private NoticeConditionPrefDto(NoticeConditionPrefEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static NoticeConditionPrefDto create() {
		return new NoticeConditionPrefDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static NoticeConditionPrefDto createSingle() {
		return new NoticeConditionPrefDto(new NoticeConditionPrefEntity());
	}
}
