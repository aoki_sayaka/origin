package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会運営担当者エンティティ<br>
 */
@Entity
@Table(name = "training_operators")
@Getter
@Setter
public class TrainingOperatorEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 研修会番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 会員氏名 */
	@Column(name = "member_nm")
	private String memberNm;

	/** 表示順 */
	@Column(name = "display_seq_no")
	private Integer displaySeqNo;

	/** 運営担当者区分 */
	@Column(name = "category_cd")
	private String categoryCd;

	/** 運営担当者問合せ担当区分 */
	@Column(name = "contact_category_cd")
	private String contactCategoryCd;

	/** 運営担当者上長宛公文書有無 */
	@Column(name = "doc_to_superiors_cd")
	private String docToSuperiorsCd;

	/** 運営担当者上長宛公文書印刷フラグ */
	@Column(name = "doc_to_superiors_print_flag")
	private Boolean docToSuperiorsPrintFlag;

	/** 運営担当者上長施設名 */
	@Column(name = "doc_to_superiors_facility_nm")
	private String docToSuperiorsFacilityNm;

	/** 運営担当者上長宛名 */
	@Column(name = "doc_to_superiors_nm")
	private String docToSuperiorsNm;

	/** 運営担当者上長宛公文書発送日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "doc_to_superiors_send_date")
	private Date docToSuperiorsSendDate;

	/** 運営担当者本人宛公文書有無 */
	@Column(name = "doc_to_person_cd")
	private String docToPersonCd;

	/** 運営担当者本人宛公文書印刷フラグ */
	@Column(name = "doc_to_person_print_flag")
	private Boolean docToPersonPrintFlag;

	/** 運営担当者本人施設名 */
	@Column(name = "doc_to_person_facility_nm")
	private String docToPersonFacilityNm;

	/** 運営担当者本人宛名 */
	@Column(name = "doc_to_person_nm")
	private String docToPersonNm;

	/** 運営担当者本人公文書発送日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "doc_to_person_send_date")
	private Date docToPersonSendDate;

	/** 士会事務局ユーザID */
	@Column(name = "secretariat_user_id")
	private String secretariatUserId;
}
