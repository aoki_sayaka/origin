package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ロールエンティティ<br>
 */
@Entity
@Table(name = "user_roles")
@Getter
@Setter
public class UserRoleEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 事務局ユーザID */
	@Column(name = "user_id")
	private Long userId;

	/** 権限コード識別ID */
	@Column(name = "role_code_id")
	private String roleCodeId;

	/** 権限コード値 */
	@Column(name = "role_code")
	private String roleCode;
}
