package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.BasicPointTrainingHistoryEntity;
import jp.or.jaot.model.entity.LifelongEducationStatusEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎ポイント研修履歴エンティティ<br>
 * ・基礎研修履歴と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class BasicPointTrainingHistoryCompositeEntity implements Entity {

	// 基礎ポイント研修履歴
	private List<BasicPointTrainingHistoryEntity> basicPointTrainingHistoryEntities = new ArrayList<BasicPointTrainingHistoryEntity>();

	// 生涯教育ステータス
	private LifelongEducationStatusEntity lifelongEducationStatusesEntity = new LifelongEducationStatusEntity();

}
