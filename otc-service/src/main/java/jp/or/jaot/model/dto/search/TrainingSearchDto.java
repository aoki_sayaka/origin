package jp.or.jaot.model.dto.search;

import java.util.Date;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会検索DTO
 */
@Getter
@Setter
public class TrainingSearchDto extends BaseSearchDto {

	/** ID */
	private Long[] ids;

	/** 研修会番号 */
	private String trainingNo;

	/** 研修会番号(複数) */
	private String[] trainingNos;

	/** 研修会番号(抽出条件) */
	private MATCH_CONDITION matchCondTrainingNo = MATCH_CONDITION.EQ; // 完全一致

	/** 開催年度 */
	private Integer holdFiscalYear;

	/** 主催者 */
	private String[] organizerCds;

	/** 研修会種別 */
	private String[] trainingTypeCds;

	/** 講座分類 */
	private String[] courseCategoryCds;

	/** 講座名 */
	private String courseNm;

	/** 受付状況 */
	private String[] receptionStatusCds;

	/** 更新状況 */
	private String[] updateStatusCds;

	/** 開催地 */
	private String[] venuePrefCds;

	/** 開催日(From) */
	private Date eventDateFrom;

	/** 開催日(To) */
	private Date eventDateTo;

	/** e-learningフラグ */
	private Boolean elearningFlag;

	/** 終了フラグ */
	private Boolean endFlag;

	/** キャンセルフラグ */
	private Boolean cancelFlag;

	/** 通知ありフラグ */
	private Boolean notificationFlag;

	// TODO : 検索条件追加
}
