package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修番号マスタエンティティ<br>
 */
@Entity
@Table(name = "training_numbers")
@Getter
@Setter
public class TrainingNumberEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 年度 */
	@Column(name = "fiscal_year")
	private Integer fiscalYear;

	/** 研修番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 内容 */
	@Column(name = "content")
	private String content;
}
