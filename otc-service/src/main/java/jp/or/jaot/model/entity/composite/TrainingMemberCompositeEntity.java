package jp.or.jaot.model.entity.composite;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.MemberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会会員エンティティ(複合)<br>
 * ・研修会会員と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 * ・リスト項目表示に必要な値を返却する<br>
 */
@Getter
@Setter
public class TrainingMemberCompositeEntity implements Entity {

	/** 会員 */
	private MemberEntity memberEntity = new MemberEntity();

	// TODO
}
