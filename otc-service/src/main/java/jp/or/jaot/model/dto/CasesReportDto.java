package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.CasesReportEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事例報告DTO
 */
@Getter
@Setter
public class CasesReportDto extends BaseDto<CasesReportDto, CasesReportEntity> {

	/**
	 * コンストラクタ
	 */
	private CasesReportDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private CasesReportDto(CasesReportEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static CasesReportDto create() {
		return new CasesReportDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static CasesReportDto createSingle() {
		return new CasesReportDto(new CasesReportEntity());
	}
}
