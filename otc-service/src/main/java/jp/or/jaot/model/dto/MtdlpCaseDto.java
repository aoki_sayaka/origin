package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MtdlpCaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP事例DTO
 */
@Getter
@Setter
public class MtdlpCaseDto extends BaseDto<MtdlpCaseDto, MtdlpCaseEntity> {

	/**
	 * コンストラクタ
	 */
	private MtdlpCaseDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MtdlpCaseDto(MtdlpCaseEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MtdlpCaseDto create() {
		return new MtdlpCaseDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MtdlpCaseDto createSingle() {
		return new MtdlpCaseDto(new MtdlpCaseEntity());
	}
}
