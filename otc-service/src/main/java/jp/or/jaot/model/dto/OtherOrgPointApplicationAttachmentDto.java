package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.OtherOrgPointApplicationAttachmentEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント添付ファイルDTO
 */
@Getter
@Setter
public class OtherOrgPointApplicationAttachmentDto extends BaseDto<OtherOrgPointApplicationAttachmentDto, OtherOrgPointApplicationAttachmentEntity> {

	/**
	 * コンストラクタ
	 */
	private OtherOrgPointApplicationAttachmentDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private OtherOrgPointApplicationAttachmentDto(OtherOrgPointApplicationAttachmentEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static OtherOrgPointApplicationAttachmentDto create() {
		return new OtherOrgPointApplicationAttachmentDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static OtherOrgPointApplicationAttachmentDto createSingle() {
		return new OtherOrgPointApplicationAttachmentDto(new OtherOrgPointApplicationAttachmentEntity());
		//CompositeEntityに変更（複合）
	}

}
