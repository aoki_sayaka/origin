package jp.or.jaot.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * アンケートエンティティ<br>
 */
@Entity
@Table(name = "questionnaires")
@Getter
@Setter
public class QuestionnaireEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 件名 */
	private String subject;

	/** 本文 */
	private String content;

	/** リンク先 */
	private String link;

	/** 掲載期間(from) */
	@Column(name = "term_from")
	private Timestamp termFrom;

	/** 掲載期間(to) */
	@Column(name = "term_to")
	private Timestamp termTo;

	/** 公開範囲 */
	@Column(name = "publish_target")
	private Integer publishTarget;

	/** 種別 */
	private Integer category;
}
