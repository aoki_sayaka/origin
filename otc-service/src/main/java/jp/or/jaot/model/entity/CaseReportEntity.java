package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事例報告エンティティ<br>
 */
@Entity
@Table(name = "case_reports")
@Getter
@Setter
public class CaseReportEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 受付番号 */
	@Column(name = "receipt_number")
	private Integer receiptNumber;

	/** 発表先コード */
	@Column(name = "presentation_cd")
	private String presentationCd;

	/** 表題 */
	@Column(name = "title")
	private String title;

	/** 総合判定日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "overall_judge_date")
	private Date overallJudgeDate;

	/** 取り下げフラグ */
	@Column(name = "withdrawn")
	private Boolean withdrawn;

	/** 公開日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "publish_date")
	private Date publishDate;

	/** 審査開始日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "judge_start_date")
	private Date judgeStartDate;

	/** メール送信フラグ */
	@Column(name = "email_sent")
	private Boolean emailSent;

	/** 合否状況 */
	@Column(name = "pass_status_cd")
	private Integer passStatusCd;

	/** 取り下げ状況 */
	@Column(name = "withdraw_status_cd")
	private Integer withdrawStatusCd;
}
