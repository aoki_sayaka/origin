package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP事例報告検索DTO
 */
@Getter
@Setter
public class MtdlpCaseReportSearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	// TODO : 検索条件追加
}
