package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberUpdateHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員更新履歴DTO
 */
@Getter
@Setter
public class MemberUpdateHistoryDto extends BaseDto<MemberUpdateHistoryDto, MemberUpdateHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberUpdateHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberUpdateHistoryDto(MemberUpdateHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberUpdateHistoryDto create() {
		return new MemberUpdateHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberUpdateHistoryDto createSingle() {
		return new MemberUpdateHistoryDto(new MemberUpdateHistoryEntity());
	}
}
