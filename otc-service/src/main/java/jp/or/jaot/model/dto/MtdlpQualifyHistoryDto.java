package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP認定履歴DTO
 */
@Getter
@Setter
public class MtdlpQualifyHistoryDto extends BaseDto<MtdlpQualifyHistoryDto, MtdlpQualifyHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private MtdlpQualifyHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MtdlpQualifyHistoryDto(MtdlpQualifyHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MtdlpQualifyHistoryDto create() {
		return new MtdlpQualifyHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MtdlpQualifyHistoryDto createSingle() {
		return new MtdlpQualifyHistoryDto(new MtdlpQualifyHistoryEntity());
	}
}
