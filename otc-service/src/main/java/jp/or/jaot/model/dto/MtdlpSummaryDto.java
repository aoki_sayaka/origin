package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.MtdlpCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP履歴DTO
 */
@Getter
@Setter
public class MtdlpSummaryDto extends BaseDto<MtdlpSummaryDto, MtdlpCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private MtdlpSummaryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MtdlpSummaryDto(MtdlpCompositeEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MtdlpSummaryDto create() {
		return new MtdlpSummaryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MtdlpSummaryDto createSingle() {
		return new MtdlpSummaryDto(new MtdlpCompositeEntity());
		//CompositeEntityに変更（複合）
	}

}
