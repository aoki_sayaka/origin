package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 履歴フィールド定義検索DTO
 */
@Getter
@Setter
public class HistoryFieldDefinitionSearchDto extends BaseSearchDto {

	/** 履歴テーブル定義ID */
	private Long tableDefinitionId;

	/** 項目名(物理) */
	private String fieldPhysicalNm;

	// TODO : 検索条件追加
}
