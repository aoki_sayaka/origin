package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.PeriodExtensionApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 期間延長申請DTO
 */
@Getter
@Setter
public class PeriodExtensionApplicationDto
		extends BaseDto<PeriodExtensionApplicationDto, PeriodExtensionApplicationEntity> {

	/**
	 * コンストラクタ
	 */
	private PeriodExtensionApplicationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private PeriodExtensionApplicationDto(PeriodExtensionApplicationEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static PeriodExtensionApplicationDto create() {
		return new PeriodExtensionApplicationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static PeriodExtensionApplicationDto createSingle() {
		return new PeriodExtensionApplicationDto(new PeriodExtensionApplicationEntity());
	}
}
