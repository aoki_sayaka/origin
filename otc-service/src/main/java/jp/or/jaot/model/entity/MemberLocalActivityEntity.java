package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員自治体参画情報エンティティ<br>
 */
@Entity
@Table(name = "member_local_activities")
@Getter
@Setter
public class MemberLocalActivityEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 活動番号 */
	@Column(name = "activity_seq")
	private Integer activitySeq;

	/** 参画フラグ */
	private Boolean participation;

	/** 自治体名 */
	@Column(name = "municipality_name")
	private String municipalityName;
}
