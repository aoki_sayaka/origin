package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.RegionRelatedMiddleClassificationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 領域関連中分類DTO
 */
@Getter
@Setter
public class RegionRelatedMiddleClassificationDto
		extends BaseDto<RegionRelatedMiddleClassificationDto, RegionRelatedMiddleClassificationEntity> {

	/**
	 * コンストラクタ
	 */
	private RegionRelatedMiddleClassificationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private RegionRelatedMiddleClassificationDto(RegionRelatedMiddleClassificationEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static RegionRelatedMiddleClassificationDto create() {
		return new RegionRelatedMiddleClassificationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static RegionRelatedMiddleClassificationDto createSingle() {
		return new RegionRelatedMiddleClassificationDto(new RegionRelatedMiddleClassificationEntity());
	}
}
