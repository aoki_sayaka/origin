package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局権限エンティティ<br>
 */
@Entity
@Table(name = "user_revoked_authorities")
@Getter
@Setter
public class UserRevokedAuthorityEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** コード識別ID */
	@Column(name = "code_id")
	private String codeId;

	/** コード値 */
	private String code;

	/** パス/キー値 */
	@Column(name = "path_or_key")
	private String pathOrKey;
}
