package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.TrainingAttendanceResultCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会出欠合否DTO
 */
@Getter
@Setter
public class TrainingAttendanceResultDto
		extends BaseDto<TrainingAttendanceResultDto, TrainingAttendanceResultCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingAttendanceResultDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingAttendanceResultDto(TrainingAttendanceResultCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingAttendanceResultDto create() {
		return new TrainingAttendanceResultDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingAttendanceResultDto createSingle() {
		return new TrainingAttendanceResultDto(new TrainingAttendanceResultCompositeEntity());
	}
}
