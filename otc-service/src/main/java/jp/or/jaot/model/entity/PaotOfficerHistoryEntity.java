package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

/**
 * 県士会役員履歴エンティティ<br>
 */
@Entity
@Table(name = "paot_officer_histories")
@Getter
@Setter
public class PaotOfficerHistoryEntity implements jp.or.jaot.core.model.Entity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 県士会番号 */
	@Column(name = "paot_cd")
	private Integer paotCd;

	/** 役職コード */
	@Column(name = "position_cd")
	private Integer positionCd;

	/** 部署コード */
	@Column(name = "department_cd")
	private Integer departmentCd;

	/** 任用開始日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "start_appoint_date")
	private Date startAppointDate;

	/** 任用終了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "end_appoint_date")
	private Date endAppointDate;

	/** 備考 */
	private String remarks;

	// TODO
}
