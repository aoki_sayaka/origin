package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.CaseReportEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事例報告DTO
 */
@Getter
@Setter
public class CaseReportDto extends BaseDto<CaseReportDto, CaseReportEntity> {

	/**
	 * コンストラクタ
	 */
	private CaseReportDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private CaseReportDto(CaseReportEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static CaseReportDto create() {
		return new CaseReportDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static CaseReportDto createSingle() {
		return new CaseReportDto(new CaseReportEntity());
	}
}
