package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 年度別情報テンポラリエンティティ<br>
 */
@Entity
@Table(name = "info_by_fiscal_year_temp")
@Getter
@Setter
public class InfoByFiscalYearTempEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 処理ID */
	@Column(name = "process_id")
	private Integer processId;

	/** 年度 */
	@Column(name = "fiscal_year")
	private Integer fiscalYear;

	/** 入学定員 */
	@Column(name = "admission_count")
	private Integer admissionCount;

	/** 施設基本番号 */
	@Column(name = "facility_base_no")
	private String facilityBaseNo;

	/** 国家試験受験者数 */
	@Column(name = "entrance_exam_count")
	private Integer entranceExamCount;

	/** 国家試験合格者数 */
	@Column(name = "pass_exam_count")
	private Integer passExamCount;

	/** エラーメッセージ */
	@Column(name = "error_message")
	private String errorMessage;
}
