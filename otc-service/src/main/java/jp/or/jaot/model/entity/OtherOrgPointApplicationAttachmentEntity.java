package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント添付ファイルエンティティ<br>
 */

@Entity
@Table(name = "other_org_point_application_attachments")
@Getter
@Setter
public class OtherOrgPointApplicationAttachmentEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 申請 */
	@Column(name = "application_id")
	private Long applicationId;

	/** 順番 */
	@Column(name = "file_order")
	private Integer fileOrder;

	/** 受講証明書ファイル名 */
	@Column(name = "file_nm")
	private String fileNm;

	/** 受講証明書ファイル */
	@Column(name = "file")
	private String file;
	
	/** 受講証明書不適切理由コード */
	@Column(name = "file_reject_cd")
	private Integer fileRejectCd;
	

}
