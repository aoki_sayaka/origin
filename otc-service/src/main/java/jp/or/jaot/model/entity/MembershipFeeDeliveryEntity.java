package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 会費納入エンティティ<br>
 */
@Entity
@Table(name = "membership_fee_deliveries")
@Getter
@Setter
@ToString
public class MembershipFeeDeliveryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 年度 */
	@Column(name = "fiscal_year")
	private Integer fiscalYear;

	/** 会費区分コード */
	@Column(name = "category_cd")
	private String categoryCd;

	/** 請求額 */
	@Column(name = "billing_amount")
	private Integer billingAmount;

	/** 入金額 */
	@Column(name = "payment_amount")
	private Integer paymentAmount;

	/** 入金日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date")
	private Date paymentDate;

	/** 取込日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "import_date")
	private Date importDate;

	/** 請求残額 */
	@Column(name = "billing_balance")
	private Integer billingBalance;

	/** 備考 */
	private String remarks;

	/** 入金種別 */
	@Column(name = "payment_type")
	private String paymentType;

	/** データ種別 */
	@Column(name = "data_type")
	private String dataType;

	/** 申請日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "application_date")
	private Date applicationDate;

	/** 申請内容 */
	@Column(name = "application_content")
	private String applicationContent;

	/** 申請状況 */
	@Column(name = "application_status")
	private String applicationStatus;
}
