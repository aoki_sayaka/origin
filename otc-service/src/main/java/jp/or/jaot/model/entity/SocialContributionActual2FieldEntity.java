package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績の領域2(後輩育成・社会貢献)エンティティ<br>
 */
@Entity
@Table(name = "social_contribution_actual2_fields")
@Getter
@Setter
public class SocialContributionActual2FieldEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 活動実績ID */
	@Column(name = "result_id")
	private Long resultId;

	/** 領域コード */
	@Column(name = "field_cd")
	private String fieldCd;
}
