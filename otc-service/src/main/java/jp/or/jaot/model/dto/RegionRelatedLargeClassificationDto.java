package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.RegionRelatedLargeClassificationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 領域関連大分類DTO
 */
@Getter
@Setter
public class RegionRelatedLargeClassificationDto
		extends BaseDto<RegionRelatedLargeClassificationDto, RegionRelatedLargeClassificationEntity> {

	/**
	 * コンストラクタ
	 */
	private RegionRelatedLargeClassificationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private RegionRelatedLargeClassificationDto(RegionRelatedLargeClassificationEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static RegionRelatedLargeClassificationDto create() {
		return new RegionRelatedLargeClassificationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static RegionRelatedLargeClassificationDto createSingle() {
		return new RegionRelatedLargeClassificationDto(new RegionRelatedLargeClassificationEntity());
	}
}
