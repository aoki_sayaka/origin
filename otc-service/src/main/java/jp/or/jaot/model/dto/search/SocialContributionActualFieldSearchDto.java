package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績の領域(後輩育成・社会貢献)検索DTO
 */
@Getter
@Setter
public class SocialContributionActualFieldSearchDto extends BaseSearchDto {

	/** 活動実績ID */
	private Long[] resultIds;

	// TODO : 検索条件追加
}
