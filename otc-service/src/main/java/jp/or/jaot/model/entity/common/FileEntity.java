package jp.or.jaot.model.entity.common;

import org.springframework.web.multipart.MultipartFile;

import jp.or.jaot.core.model.Entity;
import lombok.Getter;
import lombok.Setter;

/**
 * ファイルエンティティ
 */
@Getter
@Setter
public class FileEntity implements Entity {

	/** パス */
	private String path;

	/** 名前 */
	private String name;

	/** マルチパートファイル */
	private MultipartFile multipartFile;
}
