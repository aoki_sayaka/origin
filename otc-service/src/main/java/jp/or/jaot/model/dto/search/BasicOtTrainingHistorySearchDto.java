package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修受講履歴検索DTO
 */
@Getter
@Setter
public class BasicOtTrainingHistorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 年度 */
	private Integer fiscalYear;
}
