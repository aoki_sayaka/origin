package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.ProfessionalOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;
import jp.or.jaot.model.entity.ProfessionalOtTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士受講履歴エンティティ(複合)<br>
 * ・基礎研修履歴と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class ProfessionalOtAttendingSummaryCompositeEntity implements Entity {

	// 専門作業療法士受講履歴
	private ProfessionalOtAttendingSummaryEntity professionalOtAttendingSummaryEntity = new ProfessionalOtAttendingSummaryEntity();

	// 専門作業療法士認定更新履歴
	private List<ProfessionalOtQualifyHistoryEntity> professionalOtQualifyHistoryEntities = new ArrayList<ProfessionalOtQualifyHistoryEntity>();

	// 専門作業療法士試験結果履歴
	private List<ProfessionalOtTestHistoryEntity> professionalOtTestHistoryEntities = new ArrayList<ProfessionalOtTestHistoryEntity>();

	// 専門作業療法士研修受講履歴
	private List<ProfessionalOtTrainingHistoryEntity> professionalOtTrainingHistoryEntities = new ArrayList<ProfessionalOtTrainingHistoryEntity>();

}
