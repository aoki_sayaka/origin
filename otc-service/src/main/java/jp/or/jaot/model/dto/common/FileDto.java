package jp.or.jaot.model.dto.common;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.common.FileEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * ファイルDTO
 */
@Getter
@Setter
public class FileDto extends BaseDto<FileDto, FileEntity> {

	/**
	 * コンストラクタ
	 */
	private FileDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity ファイルエンティティ
	 */
	private FileDto(FileEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return ファイルDTO
	 */
	public static FileDto create() {
		return new FileDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return ファイルDTO
	 */
	public static FileDto createSingle() {
		return new FileDto(new FileEntity());
	}
}
