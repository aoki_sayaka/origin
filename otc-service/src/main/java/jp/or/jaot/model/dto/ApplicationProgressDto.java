package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.ApplicationProgressEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 申請進捗DTO
 */
@Getter
@Setter
public class ApplicationProgressDto extends BaseDto<ApplicationProgressDto, ApplicationProgressEntity> {

	/**
	 * コンストラクタ
	 */
	private ApplicationProgressDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ApplicationProgressDto(ApplicationProgressEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ApplicationProgressDto create() {
		return new ApplicationProgressDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ApplicationProgressDto createSingle() {
		return new ApplicationProgressDto(new ApplicationProgressEntity());
	}
}
