package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.QualificationOtHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士履歴DTO
 */
@Getter
@Setter
public class QualificationOtHistoryDto extends BaseDto<QualificationOtHistoryDto, QualificationOtHistoryEntity>{
	
	/**
	 * コンストラクタ
	 */
	private QualificationOtHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private QualificationOtHistoryDto(QualificationOtHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static QualificationOtHistoryDto create() {
		return new QualificationOtHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static QualificationOtHistoryDto createSingle() {
		return new QualificationOtHistoryDto(new QualificationOtHistoryEntity());
	}

}
