package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.domain.TrainingApplicationDomain;
import jp.or.jaot.domain.TrainingAttendanceResultDomain;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import jp.or.jaot.model.entity.TrainingReportEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会報告書エンティティ(複合)<br>
 * ・研修会報告書と関連を持つ情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 * ・エンティティが存在しない場合は親エンティティの属性を返却する
 */
@Getter
@Setter
public class TrainingReportCompositeEntity implements Entity {

	/** 研修会報告書 */
	private TrainingReportEntity trainingReportEntity = new TrainingReportEntity();

	/** 研修会[親] */
	private TrainingEntity trainingEntity = new TrainingEntity();

	/** 研修会申込[関連] */
	private List<TrainingApplicationEntity> trainingApplicationEntities = new ArrayList<TrainingApplicationEntity>();

	/** 研修会出欠合否[関連] */
	private List<TrainingAttendanceResultEntity> trainingAttendanceResultEntities = new ArrayList<TrainingAttendanceResultEntity>();

	/**
	 * 研修会報告書存在チェック
	 * @return true=存在する, false=存在しない
	 */
	public boolean exists() {
		return (trainingReportEntity != null && trainingReportEntity.getId() != null) ? true : false;
	}

	/**
	 * 対象取得
	 * @return 対象
	 */
	public String getTargetCd() {
		return exists() ? trainingReportEntity.getTargetCd() : trainingEntity.getTargetCd();
	}

	/**
	 * 定員数取得
	 * @return 定員数
	 */
	public Integer getCapacityCount() {
		return trainingEntity.getCapacityCount();
	}

	/**
	 * 確定数取得
	 * @return 確定数
	 */
	public Integer getDecisionCount() {

		// 研修会申込状況 : 受講決定
		String statusCd = TrainingApplicationDomain.STATUS_CD.DECISION_ATTENDANCE.getValue();

		// 該当数カウント
		return CollectionUtils.isEmpty(trainingApplicationEntities) ? (0)
				: CollectionUtils.size(trainingApplicationEntities.stream()
						.filter(E -> statusCd.equals(E.getApplicationStatusCd())).collect(Collectors.toList()));
	}

	/**
	 * 参加人数取得(開催日1)
	 * @return 参加人数
	 */
	public Integer getAttendanceStatus1Count() {

		// 研修会出席状況 : 出席
		String statusCd = TrainingAttendanceResultDomain.ATTENDANCE_STATUS_CD.ATTENDANCE.getValue();

		// 該当数カウント
		return CollectionUtils.isEmpty(trainingAttendanceResultEntities) ? (0)
				: CollectionUtils.size(trainingAttendanceResultEntities.stream()
						.filter(E -> statusCd.equals(E.getAttendanceStatus1Cd())).collect(Collectors.toList()));
	}

	/**
	 * 参加人数取得(開催日2)
	 * @return 参加人数
	 */
	public Integer getAttendanceStatus2Count() {

		// 研修会出席状況 : 出席
		String statusCd = TrainingAttendanceResultDomain.ATTENDANCE_STATUS_CD.ATTENDANCE.getValue();

		// 該当数カウント
		return CollectionUtils.isEmpty(trainingAttendanceResultEntities) ? (0)
				: CollectionUtils.size(trainingAttendanceResultEntities.stream()
						.filter(E -> statusCd.equals(E.getAttendanceStatus2Cd())).collect(Collectors.toList()));
	}

	/**
	 * 参加人数取得(開催日3)
	 * @return 参加人数
	 */
	public Integer getAttendanceStatus3Count() {

		// 研修会出席状況 : 出席
		String statusCd = TrainingAttendanceResultDomain.ATTENDANCE_STATUS_CD.ATTENDANCE.getValue();

		// 該当数カウント
		return CollectionUtils.isEmpty(trainingAttendanceResultEntities) ? (0)
				: CollectionUtils.size(trainingAttendanceResultEntities.stream()
						.filter(E -> statusCd.equals(E.getAttendanceStatus3Cd())).collect(Collectors.toList()));
	}

	/**
	 * 申込数取得
	 * @return 申込数
	 */
	public Integer getApplicantCount() {
		return exists() ? trainingReportEntity.getApplicantCount() : CollectionUtils.size(trainingApplicationEntities);
	}

	/**
	 * 参加費取得
	 * @return 参加費
	 */
	public Integer getEntryFee() {
		return trainingEntity.getEntryFee();
	}
}
