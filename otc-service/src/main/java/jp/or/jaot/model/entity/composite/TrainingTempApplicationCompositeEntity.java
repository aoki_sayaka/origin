package jp.or.jaot.model.entity.composite;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.util.StringUtils;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.MemberEntity;
import jp.or.jaot.model.entity.TrainingTempApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込エンティティ(複合)<br>
 * ・研修会仮申込と関連を持つ情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class TrainingTempApplicationCompositeEntity implements Entity {

	/** 研修会仮申込 */
	private TrainingTempApplicationEntity trainingTempApplicationEntity = new TrainingTempApplicationEntity();

	/** 会員[関連] */
	private MemberEntity memberEntity = new MemberEntity();

	/** 基礎研修履歴[関連] */
	private BasicOtAttendingSummaryEntity basicOtAttendingSummaryEntity = new BasicOtAttendingSummaryEntity();

	/**
	 * 氏名取得
	 * @return 氏名
	 */
	public String getFullname() {
		if (StringUtils.hasText(memberEntity.getLastName()) && StringUtils.hasText(memberEntity.getFirstName())) {
			return String.format("%s %s", memberEntity.getLastName(), memberEntity.getFirstName()); // 性 + 名
		} else if (StringUtils.hasText(memberEntity.getLastName())) {
			return String.format("%s", memberEntity.getLastName()); // 性
		} else {
			return "";
		}
	}

	/**
	 * 登録日時取得
	 * @return 登録日時
	 */
	public Timestamp getRegisteredDatetime() {
		return trainingTempApplicationEntity.getCreateDatetime();
	}

	/**
	 * 勤務先都道府県コード取得
	 * @return 勤務先都道府県コード
	 */
	public String getWorkingPrefectureCd() {
		return memberEntity.getWorkingPrefectureCd();
	}

	/**
	 * 勤務条件（施設名）取得
	 * @return 勤務条件（施設名）
	 */
	public String getWorkingConditionFacilityName() {
		return memberEntity.getWorkingConditionFacilityName();
	}

	/**
	 * 認定期間満了日
	 * @return 認定期間満了日取得
	 */
	public Date getQualificationPeriodDate() {
		return basicOtAttendingSummaryEntity.getQualificationPeriodDate();
	}
}
