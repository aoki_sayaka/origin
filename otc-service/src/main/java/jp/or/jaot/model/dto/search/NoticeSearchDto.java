package jp.or.jaot.model.dto.search;

import java.sql.Timestamp;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ検索DTO
 */
@Getter
@Setter
public class NoticeSearchDto extends BaseSearchDto {

	/** 掲載日範囲(From) */
	private Timestamp publishRangeFrom;

	/** 掲載日範囲(To) */
	private Timestamp publishRangeTo;

	// TODO : 検索条件追加
}
