package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 関連資格エンティティ<br>
 */
@Entity
@Table(name = "qualifications")
@Getter
@Setter
public class QualificationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** コード */
	private String code;

	/** 区分コード */
	@Column(name = "category_cd")
	private String categoryCd;

	/** 名称 */
	private String name;

	/** 旧コード */
	@Column(name = "old_code")
	private String oldCode;
}
