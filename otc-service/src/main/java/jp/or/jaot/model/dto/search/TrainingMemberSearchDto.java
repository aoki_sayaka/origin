package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会会員検索DTO
 */
@Getter
@Setter
public class TrainingMemberSearchDto extends BaseSearchDto {

	/** 会員.会員番号 */
	private Integer memberNo;

	/** 会員.氏名 */
	private String fullName;

	/** 講師資格フラグ(null=未指定, true=資格あり, false=資格なし) */
	private Boolean instructorQualificationFlag;

	// TODO : 検索条件追加
}
