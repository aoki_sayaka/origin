package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberEmailHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員メールアドレス履歴DTO
 */
@Getter
@Setter
public class MemberEmailHistoryDto extends BaseDto<MemberEmailHistoryDto, MemberEmailHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberEmailHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberEmailHistoryDto(MemberEmailHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberEmailHistoryDto create() {
		return new MemberEmailHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberEmailHistoryDto createSingle() {
		return new MemberEmailHistoryDto(new MemberEmailHistoryEntity());
	}
}
