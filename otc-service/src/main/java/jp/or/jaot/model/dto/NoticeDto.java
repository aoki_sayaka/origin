package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.NoticeCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせDTO
 */
@Getter
@Setter
public class NoticeDto extends BaseDto<NoticeDto, NoticeCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private NoticeDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private NoticeDto(NoticeCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static NoticeDto create() {
		return new NoticeDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static NoticeDto createSingle() {
		return new NoticeDto(new NoticeCompositeEntity());
	}
}
