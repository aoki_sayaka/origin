package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.QualificationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 関連資格DTO
 */
@Getter
@Setter
public class QualificationDto extends BaseDto<QualificationDto, QualificationEntity> {

	/**
	 * コンストラクタ
	 */
	private QualificationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private QualificationDto(QualificationEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static QualificationDto create() {
		return new QualificationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static QualificationDto createSingle() {
		return new QualificationDto(new QualificationEntity());
	}
}
