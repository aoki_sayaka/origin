package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請分類DTO
 */
@Getter
@Setter
public class FacilityApplicationCategoryDto
		extends BaseDto<FacilityApplicationCategoryDto, FacilityApplicationCategoryEntity> {

	/**
	 * コンストラクタ
	 */
	private FacilityApplicationCategoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private FacilityApplicationCategoryDto(FacilityApplicationCategoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static FacilityApplicationCategoryDto create() {
		return new FacilityApplicationCategoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static FacilityApplicationCategoryDto createSingle() {
		return new FacilityApplicationCategoryDto(new FacilityApplicationCategoryEntity());
	}
}
