package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

/**
 * 県士会在籍履歴エンティティ<br>
 */
@Entity
@Table(name = "paot_membership_histories")
@Getter
@Setter
public class PaotMembershipHistoryEntity implements jp.or.jaot.core.model.Entity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 県士会番号 */
	@Column(name = "paot_cd")
	private Integer paotCd;

	/** 在籍開始日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "start_enroll_date")
	private Date startEnrollDate;

	/** 在籍終了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "end_enroll_date")
	private Date endEnrollDate;

	/** 備考 */
	private String remarks;

	// TODO
}
