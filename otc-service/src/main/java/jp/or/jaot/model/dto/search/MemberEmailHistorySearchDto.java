package jp.or.jaot.model.dto.search;

import java.util.Date;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員メールアドレス履歴検索DTO
 */
@Getter
@Setter
public class MemberEmailHistorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 仮メールアドレスキー値 */
	private String tempKey;

	/** 仮メールアドレス有効フラグ */
	private Boolean tempFlag;

	/** 有効期限 */
	private Date tempExpirationDatetime;

}
