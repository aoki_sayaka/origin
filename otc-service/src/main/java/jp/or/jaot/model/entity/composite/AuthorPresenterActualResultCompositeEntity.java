package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.AuthorPresenterActualFieldEntity;
import jp.or.jaot.model.entity.AuthorPresenterActualResultEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(発表・著作等)エンティティ(複合)<br>
 * ・活動実績(発表・著作等)と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class AuthorPresenterActualResultCompositeEntity implements Entity {

	/** 活動実績(発表・著作等) */
	private AuthorPresenterActualResultEntity authorPresenterActualResultEntity = new AuthorPresenterActualResultEntity();

	/** 活動実績の領域(発表・著作等) */
	private List<AuthorPresenterActualFieldEntity> authorPresenterActualFieldEntities = new ArrayList<AuthorPresenterActualFieldEntity>();
}
