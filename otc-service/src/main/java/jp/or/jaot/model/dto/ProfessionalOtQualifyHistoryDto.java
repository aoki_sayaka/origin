package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.ProfessionalOtQualifyHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士認定更新履歴DTO
 */
@Getter
@Setter
public class ProfessionalOtQualifyHistoryDto
		extends BaseDto<ProfessionalOtQualifyHistoryDto, ProfessionalOtQualifyHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private ProfessionalOtQualifyHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ProfessionalOtQualifyHistoryDto(ProfessionalOtQualifyHistoryEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ProfessionalOtQualifyHistoryDto create() {
		return new ProfessionalOtQualifyHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ProfessionalOtQualifyHistoryDto createSingle() {
		return new ProfessionalOtQualifyHistoryDto(new ProfessionalOtQualifyHistoryEntity());
		//CompositeEntityに変更（複合）
	}

}
