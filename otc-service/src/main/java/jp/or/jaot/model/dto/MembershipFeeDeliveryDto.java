package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MembershipFeeDeliveryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会費納入DTO
 */
@Getter
@Setter
public class MembershipFeeDeliveryDto extends BaseDto<MembershipFeeDeliveryDto, MembershipFeeDeliveryEntity> {

	/**
	 * コンストラクタ
	 */
	private MembershipFeeDeliveryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MembershipFeeDeliveryDto(MembershipFeeDeliveryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MembershipFeeDeliveryDto create() {
		return new MembershipFeeDeliveryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MembershipFeeDeliveryDto createSingle() {
		return new MembershipFeeDeliveryDto(new MembershipFeeDeliveryEntity());
	}
}
