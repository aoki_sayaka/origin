package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.FacilityApplicationCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請DTO
 */
@Getter
@Setter
public class FacilityApplicationDto extends BaseDto<FacilityApplicationDto, FacilityApplicationCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private FacilityApplicationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private FacilityApplicationDto(FacilityApplicationCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static FacilityApplicationDto create() {
		return new FacilityApplicationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static FacilityApplicationDto createSingle() {
		return new FacilityApplicationDto(new FacilityApplicationCompositeEntity());
	}
}
