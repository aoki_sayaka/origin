package jp.or.jaot.model.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 養成校名称変更履歴エンティティ<br>
 */
@Entity
@Table(name = "training_school_name_histories")
@Getter
@Setter
public class TrainingSchoolNameHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 施設基本番号 */
	@Column(name = "facility_base_no")
	private String facilityBaseNo;

	/** 施設区分 */
	@Column(name = "facility_category_cd")
	private String facilityCategoryCd;

	/** 施設養成校ID */
	@Column(name = "facility_id")
	private Long facilityId;

	/** 施設番号 */
	@Column(name = "facility_no")
	private String facilityNo;

	/** 施設連番 */
	@Column(name = "facility_seq_no")
	private String facilitySeqNo;

	/** 年度 */
	@Column(name = "fiscal_year")
	private Integer fiscalYear;

	/** メモ */
	private String memo;

	/** 登録日付 */
	@Temporal(TemporalType.DATE)
	@Column(name = "name_create_date")
	private Date nameCreateDate;

	/** 削除日付 */
	@Temporal(TemporalType.DATE)
	@Column(name = "name_delete_date")
	private Date nameDeleteDate;

	/** 更新日時 */
	@Column(name = "name_update_date")
	private Timestamp nameUpdateDate;

	/** 更新連番 */
	@Column(name = "name_update_no")
	private Integer nameUpdateNo;

	/** 更新ユーザID */
	@Column(name = "name_update_user_id")
	private String nameUpdateUserId;

	/** 出身養成校番号 */
	@Column(name = "origin_school_no")
	private Integer originSchoolNo;

	/** 都道府県コード */
	@Column(name = "pref_code")
	private String prefCode;

	/** 連番 */
	@Column(name = "seq_no")
	private Integer seqNo;

	/** 養成校種別 */
	@Column(name = "training_school_category_cd")
	private String trainingSchoolCategoryCd;

	/** 施設名(漢字) */
	@Column(name = "training_school_nm")
	private String trainingSchoolNm;

	/** 施設名(カナ) */
	@Column(name = "training_school_nm_kana")
	private String trainingSchoolNmKana;
}
