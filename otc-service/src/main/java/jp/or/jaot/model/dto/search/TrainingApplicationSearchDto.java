package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会申込検索DTO
 */
@Getter
@Setter
public class TrainingApplicationSearchDto extends BaseSearchDto {

	/** 研修会番号 */
	private String[] trainingNos;

	/** 申込者番号 */
	private Integer memberNo;

	/** 氏名 */
	private String fullname;

	/** 勤務先名 */
	private String workPlaceNm;

	/** 勤務先都道府県 */
	private String[] workPlacePrefectureCds;

	// TODO : 検索条件追加
}
