package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.ClinicalTrainingPointApplicationEntity;
import jp.or.jaot.model.entity.FacilityEntity;
import jp.or.jaot.model.entity.MemberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習ポイント申請エンティティ(複合)<br>
 * ・臨床実習ポイント申請と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class ClinicalTrainingPointApplicationCompositeEntity implements Entity {

	// 臨床実習ポイント申請
	private ClinicalTrainingPointApplicationEntity clinicalTrainingPointApplicationEntity = new ClinicalTrainingPointApplicationEntity();

	// 会員情報
	private List<MemberEntity> memberEntities = new ArrayList<MemberEntity>();
	
	// 養成校情報
	private List<FacilityEntity> facilityEntities = new ArrayList<FacilityEntity>();

}
