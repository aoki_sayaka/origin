package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.FacilityMemberEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校勤務者DTO
 */
@Getter
@Setter
public class FacilityMemberDto extends BaseDto<FacilityMemberDto, FacilityMemberEntity> {

	/**
	 * コンストラクタ
	 */
	private FacilityMemberDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private FacilityMemberDto(FacilityMemberEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static FacilityMemberDto create() {
		return new FacilityMemberDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static FacilityMemberDto createSingle() {
		return new FacilityMemberDto(new FacilityMemberEntity());
	}
}
