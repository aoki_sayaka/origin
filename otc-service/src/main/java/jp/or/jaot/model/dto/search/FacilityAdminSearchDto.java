package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校検索DTO(事務局サイト)
 */
@Getter
@Setter
public class FacilityAdminSearchDto extends BaseSearchDto {

	/** 施設養成校名(漢字) */
	private String name;

	/** 施設養成校名(カナ) */
	private String nameKana;

	/** 都道府県コード */
	private String[] prefectureCds;

	// TODO : 検索条件追加
}
