package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.SocialContributionActual2FieldEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績の領域2(後輩育成・社会貢献)DTO
 */
@Getter
@Setter
public class SocialContributionActual2FieldDto
		extends BaseDto<SocialContributionActual2FieldDto, SocialContributionActual2FieldEntity> {

	/**
	 * コンストラクタ
	 */
	private SocialContributionActual2FieldDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private SocialContributionActual2FieldDto(SocialContributionActual2FieldEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static SocialContributionActual2FieldDto create() {
		return new SocialContributionActual2FieldDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static SocialContributionActual2FieldDto createSingle() {
		return new SocialContributionActual2FieldDto(new SocialContributionActual2FieldEntity());
	}
}
