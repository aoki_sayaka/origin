package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎ポイント研修履歴エンティティ<br>
 */

@Entity
@Table(name = "basic_point_training_histories")
@Getter
@Setter
public class BasicPointTrainingHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 研修受講年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "attend_date")
	private Date attendDate;

	/** 内容 */
	@Column(name = "content")
	private String content;

	/** ポイント種別 */
	@Column(name = "point_type")
	private String pointType;

	/** 日数 */
	@Column(name = "days")
	private Integer days;

	/** ポイント */
	@Column(name = "point")
	private Integer point;

	/** ポイント使用フラグ */
	@Column(name = "use_point")
	private boolean usePoint;

	/** 主催者コード */
	@Column(name = "promoter_cd")
	//	private String promoter_cd;
	private String promoterCd;

	/** 他団体・SIGコード */
	@Column(name = "others_sig_cd")
	private String othersSigCd;

	/** 施設番号 */
//	@Column(name = "facility_no")
//	private String facilityNo;

	/** 照会会員氏名 */
//	@Column(name = "collation_member_name")
//	private String collationMemberName;

	/** 照合率 */
//	@Column(name = "collation_rate")
//	private Integer collationRate;
//		private BigDecimal collationRate;

	/** 開始日 */
//	@Temporal(TemporalType.DATE)
//	@Column(name = "start_date")
//	private Date startDate;

	/** 終了日 */
//	@Temporal(TemporalType.DATE)
//	@Column(name = "end_date")
//	private Date endDate;

	/** 備考 */
//	@Column(name = "remarks")
//	//	private Integer remarks;
//	private String remarks;
}
