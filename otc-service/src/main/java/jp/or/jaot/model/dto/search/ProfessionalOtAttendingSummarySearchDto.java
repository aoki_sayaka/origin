package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士受講履歴検索DTO
 */
@Getter
@Setter
public class ProfessionalOtAttendingSummarySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 専門分野 */
	private String professionalField;

	// TODO : 検索条件追加

}
