package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 履歴フィールド定義エンティティ<br>
 */
@Entity
@Table(name = "history_field_definitions")
@Getter
@Setter
public class HistoryFieldDefinitionEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 履歴テーブル定義ID */
	@Column(name = "table_definition_id")
	private Long tableDefinitionId;

	/** 項目コード */
	@Column(name = "field_cd")
	private Integer fieldCd;

	/** 項目名 */
	@Column(name = "field_nm")
	private String fieldNm;

	/** 項目名(物理) */
	@Column(name = "field_physical_nm")
	private String fieldPhysicalNm;
}
