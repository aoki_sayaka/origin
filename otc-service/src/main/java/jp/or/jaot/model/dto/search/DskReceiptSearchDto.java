package jp.or.jaot.model.dto.search;

import java.util.Date;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * DskReceipt検索Dto
 */
@Getter
@Setter
public class DskReceiptSearchDto extends BaseSearchDto {

	/** 種別 */
	private String typeCd;

	/** 会員番号 */
	private String memberNo;

	/** 支払期限 */
	private Date paymentDueDate;

	// TODO : 検索条件追加

}