package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修履歴検索DTO
 */
@Getter
@Setter
public class BasicOtAttendingSummarySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 会員番号(複数) */
	private Integer[] memberNos;

	/** 申請資格 */
	private String qualificationCd;
}
