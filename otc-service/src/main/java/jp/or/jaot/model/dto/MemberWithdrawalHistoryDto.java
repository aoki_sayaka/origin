package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MemberWithdrawalHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 退会履歴DTO
 */
@Getter
@Setter
public class MemberWithdrawalHistoryDto extends BaseDto<MemberWithdrawalHistoryDto, MemberWithdrawalHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private MemberWithdrawalHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MemberWithdrawalHistoryDto(MemberWithdrawalHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MemberWithdrawalHistoryDto create() {
		return new MemberWithdrawalHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MemberWithdrawalHistoryDto createSingle() {
		return new MemberWithdrawalHistoryDto(new MemberWithdrawalHistoryEntity());
	}
}
