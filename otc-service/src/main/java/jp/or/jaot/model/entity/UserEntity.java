package jp.or.jaot.model.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ユーザエンティティ<br>
 */
@Entity
@Table(name = "users")
@Getter
@Setter
public class UserEntity extends BaseEntity implements UserDetails {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** ログインID */
	@Column(name = "login_id")
	private String loginId;

	/** パスワード */
	private String password;

	/** ユーザー名 */
	private String name;

	/** メールアドレス */
	@Column(name = "email_address")
	private String emailAddress;

	/** 士会ユーザフラグ */
	@Column(name = "paot_user")
	private Boolean paotUser;

	/** 所属部門コード */
	@Column(name = "section_cd")
	private String sectionCd;

	/** 所属部門名 */
	@Column(name = "section_name")
	private String sectionName;

	/** 役職コード */
	@Column(name = "position_cd")
	private String positionCd;

	/** 役職名 */
	@Column(name = "position_name")
	private String positionName;

	/** 研修会当日担当者フラグ */
	@Column(name = "training_operator")
	private Boolean trainingOperator;

	/** 退職日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "retirement_date")
	private Date retirementDate;

	/*
	 * 認証関連メソッド
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return this.password; // パスワード
	}

	@Override
	public String getUsername() {
		return this.loginId; // ログインID
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return BooleanUtils.isFalse(getDeleted()); // 削除時は無効
	}
}
