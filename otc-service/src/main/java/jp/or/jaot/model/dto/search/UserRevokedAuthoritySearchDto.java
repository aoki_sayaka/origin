package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.entity.GeneralCodeId;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局権限検索DTO
 */
@Getter
@Setter
public class UserRevokedAuthoritySearchDto extends BaseSearchDto {

	/** 汎用コードID */
	private GeneralCodeId[] generalCodeIds;

	// TODO : 検索条件追加
}
