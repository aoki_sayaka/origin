package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ユーザ検索DTO
 */
@Getter
@Setter
public class UserSearchDto extends BaseSearchDto {

	/** ID */
	private Long id;

	/** ログインID */
	private String loginId;

	/** ユーザ名 */
	private String name;
}
