package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.AuthorPresenterActualFieldEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績の領域(発表・著作等)DTO
 */
@Getter
@Setter
public class AuthorPresenterActualFieldDto
		extends BaseDto<AuthorPresenterActualFieldDto, AuthorPresenterActualFieldEntity> {

	/**
	 * コンストラクタ
	 */
	private AuthorPresenterActualFieldDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private AuthorPresenterActualFieldDto(AuthorPresenterActualFieldEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static AuthorPresenterActualFieldDto create() {
		return new AuthorPresenterActualFieldDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static AuthorPresenterActualFieldDto createSingle() {
		return new AuthorPresenterActualFieldDto(new AuthorPresenterActualFieldEntity());
	}
}
