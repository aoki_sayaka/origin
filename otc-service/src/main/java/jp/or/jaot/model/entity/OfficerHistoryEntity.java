package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 役員履歴エンティティ<br>
 */
@Entity
@Table(name = "officer_histories")
@Getter
@Setter
public class OfficerHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 役職コード */
	@Column(name = "position_cd")
	private String positionCd;

	/** 役職分類 */
	@Column(name = "position_type")
	private Integer positionType;

	/** 都道府県コード */
	@Column(name = "prefecture_cd")
	private String prefectureCd;

	/** 任用開始日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "start_appoint_date")
	private Date startAppointDate;

	/** 任用終了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "end_appoint_date")
	private Date endAppointDate;

	/** 備考 */
	private String remarks;
}
