package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.BasicOtAttendingSummaryCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修履歴DTO
 */
@Getter
@Setter
public class BasicOtAttendingSummaryDto
		extends BaseDto<BasicOtAttendingSummaryDto, BasicOtAttendingSummaryCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private BasicOtAttendingSummaryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private BasicOtAttendingSummaryDto(BasicOtAttendingSummaryCompositeEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static BasicOtAttendingSummaryDto create() {
		return new BasicOtAttendingSummaryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static BasicOtAttendingSummaryDto createSingle() {
		return new BasicOtAttendingSummaryDto(new BasicOtAttendingSummaryCompositeEntity());
		//CompositeEntityに変更（複合）
	}

}
