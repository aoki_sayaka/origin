package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderQualifyHistoryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderSummaryEntity;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者履歴エンティティ(複合)<br>
 * ・会員情報と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class ClinicalTrainingLeaderSummaryCompositeEntity implements Entity {

	// 臨床実習指導者履歴
	private ClinicalTrainingLeaderSummaryEntity clinicalTrainingLeaderSummaryEntity = new ClinicalTrainingLeaderSummaryEntity();

	// 臨床実習指導者認定履歴
	private List<ClinicalTrainingLeaderQualifyHistoryEntity> clinicalTrainingLeaderQualifyHistoryEntities = new ArrayList<ClinicalTrainingLeaderQualifyHistoryEntity>();

	// 臨床実習指導者研修受講履歴
	private List<ClinicalTrainingLeaderTrainingHistoryEntity> clinicalTrainingLeaderTrainingHistoryEntities = new ArrayList<ClinicalTrainingLeaderTrainingHistoryEntity>();
}
