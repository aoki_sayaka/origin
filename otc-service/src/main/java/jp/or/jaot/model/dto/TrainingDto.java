package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.TrainingCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会DTO
 */
@Getter
@Setter
public class TrainingDto extends BaseDto<TrainingDto, TrainingCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingDto(TrainingCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingDto create() {
		return new TrainingDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingDto createSingle() {
		return new TrainingDto(new TrainingCompositeEntity());
	}
}
