package jp.or.jaot.model.dto.search;

import java.util.Date;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント申請DTO
 */
@Getter
@Setter
public class OtherOrgPointApplicationSearchDto extends BaseSearchDto {
	
	/** ID */
	private Integer id;

	/** 会員番号 */
	// private Integer memberNo;

	/** 申請資格 */
	// private String qualificationCd;
	
	/** 申請日(From) */
	private Date applicationDateFrom;

	/** 申請日(To) */
	private Date applicationDateTo;

	/** 申請状態 */
	private String applicationStatusCd;

	/** 受講日（自） */
	private Date attendingDateFrom;

	/** 受講日（至） */
	private Date attendingDateTo;

	/** 主催者 */
	private String organizerCd;

	/** 他団体SIG名 */
	private Integer sigNameCd;

	/** 受講テーマ */
	private String attendingTheme;

	/** ポイント種別 */
	private String pointTypeCd;

	/** 申請者 会員番号(From) */
	private Integer memberNoFrom;

	/** 申請者 会員番号(To) */
	private Integer memberNoTo;
}
