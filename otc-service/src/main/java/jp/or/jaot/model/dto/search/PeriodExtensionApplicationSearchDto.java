package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 期間延長申請検索DTO
 */
@Getter
@Setter
public class PeriodExtensionApplicationSearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 申請資格 */
	private String qualificationCd;
}
