package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 部署履歴エンティティ<br>
 */
@Entity
@Table(name = "department_histories")
@Getter
@Setter
public class DepartmentHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 部署コード */
	@Column(name = "department_cd")
	private String departmentCd;

	/** 職務コード */
	@Column(name = "job_cd")
	private String jobCd;

	/** 任用開始日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "start_appoint_date")
	private Date startAppointDate;

	/** 任用終了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "end_appoint_date")
	private Date endAppointDate;

	/** 備考 */
	private String remarks;

	/** 公開日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "release_date")
	private Date releaseDate;
}
