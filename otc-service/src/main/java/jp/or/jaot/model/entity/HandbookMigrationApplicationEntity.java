package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 手帳移行申請エンティティ<br>
 */

@Entity
@Table(name = "handbook_migration_applications")
@Getter
@Setter
public class HandbookMigrationApplicationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;
	
	/** 申請日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "application_date")
	private Date applicationDate;

	/** 移行種別 */
	@Column(name = "migration_type_cd")
	private String migrationTypeCd;

	/** 状態・結果 */
	@Column(name = "status_cd")
	private String statusCd;

	/** 作業療法士生涯教育概論 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "introduction_date")
	private Date introductionDate;

	/** 作業療法士生涯教育概論 チェック */
	@Column(name = "introduction_check")
	private Boolean introductionCheck;

	/**作業療法士における協業・後輩育成  受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "collaboration_date")
	private Date collaborationDate;

	/** 作業療法士における協業・後輩育成  チェック */
	@Column(name = "collaboration_check")
	private Boolean collaborationCheck;

	/**職業倫理 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "professional_ethics_date")
	private Date professionalEthicsDate;

	/** 職業倫理 チェック */
	@Column(name = "professional_ethics_check")
	private Boolean professionalEthicsCheck;

	/** 保険・医療・福祉と地域支援 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "health_medical_care_date")
	private Date healthMedicalCareDate;

	/**保険・医療・福祉と地域支援 チェック */
	@Column(name = "health_medical_care_check")
	private Boolean healthMedicalCareCheck;

	/**実践のための作業療法研究 受講日  */
	@Temporal(TemporalType.DATE)
	@Column(name = "study_for_practice_date")
	private Date studyForPracticeDate;

	/**実践のための作業療法研究 チェック */
	@Column(name = "study_for_practice_check")
	private Boolean studyForPracticeCheck;

	/**作業療法の可能性 受講日  */
	@Temporal(TemporalType.DATE)
	@Column(name = "potential_date")
	private Date potentialDate;

	/**作業療法の可能性 チェック */
	@Column(name = "potential_check")
	private Boolean potentialCheck;

	/**日本と世界の作業療法の動向 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "trend_date")
	private Date trendDate;

	/**日本と世界の作業療法の動向 チェック */
	@Column(name = "trend_check")
	private Boolean trendCheck;

	/**事例報告と事例研究 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "case_report_and_study_date")
	private Date caseReportAndStudyDate;

	/**事例報告と事例研究 チェック */
	@Column(name = "case_report_and_study_check")
	private Boolean caseReportAndStudyCheck;

	/**事例検討 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "case_consider_date")
	private Date caseConsiderDate;

	/**事例検討 チェック */
	@Column(name = "case_consider_check")
	private Boolean caseConsiderCheck;

	/**事例報告 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "case_report_date")
	private Date caseReportDate;

	/**事例報告 チェック */
	@Column(name = "case_report_check")
	private Boolean caseReportCheck;

	/**MTDLP基礎研修 受講日  */
	@Temporal(TemporalType.DATE)
	@Column(name = "mtdlp_basic_date")
	private Date mtdlpBasicDate;

	/**MTDLP基礎研修 チェック  */
	@Column(name = "mtdlp_basic_check")
	private Boolean mtdlpBasicCheck;

	/**身体障碍領域 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "disability_field_date")
	private Date disabilityFieldDate;

	/**身体障碍領域 チェック */
	@Column(name = "disability_field_check")
	private Boolean disabilityFieldCheck;

	/**精神障害領域 受講日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "mental_disorder_field_date")
	private Date mentalDisorderFieldDate;

	/**精神障害領域 チェック */
	@Column(name = "mental_disorder_field_check")
	private Boolean mentalDisorderFieldCheck;

	/** 発達障害領域 受講日*/
	@Temporal(TemporalType.DATE)
	@Column(name = "developmental_disorder_field_date")
	private Date developmentalDisorderFieldDate;

	/** 発達障害領域 チェック*/
	@Column(name = "developmental_disorder_field_check")
	private Boolean developmentalDisorderFieldCheck;

	/** 老年期領域 受講日*/
	@Temporal(TemporalType.DATE)
	@Column(name = "old_age_field_date")
	private Date oldAgeFieldDate;

	/** 老年期領域 チェック*/
	@Column(name = "old_age_field_check")
	private Boolean oldAgeFieldCheck;

	/** 基礎研修受講記録（最終頁）ポイント*/
	@Column(name = "basic_training_point")
	private Integer basicTrainingPoint;

	/** 添付 受講記録3頁目ファイル名*/
	@Column(name = "attachment_p3_file_nm")
	private String attachmentP3FileNm;

	/** 添付 受講記録3頁目ファイル */
	@Column(name = "attachment_p3_file")
	private byte[] attachmentP3File;

	/** 添付 受講記録4頁目ファイル名 */
	@Column(name = "attachment_p4_file_nm")
	private String attachmentP4FileNm;

	/** 添付 受講記録4頁目ファイル */
	@Column(name = "attachment_p4_file")
	private byte[] attachmentP4File;

	/** 添付 受講記録5頁目ファイル名 */
	@Column(name = "attachment_p5_file_nm")
	private String attachmentP5FileNm;

	/** 添付 受講記録5頁目ファイル */
	@Column(name = "attachment_p5_file")
	private byte[] attachmentP5File;

	/** 添付 基礎研修受講記録最終頁ファイル名 */
	@Column(name = "attachment_last_page_file_nm")
	private String attachmentLastPageFileNm;

	/** 添付 基礎研修受講記録最終頁ファイル */
	@Column(name = "attachment_last_page_file")
	private byte[] attachmentLastPageFile;

	/** 再入会フラグ */
	@Column(name = "re_enrollment_flag")
	private String reEnrollmentFlag;

	/** サンプルフラグ */
	@Column(name = "sampling_flag")
	private String samplingFlag;

}
