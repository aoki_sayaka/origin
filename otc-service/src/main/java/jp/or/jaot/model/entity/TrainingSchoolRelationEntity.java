package jp.or.jaot.model.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 養成校関連情報エンティティ<br>
 */
@Entity
@Table(name = "training_school_relations")
@Getter
@Setter
public class TrainingSchoolRelationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 施設養成校ID */
	@Column(name = "facility_id")
	private Long facilityId;

	/** 施設基本番号 */
	@Column(name = "facility_base_no")
	private String facilityBaseNo;

	/** 連番 */
	@Column(name = "seq_no")
	private Integer seqNo;

	/** 更新ユーザID */
	@Column(name = "relation_update_user_id")
	private String relationUpdateUserId;

	/** 更新日時 */
	@Column(name = "relation_update_date")
	private Timestamp relationUpdateDate;

	/** 更新連番 */
	@Column(name = "update_seq_no")
	private Integer updateSeqNo;

	/** 登録日付 */
	@Temporal(TemporalType.DATE)
	@Column(name = "relation_create_date")
	private Date relationCreateDate;

	/** 削除日付 */
	@Temporal(TemporalType.DATE)
	@Column(name = "relation_delete_date")
	private Date relationDeleteDate;

	/** 関連施設番号 */
	@Column(name = "relation_no")
	private String relationNo;

	/** 出身養成校番号 */
	@Column(name = "origin_school_no")
	private Integer originSchoolNo;

	/** 関連事由 */
	@Column(name = "relation_reason_cd")
	private String relationReasonCd;
}
