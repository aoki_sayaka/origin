package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.UserRevokedAuthorityEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局権限DTO
 */
@Getter
@Setter
public class UserRevokedAuthorityDto extends BaseDto<UserRevokedAuthorityDto, UserRevokedAuthorityEntity> {

	/**
	 * コンストラクタ
	 */
	private UserRevokedAuthorityDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private UserRevokedAuthorityDto(UserRevokedAuthorityEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static UserRevokedAuthorityDto create() {
		return new UserRevokedAuthorityDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static UserRevokedAuthorityDto createSingle() {
		return new UserRevokedAuthorityDto(new UserRevokedAuthorityEntity());
	}
}
