package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修修了認定更新履歴DTO
 */
@Getter
@Setter
public class QualifiedOtCompletionHistoryDto
		extends BaseDto<QualifiedOtCompletionHistoryDto, QualifiedOtCompletionHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private QualifiedOtCompletionHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private QualifiedOtCompletionHistoryDto(QualifiedOtCompletionHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static QualifiedOtCompletionHistoryDto create() {
		return new QualifiedOtCompletionHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static QualifiedOtCompletionHistoryDto createSingle() {
		return new QualifiedOtCompletionHistoryDto(new QualifiedOtCompletionHistoryEntity());
	}
}
