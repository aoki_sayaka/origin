package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.ClinicalTrainingLeaderQualifyHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者認定履歴DTO
 */
@Getter
@Setter
public class ClinicalTrainingLeaderQualifyHistoryDto
		extends BaseDto<ClinicalTrainingLeaderQualifyHistoryDto, ClinicalTrainingLeaderQualifyHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private ClinicalTrainingLeaderQualifyHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ClinicalTrainingLeaderQualifyHistoryDto(ClinicalTrainingLeaderQualifyHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ClinicalTrainingLeaderQualifyHistoryDto create() {
		return new ClinicalTrainingLeaderQualifyHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ClinicalTrainingLeaderQualifyHistoryDto createSingle() {
		return new ClinicalTrainingLeaderQualifyHistoryDto(new ClinicalTrainingLeaderQualifyHistoryEntity());
	}
}
