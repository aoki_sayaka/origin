package jp.or.jaot.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会仮申込エンティティ<br>
 */
@Entity
@Table(name = "training_temp_applications")
@Getter
@Setter
public class TrainingTempApplicationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 研修会番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** メールアドレス */
	@Column(name = "mail_address")
	private String mailAddress;

	/** 対象疾患 */
	@Column(name = "target_disease")
	private String targetDisease;

	/** 症例提供 */
	@Column(name = "case_provide_cd")
	private String caseProvideCd;

	/** 経験年数 */
	@Column(name = "experience_year")
	private Integer experienceYear;

	/** 締切日時 */
	@Column(name = "application_deadline_datetime")
	private Timestamp applicationDeadlineDatetime;
}
