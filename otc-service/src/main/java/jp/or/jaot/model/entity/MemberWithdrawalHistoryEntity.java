package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

	
/**
* 退会履歴エンティティ<br>
 */
@Entity
@Table(name = "member_withdrawal_histories")
@Getter
@Setter
public class MemberWithdrawalHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;
	
	/** 退会種類 */
	@Column(name = "withdrawal_type")
	private String withdrawalType;
	
	/** 年度 */
	@Column(name = "fiscal_year")
	private Integer fiscalYear;
	
	/** 退会状況コード */
	@Column(name = "state_cd")
	private String stateCd;
	
	/** 届出受付日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "reception_date")
	private Date receptionDate;
	
	/** 退会年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "withdrawal_date")
	private Date withdrawalDate;
	
	/** 会費状況 */
	@Column(name = "member_fee")
	private Integer memberFee;
	
	/** 退会理由 */
	@Column(name = "withdrawal_reason")
	private String withdrawalReason;
	
	/** 取下日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "cancellation_date")
	private Date cancellationDate;
	
	/** 備考 */
	@Column(name = "remarks")
	private String remarks;
	
	/** 印刷年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "print_date")
	private Date printDate;

}
