package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.TrainingMemberCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会会員DTO
 */
@Getter
@Setter
public class TrainingMemberDto extends BaseDto<TrainingMemberDto, TrainingMemberCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingMemberDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingMemberDto(TrainingMemberCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingMemberDto create() {
		return new TrainingMemberDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingMemberDto createSingle() {
		return new TrainingMemberDto(new TrainingMemberCompositeEntity());
	}
}
