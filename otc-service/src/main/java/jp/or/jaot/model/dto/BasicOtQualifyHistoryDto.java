package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修修了認定更新履歴DTO
 */
@Getter
@Setter
public class BasicOtQualifyHistoryDto extends BaseDto<BasicOtQualifyHistoryDto, BasicOtQualifyHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private BasicOtQualifyHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private BasicOtQualifyHistoryDto(BasicOtQualifyHistoryEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static BasicOtQualifyHistoryDto create() {
		return new BasicOtQualifyHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static BasicOtQualifyHistoryDto createSingle() {
		return new BasicOtQualifyHistoryDto(new BasicOtQualifyHistoryEntity());
		//CompositeEntityに変更（複合）
	}

}
