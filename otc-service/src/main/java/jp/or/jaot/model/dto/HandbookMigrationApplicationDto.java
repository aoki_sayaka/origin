package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.HandbookMigrationApplicationCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 手帳移行申請DTO
 */
@Getter
@Setter
public class HandbookMigrationApplicationDto
		extends BaseDto<HandbookMigrationApplicationDto, HandbookMigrationApplicationCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private HandbookMigrationApplicationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private HandbookMigrationApplicationDto(HandbookMigrationApplicationCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static HandbookMigrationApplicationDto create() {
		return new HandbookMigrationApplicationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static HandbookMigrationApplicationDto createSingle() {
		return new HandbookMigrationApplicationDto(new HandbookMigrationApplicationCompositeEntity());
	}
}
