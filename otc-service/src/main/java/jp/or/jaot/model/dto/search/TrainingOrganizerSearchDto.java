package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会主催者検索DTO
 */
@Getter
@Setter
public class TrainingOrganizerSearchDto extends BaseSearchDto {

	/** 研修会番号 */
	private String[] trainingNos;

	// TODO : 検索条件追加
}
