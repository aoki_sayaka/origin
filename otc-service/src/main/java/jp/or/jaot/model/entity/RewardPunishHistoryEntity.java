package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 賞罰履歴エンティティ<br>
 */
@Entity
@Table(name = "reward_punish_histories")
@Getter
@Setter
public class RewardPunishHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 賞罰コード */
	@Column(name = "reward_punish_cd")
	private String rewardPunishCd;

	/** 賞罰分類 */
	@Column(name = "reward_punish_type")
	private Integer rewardPunishType;

	/** 年度 */
	@Column(name = "fiscal_year")
	private Integer fiscalYear;

	/** 備考 */
	private String remarks;
}
