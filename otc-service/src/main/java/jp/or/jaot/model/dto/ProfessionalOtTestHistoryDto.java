package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.ProfessionalOtTestHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士試験結果履歴DTO
 */
@Getter
@Setter
public class ProfessionalOtTestHistoryDto
		extends BaseDto<ProfessionalOtTestHistoryDto, ProfessionalOtTestHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private ProfessionalOtTestHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ProfessionalOtTestHistoryDto(ProfessionalOtTestHistoryEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ProfessionalOtTestHistoryDto create() {
		return new ProfessionalOtTestHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ProfessionalOtTestHistoryDto createSingle() {
		return new ProfessionalOtTestHistoryDto(new ProfessionalOtTestHistoryEntity());
		//CompositeEntityに変更（複合）
	}

}
