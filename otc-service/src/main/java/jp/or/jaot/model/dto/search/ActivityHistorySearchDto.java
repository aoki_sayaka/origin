package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動履歴検索DTO
 */
@Getter
@Setter
public class ActivityHistorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	// TODO : 検索条件追加
}
