package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 領域関連中分類エンティティ<br>
 */
@Entity
@Table(name = "region_related_middle_classifications")
@Getter
@Setter
public class RegionRelatedMiddleClassificationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** コード識別ID */
	private String codeId;

	/** コード値 */
	private String code;

	/** 内容 */
	private String content;

	/** 省略内容 */
	@Column(name = "simple_content")
	private String simpleContent;

	/** 親コード値 */
	@Column(name = "parent_code")
	private String parentCode;

	/** 表示順序 */
	@Column(name = "sort_order")
	private Integer sortOrder;
}
