package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.BasicPointTrainingHistoryCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎ポイント研修履歴DTO
 */
@Getter
@Setter
public class BasicPointTrainingHistoryDto
		extends BaseDto<BasicPointTrainingHistoryDto, BasicPointTrainingHistoryCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private BasicPointTrainingHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private BasicPointTrainingHistoryDto(BasicPointTrainingHistoryCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static BasicPointTrainingHistoryDto create() {
		return new BasicPointTrainingHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static BasicPointTrainingHistoryDto createSingle() {
		return new BasicPointTrainingHistoryDto(new BasicPointTrainingHistoryCompositeEntity());
	}

}
