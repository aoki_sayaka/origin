package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴DTO
 */
@Getter
@Setter
public class QualifiedOtTrainingHistoryDto
		extends BaseDto<QualifiedOtTrainingHistoryDto, QualifiedOtTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private QualifiedOtTrainingHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private QualifiedOtTrainingHistoryDto(QualifiedOtTrainingHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static QualifiedOtTrainingHistoryDto create() {
		return new QualifiedOtTrainingHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static QualifiedOtTrainingHistoryDto createSingle() {
		return new QualifiedOtTrainingHistoryDto(new QualifiedOtTrainingHistoryEntity());
	}
}
