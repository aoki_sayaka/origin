package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.StringUtils;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会申込エンティティ(複合)<br>
 * ・研修会申込と関連を持つ親情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 * ・リスト項目表示に必要な値を返却する<br>
 */
@Getter
@Setter
public class TrainingApplicationCompositeEntity implements Entity {

	/** 研修会申込 */
	private TrainingApplicationEntity trainingApplicationEntity = new TrainingApplicationEntity();

	/** 研修会[親] */
	private TrainingEntity trainingEntity = new TrainingEntity();

	/**
	 * 開催日(文字列)取得
	 * @return 開催日文字列(カンマ区切り)
	 */
	public String getEventDateText() {

		List<String> texts = new ArrayList<String>();

		// 開催日1
		if (trainingEntity.getEventDate1() != null) {
			texts.add(DateUtil.formatYmdeString(trainingEntity.getEventDate1()));
		}
		// 開催日2
		if (trainingEntity.getEventDate2() != null) {
			texts.add(DateUtil.formatYmdeString(trainingEntity.getEventDate2()));
		}
		// 開催日3
		if (trainingEntity.getEventDate3() != null) {
			texts.add(DateUtil.formatYmdeString(trainingEntity.getEventDate3()));
		}
		// 開催日フリー
		if (StringUtils.hasText(trainingEntity.getEventDateFree())) {
			texts.add(trainingEntity.getEventDateFree());
		}

		return texts.stream().map(E -> E.toString()).collect(Collectors.joining(","));
	}
}
