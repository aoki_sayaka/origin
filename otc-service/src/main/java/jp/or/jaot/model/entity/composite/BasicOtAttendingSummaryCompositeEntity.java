package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.BasicOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.BasicOtQualifyHistoryEntity;
import jp.or.jaot.model.entity.BasicOtTrainingHistoryEntity;
import jp.or.jaot.model.entity.PeriodExtensionApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修履歴エンティティ(複合)<br>
 * ・基礎研修履歴と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class BasicOtAttendingSummaryCompositeEntity implements Entity {

	// 基礎研修履歴
	private BasicOtAttendingSummaryEntity basicOtAttendingSummaryEntity = new BasicOtAttendingSummaryEntity();

	// 基礎研修修了認定更新履歴
	private List<BasicOtQualifyHistoryEntity> basicOtQualifyHistoryEntities = new ArrayList<BasicOtQualifyHistoryEntity>();

	// 基礎研修修了認定更新履歴
	private List<BasicOtTrainingHistoryEntity> basicOtTrainingHistoryEntities = new ArrayList<BasicOtTrainingHistoryEntity>();

	// 期間延長申請
	private List<PeriodExtensionApplicationEntity> periodExtensionApplicationEntities = new ArrayList<PeriodExtensionApplicationEntity>();
}
