package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会主催者エンティティ<br>
 */
@Entity
@Table(name = "training_organizers")
@Getter
@Setter
public class TrainingOrganizerEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 研修会番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 連番 */
	@Column(name = "seq_no")
	private Integer seqNo;

	/** 主催者コード */
	@Column(name = "organizer_cd")
	private String organizerCd;
}
