package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.TrainingInstructorEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会講師実績DTO
 */
@Getter
@Setter
public class TrainingInstructorDto extends BaseDto<TrainingInstructorDto, TrainingInstructorEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingInstructorDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingInstructorDto(TrainingInstructorEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingInstructorDto create() {
		return new TrainingInstructorDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingInstructorDto createSingle() {
		return new TrainingInstructorDto(new TrainingInstructorEntity());
	}
}
