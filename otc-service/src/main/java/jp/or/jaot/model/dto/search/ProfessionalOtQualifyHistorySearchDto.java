package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士認定更新履歴検索DTO
 */
@Getter
@Setter
public class ProfessionalOtQualifyHistorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;
}
