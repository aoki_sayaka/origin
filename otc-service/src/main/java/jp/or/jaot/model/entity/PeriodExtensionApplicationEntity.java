package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 期間延長申請エンティティ<br>
 */
@Entity
@Table(name = "period_extension_applications")
@Getter
@Setter
public class PeriodExtensionApplicationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 申請日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "application_date")
	private Date applicationDate;

	/** 申請資格 */
	@Column(name = "qualification_cd")
	private String qualificationCd;

	/** 期間延長理由 */
	@Column(name = "application_reason_cd")
	private String applicationReasonCd;

	/** その他の理由 */
	@Column(name = "application_reason_other")
	private String applicationReasonOther;

	/** 状態結果 */
	@Column(name = "application_result_cd")
	private String applicationResultCd;

	/** 申請状態 */
	@Column(name = "application_status_cd")
	private String applicationStatusCd;

	/** 事務局確認日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "secretariat_confirm_date")
	private Date secretariatConfirmDate;

	/** 休止期間(自) */
	@Temporal(TemporalType.DATE)
	@Column(name = "rest_period_from")
	private Date restPeriodFrom;

	/** 休止期間(至) */
	@Temporal(TemporalType.DATE)
	@Column(name = "rest_period_to")
	private Date restPeriodTo;

	/** 証明書ファイル名 */
	@Column(name = "certificate_file_nm")
	private String certificateFileNm;

	/** 証明書ファイル */
	@Column(name = "certificate_file")
	private byte[] certificateFile;
}
