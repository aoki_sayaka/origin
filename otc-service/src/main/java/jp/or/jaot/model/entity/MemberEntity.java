package jp.or.jaot.model.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.annotations.Formula;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員エンティティ<br>
 * ・BaseEntityに定義済みの共通カラムは本エンティティから削除する<br>
 * ・getter/setterもlombokを使用するため本エンティティから削除する<br>
 */
@Entity
@Table(name = "members")
@Getter
@Setter
public class MemberEntity extends BaseEntity implements UserDetails {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 所属都道府県コード */
	@Column(name = "affiliated_prefecture_cd")
	private String affiliatedPrefectureCd;

	/** 年齢 */
	private Integer age;

	/** 出身校名 */
	@Column(name = "alma_mater_name")
	private String almaMaterName;

	/** 県士会所属フラグ */
	@Column(name = "belong_pref_association")
	private Boolean belongPrefAssociation;

	/** 請求書送付先国内フラグ */
	@Column(name = "bill_to_japan_address")
	private Boolean billToJapanAddress;

	/** 生年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "birth_day")
	private Date birthDay;

	/** 児童福祉法関連施設 */
	@Column(name = "child_welfare_facility")
	private String childWelfareFacility;

	/** 児童福祉法指定サービス分類コード */
	@Column(name = "child_welfare_service_category_cd")
	private String childWelfareServiceCategoryCd;

	/** 学位１ 授与年 */
	@Column(name = "degree1_award_year")
	private Integer degree1AwardYear;

	/** 学位１ 教育施設名 */
	@Column(name = "degree1_education_facility_name")
	private String degree1EducationFacilityName;

	/** 学位１ 分野名 */
	@Column(name = "degree1_field_name")
	private String degree1FieldName;

	/** 学位１ 区分番号 */
	@Column(name = "degree1_no")
	private Integer degree1No;

	/** 学位２ 授与年 */
	@Column(name = "degree2_award_year")
	private Integer degree2AwardYear;

	/** 学位２ 教育施設名 */
	@Column(name = "degree2_education_facility_name")
	private String degree2EducationFacilityName;

	/** 学位２ 分野名 */
	@Column(name = "degree2_field_name")
	private String degree2FieldName;

	/** 学位２ 区分番号 */
	@Column(name = "degree2_no")
	private Integer degree2No;

	/** 学位３ 授与年 */
	@Column(name = "degree3_award_year")
	private Integer degree3AwardYear;

	/** 学位３ 教育施設名 */
	@Column(name = "degree3_education_facility_name")
	private String degree3EducationFacilityName;

	/** 学位３ 分野名 */
	@Column(name = "degree3_field_name")
	private String degree3FieldName;

	/** 学位３ 区分番号 */
	@Column(name = "degree3_no")
	private Integer degree3No;

	/** 学位４ 授与年 */
	@Column(name = "degree4_award_year")
	private Integer degree4AwardYear;

	/** 学位４ 教育施設名 */
	@Column(name = "degree4_education_facility_name")
	private String degree4EducationFacilityName;

	/** 学位４ 分野名 */
	@Column(name = "degree4_field_name")
	private String degree4FieldName;

	/** 学位４ 区分番号 */
	@Column(name = "degree4_no")
	private Integer degree4No;

	/** 学位５ 授与年 */
	@Column(name = "degree5_award_year")
	private Integer degree5AwardYear;

	/** 学位５ 教育施設名 */
	@Column(name = "degree5_education_facility_name")
	private String degree5EducationFacilityName;

	/** 学位５ 分野名 */
	@Column(name = "degree5_field_name")
	private String degree5FieldName;

	/** 学位５ 区分番号 */
	@Column(name = "degree5_no")
	private Integer degree5No;

	/** 災害支援ボランティア希望 */
	@Column(name = "disaster_support_volunteerable")
	private Boolean disasterSupportVolunteerable;

	/** 生涯教育単位数合計 */
	@Column(name = "education_credits")
	private Integer educationCredits;

	/** Email address */
	@Column(name = "email_address")
	private String emailAddress;

	/** 入会年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "entry_date")
	private Date entryDate;

	/** 認可分類コード */
	@Column(name = "facility_auth_category_cd")
	private String facilityAuthCategoryCd;

	/** 主業務領域コード */
	@Column(name = "facility_domain_cd")
	private String facilityDomainCd;

	/** 主障害種別 */
	@Column(name = "facility_handicap_type")
	private String facilityHandicapType;

	/** 施設情報開示拒否フラグ */
	@Column(name = "facility_info_hide")
	private Boolean facilityInfoHide;

	/** 従業務領域コード */
	@Column(name = "facility_sub_domain_cd")
	private String facilitySubDomainCd;

	/** 従障害種別 */
	@Column(name = "facility_sub_handicap_type")
	private String facilitySubHandicapType;

	/** 従対象疾患 */
	@Column(name = "facility_sub_target_disease")
	private String facilitySubTargetDisease;

	/** 主対象疾患 */
	@Column(name = "facility_target_disease")
	private String facilityTargetDisease;

	/** フリガナ(名) */
	@Column(name = "first_kana_name")
	private String firstKanaName;

	/** 姓名(名) */
	@Column(name = "first_name")
	private String firstName;

	/** 海外の住所情報 */
	@Column(name = "foreign_address")
	private String foreignAddress;

	/** 外国免許フラグ */
	@Column(name = "foreign_license")
	private Boolean foreignLicense;

	/** 性別 */
	private Integer gender;

	/** 学位取得 */
	@Column(name = "has_degree")
	private Boolean hasDegree;

	/** 自宅住所１ */
	@Column(name = "home_address1")
	private String homeAddress1;

	/** 自宅住所２ */
	@Column(name = "home_address2")
	private String homeAddress2;

	/** 自宅住所３ */
	@Column(name = "home_address3")
	private String homeAddress3;

	/** 自宅電話番号 */
	@Column(name = "home_phone_number")
	private String homePhoneNumber;

	/** 自宅都道府県コード */
	@Column(name = "home_prefecture_cd")
	private String homePrefectureCd;

	/** 自宅郵便番号 */
	@Column(name = "home_zipcode")
	private String homeZipcode;

	/** 名誉会員 */
	@Column(name = "honorary_membership")
	private Boolean honoraryMembership;

	/** 名誉会員取得年 */
	@Column(name = "honorary_membership_acquired_year")
	private Integer honoraryMembershipAcquiredYear;

	/** 学術誌送付媒体区分 */
	@Column(name = "journal_send_category_cd")
	private String journalSendCategoryCd;

	/** フリガナ(姓) */
	@Column(name = "last_kana_name")
	private String lastKanaName;

	/** 姓名(姓) */
	@Column(name = "last_name")
	private String lastName;

	/** 免許番号 */
	@Column(name = "license_number")
	private String licenseNumber;

	/** 旧姓 */
	@Column(name = "maiden_name")
	private String maidenName;

	/** 会員名簿フラグ */
	@Column(name = "member_list")
	private Boolean memberList;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 会員証発行フラグ */
	@Column(name = "membership_certificate_issuanced")
	private Boolean membershipCertificateIssuanced;

	/** 会費合計 */
	@Column(name = "membership_fee_balance")
	private Integer membershipFeeBalance;

	/** 携帯電話番号 */
	@Column(name = "mobile_phone_number")
	private String mobilePhoneNumber;

	/** 生活行為向上リハビリテーション実施加算 */
	@Column(name = "mtdlp_rehabilitation_addition")
	private Boolean mtdlpRehabilitationAddition;

	/** 協会メール受取可否 */
	@Column(name = "ot_association_mail_receive_cd")
	private Integer otAssociationMailReceiveCd;

	/** その他勤務施設１業務内容 */
	@Column(name = "other_facility1_domain")
	private String otherFacility1Domain;

	/** その他勤務施設１ */
	@Column(name = "other_facility1_name")
	private String otherFacility1Name;

	/** その他勤務施設２業務内容 */
	@Column(name = "other_facility2_domain")
	private String otherFacility2Domain;

	/** その他勤務施設２ */
	@Column(name = "other_facility2_name")
	private String otherFacility2Name;

	/** 前勤務先の都道府県コード */
	@Column(name = "pre_working_prefecture_cd")
	private String preWorkingPrefectureCd;

	/** 県士会支部番号 */
	@Column(name = "pref_association_branch_cd")
	private Integer prefAssociationBranchCd;

	/** 県士会向け生涯教育開示拒否フラグ */
	@Column(name = "pref_association_education_info_hide")
	private Boolean prefAssociationEducationInfoHide;

	/** 県士会会費合計 */
	@Column(name = "pref_association_fee_balance")
	private Integer prefAssociationFeeBalance;

	/** 県士会メール受取可否 */
	@Column(name = "pref_association_mail_receive_cd")
	private Integer prefAssociationMailReceiveCd;

	/** 県士会向け会員情報開示拒否フラグ */
	@Column(name = "pref_association_member_info_hide")
	private Boolean prefAssociationMemberInfoHide;

	/** 県士会番号 */
	@Column(name = "pref_association_no")
	private Integer prefAssociationNo;

	/** 県士会非会員フラグ */
	@Column(name = "pref_association_nonmember")
	private Boolean prefAssociationNonmember;

	/** 県士会都道府県コード */
	@Column(name = "pref_association_prefecture_cd")
	private String prefAssociationPrefectureCd;

	/** 県士会発送区分番号 */
	@Column(name = "pref_association_shipping_category_no")
	private Integer prefAssociationShippingCategoryNo;

	/** 県士会更新年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "pref_association_update_date")
	private Date prefAssociationUpdateDate;

	/** 県士会使用欄 */
	@Column(name = "pref_association_value")
	private Integer prefAssociationValue;

	/** 旧会員番号 */
	@Column(name = "previous_member_no")
	private Integer previousMemberNo;

	/** 印刷フラグ */
	@Column(name = "print_enabled")
	private Boolean printEnabled;

	/** 資格取得年 */
	@Column(name = "qualification_year")
	private Integer qualificationYear;

	/** 備考 */
	private String remarks;

	/** 発送区分番号 */
	@Column(name = "shipping_category_no")
	private Integer shippingCategoryNo;

	/** ステータス番号 */
	@Column(name = "status_no")
	private Integer statusNo;

	/** 対象者の年齢 */
	@Column(name = "target_age")
	private Integer targetAge;

	/** 障害者自立支援法サービス分類コード */
	@Column(name = "target_independence_support_category_cd")
	private String targetIndependenceSupportCategoryCd;

	/** 対象者のOT日数 */
	@Column(name = "target_ot_day_count")
	private Integer targetOtDayCount;

	/** 対象者属性の主目的 */
	@Column(name = "target_primary_purpose")
	private String targetPrimaryPurpose;

	/** 卒業養成校番号 */
	@Column(name = "training_school_no")
	private Integer trainingSchoolNo;

	/** 異動確認フラグ */
	@Column(name = "transfer_confirmation")
	private Boolean transferConfirmation;

	/** 会員ポータル更新年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "update_from_portal_date")
	private Date updateFromPortalDate;

	/** WEB会員ID */
	@Column(name = "web_member_id")
	private String webMemberId;

	/** WEB会員氏名 */
	@Column(name = "web_member_name")
	private String webMemberName;

	/** WEB会員パスワード */
	@Column(name = "web_member_password")
	private String webMemberPassword;

	/** 匿名希望会員フラグ */
	@Column(name = "web_published")
	private Boolean webPublished;

	/** 加入WFOT番号 */
	@Column(name = "wfot_entry_status")
	private Integer wfotEntryStatus;

	/** 退会年月日 */
	@Column(name = "withdrawal_date")
	private Date withdrawalDate;

	/** 勤務先電話番号 */
	@Column(name = "work_phone_number")
	private String workPhoneNumber;

	/** 勤務条件 */
	@Column(name = "working_condition")
	private String workingCondition;

	/** 勤務条件（施設数）  */
	@Column(name = "working_condition_facility_count")
	private Integer workingConditionFacilityCount;

	/** 勤務条件（施設名） */
	@Column(name = "working_condition_facility_name")
	private String workingConditionFacilityName;

	/** 勤務先施設コード */
	@Column(name = "working_facility_cd")
	private String workingFacilityCd;

	/** 勤務先部署名 */
	@Column(name = "working_facility_unit_name")
	private String workingFacilityUnitName;

	/** 勤務先都道府県コード */
	@Column(name = "working_prefecture_cd")
	private String workingPrefectureCd;

	/** 就業状況区分コード */
	@Column(name = "working_state_cd")
	private String workingStateCd;

	/** 会員コード */
	@Column(name = "member_cd")
	private String memberCd;

	/** セキュリティコード */
	@Column(name = "security_cd")
	private String securityCd;

	/** 認定開示フラグ */
	private Boolean disclosed;

	/** 氏名(フルネーム)[読取専用] */
	@Formula(value = "last_name || first_name")
	private String fullName;

	/** 氏名(フルネーム:半角空白区切り)[読取専用] */
	@Formula(value = "last_name || chr(32) || first_name")
	private String fullNameFormat;

	/*
	 * 認証関連メソッド
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		return webMemberPassword; // WEB会員パスワード
	}

	@Override
	public String getUsername() {
		return memberNo.toString(); // 会員番号
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return BooleanUtils.isFalse(getDeleted()); // 削除時は無効
	}

	public String setEmailAddress() {
		return emailAddress; // 会員メールアドレス

	}
}