package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修番号マスタ検索DTO
 */
@Getter
@Setter
public class TrainingNumberSearchDto extends BaseSearchDto {

	/** 年度 */
	private Integer fiscalYear;

	/** 研修番号 */
	private String trainingNo;
}
