package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.ProfessionalOtAttendingSummaryCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士受講履歴DTO
 */
@Getter
@Setter
public class ProfessionalOtAttendingSummaryDto
		extends BaseDto<ProfessionalOtAttendingSummaryDto, ProfessionalOtAttendingSummaryCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private ProfessionalOtAttendingSummaryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ProfessionalOtAttendingSummaryDto(ProfessionalOtAttendingSummaryCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ProfessionalOtAttendingSummaryDto create() {
		return new ProfessionalOtAttendingSummaryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ProfessionalOtAttendingSummaryDto createSingle() {
		return new ProfessionalOtAttendingSummaryDto(new ProfessionalOtAttendingSummaryCompositeEntity());
	}

}
