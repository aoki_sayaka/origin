package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修修了認定更新履歴エンティティ<br>
 */
@Entity
@Table(name = "qualified_ot_completion_histories")
@Getter
@Setter
public class QualifiedOtCompletionHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 回数 */
	@Column(name = "seq")
	private Integer seq;

	/** 修了年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "completion_date")
	private Date completionDate;

}
