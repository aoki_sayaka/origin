package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.FacilityApplicationCategoryEntity;
import jp.or.jaot.model.entity.FacilityApplicationEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設登録申請エンティティ(複合)<br>
 * ・施設登録申請と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class FacilityApplicationCompositeEntity implements Entity {

	/** 施設登録申請 */
	private FacilityApplicationEntity facilityApplicationEntity = new FacilityApplicationEntity();

	/** 施設登録申請分類 */
	private List<FacilityApplicationCategoryEntity> facilityApplicationCategoryEntities = new ArrayList<FacilityApplicationCategoryEntity>();
}
