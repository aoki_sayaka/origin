package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.MtdlpCaseEntity;
import jp.or.jaot.model.entity.MtdlpCaseReportEntity;
import jp.or.jaot.model.entity.MtdlpQualifyHistoryEntity;
import jp.or.jaot.model.entity.MtdlpSummaryEntity;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP履歴エンティティ(複合)<br>
 * ・会員情報と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class MtdlpCompositeEntity implements Entity {

	// MTDLP履歴
	private MtdlpSummaryEntity mtdlpSummaryEntity = new MtdlpSummaryEntity();

	// MTDLP認定履歴
	private List<MtdlpQualifyHistoryEntity> mtdlpQualifyHistoryEntities = new ArrayList<MtdlpQualifyHistoryEntity>();

	// MTDLP研修受講履歴
	private List<MtdlpTrainingHistoryEntity> mtdlpTrainingHistoryEntities = new ArrayList<MtdlpTrainingHistoryEntity>();

	// MTDLP事例
	private List<MtdlpCaseEntity> mtdlpCaseEntities = new ArrayList<MtdlpCaseEntity>();

	// MTDLP事例報告
	private List<MtdlpCaseReportEntity> mtdlpCaseReportEntities = new ArrayList<MtdlpCaseReportEntity>();
}
