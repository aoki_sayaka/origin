package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LifelongEducationStatusSearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;
}
