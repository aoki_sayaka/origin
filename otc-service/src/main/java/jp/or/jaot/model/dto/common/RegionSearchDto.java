package jp.or.jaot.model.dto.common;

import lombok.Getter;
import lombok.Setter;

/**
 * 領域検索DTO
 */
@Getter
@Setter
public class RegionSearchDto {

	/** 分類種別 */
	private Integer category;

	/** 該当(true=該当あり, false=該当なし) */
	private Boolean isApplicable = false; // 該当なし

	/** 分類 */
	private ClassificationSearchDto[] classifications;
}
