package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(施設)検索DTO
 */
@Getter
@Setter
public class NoticeConditionFacilitySearchDto extends BaseSearchDto {

	/** お知らせID */
	private Long[] noticeIds;

	// TODO : 検索条件追加
}
