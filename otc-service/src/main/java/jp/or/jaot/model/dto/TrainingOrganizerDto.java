package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.TrainingOrganizerEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会主催者DTO
 */
@Getter
@Setter
public class TrainingOrganizerDto extends BaseDto<TrainingOrganizerDto, TrainingOrganizerEntity> {

	/**
	 * コンストラクタ
	 */
	private TrainingOrganizerDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private TrainingOrganizerDto(TrainingOrganizerEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static TrainingOrganizerDto create() {
		return new TrainingOrganizerDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static TrainingOrganizerDto createSingle() {
		return new TrainingOrganizerDto(new TrainingOrganizerEntity());
	}
}
