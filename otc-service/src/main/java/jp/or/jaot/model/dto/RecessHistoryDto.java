package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.RecessHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 休会履歴DTO
 */
@Getter
@Setter
public class RecessHistoryDto extends BaseDto<RecessHistoryDto, RecessHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private RecessHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private RecessHistoryDto(RecessHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static RecessHistoryDto create() {
		return new RecessHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static RecessHistoryDto createSingle() {
		return new RecessHistoryDto(new RecessHistoryEntity());
	}
}
