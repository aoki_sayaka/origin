package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 専門作業療法士研修受講履歴検索DTO
 */
@Getter
@Setter
public class ProfessionalOtTrainingHistorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;
}
