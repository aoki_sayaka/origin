package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校勤務者エンティティ<br>
 */
@Entity
@Table(name = "facility_members")
@Getter
@Setter
public class FacilityMemberEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 施設養成校ID */
	@Column(name = "facility_id")
	private Long facilityId;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;
}
