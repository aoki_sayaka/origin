package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(宛先)検索DTO
 */
@Getter
@Setter
public class NoticeDestinationSearchDto extends BaseSearchDto {

	/** お知らせID */
	private Long[] noticeIds;

	/** 会員番号 */
	private Integer memberNo;

	// TODO : 検索条件追加
}
