package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴エンティティ<br>
 */
@Entity
@Table(name = "qualification_ot_attending_histories")
@Getter
@Setter
public class QualificationOtAttendingHistoryEntity  extends BaseEntity{
	
	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;
	
	/** 回数 */
	@Temporal(TemporalType.DATE)
	@Column(name = "attend_date")
	private Date attendDate;
	
	/** カテゴリ */
	@Column(name = "category")
	private String category;
	
	/** 専門分野 */
	@Column(name = "professional_field")
	private String professionalField;
	
	/** 研修種別 */
	@Column(name = "training_type")
	private String trainingType;
	
	/** 研修名 */
	@Column(name = "training_name")
	private String trainingName;
	
	/** 回数 */
	@Column(name = "seq")
	private Integer seq;
	
	/** 免除理由コード */
	@Column(name = "remission_cause_cd")
	private String remissionCausecd;
	
	/** 免除資格コード */
	@Column(name = "remission_qualification_cd")
	private String remission_QualificationCd;
	
	/** 合否判定コード */
	@Column(name = "acceptance_cd")
	private String acceptanceCd;
	
	/** 主催者コード */
	@Column(name = "promoter_cd")
	private String promoterCd;
	
	/** 他団体・SIGコード */
	@Column(name = "others_sig_cd")
	private String othersSigCd;
	
	/** 内容 */
	@Column(name = "content")
	private String content;
	
	/** ポイント種別コード */
	@Column(name = "point_type_cd")
	private String pointTypeCd;
	
	/** 日数 */
	@Column(name = "days")
	private Integer days;
	
	/** ポイント */
	@Column(name = "point")
	private Integer point;
	
	/** 施設番号 */
	@Column(name = "facility_no")
	private String facilityNo;
	
	/** ポイント使用フラグ */
	@Column(name = "use_point")
	private boolean usePoint;
	
	/** 照会会員氏名 */
	@Column(name = "collation_member_name")
	private String collationMemberName;
	
	/** 照会率 */
	@Column(name = "collation_rate")
	private String collationRate;
	
	/** 開始日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Date startDate;
	
	/** 終了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Date endDate;
	
	/** チェックフラグ */
	@Column(name = "check_marked")
	private boolean checkMarked;
	
	/** 備考 */
	@Column(name = "remarks")
	private String remarks;
	
}
