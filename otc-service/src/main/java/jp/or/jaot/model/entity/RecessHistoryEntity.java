package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 休会履歴エンティティ<br>
 */
@Entity
@Table(name = "recess_histories")
@Getter
@Setter
public class RecessHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 回数 */
	private Integer times;

	/** 休会手続き状況 */
	@Column(name = "process_status")
	private String processStatus;

	/** 休会理由 */
	@Column(name = "recess_reason")
	private String recessReason;

	/** 不受理理由 */
	@Column(name = "reject_reason")
	private String rejectReason;

	/** 休会開始年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "start_recess_date")
	private Date startRecessDate;

	/** 休会終了年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "end_recess_date")
	private Date endRecessDate;

	/** 取り下げ手続き状況 */
	@Column(name = "withdrawal_process_status")
	private String withdrawalProcessStatus;

	/** 取り下げ確定日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "withdrawal_date")
	private Date withdrawalDate;

	/** 取り下げ理由 */
	@Column(name = "withdrawal_reason")
	private String withdrawalReason;

	/** 復会年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "comeback_date")
	private Date comebackDate;

	/** 印刷年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "print_date")
	private Date printDate;

	/** 勤務先データ削除フラグ */
	@Column(name = "work_place_data_deletion")
	private Boolean workPlaceDataDeletion;

	/** 証明書受領フラグ */
	@Column(name = "certificate_received")
	private Boolean certificateReceived;

	/** 備考 */
	private String remarks;
}
