package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 活動実績(後輩育成・社会貢献)検索DTO
 */
@Getter
@Setter
public class SocialContributionActualResultSearchDto extends BaseSearchDto {

	/** ID */
	private Long id;

	/** 会員番号 */
	private Integer memberNo;

	// TODO : 検索条件追加
}
