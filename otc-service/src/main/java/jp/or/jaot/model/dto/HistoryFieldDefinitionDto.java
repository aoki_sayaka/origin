package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.HistoryFieldDefinitionEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 履歴フィールド定義DTO
 */
@Getter
@Setter
public class HistoryFieldDefinitionDto extends BaseDto<HistoryFieldDefinitionDto, HistoryFieldDefinitionEntity> {

	/**
	 * コンストラクタ
	 */
	private HistoryFieldDefinitionDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private HistoryFieldDefinitionDto(HistoryFieldDefinitionEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static HistoryFieldDefinitionDto create() {
		return new HistoryFieldDefinitionDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static HistoryFieldDefinitionDto createSingle() {
		return new HistoryFieldDefinitionDto(new HistoryFieldDefinitionEntity());
	}
}
