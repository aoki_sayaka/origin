package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修履歴エンティティ<br>
 * ・自動生成されたクラスのメンバ変数定義を最大限流用するため、各メンバ変数へのコメントは記述しない<br>
 * ・BaseEntityに定義済みの共通カラムは本エンティティから削除する<br>
 * ・getter/setterもlombokを使用するため本エンティティから削除する<br>
 */
@Entity
@Table(name = "qualified_ot_attending_summaries")
@Getter
@Setter
public class QualifiedOtAttendingSummaryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 認定年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "qualification_date")
	private Date qualificationDate;

	/** 認定期間満了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "qualification_period_date")
	private Date qualificationPeriodDate;

	/** 認定番号 */
	@Column(name = "qualification_number")
	private Integer qualificationNumber;

	/** 更新回数 */
	@Column(name = "renewal_count")
	private Integer renewalCount;

}