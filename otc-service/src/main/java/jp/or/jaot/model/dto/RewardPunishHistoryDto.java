package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.RewardPunishHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 賞罰履歴DTO
 */
@Getter
@Setter
public class RewardPunishHistoryDto extends BaseDto<RewardPunishHistoryDto, RewardPunishHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private RewardPunishHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private RewardPunishHistoryDto(RewardPunishHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static RewardPunishHistoryDto create() {
		return new RewardPunishHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static RewardPunishHistoryDto createSingle() {
		return new RewardPunishHistoryDto(new RewardPunishHistoryEntity());
	}
}
