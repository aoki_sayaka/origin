package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 会員パスワード履歴エンティティ<br>
 */
@Entity
@Table(name = "member_password_histories")
@Getter
@Setter
public class MemberPasswordHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** パスワード */
	private String password;
	
	/** 仮パスワード */
	@Column(name="temp_password")
	private String tempPassword;

	/** 仮パスワードフラグ */
	@Column(name="temp_password_flag")
	private Boolean tempPasswordFlag;
	
	/** 有効期限 */
	@Column(name="temp_password_expiration_datetime")
	private Date tempPasswordExpirationDatetime;
}
