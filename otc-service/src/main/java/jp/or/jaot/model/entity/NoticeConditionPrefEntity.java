package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(都道府県)エンティティ<br>
 */
@Entity
@Table(name = "notice_condition_prefs")
@Getter
@Setter
public class NoticeConditionPrefEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** お知らせID */
	@Column(name = "notice_id")
	private Long noticeId;

	/** 都道府県コード */
	@Column(name = "pref_code")
	private String prefCode;
}
