package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者認定履歴エンティティ<br>
 */
@Entity
@Table(name = "clinical_training_leader_qualify_histories")
@Getter
@Setter
public class ClinicalTrainingLeaderQualifyHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 研修種別 */
	@Column(name = "training_type")
	private String trainingType;

	/** 回数 */
	@Column(name = "seq")
	private Integer seq;

	/** 認定年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "qualification_date")
	private Date qualificationDate;

	/** 認定番号 */
	@Column(name = "qualification_number")
	private Integer qualificationNumber;

}
