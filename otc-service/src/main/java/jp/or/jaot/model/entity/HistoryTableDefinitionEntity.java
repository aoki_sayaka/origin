package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 履歴テーブル定義エンティティ<br>
 */
@Entity
@Table(name = "history_table_definitions")
@Getter
@Setter
public class HistoryTableDefinitionEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** テーブルコード */
	@Column(name = "table_cd")
	private Integer tableCd;

	/** テーブル名 */
	@Column(name = "table_nm")
	private String tableNm;

	/** テーブル名(物理) */
	@Column(name = "table_physical_nm")
	private String tablePhysicalNm;
}
