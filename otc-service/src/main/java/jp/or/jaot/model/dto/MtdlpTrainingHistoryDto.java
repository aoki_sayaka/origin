package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.MtdlpTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * MTDLP研修受講履歴DTO
 */
@Getter
@Setter
public class MtdlpTrainingHistoryDto extends BaseDto<MtdlpTrainingHistoryDto, MtdlpTrainingHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private MtdlpTrainingHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private MtdlpTrainingHistoryDto(MtdlpTrainingHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static MtdlpTrainingHistoryDto create() {
		return new MtdlpTrainingHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static MtdlpTrainingHistoryDto createSingle() {
		return new MtdlpTrainingHistoryDto(new MtdlpTrainingHistoryEntity());
	}
}
