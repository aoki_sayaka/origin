package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会講師実績エンティティ<br>
 */
@Entity
@Table(name = "training_instructors")
@Getter
@Setter
public class TrainingInstructorEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 研修会番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 講師番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 講師名 */
	@Column(name = "instructor_nm")
	private String instructorNm;

	/** 表示順 */
	@Column(name = "display_seq_no")
	private Integer displaySeqNo;

	/** 講師区分 */
	@Column(name = "instructor_type_cd")
	private String instructorTypeCd;

	/** 講師分類 */
	@Column(name = "instructor_category_cd")
	private String instructorCategoryCd;

	/** 講師上長宛公文書有無 */
	@Column(name = "doc_to_superiors_cd")
	private String docToSuperiorsCd;

	/** 講師上長宛公文書印刷フラグ */
	@Column(name = "doc_to_superiors_print_flag")
	private Boolean docToSuperiorsPrintFlag;

	/** 講師上長施設名 */
	@Column(name = "doc_to_superiors_facility_nm")
	private String docToSuperiorsFacilityNm;

	/** 講師上長宛名 */
	@Column(name = "doc_to_superiors_nm")
	private String docToSuperiorsNm;

	/** 講師上長宛公文書発送日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "doc_to_superiors_send_date")
	private Date docToSuperiorsSendDate;

	/** 講師本人宛公文書有無 */
	@Column(name = "doc_to_person_cd")
	private String docToPersonCd;

	/** 講師本人宛公文書印刷フラグ */
	@Column(name = "doc_to_person_print_flag")
	private Boolean docToPersonPrintFlag;

	/** 講師本人施設名 */
	@Column(name = "doc_to_person_facility_nm")
	private String docToPersonFacilityNm;

	/** 講師本人宛名 */
	@Column(name = "doc_to_person_nm")
	private String docToPersonNm;

	/** 講師本人公文書発送日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "doc_to_person_send_date")
	private Date docToPersonSendDate;

	/** 謝金記載区分 */
	@Column(name = "reward_description_category_cd")
	private String rewardDescriptionCategoryCd;

	/** 謝金記載 */
	@Column(name = "reward_description")
	private String rewardDescription;

	/** 謝金区分 */
	@Column(name = "reward_category_cd")
	private String rewardCategoryCd;

	/** 講師ポイント */
	@Column(name = "instructor_point")
	private Integer instructorPoint;

	/** 講師実績フラグ */
	@Column(name = "instructor_achievement_flag")
	private String instructorAchievementFlag;
}
