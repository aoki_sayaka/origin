package jp.or.jaot.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * お知らせ(宛先)エンティティ<br>
 */
@Entity
@Table(name = "notice_destinations")
@Getter
@Setter
public class NoticeDestinationEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** お知らせID */
	@Column(name = "notice_id")
	private Long noticeId;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 既読フラグ */
	@Column(name = "read_flg")
	private Boolean readFlg;

	/** 開封日時 */
	@Column(name = "read_time")
	private Timestamp readTime;
}
