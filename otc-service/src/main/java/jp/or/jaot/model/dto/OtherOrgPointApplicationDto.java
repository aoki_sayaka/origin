package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.OtherOrgPointApplicationCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 他団体・SIGポイント申請DTO
 */
@Getter
@Setter
public class OtherOrgPointApplicationDto
		extends BaseDto<OtherOrgPointApplicationDto, OtherOrgPointApplicationCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private OtherOrgPointApplicationDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private OtherOrgPointApplicationDto(OtherOrgPointApplicationCompositeEntity entity) {
		//CompositeEntityに変更（複合）
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static OtherOrgPointApplicationDto create() {
		return new OtherOrgPointApplicationDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static OtherOrgPointApplicationDto createSingle() {
		return new OtherOrgPointApplicationDto(new OtherOrgPointApplicationCompositeEntity());
		//CompositeEntityに変更（複合）
	}

}
