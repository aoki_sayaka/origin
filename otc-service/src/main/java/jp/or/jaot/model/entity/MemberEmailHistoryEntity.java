package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "member_email_address_histories")
@Getter
@Setter

public class MemberEmailHistoryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** メールアドレス */
	@Column(name="email_address")
	private String emailAddress ;

	/** 仮メールアドレスフラグ */
	@Column(name="temp_flag")
	private Boolean tempFlag;

	/** 仮メールアドレスキー値 */
	@Column(name="temp_key")
	private String tempKey;

	/** 有効期限 */
	@Column(name="temp_expiration_datetime")
	private Date tempExpirationDatetime;
}
