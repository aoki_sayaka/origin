package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.CaseReportEntity;
import jp.or.jaot.model.entity.CasesReportEntity;
import jp.or.jaot.model.entity.PeriodExtensionApplicationEntity;
import jp.or.jaot.model.entity.QualificationOtAppHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtAttendingSummaryEntity;
import jp.or.jaot.model.entity.QualifiedOtCompletionHistoryEntity;
import jp.or.jaot.model.entity.QualifiedOtTrainingHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修履歴エンティティ(複合)<br>
 * ・会員情報と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class QualifiedOtAttendingSummaryCompositeEntity implements Entity {

	// 認定作業療法士研修履歴
	private QualifiedOtAttendingSummaryEntity qualifiedOtAttendingSummaryEntity = new QualifiedOtAttendingSummaryEntity();

	// 認定作業療法士研修修了認定更新履歴
	private List<QualifiedOtCompletionHistoryEntity> qualifiedOtCompletionHistoryEntities = new ArrayList<QualifiedOtCompletionHistoryEntity>();

	// 認定作業療法士研修受講履歴
	private List<QualifiedOtTrainingHistoryEntity> qualifiedOtTrainingHistoryEntities = new ArrayList<QualifiedOtTrainingHistoryEntity>();

	// 事例報告
	private List<CasesReportEntity> casesReportEntities = new ArrayList<CasesReportEntity>();

	// 事例報告
	private List<CaseReportEntity> caseReportEntities = new ArrayList<CaseReportEntity>();

	// 認定作業療法士申請履歴
	private List<QualificationOtAppHistoryEntity> qualificationOtAppHistoryEntities = new ArrayList<QualificationOtAppHistoryEntity>();

	// 期間延長申請
	private List<PeriodExtensionApplicationEntity> periodExtensionApplicationEntities = new ArrayList<PeriodExtensionApplicationEntity>();
}
