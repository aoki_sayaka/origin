package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.PaotOfficerHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 県士会役員履歴DTO
 */
@Getter
@Setter
public class PaotOfficerHistoryDto extends BaseDto<PaotOfficerHistoryDto, PaotOfficerHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private PaotOfficerHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private PaotOfficerHistoryDto(PaotOfficerHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static PaotOfficerHistoryDto create() {
		return new PaotOfficerHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static PaotOfficerHistoryDto createSingle() {
		return new PaotOfficerHistoryDto(new PaotOfficerHistoryEntity());
	}
}
