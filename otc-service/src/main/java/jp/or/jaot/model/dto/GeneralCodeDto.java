package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.GeneralCodeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 汎用コードDTO
 */
@Getter
@Setter
public class GeneralCodeDto extends BaseDto<GeneralCodeDto, GeneralCodeEntity> {

	/**
	 * コンストラクタ
	 */
	private GeneralCodeDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private GeneralCodeDto(GeneralCodeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static GeneralCodeDto create() {
		return new GeneralCodeDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static GeneralCodeDto createSingle() {
		return new GeneralCodeDto(new GeneralCodeEntity());
	}
}
