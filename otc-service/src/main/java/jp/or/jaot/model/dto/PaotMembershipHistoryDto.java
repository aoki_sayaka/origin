package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.PaotMembershipHistoryEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 県士会在籍履歴DTO
 */
@Getter
@Setter
public class PaotMembershipHistoryDto extends BaseDto<PaotMembershipHistoryDto, PaotMembershipHistoryEntity> {

	/**
	 * コンストラクタ
	 */
	private PaotMembershipHistoryDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private PaotMembershipHistoryDto(PaotMembershipHistoryEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static PaotMembershipHistoryDto create() {
		return new PaotMembershipHistoryDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static PaotMembershipHistoryDto createSingle() {
		return new PaotMembershipHistoryDto(new PaotMembershipHistoryEntity());
	}
}
