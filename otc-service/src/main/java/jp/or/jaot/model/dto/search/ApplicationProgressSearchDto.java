package jp.or.jaot.model.dto.search;

import java.util.Date;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 申請進捗検索DTO
 */
@Getter
@Setter
public class ApplicationProgressSearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;

	/** 受付日範囲(From) */
	private Date applyDateRangeFrom;

	/** 受付日範囲(To) */
	private Date applyDateRangeTo;

	// TODO : 検索条件追加
}
