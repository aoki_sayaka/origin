package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.FacilityCategoryEntity;
import jp.or.jaot.model.entity.FacilityEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校エンティティ(複合)<br>
 * ・施設養成校と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class FacilityCompositeEntity implements Entity {

	/** 施設養成校 */
	private FacilityEntity facilityEntity = new FacilityEntity();

	/** 施設分類 */
	private List<FacilityCategoryEntity> facilityCategoryEntities = new ArrayList<FacilityCategoryEntity>();
}
