package jp.or.jaot.model.entity.composite;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.model.entity.TrainingAttendanceResultEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会出欠合否エンティティ(複合)<br>
 * ・研修会出欠合否と関連を持つ親情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 */
@Getter
@Setter
public class TrainingAttendanceResultCompositeEntity implements Entity {

	/** 研修会出欠合否 */
	private TrainingAttendanceResultEntity trainingAttendanceResultEntity = new TrainingAttendanceResultEntity();

	/** 研修会[親] */
	private TrainingEntity trainingEntity = new TrainingEntity();
}
