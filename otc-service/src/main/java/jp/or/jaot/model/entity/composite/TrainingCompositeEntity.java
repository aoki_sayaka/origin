package jp.or.jaot.model.entity.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.StringUtils;

import jp.or.jaot.core.model.Entity;
import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.domain.TrainingApplicationDomain;
import jp.or.jaot.model.entity.TrainingApplicationEntity;
import jp.or.jaot.model.entity.TrainingEntity;
import jp.or.jaot.model.entity.TrainingInstructorEntity;
import jp.or.jaot.model.entity.TrainingOperatorEntity;
import jp.or.jaot.model.entity.TrainingOrganizerEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会エンティティ(複合)<br>
 * ・研修会と関連を持つ従属情報を保持するエンティティ<br>
 * ・通常のエンティティとは違い永続化等は行わない<br>
 * ・リスト項目表示に必要な値を返却する<br>
 */
@Getter
@Setter
public class TrainingCompositeEntity implements Entity {

	/** 研修会 */
	private TrainingEntity trainingEntity = new TrainingEntity();

	/** 研修会申込 */
	private List<TrainingApplicationEntity> trainingApplicationEntities = new ArrayList<TrainingApplicationEntity>();

	/** 研修会主催者 */
	private List<TrainingOrganizerEntity> trainingOrganizerEntities = new ArrayList<TrainingOrganizerEntity>();

	/** 研修会講師実績 */
	private List<TrainingInstructorEntity> trainingInstructorEntities = new ArrayList<TrainingInstructorEntity>();

	/** 研修会運営担当者 */
	private List<TrainingOperatorEntity> trainingOperatorEntities = new ArrayList<TrainingOperatorEntity>();

	/**
	 * 通知取得
	 * @return 通知
	 */
	public String getNotification() {

		// TODO
		String notificationText = "";

		return notificationText;
	}

	/**
	 * 確定数取得
	 * @return 確定数
	 */
	public Integer getDecisionCount() {

		// 研修会申込状況 : 受講決定
		String statusCd = TrainingApplicationDomain.STATUS_CD.DECISION_ATTENDANCE.getValue();

		// 該当数カウント
		return CollectionUtils.isEmpty(trainingApplicationEntities) ? (0)
				: CollectionUtils.size(trainingApplicationEntities.stream()
						.filter(E -> statusCd.equals(E.getApplicationStatusCd())).collect(Collectors.toList()));
	}

	/**
	 * 申込数取得
	 * @return 申込数
	 */
	public Integer getSubscriptionCount() {
		return CollectionUtils.size(trainingApplicationEntities);
	}

	/**
	 * 開催日(文字列)取得
	 * @return 開催日文字列(カンマ区切り)
	 */
	public String getEventDateText() {

		List<String> texts = new ArrayList<String>();

		// 開催日1
		if (trainingEntity.getEventDate1() != null) {
			texts.add(DateUtil.formatYmdeString(trainingEntity.getEventDate1()));
		}
		// 開催日2
		if (trainingEntity.getEventDate2() != null) {
			texts.add(DateUtil.formatYmdeString(trainingEntity.getEventDate2()));
		}
		// 開催日3
		if (trainingEntity.getEventDate3() != null) {
			texts.add(DateUtil.formatYmdeString(trainingEntity.getEventDate3()));
		}
		// 開催日フリー
		if (StringUtils.hasText(trainingEntity.getEventDateFree())) {
			texts.add(trainingEntity.getEventDateFree());
		}

		return texts.stream().map(E -> E.toString()).collect(Collectors.joining(","));
	}
}
