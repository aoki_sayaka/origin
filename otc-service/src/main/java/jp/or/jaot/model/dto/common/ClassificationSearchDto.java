package jp.or.jaot.model.dto.common;

import jp.or.jaot.core.model.BaseSearchDto.CATEGORY_DIVISION;
import jp.or.jaot.core.model.BaseSearchDto.CLASS_CONDITION;
import lombok.Getter;
import lombok.Setter;

/**
 * 分類検索DTO
 */
@Getter
@Setter
public class ClassificationSearchDto {

	/** 分類条件 */
	private CLASS_CONDITION condition = CLASS_CONDITION.OR; // 含む

	/** 分類区分 */
	private CATEGORY_DIVISION division = CATEGORY_DIVISION.LARGE; // 大分類

	/** 分類コード */
	private String[] codes;
}
