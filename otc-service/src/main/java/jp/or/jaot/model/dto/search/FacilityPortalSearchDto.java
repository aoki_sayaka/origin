package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import jp.or.jaot.model.dto.common.RegionSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校検索DTO(会員ポータル)
 */
@Getter
@Setter
public class FacilityPortalSearchDto extends BaseSearchDto {

	/** 施設番号 */
	private String facilityNo;

	/** 施設養成校名(漢字) */
	private String name;

	/** 施設養成校名(カナ) */
	private String nameKana;

	/** 都道府県コード */
	private String[] prefectureCds;

	/** 施設区分[分類種別] */
	private String[] facilityCategories;

	/** 養成校種別 */
	private String[] trainingSchoolCategoryCds;

	/** 開設者種別 */
	private String[] openerTypeCds;

	/** 住所 */
	private String address;

	/** 領域 */
	private RegionSearchDto[] regions;

	/** 英文表記 */
	private String englishNotation;

	/** 課程年限 */
	private String courseYears;

	/** 修士課程有無 */
	private String masterCourseFlag;

	/** 博士課程有無 */
	private String doctorCourseFlag;

	/** 昼間・夜間フラグ */
	private String dayNightFlag;

	/** 開設年度(From) */
	private Integer openFiscalYearFrom;

	/** 開設年度(To) */
	private Integer openFiscalYearTo;

	/** 会員在籍者 */
	private Integer countMember;

	/** 会員在籍者(範囲条件) */
	private RANGE_CONDITION countMemberRC;

	/** 免許取得３年以上 */
	private Integer countLicense3year;

	/** 免許取得３年以上(範囲条件) */
	private RANGE_CONDITION countLicense3yearRC;

	/** 免許取得５年以上 */
	private Integer countLicense5year;

	/** 免許取得５年以上(範囲条件) */
	private RANGE_CONDITION countLicense5yearRC;

	/** 臨床実習指導施設認定状況 */
	private String[] clinicalTrainingFacilityCertifiedStatuses;

	/** WFOT認定状況 */
	private Integer[] wfotCertifiedStatuses;

	/** MTDLP推進協力校(true=認定, false=未申請) */
	private Boolean mtdlpCooperatorFlag;

	/** 基礎研修修了者在籍有無(true=在籍している, false=在籍していない) */
	private Boolean isRegistBasicOtQualify;

	/** 認定作業療法士在籍有無(true=在籍している, false=在籍していない) */
	private Boolean isRegistQualificationOtAttending;

	/** 専門作業療法士在籍分野 */
	private String[] professionalOtAttendingFields;

	/** 臨床実習指導者研修修了者在籍有無(true=在籍している, false=在籍していない) */
	private Boolean isRegistClinicalTrainingLeaderTraining;

	/** 臨床実習指導者講習受講者在籍有無(true=在籍している, false=在籍していない) */
	private Boolean isRegistSessionAttendQualification;

	/** MTDLP基礎研修修了者在籍有無(true=在籍している, false=在籍していない) */
	private Boolean isRegistMtdlpBasicAttending;

	/** MTDLP研修修了者在籍有無(true=在籍している, false=在籍していない) */
	private Boolean isRegistMtdlpTraining;

	/** MTDLP認定者在籍有無(true=在籍している, false=在籍していない) */
	private Boolean isRegistMtdlpTrainingLeader;
}
