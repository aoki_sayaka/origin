package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.ZipcodeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 郵便番号DTO
 */
@Getter
@Setter
public class ZipcodeDto extends BaseDto<ZipcodeDto, ZipcodeEntity> {

	/**
	 * コンストラクタ
	 */
	private ZipcodeDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private ZipcodeDto(ZipcodeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static ZipcodeDto create() {
		return new ZipcodeDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static ZipcodeDto createSingle() {
		return new ZipcodeDto(new ZipcodeEntity());
	}
}
