package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 事務局ロール検索DTO
 */
@Getter
@Setter
public class UserRoleSearchDto extends BaseSearchDto {

	/** 事務局ユーザID */
	private Long userId;

	// TODO : 検索条件追加
}
