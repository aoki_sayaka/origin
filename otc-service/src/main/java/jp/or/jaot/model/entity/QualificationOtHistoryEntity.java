package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 認定作業療法士研修受講履歴エンティティ<br>
 */
@Entity
@Table(name = "qualification_ot_histories")
@Getter
@Setter
public class QualificationOtHistoryEntity  extends BaseEntity{
	
	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;
	
	/** 回数 */
	@Column(name = "seq")
	private Integer seq;
	
	/** 申請パターン */
	@Column(name = "application_pattern")
	private String applicationPattern;
	
	/** 実例報告登録制度1テーマ */
	@Column(name = "case_report1_theme")
	private String caseReport1Theme;
	
	/** 実例報告登録制度1年月日 */
	@Column(name = "case_report1_date")
	private String caseReport1Date;
	
	/** 実例報告登録制度2テーマ */
	@Column(name = "case_report2_theme")
	private String caseReport2Theme;
	
	/** 実例報告登録制度2年月日 */
	@Column(name = "case_report2_date")
	private String caseReport2Date;
	
	/** 実例報告登録制度3テーマ */
	@Column(name = "case_report3_theme")
	private String caseReport3Theme;
	
	/** 実例報告登録制度3年月日 */
	@Column(name = "case_report3_date")
	private String caseReport3Date;
	
	/** 活動実績（協会主催）1年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result_jaot1_date")
	private Date activityResultJaot1Date;
	
	/** 活動実績（協会主催）1内容*/
	@Column(name = "activity_result_jaot1_content")
	private String activityResultJaot1Content;
	
	/** 活動実績（協会主催）2年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result_jaot2_date")
	private Date activityResultJaot2Date;
	
	/** 活動実績（協会主催）2内容 */
	@Column(name = "activity_result_jaot2_content")
	private String activityResultJaot2Content;
	
	/** 活動実績（協会主催）3年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result_jaot3_date")
	private Date activityResultJaot3Date;
	
	/** 活動実績（協会主催）3内容 */
	@Column(name = "activity_result_jaot3_content")
	private String activityResultJaot3Content;
	
	/** 活動実績（協会主催）4年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result_jaot4_date")
	private Date activityResultJaot4Date;
	
	/** 活動実績（協会主催）4内容 */
	@Column(name = "activity_result_jaot4_content")
	private String activityResultJaot4Content;
	
	/** 活動実績（協会主催）5年月日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result_jaot5_date")
	private Date activityResultJaot5Date;
	
	/** 活動実績（協会主催）5内容 */
	@Column(name = "activity_result_jaot5_content")
	private String activityResultJaot5Content;
	
	/** 活動実績1発表日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result1_presentation_date")
	private Date activityResult1PresentationDate;
	
	/** 活動実績1発表者区分コード */
	@Column(name = "activity_result1_role_cd")
	private String activityResult1RoleCd;
	
	/** 活動実績1タイトル */
	@Column(name = "activity_result1_title")
	private String activityResult1Title;
	
	/** 活動実績1カテゴリコード */
	@Column(name = "activity_result1_category_cd")
	private String activityResult1CategoryCd;
	
	/** 活動実績2発表日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result2_presentation_date")
	private Date activityResult2PresentationDate;
	
	/** 活動実績2発表者区分コード */
	@Column(name = "activity_result2_role_cd")
	private String activityResult2RoleCd;
	
	/** 活動実績2タイトル */
	@Column(name = "activity_result2_title")
	private String activityResult2Title;
	
	/** 活動実績2カテゴリコード */
	@Column(name = "activity_result2_category_cd")
	private String activityResult2CategoryCd;
	
	/** 活動実績3発表日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result3_presentation_date")
	private Date activityResult3PresentationDate;
	
	/** 活動実績3発表者区分コード */
	@Column(name = "activity_result3_role_cd")
	private String activityResult3RoleCd;
	
	/** 活動実績3タイトル */
	@Column(name = "activity_result3_title")
	private String activityResult3Title;
	
	/** 活動実績3カテゴリコード */
	@Column(name = "activity_result3_category_cd")
	private String activityResult3CategoryCd;
	
	/** 活動実績4発表日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result4_presentation_date")
	private Date activityResult4PresentationDate;
	
	/** 活動実績4発表者区分コード */
	@Column(name = "activity_result4_role_cd")
	private String activityResult4RoleCd;
	
	/** 活動実績4タイトル */
	@Column(name = "activity_result4_title")
	private String activityResult4Title;
	
	/** 活動実績4カテゴリコード */
	@Column(name = "activity_result4_category_cd")
	private String activityResult4CategoryCd;
	
	/** 活動実績5発表日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "activity_result5_presentation_date")
	private Date activityResult5PresentationDate;
	
	/** 活動実績5発表者区分コード */
	@Column(name = "activity_result5_role_cd")
	private String activityResult5RoleCd;
	
	/** 活動実績5タイトル */
	@Column(name = "activity_result5_title")
	private String activityResult5Title;
	
	/** 活動実績5カテゴリコード */
	@Column(name = "activity_result5_category_cd")
	private String activityResult5CategoryCd;
	
	/** 臨床実習能力試験受験日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "clinical_training_capacity_test_date")
	private Date clinicalTrainingCapacityTestDate;
	
	/** 他団体・SIG等の認定資格コード */
	@Column(name = "other_sig_qualification_cd")
	private String otherSigQualificationCd;
	
	/** 他団体・SIG等の認定資格　取得日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "other_sig_qualification_date")
	private Date otherSigQualificationDate;
	
	/** 他団体・SIG等の認定資格　証明書ファイル名 */
	@Column(name = "other_sig_qualification_file_name")
	private String otherSigQualificationFileName;
	
	/** 他団体・SIG等の認定資格　証明書 */
	@Column(name = "other_sig_qualification_file_data")
	private String otherSigQualificationFileData;
	
	/** 臨床実践報告書1指導会員番号 */
	@Column(name = "clinical_training_report1_member_no")
	private Integer clinicalTrainingReport1MemberNo;
	
	/** 臨床実践報告書1報告書ファイル名 */
	@Column(name = "clinical_training_report1_file_name")
	private String clinicalTrainingReport1FileName;
	
	/** 臨床実践報告書1報告書 */
	@Column(name = "clinical_training_report1_file_data")
	private String clinicalTrainingReport1FileData;
	
	/** 臨床実践報告書2指導会員番号 */
	@Column(name = "clinical_training_report2_member_no")
	private Integer clinicalTrainingReport2MemberNo;
	
	/** 臨床実践報告書2報告書ファイル名 */
	@Column(name = "clinical_training_report2_file_name")
	private String clinicalTrainingReport2FileName;
	
	/** 臨床実践報告書2報告書 */
	@Column(name = "clinical_training_report2_file_data")
	private String clinicalTrainingReport2FileData;
	
	/** 臨床実践報告書3指導会員番号 */
	@Column(name = "clinical_training_report3_member_no")
	private Integer clinicalTrainingReport3MemberNo;
	
	/** 臨床実践報告書3報告書ファイル名 */
	@Column(name = "clinical_training_report3_file_name")
	private String clinicalTrainingReport3FileName;
	
	/** 臨床実践報告書3報告書 */
	@Column(name = "clinical_training_report3_file_data")
	private String clinicalTrainingReport3FileData;
	
	/** 臨床実践報告書4指導会員番号 */
	@Column(name = "clinical_training_report4_member_no")
	private Integer clinicalTrainingReport4MemberNo;
	
	/** 臨床実践報告書4報告書ファイル名 */
	@Column(name = "clinical_training_report4_file_name")
	private String clinicalTrainingReport4FileName;
	
	/** 臨床実践報告書4報告書 */
	@Column(name = "clinical_training_report4_file_data")
	private String clinicalTrainingReport4FileData;
	
	/** 臨床実践報告書5指導会員番号 */
	@Column(name = "clinical_training_report5_member_no")
	private Integer clinicalTrainingReport5MemberNo;
	
	/** 臨床実践報告書5報告書ファイル名 */
	@Column(name = "clinical_training_report5_file_name")
	private String clinicalTrainingReport5FileName;
	
	/** 臨床実践報告書5報告書 */
	@Column(name = "clinical_training_report5_file_data")
	private String clinicalTrainingReport5FileData;
	
	/** 論文1発表先コード */
	@Column(name = "paper1_presentation_cd")
	private String paper1PresentationCd;
	
	/** 論文1備考 */
	@Column(name = "paper1_remarks")
	private String paper1Remarks;
	
	/** 論文1その他 */
	@Column(name = "paper1_other")
	private String paper1Other;
	
	/** 論文2発表先コード */
	@Column(name = "paper2_presentation_cd")
	private String paper2PresentationCd;
	
	/** 論文2備考 */
	@Column(name = "paper2_remarks")
	private String paper2Remarks;
	
	/** 論文2その他 */
	@Column(name = "paper2_other")
	private String paper2Other;
	
}
