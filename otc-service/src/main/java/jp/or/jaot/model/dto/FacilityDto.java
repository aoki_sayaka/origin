package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.composite.FacilityCompositeEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校DTO
 */
@Getter
@Setter
public class FacilityDto extends BaseDto<FacilityDto, FacilityCompositeEntity> {

	/**
	 * コンストラクタ
	 */
	private FacilityDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private FacilityDto(FacilityCompositeEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static FacilityDto create() {
		return new FacilityDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static FacilityDto createSingle() {
		return new FacilityDto(new FacilityCompositeEntity());
	}
}
