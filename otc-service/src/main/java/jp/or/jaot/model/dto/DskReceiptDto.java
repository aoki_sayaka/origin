package jp.or.jaot.model.dto;

import jp.or.jaot.core.model.BaseDto;
import jp.or.jaot.model.entity.DskReceiptEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * DskPOSTリクエストDTO
 */
@Getter
@Setter
public class DskReceiptDto extends BaseDto<DskReceiptDto, DskReceiptEntity> {
	/**
	 * コンストラクタ
	 */
	private DskReceiptDto() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param entity エンティティ
	 */
	private DskReceiptDto(DskReceiptEntity entity) {
		super(entity);
	}

	/**
	 * DTO生成
	 * @return DTO
	 */
	public static DskReceiptDto create() {
		return new DskReceiptDto();
	}

	/**
	 * DTO生成(単一エンティティ追加)
	 * @return DTO
	 */
	public static DskReceiptDto createSingle() {
		return new DskReceiptDto(new DskReceiptEntity());
	}
}
