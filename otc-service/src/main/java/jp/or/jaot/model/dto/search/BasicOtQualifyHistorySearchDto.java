package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 基礎研修修了認定更新履歴検索DTO
 */
@Getter
@Setter
public class BasicOtQualifyHistorySearchDto extends BaseSearchDto {

	/** 会員番号 */
	private Integer memberNo;
}
