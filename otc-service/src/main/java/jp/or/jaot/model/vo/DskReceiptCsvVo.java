package jp.or.jaot.model.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.github.mygreen.supercsv.annotation.CsvBean;
import com.github.mygreen.supercsv.annotation.CsvColumn;
import com.github.mygreen.supercsv.annotation.conversion.CsvNullConvert;
import com.github.mygreen.supercsv.annotation.conversion.CsvRegexReplace;

import jp.or.jaot.core.util.DateUtil;
import jp.or.jaot.model.entity.DskReceiptEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * DskPOSTリクエストCSVBean
 */
@CsvBean(header = true)
@Getter
@Setter
public class DskReceiptCsvVo {
	@CsvColumn(number = 1, label = "種別")
	private String typeCd;

	@CsvColumn(number = 2, label = "収納日")
	@CsvRegexReplace(regex = "([0-9]{4})([0-9]{1,2})([0-9]{1,2})", replacement = "$1-$2-$3")
	private String receiptedDate;

	@CsvColumn(number = 3, label = "収納時刻")
	@CsvNullConvert("0")
	@CsvRegexReplace(regex = "([0-9]{1,2})([0-9]{1,2})", replacement = "$1:$2")
	private String receiptedTime;

	@CsvColumn(number = 4, label = "バーコード情報")
	private String barcode;

	@CsvColumn(number = 5, label = "ユーザー使用欄1")
	private String userData1;

	@CsvColumn(number = 6, label = "ユーザー使用欄2")
	private String userData2;

	@CsvColumn(number = 7, label = "印紙フラグ")
	private String stamp;

	@CsvColumn(number = 8, label = "金額")
	private String amountOfMoney;

	@CsvColumn(number = 9, label = "コンビニ本部コード")
	private String convenienceHeadCd;

	@CsvColumn(number = 10, label = "コンビニ名")
	private String convenienceName;

	@CsvColumn(number = 11, label = "コンビニ店舗コード")
	private String convenienceCd;

	@CsvColumn(number = 12, label = "振込日")
	@CsvNullConvert("0")
	@CsvRegexReplace(regex = "([0-9]{4})([0-9]{1,2})([0-9]{1,2})", replacement = "$1-$2-$3")
	private String transferDate;

	@CsvColumn(number = 13, label = "データ作成日")
	@CsvRegexReplace(regex = "([0-9]{4})([0-9]{1,2})([0-9]{1,2})", replacement = "$1-$2-$3")
	private String creationDate;

	public DskReceiptCsvVo() {
	}

	/**
	 * エンティティにフィールドの値を代入するメソッド
	 * @return　エンティティ
	 */
	public DskReceiptEntity getEntity() throws Exception {
		Date dates = null;
		Boolean stamps = null;
		Integer moneys = 0;
		String billingId = null;
		String memberNo = null;
		int numberOfReissues = 0;
		String paymentDueDate = null;

		DskReceiptEntity entity = new DskReceiptEntity();

		entity.setTypeCd(this.typeCd);

		dates = DateUtil.parseDateDB(this.receiptedDate);
		entity.setReceiptedDate(dates);

		dates = DateUtil.parseTimeDB(this.receiptedTime);
		entity.setReceiptedTime(dates);

		entity.setBarcode(this.barcode);
		entity.setUserData1(this.userData1);
		entity.setUserData2(this.userData2);

		if ("0".equals(this.stamp)) {
			stamps = false;
		} else if ("1".equals(this.stamp)) {
			stamps = true;
		}
		entity.setStamp(stamps);

		moneys = Integer.parseInt(this.amountOfMoney);
		entity.setAmountOfMoney(moneys);

		entity.setConvenienceHeadCd(this.convenienceHeadCd);
		entity.setConvenienceName(this.convenienceName);
		entity.setConvenienceCd(this.convenienceCd);

		dates = DateUtil.parseDateDB(this.transferDate);
		entity.setTransferDate(dates);

		dates = DateUtil.parseDateDB(this.creationDate);
		entity.setCreationDate(dates);

		billingId = this.barcode.substring(16, 26);
		entity.setBillingId(billingId);

		memberNo = this.barcode.substring(26, 32);
		entity.setMemberNo(memberNo);

		numberOfReissues = Integer.parseInt(this.barcode.substring(32, 33));
		entity.setNumberOfReissues(numberOfReissues);

		paymentDueDate = this.barcode.substring(33, 39);
		if (!("999999".equals(paymentDueDate))) {
			SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
			Date date = format.parse(paymentDueDate);
			entity.setPaymentDueDate(date);
		} else if ("999999".equals(paymentDueDate)) {
			entity.setPaymentDueDate(null);
		}

		return entity;

	}
}
