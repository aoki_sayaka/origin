package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 施設養成校勤務者検索DTO
 */
@Getter
@Setter
public class FacilityMemberSearchDto extends BaseSearchDto {

	/** 施設養成校ID */
	private Long[] facilityIds;

	/** 会員番号 */
	private Integer memberNo;
}
