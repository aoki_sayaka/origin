package jp.or.jaot.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 郵便番号エンティティ<br>
 */
@Entity
@Table(name = "zipcodes")
@Getter
@Setter
public class ZipcodeEntity implements jp.or.jaot.core.model.Entity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 郵便番号 */
	private String zipcode;

	/** 住所 */
	private String address;

	/** 都道府県番号 */
	@Column(name = "prefecture_cd")
	private String prefectureCd;

	/** 都道府県名 */
	@Column(name = "prefecture_name")
	private String prefectureName;
}
