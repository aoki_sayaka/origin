package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 研修会出欠合否エンティティ<br>
 */
@Entity
@Table(name = "training_attendance_results")
@Getter
@Setter
public class TrainingAttendanceResultEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 研修会番号 */
	@Column(name = "training_no")
	private String trainingNo;

	/** 申込者番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 開催日１日目出席状況コード */
	@Column(name = "attendance_status1_cd")
	private String attendanceStatus1Cd;

	/** 開催日２日目出席状況コード */
	@Column(name = "attendance_status2_cd")
	private String attendanceStatus2Cd;

	/** 開催日３日目出席状況コード */
	@Column(name = "attendance_status3_cd")
	private String attendanceStatus3Cd;

	/** 修了日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "completion_date")
	private Date completionDate;

	/** 修了判定 */
	@Column(name = "completion_status_cd")
	private String completionStatusCd;

	/** 参加費 */
	@Column(name = "entry_fee")
	private Integer entryFee;

	/** 氏名 */
	private String fullname;

	/** 開催日１日目退席状況コード */
	@Column(name = "leave_status1_cd")
	private String leaveStatus1Cd;

	/** 開催日２日目退席状況コード */
	@Column(name = "leave_status2_cd")
	private String leaveStatus2Cd;

	/** 開催日３日目退席状況コード */
	@Column(name = "leave_status3_cd")
	private String leaveStatus3Cd;

	/** 当日収納フラグ */
	@Column(name = "pay_on_the_day_flag")
	private Boolean payOnTheDayFlag;

	/** 再試験日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "retest_date")
	private Date retestDate;

	/** 再試験合格日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "retest_pass_date")
	private Date retestPassDate;

	/** 再試験合否 */
	@Column(name = "retest_result_cd")
	private String retestResultCd;

	/** 試験合格日 */
	@Temporal(TemporalType.DATE)
	@Column(name = "test_pass_date")
	private Date testPassDate;

	/** 試験合否 */
	@Column(name = "test_result_cd")
	private String testResultCd;
}
