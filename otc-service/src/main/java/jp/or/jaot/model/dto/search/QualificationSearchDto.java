package jp.or.jaot.model.dto.search;

import jp.or.jaot.core.model.BaseSearchDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 関連資格検索DTO
 */
@Getter
@Setter
public class QualificationSearchDto extends BaseSearchDto {

	/** コード */
	private String code;

	/** 区分コード */
	private String categoryCd;

	// TODO : 検索条件追加
}
