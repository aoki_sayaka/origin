package jp.or.jaot.model.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

/**
 * 汎用コードID(複合キー)<br>
 */
@Embeddable
@Getter
@Setter
public class GeneralCodeId implements Serializable {

	/** コードID */
	private String id;

	/** コード値 */
	private String code;

	/**
	 * コンストラクタ(デフォルト)
	 */
	public GeneralCodeId() {
	}

	/**
	 * コンストラクタ
	 * @param id コードID
	 * @param code コード値
	 */
	public GeneralCodeId(String id, String code) {
		this.id = id;
		this.code = code;
	}
}
