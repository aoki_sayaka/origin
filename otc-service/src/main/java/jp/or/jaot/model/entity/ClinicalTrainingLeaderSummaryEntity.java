package jp.or.jaot.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import jp.or.jaot.core.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 臨床実習指導者履歴エンティティ
 *
 */
@Entity
@Table(name = "clinical_training_leader_summaries")
@Getter
@Setter
public class ClinicalTrainingLeaderSummaryEntity extends BaseEntity {

	/** ID */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/** 会員番号 */
	@Column(name = "member_no")
	private Integer memberNo;

	/** 臨床実習指導者認定番号*/
	@Column(name = "training_qualification_number")
	private Integer trainingQualificationNumber;

	/** 臨床実習指導者研修修了認定日*/
	@Temporal(TemporalType.DATE)
	@Column(name = "training_qualification_date")
	private Date trainingQualificationDate;

	/** 臨床実習指導者講習会受講認定日*/
	@Temporal(TemporalType.DATE)
	@Column(name = "session_attend_qualification_date")
	private Date sessionAttendQualificationDate;

	/** 臨床実習指導者講習会受講認定番号*/
	@Column(name = "session_attend_qualification_number")
	private Integer sessionAttendQualificationNumber;

	/** 厚生労働省認定日*/
	@Temporal(TemporalType.DATE)
	@Column(name = "ministry_qualification_date")
	private Date ministryQualificationDate;

	/** 厚生労働省認定番号*/
	@Column(name = "ministry_qualification_number")
	private Integer ministryQualificationNumber;
}
